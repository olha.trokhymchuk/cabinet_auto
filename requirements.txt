appdirs==1.4.3
antigate==1.4.0
celery==4.1.1
dateparser==0.7.0
deepdiff==3.3.0
Django==1.11.10
#TODO: move to version 2
django-appconf==1.0.2
django-anymail==1.4
django-celery-beat==1.0.1
django-compressor==2.2
django-constance==2.2.0
django-environ==0.4.4
django-field-history==0.6.0
django-filter==1.0.4
django-imagekit==4.0.2
django-jsoneditor==0.0.12
django-model-utils==3.1.1
django-safedelete==0.4.5
django-silk==1.0.0
djangorestframework==3.3.2
django-db-mailer==2.3.19
grab==0.6.38
#grab
jsonfield==2.0.2
lxml==4.1.1
beautifulsoup4==4.6.3
fake-useragent==0.1.10
Markdown==2.6.8
olefile==0.45.1
opbeat==3.6.1
packaging==16.8
pilkit==2.0
Pillow==4.1.1
pyparsing==2.2.0
pytz==2018.3
#redis==2.10.6
redis==3.2.1
requests==2.18.4
urllib3<1.23,>=1.21.1
six==1.11.0
psycopg2==2.7.4
django-admin-view-permission==1.4
ipython
gunicorn
django-environ==0.4.4
cssutils
# Envs
envdir==0.7
environ

#mysql
#mysql==0.0.2

# For user registration, either via email or social
# Well-built with regular release cycles!
django-allauth==0.35.0
django-cors-headers==2.3.0

#interface
humanize==0.5.1


#proxy
pysocks
#pycurl
stem==1.6.0
requests[socks]

passlib==1.7.1


#test
pytest
pytest-django
pytest-cov
mixer

#pdf
WeasyPrint


#xlsx
XlsxWriter

#pytelegrambotapi
pytelegrambotapi==3.6.7

Fabric==1.13.1
fabtools==0.20.0

selenium-requests

#serializer
phpserialize

django-mathfilters

ecommpay-sdk

django-tinymce==2.9.0

#google photo api
google-api-python-client==1.10.1
google-auth==1.19.1
google-auth-httplib2==0.0.3
google-auth-oauthlib==0.4.1
# aiohttp==3.6.2

PyJWT==1.7.1

#paramiko
paramiko==2.7.2
