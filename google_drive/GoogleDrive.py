import pickle
import os
from datetime import datetime
from django.conf import settings
from google_auth_oauthlib.flow import Flow, InstalledAppFlow
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from .google_drive_settings import APPLICATION_NAME, DRIVE_API_VERSION, DRIVE_CLIENT_SECRET_FILE, DRIVE_SCOPES

os.environ['DJANGO_SETTINGS_MODULE'] = 'autopodbor.settings'
GOOGLE_DRIVE_TOKENS_FOLDER = settings.GOOGLE_DRIVE_TOKENS_FOLDER

def Create_Service():
    cred = None
    pickle_file = GOOGLE_DRIVE_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('autopodborbackup', DRIVE_API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(DRIVE_CLIENT_SECRET_FILE, DRIVE_SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(APPLICATION_NAME, DRIVE_API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def convert_to_RFC_datetime(year=1900, month=1, day=1, hour=0, minute=0):
    dt = datetime.datetime(year, month, day, hour, minute, 0).isoformat() + 'Z'
    return dt
