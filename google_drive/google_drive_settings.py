import os.path
from django.conf import Settings

PATH_BACK_UP_DB = '/home/aleksey/backap/'
DRIVE_SCOPES = ['https://www.googleapis.com/auth/drive',
                'https://www.googleapis.com/auth/drive.file',
                'https://www.googleapis.com/auth/drive.appdata',
                'https://www.googleapis.com/auth/drive.apps.readonly']
DRIVE_CLIENT_SECRET_FILE = 'client_secrets_to_backup.json'
APPLICATION_NAME = 'drive'
DRIVE_API_VERSION = 'v3'

FOLDER_ID = '1jvwOlc69eU_k2VzcH34Z1V3-mv-WCP8g'