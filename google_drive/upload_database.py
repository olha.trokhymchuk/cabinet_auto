import os
from .google_drive_settings import PATH_BACK_UP_DB
from googleapiclient.http import MediaFileUpload
from .GoogleDrive import Create_Service


def upload_db_to_google_drive():
    service = Create_Service()
    path = PATH_BACK_UP_DB
    dir_list = os.listdir(path)
    f = open("google_folder_id.txt", "r")
    get_folder_id = f.read()
    if dir_list:
        for file in dir_list:
            file_name = file
            file_metadata = {
                'name': file_name,
                'parents': [get_folder_id],
            }

            media = MediaFileUpload(path + file_name, resumable=True)
            try:
                upload_file = service.files().create(body=file_metadata, media_body=media).execute()
                if upload_file:
                    print('finish upload database_')
                    os.remove(path + file_name)
            except:
                return '#error upload'