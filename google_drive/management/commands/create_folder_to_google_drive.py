from django.core.management.base import BaseCommand

from google_drive.create_folder_to_google_drive import create_folder_to_google_drive

class Command(BaseCommand):
    help = 'Create folder to Google Drive'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        create_folder_to_google_drive()