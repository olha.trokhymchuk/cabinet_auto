from django.core.management.base import BaseCommand

from google_drive.upload_database import upload_db_to_google_drive

class Command(BaseCommand):
    help = 'Backup database to Google Drive'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        upload_db_to_google_drive()