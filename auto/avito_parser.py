import logging
import os
import re
import urllib
import requests
import pycurl
import cssutils
import urllib.request
from datetime import datetime

from stem import Signal
from stem.control import Controller

import subprocess

import grab
# from .custom_proxy import Proxy
import base64
import json
import dateparser
from django.core.files import File
from django.forms import model_to_dict

from lxml import html
from grab import Grab
from grab.error import GrabTimeoutError,GrabConnectionError
from antigate import AntiGate, AntiGateError
from urllib.parse import parse_qs
from django.conf import settings
from django.db import DatabaseError
from grab.spider import Spider, Task
from weblib.error import DataNotFound

from auto.models import ModelAuto, Generation, MarkAuto, AUTHOR_TYPE, AutoPhoto, Year, BodyType
from auto.utils import get_unique_list_by_key, get_choice_by_name, is_int

from .parser import Parser
logger = logging.getLogger(__name__)


#парсит объявления
class AvitoRuParser(Parser):
    def __init__(self, *args, **kwargs):
        super(AvitoRuParser, self).__init__(*args, **kwargs)
        self.headers={
            'Accept':'application/json',
            'Accept-Language':'ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4',
            'authority':'www.avito.ru',
            'pragma': 'no-cache',
            'cache-control': 'no-cache',
            'origin':'http://www.avito.ru',
            'upgrade-insecure-requests': '1',
            'content-type': 'application/x-www-form-urlencoded',
            'user-agent':'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
            'accept':'text/html, application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*,q=0.8',
            'referer': 'http://www.avito.ru/tolyatti?verifyUserLocation=1',
            'accept-encoding':'gzip, deflate, br',
            'accept-language':'ru-Ru,ru;q=0.9,en-US;q=0.8,en;q=0.7',
            'method':'GET',
            # 'path':'/user/get-status/72933886',
            'scheme':'https',
            'Content-Type':'text/html',
            'X-Requested-With':'XMLHttpRequest',
            'Connection':'keep-alive',
            'charset':'UTF-8',
            'Cookie':'u=21vmd2no.n9j660.f6wvmteb71; _ym_uid=14714265131042248860; __gads=ID=abdce9bfb52a4f31:T=1471609884:S=ALNI_Man9QAeuZ6Tl3tkxrGSUGG6wcNWTA; dfp_group=16; weborama-viewed=1; f=4.b53ee41b77d9840ae5ba07059b0d202f6e619f2005f5c6dc6e619f2005f5c6dc6e619f2005f5c6dc6e619f2005f5c6dc6e619f2005f5c6dc6e619f2005f5c6dc6e619f2005f5c6dc6e619f2005f5c6dc6e619f2005f5c6dc6e619f2005f5c6dc5b68b402642848be5b68b402642848bead33eaef55dd0da15b68b402642848be44620aa09dfab02de75a2b007093b89d05886bb864a616652f4891ba472e4f2618dc79c78ea31ba1ea48e2d99c5312aaffe65fd77b784b7bffe65fd77b784b7bb8a109ce707ef6137c6d6c44a42cb1c70176a16018517b5da399e993193588ae728b89f8cc435269728b89f8cc435269728b89f8cc435269728b89f8cc435269ffe65fd77b784b7b862357a052e106f23f601feec47f73646b10d486f2e98b94bbdd84537b03ad770afd39af11174777efa5660fd55a65b968eae11c327fbc017e3896e0dc5507a54fe26563f7e70342b3db510bee0b105f2878bfba0574374f5b68b402642848be5b68b402642848beec8be4370a6135b1dca1b47b9709106b31ad00aa0bbae7adb817e52b74497bd1; _ym_isad=1; anid=removed; sessid=af43614ec2d2387e2822cf0b5c2e7d86.1495907798; _mlocation=637640; nps_sleep=1; nfh=14fc4f6eb1638a2e3d85b8f244d7bfc4; __utmt=1; __utma=99926606.64612684.1471426513.1495906259.1495916735.183; __utmb=99926606.1.10.1495916736; __utmc=99926606; __utmz=99926606.1495216859.178.58.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); _ga=GA1.2.64612684.1471426513; _gid=GA1.2.56430582.1495885618; _gat=1; _ga=GA1.3.64612684.1471426513; _gid=GA1.3.56430582.1495885618; _gat_UA-20699421-1=1; crtg_rta=cravadb240%3D1%3B; v=1495915550',
            
        }
        self.text_blocking = 'Доступ с вашего IP-адреса временно ограничен'
        
    
    def parse_car_info(self):
        data = {}
        xpath = '//div[contains(@class, "item-view-block")]'
        try:
            path = self.grab.doc.select(xpath)[0]
        except IndexError:
            return data
        else:
            xpath = '//li[contains(@class, "item-params-list-item")]'
            path =  path.select(xpath)
            mark, model, generation = path[0].text(), path[1].text(), path[2].text()
            
            data.update({'марка': mark, 'модель': model, 'поколение': generation})
        return data

    def parse_meta_info(self):
        data = {}
        xpath = '//div[contains(@class, "title-info-metadata-item")]'
        text = self.get_text_from_element(xpath)
        try:
            list_text = text.split(' ')
        except AttributeError:
            number = list_text = text
        else:
            number = list_text[1][:-1]
        date = self.get_date_from_avito(list_text)
        xpath = '//a[contains(@class, "js-show-stat")]'

        try:
            count_views = self.grab.doc.select(xpath).text()
        except DataNotFound:
            count_of_views = 0
        else:
            count_of_views = count_views.split(' ')[0]

        data.update({
            'номер объявления': number,
            'дата публикации': dateparser.parse(date),
            'количество просмотров': count_of_views
        })

        return data

    def get_date_from_avito(self, list_text):
        try:
            day = list_text[3]
            month = list_text[4]
            time = "-{}".format(list_text[6])
        except (IndexError, TypeError) as e:
            return ""  
        return "{}-{}".format(day, month, time)

    def get_text_from_element(self, xpath):
        try:
            return self.grab.doc.select(xpath).one().text()
        except grab.error.DataNotFound as ex:
            logger.warning(ex)
            return None

    def parse_additional_info(self):
        data = {}
        try:
            price = self.grab.doc.select('//div[contains(@class, "item-price")]').text()
        except DataNotFound:
            price = 0
        else:
            price = price.replace(u'\xa0', u'').replace(' ', '').replace('₽', '')
        try:
            author = self.grab.doc.select('//div[contains(@class, "seller-info-name")]').text()
        except DataNotFound:
            author = ''
        try:
            author_type = self.get_text_from_element('//div[contains(@class, "item-view-seller-info")]').split(' ')[-1]
        except AttributeError:
            author_type = ""

        try:
            description = self.grab.doc.select('//div[contains(@class, "item-description-text")]').text()
        except DataNotFound as ex:
            description = ''
        photos = self.grab.doc.select('//span[contains(@class, "gallery-img-cover")]')
        urls = []
        data.update({'автор': author, 'тип автора': author_type, 'комментарий': description, 'цена': price})
        for p in photos:
            style = cssutils.parseStyle(p.attr('style'))
            url = style['background-image']
            urls.append('https:' + url.replace('url(', '').replace(')', ''))
        data.update({'фото': urls})
        return data

    def parse_characteristics(self):
        data = {}
        parameter = '//ul[contains(@class, "item-params-list")]/li[contains(@class, "item-params-list-item")]'
        allowed_values = ['Модель', 'Марка', 'Год выпуска', 'Тип кузова', 'Цвет', 'Объём двигателя', 'Коробка передач',
                              'Тип двигателя', 'Привод', 'Руль', 'VIN или номер кузова', 'Мощность двигателя:']
        value = self.grab.doc.select(parameter)
        for v in value:
            label, val = v.text().split(':')
            label = label.lower()
            if(label=="цвет"):
                data.update({'цвет': val.lstrip()})
            elif(label=="пробег"):
                data.update({'пробег': int(val.replace('км', '').lstrip())})
            elif(label=="мощность двигателя"):
                data.update({'двигатель лс': val.replace(' л.с.', '').lstrip()})
            elif(label=="владельцев по птс"):
                data.update({'поколение': val.lstrip()})
            elif(label=="владельцев по птс"):
                data.update({'владельцы': val.lstrip()})
            # elif(label=="тип двигателя"):
            #     data.update({'топливо': val.lstrip()})
            elif(label=="тип двигателя"):
                data.update({'двигатель топливо': val.lstrip()})
            elif(label=="коробка передач"):
                data.update({'коробка': val.lstrip()})
            elif(label=="объём двигателя"):
                data.update({'двигатель объем': val.lstrip().replace(' л', '')})
            elif(label=="vin или номер кузова"):
                data.update({'vin': val.lstrip()})
            elif(label=="тип кузова"):
                data.update({'кузов': val.lstrip()})
            else:
                data.update({label: val.lstrip()})
        return data

    def send_captcha(self):
        img = self.grab.doc.select('//img[contains(@class, "form-captcha-image")]').one()
        img_src = img.attr('src')
        captcha_base64 = base64.b64encode(requests.get(img_src).content)

        config = {'connect_timeout': 10, 'timeout': 60, 'cookiefile': self.cookiefile}
        try:
            captcha = AntiGate(settings.ANTICAPTCHA_API, captcha_base64, grab_config=config).captcha_result
            self.grab.doc.set_input_by_id('rep', captcha)
        except AntiGateError as ex:
            logger.warning(ex)

        self.grab.doc.submit()

    def parse_mobile_phone(self):
        encoded_app_config = self.grab.doc.rex_text('(?<=decodeURIComponent\(")(.*)(?=")')
        app_config = json.loads((urllib.parse.unquote(encoded_app_config)))

        if not app_config or 'pageParams' not in app_config or 'crc' not in app_config:
            return {}

        crc = app_config.get('crc')
        sale_id = app_config.get('pageParams').get('sale_id')
        sale_hash = app_config.get('pageParams').get('sale_hash')

        if not crc or not sale_id or not sale_hash:
            return {}

        url = 'https://auto.ru/-/ajax/phones/?category=cars&sale_id={sale_id}' \
              '&sale_hash={sale_hash}&__blocks=antifraud%2Ccard-phones%2Ccall-numbers&' \
              'crc={crc}'.format(sale_id=sale_id, sale_hash=sale_hash, crc=crc)

        self.grab.go(url)
        try:
            raw_phone_number = self.grab.doc.json['blocks']['call-numbers']
            tree = html.fromstring(raw_phone_number)
            raw_phone = tree.xpath('//div[contains(@class, "call-numbers__phone")]')[0].text
            phone = raw_phone.replace('-', '').replace(' ', '')
            return {'телефон': phone}
        except json.decoder.JSONDecodeError as ex:
            logger.error(ex)
            return {}

    def parse_all(self):
        data = {'ссылка': self.url}
        data.update(self.parse_meta_info())
        # data.update(self.parse_car_info())
        data.update(self.parse_characteristics())
        data.update(self.parse_additional_info())
        # data.update(self.parse_mobile_phone())
        # attempts = False
        return data


def avitoru_data_to_model(auto_ru_data):
    from auto.glossary import TRANSMISSION_TYPE, ENGINE_CAPACITY, ENGINE_TYPE, DRIVE_TYPE
    from auto.models import Generation, MarkAuto, ModelAuto, Avito

    markauto = MarkAuto.objects.filter(name=auto_ru_data['марка']).first()
    if markauto is None:
        markauto = MarkAuto.objects.create(name=auto_ru_data['марка'])

    try:
        modelauto = ModelAuto.objects.filter(mark_auto=markauto, name=auto_ru_data['модель']).first()
    except KeyError:
        modelauto = ModelAuto.objects.none()
    else:
        if modelauto is None:
            modelauto = ModelAuto.objects.create(mark_auto=markauto, name=auto_ru_data['модель'])
    try:
        generation = Generation.objects.filter(model_auto=modelauto, name=auto_ru_data['поколение']).first()
    except KeyError:
        generation = Generation.objects.none()
    else:
        if generation is None:
            Generation.objects.create(model_auto=modelauto, name=auto_ru_data['поколение'], years=(None, None))

    if auto_ru_data.get('кузов'):
        body_type = BodyType.objects.filter(name=auto_ru_data.get('кузов')).first()
        if body_type is None:
            body_type = BodyType.objects.create(name=auto_ru_data.get('кузов'))
    else:
        body_type = None

    if auto_ru_data.get('год выпуска'):
        year = Year.objects.filter(year=auto_ru_data.get('год выпуска')).first()
        if year is None:
            year = Year.objects.create(year=auto_ru_data.get('год выпуска'))
    else:
        year = None

    engine_type = auto_ru_data.get('двигатель топливо') if auto_ru_data.get('двигатель топливо') else None
    auto = Avito(vin=auto_ru_data.get('vin'),
                  mark_auto=markauto,
                  model_auto=modelauto,
                  generation=generation,
                  year_auto=year,
                  body_type_auto=body_type,
                  transmission_type=get_choice_by_name(TRANSMISSION_TYPE, auto_ru_data.get('коробка')),
                  engine_capacity=get_choice_by_name(ENGINE_CAPACITY,
                                                     auto_ru_data.get('двигатель объем'),
                                                     Avito._meta.get_field('engine_capacity').get_default()),
                  engine_type=get_choice_by_name(ENGINE_TYPE, engine_type),
                  horsepower=auto_ru_data.get('двигатель лс'),
                  drive_type=get_choice_by_name(DRIVE_TYPE, auto_ru_data.get('привод')),
                  color_auto=auto_ru_data.get('цвет'),
                  comment=auto_ru_data.get('комментарий'),
                  cost=auto_ru_data.get('цена'),
                  author=auto_ru_data.get('автор'),
                  author_type=get_choice_by_name(AUTHOR_TYPE, auto_ru_data.get('тип автора'),
                                                 Avito._meta.get_field('author_type').get_default()),
                  owners=auto_ru_data.get('владельцы'),
                  state=auto_ru_data.get('состояние'),
                  mileage=auto_ru_data.get('пробег'),
                  phone=auto_ru_data.get('телефон'),
                  ad_number=auto_ru_data.get('номер объявления'),
                  public_date=auto_ru_data.get('дата публикации'),
                  view_count=auto_ru_data.get('количество просмотров'),
                  source=auto_ru_data.get('ссылка'))

    photos = []
    for image_url in auto_ru_data['фото']:
        raw_photo = requests.get(image_url)
        uri = ("data:" +
               raw_photo.headers['Content-Type'] + ";" +
               "base64," + base64.b64encode(raw_photo.content).decode('ascii'))
        photos.append(uri)

    model_dict = model_to_dict(auto)
    if auto_ru_data['фото']:
        model_dict.update({'photos': photos})
    return model_dict
