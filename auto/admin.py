from django.contrib import admin
from django.contrib import messages
from django.core.exceptions import ValidationError
from tinymce.widgets import TinyMCE
from django.db import models

from safedelete.admin import highlight_deleted, SafeDeleteAdmin

from .models import AutoPhoto, MarkAuto, ModelAuto, Generation, Auto, AutoRU, Avito, BodyType, Year, AutoDataBase, AutoDataBaseCategorys
from field_history.models import FieldHistory

admin.site.unregister(FieldHistory)


class MarkAutoAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', )


class ModelAutoAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'mark_auto')
    raw_id_fields = ('mark_auto',)


class GenerationAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'years', 'model_auto')
    raw_id_fields = ('model_auto',)

    def save_model(self, request, obj, form, change):
        try:
            super(GenerationAdmin, self).save_model(request, obj, form, change)
        except ValidationError as ex:
            messages.add_message(request, messages.INFO, ', '.join(ex))


class BodyTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', )


class YearAdmin(admin.ModelAdmin):
    list_display = ('id', 'year')


class AutoAdmin(SafeDeleteAdmin):
    list_display = ('id', highlight_deleted, 'mark_auto', 'model_auto',
                    'year_auto', 'comment', 'created') + SafeDeleteAdmin.list_display
    raw_id_fields = ('model_auto',)
    search_fields = ('vin', )
    exclude = ('notifications',)


class AvitoAdmin(SafeDeleteAdmin):
    list_display = ('id', highlight_deleted, 'mark_auto', 'model_auto',
                    'year_auto', 'comment', 'created') + SafeDeleteAdmin.list_display
    raw_id_fields = ('model_auto',)
    search_fields = ('vin', )


class PhotoAdmin(admin.ModelAdmin):
    list_display = ('id', 'auto', 'image', 'image_small')


class FieldHistoryAdmin(admin.ModelAdmin):
    list_display = ('user', 'object', 'field_name',
                    'serialized_data', 'date_created')
    raw_id_fields = ('user',)
    search_fields = ('user', 'date_created')


class AutoDataBaseCategorysAdmin (admin.ModelAdmin):
    list_display = ('id', 'title', )


class AutoDataBaseAdmin (admin.ModelAdmin):
    list_display = ('id', 'mark_auto', 'model_auto',
                    'generation', 'category', 'title')
    raw_id_fields = ('mark_auto', 'model_auto', 'generation')
    search_fields = ('mark_auto', 'generation', 'model_auto')
    # formfield_overrides = {
    #     models.TextField: {'widget': RichTextEditorWidget},
    # }
    formfield_overrides = {
        models.TextField: {
            'widget': TinyMCE,
        },
    }


admin.site.register(MarkAuto, MarkAutoAdmin)
admin.site.register(AutoRU, AutoAdmin)
admin.site.register(ModelAuto, ModelAutoAdmin)
admin.site.register(Auto, AutoAdmin)
admin.site.register(Avito, AvitoAdmin)
admin.site.register(Generation, GenerationAdmin)
admin.site.register(BodyType, BodyTypeAdmin)
admin.site.register(Year, YearAdmin)
admin.site.register(FieldHistory, FieldHistoryAdmin)
admin.site.register(AutoPhoto, PhotoAdmin)
admin.site.register(AutoDataBase, AutoDataBaseAdmin)
admin.site.register(AutoDataBaseCategorys, AutoDataBaseCategorysAdmin)
