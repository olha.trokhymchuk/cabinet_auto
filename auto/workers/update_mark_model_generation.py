import json
from psycopg2.extras import NumericRange
from auto.models import ModelAuto, Generation, MarkAuto
from django.core.exceptions import ValidationError


def update_mark_model_generation():
    years = None
    with open('new_gen__data.json') as json_file:
        data = json.load(json_file)
        for row in data:
            if row["yearFrom"] or row["yearTo"]:
                years = NumericRange(row["yearFrom"], row["yearTo"])

                mark, created = MarkAuto.objects.get_or_create(name=row["mark"])
                model, created = ModelAuto.objects.get_or_create(mark_auto=mark, name=row["model"])
        print('mark and model updated')

        for row in data:
            if years:
                try:
                    gen = Generation.objects.get(model_auto=model, name=row["gen"], years=years)

                except (Generation.DoesNotExist, Generation.MultipleObjectsReturned, ValidationError):
                    gen = Generation(model_auto=model, name=row["gen"], years=years)
                    gen.save()
        print('Generation updated')