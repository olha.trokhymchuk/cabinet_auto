from auto.models import Auto, MarkAuto, ModelAuto, Generation
from service.models import PodborAuto

class RemoveDuble:
	def __init__(self, actions):
		for action in actions:
			getattr(self, action)()

	def remove_mark_dub(self):
		marks = MarkAuto.objects.all()
		mark_dict = {}
		for_deleted = []
		for mark in marks:
			try:
				mark_dict[mark.name]
			except KeyError:
				mark_dict[mark.name] = []
			mark_dict[mark.name].append(mark.pk)
		for mark in mark_dict:
			name = mark
			print(name)
			pk = mark_dict[mark].pop()
			for markid in  mark_dict[mark]:
				for_deleted.append(markid)
				ppk = PodborAuto.objects.filter(mark_auto_id=markid)
				ppk.update(mark_auto_id = pk)
				ppk = PodborAuto.objects.deleted_only().filter(mark_auto_id=markid)
				ppk.update(mark_auto_id = pk)
				auto = Auto.objects.filter(mark_auto_id=markid)
				auto.update(mark_auto_id = pk)
				auto = Auto.objects.deleted_only().filter(mark_auto_id=markid)
				auto.update(mark_auto_id = pk)
				modelauto = ModelAuto.objects.filter(mark_auto_id=markid)
				modelauto.update(mark_auto_id = pk)
		print(for_deleted)


	def remove_model_dub(self):
		marks = MarkAuto.objects.all()
		model_dict = {}
		for_deleted = []
		for mark in marks:
			models = ModelAuto.objects.filter(mark_auto=mark)
			for model in models:
				model_name = model.name + "_" + mark.name
				model_name = model_name.replace(" ", "_")
				try:
					model_dict[model_name]
				except KeyError:
					model_dict[model_name] = []
				model_dict[model_name].append(model.pk)
		for model in model_dict:
			model_name = model
			try:
				pk = model_dict[model_name].pop()
				print(pk)
				for modelid in  model_dict[model_name]:
					for_deleted.append(modelid)
					ppk = PodborAuto.objects.filter(model_auto_id=modelid)
					ppk.update(model_auto_id = pk)
					ppk = PodborAuto.objects.deleted_only().filter(model_auto_id=modelid)
					ppk.update(model_auto_id = pk)
					auto = Auto.objects.filter(model_auto_id=modelid)
					auto.update(model_auto_id = pk)
					auto = Auto.objects.deleted_only().filter(model_auto_id=modelid)
					auto.update(model_auto_id = pk)
					generation = Generation.objects.filter(model_auto_id=modelid)
					generation.update(model_auto_id = pk)
			except IndexError as ex:
				print(ex)
		print(for_deleted)


	def remove_gen_dub(self):
			models = ModelAuto.objects.all()
			generation_dict = {}
			for_deleted = []
			for model in models:
				generations = Generation.objects.filter(model_auto=model)
				for generation in generations:
					try:
						g_name = generation.name + "_" +  model.name
						try:
							generation_dict[g_name]
						except KeyError:
							generation_dict[g_name] = []
						generation_dict[g_name].append(generation.pk)
					except TypeError:
						pass
			for generation in generation_dict:
				try:
					pk = generation_dict[generation].pop()
					for generationid in  generation_dict[generation]:
						for_deleted.append(generationid)
						ppk = PodborAuto.objects.filter(generation_id=generationid)
						ppk.update(generation_id = pk)
						ppk = PodborAuto.objects.deleted_only().filter(generation_id=generationid)
						ppk.update(generation_id = pk)
						auto = Auto.objects.filter(generation_id=generationid)
						auto.update(generation_id = pk)
						auto = Auto.objects.deleted_only().filter(generation_id=generationid)
						auto.update(generation_id = pk)
				except IndexError as ex:
					print(ex)
			print(for_deleted)

