import json, os, sys, time
from json.decoder import JSONDecodeError

from selenium.webdriver.common.proxy import *
from selenium.common.exceptions import WebDriverException

from psycopg2.extras import NumericRange

from django.core.exceptions import ValidationError

import requests

import xlsxwriter

from auto.models import Auto, MarkAuto, ModelAuto, Generation, Year
from auto.parser import ConnectTroughSelenium


class CreateExel(ConnectTroughSelenium):
    def __init__(self, actions):
        workbook = xlsxwriter.Workbook('new_mark_model_generation.xlsx')
        self.worksheet = workbook.add_worksheet()
        self.current_dir = os.path.abspath(os.path.dirname(__file__))
        for action in actions:
            print(action)
            getattr(self, action)()

        workbook.close()

    def create_exel(self):
        marks = MarkAuto.objects.all()
        #self.worksheet.write('A' + str(i), str(mark))
        #self.worksheet.write('B' + str(i), str(model))
        #self.worksheet.write('C' + str(i), str(generation))
        #self.worksheet.write('D' + str(i), str(generation.xlsx_years))
        i = 1
        for mark in marks:
            models = mark.modelauto_set.all()
            for model in models:
                generations = model.generation_set.all()
                for generation in generations:
                    self.worksheet.write('A' + str(i), str(mark))
                    self.worksheet.write('B' + str(i), str(model))
                    self.worksheet.write('C' + str(i), str(generation))
                    self.worksheet.write('D' + str(i), str(generation.xlsx_years))
                    
                    i += 1   

    def add_to_row(self, item, i):
        self.worksheet.write('A' + str(i), str(item["mark"]))
        self.worksheet.write('B' + str(i), str(item["model"]))
        self.worksheet.write('C' + str(i), str(item["generation"]))
        self.worksheet.write('D' + str(i), str(item["yearFrom"]))
        self.worksheet.write('E' + str(i), str(item["yearTo"]))
    
    def create_new_model_mark_generation(self):
        path = "{}{}".format(self.current_dir, "/autoru/all_m_m_g_new.json")
        all_m_m_g_new = open(path, 'r').read()
        all_m_m_g_new = json.loads(all_m_m_g_new) if all_m_m_g_new else []
        years = None
        for item in  all_m_m_g_new:
            if item["yearFrom"] or item["yearTo"]:
                years = NumericRange(item["yearFrom"],item["yearTo"])
            mark, created = MarkAuto.objects.get_or_create(name=item["mark"])
            model, created = ModelAuto.objects.get_or_create(mark_auto=mark, name=item["model"])
            try:
                if years:
                    generation, created = Generation.objects.get_or_create(model_auto=model, name=item["generation"], years=years)
                else:
                    generation, created = Generation.objects.get_or_create(model_auto=model, name=item["generation"]) 
            except ValidationError as ex:
                pass
                
    def create_model_mark_popular(self):
        path = "{}{}".format(self.current_dir, "/autoru/marks_models_popular.json")
        marks_models_popular = open(path, 'r').read() 
        marks_models_popular = json.loads(marks_models_popular) if marks_models_popular else []
        
        for item in  marks_models_popular:
            for mark_item in item:
                mark = MarkAuto.objects.get(name=mark_item)
                mark.popular = True
                mark.save()
                for model_item in item[mark_item]:
                    model = ModelAuto.objects.get(mark_auto=mark, name=model_item["name"])
                    model.popular = True
                    model.save()

    def create_exel_with_new_from_json(self):
        i = 1
        path = "{}{}".format(self.current_dir, "/autoru/all_m_m_g_new.json")
        all_m_m_g_new = open(path, 'r').read()
        all_m_m_g_new = json.loads(all_m_m_g_new) if all_m_m_g_new else []
        for item in all_m_m_g_new:
            self.add_to_row(item, i)
            i += 1

    def create_exel_with_new(self):
        marks = MarkAuto.objects.all()
        # all_m_m_g_new = []
        i = 1
        path = "{}{}".format(self.current_dir, "/autoru/all_m_m_g.json")
        all_m_m_g = open(path, 'r').read()
        all_m_m_g = json.loads(all_m_m_g) if all_m_m_g else []
        path = "{}{}".format(self.current_dir, "/autoru/all_m_m_g_new.json")
        all_m_m_g_new = open(path, 'r').read()
        all_m_m_g_new = json.loads(all_m_m_g_new) if all_m_m_g_new else []
        for item in all_m_m_g:
            try:
                mark = MarkAuto.objects.get(name=item["mark"])
            except MarkAuto.DoesNotExist:
                all_m_m_g_new.append(item)
                self.add_to_row(item, i)
                i += 1
                continue
            try:
                if item["model"] == "":
                    continue
                model = ModelAuto.objects.get(mark_auto=mark, name=item["model"])
            except ModelAuto.DoesNotExist:
                all_m_m_g_new.append(item)
                self.add_to_row(item, i)
                i += 1
                continue
            try:
                if item["generation"] == "":
                    continue
                generation = Generation.objects.get(model_auto=model, name=item["generation"])
            except Generation.DoesNotExist:
                all_m_m_g_new.append(item)
                self.add_to_row(item, i)
                i += 1
                continue
            except Generation.MultipleObjectsReturned:
                pass
        all_m_m_g_new_json = open(path, 'w')
        json.dump(all_m_m_g_new, all_m_m_g_new_json)

    def create_exel_with_all(self):
        i = 1
        all_m_m_g = open("./autoru/all_m_m_g.json", 'r').read()
        all_m_m_g = json.loads(all_m_m_g) if all_m_m_g else []
        for item in all_m_m_g:
            self.worksheet.write('A' + str(i), str(item["mark"]))
            self.worksheet.write('B' + str(i), str(item["model"]))
            self.worksheet.write('C' + str(i), str(item["generation"]))
            self.worksheet.write('D' + str(i), str(item["yearFrom"]))
            self.worksheet.write('E' + str(i), str(item["yearTo"]))
            i += 1


    def get_marks_selenium(self):
        self.data = {}
        url = "https://auto.ru/-/ajax/desktop/getBreadcrumbsWithFilters/"
        self.request_through_selenium()
        response = self.driver.request('POST', url, data=self.data, cookies=self.cookies, headers=self.headers)
        res = json.loads(response.text)
        path = "{}{}".format(self.current_dir, "/autoru/auto_data_name.json")
        file_object = open(path, 'a')
        list_of_marks = []
        list_of_marks = {}

        for item in res[0]["entities"]:
            if item["popular"]:
                print(item["name"])  
            list_of_marks[item["id"]] = item["name"]
        json.dump(list_of_marks, file_object)
        time.sleep(2)
        self.driver.close()

    def get_marks_selenium_popular(self):
        self.data = {}
        url = "https://auto.ru/-/ajax/desktop/getBreadcrumbsWithFilters/"
        self.request_through_selenium()
        response = self.driver.request('POST', url, data=self.data, cookies=self.cookies, headers=self.headers)
        res = json.loads(response.text)
        path = "{}{}".format(self.current_dir, "/autoru/mark_autoru_popular.json")
        file_object = open(path, 'a')
        list_of_marks = {}

        for item in res[0]["entities"]:
            if item["popular"]:
                print(item["name"])  
                list_of_marks[item["id"]] = item["name"]
        json.dump(list_of_marks, file_object)
        time.sleep(2)
        self.driver.close()

    def get_models_selenium(self):
        url = "https://auto.ru/-/ajax/desktop/getBreadcrumbsWithFilters/"
        parse_marks = open("./autoru/parse_marks.json", 'r').read()
        parsed_marks = json.loads(parse_marks) if parse_marks else []

        marks_models = open("./autoru/marks_models.json", 'r').read()
        marks_models = json.loads(marks_models) if marks_models else []

        i = 0
        with open('./autoru/autoru_data.json') as json_file:
            data = json.load(json_file)
            self.data = {}
            self.request_through_selenium()
            for item in data:
                if item["mark"] in parsed_marks:
                    continue

                if i >= 100:
                    break
                self.data["mark_model_nameplate"] = item["mark"]
                parsed_marks.append(item["mark"])
                response = self.driver.request('POST', url, data=self.data, cookies=self.cookies, headers=self.headers)
                try:
                    res = json.loads(response.text)
                except JSONDecodeError:
                    pass
                else:
                    models = []
                    for item_mod in res[0]["entities"]:
                        model = {"id": item_mod["id"], "name": item_mod["name"]}
                        models.append(model)
                    mark = {item["mark"]: models}
                    marks_models.append(mark)
                    i += 1

            marks_models_file = open("./autoru/marks_models.json", 'w')
            json.dump(marks_models, marks_models_file)
            parse_marks = open("./autoru/parse_marks.json", 'w')
            json.dump(parsed_marks, parse_marks)
            self.driver.close()

    def get_models_selenium_popular(self):
        url = "https://auto.ru/-/ajax/desktop/getBreadcrumbsWithFilters/"
        
        parse_marks_path = "{}{}".format(self.current_dir, "/autoru/parse_marks.json")
        parse_marks = open(parse_marks_path, 'r').read()
        parsed_marks = json.loads(parse_marks) if parse_marks else []
        print(parsed_marks)

        marks_models_popular_path = "{}{}".format(self.current_dir, "/autoru/marks_models_popular.json")
        marks_models = open(marks_models_popular_path, 'r').read()
        marks_models = json.loads(marks_models) if marks_models else []
        print(marks_models)

        i = 0
        path = "{}{}".format(self.current_dir, "/autoru/mark_autoru_popular.json")
        with open(path) as json_file:
            data = json.load(json_file)
            self.data = {}
            self.request_through_selenium()
            for item in data:
                if item in parsed_marks:
                    continue

                if i >= 100:
                    break
                print(item)
                self.data["mark_model_nameplate"] = item
                parsed_marks.append(item)
                response = self.driver.request('POST', url, data=self.data, cookies=self.cookies, headers=self.headers)
                try:
                    res = json.loads(response.text)
                except JSONDecodeError:
                    pass
                else:
                    models = []
                    print(res)
                    for item_mod in res[0]["entities"]:
                        if item_mod["count"] > 0:
                            model = {"id": item_mod["id"], "name": item_mod["name"]}
                            models.append(model)
                    mark = {data[item]: models}
                    marks_models.append(mark)
                    print(marks_models)
                
                    i += 1

            marks_models_file = open(marks_models_popular_path, 'w')
            json.dump(marks_models, marks_models_file)
            parse_marks = open(parse_marks_path, 'w')
            json.dump(parsed_marks, parse_marks)
            self.driver.close()

    def get_response(self, url):
        try:
            return self.driver.get(url)
        except WebDriverException:
            self.get_response(url)

    def test(self):
        self.data = {}
        url = "http://eo.vokt.ru:9092/api/tutor/"
        url = "http://whatismyipaddress.com"
        url = "https://api.ipify.org"
        self.data["mark_model_nameplate"] = ""
        self.request_through_selenium()
        response = self.get_response(url)
        print(response)

        time.sleep(2)
        self.driver.close()

    def get_gens_selenium(self):
        url = "https://auto.ru/-/ajax/desktop/getBreadcrumbsWithFilters/"
        self.data = {}
        all_m_m_g = []
        path = "{}{}".format(self.current_dir, "/autoru/marks_models.json")
        marks_models = open(path, 'r').read()
        marks_models = json.loads(marks_models) if marks_models else []

        path = "{}{}".format(self.current_dir, "/autoru/auto_data_name.json")
        autoru_data_by_name = open(path, 'r').read()
        autoru_data_by_name = json.loads(autoru_data_by_name)

        path = "{}{}".format(self.current_dir, "/autoru/parse_gen.json")
        parse_gens = open(path, 'r').read()
        parsed_gens = json.loads(parse_gens) if parse_gens else []

        path = "{}{}".format(self.current_dir, "/autoru/parse_gen_notf.json")
        parse_gens_notf = open(path, 'r').read()
        parsed_gens_notf = json.loads(parse_gens_notf) if parse_gens_notf else []

        path = "{}{}".format(self.current_dir, "/autoru/all_m_m_g.json")
        all_m_m_g = open(path, 'r').read()
        all_m_m_g = json.loads(all_m_m_g) if all_m_m_g else []

        i = 0
        self.request_through_selenium()
        for marks in marks_models:
            for mark in marks:
                # print(mark)
                for model in marks[mark]:
                    mark_model_nameplate = "{}#{}".format(mark,model["id"])
                    if mark_model_nameplate in parsed_gens:
                        continue
                    if i >= 150:
                        break

                    self.data["mark_model_nameplate"] = mark_model_nameplate
                    mark_name = autoru_data_by_name[mark]
                    model_name = model["name"]
                    response = self.driver.request('POST', url, data=self.data, cookies=self.cookies, headers=self.headers)
                    try:
                        res = json.loads(response.text)
                    except JSONDecodeError:
                        parsed_gens.append(mark_model_nameplate)
                        parsed_gens_notf.append(mark_model_nameplate)
                        # print(mark_model_nameplate)
                        m_m_g = {"mark": mark_name, "model": model_name, "generation": "", "yearFrom": "", "yearTo": ""}
                        all_m_m_g.append(m_m_g)
                        i += 1
                    else:
                        mark_name = res[0]["mark"]["name"]
                        model_name = res[0]["model"]["name"]
                        # print("res[0]")
                        # print(res[0])
                        # print("res[0]")
                        print(res[0]["entities"])
                        for item_mod in res[0]["entities"]:
                            m_m_g = {"mark": mark_name, "model": model_name, "generation": item_mod["name"], "yearFrom": item_mod["yearFrom"], "yearTo": item_mod["yearTo"]}
                            all_m_m_g.append(m_m_g)
                        i += 1
                        parsed_gens.append(mark_model_nameplate)
                    # print("i")
                    # print(i)
                    # print("i")

        # print(all_m_m_g)
        path = "{}{}".format(self.current_dir, "/autoru/all_m_m_g.json")
        all_m_m_g_json = open(path, 'w')
        json.dump(all_m_m_g, all_m_m_g_json)

        path = "{}{}".format(self.current_dir, "/autoru/parse_gen.json")
        parse_gens = open(path, 'w')
        json.dump(parsed_gens, parse_gens)

        path = "{}{}".format(self.current_dir, "/autoru/parse_gen_notf.json")
        parse_gens_notf = open(path, 'w')
        json.dump(parsed_gens_notf, parse_gens_notf)
        self.driver.close()
