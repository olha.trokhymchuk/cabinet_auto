import csv
import json


def parse_csv_file_mark_model_gen():
    csvfile_mark = open('car_mark.csv', 'r', encoding='utf-8')
    csvfile_model = open('car_model.csv', 'r', encoding='utf-8')
    csvfile_generation = open('car_generation.csv', 'r', encoding='utf-8')

    jsonfile_mark = []
    jsonfile_model = []
    jsonfile_gen = []

    finish_list_mark = []
    finish_list_model = []
    db = []
    #

    reader_mark = csv.DictReader(csvfile_mark)
    for row in reader_mark:
        data = {
            'id_car_mark': row["'id_car_mark'"],
            'name': row["'name'"]
        }

        jsonfile_mark.append(data)

    reader_model = csv.DictReader(csvfile_model)
    for row in reader_model:
        data = {
            'id_car_model': row["'id_car_model'"],
            'id_car_mark': row["'id_car_mark'"],
            'name': row["'name'"]
        }

        jsonfile_model.append(data)

    reader_gen = csv.DictReader(csvfile_generation)
    for row in reader_gen:
        data = {
            'id_car_model': row["'id_car_model'"],
            'name': row["'name'"],
            'yearFrom': row["'year_begin'"][1:-1],
            'yearTo': row["'year_end'"][1:-1]
        }

        jsonfile_gen.append(data)

    for mark in jsonfile_mark:
        for model in jsonfile_model:
            for gen in jsonfile_gen:
                if model['id_car_mark'] == mark['id_car_mark']:
                    if model['id_car_model'] == gen['id_car_model']:
                        if gen['yearFrom'] == 'UL' or gen['yearTo'] == 'UL' or not gen['yearFrom'] or not gen['yearTo']:
                            pass
                        else:
                            try:
                                finish_list_model.append(model['name'])
                                data = {
                                    'mark': mark['name'][1:-1],
                                    'model': model['name'][1:-1],
                                    'gen': gen['name'][1:-1],
                                    'yearFrom': int(gen['yearFrom']),
                                    'yearTo': int(gen['yearTo']),

                                }
                                db.append(data)
                                a_file = open("autopodbor_auto/new_gen__data.json", "w")
                                json.dump(db, a_file)
                                a_file.close()
                            except ValueError:
                                pass

                else:
                    pass

