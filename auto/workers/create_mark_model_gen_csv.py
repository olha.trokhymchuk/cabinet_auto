import csv
import json

from auto.models import Auto, MarkAuto, ModelAuto, Generation
from service.models import PodborAuto


def create_mark_model_gen_csv():
    m_m_g_data = []
    marks = MarkAuto.objects.all()
    for mark in marks:
        models = ModelAuto.objects.filter(mark_auto=mark)
        for model in models:
            generations = Generation.objects.filter(model_auto=model)

            for generation in generations:
                auto_count = Auto.objects.filter(generation=generation).count()
                podbor_auto_count = PodborAuto.objects.filter(generation=generation).count()
                if generation.years:
                    years = str(generation.years.lower) + ' - ' + str(generation.years.upper)
                else:
                    years = None

                data_json = {
                    'mark': mark.name,
                    'model': model.name,
                    'generation': generation.name,
                    'generation_years': years,
                    'gen_model_auto_id': generation.model_auto.id,
                    'auto_count': auto_count,
                    'podbor_auto_count': podbor_auto_count,
                    'generation_id': generation.id
                }
                m_m_g_data.append(data_json)

    a_file = open("data_delete_dub_generation.json", "w")
    json.dump(m_m_g_data, a_file)
    a_file.close()

    # Opening JSON file and loading the data
    # into the variable data
    with open('data_delete_dub_generation.json') as json_file:
        data = json.load(json_file)

    # employee_data = data['emp_details']

    # now we will open a file for writing
    data_file = open('data_delete_dub_generation.csv', 'w')

    # create the csv writer object
    csv_writer = csv.writer(data_file)

    # Counter variable used for writing
    # headers to the CSV file
    count = 0

    for emp in data:
        if count == 0:
            # Writing headers of CSV file
            header = emp.keys()
            csv_writer.writerow(header)
            count += 1

        # Writing data of CSV file
        csv_writer.writerow(emp.values())

    data_file.close()
