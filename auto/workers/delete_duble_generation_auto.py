from django.db.models import Count
from auto.models import ModelAuto, Generation, Auto, AutoDataBase
from service.models import PodborAuto


def delete_dub_generation():
    Generation.objects.filter(name='').update(name='I')
    Generation.objects.filter(name__isnull=True).update(name='I')
    print('---- START DELETE DUB GENERATIONS ----')
    models = ModelAuto.objects.all()
    for model in models:

        duplicate_gen = Generation.objects.values('name', 'model_auto_id') \
            .annotate(records=Count('name'), model_auto=Count('model_auto_id')) \
            .filter(records__gt=1, model_auto__gt=1, model_auto_id=model.id, years__isnull=False) \
            .exclude(name__exact='', years__exact=None)

        for data in duplicate_gen:
            gen_name = data['name']
            gen_model_auto_id = data['model_auto_id']

            new_generation = Generation.objects.filter(name=gen_name, model_auto_id=gen_model_auto_id).\
                order_by('-id').first()
            old_generation = Generation.objects.filter(name=gen_name, model_auto_id=gen_model_auto_id).\
                order_by('id').first()

            if new_generation and old_generation \
                and new_generation.years.lower == old_generation.years.lower \
                and new_generation.model_auto_id == old_generation.model_auto_id \
                and new_generation.name == old_generation.name:

                print('new_generation : ',
                      new_generation.id, ' / ', new_generation, ' / ', new_generation.model_auto_id)
                print('old_generation : ',
                      old_generation.id, ' / ', old_generation, ' / ', old_generation.model_auto_id, '\n')

                Auto.objects.filter(generation=old_generation).all().update(generation=new_generation)
                PodborAuto.objects.filter(generation=old_generation).all().update(generation=new_generation)
                AutoDataBase.objects.filter(generation=old_generation).all().update(generation=new_generation)

                if not Auto.objects.filter(generation=old_generation) \
                        and not PodborAuto.objects.filter(generation=old_generation) \
                        and not AutoDataBase.objects.filter(generation=old_generation):
                    print('delete old_generation : ', old_generation.id, ' / ', old_generation, ' / ', old_generation.model_auto_id, '\n\n')
                    old_generation.delete()
                else:
                    print('old_generation can not delete : ', old_generation)

    print('---- FINISH DELETE DUB GENERATIONS ---- \n')

def delete_dub_gen_and_check_count():
    print('---- START DELETE DUB GENERATIONS ----')
    models = ModelAuto.objects.all()
    for model in models:

        generations = Generation.objects.filter(model_auto=model)
        for generation in generations:
            if generation.years:
                get_generations = Generation.objects.filter(name=generation.name,
                                                            model_auto_id=generation.model_auto_id,
                                                            years__startswith=generation.years.lower).all()

                if get_generations.count() == 2:
                    old_generation = get_generations.order_by('id').first()
                    new_generation = get_generations.order_by('-id').first()
                    print('new_generation : ', new_generation.id, ' / ', new_generation, ' / ',
                          new_generation.model_auto_id)
                    print('old_generation : ', old_generation.id, ' / ', old_generation, ' / ',
                          old_generation.model_auto_id, '\n')

                    if new_generation and old_generation and new_generation.id != old_generation.id and \
                            new_generation.name == old_generation.name and \
                            new_generation.model_auto_id == old_generation.model_auto_id:

                        Auto.objects.filter(generation=old_generation).update(generation=new_generation)
                        PodborAuto.objects.filter(generation=old_generation).update(generation=new_generation)
                        AutoDataBase.objects.filter(generation=old_generation).all().update(generation=new_generation)

                        if not Auto.objects.filter(generation=old_generation) and not PodborAuto.objects.filter(
                                generation=old_generation) and not AutoDataBase.objects.filter(generation=old_generation):
                            print('delete old_generation : ', old_generation.id, ' / ', old_generation, ' / ',
                                  old_generation.model_auto_id, '\n\n')
                            old_generation.delete()
                        else:
                            print('old_generation can not delete : ', old_generation)
                if get_generations.count() == 3:
                    print('get_generations count == 3 : ', get_generations)
    print('---- FINISH DELETE DUB GENERATIONS ---- \n')


