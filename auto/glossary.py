import datetime

import order.order_constants as order_const

now = datetime.datetime.now()

RANGE_YEAR = range(1892,int(now.year)+1)


ENGINE_CAPACITY = (
    ('0.0', 'Не важно'),
    ('0.2', '0.2 л'),
    ('0.3', '0.3 л'),
    ('0.4', '0.4 л'),
    ('0.5', '0.5 л'),
    ('0.6', '0.6 л'),
    ('0.7', '0.7 л'),
    ('0.8', '0.8 л'),
    ('0.9', '0.9 л'),
    ('1.0', '1.0 л'),
    ('1.1', '1.1 л'),
    ('1.2', '1.2 л'),
    ('1.3', '1.3 л'),
    ('1.4', '1.4 л'),
    ('1.5', '1.5 л'),
    ('1.6', '1.6 л'),
    ('1.7', '1.7 л'),
    ('1.8', '1.8 л'),
    ('1.9', '1.9 л'),
    ('2.0', '2.0 л'),
    ('2.1', '2.1 л'),
    ('2.2', '2.2 л'),
    ('2.3', '2.3 л'),
    ('2.4', '2.4 л'),
    ('2.5', '2.5 л'),
    ('2.6', '2.6 л'),
    ('2.7', '2.7 л'),
    ('2.8', '2.8 л'),
    ('2.9', '2.9 л'),
    ('3.0', '3.0 л'),
    ('3.1', '3.1 л'),
    ('3.2', '3.2 л'),
    ('3.3', '3.3 л'),
    ('3.4', '3.4 л'),
    ('3.5', '3.5 л'),
    ('3.6', '3.6 л'),
    ('3.7', '3.7 л'),
    ('3.8', '3.8 л'),
    ('3.9', '3.9 л'),
    ('4.0', '4.0 л'),
    ('4.1', '4.1 л'),
    ('4.2', '4.2 л'),
    ('4.3', '4.3 л'),
    ('4.4', '4.4 л'),
    ('4.5', '4.5 л'),
    ('4.6', '4.6 л'),
    ('4.7', '4.7 л'),
    ('4.8', '4.8 л'),
    ('4.9', '4.9 л'),
    ('5.0', '5.0 л'),
    ('5.1', '5.1 л'),
    ('5.2', '5.2 л'),
    ('5.3', '5.3 л'),
    ('5.4', '5.4 л'),
    ('5.5', '5.5 л'),
    ('5.6', '5.6 л'),
    ('5.7', '5.7 л'),
    ('5.8', '5.8 л'),
    ('5.9', '5.9 л'),
    ('6.0', '6.0 л'),
    ('6.1', '6.1 л'),
    ('6.2', '6.2 л'),
    ('6.3', '6.3 л'),
    ('6.4', '6.4 л'),
    ('6.5', '6.5 л'),
    ('6.6', '6.6 л'),
    ('6.7', '6.7 л'),
    ('6.8', '6.8 л'),
    ('6.9', '6.9 л'),
    ('7.0', '7.0 л'),
    ('7.1', '7.1 л'),
    ('7.2', '7.2 л'),
    ('7.3', '7.3 л'),
    ('7.4', '7.4 л'),
    ('7.5', '7.5 л'),
    ('7.6', '7.6 л'),
    ('7.7', '7.7 л'),
    ('7.8', '7.8 л'),
    ('7.9', '7.9 л'),
    ('8.0', '8.0 л'),
    ('8.1', '8.1 л'),
    ('8.2', '8.2 л'),
    ('8.3', '8.3 л'),
    ('8.4', '8.4 л'),
    ('8.5', '8.5 л'),
    ('8.6', '8.6 л'),
    ('8.7', '8.7 л'),
    ('8.8', '8.8 л'),
    ('8.9', '8.9 л'),
    ('9.0', '9.0 л'),
    ('9.1', '9.1 л'),
    ('9.2', '9.2 л'),
    ('9.3', '9.3 л'),
    ('9.4', '9.4 л'),
    ('9.5', '9.5 л'),
    ('9.6', '9.6 л'),
    ('9.7', '9.7 л'),
    ('9.8', '9.8 л'),
    ('9.9', '9.9 л'),
    ('10.0', '10.0 л'),

)

WHEEL_CAR_TYPE = (
    (0, 'Не важно'),
    (1, 'Левый'),
    (2, 'Правый'),
)
ANY = 0
SEDAN = 1
COUPE = 2
CABRIOLET = 3
WAGON5 = 4
SEDANHARDTOP = 5
COUPEHARDTOP = 6
MINIVAN = 7
COMPACTVAN = 8 
SUV5 = 9 
HATCHBACK5 = 10
SUV3 = 11
MICROVAN = 12
PICKUPDOUBL = 13
PICKUPMONO = 14 
PICKUPMONOANDHALF =15  
LIFTBACK = 16 
SEDANTWODOOR = 17
WAGON3 = 18 
SUVOPEN = 19
HATCHBACK3 = 20
RODSTER = 21
TARGA = 22
HATCHBACK4 = 23
SPIDSTER = 24 
FAETON = 25 
VAN = 26
LIMOUSINE = 27
FASTBACK = 28


BODY_TYPE = (
    (ANY, 'Не важно'),
    (SEDAN, 'Седан'),(SEDANHARDTOP, 'Седан-хардтоп'), (HATCHBACK3, 'Хэтчбек 3дв'), (HATCHBACK5, 'Хэтчбек 5дв'),
    (LIFTBACK, 'Лифтбек'), (SUV3, 'Внедорожник 3дв'), (SUV5, 'Внедорожник 5дв'),
    (WAGON5, 'Универсал 5 дв.'), (COUPE, 'Купе'),(COUPEHARDTOP, 'Купе-хардтоп'), (MINIVAN, 'Минивэн'),
    (PICKUPDOUBL, 'Пикап'), (LIMOUSINE, 'Лимузин'), (VAN, 'Фургон'),(PICKUPDOUBL, 'Пикап Двойная кабина'),
    (PICKUPMONO, 'Пикап Одинарная кабина'),(PICKUPMONOANDHALF, 'Пикап Полуторная кабина'),
    (CABRIOLET, 'Кабриолет'), (COMPACTVAN, 'Компактвэн'), (MICROVAN, 'Микровэн'),(SEDANTWODOOR, 'Седан 2 дв.'),
    (WAGON3, 'Универсал 3 дв.'), (SUVOPEN, 'Внедорожник открытый'), (RODSTER, 'Родстер'), (TARGA, 'Тарга'),
    (HATCHBACK4, 'Хэтчбек 4дв'), (SPIDSTER, 'Спидстер'), (FAETON, 'Фаэтон'), (FASTBACK, 'Фастбек')
)
 


REFUSALS_TYPE = (
    (0, 'NONE'),
)

TRANSMISSION_TYPE_ANY = 0
TRANSMISSION_TYPE_AUTO = 1
TRANSMISSION_TYPE_MECH = 2
TRANSMISSION_TYPE_ROBOT = 3
TRANSMISSION_TYPE_VAR = 4


TRANSMISSION_TYPE = (
    (TRANSMISSION_TYPE_ANY, 'любая'),
    (TRANSMISSION_TYPE_AUTO, 'автоматическая'), (TRANSMISSION_TYPE_MECH, 'механическая'),
    (TRANSMISSION_TYPE_ROBOT, 'робот'), (TRANSMISSION_TYPE_VAR, 'вариатор'),
)
TRANSMISSION_TYPE_LK = (
    (0, 'любая'),
    (1, 'АКПП'), (2, 'МКПП'),
    (3, 'РКПП'), (4, 'вариатор'),
)

ENGINE_ANY = 0
ENGINE_PETROL = 1
ENGINE_DIESEL = 2
ENGINE_GEBRID = 3
ENGINE_ELECTRO = 4

ENGINE_TYPE = (
    (ENGINE_ANY, 'любой'),
    (ENGINE_PETROL, 'бензин'), (ENGINE_DIESEL, 'дизель'),
    (ENGINE_GEBRID, 'гибрид'), (ENGINE_ELECTRO, 'электро'),
)

ENGINE_AIR_TYPE = (
    (0, 'Не важно'),
    (1, 'Атмосферный'), (2, 'Турбированный'),
    (2, 'Газобалонное оборудование'),
)

DRIVE_TYPE_ANY = 0
DRIVE_TYPE_FRONT = 1
DRIVE_TYPE_BACK = 2
DRIVE_TYPE_FULL = 3
DRIVE_TYPE = (
    (DRIVE_TYPE_ANY, 'любой'),
    (DRIVE_TYPE_FRONT, 'передний'), (DRIVE_TYPE_BACK, 'задний'), (DRIVE_TYPE_FULL, 'полный'),
)

SALON_AUTO_ANY = 0
SALON_AUTO = (
    (SALON_AUTO_ANY, 'любой'),
    (1, 'ткань'), (2, 'велюр'),
    (3, 'кожа'),(4, 'комбинированный'),
)

NUMBER_OF_HOST_TYPE = (
    (0, 'любое'),
    (1, '1'), (2, '2'), (3, '3'), (4, 'более трех'),
)

SALON_TYPE = (
    (0, 'Не важно'),
    (1, 'Ткань'), (2, 'Велюр'), (3, 'Кожа'), (4, 'Комбинированный'),
)

COLOR_SALON_TYPE_ANY  = 0
COLOR_SALON_TYPE = (
    (COLOR_SALON_TYPE_ANY, 'любой'),
    (1, 'светлый'), (2, 'тёмный'),
)

COLOR_OF_AUTO_ANY = 0
COLOR_OF_AUTO = (
    (COLOR_OF_AUTO_ANY, 'любой'),
    (1, 'чёрный'), (2, 'серебристый'), (3, 'белый'),
    (4, 'серый'), (5, 'синий'), (6, 'красный'),
    (7, 'зелёный'), (8, 'коричневый'), (9, 'бежевый'),
    (10, 'голубой'), (11, 'золотистый'), (12, 'пурпурный'),
    (13, 'фиолетовый'), (14, 'жёлтый'), (15, 'оранжевый'),
    (16, 'розовый'),(23, 'бордовый')
)

YEAR_OF_ISSUE = (
    (0, 'Не важно'),
    (1, '1995'), (2, '1996'), (3, '1997'), (4, '1998'),
    (5, '1999'), (6, '2000'), (7, '2001'), (8, '2002'),
    (9, '2003'), (10, '2004'), (11, '2005'), (12, '2006'),
    (13, '2007'), (14, '2008'), (15, '2009'), (16, '2010'),
    (17, '2011'), (18, '2012'), (19, '2013'), (20, '2014'),
    (21, '2015'), (22, '2016'), (23, '2017'), (24, '2018'),
    (25, '2019'), (26, '2020'), (27, '2021'), (28, '2022'),
)

AUTHOR_TYPE_OFF = 0
AUTHOR_TYPE_PRIVE = 1
AUTHOR_TYPE_FREE = 2
AUTHOR_TYPE_TRADER = 3
AUTHOR_TYPE_ = 4
AUTHOR_TYPE = (
    (AUTHOR_TYPE_OFF, 'официальный дилер'),
    (AUTHOR_TYPE_PRIVE, 'частное лицо'),
    (AUTHOR_TYPE_FREE, 'неофициальный дилер'),
    (AUTHOR_TYPE_TRADER, 'перекуп')
)

choise_data = {
    "transmission_type": {
        'любая': TRANSMISSION_TYPE_ANY,
        'автоматическая': TRANSMISSION_TYPE_AUTO, 
        'механическая':TRANSMISSION_TYPE_MECH,
        'робот': TRANSMISSION_TYPE_ROBOT, 
        'вариатор': TRANSMISSION_TYPE_VAR
    },
    "engine_type": {
        'любой':ENGINE_ANY, 'бензин': ENGINE_PETROL,
        'дизель': ENGINE_DIESEL, 'гибрид': ENGINE_GEBRID, 
        'электро': ENGINE_ELECTRO
    },
    "author_type": {
        'официальный дилер': AUTHOR_TYPE_OFF,
        'частное лицо': AUTHOR_TYPE_PRIVE,
        'неофициальный дилер': AUTHOR_TYPE_FREE,
        'перекуп': AUTHOR_TYPE_TRADER,
        '-': AUTHOR_TYPE_,
    },
    "drive_type": {
        'любой':DRIVE_TYPE_ANY,'передний': DRIVE_TYPE_FRONT, 
        'задний': DRIVE_TYPE_BACK, 'полный': DRIVE_TYPE_FULL,
    }

}
