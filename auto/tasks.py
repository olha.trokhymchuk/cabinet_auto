from autopodbor.celery import app
from .utils import parse_cars_from_auto_ru


@app.task
def update_auto_info():
    parse_cars_from_auto_ru()

