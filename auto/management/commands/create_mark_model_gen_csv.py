from django.core.management.base import BaseCommand

from auto.workers.create_mark_model_gen_csv import create_mark_model_gen_csv


class Command(BaseCommand):
    help = 'create mark model generation csv file'

    def handle(self, *args, **options):
        create_mark_model_gen_csv()
