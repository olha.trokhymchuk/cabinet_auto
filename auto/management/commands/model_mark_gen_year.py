from django.core.management.base import BaseCommand

from auto.workers.model_mark_gen_year import CreateExel

class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def add_arguments(self, parser):
		"""
			create_exel - создать exel марка модель поколение из базы
			create_exel_with_new - создать exel с автору которых нету в базе
			create_new_model_mark_generation - записать новые марки модели поколения
			create_exel_with_all - создать exel c автору

			get_marks_selenium - взять все марки с авто ру
			get_marks_selenium_popular - взять популярные марки с авто ру
        	get_models_selenium - взять все модели с автору в соответсвии с маркой
        	get_models_selenium_popular - взять популярные модели с автору
        	get_gens_selenium  - взять все поколения с автору в соответсвии с моделью

		"""
		parser.add_argument("--actions", nargs='+',  type=str, default="")

	def handle(self, *args, **options):
		actions = options['actions']
		print(actions)
		CreateExel(actions)

