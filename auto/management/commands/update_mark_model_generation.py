from django.core.management.base import BaseCommand

from auto.workers.update_mark_model_generation import update_mark_model_generation

class Command(BaseCommand):
    help = 'update generation to model'

    def handle(self, *args, **options):
        update_mark_model_generation()
