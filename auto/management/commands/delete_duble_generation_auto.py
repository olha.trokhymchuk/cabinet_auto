from django.core.management.base import BaseCommand

from auto.workers.delete_duble_generation_auto import delete_dub_generation, \
    delete_dub_gen_and_check_count


class Command(BaseCommand):
    help = 'delete dub generations'

    def handle(self, *args, **options):
        delete_dub_generation()
        delete_dub_gen_and_check_count()