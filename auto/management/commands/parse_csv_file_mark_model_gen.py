from django.core.management.base import BaseCommand

from auto.workers.parse_csv_file_mark_model_gen import parse_csv_file_mark_model_gen

class Command(BaseCommand):
    help = 'Send reports to auto library'

    def handle(self, *args, **options):
        parse_csv_file_mark_model_gen()