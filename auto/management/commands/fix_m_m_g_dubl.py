from django.core.management.base import BaseCommand

from auto.workers.model_mark_gen_duble import RemoveDuble

class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def add_arguments(self, parser):
		"""
			create_exel - создать exel марка модель поколение из базы
		"""
		parser.add_argument("--actions", nargs='+',  type=str, default="")

	def handle(self, *args, **options):
		actions = options['actions']
		print(actions)
		RemoveDuble(actions)