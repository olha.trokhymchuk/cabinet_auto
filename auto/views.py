import json
import logging
from urllib.parse import urlencode
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.core.files.base import ContentFile
from django.db.models import Q, Min, Max
from django.forms import model_to_dict
from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.http import HttpResponse, Http404, JsonResponse, HttpResponseRedirect
from django.views.generic import UpdateView, CreateView, DetailView
from django.views.generic.list import ListView
from safedelete import SOFT_DELETE_CASCADE
from django.views.decorators.csrf import csrf_exempt

from django.conf import settings

from auto.filters import AutoFilter, AutoDatabaseFilter
from auto.glossary import ENGINE_CAPACITY
from auto.serializers import AutoSerializer
from core.utils import ImprovedListView
from custom_admin.utils import FilteredFieldsMixin, FilteredFieldContextMixin, get_allowed_fields_dict
from report.models import ReportPodbor
from .models import MarkAuto, ModelAuto, Auto, Generation, AutoPhoto, BodyType, AutoDataBase, AutoDataBaseCategorys
from .forms import AutoForm, AutoArticleForm, RecommendedAutoForm

from django_filters.views import FilterView

logger = logging.getLogger(__name__)


@login_required
def get_marks(request):
    qs = MarkAuto.objects.all()
    marks = [{'id': obj.id, 'name': obj.__str__()} for obj in qs]
    return JsonResponse(marks, safe=False)


@login_required
def get_models(request, mark_id):
    qs = ModelAuto.objects.filter(mark_auto__id=mark_id)
    popular = qs.filter(popular=True)
    marks = {}
    popular = [{'id': obj.id, 'name': obj.__str__()} for obj in popular]
    all_models = [{'id': obj.id, 'name': obj.__str__()} for obj in qs]
    marks["popular"] = popular
    marks["all_models"] = all_models
    return JsonResponse(marks, safe=False)


@login_required
def get_generations(request, model_id):
    qs = Generation.objects.filter(model_auto__id=model_id)
    marks = [{'id': obj.id, 'name': obj.__str__()} for obj in qs]
    return JsonResponse(marks, safe=False)


# Список автомобилей в базе
@login_required
def auto_list(request):
    groups = [g.name for g in request.user.groups.all()]
    perms = get_allowed_fields_dict(Auto, groups, 'read')

    return render(request, "auto/auto_list.html", {
        'auto_marks': MarkAuto.objects.all(),
        'allowed_fields': perms,
        'html_title': 'Проверенные авто в базе',
        'page_title_span': 'Проверенные авто',
        'page_title': 'список автомобилей'
    })


class AutoDatabaseList(LoginRequiredMixin, FilterView):
    raise_exception = True
    model = AutoDataBase
    paginate_by = settings.PAGINATE_BY
    template_name = 'auto/autoDatabaseList.html'
    filterset_class = AutoDatabaseFilter

    def url_with_querystring(self, path, **kwargs):
        return path + '?' + urlencode(kwargs)

    def get_context_data(self, **kwargs):
        context = super(AutoDatabaseList, self).get_context_data(**kwargs)
        context["categories"] = AutoDataBaseCategorys.objects.all()
        context["account"] = self.request.GET.get('account', None)
        mark_auto = self.request.GET.get('mark_auto', '')
        model_auto = self.request.GET.get('model_auto', '')
        generation = self.request.GET.get('generation', '')
        category = self.request.GET.get('category', '')
        title__icontains = self.request.GET.get('title__icontains', '')
        path = '/auto/auto-database/'

        filter_array = []
        if mark_auto == '':
            mark_auto_objects = MarkAuto.objects.all()
            filter_array.extend(
                {
                    "popular": item.popular,
                    "title": item.name,
                    "url": self.url_with_querystring(path=path, category=category, mark_auto=item.id, model_auto=model_auto, generation=generation),
                    "active": 'active' if mark_auto == str(item.id) else ''
                } for item in mark_auto_objects
            )
        elif mark_auto != '' and model_auto == '' and generation == '':
            model_auto_objects = ModelAuto.objects.filter(
                mark_auto=int(mark_auto))
            filter_array.extend(
                {
                    "title": item.name,
                    "url": self.url_with_querystring(path=path, category=category, mark_auto=mark_auto, model_auto=item.id, generation=generation),
                    "active": 'active' if model_auto == str(item.id) else ''
                } for item in model_auto_objects
            )
        elif model_auto != '' and generation == '':
            generation_objects = Generation.objects.filter(
                model_auto=int(model_auto))
            filter_array.extend(
                {
                    "title": item.name,
                    "url": self.url_with_querystring(path=path, category=category, mark_auto=mark_auto, model_auto=model_auto, generation=item.id),
                    "active": 'active' if generation == str(item.id) else ''
                } for item in generation_objects
            )
        elif generation != '':
            mark_auto_objects = MarkAuto.objects.all()
            filter_array.extend(
                {
                    "popular": item.popular,
                    "title": item.name,
                    "url": self.url_with_querystring(path=path, category=category, mark_auto=item.id, model_auto='', generation=''),
                    "active": 'active' if mark_auto == str(item.id) else ''
                } for item in mark_auto_objects
            )

        category_objects = AutoDataBaseCategorys.objects.all()

        categories_array = []

        categories_array.append(
            {
                "title": "Все",
                "url": self.url_with_querystring(path=path, category='', mark_auto=mark_auto, model_auto=model_auto, generation=generation),
                "active": 'active' if category == '' else ''
            }
        )

        categories_array.extend(
            {
                "title": item.title,
                "url": self.url_with_querystring(path=path, mark_auto=mark_auto, model_auto=model_auto, generation=generation, category=item.id),
                "active": 'active' if category == str(item.id) else ''
            } for item in category_objects)

        context['categories'] = categories_array
        context['filter_array'] = filter_array
        context['search_input_array'] = {
            'mark_auto': mark_auto,
            'model_auto': model_auto,
            'generation': generation,
            'category': category,
            'title__icontains': title__icontains
        }
        context['query_url'] = self.url_with_querystring(
            path=path, mark_auto=mark_auto, model_auto=model_auto, generation=generation, category=category)+"&"
        context['search_name'] = {
            'mark_auto': get_object_or_404(MarkAuto, pk=mark_auto) if mark_auto else None,
            'model_auto': get_object_or_404(ModelAuto, pk=model_auto) if model_auto else None,
            'generation': get_object_or_404(Generation, pk=generation) if generation else None,
        }
        return context

    def get_queryset(self):
        return super(AutoDatabaseList, self).get_queryset()

# Деталка базы знаний


class AutoDatabaseView(DetailView, LoginRequiredMixin):
    raise_exception = True
    model = AutoDataBase
    template_name = 'auto/autoDatabaseItem.html'
    context_object_name = 'database'

    def url_with_querystring(self, path, **kwargs):
        return path + '?' + urlencode(kwargs)

    def get_context_data(self, **kwargs):
        context = super(AutoDatabaseView, self).get_context_data(**kwargs)
        path = '/auto/auto-database/'
        # Формирование списка категорий для вывода
        category_objects = AutoDataBaseCategorys.objects.all()

        categories_array = []

        categories_array.append(
            {
                "title": "Все",
                "url": self.url_with_querystring(path=path, category='')
            }
        )

        categories_array.extend(
            {
                "title": item.title,
                "url": self.url_with_querystring(path=path, category=item.id)
            } for item in category_objects)
        # END Формирование списка категорий для вывода

        mark_auto_objects = MarkAuto.objects.all()

        mark_auto = []

        mark_auto.extend(
            {
                "popular": item.popular,
                "title": item.name,
                "url": self.url_with_querystring(path=path, mark_auto=item.id),
            } for item in mark_auto_objects
        )

        context.update({
            'mark_auto': mark_auto,
            'categories': categories_array,
        })
        return context

    def get(self, request, **kwargs):
        return super(AutoDatabaseView, self).get(request, **kwargs)


class AutoListView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView):
    permission_required = 'auto.view_auto'
    raise_exception = True
    model = Auto
    paginate_by = settings.PAGINATE_BY
    template_name = 'auto/auto_list.html'
    filterset_class = AutoFilter
    search_fields_all_match = ['vin']
    search_fields = ['vin', 'source']
    serializer = AutoSerializer

    def get_context_data(self, **kwargs):
        context = super(AutoListView, self).get_context_data()
        context.update({'html_title': 'Проверенные авто'})
        return context

    def get_queryset(self):
        queryset = super(AutoListView, self).get_queryset().filter(
            recommended=False)
        querystring = self.request.GET.get('querystring')
        if self.request.user.is_client and not querystring:
            return queryset.filter(reportpodbor__order__client=self.request.user).distinct()
        if self.request.user.is_client and not querystring:
            return queryset.filter(vin__iexact=querystring)
        return queryset


class RecommendedAutoListView(AutoListView):
    search_fields_all_match = ['location']
    search_fields = ['location']
    permission_required = 'auto.access_to_recommended_car'

    def get_context_data(self, **kwargs):
        context = super(RecommendedAutoListView, self).get_context_data()
        locations = set(Auto.objects.filter(recommended=True).values_list('location', flat=True))
        if None in locations:
            locations.remove(None)
        context.update({'html_title': 'Рекомендованные авто'})
        context.update({'recommended_car': True,
                        'locations': locations,
                        })
        return context

    def get_queryset(self):
        queryset = Auto.objects.filter(
            recommended=True).order_by("sales", "-created")
        querystring = self.request.GET.get('querystring')
        if querystring:
            queryset = queryset.filter(location__icontains=querystring)
        return queryset


@login_required
@permission_required('auto.view_auto', raise_exception=True)
def get_car_filter(request):
    if request.is_ajax():
        data = {}
        view = AutoListView()
        view.request = request
        cars = view.get_queryset().order_by()

        # получаем уникальные объекты либо автомобили с уникальными полями
        marks = MarkAuto.objects.filter(auto__in=cars).distinct()
        models = ModelAuto.objects.filter(auto__in=cars).distinct()
        generations = Generation.objects.filter(auto__in=cars).distinct()
        year_min = cars.aggregate(Min('year_auto__year')).get(
            'year_auto__year__min')
        year_max = cars.aggregate(Max('year_auto__year')).get(
            'year_auto__year__max')
        body_types = BodyType.objects.filter(auto__in=cars).distinct()
        colors = cars.distinct('color_auto').exclude(
            color_auto=None).order_by('color_auto').values_list('color_auto')
        engine_types = cars.distinct('engine_type').exclude(engine_type=None)
        capacity_min = cars.aggregate(
            Min('engine_capacity')).get('engine_capacity__min') or 0.1
        capacity_max = cars.aggregate(
            Max('engine_capacity')).get('engine_capacity__max')
        drive_types = cars.exclude(drive_type=None).distinct('drive_type')
        transmission_types = cars.distinct(
            'transmission_type').exclude(drive_type=None)
        salons_auto = cars.distinct('salon_auto').exclude(
            salon_auto=None).values_list('salon_auto')
        colors_salon = cars.distinct('color_salon').exclude(
            color_salon=None).values_list('color_salon')
        owners_max = cars.aggregate(Max('owners')).get('owners__max')

        capacities = [float(i[0]) for i in ENGINE_CAPACITY if capacity_min and capacity_max and float(
            i[0]) >= float(capacity_min) and float(i[0]) <= float(capacity_max)]
        horsepowers = [75, 100, 150, 200, 250]
        mileage = [25000, 50000, 75000, 100000, 150000, 200000]
        if year_min is not None and year_max is not None:
            years = list(range(year_min, year_max + 1))
        else:
            years = ''

        if owners_max is not None:
            owners = [o for o in range(1, owners_max + 1)]
        else:
            owners = ''

        data.update({
            'marks': [{'id': mark.id, 'name': mark.__str__()} for mark in marks],
            'models': [{'id': model.id, 'name': model.__str__(), 'mark': model.mark_auto.id} for model in models],
            'generations': [{'id': generation.id, 'name': generation.__str__(), 'model': generation.model_auto.id} for generation in generations],
            'years': years,
            'body_types': [{'id': b.id, 'name': b.name} for b in body_types],
            'transmission_types': [{'id': t.transmission_type, 'name': t.get_transmission_type_display()} for t in transmission_types],
            'colors': [c[0] for c in colors],
            'engine_types': [{'id': e.engine_type, 'name': e.get_engine_type_display()} for e in engine_types],
            'capacities': capacities,
            'horsepowers': horsepowers,
            'mileage': mileage,
            'drive_types': [{'id': d.drive_type, 'name': d.get_drive_type_display()} for d in drive_types],
            'salons_auto': [s[0] for s in salons_auto],
            'colors_salon': [s[0] for s in colors_salon],
            'owners': owners,
        })

        return JsonResponse(data)

    raise Http404


@login_required
# @permission_required('auto.change_auto', raise_exception=True)
def add_photo_auto(request, auto_id):
    if request.method == 'POST':
        files = request.FILES
        auto = get_object_or_404(Auto, id=auto_id)
        photos = files.getlist('photo')
        for f in photos:
            data = f.read()
            photo = AutoPhoto(auto=auto)
            prev_photo = AutoPhoto.get_prev_photo()
            if prev_photo:
                photo.sort_key = prev_photo.sort_key + 1

            photo.image.save('', ContentFile(data))
            photo.save()
            content = {
                'id': photo.id,
                'image': photo.image.url,
                'image_small': photo.image_small.url, 'sort_key': photo.sort_key
            }
            return JsonResponse(data=content, safe=False)

        return HttpResponse(status=500)

    return HttpResponse(status=404)


@login_required
@permission_required('auto.change_auto', raise_exception=True)
def update_photo_auto(request, photo_id, sort_key):
    if request.method == 'POST':
        files = request.FILES
        photo = get_object_or_404(AutoPhoto, pk=photo_id)
        photo.sort_key = sort_key
        photos = files.getlist('photo')
        for f in photos:
            data = f.read()
            photo.image.save('', ContentFile(data))
            photo.save()
            content = '"id": {}, "image": "{}", "image_small": "{}", "sort_key": {}'.format(photo.pk,
                                                                                            photo.image.url,
                                                                                            photo.image_small.url,
                                                                                            photo.sort_key)
            content = '{' + content + '}'
            return HttpResponse(status=200, content=content)

        return HttpResponse(status=500)

    return HttpResponse(status=404)


@login_required
@permission_required('auto.change_auto', raise_exception=True)
def update_photo_auto_position(request, photo_id, sort_key):
    if request.method == 'POST':
        photo = get_object_or_404(AutoPhoto, pk=photo_id)
        photo.sort_key = sort_key
        photo.save()
        content = '"id": {}, "image": "{}", "image_small": "{}", sort_key: {}'.format(photo.pk,
                                                                                      photo.image.url,
                                                                                      photo.image_small.url,
                                                                                      photo.sort_key)
        content = '{' + content + '}'
        return HttpResponse(status=200, content=content)

    return HttpResponse(status=404)


@login_required
@permission_required('auto.change_auto', raise_exception=True)
def delete_photo_auto(request, photo_id):
    if request.method == 'POST':
        photo = get_object_or_404(AutoPhoto, pk=photo_id)
        photo.delete()
        return HttpResponse(status=200)

    return HttpResponse(status=404)


# Добавить автомобиль в базу
class AutoCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    permission_required = 'auto.add_auto'
    raise_exception = True
    model = Auto
    form_class = AutoForm
    template_name = 'auto/auto_edit.html'
    action = 'create'

    def get_success_url(self):
        return reverse("auto:auto_list")

    def get_form(self, form_class=None):
        form = super(AutoCreateView, self).get_form(form_class=None)
        if self.request.method == 'POST':
            form.fields['generation'].queryset = Generation.objects.all()
            form.fields['model_auto'].queryset = ModelAuto.objects.all()
        return form

    def get_context_data(self, **kwargs):
        context = super(AutoCreateView, self).get_context_data(**kwargs)
        form = context['form']
        mark = form['mark_auto'].value()
        model = form['model_auto'].value()
        form.fields['model_auto'].queryset = ModelAuto.objects.filter(
            mark_auto_id=mark)
        form.fields['generation'].queryset = Generation.objects.filter(
            model_auto_id=model)
        context.update({'html_title': 'Добавление автомобиля'})
        return context

    def form_valid(self, form):
        self.object = form.save()
        photos = self.request.FILES.getlist('photo')
        for f in photos:
            data = f.read()
            photo = AutoPhoto(auto=self.object)
            photo.image.save('', ContentFile(data))
            photo.save()

        if self.request.POST:
            return JsonResponse({'auto_id': self.object.id, 'redirect': self.get_success_url()})

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        return JsonResponse(status=200, data={'errors': form.errors})


# add recommended auto
class RecommendedCreateView(AutoCreateView):
    form_class = RecommendedAutoForm
    permission_required = 'auto.access_to_recommended_car'

    def get_success_url(self):
        return reverse("auto:recommended_auto")

    def get_context_data(self, **kwargs):
        context = super(RecommendedCreateView, self).get_context_data(**kwargs)
        form = context['form']
        mark = form['mark_auto'].value()
        model = form['model_auto'].value()
        form.fields['model_auto'].queryset = ModelAuto.objects.filter(
            mark_auto_id=mark)
        form.fields['generation'].queryset = Generation.objects.filter(
            model_auto_id=model)
        context.update({'html_title': 'Добавить рекомендованное авто',
                        'recommended_page': True,
                        })
        return context

    def form_valid(self, form):
        obj = form.save(commit=False)
        obj.recommended = True
        self.object = form.save()
        photos = self.request.FILES.getlist('photo')
        for f in photos:
            data = f.read()
            photo = AutoPhoto(auto=self.object)
            photo.image.save('', ContentFile(data))
            photo.save()

        if self.request.POST:
            return JsonResponse({'auto_id': self.object.id, 'redirect': self.get_success_url()})
        return HttpResponseRedirect(self.get_success_url())


# Добавить статью в базу знаний
class AutoArticleCreateView(LoginRequiredMixin, CreateView):
    model = AutoDataBase
    form_class = AutoArticleForm
    template_name = 'auto/auto_article.html'
    action = 'create'

    def get_success_url(self):
        return reverse("auto:auto_database_list")

    def get_context_data(self, **kwargs):
        context = super(AutoArticleCreateView, self).get_context_data(**kwargs)
        context.update({'html_title': 'Добавление статьи'})
        return context


# Редактировать статью из перечня статтей базы знаний
class AutoArticleUpdateView(LoginRequiredMixin, UpdateView):
    model = AutoDataBase
    form_class = AutoArticleForm
    template_name = 'auto/auto_article.html'
    action = 'update'

    def get_context_data(self, **kwargs):
        context = super(AutoArticleUpdateView, self).get_context_data(**kwargs)
        context.update({'html_title': 'Редактирование статьи'})
        context.update({'edit': True})
        return context

    def get_success_url(self):
        return reverse("auto:auto_database_list")


# Редактировать автомобиль
class AutoUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    permission_required = 'auto.change_auto'
    raise_exception = True
    model = Auto
    form_class = AutoForm
    pk_url_kwarg = 'auto_id'
    template_name = 'auto/auto_edit.html'
    action = 'update'

    def get_context_data(self, **kwargs):
        context = super(AutoUpdateView, self).get_context_data(**kwargs)
        form = context['form']
        mark = form['mark_auto'].value()
        model = form['model_auto'].value()
        form.fields['model_auto'].queryset = ModelAuto.objects.filter(
            mark_auto_id=mark)
        form.fields['generation'].queryset = Generation.objects.filter(
            model_auto_id=model)
        context.update({'html_title': 'Редактирование автомобиля'})
        context.update({'edit': True})
        photos = AutoPhoto.objects.filter(
            auto=self.object).order_by('sort_key')
        photos = []
        for i, p in enumerate(photos):
            try:
                photos.append({'id': p.id, 'image': p.image.url,
                               'image_small': p.image_small.url, 'sort_key': i})
            except OSError:
                continue
        json_photos = json.dumps(photos)
        context.update({'photos': json_photos})
        context.update({'auto_id': self.object.id})
        return context

    def form_valid(self, form):
        logger.error('form_valid')
        self.object = form.save()
        photos = self.request.FILES.getlist('photo')

        for f in photos:
            data = f.read()
            photo = AutoPhoto(auto=self.object)
            photo.image.save('', ContentFile(data))
            photo.save()

        if self.request.POST:
            return JsonResponse({'auto_id': self.object.id, 'redirect': self.get_success_url()})

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        logger.error('form_invalid')
        return JsonResponse(status=200, data={'errors': form.errors})

    def get_success_url(self):
        logger.error('get_success_url')
        return reverse("auto:auto_list")


class RecommendedAutoUpdateView(AutoUpdateView):
    form_class = RecommendedAutoForm
    permission_required = 'auto.access_to_recommended_car'

    def get_context_data(self, **kwargs):
        context = super(RecommendedAutoUpdateView, self).get_context_data(**kwargs)
        context.update({'recommended_page': True})
        return context

    def get_success_url(self):
        logger.error('get_success_url')
        return reverse("auto:recommended_auto")

    def form_valid(self, form):
        logger.error('form_valid')
        obj = form.save(commit=False)
        obj.recommended = True
        self.object = form.save()
        photos = self.request.FILES.getlist('photo')

        for f in photos:
            data = f.read()
            photo = AutoPhoto(auto=self.object)
            photo.image.save('', ContentFile(data))
            photo.save()
        if self.request.POST:
            return JsonResponse({'auto_id': self.object.id, 'redirect': self.get_success_url()})
        return HttpResponseRedirect(self.get_success_url())


# Удаление фото recommended car
# @csrf_exempt
@login_required
def delete_recommended_photos(request):
    if request.method == 'POST':
        photos = request.POST.getlist('photo_id')
        for file_id in photos:
            photo = AutoPhoto.objects.get(id=file_id)
            photo.delete()
        return HttpResponse(status=200)
    return HttpResponse(status=404)


# Просмотр авто - детально
class AutoDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView, FilteredFieldContextMixin):
    permission_required = 'auto.view_auto'
    raise_exception = True
    model = Auto
    slug_field = 'vin'
    slug_url_kwarg = 'slug'
    template_name = 'auto/auto_detail.html'

    def get_context_data(self, **kwargs):
        context = super(AutoDetailView, self).get_context_data(**kwargs)
        photos = self.object.photos_auto.order_by('sort_key').all()
        get_new_report = ReportPodbor.objects.filter(auto=self.object).order_by('-id').first()
        context.update({
            'html_title': 'Автомобиль {}'.format(self.object),
            'photos': photos,
            'get_new_report': get_new_report
        })
        return context

# Просмотр авто - детально


class AutoDetailByPkView(AutoDetailView):
    permission_required = 'auto.view_auto'
    raise_exception = True
    model = Auto
    pk_url_kwarg = 'auto_id'
    template_name = 'auto/auto_detail.html'

    def get_queryset(self):
        queryset = super(AutoDetailByPkView, self).get_queryset()
        if self.request.user.is_client:
            return Auto.objects.none()
        return queryset
        # order__client__filials__in=user.filials.all()


@login_required
@permission_required('auto.delete_auto', raise_exception=True)
def delete_auto(request, auto_id):
    if request.method == 'POST':
        auto = get_object_or_404(Auto, pk=auto_id)
        auto.delete(force_policy=SOFT_DELETE_CASCADE)
        return HttpResponse(status=200)
    raise Http404


@login_required
def delete_recommended_auto(request, auto_id):
    if request.method == 'POST':
        auto = get_object_or_404(Auto, pk=auto_id)
        auto.delete(force_policy=SOFT_DELETE_CASCADE)
        return HttpResponse(status=200)
    raise Http404


@login_required
def sales_auto(request, auto_id):
    if request.method == 'POST':
        auto = get_object_or_404(Auto, pk=auto_id)
        auto.sales = True
        auto.save()
        return HttpResponse(status=200)
    raise Http404


@login_required
@permission_required('auto.add_auto', raise_exception=True)
def get_data_from_auto_ru(request):
    if request.method == 'POST':
        url = request.POST.get('url')
        from .utils import get_data_from_auto_ru
        auto_ru_data = get_data_from_auto_ru(url)

        if auto_ru_data['status'] == 'error':
            return JsonResponse(data={'errors': ''})
        else:
            return JsonResponse(data=auto_ru_data.get('data'))

    return HttpResponseRedirect(redirect_to=reverse('auto:auto_add'))


@login_required
@permission_required('auto.add_auto', raise_exception=True)
def get_data_from_avito_ru(request):
    if request.method == 'POST':
        url = request.POST.get('url')
        from .utils import get_data_from_avito_ru
        avito_ru_data = get_data_from_avito_ru(url)

        if avito_ru_data['status'] == 'error':
            return JsonResponse(data={'errors': ''})
        else:
            return JsonResponse(data=avito_ru_data.get('data'))

    return HttpResponseRedirect(redirect_to=reverse('auto:auto_add'))
