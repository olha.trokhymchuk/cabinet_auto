from django.conf.urls import url
from . import views
from .views import *
from auto.api.views import ReportDetaiApi

app_name = 'auto'

urlpatterns = [
    url(r'^all-auto/$', views.AutoListView.as_view(), name='auto_list'),
    url(r'^recommended-auto/$', views.RecommendedAutoListView.as_view(), name='recommended_auto'),

    url(r'^filter/$', views.get_car_filter, name='car_filter'),
    url(r'^add-article/$', views.AutoArticleCreateView.as_view(), name='auto_article_add'),
    url(r'^edit-article/(?P<pk>\d+)/$',
        views.AutoArticleUpdateView.as_view(), name='article_edit'),
    url(r'^add-auto/$', views.AutoCreateView.as_view(), name='auto_add'),
    url(r'^edit-auto/(?P<auto_id>\d+)/$',
        views.AutoUpdateView.as_view(), name='auto_edit'),

    url(r'^add-recommended-auto/$', views.RecommendedCreateView.as_view(), name='recommended_auto_add'),
    url(r'^edit-recommended-auto/(?P<auto_id>\d+)/$',
        views.RecommendedAutoUpdateView.as_view(), name='recommended_edit'),
    url(r'^sales-auto/(?P<auto_id>\d+)/$',
        views.sales_auto, name='sales_recommended_auto'),
    url(r'^delete-recommended-photos/$', delete_recommended_photos, name='delete_recommended_photos'),

    url(r'^delete-auto/(?P<auto_id>\d+)/$',
        views.delete_auto, name='auto_delete'),
    url(r'^delete-recommended-auto/(?P<auto_id>\d+)/$',
        views.delete_recommended_auto, name='auto_recommended_delete'),
    url(r'^detail-auto/(?P<auto_id>\d+)/$',
        views.AutoDetailByPkView.as_view(), name='auto_detail'),
    url(r'^detail-auto/(?P<slug>[-\w]+)/$',
        views.AutoDetailView.as_view(), name='auto_detail_slug'),

    # Auto database
    url(r'^auto-database/$', views.AutoDatabaseList.as_view(),
        name='auto_database_list'),
    url(r'^auto-database/(?P<pk>\d+)$',
        views.AutoDatabaseView.as_view(), name='auto_database_item'),

    # ajax form data
    url(r'^marks/$', views.get_marks, name='get_marks'),
    url(r'^models/(?P<mark_id>\d+)$', views.get_models, name='get_models'),
    url(r'^generations/(?P<model_id>\d+)$',
        views.get_generations, name='get_generations'),
    url(r'^get-data-from-auto-ru/$', views.get_data_from_auto_ru,
        name='add_auto_from_auto_ru'),
    url(r'^get-data-from-avito-ru/$', views.get_data_from_avito_ru,
        name='add_auto_from_avito_ru'),
    url(r'^add-photo-auto/(?P<auto_id>\d+)$',
        views.add_photo_auto, name='add_photo_auto'),
    url(r'^update-photo-auto/(?P<photo_id>\d+)/(?P<sort_key>\d+)$',
        views.update_photo_auto, name='update_photo_auto'),
    url(r'^delete-photo-auto/(?P<photo_id>\d+)$',
        views.delete_photo_auto, name='delete_photo_auto'),
    url(r'^update-photo-auto-position/(?P<photo_id>\d+)/(?P<sort_key>\d+)$',
        views.update_photo_auto_position, name='update_photo_auto_position'),
    url(r'^api/form_data/$', ReportDetaiApi.as_view(), name='auto_form_data'),
]
