import logging
import os,random, time

from seleniumrequests import Firefox
from selenium import webdriver

import re
import urllib
import urllib.request

from lxml import html

import requests
import pycurl
import cssutils
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
from datetime import datetime
import socket
import socks
from random import choice, uniform

from stem import Signal
from stem.control import Controller

import subprocess

import grab
# from .custom_proxy import Proxy
import base64
import json
import dateparser
from django.core.files import File
from django.forms import model_to_dict

from lxml import html
from grab import Grab
from grab.error import GrabTimeoutError,GrabConnectionError,GrabNetworkError
from antigate import AntiGate, AntiGateError
from urllib.parse import parse_qs
from django.conf import settings
from django.db import DatabaseError
from grab.spider import Spider, Task
from weblib.error import DataNotFound

from auto.models import ModelAuto, Generation, MarkAuto, AUTHOR_TYPE, AutoPhoto, Year, BodyType
from auto.utils import get_unique_list_by_key, get_choice_by_name, is_int

logger = logging.getLogger(__name__)


#предустановка headers
class Headers:
    def pre_set_headers(self):
        useragents = open("./useragents.txt", 'r')
        useragents = useragents.read()
        useragents = useragents.split("\n")
        useragent = useragents[random.randint(0, len(useragents))]
        self.headers = {'Content-type': 'application/x-www-form-urlencoded;charset=UTF-8', 
                    'Accept': '*/*',
                    'Accept-Encoding': 'gzip, deflate, br',
                    'Accept-Language': 'en-US,en;q=0.9,ru;q=0.8,de;q=0.7,uk;q=0.6,und;q=0.5',
                    'Connection': 'keep-alive',
                    'Content-Length': '85',
                    'Host': 'auto.ru',
                    'Origin': 'https://auto.ru',
                    'Referer': 'https://auto.ru/volgograd/cars/audi/all/?sort=fresh_relevance_1-desc',
                    'Sec-Fetch-Mode': 'same-origin',
                    'Sec-Fetch-Site': 'same-origin',
                    'User-Agent':useragent,
                    'x-client-app-version': '201909.10.160930',
                    'x-csrf-token': '677ce6d590834cc75091fee1c0ed80a71b20a618f4818c75',
                    'x-requested-with': 'fetch'
                    }
        self.cookies = {
            "_csrf_token": "677ce6d590834cc75091fee1c0ed80a71b20a618f4818c75", 
            "autoru_sid": "a%3Ag5d78f67e2fc1ghtpibvljk8n5gpvlu1.7546ebb24df781f689659e1c0bb34b76%7C1568208510918.604800.SC1yaDijbu74lseXqevcAQ.VS6sCrfQ47vC5-0doTE_JdD14UQz9oTqrIc6WA6fxPE",
            "autoruuid": "g5d78f67e2fc1ghtpibvljk8n5gpvlu1.7546ebb24df781f689659e1c0bb34b76",
            "suid": "ff301d1245cce3586fbc14d873f0d5b7.147a92f6ba6dc8361f21b754b1a728e6", 
            "counter_ga_all7":"1",
            "X-Vertis-DC": "myt",
            "_ym_wasSynced": "%7B%22time%22%3A1568208547247%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D",
            "from": "yandex",
            "yandexuid": "3112308321506346419",
            "my": "YwA%3D",
            "_ym_uid": "15682085551059184621",
            "ym_d":"1568208555",
            "ym_visorc_22753222": "w",
            "_ym_isad": "2",
            "af_lpdid": "22:953088520", 
            "_ga": "GA1.2.150830891.1568208979",
            "_gid": "GA1.2.408080465.1568208979",
            "cycada": "OgVKs2qG+NEQFAH+EThNbl72DIicAr6D38p2UfQG56U=",
            "from_lifetime": "1568209585042"
        }


#парсит объявления через selenium
class ConnectTroughSelenium:
    def __init__(self, url):
        self.url = url

    def get_text_from_element_parent(self, xpath):
        return self.driver.find_element_by_xpath(xpath)

    def get_text_from_element(self, xpath):
        element = self.get_text_from_element_parent(xpath)
        return element.text
        return element.get_attribute('outerHTML')

    def request_through_selenium(self):
        self.data.update({"section": "all", "category": "cars", "sort": "fresh_relevance_1-desc", "geo_id": 38})
        profile = webdriver.FirefoxProfile()
        profile.set_preference("network.proxy.type", 1)
        profile.set_preference('network.proxy.socks', '127.0.0.1')
        profile.set_preference('network.proxy.socks_port', 9050)
        profile.set_preference("network.proxy.socks_remote_dns", False)
        profile.update_preferences()
        self.driver = Firefox(firefox_profile=profile)

    def parser_connect(self):
        self.data = {}
        self.request_through_selenium()
        self.set_headers()
        # https://auto.ru/cars/used/sale/toyota/land_cruiser_prado/1092097232-4ccfe978/?page_from=page_listing%2Cbloc0k_listing%2Ctype_single
        page = {}
        response = self.driver.get(self.url)
        self.driver.find_element_by_css_selector('#confirm-button').click()
        time.sleep(10)
        parse_page = self.driver.page_source.encode('utf-8') 
        time.sleep(2)
        page.update({'status': 'ok', 'data': self.parse_all()})
        self.driver.close()
        return page

#парсит объявления
class ParserThroughRequest:
    def __init__(self, url):
        self.url = url
        with Controller.from_port(port = 9051) as c:
            c.authenticate(settings.TORRPASSWORD)
            c.signal(Signal.NEWNYM) 
        self.session = requests.session()
        self.session.proxies = {
            'http': 'socks5://127.0.0.1:9050',
            'https': 'socks5://127.0.0.1:9050'
        }
        self.get_page()

    def get_page(self):
        r = self.session.get(self.url)
        r.encoding = 'utf-8'
        self.response = html.fromstring(r.text)
        # print(r.text)

    def get_text_from_element_parent(self, xpath):
        return self.response.xpath(xpath)

    def get_text_from_element(self, xpath):
        res = self.get_text_from_element_parent(xpath)
        return res[0].text
        
    def parser_connect(self):
        response = {}
        response.update({'status': 'ok', 'data': self.parse_all()})
        return response

#парсит объявления
class Parser:
    def __init__(self, url):
        self.text_blocking = ''
        # количество попыток парсинга (если появляется капча)
        self.attempt_count = settings.ATTEMPT_PARSE_COUNT
        self.set_headers()
        self.grab = Grab()
        self.grab.setup(log_file='grab_out.html')
        self.grab.setup(headers=self.headers)
        self.grab.setup(cookies=self.cookies)
        self.url = url
        session = self.get_tor_session()
    
    def get_text_from_element_parent(self, xpath):
        return self.grab.doc.select(xpath)

    def get_text_from_element(self, xpath):
        try:
            return self.get_text_from_element_parent(xpath).one().text()

        except grab.error.DataNotFound as ex:
            logger.warning(ex)
            return None
        
    def get_proxy(self):
        proxy = Proxy()
        return proxy.get_proxy()

    def get_tor_session(self):
        self.grab.setup(proxy="127.0.0.1:9050", proxy_type="socks5", connect_timeout=settings.CONNECT_TIMEOUT, timeout=settings.TIMEOUT)

    def parser_connect(self):
        response = {}
        attempts = self.attempt_count
        while attempts > 0:
            useragents = open('useragents.txt').read().split('\n')
            self.grab.setup(debug=True)
            try:
                result = self.grab.go(self.url)
            except GrabTimeoutError as er:
                logger.error(er)
                logger.error('request_headers: ' + self.grab.request_headers)
                continue
            except GrabConnectionError as er:
                logger.error(er)
                self.grab.request_headers
                continue
            except DataNotFound as er:
                logger.error(er)
                logger.error('request_headers: ' + str(self.grab.request_headers))
                continue
            except GrabNetworkError as er:
                logger.error(er)
                logger.error('request_headers: ' + self.grab.request_headers)
                continue
            logger.error('request_headers: ' + self.grab.request_headers)
            if self.grab.doc.text_search(self.text_blocking):
                try:   
                    logger.error(self.text_blocking)
                    if attempts > 1:
                        self.send_captcha()
                    attempts -= 1
                except DataNotFound:
                    continue
            else:
                response.update({'status': 'ok', 'data': self.parse_all()})
                attempts = False
                return response

        logger.error('captcha did not resolve')
        response.update({'status': 'error'})
        return response

