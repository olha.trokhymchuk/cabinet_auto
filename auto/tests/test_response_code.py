from django.urls import reverse

from auto.models import Auto
from core.tests.test_response_code import _ResponseCodeTestCase
from order.models import Order
from report.models import ReportPodbor


class AutoListTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(AutoListTestCase, self).__init__(*args, **kwargs)
        self.codes = {
            'Администратор': [200],
            'Руководитель': [200],
            'Бухгалтер': [200],
            'Контент-менеджер': [200],
            'Эксперт': [200],
            'Оператор': [200],
            'Клиент': [200],
            '*': [302, 403]
        }

        self.perm = 'auto.view_auto'
        self.url = reverse('auto:auto_list')


class AutoAddTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(AutoAddTestCase, self).__init__(*args, **kwargs)
        self.codes = {
            'Администратор': [200],
            'Руководитель': [200],
            'Бухгалтер': [403],
            'Контент-менеджер': [403],
            'Эксперт': [200],
            'Оператор': [403],
            'Клиент': [403],
            '*': [302, 403]
        }

        self.perm = 'auto.add_auto'
        self.url = reverse('auto:auto_add')


class AutoEditTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(AutoEditTestCase, self).__init__(*args, **kwargs)
        self.codes = {
            'Администратор': [200],
            'Руководитель': [403],
            'Бухгалтер': [403],
            'Контент-менеджер': [403],
            'Эксперт': [200],
            'Оператор': [403],
            'Клиент': [403],
            '*': [302, 403]
        }

        self.perm = 'auto.change_auto'

    def setUp(self):
        super(AutoEditTestCase, self).setUp()
        self.auto = Auto.objects.create()
        for user, client in self.clients:
            if user.is_expert:
                self.report = ReportPodbor.objects.create(auto=self.auto, executor=user)
        self.url = reverse('auto:auto_edit', args=(self.auto.pk,))

    def tearDown(self):
        Auto.objects.all().delete()
        ReportPodbor.objects.all().delete()
        super(AutoEditTestCase, self).tearDown()


class AutoDetailTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(AutoDetailTestCase, self).__init__(*args, **kwargs)
        self.codes = {
            'Администратор': [200],
            'Руководитель': [200],
            'Бухгалтер': [200],
            'Контент-менеджер': [200],
            'Эксперт': [200],
            'Оператор': [200],
            'Клиент': [200],
            '*': [302, 403]
        }

        self.perm = 'auto.view_auto'

    def setUp(self):
        super(AutoDetailTestCase, self).setUp()
        for u, c in self.clients:
            if u.is_client:
                auto = Auto.objects.create()
                order = Order.objects.create(client=u)
                report = ReportPodbor.objects.create(auto=auto)
                report.order.add(order)
                self.url = reverse('auto:auto_detail', args=(auto.pk,))
                return

        auto = Auto.objects.create()
        self.url = reverse('auto:auto_detail', args=(auto.pk, ))

    def tearDown(self):
        Auto.objects.all().delete()
        Order.objects.all().delete()
        ReportPodbor.objects.all().delete()
        super(AutoDetailTestCase, self).tearDown()
