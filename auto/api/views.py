from rest_framework.views import APIView
from rest_framework.response import Response

from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from rest_framework.renderers import JSONRenderer

import auto.glossary as auto_glossary

from auto.models import Auto, Year, BodyType, AutoPhoto, MarkAuto, ModelAuto, Generation

class ReportDetaiApi(APIView):
    """
    Retrieve, AutoFormData.
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def post(self, request):
        mark_auto = [{"id": mark.id, "name": mark.name} for mark in MarkAuto.objects.all()]
        year_auto = [{"id": year.id, "name": year.year} for year in Year.objects.all()]
        body_type_auto = [{"id": body_type.id, "name": body_type.name} for body_type in BodyType.objects.all()]
        engine_type = [{"id": engine[0], "name": engine[1]} for engine in auto_glossary.ENGINE_TYPE]
        engine_capacity = [{"id": capacity[0], "name": capacity[1]} for capacity in auto_glossary.ENGINE_CAPACITY]
        transmission_type = [{"id": transmission[0], "name": transmission[1]} for transmission in auto_glossary.TRANSMISSION_TYPE]
        drive_type = [{"id": drive[0], "name": drive[1]} for drive in auto_glossary.DRIVE_TYPE]
        author_type = [{"id": author[0], "name": author[1]} for author in auto_glossary.AUTHOR_TYPE]
        
        model_auto = []
        generation = []
        if request.POST:
            mark_id = request.POST.get("mark_auto", "")
            model_id = request.POST.get("model_auto", "")
            if mark_id:
                model_auto = [{"id": model.id, "name": model.name} for model in
                              ModelAuto.objects.filter(id=int(model_id))]
                if model_auto:
                    generation = [{"id": generation.id, "name": generation.name} for generation in
                                  Generation.objects.filter(model_auto_id=int(model_id))]

        data = {"mark_auto": mark_auto,"model_auto": model_auto,"generation": generation,
        		"year_auto":year_auto,"body_type_auto":body_type_auto,"engine_type":engine_type,
        		"engine_capacity":engine_capacity,"transmission_type":transmission_type,
        		"drive_type":drive_type,"author_type":author_type,
        }
        return Response(data)