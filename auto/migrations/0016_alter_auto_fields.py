# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-07 08:11
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('auto', '0015_auto_20171207_1111'),
    ]

    operations = [
        migrations.AlterField(
            model_name='auto',
            name='body_type_auto',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='auto.BodyType', verbose_name='Тип кузова'),
        ),
        migrations.AlterField(
            model_name='auto',
            name='year_auto',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='auto.Year', verbose_name='Год выпуска'),
        ),
    ]
