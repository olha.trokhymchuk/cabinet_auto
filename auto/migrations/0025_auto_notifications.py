# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-10-30 17:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0014_auto_20181030_2058'),
        ('auto', '0024_auto_20181002_1422'),
    ]

    operations = [
        migrations.AddField(
            model_name='auto',
            name='notifications',
            field=models.ManyToManyField(blank=True, default=None, related_name='notifications', to='notifications.Notification', verbose_name='Оповещения'),
        ),
    ]
