# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-11-20 06:40
from __future__ import unicode_literals

import auto.models
from django.db import migrations, models
import django.db.models.deletion
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('auto', '0008_auto_20171108_1935'),
    ]

    operations = [
        migrations.CreateModel(
            name='AutoRU',
            fields=[
                ('auto_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='auto.Auto')),
                ('ad_number', models.CharField(blank=True, default=None, max_length=20, null=True, verbose_name='Номер объявления')),
                ('public_date', models.DateTimeField(verbose_name='Дата публикации')),
                ('view_count', models.IntegerField(blank=True, null=True, verbose_name='Количество просмотров')),
            ],
            options={
                'verbose_name': 'Данные с авто.ру',
                'verbose_name_plural': 'Данные с авто.ру',
            },
            bases=('auto.auto',),
        ),
        migrations.CreateModel(
            name='AutoRuPhoto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to=auto.models.upload_auto, verbose_name='Фото')),
            ],
        ),
        migrations.AddField(
            model_name='auto',
            name='author',
            field=models.CharField(blank=True, default=None, max_length=30, null=True, verbose_name='Автор'),
        ),
        migrations.AddField(
            model_name='auto',
            name='author_type',
            field=models.IntegerField(choices=[(0, 'юридическое лицо'), (1, 'частное лицо')], default=0, verbose_name='Тип автора'),
        ),
        migrations.AddField(
            model_name='auto',
            name='cost',
            field=models.IntegerField(blank=True, null=True, verbose_name='Цена'),
        ),
        migrations.AddField(
            model_name='auto',
            name='mileage',
            field=models.PositiveIntegerField(blank=True, default=None, null=True, verbose_name='Пробег'),
        ),
        migrations.AddField(
            model_name='auto',
            name='owners',
            field=models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Количество владельцев'),
        ),
        migrations.AddField(
            model_name='auto',
            name='phone',
            field=models.CharField(blank=True, default=None, max_length=16, null=True, verbose_name='Номер телефона'),
        ),
        migrations.AddField(
            model_name='auto',
            name='state',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='Состояние'),
        ),
        migrations.AlterField(
            model_name='auto',
            name='comment',
            field=models.TextField(blank=True, default=None, null=True, verbose_name='Комментарий'),
        ),
        migrations.AlterField(
            model_name='auto',
            name='engine_type',
            field=models.PositiveSmallIntegerField(choices=[(0, 'Не важно'), (1, 'Бензин'), (2, 'Дизель'), (3, 'Гибрид'), (3, 'Электро')], default=0, verbose_name='Тип двигателя'),
        ),
        migrations.AlterUniqueTogether(
            name='generation',
            unique_together=set([]),
        ),
        migrations.AlterUniqueTogether(
            name='modelauto',
            unique_together=set([]),
        ),
        migrations.AddField(
            model_name='autoruphoto',
            name='auto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='photos_auto', to='auto.Auto'),
        ),
    ]
