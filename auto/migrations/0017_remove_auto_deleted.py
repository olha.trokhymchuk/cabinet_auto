# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-22 12:23
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('auto', '0016_alter_auto_fields'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='auto',
            name='deleted',
        ),
    ]
