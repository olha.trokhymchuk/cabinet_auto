import json

from django.contrib.sites.shortcuts import get_current_site
from django.contrib.postgres.fields import IntegerRangeField
from django.contrib.postgres.validators import RangeMinValueValidator
from django.db import models
from django.contrib.postgres.fields import JSONField
from django.core.exceptions import ValidationError

from imagekit.models import ProcessedImageField, ImageSpecField
from safedelete import HARD_DELETE
from safedelete.models import SafeDeleteModel

from auto.glossary import *
from auto.utils import OriginalSpec, ThumbnailSpec
from core.utils import CustomFieldHistoryTracker

from notifications.models import Notification
from django.db.models import Lookup
from django.db.models.fields import Field

now = datetime.datetime.now()
year = now.strftime("%Y")
month = now.strftime("%m")
day = now.strftime("%d")
time = now.strftime("%H%M%S")


def upload_auto(instance, filename):
    filebase, extension = filename.split(".")
    return "auto/%s/%s/%s/id_%s/auto_%s.%s" % (year, month, day, instance.auto.id, time, extension)


@Field.register_lookup
class InLower(Lookup):
    lookup_name = 'inlower'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return 'lower(%s) IN (%s)' % (lhs, rhs), params


class MarkAuto(models.Model):
    name = models.CharField('Марка', max_length=20,
                            blank=True, null=True, default=None, unique=True)
    popular = models.BooleanField('Популярная', default=False)

    class Meta:
        verbose_name = "Марка"
        verbose_name_plural = "Марки"
        ordering = ["name"]

    def __str__(self):
        return self.name


class ModelAuto(models.Model):
    name = models.CharField('Модель', max_length=50,
                            blank=True, null=True, default=None, db_index=True)
    mark_auto = models.ForeignKey(MarkAuto, default=None, verbose_name='Марка')
    popular = models.BooleanField('Популярная', default=False)

    class Meta:
        verbose_name = "Модель"
        verbose_name_plural = "Модели"
        ordering = ["name"]
        unique_together = [['mark_auto', 'name']]

    def __str__(self):
        return self.name


class Generation(models.Model):
    name = models.CharField('Кузов', max_length=255,
                            blank=True, null=True, default=None, db_index=True)
    years = IntegerRangeField('Период (годы)', validators=[
                              RangeMinValueValidator(1890)], null=True, blank=True)
    model_auto = models.ForeignKey(ModelAuto, verbose_name='Модель')

    class Meta:
        verbose_name = "Поколение"
        verbose_name_plural = "Поколения"
        ordering = ["name"]

    def __str__(self):
        name = self.name + ' / ' if self.name else ''
        return name + self.human_years

    @property
    def human_years(self):
        if self.years:
            upper_year = str(self.years.upper) + \
                'г.' if self.years.upper else 'н.в.'
            years = 'с %sг. по %s' % (self.years.lower, upper_year)
            return years
        else:
            return ''

    @property
    def xlsx_years(self):
        if self.years:
            upper_year = str(self.years.upper) if self.years.upper else ''
            years = '%s-%s' % (self.years.lower, upper_year)
            return years
        else:
            return ''

    def save(self, *args, **kwargs):
        if self.years and self.years.lower:
            generation = Generation.objects.exclude(pk=self.id).filter(
                name=self.name, model_auto=self.model_auto, years__inlower=self.years.lower)
        else:
            generation = Generation.objects.exclude(pk=self.id).filter(
                name=self.name, model_auto=self.model_auto)
        if generation.count() < 1:
            # pass
            super(Generation, self).save(*args, **kwargs)
        else:
            pass
            # raise ValidationError("Поколение уже существует")


class Year(models.Model):
    year = models.PositiveSmallIntegerField('Год')

    class Meta:
        verbose_name = "Год"
        verbose_name_plural = "Года"
        ordering = ["year"]

    def __str__(self):
        return "{}".format(self.year)


class BodyType(models.Model):
    name = models.CharField('Кузов', max_length=255)

    class Meta:
        verbose_name = "Кузов"
        verbose_name_plural = "Кузова"
        ordering = ["name"]

    def __str__(self):
        return self.name


class Auto(SafeDeleteModel):
    _safedelete_policy = HARD_DELETE

    created = models.DateTimeField(
        'Дата создания', auto_now_add=True, db_index=True)
    modified = models.DateTimeField(
        'Последнее изменение', auto_now=True, null=True)
    vin = models.CharField('VIN', max_length=17, unique=True, blank=True, null=True, db_index=True)
    mark_auto = models.ForeignKey(
        MarkAuto, blank=True, null=True, default=None, verbose_name='Марка')
    model_auto = models.ForeignKey(
        ModelAuto, blank=True, null=True, default=None, verbose_name='Модель')
    generation = models.ForeignKey(
        Generation, blank=True, null=True, default=None, verbose_name='Поколение')
    year_auto = models.ForeignKey(
        Year, blank=True, null=True, default=None, verbose_name='Год выпуска')
    body_type_auto = models.ForeignKey(
        BodyType, blank=True, null=True, default=None, verbose_name='Тип кузова')
    transmission_type = models.PositiveSmallIntegerField(
        'Тип трасмиссии', null=True, blank=True, choices=TRANSMISSION_TYPE, default=0)
    engine_capacity = models.CharField(
        'Объём двигателя', blank=True, null=True, choices=ENGINE_CAPACITY, max_length=4, default='0.0')
    engine_type = models.PositiveSmallIntegerField(
        'Тип двигателя', blank=True, null=True, choices=ENGINE_TYPE, default=0)
    horsepower = models.PositiveIntegerField(
        'Лошадиные силы', blank=True, null=True, default=None)
    drive_type = models.IntegerField(
        'Тип привода', blank=True, null=True, choices=DRIVE_TYPE, default=0)
    color_auto = models.CharField(
        'Цвет', max_length=50, default='', null=True, blank=True)
    color_salon = models.CharField(
        'Цвет салона', max_length=50, default=None, null=True, blank=True)
    salon_auto = models.CharField(
        'Салон', max_length=50, default='', null=True, blank=True)
    equipment = models.CharField(
        'Комплектация', max_length=2250, default='', null=True, blank=True)
    comment = models.TextField(
        'Комментарий', blank=True, null=True, default=None)
    cost = models.IntegerField('Цена', blank=True, null=True, default=None)
    author = models.CharField('Автор', max_length=255,
                              blank=True, null=True, default=None)
    author_type = models.IntegerField(
        'Тип автора', blank=True, null=True, choices=AUTHOR_TYPE, default=0)
    owners = models.PositiveSmallIntegerField(
        'Количество владельцев', blank=True, null=True)
    state = models.CharField('Состояние', max_length=30, blank=True, null=True)
    mileage = models.PositiveIntegerField(
        'Пробег', blank=True, null=True, default=None)
    phone = models.CharField(
        'Номер телефона', max_length=16, blank=True, null=True, default=None)
    source = models.CharField(
        'Источник', max_length=255, blank=True, null=True, default=None)
    location = models.CharField(
        'Местоположение', max_length=255, blank=True, null=True, default=None)
    resell = models.BooleanField('Перекуп',  default=False)
    send_autospot = models.BooleanField(
        'Отправить в автоспот', default=False)
    notifications = models.ManyToManyField(
        Notification, blank=True, default=None, verbose_name='Оповещения', related_name="notifications")
    data_lk = JSONField('Данные с лк', default=dict, blank=True, null=True)
    recommended = models.BooleanField('Рекомендованое авто', default=False)
    sales = models.BooleanField('Продано', default=False)

    field_history = CustomFieldHistoryTracker(
        ['vin', 'mark_auto', 'model_auto', 'generation', 'year_auto', 'body_type_auto',
         'transmission_type', 'engine_capacity', 'horsepower', 'drive_type', 'color_auto',
         'salon_auto', 'equipment', 'comment', 'cost', 'author', 'author_type', 'owners',
         'state', 'mileage', 'phone', 'source', 'location', 'resell', 'send_autospot'])

    class Meta:
        verbose_name = "Авто"
        verbose_name_plural = "Авто"
        ordering = ["-created"]
        permissions = (
            ('access_to_recommended_car', "Can add or delete recommended car"),
        )

    def get_engine_str(self):
        return '{0} л./ {1} л.с./ {2}'.format(
            self.engine_capacity,
            self.horsepower,
            ENGINE_TYPE[self.engine_type][1]
        )

    def get_author_type_str(self):
        return AUTHOR_TYPE[self.author_type][1]

    def get_transmission_type_str(self):
        return TRANSMISSION_TYPE[self.transmission_type][1]

    def get_drive_type_str(self):
        return DRIVE_TYPE[self.drive_type][1]

    def get_name(self):
        return '%s %s' % (self.mark_auto.name, self.model_auto.name)

    def get_avatar(self):
        return self.photos_auto.order_by('sort_key').first()

    def get_media_base(self):
        return get_current_site(None).domain

    def get_first_photo(self):
        photo = self.photos_auto.last()
        return photo

    def get_all_photos(self):
        return self.photos_auto.all()

    def __str__(self):
        return self.vin if self.vin else '{} {} {}'.format(self.mark_auto, self.model_auto, self.generation)


class AutoRU(Auto):
    ad_number = models.CharField(
        'Номер объявления', max_length=20, blank=True, null=True, default=None)
    public_date = models.DateTimeField('Дата публикации')
    view_count = models.IntegerField(
        'Количество просмотров', blank=True, null=True)

    class Meta:
        verbose_name = "Данные с авто.ру"
        verbose_name_plural = "Данные с авто.ру"


class AutoPhoto(models.Model):
    auto = models.ForeignKey(Auto, related_name='photos_auto')
    embed_url = models.URLField('embed url', max_length=256, default='')
    image = ProcessedImageField(verbose_name='Фото', upload_to=upload_auto,
                                blank=True, null=True, spec=OriginalSpec, autoconvert=None)
    image_small = ImageSpecField(source='image', spec=ThumbnailSpec)
    image_google = models.TextField('google_auto_id', blank=True, null=True, default='')
    sort_key = models.IntegerField(blank=True, null=True, default=0)

    def __str__(self):
        return "image of {}".format(self.auto)

    @staticmethod
    def get_prev_photo():
        return AutoPhoto.objects.order_by('-sort_key').first()

    @staticmethod
    def get_avatar(auto_id):
        return AutoPhoto.objects.filter(auto=auto_id).order_by('sort_key').first()


class Avito(Auto):
    ad_number = models.CharField(
        'Номер объявления', max_length=20, blank=True, null=True, default=None)
    public_date = models.DateTimeField('Дата публикации')
    view_count = models.IntegerField(
        'Количество просмотров', blank=True, null=True)

    class Meta:
        verbose_name = "Данные с avito"
        verbose_name_plural = "Данные с avito"


class AutoDataBaseCategorys (models.Model):

    class Meta:
        verbose_name = "Категорию статей"
        verbose_name_plural = "Категории статей"

    title = models.CharField(
        'Название категории', max_length=255, default='-', null=False, blank=False)

    def __str__(self):
        return self.title


class AutoDataBase (models.Model):

    class Meta:
        verbose_name = "Базу знаний"
        verbose_name_plural = "База знаний"

    mark_auto = models.ForeignKey(
        MarkAuto, blank=True, null=True, default=None, verbose_name='Марка')
    model_auto = models.ForeignKey(
        ModelAuto, blank=True, null=True, default=None, verbose_name='Модель')
    generation = models.ForeignKey(
        Generation, blank=True, null=True, default=None, verbose_name='Поколение')
    category = models.ForeignKey(
        AutoDataBaseCategorys, blank=True, null=True, default=None, verbose_name='Категория')
    title = models.CharField(
        'Название статьи', max_length=255, default='-', null=False, blank=False)
    text = models.TextField('Текст статьи', blank=True, null=True, default='-')
    created = models.DateTimeField(
        'Дата создания', auto_now_add=True, db_index=True)
    description = models.CharField(
        'Краткое описание', max_length=255, default='', null=False, blank=False)

    def __str__(self):
        return self.title
