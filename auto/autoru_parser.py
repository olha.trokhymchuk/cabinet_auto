import logging
import os
import re
import urllib
import cssutils
import urllib.request
from datetime import datetime

import grab
# from .custom_proxy import Proxy
import requests
import base64
import json
import dateparser
from django.core.files import File
from django.forms import model_to_dict

from stem import Signal
from stem.control import Controller

from lxml import html

from grab import Grab
from antigate import AntiGate, AntiGateError
from urllib.parse import parse_qs
from django.conf import settings
from django.db import DatabaseError
from grab.spider import Spider, Task
from weblib.error import DataNotFound

from auto.models import ModelAuto, Generation, MarkAuto, AUTHOR_TYPE, AutoPhoto, Year, BodyType
from auto.utils import get_unique_list_by_key, get_choice_by_name, is_int

from .parser import Parser, ConnectTroughSelenium, Headers, ParserThroughRequest

logger = logging.getLogger(__name__)


marks_url = "https://auto.ru/ajax/form/get_marks/?category_id=15&section_id=1"
model_url = "https://auto.ru/ajax/form/get_folders/?section_id=1&category_id=15&mark_id={mark_id}&level=1"
years_url = "https://auto.ru/ajax/form/get_year/?category_id=15&section_id=1&mark_id={mark_id}&folder_id={folder_id}"
body_type_url = "https://auto.ru/ajax/form/get_body/?category_id=15&year={year}&mark_id={mark_id}&folder_id={folder_id}"
generations_url = "https://auto.ru/ajax/get_mmm_data/?category_id=15&level=2&type=folder&mark_id={mark_id}&id={folder_id}"
info_url = "https://auto.ru/ajax/filter_modification/?category_id=15&section_id=1&mark_id={mark_id}&folder_id={folder_id}&year={year}"
series_url = "https://auto.ru/ajax/get_mmm_data/?category_id=15&section_id=1&level=1&type=mark&mark_id={mark_id}"


# парсит рубрикатор
class CarSpider(Spider):
    initial_urls = [marks_url]

    def task_initial(self, grab, task):
        current_year = datetime.now().year
        for year in range(1906, current_year + 1):
            Year.objects.get_or_create(year=year)

        if 'result' in grab.doc.json and grab.doc.json['result']:
            marks = grab.doc.json['data']
            for mark_id in marks.keys():
                try:
                    markauto, created = MarkAuto.objects.get_or_create(name=marks[mark_id]['name'])
                    models = model_url.format(mark_id=mark_id)
                    yield Task('models', url=models, mark_id=mark_id, markauto=markauto)
                except DatabaseError as ex:
                    logger.error(ex)

    def task_models(self, grab, task):
        models = grab.doc.json['data']

        if 'folders' in models:
            unique_folders = get_unique_list_by_key(models['folders'], 'id')
            models.update({'folders': unique_folders})

            for model in models['folders']:
                try:
                    modelauto, created = ModelAuto.objects.get_or_create(mark_auto=task.markauto, name=model['name'])
                    generations = generations_url.format(mark_id=task.mark_id, folder_id=model['id'])
                    yield Task('generations', url=generations, modelauto=modelauto, mark_id=task.mark_id, model_id=model['id'])
                except DatabaseError as ex:
                    logger.error(ex)

    def task_generations(self, grab, task):
        generations = grab.doc.json['data']

        if 'items' in generations:
            for generation in generations['items']:
                try:
                    start_year, end_year = generation['start_year_production'], generation['end_year_production']
                    start_year = int(start_year) if is_int(start_year) else None
                    end_year = int(end_year) if is_int(end_year) else None

                    Generation.objects.get_or_create(model_auto=task.modelauto,
                                                     years=(start_year, end_year),
                                                     name=generation['name'])
                    if not end_year:
                        end_year = datetime.now().year
                    body_types = body_type_url.format(year=end_year, mark_id=task.mark_id, folder_id=task.model_id)
                    yield Task('body_types', url=body_types)
                except DatabaseError as ex:
                    logger.error(ex)

    def task_body_types(self, grab, task):
        body_types = grab.doc.json['data']

        if 'body' in body_types:
            for body_type in body_types['body']:
                try:
                    BodyType.objects.get_or_create(name=body_type['name'])
                except DatabaseError as ex:
                    logger.error(ex)


#парсит объявления
class AutoRuParser(ParserThroughRequest, Headers):
    def __init__(self, *args, **kwargs):
        super(AutoRuParser, self).__init__(*args, **kwargs)
        self.text_blocking = 'временно заблокировать доступ'
        self.typeparser = "requests"
        self.pre_set_headers()

    def set_headers(self):
        self.headers['Referer'] = 'https://auto.ru/volgograd/cars/toyota/all/'
        self.headers['Accept'] = 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3'
        self.headers['Cache-Control'] = 'max-age=0'
        self.headers['Sec-Fetch-Mode'] ='navigate'
        self.headers['Sec-Fetch-User'] = '?1'
        self.headers['Upgrade-Insecure-Requests'] = '1'
        self.cookies = {
            "_csrf_token": "74f7e42e1b37f7191b64b18b6eedb49eb2fb2755ce0976d1", 
            "autoru_sid": "a%3Ag5d78f67e2fc1ghtpibvljk8n5gpvlu1.7546ebb24df781f689659e1c0bb34b76%7C1570614906811.604800.0qIn9rNL0sb5hslsK1Ewwg.flMfkzPZWkQsqhvFCgqxBfzhSIJPxJ5o2oOxaisXV9w",
            "autoruuid": "g5d78f67e2fc1ghtpibvljk8n5gpvlu1.7546ebb24df781f689659e1c0bb34b76",
            "suid": "ff301d1245cce3586fbc14d873f0d5b7.147a92f6ba6dc8361f21b754b1a728e6", 
            "counter_ga_all7":"1",
            "X-Vertis-DC": "myt",
            "_ym_wasSynced": "%7B%22time%22%3A1570615264120%2C%22params%22%3A%7B%22eu%22%3A0%7D%2C%22bkParams%22%3A%7B%7D%7D",
            "from": "yandex",
            "yandexuid": "3112308321506346419",
            "my": "YwA%3D",  
            "_ym_uid": "15682085551059184621",
            "ym_d":"1570615423",
            # "ym_visorc_22753222": "w",
            "_ym_isad": "2",
            "af_lpdid": "22:953088520", 
            "_ga": "GA1.2.150830891.1568208979",
            "_gid": "GA1.2.853871669.1570615397",
            "cycada": "f0BdsVB9gi+rOUWihlYt/JmiSNF3M5ecLThUDroBLsg=",
            "from_lifetime": "1570615424812",
            "spravka": "=dD0xNTM2OTA4NDY3O2k9ODUuMjYuMjMyLjE7dT0xNTM2OTA4NDY3MzI1MDY5NzkwO2g9NzVmM2Q0ZWU5M2M5NjBjNjdhM2Y0MjEzNDNlZjQ4OGU="
        }

    def parse_car_info(self):
        data = {}
        xpath = '//div[contains(@class, "CardHead-module__line_1")]/div[contains(@class, "CardHead-module__title")]'
        
        if self.typeparser=="selenium":
            path = self.get_text_from_element_parent(xpath)
            path = path.text.split(' ')
        else:
            path = self.get_text_from_element(xpath)
            path = path.split(' ')

        try:
            mark = path[0]
        except IndexError:
            mark = "";

        try:
            model = path[1]
        except IndexError:
            model = "";

        try:
            generation = path[2]
        except IndexError:
            generation = "";

        data.update({'марка': mark, 'модель': model, 'поколение': generation})
        return data

    def parse_meta_info(self):
        data = {}
        xpath = '//div[contains(@class, "CardHead-module__line_2")]/div[contains(@class, "CardHead-module__info-item")]'
        try:
            date = self.get_text_from_element(xpath + '[1]')
            if date is not None:
                date = dateparser.parse(date)
                data.update({'дата публикации': date})
        except IndexError:
            pass

        try:
            count_of_views = self.get_text_from_element(xpath + '[2]')
            if count_of_views:
                count_of_views = count_of_views.replace(u'\xa0', u' ').split(' ')[0]
                data.update({
                    'количество просмотров': count_of_views
                })
        except IndexError:
            pass

        try:
            number = self.get_text_from_element(xpath + '[3]')
            if number:
                number = number.split(' ')[1]
                data.update({'номер объявления': number})
        except IndexError:
            pass
        
        return data

    def parse_additional_info(self):
        data = {}
        xpath = '//div[contains(@class, "Price-module__caption")]'
        if self.typeparser=="selenium":
            pass
        else:
            try:
                price = self.get_text_from_element(xpath)
                price = price.replace(u'\xa0', u'').replace(' ', '').replace('₽', '')
                price = price.replace(u'\xa0', u'').replace(' ', '').replace('₽', '')
            except AttributeError:
                pass
        try:
            xpath = '//div[contains(@class, "CardOwner-module__name")]'
            if self.typeparser=="selenium":
                pass
            else:
                author = self.get_text_from_element_parent(xpath)
            xpath = '//div[contains(@class, "CardOwner-module__name")]'
            author_type = self.get_text_from_element_parent(xpath)
            if author_type:
                author_type = author_type[author_type.find("(") + 1:author_type.find(")")]
        except AttributeError:
            author = self.get_text_from_element_parent('//a[contains(@class, "salon-link")]')
            author_type = 'юридическое лицо'

        descriptions = self.get_text_from_element_parent('//div[contains(@class, "CardDescription__textInner")]/span')
        description = ""
        for decs in descriptions:
            description += decs.text_content()

        data.update({'автор': author, 'тип автора': author_type, 'комментарий': description, 'цена': price})
        xpath = '//div[contains(@class, "image-gallery-image")]/img'
        if self.typeparser=="selenium":
            pass
        else:
            photos = self.get_text_from_element_parent(xpath)
        urls = []
        for p in photos:
            urls.append("https:" + p.attrib['src'])
        data.update({'фото': urls})
        return data

    def parse_characteristics(self):
        data = {}
        try:
            parameter = '//span[text()="{}"]/following-sibling::span[1]'
            allowed_values = ['Год выпуска', 'Пробег', 'Цвет', 'Двигатель', 'Кузов', 'Коробка', 'Привод',
                              'Руль', 'Состояние', 'Владельцы', 'VIN']
            for label in allowed_values:
                xpath = parameter.format(label)
                if label == 'Двигатель':
                    if self.typeparser=="selenium":
                        pass
                    elif self.typeparser=="requests":
                        value = self.get_text_from_element(xpath)
                        engine = value.split('/')
                    else:
                        value = self.grab.doc.select(xpath).one()
                        engine = value.text().split('/')

                    data.update({'двигатель объем': engine[0].replace(' л', '')})
                    data.update({'двигатель лс': engine[1].replace(' л.с.', '')})
                    data.update({'двигатель топливо': engine[2]})
                elif label == 'Пробег':
                    if self.typeparser=="selenium":
                        pass
                    elif self.typeparser=="requests":
                        km = self.get_text_from_element(xpath)
                    else:
                        km = self.get_text_from_element(xpath)
                    if km:
                        km = km.replace('\xa0', '').replace(' ', '').replace('км', '')
                        data.update({'пробег': km})
                elif label == 'Владельцы':
                    value = self.get_text_from_element(xpath)
                    try:
                        owners = int(re.search(r'\d+', value).group())
                        data.update({'владельцы': owners})
                        data.update({'поколение': owners})
                    except TypeError as ex:
                        logger.warning(ex)
                elif label == 'Год выпуска':
                    if self.typeparser=="selenium":
                        pass
                    elif self.typeparser=="requests":
                        value = self.get_text_from_element_parent(xpath)
                    else:
                        value = self.grab.doc.select(xpath).one()
                    try:
                        value = int(re.search(r'\d+', value).group())
                        data.update({'год выпуска': value})
                    except TypeError as ex:
                        logger.warning(ex)
                else:
                    if self.typeparser=="selenium":
                        pass
                    elif self.typeparser=="requests":
                        value = self.get_text_from_element(xpath)
                    else:
                        value = self.get_text_from_element(xpath)
                    data.update({label.lower(): value})
        except grab.error.DataNotFound as ex:
            logger.error(ex)

        return data

    def send_captcha(self):
        img = self.grab.doc.select('//img[contains(@class, "form__captcha")]').one()
        img_src = img.attr('src')
        captcha_base64 = base64.b64encode(requests.get(img_src).content)

        config = {'connect_timeout': 10, 'timeout': 60, 'cookiefile': self.cookiefile}
        try:
            captcha = AntiGate(settings.ANTICAPTCHA_API, captcha_base64, grab_config=config).captcha_result
            self.grab.doc.set_input_by_id('rep', captcha)
        except AntiGateError as ex:
            logger.warning(ex)

        self.grab.doc.submit()

    def parse_mobile_phone(self):
        encoded_app_config = self.grab.doc.rex_text('(?<=decodeURIComponent\(")(.*)(?=")')
        app_config = json.loads((urllib.parse.unquote(encoded_app_config)))

        if not app_config or 'pageParams' not in app_config or 'crc' not in app_config:
            return {}

        crc = app_config.get('crc')
        sale_id = app_config.get('pageParams').get('sale_id')
        sale_hash = app_config.get('pageParams').get('sale_hash')

        if not crc or not sale_id or not sale_hash:
            return {}

        url = 'https://auto.ru/-/ajax/phones/?category=cars&sale_id={sale_id}' \
              '&sale_hash={sale_hash}&__blocks=antifraud%2Ccard-phones%2Ccall-numbers&' \
              'crc={crc}'.format(sale_id=sale_id, sale_hash=sale_hash, crc=crc)

        self.grab.go(url)
        try:
            raw_phone_number = self.grab.doc.json['blocks']['call-numbers']
            tree = html.fromstring(raw_phone_number)
            raw_phone = tree.xpath('//div[contains(@class, "call-numbers__phone")]')[0].text
            phone = raw_phone.replace('-', '').replace(' ', '')
            return {'телефон': phone}
        except json.decoder.JSONDecodeError as ex:
            logger.error(ex)
            return {}

    def parse_all(self):
        data = {'ссылка': self.url}
        data.update(self.parse_car_info())
        data.update(self.parse_meta_info())
        data.update(self.parse_characteristics())
        data.update(self.parse_additional_info())
        # data.update(self.parse_mobile_phone())
        return data


def autoru_data_to_model(auto_ru_data):
    from auto.glossary import TRANSMISSION_TYPE, ENGINE_CAPACITY, ENGINE_TYPE, DRIVE_TYPE
    from auto.models import Generation, MarkAuto, ModelAuto, AutoRU
    markauto = MarkAuto.objects.filter(name=auto_ru_data['марка']).first()
    if markauto is None:
            markauto = MarkAuto.objects.create(name=auto_ru_data['марка'])

    modelauto = ModelAuto.objects.filter(mark_auto=markauto, name=auto_ru_data['модель']).first()
    if modelauto is None:
            modelauto = ModelAuto.objects.create(mark_auto=markauto, name=auto_ru_data['модель'])
    
    generation = Generation.objects.filter(model_auto=modelauto, name=auto_ru_data['поколение']).first()
    if generation is None:
            Generation.objects.create(model_auto=modelauto, name=auto_ru_data['поколение'], years=(None, None))

    if auto_ru_data.get('кузов'):
        body_type = BodyType.objects.filter(name=auto_ru_data.get('кузов')).first()
        if body_type is None:
            body_type = BodyType.objects.create(name=auto_ru_data.get('кузов'))
    else:
        body_type = None

    if auto_ru_data.get('год выпуска'):
        year = Year.objects.filter(year=auto_ru_data.get('год выпуска')).first()
        if year is None:
            year = Year.objects.create(year=auto_ru_data.get('год выпуска'))
    else:
        year = None

    engine_type = auto_ru_data.get('двигатель топливо').capitalize() if auto_ru_data.get('двигатель топливо') else None
    auto = AutoRU(vin=auto_ru_data.get('vin'),
                  mark_auto=markauto,
                  model_auto=modelauto,
                  generation=generation,
                  year_auto=year,
                  body_type_auto=body_type,
                  transmission_type=get_choice_by_name(TRANSMISSION_TYPE, auto_ru_data.get('коробка')),
                  engine_capacity=get_choice_by_name(ENGINE_CAPACITY,
                                                     auto_ru_data.get('двигатель объем'),
                                                     AutoRU._meta.get_field('engine_capacity').get_default()),
                  engine_type=get_choice_by_name(ENGINE_TYPE, engine_type),
                  horsepower=auto_ru_data.get('двигатель лс'),
                  drive_type=get_choice_by_name(DRIVE_TYPE, auto_ru_data.get('привод')),
                  color_auto=auto_ru_data.get('цвет'),
                  comment=auto_ru_data.get('комментарий'),
                  cost=auto_ru_data.get('цена'),
                  author=auto_ru_data.get('автор'),
                  author_type=get_choice_by_name(AUTHOR_TYPE, auto_ru_data.get('тип автора'),
                                                 AutoRU._meta.get_field('author_type').get_default()),
                  owners=auto_ru_data.get('владельцы'),
                  state=auto_ru_data.get('состояние'),
                  mileage=auto_ru_data.get('пробег'),
                  phone=auto_ru_data.get('телефон'),
                  ad_number=auto_ru_data.get('номер объявления'),
                  public_date=auto_ru_data.get('дата публикации'),
                  view_count=auto_ru_data.get('количество просмотров'),
                  source=auto_ru_data.get('ссылка'))

    photos = []
    for image_url in auto_ru_data['фото']:
        raw_photo = requests.get(image_url)
        uri = ("data:" +
               raw_photo.headers['Content-Type'] + ";" +
               "base64," + base64.b64encode(raw_photo.content).decode('ascii'))
        photos.append(uri)

    model_dict = model_to_dict(auto)
    if auto_ru_data['фото']:
        model_dict.update({'photos': photos})
    return model_dict

