import logging
import re

from django import forms
from django.utils.datastructures import MultiValueDictKeyError


from core.forms import MyModelForm
from .models import Generation, ModelAuto, Auto, MarkAuto, AutoDataBase

logger = logging.getLogger(__name__)

class AutoForm(MyModelForm):
    def __init__(self, *args, **kwargs):
        super(AutoForm, self).__init__(*args, **kwargs)
        self.fields["vin"].required = True
        self.fields["mark_auto"].required = True
        self.fields["year_auto"].required = True
        self.fields["model_auto"].required = True
        try:
            if 'cost' in self.data and self.data['cost'] != '':
                val = int(re.sub(r'\D', '', str(self.data['cost'])))
                data = self.data.copy()
                data['cost'] = val
                self.data = data
        except ValueError as ex:
            logger.error(ex)

        try:
            if 'mileage' in self.data and self.data['mileage'] != '':
                val = int(re.sub(r'\D', '', str(self.data['mileage'])))
                data = self.data.copy()
                data['mileage'] = val
                self.data = data
        except ValueError as ex:
            logger.error(ex)
        if not args and self.instance and self.fields:
            try:
                if 'model_auto' in self.fields and self.data and self.data['mark_auto']:
                    self.fields['model_auto'].queryset = ModelAuto.objects.filter(mark_auto=self.data['mark_auto'])
                else:
                    self.fields['model_auto'].queryset = ModelAuto.objects.filter(mark_auto=self.instance.mark_auto)
            except MultiValueDictKeyError as ex:
                logger.error(ex)
            try:
                if 'generation' in self.fields  and self.data and self.data['model_auto']: 
                    self.fields['generation'].queryset = Generation.objects.filter(model_auto=self.data['model_auto'])
                else:
                    self.fields['generation'].queryset = Generation.objects.filter(model_auto=self.instance.model_auto)
            except MultiValueDictKeyError as ex:
                logger.error(ex)

        if 'year_auto' in self.fields:
            self.fields['year_auto'].queryset = (
                self.fields['year_auto'].queryset.order_by('-year'))

    class Meta:
        model = Auto
        fields = '__all__'


class RecommendedAutoForm(AutoForm):
    def __init__(self, *args, **kwargs):
        super(RecommendedAutoForm, self).__init__(*args, **kwargs)
        self.fields["vin"].required = False
        self.fields["year_auto"].required = False
        self.fields["model_auto"].required = False
        self.fields["sales"].initial = ''


class AutoArticleForm(forms.ModelForm):
    class Meta:
        model = AutoDataBase
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super(AutoArticleForm, self).__init__(*args, **kwargs)
        self.fields['text'].required = True
        self.fields['title'].required = True
        self.fields['mark_auto'].required = True
        self.fields['title'].initial = ''
        self.fields['text'].initial = ''
