from constance import config
from imagekit import ImageSpec
from pilkit.processors import ResizeToFit, ResizeToFill


class OriginalSpec(ImageSpec):
    processors = [ResizeToFit(1300, 1300)]
    format = 'JPEG'
    options = {'quality': 100}
    _options = options

    @property
    def options(self):
        options = self._options

        if self.source.size > 250000 and options['quality'] == 100:
            options['quality'] = 80

        return options


class ThumbnailSpec(ImageSpec):
    format = 'JPEG'
    options = {'quality': 100}

    @property
    def processors(self):
        return [ResizeToFill(config.THUMBNAIL_WIDTH, config.THUMBNAIL_HEIGHT)]


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def get_unique_list_by_key(list_of_dict, key):
    unique_keys = []
    unique_list = []
    for f in list_of_dict:
        if f[key] not in unique_keys:
            unique_list.append(f)
            unique_keys.append(f[key])
    return unique_list


def parse_cars_from_auto_ru():
    from auto.parser import CarSpider
    bot = CarSpider(thread_number=1)
    bot.run()


def get_data_from_auto_ru(url):
    from auto.autoru_parser import AutoRuParser, autoru_data_to_model
    parser = AutoRuParser(url)
    auto_ru_data = parser.parser_connect()

    if auto_ru_data['status'] == 'ok':
         data = autoru_data_to_model(auto_ru_data['data'])
         auto_ru_data.update({'data': data})
    return auto_ru_data

def get_data_from_avito_ru(url):
    from auto.avito_parser import AvitoRuParser, avitoru_data_to_model
    parser = AvitoRuParser(url)
    avito_ru_data = parser.parser_connect()

    if avito_ru_data['status'] == 'ok':
        data = avitoru_data_to_model(avito_ru_data['data'])
        avito_ru_data.update({'data': data})

    return avito_ru_data


def get_choice_by_name(choices, name, default=0):
    if name:
        return next(iter([x[0] for x in choices if name in x]), default)
    return default
