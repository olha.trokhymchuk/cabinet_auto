import logging

from django.db.utils import IntegrityError
from django.db.utils import DataError

from rest_framework import serializers
from auto.models import Auto, Year, BodyType, AutoPhoto, MarkAuto, ModelAuto, Generation
from report.models import ReportPodbor

import auto.glossary as auto_glossary 

logger = logging.getLogger(__name__)

class ReportSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportPodbor
        fields = (
            'id',
        )

class YearSerializer(serializers.ModelSerializer):
    class Meta:
        model = Year
        fields = ('id', 'year')


class BodyTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BodyType
        fields = ('id', 'name')


class MarkAutoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarkAuto
        fields = ('id', 'name')


class ModelAutoSerializer(serializers.ModelSerializer):
    mark_auto = MarkAutoSerializer(required=False, allow_null=True)
    class Meta:
        model = ModelAuto
        fields = ('id', 'name', 'mark_auto')


class GenerationAutoSerializer(serializers.ModelSerializer):
    model_auto = ModelAutoSerializer(read_only=True, many=True)
    class Meta:
        model = Generation
        fields = ('id', 'name', 'model_auto')


class AutoPhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = AutoPhoto
        fields = ['id', 'image', 'embed_url']


class AutoSerializer(serializers.ModelSerializer):
    auto = serializers.SerializerMethodField()
    name = serializers.SerializerMethodField()
    comment_short = serializers.SerializerMethodField()
    body_type_auto = serializers.SerializerMethodField()
    transmission_type = serializers.SerializerMethodField()
    engine_capacity = serializers.SerializerMethodField()
    engine_type = serializers.SerializerMethodField()
    drive_type = serializers.SerializerMethodField()
    author_type = serializers.SerializerMethodField()
    thumbnail = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    report_count = serializers.SerializerMethodField()
    has_non_read_reports = serializers.SerializerMethodField()
    photos_auto = AutoPhotoSerializer(read_only=True, many=True)
    vin = serializers.CharField()
    last_report_id = serializers.SerializerMethodField()
    first_report_mileage = serializers.SerializerMethodField()

    class Meta:
        model = Auto
        depth = 1
        exclude = ( )

    def get_first_report_mileage(self, obj):
        first_report = obj.reportpodbor_set.order_by('-id').first()
        if (first_report):
            return first_report.mileage
        return None

    def validate_vin(self, obj):
        return obj

    def get_comment_short(self, obj):
        if obj.comment:
            info = (obj.comment[:200] + '..') if len(obj.comment) > 200 else obj.comment
            return info
        return ''

    def get_body_type_auto(self, obj):
        if obj.body_type_auto:
            data = {
                'id': obj.body_type_auto.id, 
                'name': obj.body_type_auto.name
            }
            bt = BodyTypeSerializer(data=data)
            bt.is_valid()
            logger.error(bt.data)
            return bt.data
        else:
            try:
                logger.error({'name': obj.data_lk['body_type_auto']})
                return {'name': obj.data_lk['body_type_auto']}
            except KeyError as ex:
                return ''
    def get_last_report_id(self, obj):
        last_report = obj.reportpodbor_set.last()
        if(last_report):
            return last_report.id
        return None

    def get_report_count(self, obj):
        return obj.reportpodbor_set.count()

    def get_has_non_read_reports(self, obj):
        return obj.reportpodbor_set.filter(is_read=False).count()>0 if True else False

    def get_transmission_type(self, obj):
        return 0 if obj.transmission_type == 0 else obj.get_transmission_type_display()

    def get_engine_capacity(self, obj):
        return '' if obj.engine_capacity == '0.0' else obj.get_engine_capacity_display()

    def get_engine_type(self, obj):
        return '' if obj.engine_type == 0 else obj.get_engine_type_display()

    def get_drive_type(self, obj):
        return '' if obj.drive_type == 0 else obj.get_drive_type_display()

    def get_author_type(self, obj):
        return '' if obj.author_type == 0 else obj.get_author_type_display()

    def get_thumbnail(self, obj):
        photo = AutoPhoto.get_avatar(obj.id)
        try:
            if photo and photo.image:
                return photo.image_small.url
            elif photo and photo.embed_url:
                return photo.embed_url
            else:
                return ''
        except (OSError, FileNotFoundError) as ex:
            pass

    def get_image(self, obj):
        try:
            photo = AutoPhoto.get_avatar(obj.id)
        except FileNotFoundError as ex:
            return ''
        return photo.image.url if photo and photo.image else ''

    def get_auto(self, obj):
        name = ''
        if obj.mark_auto and obj.mark_auto.name:
            name = '{} {}'.format(name, obj.mark_auto.name)
        else:
            try:
                name = '{} {}'.format(name, obj.data_lk['mark'])
            except KeyError as ex:
                logger.error(ex)
        if obj.model_auto and obj.model_auto.name:
            name = '{} {}'.format(name, obj.model_auto.name)
        else:
            try:
                name = '{} {}'.format(name, obj.data_lk['model'])
            except KeyError as ex:
                logger.error(ex)
        if obj.generation and obj.generation.name:
            name = '{} {}'.format(name, obj.generation.name)

        return name

    def get_name(self, obj):
        name = self.get_auto(obj)
        if obj.vin:
            name = '{} {}'.format(name, obj.vin)
        return name


class PreviewAutoSerializer(AutoSerializer):
    class Meta:
        model = Auto
        depth = 1
        exclude = ('photos_auto', 'id', 'comment_short', 'thumbnail', 'report_count', 'has_non_read_reports', 'last_report_id',
                   'comment', 'cost', 'author', 'owners', 'state', 'mileage', 'phone', 'source', 'location', 'resell', 'send_autospot',
                   'data_lk', 'recommended', 'sales', 'notifications', 'deleted', 'created', 'modified')


class AutoCreateSerializer(serializers.ModelSerializer):
    auto = serializers.SerializerMethodField()
    photos_auto = AutoPhotoSerializer(read_only=True, many=True)
    mark_auto = MarkAutoSerializer(required=False, allow_null=True)
    model_auto = ModelAutoSerializer(required=False, allow_null=True)
    generation = GenerationAutoSerializer(required=False, allow_null=True)
    vin = serializers.CharField(allow_null=False, max_length=17)
    thumbnail = serializers.SerializerMethodField()
    drive_type = serializers.ChoiceField(choices=auto_glossary.DRIVE_TYPE, required=False, allow_blank=False)
    author_type = serializers.ChoiceField(choices=auto_glossary.AUTHOR_TYPE, required=False,  allow_blank=False)
    engine_type = serializers.ChoiceField(choices=auto_glossary.ENGINE_TYPE, required=False, allow_blank=False)
    engine_capacity = serializers.ChoiceField(choices=auto_glossary.ENGINE_CAPACITY, required=False, allow_blank=False)
    year_auto = YearSerializer(required=False, allow_null=True)
    body_type_auto = BodyTypeSerializer(required=False, allow_null=True)


    def validate_vin(self, obj):
        return obj

    class Meta:
        model = Auto
        depth = 1
        exclude = ('comment',)

    def create_fk(self, instance, validated_data):
        mark_auto = validated_data.pop('mark_auto', None)
        model_auto = validated_data.pop('model_auto', None)
        generation = validated_data.pop('generation', None)
        thumbnail = validated_data.pop('thumbnail', None)
        year_auto = validated_data.pop('year_auto', None)
        body_type_auto = validated_data.pop('body_type_auto', None)
        auto = validated_data.pop('auto', None)
        if not instance:
            try:
                instance = Auto.objects.create(**validated_data)
            except (ValueError, DataError, Auto.MultipleObjectsReturned) as ex:
                print(ex)

        try:
            if self.initial_data['mark_auto']:
                instance.mark_auto = MarkAuto.objects.get(**self.initial_data['mark_auto'])
        except KeyError:
            pass
        try:
            if self.initial_data['model_auto']:
                instance.model_auto = ModelAuto.objects.get(**self.initial_data['model_auto'])
        except KeyError:
            pass
        except ModelAuto.MultipleObjectsReturned:
            self.initial_data['model_auto'].update({"mark_auto__name":self.initial_data['mark_auto']['name']})
            instance.model_auto = ModelAuto.objects.get(**self.initial_data['model_auto'])
        try:
            self.initial_data["generation"].pop("years")
        except (KeyError, AttributeError):
            pass
        try:
            if self.initial_data['generation']:
                generation = Generation.objects.get(name=self.initial_data['generation']['name'],
                                                model_auto__name=self.initial_data['model_auto']['name'])
        except (TypeError, KeyError, Generation.MultipleObjectsReturned, Generation.DoesNotExist):
            pass
        try:
            if self.initial_data['year_auto']:
                instance.year_auto = Year.objects.get(**self.initial_data['year_auto'])
        except KeyError:
            pass
        try:
            if self.initial_data['body_type_auto']:
                instance.body_type_auto = BodyType.objects.get(**self.initial_data['body_type_auto'])
        except KeyError:
            pass
        choices_fied = ["transmission_type","drive_type","author_type","engine_type"]
        for attr, value in validated_data.items():
            if attr in choices_fied and type(value) != int:
                value = 0
            if(attr != "created"):
                setattr(instance, attr, value)
        return instance

    def create(self, validated_data):
        instance=None
        try:
            instance = Auto.objects.get(vin=validated_data["vin"])
        except Auto.DoesNotExist:
            pass
        try:
            instance = self.create_fk(instance, validated_data)
            instance.save()
        except AttributeError:
            pass
        return instance

    def update(self, instance, validated_data):
        instance = self.create_fk(instance, validated_data)
        instance.save()
        return instance

    def get_auto(self, obj):
        name = ''
        try:
            name = '{} {}'.format(name, obj["mark_auto"]["name"])
        except (TypeError, KeyError):
            try:
                name = '{} {}'.format(name, obj.data_lk['mark'])
            except (KeyError, AttributeError) as ex:
                logger.error(ex)
        try:
            name = '{} {}'.format(name, obj["model_auto"]["name"])
        except (TypeError, KeyError):
            try:
                name = '{} {}'.format(name, obj.data_lk['model'])
            except (KeyError, AttributeError) as ex:
                logger.error(ex)
        try:
            name = '{} {}'.format(name, obj["generation"]["name"])
        except (TypeError, KeyError) as ex:
            pass
        self.validated_data["auto"] = name
        return name

    def get_thumbnail(self, obj):
        try:
            return self.initial_data["thumbnail"]
        except KeyError:
            return ""


class AutoFormDataSerializer(serializers.ModelSerializer):
    mark_auto = MarkAutoSerializer()
    
    class Meta:
        model = Auto
        depth = 1
        fields = ('mark_auto','model_auto','generation', )
     

class ExportMobileAutoCreateSerializer(AutoCreateSerializer):
    mark_auto = MarkAutoSerializer(read_only=True, many=True)
    model_auto = ModelAutoSerializer(read_only=True, many=True)
    generation = GenerationAutoSerializer(read_only=True, many=True)


class AutoCreateSerializerByReport(AutoCreateSerializer):
    mark_auto = MarkAutoSerializer(read_only=True, many=True)
    model_auto = ModelAutoSerializer(read_only=True, many=True)
    generation = GenerationAutoSerializer(read_only=True, many=True)
    year_auto = YearSerializer(read_only=True, many=True)
    body_type_auto = BodyTypeSerializer(read_only=True, many=True)