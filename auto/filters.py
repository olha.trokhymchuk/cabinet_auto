import django_filters
from auto.models import Auto, AutoDataBase
from core.filters import ListFilter


class AutoFilter(django_filters.FilterSet):
    body_type_auto = ListFilter()
    color_auto = ListFilter()
    engine_type = ListFilter()
    transmission_type = ListFilter()
    drive_type = ListFilter()
    salon_auto = ListFilter()
    color_salon = ListFilter()
    recommended = django_filters.CharFilter(method='recommended_filter')
    year__gte = django_filters.NumberFilter(
        name='year_auto__year', lookup_expr='gte')
    year__lte = django_filters.NumberFilter(
        name='year_auto__year', lookup_expr='lte')
    mileage__lt = django_filters.NumberFilter(name='mileage', lookup_expr='lt')
    cost__lte = django_filters.NumberFilter(name='cost', lookup_expr='lt')
    engine_capacity__gte = django_filters.NumberFilter(
        name='engine_capacity', lookup_expr='gte')
    engine_capacity__lte = django_filters.NumberFilter(
        name='engine_capacity', lookup_expr='lte')
    horsepower__gte = django_filters.NumberFilter(
        name='horsepower', lookup_expr='gte')
    horsepower__lte = django_filters.NumberFilter(
        name='horsepower', lookup_expr='lte')
    owners__lte = django_filters.NumberFilter(name='owners', lookup_expr='lte')

    class Meta:
        model = Auto
        fields = (
            'vin',
            'mark_auto',
            'model_auto',
            'generation',
            'equipment',
            'source'
        )

    def recommended_filter(self, queryset, name, value):
        if value == "true":
            return queryset.filter(reportpodbor__recommended=True)
        return queryset.filter(reportpodbor__recommended=False)


class AutoDatabaseFilter(django_filters.FilterSet):

    class Meta:
        model = AutoDataBase
        fields = {
            'mark_auto': ['exact'],
            'model_auto': ['exact'],
            'generation': ['exact'],
            'category': ['exact'],
            'title': ['icontains']
        }
        exclude = ['text', ]
