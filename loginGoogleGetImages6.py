import os
import pickle
from google_auth_oauthlib.flow import Flow

# Create the flow using the client secrets file from the Google API
# Console.
from django.conf import settings
os.environ['DJANGO_SETTINGS_MODULE'] = 'autopodbor.settings'
GOOGLE_PHOTO_TOKENS_FOLDER = settings.GOOGLE_PHOTO_TOKENS_FOLDER


flow = Flow.from_client_secrets_file(
    'google_photo/client_secret_google_get_images_6.json',
    scopes=['https://www.googleapis.com/auth/photoslibrary',
          'https://www.googleapis.com/auth/photoslibrary.sharing',
          'https://www.googleapis.com/auth/photoslibrary.readonly'],
    redirect_uri='http://localhost:8000/')

# Tell the user to go to the authorization URL.
auth_url, _ = flow.authorization_url(prompt='consent')

print('Please go to this URL: {}'.format(auth_url))

# The user will get an authorization code. This code is used to get the
# access token.
code = input('Enter the authorization code: ')

flow.fetch_token(code=code)

API_NAME = 'photoslibrary_get_image6'
API_VERSION = 'v1'
pickle_file = 'token_{}_{}.pickle'.format(API_NAME, API_VERSION)

with open(GOOGLE_PHOTO_TOKENS_FOLDER + pickle_file, 'wb') as token:
    pickle.dump(flow.credentials, token)