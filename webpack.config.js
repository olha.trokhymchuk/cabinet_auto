var path = require('path');
var webpack = require('webpack');


module.exports = {
    entry: {
      bus: './vue/vue/bus.js',
      autoedit: './vue/auto/auto-edit.js',
      autolist: './vue/auto/auto-list.js',
      autodetail: './vue/auto/auto-detail.js',
      autodb: './vue/auto/auto-db.js',
      autofilter: './vue/auto/filter.js',
      orderlist: './vue/order/order-list.js',
      orderdetail: './vue/order/order-detail.js',
      orderedit: './vue/order/order-edit.js',
      orderfilter: './vue/order/filter.js',
      orderppklist: './vue/order/order-ppk-list.js',
      ordercarlist: './vue/order/order-car-list.js',
      ordercaradd: './vue/order/order-car-add.js',
      orderreportlist: './vue/order/order-report-list.js',
      ordercommentlist: './vue/order/order-comment-list.js',
      reportlist: './vue/report/report-list.js',
      reportsaleslist: './vue/report/report-sales-list.js',
      reportdetail: './vue/report/report-detail.js',
      ppkedit: './vue/ppk/ppk-edit.js',
      ppkadd: './vue/ppk/ppk-add.js',
      reportadd: './vue/report/report-add.js',
      reportpreview: './vue/report/report-preview.js',
      userlist: './vue/user/user-list.js',
      reviews: './vue/reviews/reviews.js',
      reviewsfilter: './vue/reviews/filters.js',
      userfilter: './vue/user/filter.js',
      useredit: './vue/user/user-edit.js',
      userdetail: './vue/user/user-detail.js',
      userclient: './vue/user/user-client.js',
      filialdetail: './vue/filial/filial-detail.js',
      filiallist: './vue/filial/filial-list.js',
      filialedit: './vue/filial/filial-edit.js',
      messagedetail: './vue/message/message-detail.js',
      messagelist: './vue/message/message-list.js',
      globalsearch: './vue/globalSearch/globalSearch.js',
    },
    output: {
        path: path.join(__dirname, "static/js/"),
    filename: "[name].bundle.js",
    chunkFilename: "[id].chunk.js"
    },
    module: {
        rules: [
            {
                test: /\.js$/,
            }
        ]
    },
    stats: {
        colors: true
    },
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    devtool: 'source-map'
};

console.info(process.env.NODE_ENV);

if (process.env.NODE_ENV === 'production') {
  console.info('start compress');

  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
  ])

  module.exports.optimization = {
    splitChunks: {
      chunks: 'async',
      minSize: 20000,
      minRemainingSize: 0,
      minChunks: 1,
      maxAsyncRequests: 30,
      maxInitialRequests: 30,
      enforceSizeThreshold: 50000,
      cacheGroups: {
        defaultVendors: {
          test: /[\\/]node_modules[\\/]/,
          priority: -10,
          reuseExistingChunk: true,
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
      },
    },
  }
}