import os
import fabtools
import fabtools.require.files as rfiles

PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
DOCS_DIR = os.path.join(PROJECT_DIR)

from fabric.api import *
from fabric.contrib.files import exists


def sudo_project(*args, **kwargs):
    return run(*args, **kwargs)


def manage(cmd):
    # src path should be set by caller
    with cd(env.src_path):
        return sudo_project('%s %s %s' % (
            env.project.python, env.project.manage, cmd))


def update_code(src_path):
    with cd(src_path):
        if env.project.src_branch == "develop2":
            sudo_project('git pull origin develop2')
        else:    
            sudo_project('git pull origin develop')
        sudo_project('git checkout %s' % env.project.src_branch)

@task
def postgresql():
    sudo('wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -')
    fabtools.require.deb.source('pgdg', 'http://apt.postgresql.org/pub/repos/apt/', 'xenial-pgdg', 'main')
    fabtools.require.postgres.server(version='9.6')
    fabtools.require.postgres.user('autopodbor', '2l5g423dp5cjteO')
    fabtools.require.postgres.database('autopodbor', owner='autopodbor')


@task
def build_js(src_path):
    if not fabtools.nodejs.version():
        fabtools.nodejs.install_nodejs()

    with cd(src_path):
        fabtools.nodejs.install_dependencies()
        fabtools.nodejs.install_package('webpack')
        run('webpack')

def python():
    username = env.project.username
    home = '/home/%s' % username
    repo = env.project.src_repo
    path = '%s/%s' % (home, env.project.src_dir)

    # Clean old installation
    if fabtools.files.is_dir(path):
        run('rm -rf %s' % path)

    # Cloning:
    with cd(home):
        sudo_project('git clone -q {repo} {dest}'.format(repo=repo, dest=path))

    # Co needed branch
    with cd(path):
        sudo_project('git checkout %s' % env.project.src_branch)

    with cd(path):
        if env.project.src_branch == "develop2":
            sudo_project('git pull origin develop2 %s' % env.project.src_branch)
        elif env.project.src_branch == "develop_google_api":
            sudo_project('git pull origin develop_google_api %s' % env.project.src_branch)
        else:    
            sudo_project('git pull origin develop %s' % env.project.src_branch)
    # Creating venv
    venv = '%s/%s' % (home, env.project.venv)
    pip = '%s/bin/pip' % venv
    with cd(home):
        if not fabtools.files.is_dir(venv):
            sudo_project('virtualenv --python=python3.5 %s' % venv)
            sudo_project('%s install --upgrade pip' % pip)

        reqs = '%s/%s' % (path, env.project.reqs)
        cache_dir = os.path.join(home, env.project.pip_cache)

        fabtools.require.directory(
            cache_dir,
            use_sudo=False,
            owner=env.project.username,
            group=env.project.username
        )

        sudo_project('{pip} install -r {reqs}'.format(pip=pip, reqs=reqs))

        pers_dirs = env.project.get('persistent_dirs', [])
        process_persistent_dirs(pers_dirs)


def make_links(src_path):
    for link_target, link_path in env.project.links:
        lt = os.path.join(env.project.home, link_target)
        fabtools.require.directory(lt, use_sudo=False, owner=env.project.username, group=env.project.username)
        lp = os.path.join(src_path, link_path)
        sudo_project('ln -s %s %s' % (lt, lp))


def process_persistent_dirs(conf_list):
    def _req_dir(path):
        rfiles.directory(path, use_sudo=False, owner=env.project.username, group=env.project.username)

    def _process(parent, lst):
        for item in lst:
            if isinstance(item, basestring):
                _req_dir(os.path.join(parent, item))
            elif isinstance(item, dict):
                for key, val in item.iteritems():
                    p = os.path.join(parent, key)
                    _req_dir(p)
                    _process(p, val)
            else:
                raise AssertionError(u'Unknown item: %s, class: %s' % (item, item.__class__))
    _process(env.project.home, conf_list)


def make_static(src_path):
    with cd(src_path):
        manage('collectstatic --noinput --ignore=cache '
               '--ignore=upload --ignore=multiuploader_images')


def compress(src_path):
    with cd(src_path):
        manage('compress --force')


def copy_project(src_path):
    copied_src = os.path.join(env.project.home, 'web_new')
    with settings(hide('stdout'), warn_only=True):
        run('rm -rf %s' % copied_src)

    sudo_project('cp -R %s %s' % (src_path, copied_src))
    return copied_src


def migrate_db(src_path):
    with cd(src_path):
        manage('syncdb --noinput')
        manage('migrate')


def migrate_db2(src_path):
    with cd(src_path):
        manage('migrate')


def move_project(src_path):
    pjt = os.path.join(env.project.home, 'autopodbor')
    old = os.path.join(env.project.home, 'web_old')

    if fabtools.files.is_dir(pjt) and fabtools.files.is_dir(old):
        with settings(warn_only=True):
            run('rm -rf %s' % old)

    if fabtools.files.is_dir(pjt):
        with settings(warn_only=True):
            run('mv %s %s' % (pjt, old))

    run('mv %s %s' % (src_path, pjt))

    return pjt, old


def clean_project(old):
    with settings(hide('stdout'), warn_only=True):
        run('rm -rf %s' % old)


def make_persistent():
    pd = env.project.get('persistent_dirs', [])
    process_persistent_dirs(pd)


def restart_service(name):
    if fabtools.service.is_running(name):
        fabtools.service.restart(name)
    else:
        fabtools.service.start(name)


def copy_file(src, dst):
    sudo('cp -f {src} {dst}'.format(src=src, dst=dst))


def build_static(src_path):
    with cd(os.path.join(src_path)):
        sudo_project('npm install')
        sudo_project('npm install npm@latest')
        sudo_project('npm run build')


def load_fixtures(src_path):
    with cd(src_path):
        manage('loaddata ./auto/fixtures/MarkAuto.json')
        manage('loaddata ./auto/fixtures/ModelAuto.json')
        manage('loaddata ./auto/fixtures/Generation.json')
        manage('loaddata ./auto/fixtures/BodyType.json')
        manage('loaddata ./core/fixtures/Group.json')
        # manage('loaddata ./core/fixtures/Site.json')
        manage('loaddata ./notifications/fixtures/MailTemplate.json')
        manage('loaddata ./custom_admin/fixtures/permissions.json')
        manage('loaddata ./report/fixtures/steps.json')

#####################


@task
def full():
    src_path = os.path.join(env.project.home, env.project.src_dir)
    env.src_path = src_path

    python()

    src_path = env.src_path = copy_project(src_path)
    env.project.manage = os.path.join(src_path, 'manage.py')

    make_links(src_path)
    build_static(src_path)
    make_static(src_path)
    if env.type is 'dev':
        sudo_project('cp {} {}'.format(
            os.path.join(src_path, 'deploy', 'assets', 'settings_dev.py'),
            os.path.join(src_path, 'autopodbor', 'local_settings.py'),
        ))

        sudo_project('cp -f {} {}'.format(
            os.path.join(src_path, 'deploy', 'assets', 'gunicorn_dev.conf.py'),
            os.path.join(src_path, 'autopodbor', 'gunicorn_dev.conf.py'),
        ))
    migrate_db2(src_path)
    # load_fixtures(src_path)
    # build_js(src_path)
    # compress(src_path)

    pjt, old = move_project(src_path)

    clean_project(old)


@task
def complete():
    src_path = os.path.join(env.project.home, env.project.src_web)
    env.src_path = src_path
    env.project.manage = os.path.join(src_path, 'manage.py')

    update_code(src_path)

    make_persistent()
    build_static(src_path)
    make_static(src_path)
    migrate_db2(src_path)
    compress(src_path)
    # build_js(src_path)
    load_fixtures(src_path)
