# Dev key
TOKEN = "851335790:AAGBAq9xOmw5J8z2U89aKgpP4btWwwgzPv8"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'autopodbor_dev',
        'USER': 'autopodbor',
        'PASSWORD': '2l5g423dp5cjteO',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

# path to google photo api tokens
GOOGLE_PHOTO_TOKENS_FOLDER = '/home/autodev/google_photo_library_tokens/'

# path to google drive api tokens
GOOGLE_DRIVE_TOKENS_FOLDER = '/home/autodev/google_drive_library_tokens/'

# Stop run command before the upload photos run again (1 minutes 30 sec)
SLEEP_BEFORE_UPLOAD = 90

# Stop run command if not found photos to upload google api (1 hour)
SLEEP_AFTER_UPLOAD = 3600

CELERY_BROKER_URL = 'redis://localhost:6379/2'
CELERY_RESULT_BACKEND = 'redis://localhost:6379/3'
ALLOWED_HOSTS = ['148.251.13.108', 'xn--2-ctbfe.xn----7sbecl2dbcfoo.xn--p1ai', 'xn--b1add.xn----7sbecl2dbcfoo.xn--p1ai', 'localhost', '127.0.0.1','xn--3-ctbfe.xn----7sbecl2dbcfoo.xn--p1ai']