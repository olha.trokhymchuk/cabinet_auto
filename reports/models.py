import datetime
import logging

from django.contrib.sites.models import Site
from django.db import models
from django.db.models import Q
from django.urls import reverse

from django.db.utils import DataError

from imagekit.models import ImageSpecField, ProcessedImageField
from safedelete import HARD_DELETE
from safedelete.models import SafeDeleteModel
from jsoneditor.fields.django_jsonfield import JSONField as JSONField2
from django.contrib.postgres.fields import JSONField

from core.models import User, Filial
from core.utils import CustomFieldHistoryTracker
from notifications.models import Notification
from notifications.utils import send_notification
from order.models import Order
from auto.models import Auto, AutoPhoto
from auto.glossary import NUMBER_OF_HOST_TYPE
from report.utils import OriginalSpec, ThumbnailSpec

import report.report_constants as report_constant

logger = logging.getLogger(__name__)

now = datetime.datetime.now()
year = now.strftime("%Y")
month = now.strftime("%m")
day = now.strftime("%d")
time = now.strftime("%H%M%S")


def upload_auto(instance, filename):
    if len(filename.split('.')) > 2:
        filebase, extension = filename.split(".")[-2:]
    else:
        filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/auto_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_gibdd(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/gibdd_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_fssp(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/fssp_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_zalog(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/zalog_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_mycreditinfo(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/mycreditinfo_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_avtokod(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/avtokod_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_dopserv(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/dopserv_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_photo_vin1(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin1_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_photo_vin2(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin2_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_photo_vin3(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin3_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_photo_vin_table(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin_table_%s.%s" % (year, month, day, instance.id, time, extension)


def upload_photo_vin_glass(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin_glass_%s.%s" % (year, month, day, instance.id, time, extension)


class ReportsPodbor(SafeDeleteModel):
    _safedelete_policy = HARD_DELETE

    REPORT_TYPE = (
        (0, 'Подбор'),
        (1, 'Перепроверка'),
        (2, 'Сдача'),
    )

    STATUS = (
        (0, 'в работе'),
        (1, 'ожидает проверки'),
        (2, 'проверен'),
    )

    is_read = models.BooleanField('Прочитан отчет руководителем', default=True)
    in_recheck = models.BooleanField('На перепроверке', default=False)
    created = models.DateTimeField('Дата создания', auto_now_add=True, db_index=True)
    modified = models.DateTimeField('Последнее изменение', auto_now=True)
    vin = models.CharField('VIN', max_length=17, blank=True, null=True, default=None, db_index=True)
    links = models.CharField('Ссылки на объявления', max_length=1000, blank=True, null=True, default='')
    mileage = models.PositiveIntegerField('Пробег', blank=True, null=True, default=None)
    cost_before = models.PositiveIntegerField('Стоимость до торга', blank=True, null=True, default=None)
    cost_after = models.PositiveIntegerField('Стоимость после торга', blank=True, null=True, default=None)
    cost_estimated = models.PositiveIntegerField('Оценочная стоимость', blank=True, null=True, default=None)
    number_of_hosts = models.IntegerField('Количество по ПТС', blank=True, choices=NUMBER_OF_HOST_TYPE, default=0)
    report_type = models.PositiveSmallIntegerField('Тип', blank=True, choices=REPORT_TYPE, default=0)
    status = models.PositiveSmallIntegerField('Статус', blank=True, choices=STATUS, default=0)
    data = JSONField('Данные', default=dict, blank=True)
    category = models.IntegerField('Категория авто', blank=True, null=True, default=None)
    comment_expert = models.TextField('Комментарий эксперта', blank=True, default='')
    comment_manager = models.TextField('Комментарий руководителя', blank=True, default='')
    result_expert = models.TextField('Вывод эксперта', blank=True, default='')
    pluses = models.TextField('Плюсы', blank=True, default='')
    minuses = models.TextField('Минусы', blank=True, default='')

    recommended = models.NullBooleanField('Рекомендован', blank=True, null=True)
    published = models.NullBooleanField('Опубликован', blank=True, null=True)

    lk_id = models.PositiveIntegerField('Id со старого лк', blank=True, null=True, default=None)
    auto = models.ForeignKey(Auto, verbose_name='Авто', null=True, blank=True, db_index=True)
    executor = models.ForeignKey(User, blank=True, null=True, verbose_name='Исполнитель')

    class Meta:
        verbose_name = "отчет"
        verbose_name_plural = "отчеты"
        ordering = ["-created"]

    def auto_add_photo_from_report(self, photo):
        try:
            auto_photo = AutoPhoto(auto_id=self.auto.id, image=photo['image'])
            auto_photo.save()
        except AttributeError as ex:
            pass

    def save(self, *args, **kwargs):
        created = False if self.id else True
        old_obj = ReportsPodbor.all_objects.get(
            id=self.id) if not created else None

        super(ReportsPodbor, self).save(*args, **kwargs)

        try:
            self.auto.photos_auto.all().values()[0]['id']
        except (KeyError, IndexError, AttributeError) as ex:
            try:
                photo_report = self.photos_auto.all().values()[0]
            except IndexError as ex:
                print(ex)
            else:
                self.auto_add_photo_from_report(photo_report)
                print(ex)
        try:
            if self.published and not old_obj.published:
                orders = self.order.all()
                for order in orders:
                    data = {
                        'order_id': order.number_buh,
                        'order_url': self.get_full_url()
                    }
                    send_notification([order.client], Notification.NEW_REPORT, data=data, channels=[
                                      'email', 'sms', 'lk'])
        except AttributeError as ex:
            logger.error(ex)

        if not created:
            recheck = self.report_type != old_obj.report_type and \
                self.get_report_type_display() == 'перепроверка'

            closing = self.report_type != old_obj.report_type and \
                self.get_report_type_display() == 'сдача'

            if recheck and self.executor:
                orders = self.order.all()
                for order in orders:
                    data = {
                        'order_id': order.number_buh,
                        'order_url': self.get_full_url()
                    }

                    send_notification(
                        [self.executor], Notification.CHECK_ORDER, data=data, channels=['lk'])

            if closing:
                orders = self.order.all()
                recipients = User.objects.filter(Q(filials__in=self.executor.filials.all(), groups__name='Бухгалтер') | Q(
                    filials__in=self.executor.filials.all(), groups__name='Контент-менеджер'))
                for order in orders:
                    data = {'order_id': order.number_buh}
                    send_notification(
                        recipients, Notification.CLOSE_ORDER, data=data, channels=['lk'])

            try:
                filials = self.executor.filials.all()
            except AttributeError:
                pass
            else:
                mark = None
                try:
                    mark = self.auto.mark_auto.name
                except AttributeError as ex:
                    logger.error(ex)
                model = None
                try:
                    model = self.auto.model_auto.name
                except AttributeError as ex:
                    logger.error(ex)

                data = {
                    'order_url_name': '{}'.format(self.get_full_url()),
                    'mark': mark,
                    'model': model
                }

                if self.recommended and not old_obj.recommended:
                    for filial in filials:
                        for lider in filial.leaders:
                            logger.error(lider)
                            send_notification(
                                [lider], Notification.ADVICE_REPORT, data=data, channels=['email', 'lk'])
                if not self.recommended and closing:
                    for filial in filials:
                        for lider in filial.leaders:
                            send_notification(
                                [lider], Notification.NOT_ADVICE_REPORT, data=data, channels=['lk'])

    def __str__(self):
        # return self.id
        return self.auto.__str__() if self.auto else "{}".format(self.id)

    def get_absolute_url(self):
        return reverse('reports:report_view', kwargs={'report_id': self.id})

    def get_full_url(self):
        host = Site.objects.get_current().domain
        return 'https://{host}{abs_url}'.format(host=host, abs_url=self.get_absolute_url())


class ReportsDamage(models.Model):
    title = models.CharField('Название', max_length=255,
                             blank=True, null=False, default=None)
    slug = models.CharField('Обозначение', max_length=255,
                            blank=True, null=False, default=None)
    default_val = models.TextField(
        'Значение по умолчанию', blank=True, null=True, default=None)
    type = models.CharField('Тип', blank=True, max_length=255, default='')
    block = models.CharField(
        'Отнести к блоку', max_length=255, blank=True, default='')
    parent_id = models.ForeignKey(
        "self", blank=True, null=True, verbose_name='Относится к пункту')
    position = models.PositiveIntegerField(
        'Поззиция', blank=True, default=None)


class ReportsDamagePivot(models.Model):
    damage_id = models.ForeignKey(
        ReportsDamage, blank=True, null=True, verbose_name='Повреждение')
    report_id = models.ForeignKey(
        ReportsPodbor, blank=True, null=True, verbose_name='Отчет')
    is_check = models.NullBooleanField(
        'Статуст проверки', blank=True, null=True)
    val = models.TextField('Значение', blank=True, null=True, default=None)

    comment = models.TextField(
        'Комментарий', blank=True, null=True, default=None)


class ReportsSteps(models.Model):
    data = JSONField2(verbose_name="шаги", null=True, blank=True)

    class Meta:
        verbose_name = "Шаги отчета"
        verbose_name_plural = "Шаги отчета"


class ReportsPhoto(models.Model):
    photo_type = models.PositiveSmallIntegerField(
        'Тип', blank=True, choices=report_constant.PHOTO_TYPE, default=0)
    report_podbor = models.ForeignKey(
        ReportsPodbor, blank=True, null=True, default=None, verbose_name='Отчёт', related_name='photos_auto')
    embed_url = models.URLField('embed url', max_length=255, default='')
    image = ProcessedImageField(upload_to=upload_auto, blank=True, null=True,
                                verbose_name='Фото (несколько)', spec=OriginalSpec, autoconvert=None, max_length=250)
    image_small = ImageSpecField(source='image', spec=ThumbnailSpec)
    from_inspection = models.BooleanField(
        'С места осмотра', blank=True, default=False)

    class Meta:
        verbose_name = "Фото отчета"
        verbose_name_plural = "Фото отчетов"


class LeasePhoto(models.Model):
    lease_podbor = models.ForeignKey(ReportsPodbor, blank=True, null=True,
                                     default=None, verbose_name='Отчёт со сдачи', related_name='surrphotos_auto')
    image = ProcessedImageField(upload_to=upload_auto, blank=True, null=True,
                                verbose_name='Фото (несколько)', spec=OriginalSpec, autoconvert=None)
    image_small = ImageSpecField(source='image', spec=ThumbnailSpec)
    from_inspection = models.BooleanField(
        'со сдачи', blank=True, default=False)

    class Meta:
        verbose_name = "Фото со сдачи"
        verbose_name_plural = "Фото со сдачи"
