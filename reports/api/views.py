import base64
import io
import json
import re
import logging

import auto.glossary as auto_glossary
from auto.models import AutoPhoto

from django.conf import settings
from django.db.models import Q

from django.contrib.auth.decorators import login_required, permission_required

from django.http import JsonResponse

from rest_framework.generics import  ListCreateAPIView
from rest_framework.views import APIView

from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from report.models import ReportsPodbor, ReportsPhoto
from report.serializers import ReportListSerialializer, ReportSerializerV3, ReportSerializerCreateUpdateV3
from rest_framework import serializers
from .pagination import LimitOffsetPagination, PageNumberPagination

from core.models import User
from core.api_serializers import UserSerializer
import report.report_constants as report_constant

logger = logging.getLogger(__name__)


class ApiReportByUserList(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = ReportsPodbor.objects.all()
    serializer_class = ReportListSerialializer
    pagination_class = PageNumberPagination

    def list(self, request, pk):
        user = User.objects.get(pk=pk)
        queryset = self.get_queryset()

        if user.is_client:
            queryset = queryset.filter(order__client=user).distinct()
        else:
            queryset = queryset.filter(Q(executor=user) |
                                    Q(order__expert=user) |
                                    Q(order__expert_check=user) |
                                      Q(order__transferring=user) |
                                    Q(order__operator=user))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ApiReportByFilialList(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = ReportsPodbor.objects.all()
    serializer_class = ReportListSerialializer
    pagination_class = PageNumberPagination

    def list(self, request, pk):
        queryset = self.get_queryset()
        queryset = queryset.filter(executor__filials__id=pk).distinct()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ReportDetaiApi(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    
    def queryset_by_user(self):
        queryset = ReportsPodbor.objects.all()
        if self.request.user.is_client:
            queryset = queryset.filter(order__client=self.request.user)
        try:
            queryset = queryset.get(id=self.kwargs.get('report_id'))
            return queryset
        except ReportsPodbor.DoesNotExist:
            raise Http404

    def get(self, request, report_id, format=None):
        report = self.queryset_by_user()
        serializer = ReportSerializerV3(report)
        return Response(serializer.data)

    def put(self, request, report_id=None):
        cusmom_error = False
        if report_id:
            saved_report = self.queryset_by_user()
        else:
            saved_report = None
        auto_set_or_create = False
        data = json.loads(request.POST["data"])
        try:
            for field in ["mileage", "cost_before"]:
                number = ''.join(re.findall(r'\d+', data["auto"][field]))
                data["auto"][field] = number
        except (KeyError, TypeError) as ex:
            pass
        deleted = request.POST.get("deleted", None)
        logger.error("deleted")
        logger.error(deleted)
        logger.error("deleted")
        if deleted:
            for delet in json.loads(deleted):
                try:
                    ReportsPhoto.objects.get(pk=delet).delete()
                except ReportsPhoto.DoesNotExist as ex:
                    print(ex)
        try:
            for item in auto_glossary.choise_data:
                try:
                    data["auto"][item] = auto_glossary.choise_data[item][str(data["auto"][item]).lower()]
                except (KeyError, TypeError):
                    pass
        except KeyError:
            pass
        mark_auto = None
        try:
            if data["auto"]["mark_auto"]:
                mark_auto = data["auto"]["mark_auto"]
                data["auto"]["mark_auto"] = {}
            else:
                data["auto"]["mark_auto"] = None
        except (KeyError, TypeError) as ex:
            pass
        if mark_auto:
            try:
                data["auto"]["mark_auto"].update({"name":mark_auto["name"]})
            except (KeyError, TypeError) as ex:
                data["auto"]["mark_auto"].update({"name":mark_auto})
                logger.error(ex)
            try:
                data["auto"]["mark_auto"].update({"id": mark_auto["id"]})
            except (KeyError, TypeError) as ex:
                logger.error(ex)
        model_auto = None
        try:
            if data["auto"]["model_auto"]:
                model_auto = data["auto"]["model_auto"]
                data["auto"]["model_auto"] = {}
            else:
                data["auto"]["model_auto"] = None
        except (KeyError, TypeError) as ex:
            pass
        if model_auto:
            try:
                data["auto"]["model_auto"].update({"name": model_auto["name"]})
            except (KeyError, TypeError) as ex:
                data["auto"]["model_auto"].update({"name": model_auto})
                logger.error(ex)
            try:
                data["auto"]["model_auto"].update({"mark_auto__name": mark_auto["name"]})
            except (KeyError, TypeError) as ex:
                data["auto"]["model_auto"].update({"mark_auto__name": mark_auto})
                logger.error(ex)
        generation = None
        try:
            if data["auto"]["generation"]:
                generation = data["auto"]["generation"]
                data["auto"]["generation"] = {}
            else:
                data["auto"]["generation"] = None
        except (KeyError, TypeError) as ex:
            pass
        if generation:
            try:
                data["auto"]["generation"].update({"name": generation["name"]})
            except (KeyError, TypeError):
                data["auto"]["generation"].update({"name": generation})
                pass
            try:
                data["auto"]["generation"].update({"model_auto__name": model_auto["name"]})
            except (KeyError, TypeError):
                data["auto"]["generation"].update({"model_auto__name": model_auto})
                pass
        try:
            logger.error(data["auto"]["engine_capacity"])
            data["auto"]["engine_capacity"] = str(data["auto"]["engine_capacity"]).split()[0]
        except (KeyError, TypeError, IndexError, AttributeError):
            pass
        try:
            logger.error(data["executor"].pop("date_joined"))
            logger.error(data["executor"].pop("avatar"))
        except (KeyError, TypeError, IndexError, AttributeError):
            pass
        serializer = ReportSerializerCreateUpdateV3(instance=saved_report, data=data)
        is_valid = serializer.is_valid()
        errors_report = serializer.errors
        logger.error("errors_report")
        logger.error(errors_report)
        logger.error("errors_report")
        try:
            exclude = ["vin"]
            for item in  errors_report["auto"]:
                if item not in exclude:
                    data["auto"].pop(item, None)
            errors_report.pop("auto") 
            cusmom_error = True
        except KeyError:
            pass
        try:
            for item in  errors_report:
                if type(errors_report[item]) != dict and "A valid integer is required." in errors_report[item]:
                    data[item] = int(''.join(re.findall(r'\d+', data[item])))
            cusmom_error = True
        except KeyError:
            pass 
        if cusmom_error:
            serializer = ReportSerializerCreateUpdateV3(instance=saved_report, data=data)
            is_valid = serializer.is_valid()
        if not is_valid:
            raise serializers.ValidationError(serializer.errors)
        if saved_report:
            report_saved = serializer.update(saved_report, data)
        else:
            report_saved = serializer.create(data)
        collections_photo = {}
        type_list = []
        if deleted:
            for photo in ReportsPhoto.objects.filter(report_podbor=report_saved):
                try:
                    image = photo.image.url
                    image_small = photo.image_small.url
                    collections_photo[report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]].append({"image": image, "image_small": image_small})
                except (KeyError):
                    collections_photo[report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]] = []
                    collections_photo[report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]].append({"image": image, "image_small": image_small})
                except (ValueError,FileNotFoundError) as ex:
                    logger.error(ex)
            serializer.validated_data.update({"photos": collections_photo})

        if report_saved.auto and report_saved.auto.pk:
            auto_set_or_create = True
        if is_valid:
            return Response({"success": "Report '{}' updated successfully".format(report_saved.pk, auto_set_or_create),
                            "auto_set_or_create": auto_set_or_create, "report": serializer.validated_data, 
                            "id": report_saved.pk, "errors": errors_report
                    })
        else:
            return Response({"success": "Report '{}' updated unsuccessfully".format(report_saved.pk, auto_set_or_create),
                            "auto_set_or_create": auto_set_or_create, "report": serializer.validated_data, 
                            "id": report_saved.pk, "errors": errors_report
                    })

# Добавление фото к отчету
@login_required
@permission_required('report.change_reportpodbor', raise_exception=True)
def add_photo(request, report_id):
    if request.method == 'POST':
        thumbnail = AutoPhoto.objects.filter(auto__reportpodbor__id=report_id).first()
        photo_types = []
        try:
            response = {"thumbnail": thumbnail.image_small.url}
        except AttributeError:
            response = {}
        for photo_type in request.FILES:
            files = request.FILES.getlist(photo_type)
            for file in files:
                photo_type_name = photo_type[:-2]
                photo = ReportsPhoto.objects.create(photo_type=report_constant.type_photo[photo_type_name], report_podbor_id=report_id)
                try:
                    photo.image = file
                    photo.save()
                    photo.image_small.generate()
                    created_photo = {"image_small": photo.image_small.url,
                        "image": photo.image.url,"photo_type": photo_type_name}
                    photo_types.append(created_photo)
                except ValueError as ex:
                    photo.delete()
                    return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        response.update({"photos":photo_types})
        return JsonResponse(response, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_404_NOT_FOUND)
