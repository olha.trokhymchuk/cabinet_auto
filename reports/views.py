import json
import logging
import weasyprint
from django.template.loader import get_template

from deepdiff import DeepDiff
from django.contrib.auth.mixins import PermissionRequiredMixin, LoginRequiredMixin
from django.db.models import Q, Max, Min, Count
from django.core import serializers
from django.http import JsonResponse, Http404, HttpResponse
from django.shortcuts import HttpResponseRedirect, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import CreateView, UpdateView, DetailView
from django.views.generic.base import TemplateView
from field_history.models import FieldHistory
from field_history.tracker import get_serializer_name
from safedelete import SOFT_DELETE_CASCADE

from django.conf import settings

from auto.forms import AutoForm
from auto.glossary import *
from auto.views import AutoListView
from core.models import Filial, User
from core.serializers import FilialSerializerPure, UserSerializer
from core.utils import ImprovedListView
from custom_admin.utils import FilteredFieldsMixin, FilteredFieldContextMixin
from notifications.models import Notification
from notifications.utils import send_notification

import report.report_constants as report_constant
from report.filters import ReportFilter
from report.serializers import ReportListSerialializer, ReportSerializer, ReportSerializerV2
from .models import ReportsPodbor, ReportsPhoto, ReportsSteps, LeasePhoto

from auto import utils
from auto.models import Auto, BodyType, MarkAuto, ModelAuto, Generation, AutoPhoto
from .forms import PodborForm
from order.models import Order

import os
from django.template.loader import get_template
from django.template import Context, Template
from weasyprint import HTML, CSS

logger = logging.getLogger(__name__)

def link_callback(uri, rel):
    sUrl = settings.STATIC_URL
    mUrl = settings.MEDIA_URL
    if uri.startswith(sUrl) or uri.startswith(mUrl):
        path = settings.BASE_DIR + uri
    else:
        return uri
    if not os.path.isfile(path):
            raise Exception(
                'media URI must start with %s or %s' % (sUrl, mUrl)
            )
    return path

def img_url_fetcher(url):
    if url.startswith('file:') and url.find(settings.BASE_DIR) < 0 :
        return weasyprint.default_url_fetcher('file://'+settings.BASE_DIR+url[5:])
    return weasyprint.default_url_fetcher(url)

def render_to_pdf(request):
    html = request.POST.get('html')
    pdf_file = HTML(string=html,url_fetcher=img_url_fetcher,base_url='').write_pdf(stylesheets=[CSS(settings.BASE_DIR +  '/static/css/report_view.css')],zoom=1.1)
    response = HttpResponse(pdf_file, content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="report.pdf"'
    return response

# Список отчетов
class ReportList(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView):
    permission_required = 'report.view_reportpodbor'
    raise_exception = True
    model = ReportsPodbor
    paginate_by = settings.PAGINATE_BY
    template_name = 'report/report_list.html'
    filterset_class = ReportFilter
    search_fields = ['auto__vin', 'auto__source', 'id']
    serializer = ReportListSerialializer

    def get_queryset(self):
        user = self.request.user
        reports_without_orders = ReportsPodbor.objects.annotate(num_orders=Count('order')).filter(num_orders=0)
        reports = ReportsPodbor.objects.filter(Q(order__client__filials__in=user.filials.all()) |
                                              Q(id__in=reports_without_orders))
        return reports.distinct()

    def get_context_data(self, **kwargs):
        context = super(ReportList, self).get_context_data(**kwargs)
        context.update({'html_title': 'Список отчетов'})
        return context


# Список отчетов по автомобилю
class ReportsByCar(ReportList):
    def get_queryset(self):
        # queryset = super(ReportsByCar, self).get_queryset()
        if 'car_id' in self.kwargs:
            queryset = ReportsPodbor.objects.filter(auto=self.kwargs['car_id'])
        if 'slug' in self.kwargs:
            queryset = ReportsPodbor.objects.filter(auto__vin=self.kwargs['slug'])
        return queryset.distinct()


# Добавление авто для отчета по ссылке
@login_required
@permission_required('report.change_reportpodbor', raise_exception=True)
def add_auto_from_auto_ru(request, report_id=None):
    if request.POST:
        url = request.POST.get('url')
        from auto.utils import get_data_from_auto_ru
        auto_ru_data = get_data_from_auto_ru(url)

        if auto_ru_data['status'] == 'error':
            return HttpResponse('', status=500)
        else:
            report = get_object_or_404(ReportsPodbor, id=report_id) if report_id else ReportsPodbor()
            auto = get_object_or_404(Auto, id=auto_ru_data.get('data').get('id'))
            report.auto = auto
            report.save()
            redirect_to = reverse('report:report_edit', args=(report.id,))
            return redirect(redirect_to)

    raise Http404


# Редактировать автомобиль отчета
@login_required
@permission_required('order.change_order', raise_exception=True)
def report_set_auto(request, report_id=None):
    if request.POST:
        report = get_object_or_404(ReportsPodbor, id=report_id) if report_id else ReportsPodbor()
        form = AutoForm(request.POST)
        if form.is_valid():
            auto = form.save()
            report.auto = auto
            report.save()
            redirect_to = reverse('report:report_edit', args=(report.id,))
            redirect_to += '?start=1'
            return JsonResponse(status=200, data={'auto_id': auto.id, 'redirect': redirect_to})
        else:
            return JsonResponse(status=200, data={'errors': form.errors})

    raise Http404


# Просмотр отчета
class ReportWithoutPermissionView(DetailView, FilteredFieldContextMixin):
    permission_required = 'report.view_reportpodbor'
    raise_exception = True
    model = ReportsPodbor
    template_name = 'report/report-view.html'
    context_object_name = 'report'

    def get_engine_type(self, engine_type):
        return ENGINE_TYPE[engine_type][1] if isinstance(engine_type, int) else engine_type

    def get_transmission_type(self,transmission_type):
        return TRANSMISSION_TYPE[transmission_type][1] if isinstance(transmission_type, int) else transmission_type

    def get_drive_type(self, drive_type):
        return DRIVE_TYPE[drive_type][1] if isinstance(drive_type, int) else drive_type

    def get_context_data(self, **kwargs):
        context = super(ReportWithoutPermissionView, self).get_context_data(**kwargs)
        photos = ReportsPhoto.objects.filter(report_podbor=self.object)
        surrphotos = LeasePhoto.objects.filter(surrender_podbor=self.object)
        auto_photos = AutoPhoto.objects.filter(auto=self.object.auto)
        try:
            transmission_type = self.get_transmission_type(self.object.auto.transmission_type)
        except AttributeError:
            transmission_type = None
        try:

            engine_type = self.get_engine_type(self.object.auto.engine_type)
        except AttributeError:
            engine_type = None
        try:
            salon_auto = BODY_TYPE[self.object.auto.salon_auto][1] if isinstance(self.object.auto.salon_auto, int) else self.object.auto.salon_auto
        except AttributeError:
            salon_auto = None
        try:
            drive_type = self.get_drive_type(self.object.auto.drive_type)
        except AttributeError:
            drive_type = None
        year_auto = ''
        try:
            year_auto = self.object.auto.year_auto
        except AttributeError as ex:
            logger.error(ex)

        context.update({
            'html_title': 'Отчет {}'.format(self.object),
            'photos': photos,
            'surrphotos': surrphotos,
            'auto_photos': auto_photos,
            'transmission_type': transmission_type,
            'engine_type': engine_type,
            'salon_auto': salon_auto,
            'drive_type': drive_type,
            'year_auto': year_auto
        })
        return context

    def get_object(self, queryset=None):
        queryset = self.get_queryset()
        if self.request.user.is_client:
            queryset = queryset.filter(order__client=self.request.user)
        try:
            queryset = queryset.get(id=self.kwargs.get('report_id'))
            return queryset
        except ReportsPodbor.DoesNotExist:
            raise Http404

    def get(self, request, **kwargs):
        if request.is_ajax():
            data = ReportSerializer(self.get_object()).data
            return JsonResponse(data=data)
        return super(ReportWithoutPermissionView, self).get(request, **kwargs)


class PermissionDemo(PermissionRequiredMixin):
    def get_object(self, queryset=None):
        queryset = self.get_queryset()

        try:
            queryset = queryset.get(id=self.kwargs.get('report_id'))
            return queryset
        except ReportsPodbor.DoesNotExist:
            raise Http404

    def get(self, request, **kwargs):
        if not self.request.user and int(self.kwargs['report_id']) != 14586:
           raise Http404

        if request.is_ajax():
            data = ReportSerializerV2(self.get_object()).data
            return JsonResponse(data=data)
        return super(PermissionDemo, self).get(request, **kwargs)

    def dispatch(self, request, *args, **kwargs):
        if self.request.user.is_anonymous and int(self.kwargs['report_id']) != 14586 and not self.has_permission():
            return self.handle_no_permission()
        return self.get(request, **kwargs)

class ReportView(LoginRequiredMixin, PermissionRequiredMixin, ReportWithoutPermissionView):
    permission_required = 'report.view_reportpodbor'
    raise_exception = True
    model = ReportsPodbor
    template_name = 'report/report-view.html'
    context_object_name = 'report'


class ReportViewV2(ReportView):
    template_name = 'report/report-view-new3.html'

    def get(self, request, **kwargs):
        if self.request.user.is_leader:
            report = ReportsPodbor.objects.get(id=int(self.kwargs['report_id']))
            report.is_read = True
            report.save()
        return super(ReportView, self).get(request, **kwargs)


class GuidViewV2(PermissionDemo, ReportWithoutPermissionView):
    permission_required = 'report.view_reportpodbor'
    raise_exception = True
    model = ReportsPodbor
    template_name = 'report/report-view-demo.html'
    context_object_name = 'report'

    def get(self, request, **kwargs):
        if int(self.kwargs['report_id']) != 14586:
           raise Http404

        if request.is_ajax():
            data = ReportSerializerV2(self.get_object()).data
            return JsonResponse(data=data)
        return super(GuidViewV2, self).get(request, **kwargs)

    def get_object(self, queryset=None):
        queryset = self.get_queryset()

        try:
            queryset = queryset.get(id=self.kwargs.get('report_id'))
            return queryset
        except ReportsPodbor.DoesNotExist:
            raise Http404


class ReportViewV2Api(PermissionDemo, ReportWithoutPermissionView):
    template_name = 'report/report-view-new3.html'

    def get(self, request, **kwargs):
        data = ReportSerializerV2(self.get_object()).data
        return JsonResponse(data=data)


class ReportViewV3Api(PermissionDemo, Template):
    template_name = 'report/report-view-new3.html'


class ReportCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView, FilteredFieldsMixin):
    permission_required = 'report.add_reportpodbor'
    raise_exception = True
    model = ReportsPodbor
    form_class = PodborForm
    template_name = 'report/report-add.html'
    action = 'create'

    def get_success_url(self):
        success_url = reverse("report:report_edit", args=(self.object.id,))
        success_url += '?start=1'
        return success_url

    def form_valid(self, form):
        super(ReportCreateView, self).form_valid(form)
        self.object.executor = self.request.user
        self.object.save()
        if self.request.is_ajax():
            return JsonResponse(status=200, data={'redirect': self.get_success_url()})

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        super(ReportCreateView, self).form_invalid(form)
        return JsonResponse(status=200, data={'errors': form.errors})

    def get_initial(self):
        initial = super(ReportCreateView, self).get_initial()
        initial['auto'] = self.kwargs.get('auto_id')
        initial['order'] = self.kwargs.get('order_id')
        return initial

    def get_context_data(self, **kwargs):
        context = super(ReportCreateView, self).get_context_data(**kwargs)
        report_types = [{'id': value, 'text': text} for value, text in ReportsPodbor.REPORT_TYPE]
        numbers_of_hosts = [{'id': value, 'text': text} for value, text in NUMBER_OF_HOST_TYPE[1:]]
        context.update({
            'html_title': 'Добавление отчета',
            'auto_edit_title': 'Добавление автомобиля',
            'order_id': self.kwargs.get('order_id'),
            'action_for_manual_add': reverse('report:add_auto_manual'),
            'action_for_add_from_auto_ru': reverse('report:add_auto_from_auto_ru'),
            'form': AutoForm(),
            'report_form': self.get_form(),
            'report_types': json.dumps(report_types),
            'numbers_of_hosts': json.dumps(numbers_of_hosts)
        })

        return context

    def get(self, request, **kwargs):
        if request.is_ajax():
            return JsonResponse(data={})
        return super(ReportCreateView, self).get(request, **kwargs)

    def post(self, request, **kwargs):
        return super(ReportCreateView, self).post(request, **kwargs)


# добавление отчета для автомобиля
@login_required
@permission_required('report.add_reportpodbor', raise_exception=True)
def add_report_for_auto(request, auto_id, order_id=False):
    order = None
    if request.method == 'POST':
        if order_id and isinstance(int(order_id), int):
            order = get_object_or_404(Order, id=order_id)
        auto = get_object_or_404(Auto, id=auto_id)
        report = ReportsPodbor.objects.create(auto=auto, executor=request.user)
        if order:
            report.order.add(order)
        redirect_to = reverse('report:report_edit', args=(report.id,))
        redirect_to += '?start=1'
        return JsonResponse(data={'redirect': redirect_to}) if request.is_ajax() else HttpResponseRedirect(redirect_to)
    raise Http404


# Редактирование отчета
class ReportUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView, FilteredFieldsMixin):
    permission_required = 'report.change_reportpodbor'
    raise_exception = True
    model = ReportsPodbor
    form_class = PodborForm
    action = 'update'
    context_object_name = 'report'
    template_name = 'report/report-add.html'

    def save_history(self, old_data):
        field_histories = []
        difference = DeepDiff(old_data, self.object.data, view='tree')
        if difference and difference.get('values_changed'):
            for value in list(difference.get('values_changed')):
                try:
                    label = parent.t1.get('label')
                    new_value = value.t2
                    setattr(self.object, label, new_value)
                    data = serializers.serialize(get_serializer_name(),
                                                 [self.object],
                                                 fields=[label])
                    history = FieldHistory(
                        object=self.object,
                        field_name=label,
                        serialized_data=data,
                        user=self.request.user,
                    )
                    field_histories.append(history)
                except (AttributeError, NameError) as ex:
                    pass
            if field_histories:
                FieldHistory.objects.bulk_create(field_histories)

    def get_object(self, queryset=None):
        report = get_object_or_404(ReportsPodbor, id=self.kwargs['report_id'])
        return report

    def form_valid(self, form):
        #self.object.executor = self.request.user
        old_data = self.get_object().data
        if self.request.POST.get('data'):
            self.object.data = json.loads(self.request.POST.get('data'))
        self.object.save()
        self.save_history(old_data)

        if self.request.is_ajax():
            return JsonResponse(status=200, data={'redirect': ''})

        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        super(ReportUpdateView, self).form_invalid(form)
        return JsonResponse(status=200, data={'errors': form.errors})

    def get_initial(self):
        some = super(ReportUpdateView, self).get_initial()
        return some

    def get_context_data(self, **kwargs):
        context = super(ReportUpdateView, self).get_context_data(**kwargs)
        report_types = [{'id': value, 'text': text} for value, text in ReportsPodbor.REPORT_TYPE]
        numbers_of_hosts = [{'id': value, 'text': text} for value, text in NUMBER_OF_HOST_TYPE[1:]]
        context.update({
            'html_title': 'Редактирование отчёта',
            'auto_edit_title': 'Добавление автомобиля',
            'report_id': self.object.id,
            'action_for_manual_add': reverse('report:add_auto_manual', args=(self.kwargs.get('report_id'),)),
            'action_for_add_from_auto_ru': reverse('report:add_auto_from_auto_ru', args=(self.kwargs.get('report_id'),)),
            'form': AutoForm(),
            'report_form': self.get_form(),
            'report_types': json.dumps(report_types),
            'numbers_of_hosts': json.dumps(numbers_of_hosts)
        })

        return context

    def get(self, request, **kwargs):
        if request.is_ajax():
            self.object = self.get_object()
            return JsonResponse(data=ReportSerializer(self.object).data)
        return super(ReportUpdateView, self).get(request, **kwargs)


# список авто для отчета при добавлении
class CarsForAddReport(AutoListView):
    paginate_by = settings.PAGINATE_BY
    search_fields = ['vin', 'source']

    def get_queryset(self):
        order_id = self.kwargs.get('order_id')
        if order_id:
            order = get_object_or_404(Order, id=order_id)
            return order.cars.all()

        return Auto.objects.all()


# добавляет id фотографий к image полям в отчете
def set_photo_to_field(report, photo, filename):
    file_extension = filename.split('.')[1]
    steps = report.data
    for s, step in enumerate(steps):
        if step.get('groups'):
            for g, group in enumerate(step.get('groups')):
                for f, field in enumerate(group.get('fields')):
                    if field.get('type') == 'image' and filename == '{}|{}|{}.{}'.format(s, g, f, file_extension):
                        old_images = field.get('value', [])
                        old_images.append({'id': photo.id})
                        field.update({'value': old_images})
                        report.data = steps
                        report.save()
                        return


# Изменить обложку авто из отчета
@login_required
@permission_required('report.change_reportpodbor', raise_exception=True)
def update_auto_cover(request, report_id):
    if request.method == 'POST':
        # photo = request.FILES.get('photo')
        photo = request.POST.get('photo')
        photo = photo.split('/')
        del photo[0]
        del photo[0]
        photo = '/'.join(photo)
        auto = get_object_or_404(Auto, reportpodbor=report_id)
        if auto:
            photo = AutoPhoto.objects.create(auto=auto, image=photo)
    return HttpResponse(status=200)


# Редактирование фотографии отчета
@login_required
@permission_required('report.change_reportpodbor', raise_exception=True)
def update_photo(request, photo_id):
    if request.method == 'POST':
        photo = get_object_or_404(ReportsPhoto, id=photo_id)
        photos = request.FILES.getlist('photo')
        for f in photos:
            photo.image = f
            photo.save()
            photo.image_small.generate()
            data = {"photos": photo}
            return HttpResponse(str(photo), 'image/jpeg', status=200)

    return HttpResponse(status=404)


# Удаление фото отчета
@login_required
@permission_required('report.change_reportpodbor', raise_exception=True)
def delete_photos(request):
    if request.method == 'POST':
        photos = request.POST.getlist('photos')
        for file_id in photos:
            photo = ReportsPhoto.objects.get(id=file_id)
            photo.delete()

        return HttpResponse(status=200)

    return HttpResponse(status=404)


# список авто для отчета при изменении
class CarsForUpdateReport(AutoListView):
    paginate_by = settings.PAGINATE_BY
    search_fields = ['vin', 'source']

    def get_queryset(self):
        report = get_object_or_404(ReportsPodbor, id=self.kwargs.get('report_id'))
        cars = Auto.objects.all()
        if report.order.all().count() > 0:
            orders = report.order.all()
            cars = cars.filter(order__in=orders)
        if report.auto:
            cars = cars.exclude(id=report.auto.id)
        return cars


# Параметры для фильтра отчета
@login_required
@permission_required('report.view_reportpodbor', raise_exception=True)
def get_report_filter(request):
    if request.is_ajax():
        data = {}

        # получаем уникальные объекты либо автомобили с уникальными полями
        cars = Auto.objects.exclude(reportpodbor=None).distinct().order_by()
        marks = MarkAuto.objects.filter(auto__in=cars).distinct()
        models = ModelAuto.objects.filter(auto__in=cars).distinct()
        generations = Generation.objects.filter(auto__in=cars).distinct()
        year_min = cars.aggregate(Min('year_auto__year')).get('year_auto__year__min')
        year_max = cars.aggregate(Max('year_auto__year')).get('year_auto__year__max')
        body_types = BodyType.objects.filter(auto__in=cars).distinct()
        colors = cars.distinct('color_auto').exclude(color_auto=None).order_by('color_auto').values_list('color_auto')
        engine_types = cars.distinct('engine_type').exclude(engine_type=None)
        capacity_min = cars.aggregate(Min('engine_capacity')).get('engine_capacity__min')
        capacity_max = cars.aggregate(Max('engine_capacity')).get('engine_capacity__max')
        drive_types = cars.exclude(drive_type=None).distinct('drive_type')
        transmission_types = cars.distinct('transmission_type').exclude(drive_type=None)
        salons_auto = cars.distinct('salon_auto').exclude(salon_auto=None).values_list('salon_auto')
        colors_salon = cars.distinct('color_salon').exclude(color_salon=None).values_list('color_salon')
        owners_max = cars.aggregate(Max('owners')).get('owners__max')

        if capacity_min and capacity_max:
            capacities = [float(i[0]) for i in ENGINE_CAPACITY if float(i[0]) >= float(capacity_min) and float(i[0]) <= float(capacity_max)]
        else:
            capacities = []

        horsepowers = [75, 100, 150, 200, 250]
        mileage = [25000, 50000, 75000, 100000, 150000, 200000]

        filials = Filial.objects.all()
        executors = User.objects.exclude(groups__name='Клиент').filter(is_active=True).distinct()

        data.update({
            'marks': [{'id': mark.id, 'name': mark.__str__()} for mark in marks],
            'models': [{'id': model.pk, 'name': model.__str__(), 'mark': model.mark_auto.id} for model in models],
            'generations': [{'id': generation.id, 'name': generation.__str__(), 'model': generation.model_auto.id} for generation in generations],
            'years': list(range(year_min, year_max + 1)) if year_min and year_max else [],
            'body_types': [{'id': b.id, 'name': b.name} for b in body_types],
            'transmission_types': [{'id': t.transmission_type, 'name': t.get_transmission_type_display()} for t in transmission_types],
            'colors': [c[0] for c in colors],
            'engine_types': [{'id': e.engine_type, 'name': e.get_engine_type_display()} for e in engine_types],
            'capacities': capacities,
            'horsepowers': horsepowers,
            'mileage': mileage,
            'drive_types': [{'id': d.drive_type, 'name': d.get_drive_type_display()} for d in drive_types],
            'salons_auto': [s[0] for s in salons_auto],
            'colors_salon': [s[0] for s in colors_salon],
            'owners': [o for o in range(1, owners_max + 1)] if owners_max else [],
            'filials': FilialSerializerPure(filials, many=True).data,
            'executors': UserSerializer(executors, many=True).data,
        })

        return JsonResponse(data)

    raise Http404


# Подбор под ключ - удалить отчет
@login_required
@permission_required('report.delete_reportpodbor', raise_exception=True)
def ppk_report_delete(request, report_id):
    if request.method == 'POST':
        report = get_object_or_404(ReportsPodbor, id=report_id)
        report.delete(force_policy=SOFT_DELETE_CASCADE)
        return HttpResponse(status=200)

    raise Http404


@login_required
def get_data_from_auto_ru(request):
    if request.GET:
        url = request.GET.get('url')
        data = utils.get_data_from_auto_ru(url)
        return JsonResponse(data)

    return JsonResponse({})


def contains(list, filter):
    for x in list:
        if filter(x):
            return True
    return False


# обновляет url для изображений
def insert_images(steps):
    for step in steps:
        for group in step.get('groups', []):
            for field in group.get('fields', []):
                if field.get('type') == 'image':
                    images = field.get('value', [])
                    removed_images = []
                    for image in images:
                        photo_id = image.get('id')
                        if photo_id:
                            photo = ReportsPhoto.objects.filter(id=photo_id).first()
                            try:
                                image.update({'image': photo.image.url, 'image_small': photo.image_small.url})
                            except (FileNotFoundError, AttributeError) as ex:
                                removed_images.append(image)
                        else:
                            removed_images.append(image)

                    for image in removed_images:
                        images.remove(image)


# возвращает шаги для отчета
@login_required
def get_report_steps(request, report_id=None):
    if request.GET:
        if report_id:
            report = get_object_or_404(ReportsPodbor, id=report_id)
            if report.data:
                report_steps = report.data
                insert_images(report_steps)
                return JsonResponse(report.data, safe=False)

        steps = ReportsSteps.objects.last()
        steps = steps.data.get('steps') if steps else []
        return JsonResponse(steps, safe=False)

    raise Http404

@login_required
def set_unread_report_by_auto_id(request, auto_id):
    reports = ReportsPodbor.objects.filter(auto=auto_id).all()
    for report in reports:
        report.is_read = False
        report.save()
    return JsonResponse({'error':False}, safe=False)