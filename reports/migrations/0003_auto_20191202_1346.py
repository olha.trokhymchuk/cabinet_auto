# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-12-02 10:46
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reports', '0002_auto_20191118_1553'),
    ]

    operations = [
        migrations.AddField(
            model_name='reportspodbor',
            name='in_recheck',
            field=models.BooleanField(default=False, verbose_name='На перепроверке'),
        ),
        migrations.AddField(
            model_name='reportspodbor',
            name='is_read',
            field=models.BooleanField(default=True, verbose_name='Прочитан отчет руководителем'),
        ),
    ]
