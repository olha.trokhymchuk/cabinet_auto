from django.core.management.base import BaseCommand

from report.workers.set_properly_json import setJson

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('id')

    def handle(self, *args, **options):
        params = options['id'].split(',')
        setJson(params)

