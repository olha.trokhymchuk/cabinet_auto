from django.core.management.base import BaseCommand

from report.workers.steps_fix import steps_fix

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        steps_fix()
