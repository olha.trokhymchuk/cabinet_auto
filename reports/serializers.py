from datetime import datetime
import logging

from django.conf import settings
from rest_framework import serializers
from rest_framework.fields import ImageField

from auto.serializers import AutoSerializer, AutoCreateSerializer
import auto.glossary as auto_glossary

from core.serializers import UserSerializer
from report.models import ReportsPodbor, ReportsPhoto, LeasePhoto
from auto.models import Auto
from core.models import User

import report.report_constants as report_constant
import auto.glossary as auto_glossary

logger = logging.getLogger(__name__)


class ReportsListSerialializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(format=settings.DATE_FORMAT, required=False)
    executor = UserSerializer()
    status = serializers.CharField(source='get_status_display')
    report_type = serializers.CharField(source='get_report_type_display')
    auto = AutoSerializer()
    name = serializers.SerializerMethodField()
    percent = serializers.SerializerMethodField()

    class Meta:
        model = ReportsPodbor
        depth = 1
        fields = ('id', 'vin', 'created', 'executor', 'auto', 'name', 'status', 'report_type', 'recommended', 'published', 'percent')

    def get_name(self, obj):
        return '{} {}'.format(obj.created.strftime(settings.DATE_FORMAT), obj.auto)

    def get_percent(self, obj):
        field_count = 1
        not_empty = 1 if obj.auto else 0

        try:
            for step in obj.data:
                for group in step.get('groups', []):
                    for field in group.get('fields', []):
                        field_count += 1
                        if field.get('value') or isinstance(field.get('value'), bool):
                            not_empty += 1

            percent = int(not_empty / field_count * 100)
            return '{}'.format(percent)
        except AttributeError as ex:
            logger.error(ex)
            return ''


class ReportSerializer(ReportListSerialializer):
    executor = UserSerializer()
    auto = AutoSerializer()
    order = serializers.SerializerMethodField()
    order_select = serializers.SerializerMethodField()
    report_type = serializers.SerializerMethodField()
    photos = serializers.SerializerMethodField()
    leasephotos = serializers.SerializerMethodField()

    class Meta:
        model = ReportsPodbor
        fields = (
            'id', 'vin', 'order_select', 'created', 'report_type', 'mileage', 'cost_before', 'cost_after', 'cost_estimated', 'number_of_hosts',
            'executor', 'order', 'auto', 'pluses', 'minuses', 'category', 'recommended', 'published', 'result_expert',
            'comment_expert', 'comment_manager', 'photos', 'leasephotos'
        )
        depth = 1

    def get_order(self, report):
        orders = report.order.all()
        return [order.id for order in orders]

    def get_order_select(self, report):
        orders = report.order.all()
        return [{'id': order.id, 'text': order.number_buh} for order in orders]

    def get_report_type(self, obj):
        return obj.report_type

    def get_photos(self, obj):
        photos = ReportsPhoto.objects.filter(report_podbor=obj, from_inspection=True)
        try:
            raw = [{'id': photo.id, 'image': photo.image.url, 'image_small': photo.image_small.url} for photo in photos if photo.image]
            return raw
        except FileNotFoundError as ex:
            return ''

    def get_leasephotos(self, obj):
        photos = LeasePhoto.objects.filter(surrender_podbor=obj)
        try:
            raw = [{'id': photo.id, 'image': photo.image.url, 'image_small': photo.image_small.url} for photo in photos if photo.image]
            return raw
        except FileNotFoundError as ex:
            return ''


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportsPhoto
        fields = (
            'id', 'image', 'embed_url', 'photo_type'
        )
        depth = 1


class PhotoLeaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeasePhoto
        fields = (
            'id', 'image'
        )
        depth = 1


class ReportSerializerV2(serializers.ModelSerializer):
    executor = UserSerializer()
    auto = AutoSerializer()
    name = serializers.SerializerMethodField()
    executor_name = serializers.SerializerMethodField()
    created = serializers.DateTimeField(format=settings.DATE_FORMAT)
    photos_auto = PhotoSerializer(many=True, read_only=True)
    surrphotos_auto = PhotoLeaseSerializer(many=True, read_only=True)

    class Meta:
        model = ReportsPodbor
        fields = (
            'id', 'created', 'executor_name', 'name', 'photos_auto', 'surrphotos_auto',
            'vin', 'report_type', 'mileage', 'cost_before', 'cost_after', 'cost_estimated', 'number_of_hosts',
            'executor', 'order', 'auto', 'pluses', 'minuses', 'category', 'recommended', 'published', 'result_expert',
            'comment_expert', 'comment_manager', 'data',
        )
        depth = 1

    def get_name(self, obj):
        return obj.__str__()

    def get_executor_name(self, obj):
        if type(obj.executor) is UserSerializer:
            return obj.executor.get_full_name()
        return ''


class ReportSerializerV3(serializers.ModelSerializer):
    executor = UserSerializer(read_only=True)
    auto = AutoSerializer()
    name = serializers.SerializerMethodField()
    executor_name = serializers.SerializerMethodField()
    photos = serializers.SerializerMethodField()
    created = serializers.DateTimeField(format=settings.DATE_FORMAT)
    class Meta:
        model = ReportsPodbor
        exclude = ('lk_id','modified','status','links','data', )
        depth = 1

    def get_name(self, obj):
        return obj.__str__()

    def get_photos(self, obj):
        photos = {}
        for photo in obj.photos_auto.all():
            photo_type = report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]
            try:
                photos[photo_type]
            except KeyError as ex:
                photos[photo_type]=[]
            try:
                photo.image_small.url
            except:
                continue
            try:
                photo.image_small.url.split("/")[3]
                image_small = "/media/" + str(photo.image_small)
                photos[photo_type].append({"image_small": image_small, "image":"/media/" + str(photo.image), "pk":photo.pk, })
            except (FileNotFoundError, ValueError,IndexError) as ex:
                logger.error(ex)
                image_small = ""

        return photos

    def get_executor_name(self, obj):
        if type(obj.executor) is UserSerializer:
            return obj.executor.get_full_name()
        return ''


class ReportSerializerCreateUpdateV3(serializers.ModelSerializer):
    executor = UserSerializer(required=False, allow_null=True)
    photos = serializers.SerializerMethodField()
    auto = AutoCreateSerializer(required=False, allow_null=True)
    executor_name = serializers.SerializerMethodField(required=False, allow_null=True)

    def get_photos(self, obj):
        photos = {}
        try:
            for photo in obj.photos_auto.all().order_by('-pk'):
                photo_type = report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]
                try:
                    photos[photo_type].append({"image_small":"/media/" + str(photo.image_small), "image":"/media/" + str(photo.image), "pk":photo.pk, })
                except KeyError as ex:
                    photos[photo_type]=[]
                    photos[photo_type].append({"image_small":"/media/" + str(photo.image_small), "image":"/media/" + str(photo.image), "pk":photo.pk, })
        except AttributeError:
            pass
        return photos    

    def create_auto(self, instance, auto_data):
        if auto_data:
            auto_serialized_data = AutoCreateSerializer(data=auto_data)
            auto_serialized_data.is_valid()
            logger.error("auto_serialized_data.errors")
            logger.error(auto_serialized_data.errors)
            logger.error("auto_serialized_data.errors")
            auto_data = auto_serialized_data.data
            try:
                vin = auto_data["vin"]
                auto = Auto.objects.get(vin=vin)
                auto_serialized_data.update(auto, auto_data)
                instance.auto = auto
            except (KeyError, Auto.DoesNotExist, AssertionError) as ex:
                logger.error(ex)
                auto_serialized_data.create(auto_serialized_data.data)
                auto = Auto.objects.get(vin=vin)
                instance.auto = auto
            except Auto.MultipleObjectsReturned as ex:
                logger.error(ex)
            try:
                self.validated_data["auto"]
            except KeyError as ex:
                self.validated_data["auto"] = auto_serialized_data.validated_data
                self.validated_data["auto"]["auto"] = {}
            self.validated_data["auto"]["id"] = auto.pk
            try:
                self.validated_data["auto"]["auto"] = auto_serialized_data.data["auto"]
            except KeyError as ex:
                pass
                
            self.validated_data["auto"]["thumbnail"] = auto_serialized_data.data["thumbnail"]
            try:
                self.validated_data["auto"]["engine_type"] = self.get_engine_type(auto_serialized_data.data["engine_type"])
            except KeyError:
                pass
            try:
                self.validated_data["auto"]["transmission_type"] = self.get_transmission_type(auto_serialized_data.data["transmission_type"])
            except KeyError:
                pass
                
        return instance

    def get_engine_type(self, engine_type):
        return auto_glossary.ENGINE_TYPE[engine_type][1] if isinstance(engine_type, int) else engine_type
    
    def get_transmission_type(self,transmission_type):
        return auto_glossary.TRANSMISSION_TYPE[transmission_type][1] if isinstance(transmission_type, int) else transmission_type

    def create(self, validated_data):
        auto_data = validated_data.pop('auto', None)
        order = validated_data.pop('order', None)
        photos = validated_data.pop('photos', None)
        executor = validated_data.pop('executor', None)
        try:
            vin = validated_data["vin"]
        except KeyError:
            vin = None
        instance =  ReportsPodbor.objects.create(**validated_data)
        instance = self.create_executor(instance, executor)
        logger.error("instance")
        logger.error(instance)
        instance = self.create_auto(instance, auto_data)
        instance.save()
        validated_data["auto"] = auto_data
        return instance

    def create_executor(self, instance, executor):
        if executor:
            try:
                instance.executor = User.objects.get(phone=executor["phone"])
            except (User.DoesNotExist, TypeError):
                pass
        if executor and not instance.executor:
            try:
                instance.executor = User.objects.get(pk=executor["id"])
            except (TypeError, KeyError):
                pass
        return instance

    def update(self, instance, validated_data):
        auto_data = validated_data.pop('auto', None)
        executor = validated_data.pop('executor', None)
        instance = self.create_executor(instance, executor)
        instance = self.create_auto(instance, auto_data)
        for attr, value in validated_data.items():
            if(attr != "created"):
                try:
                    setattr(instance, attr, value)
                except (TypeError, ValueError):
                    pass
        validated_data["auto"] = auto_data
        instance.save()
        return instance

    def get_executor_name(self, obj):
        try:
            if type(obj.executor) is UserSerializer:
                return obj.executor.get_full_name()
        except AttributeError:
            pass
        return ''

    class Meta:
        model = ReportsPodbor
        exclude = ('lk_id','modified','status','links','data', )
        depth = 1

