from django.conf.urls import url
from .views import *
from .api.views import ReportDetaiApi, add_photo

app_name = 'report'

urlpatterns = [
    url(r'^view-old/(?P<report_id>\d+)/$', ReportView.as_view(), name='report_view_old'),
    url(r'^view/(?P<report_id>\d+)/$', ReportViewV2.as_view(), name='report_view'),
    url(r'^view-api/(?P<report_id>\d+)/$', ReportViewV2Api.as_view(), name='report_view'),
    url(r'^view-api3/$', ReportDetaiApi.as_view(), name='report_view'),
    url(r'^view-api3/(?P<report_id>\d+)/$', ReportDetaiApi.as_view(), name='report_view'),
    url(r'^add-photo/(?P<report_id>\d+)/$', add_photo, name='add_photo'),
    
    url(r'^get-pdf/$', render_to_pdf, name='get_pdf'),
    url(r'^report-edit/(?P<report_id>\d+)/$', ReportUpdateView.as_view(), name='report_edit'),
    url(r'^report-add/$', ReportCreateView.as_view(), name='add_report'),
    url(r'^report-add/(?P<order_id>\d+)/$', ReportCreateView.as_view(), name='add_report_for_order'),
    url(r'^add-report-for-order/(?P<order_id>\d+)$', add_report_for_auto, name='add_report_for_auto'),
    url(r'^add-report-for-auto/(?P<auto_id>\d+)/$', add_report_for_auto, name='add_report_for_auto'),
    url(r'^add-report-for-auto/(?P<auto_id>\d+)/(?P<order_id>\d+)/$', add_report_for_auto, name='add_report_for_auto'),
    url(r'^add-report-for-auto/(?P<auto_id>\d+)/(?P<order_id>[-\w]+)/$', add_report_for_auto, name='add_report_for_auto'),
    url(r'^add-auto-from-auto-ru/$', add_auto_from_auto_ru, name='add_auto_from_auto_ru'),
    url(r'^add-auto-from-auto-ru/(?P<report_id>\d+)/$', add_auto_from_auto_ru, name='add_auto_from_auto_ru'),
    url(r'^add-auto-manual/$', report_set_auto, name='add_auto_manual'),
    url(r'^add-auto-manual/(?P<report_id>\d+)/$', report_set_auto, name='add_auto_manual'),
    url(r'^cars-for-add-report/$', CarsForAddReport.as_view(), name='cars_for_add_report'),
    url(r'^cars-for-add-report/(?P<order_id>\d+)/$', CarsForAddReport.as_view(), name='cars_for_add_report_by_order'),
    url(r'^cars-for-update-report/(?P<report_id>\d+)/$', CarsForUpdateReport.as_view(), name='cars_for_update_report'),
    url(r'^delete/(?P<report_id>\d+)/$', ppk_report_delete, name='report_delete'),
    url(r'^list/$', ReportList.as_view(), name='report_list'),
    url(r'^list-by-car/(?P<car_id>\d+)$', ReportsByCar.as_view(), name='reports_by_car'),
    url(r'^list-by-car/(?P<slug>[-\w]+)$', ReportsByCar.as_view(), name='reports_by_car_slug'),
    url(r'^filter/$', get_report_filter, name='report_filter'),
    url(r'^get_data_from_auto_ru/$', get_data_from_auto_ru, name='get_data_from_auto_ru'),
    url(r'^get-report-steps/(?P<report_id>\d+)$', get_report_steps, name='get_report_data'),
    url(r'^udate-auto-cover/(?P<report_id>\d+)/$', udate_auto_cover, name='udate_auto_cover'),
    url(r'^update-photo/(?P<photo_id>\d+)$', update_photo, name='update_photo'),
    url(r'^delete-photos/$', delete_photos, name='delete_photos'),
    url(r'^set-unread-report/(?P<auto_id>\d+)$', set_unread_report_by_auto_id, name='set_unread_report_by_auto_id'),
]