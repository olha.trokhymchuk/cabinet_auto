import re
import os

from django import template
from django.template import TemplateSyntaxError, Node
from django.template.defaulttags import TemplateIfParser, IfNode

from django.contrib.humanize.templatetags.humanize import naturalday, intcomma

from django.conf import settings
from django.conf.urls.static import static

from PIL import Image, ImageEnhance, ImageDraw

register = template.Library()
import base64
from io import BytesIO


@register.filter
def water_sign(value):
    return value
    name = value.split('/')[-1]
    output_path = settings.MEDIA_ROOT + '/watermark/' + name
    if os.path.exists(output_path):
        return '/media/watermark/' + name
    url = settings.STATICFILES_DIRS[0] + '/img/logo.jpeg'
    url_media = settings.BASE_DIR + value
    watermark = Image.open(url)
    image = Image.open(url_media)
    image = add_watermark(image, watermark).save(output_path, format="JPEG")                  
    return '/media/watermark/' + name


def add_watermark(image, watermark, opacity=0.5, wm_interval=650):
    assert opacity >= 0 and opacity <= 1
    if opacity < 1:
        if watermark.mode != 'RGBA':
            watermark = watermark.convert('RGBA')
        else:
            watermark = watermark.copy()
        alpha = watermark.split()[3]
        alpha = ImageEnhance.Brightness(alpha).enhance(opacity)
        watermark.putalpha(alpha)
    layer = Image.new('RGBA', image.size, (0,0,0,0))
    # for y in range(0, image.size[1], watermark.size[1]+wm_interval):
    #     for x in range(0, image.size[0], watermark.size[0]+wm_interval):
    print(image.size)
    x = image.size[0]-watermark.size[0]
    y = image.size[1]-watermark.size[1]
    print(str(x) + " " + str(y))
    layer.paste(watermark, (x, y))
    return Image.composite(layer,  image,  layer)


