from django import template
from django.template.defaultfilters import stringfilter
from django.utils.safestring import mark_safe
from django.utils.text import normalize_newlines

register = template.Library()

@register.filter()
@stringfilter
def linebreaksodt(value, autoescape=None):
    value = str(value).split('\\n')
    return value

