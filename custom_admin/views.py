from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.views import View
from django.apps import apps
from django.views.generic.base import ContextMixin, TemplateResponseMixin
from custom_admin.utils import set_permissions, get_permissions, merge
from django.http import Http404


class ModelsView(LoginRequiredMixin, PermissionRequiredMixin, ContextMixin, TemplateResponseMixin, View):
    permission_required = 'custom_admin.view_storage'
    raise_exception = True
    template_name = 'admin/perms/model-list.html'
    object = None

    def get(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return self.render_to_response(self.get_context_data())


    def get_context_data(self, **kwargs):
        context = super(ModelsView, self).get_context_data(**kwargs)
        applications = ['auto', 'core', 'order', 'report', 'service', 'message']

        context['apps'] = []

        for app in applications:
            app_models = apps.get_app_config(app).get_models()
            context['apps'].append((app, [m._meta.model_name for m in app_models]))

        return context


class EditPerms(LoginRequiredMixin, PermissionRequiredMixin, ContextMixin, TemplateResponseMixin, View):
    permission_required = 'custom_admin.change_storage'
    raise_exception = True
    template_name = 'admin/perms/edit-model.html'
    object = None

    def get(self, request, *args, **kwargs):
        return self.render_to_response(self.get_context_data())

    def post(self, request, *args, **kwargs):
        self.save_form(request.POST)
        return self.render_to_response(self.get_context_data())

    def _get_empty_perms_dict(self, app_name, model_name, groups):
        model = apps.get_model(app_label=app_name, model_name=model_name)
        fieldset = [f.name for f in model._meta.get_fields()]
        perms = {app_name: {model_name: {}}}
        model_perms = perms[app_name][model_name]

        for field in fieldset:
            for group in groups:
                for action in ['create', 'read', 'update', 'delete']:
                    if field not in model_perms:
                        model_perms.update({field: {group: {action: False}}})
                    elif group not in model_perms[field]:
                        model_perms[field].update({group: {action: False}})
                    else:
                        model_perms[field][group].update({action: False})
        return perms

    def _parse_permissions(self, post):
        app_name, model_name = self.kwargs['app_name'], self.kwargs['model_name']
        perms = self._get_empty_perms_dict(app_name, model_name, [g.name for g in Group.objects.all()])
        inputs = [key.split(':') for key in post.keys() if key.count(':') == 2]

        for field, group, action in inputs:
            perms[app_name][model_name][field][group].update({action: True})

        return perms

    def save_form(self, post):
        new_permissions = self._parse_permissions(post)
        old_permissions = get_permissions()
        merge(new_permissions, old_permissions)
        set_permissions(old_permissions)

    def get_context_data(self, **kwargs):
        context = super(EditPerms, self).get_context_data(**kwargs)
        app_name, model_name = self.kwargs['app_name'], self.kwargs['model_name']
        model = apps.get_model(app_label=app_name, model_name=model_name)
        groups = Group.objects.all()

        context['app_name'] = app_name
        context['model_name'] = model_name
        context['fieldset'] = model._meta.get_fields()
        context['groups'] = [g.name for g in groups]
        context['permissions'] = get_permissions()

        return context
