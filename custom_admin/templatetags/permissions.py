from django import template
from django.template import TemplateSyntaxError, Node
from django.template.defaulttags import TemplateIfParser, IfNode

register = template.Library()


@register.simple_tag
def checked(permissions, app, model, field, group, action):
    try:
        if permissions[app][model][field][group][action]:
            return 'checked'
    except KeyError:
        return None


@register.tag
def if_perms(parser, token):
    try:
        tag, perms, model, field = token.contents.split()
    except (ValueError, TypeError):
        raise TemplateSyntaxError(
            "'%s' tag takes two parameters" % tag)

    default_states = ['if_perms', 'else']
    end_tag = 'endperms'

    states = {}
    while token.contents != end_tag:
        current = token.contents
        states[current.split()[0]] = parser.parse(default_states + [end_tag])
        token = parser.next_token()

    perms = parser.compile_filter(perms)
    model = parser.compile_filter(model)
    field = parser.compile_filter(field)

    return PermsNode(states, perms, model, field)


class PermsNode(Node):
    def __init__(self, states, perms, model, field):
        self.states = states
        self.perms = perms
        self.model = model
        self.field = field

    def render(self, context):
        perms = self.perms.resolve(context, True)
        model = self.model.resolve(context, True)
        field = self.field.resolve(context, True)

        is_true = field in perms[model._meta.model_name]
        try:
            return self.states[is_true and 'if_perms' or 'else'].render(context)
        except KeyError:
            return ''
