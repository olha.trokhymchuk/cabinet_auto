"""autopodbor URL Configuration
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from custom_admin.views import EditPerms, ModelsView


admin.site.index_template = 'admin/perms/custom_index.html'
admin.autodiscover()


urlpatterns = [
    url(r'^perms/$', ModelsView.as_view(), name='index'),
    url(r'^perms/(?P<app_name>\w+)/(?P<model_name>\w+)/$', EditPerms.as_view(), name='detail_perms')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
