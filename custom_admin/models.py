from django.db import models
from django.contrib.postgres.fields import JSONField


class Storage(models.Model):
    PERMISSIONS = 'permissions'
    NAME_CHOICES = (
        (PERMISSIONS, 'Права доступа'),
    )
    name = models.CharField(max_length=15, choices=NAME_CHOICES, unique=True)
    data = JSONField(default=dict, blank=True)

    class Meta:
        verbose_name = "Хранилище"
        verbose_name_plural = "Хранилище"

    def __str__(self):
        return self.name
