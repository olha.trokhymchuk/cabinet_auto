import logging

import redis
from django.apps import apps
from django.conf import settings
from django.views.generic.edit import FormMixin
from django.views.generic.base import ContextMixin
from custom_admin.models import Storage


logger = logging.getLogger(__name__)


REDIS_EMPTY_LIST = ['']


def get_redis():
    return redis.StrictRedis(host=settings.REDIS_CACHE_HOST,
                             port=settings.REDIS_CACHE_PORT,
                             db=settings.REDIS_CACHE_DB,
                             decode_responses=True)


def get_allowed_fields_from_cache(app, model, groups, action):
    try:
        r = get_redis()
        allowed_fields = r.lrange('permissions:{}:{}:{}:{}'.format(app, model, sorted(groups), action), 0, -1)
        if not allowed_fields:
            return None, []
        if allowed_fields == REDIS_EMPTY_LIST:
            return 'cached', []
        return 'cached', allowed_fields
    except Exception as ex:
        logger.warning(ex)
        return None, []


def delete_cache():
    try:
        r = get_redis()
        keys = r.keys('permissions:*')
        for key in keys:
            r.delete(key)
    except Exception as ex:
        logger.warning(ex)


def set_permission_cache(app, model, groups, action, allowed_fields):
    try:
        r = get_redis()
        if allowed_fields:
            r.rpush('permissions:{}:{}:{}:{}'.format(app, model, sorted(groups), action), *allowed_fields)
        else:
            r.rpush('permissions:{}:{}:{}:{}'.format(app, model, sorted(groups), action), *REDIS_EMPTY_LIST)
    except Exception as ex:
        logger.warning(ex)


def get_permissions():
    permission_storage, created = Storage.objects.get_or_create(name=Storage.PERMISSIONS)
    return permission_storage.data


def set_permissions(permissions):
    permission_storage, created = Storage.objects.get_or_create(name=Storage.PERMISSIONS)
    permission_storage.data = permissions
    permission_storage.save()
    delete_cache()


def get_allowed_fields(app_name, model_name, groups, action):
    cached, allowed_fields = get_allowed_fields_from_cache(app_name, model_name, groups, action)
    if cached:
        return allowed_fields

    permissions = get_permissions()

    try:
        permissions = permissions[app_name][model_name]
    except KeyError:
        return []

    allowed_fields = set()

    for field in permissions:
        for group in groups:
            if group in permissions[field] and action in permissions[field][group] and permissions[field][group][action]:
                allowed_fields.add(field)

    set_permission_cache(app_name, model_name, groups, action, list(allowed_fields))

    return allowed_fields


def get_allowed_fields_dict(model, groups, action):
    app_name = model._meta.app_label
    model_name = model._meta.model_name
    allowed_fields = get_allowed_fields(app_name, model_name, groups, action)
    model_fields = {}
    for f in allowed_fields:
        if hasattr(model, f):
            model_fields.update({f: getattr(model, f)})

    return {model_name: model_fields}

def get_allowed_fields_api(model, groups, action):
    app_name = model._meta.app_label
    model_name = model._meta.model_name
    allowed_fields = get_allowed_fields(app_name, model_name, groups, action)
    model_fields = []
    for f in allowed_fields:
        if hasattr(model, f):
            model_fields.append(f)

    return {model_name: model_fields}


def merge(source, destination):
    for key, value in source.items():
        if isinstance(value, dict):
            node = destination.setdefault(key, {})
            merge(value, node)
        else:
            destination[key] = value

    return destination


class FilteredFieldsMixin(FormMixin):
    action = 'read'

    def get_form(self, *args, **kwargs):
        form = super().get_form(*args, **kwargs)
        user_groups = [g.name for g in self.request.user.groups.all()]
        if self.request.user.is_admin:
            return form

        app_name, model_name = self.model._meta.app_label, self.model._meta.model_name
        modelfields = [f.name for f in self.model._meta.get_fields()]
        allowed_fields = get_allowed_fields(app_name, model_name, user_groups, self.action)
        filtered_fields = [field for field in form.fields if field not in allowed_fields and field in modelfields]

        for field in filtered_fields:
            del form.fields[field]

        return form


class FilteredFieldContextMixin(ContextMixin):
    field_perms_model = None
    action = 'read'

    def get_context_data(self, **kwargs):
        target_model = self.field_perms_model if self.field_perms_model else self.model
        context = super(FilteredFieldContextMixin, self).get_context_data(**kwargs)
        user_groups = [g.name for g in self.request.user.groups.all()]
        perms = get_allowed_fields_dict(target_model, user_groups, self.action)
        context.update({'field_perms': perms})
        return context

class FilteredFieldApiMixin():
    field_perms_model = None
    action = 'read'

    def get_perms(self, **kwargs):

        try:
            user = self.context['request'].user
            self.user_page = self.context['user_page']
            user_groups = [g.name for g in user.groups.all()]
        except KeyError as ex:
            logger.error(ex)
        else:
            target_model = self.field_perms_model if self.field_perms_model else self.model
            self.perms = get_allowed_fields_api(target_model, user_groups, self.action)
