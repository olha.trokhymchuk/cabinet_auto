from django.forms import ModelForm

from custom_admin.utils import get_allowed_fields


class FilteredForm(ModelForm):
    def __init__(self, *args, **kwargs):
        if 'request' not in kwargs and 'action' not in kwargs:
            kwargs.pop('request', None)
            kwargs.pop('action', None)
            super(FilteredForm, self).__init__(*args, **kwargs)
        else:
            request, action = kwargs.pop('request'), kwargs.pop('action')
            super(FilteredForm, self).__init__(*args, **kwargs)

            user_groups = [g.name for g in request.user.groups.all()]
            app_name = self._meta.model._meta.app_label
            model_name = self._meta.model._meta.model_name

            allowed_fields = get_allowed_fields(app_name, model_name, user_groups, action)
            filtered_fields = [field for field in self.fields if field not in allowed_fields]

            for field in filtered_fields:
                del self.fields[field]
