from django.test import SimpleTestCase
from django.template import Context, Template


class TestModel:
    class Meta:
        model_name = 'auto'
    _meta = Meta()


class TestTagIfPerms(SimpleTestCase):
    def setUp(self):
        self.perms = {'auto': ['id', 'mark_auto']}
        self.model = TestModel()
        self.context = Context({
            'perms': self.perms,
            'model': self.model,
        })

    def test_if(self):
        self.context.update({'field': 'id'})
        template_to_render = Template(
            '{% load permissions %}'
            '{% if_perms perms model field %}'
            'ok'
            '{% endperms %}'
        )

        rendered_template = template_to_render.render(self.context)
        self.assertInHTML('ok', rendered_template)

    def test_else(self):
        self.context.update({'field': 'model_auto'})
        template_to_render = Template(
            '{% load permissions %}'
            '{% if_perms perms model field %}'
            'error'
            '{% else %}'
            'ok'
            '{% endperms %}'
        )

        rendered_template = template_to_render.render(self.context)
        self.assertInHTML('ok', rendered_template)

    def test_after_tag(self):
        self.context.update({'field': 'model_auto'})
        template_to_render = Template(
            '{% load permissions %}'
            '{% if_perms perms model field %}'
            'error'
            '{% endperms %}'
            'ok'
        )

        rendered_template = template_to_render.render(self.context)
        self.assertInHTML('ok', rendered_template)
