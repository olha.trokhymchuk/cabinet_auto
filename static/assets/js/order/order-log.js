function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();
  var elemTop = $(elem).offset().top;
  var elemBottom = elemTop + $(elem).height();
  return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}


var logs = new Vue({
  delimiters: ['${', '}'],
  el: '#logs',
  data: {
    objects: [],
    count: 0,
    loading: false,
    page: {
      num_pages: 1,
      number: 1,
      has_previous: false,
      has_next: false,
      has_other_pages: false,
      start_index: 1,
      end_index: 1
    }
  },
  created: function () {
    window.addEventListener('scroll', this.handleScroll);
  },
  mounted: function() {
    this.updateObjects();
  },
  destroyed: function () {
    window.removeEventListener('scroll', this.handleScroll);
  },
  methods: {
    getReportName(report) {
      return `${report.auto.mark_auto.name} ${report.auto.model_auto.name} ${report.auto.generation.name} ${report.auto.vin}`;
    },
    updateObjects() {
      if (this.loading)
        return;

      var self = this;
      this.loading = true

      $.ajax({
        url: `${window.location.href}?page=${this.page.number}`,
        cache: false,
        method: 'GET',
        success: function (data) {
          self.objects = self.objects.concat(data.objects);
          self.count = data.count;
          self.page = data.page;
          self.loading = false;
        },
        error: function (error) {
          self.loading = false;
          console.log(error);
        }
      });
    },
    isScrolledIntoView(elem) {
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    },
    handleScroll: function (event) {
      elem = $('#end');
      if (isScrolledIntoView(elem) && !this.loading && this.page.has_next) {
        this.page.number = this.page.next_page_number;
        this.updateObjects();
      }
    }
  }
})

