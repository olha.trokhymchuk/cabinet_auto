
// OLD FUNCTIONS ! - should be removed later

var orderDetailHeader = new Vue({
  delimiters: ['${', '}'],
  el: '#order-detail-header',
  data: {
    csrf: csrf,
  },
  methods: {
    openDropdown(e) {
      $('.dropdown_open').removeClass('dropdown_open');
      $(e.target).parents('.dropdown').toggleClass('dropdown_open');
    },
    removeOrder(id) {
      self = this;
      $.ajax({
        url : `/order/delete/${id}/`,
        type: "POST",
        data : {csrfmiddlewaretoken: this.csrf},
        success: function() {
          window.location = '/order/list';
        }
      });
    },
  }
});


var orderDetailTabs = new Vue({
  delimiters: ['${', '}'],
  el: '#order-detail-tabs',
  data: {
    order_id: order_id,
    ppkCount: 0,
    carsCount: 0,
    reportsCount: 0,
    logsCount: 0
  },
  mounted: function() {
    this.updateCounts();
  },
  methods: {
    updateCounts() {
      var self = this;

      $.ajax({
        url: `/order/stats/${order_id}`,
        cache: false,
        method: 'GET',
        success: function (data) {
          self.ppkCount = data.ppk_count;
          self.carsCount = data.car_count;
          self.reportsCount = data.reports_count;
          self.logsCount = data.log_count;
        },
        error: function (error) {
          console.log(error);
        }
      });
    }
  }
});
