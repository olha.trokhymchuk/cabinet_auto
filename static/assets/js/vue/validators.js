const uniqueUrl = {
  getMessage(field, params, data) {
    return (data && data.message) || 'Такой автомобиль уже существует';
  },
  validate(value, args) {
    if (value.startsWith('http://')) {
      value = value.replace('http://', 'https://');
    }
    if (!value.endsWith('/')) {
      value += '/';
    }

    result = $.ajax({
      url: `/auto/all-auto/?source=${value}&no_paginate=1`,
      method: 'GET',
      error: function (error) {
        console.log(error);
      }
    });

    return result.then(function(response) {
      return {
        valid: response.count === 0,
        data: value
      }
    });
  }
};

const uniqueSource = {
  getMessage(field, params, data) {
    return (data && data.message) || 'Такой автомобиль уже существует';
  },
  validate(value, args) {
    if (value.startsWith('http://')) {
      value = value.replace('http://', 'https://');
    }
    if (!value.endsWith('/')) {
      value += '/';
    }

    result = $.ajax({
      url: `/auto/all-auto/?source=${value}&no_paginate=1`,
      method: 'GET',
      error: function (error) {
        console.log(error);
      }
    });

    return result.then(function(response) {
      return {
        valid: response.count === 0 || (response.count === 1 && response.objects[0].id == auto_id),
        data: value
      }
    });
  }
};

const uniqueVin = {
  getMessage(field, params, data) {
    return (data && data.message) || 'Такой автомобиль уже существует';
  },
  validate(value, args) {
    result = $.ajax({
      url: `/auto/all-auto/?vin=${value}&no_paginate=1`,
      method: 'GET',
      error: function (error) {
          console.log(error);
      }
    });

    return result.then(function(response) {
      return {
        valid: response.count === 0 || (response.count === 1 && response.objects[0].id == auto_id),
        data: value
      }
    });
  }
};

VeeValidate.Validator.extend('unique_url', uniqueUrl);
VeeValidate.Validator.extend('unique_vin', uniqueVin);
VeeValidate.Validator.extend('unique_source', uniqueSource);