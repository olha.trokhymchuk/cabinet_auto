Vue.directive('select2', {
  inserted: function (el, binding, vnode) {
    var key = binding.expression;
    var select = $(el);

    select.select2({});
    select.on('change', function () {
      Vue.set(vnode.context, key, select.val());
    });
  },
  unbind: function (el, binding, vnode) {
    var select = $(el);
    select.select2('destroy');
  },
});