$(function() {
    resizedImages = {}

    function resizeImage(file) {
        var canvas = document.createElement('canvas');
        var context = canvas.getContext('2d');
        var maxW = 400;
        var maxH = 400;
        var img = document.createElement('img');
  
        img.onload = function() {
          var iw = img.width;
          var ih = img.height;
          var scale = Math.min((maxW / iw), (maxH / ih));
          var iwScaled = iw * scale;
          var ihScaled = ih * scale;
          canvas.width = iwScaled;
          canvas.height = ihScaled;
          context.drawImage(img, 0, 0, iwScaled, ihScaled);
          console.log(canvas.toDataURL());
          document.body.innerHTML+=canvas.toDataURL();
        }
        img.src = URL.createObjectURL(file);
      }

    //  $(document).on('change', 'input[name^="file"]', function(){
    //     file = file.files[0];
    //     console.log(file)
    //     if (file) {
    //       resizeImage(file);
    //     }
    //  });

    // $(document).on("imageResized", function (event) {
    //     console.log(event.blob)
    // })
})
