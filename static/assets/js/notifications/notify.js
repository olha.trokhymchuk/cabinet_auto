var notifyBadgeClass = 'unread_count';
var notifyMenuClass = 'messages';
var notifyApiUrl = '/notifications/';
var notifyRefreshPeriod = 600000;


function fillNotificationBadge(data) {
    var badges = document.getElementsByClassName(notifyBadgeClass);

    if (badges) {
        for (var i = 0; i < badges.length; i++) {
            badges[i].innerHTML = data['unread_count'] == 0 ? '' : data['unread_count']
        }
    }
}

function fillNotificationList(data) {
    var messages = document.getElementsByClassName(notifyMenuClass);

    if (messages) {
        for (var i = 0; i < messages.length; i++) {
            messages[i].innerHTML = data['html']
        }
    }
}

function updateNotifications() {
    $.ajax({
        url: notifyApiUrl,
        success: function(data) {
            data = JSON.parse(data);
            fillNotificationBadge(data);
            fillNotificationList(data);
            setTimeout(updateNotifications, notifyRefreshPeriod);
        }
    });
}


$(document).ready(function() {
    updateNotifications();
});
