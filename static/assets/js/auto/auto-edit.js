Vue.use(VeeValidate);

var app = new Vue({
  delimiters: ['${', '}'],
  el: '.car-edit',
  data: {
    method: 1,
    files: [],
    uploadedFiles: [],
    minSize: 1024,
    size: 1024 * 1024 * 10,
    autoCompress: 1024 * 512,
    name: 'photo',
    uploadAuto: false,
    dropDirectory: true,
    drop: true,
    addIndex: false,
    thread: 3,
    autoCompress: 1024 * 1024,
    uploadAuto: false,
    isOption: false,
    isPositionChanged: false,
    addData: {
      show: false,
      name: '',
      type: '',
      content: '',
    },
    editFile: {
      show: false,
      name: '',
    },
  },
  components: {
    FileUpload: VueUploadComponent
  },
  watch: {
    'editFile.show'(newValue, oldValue) {
      if (!newValue && oldValue) {
        this.$refs.upload.update(this.editFile.id, { error: this.editFile.error || '' })
      }
      if (newValue) {
        this.$nextTick(function () {
          if (!this.$refs.editImage) {
            return
          }
          let cropper = new Cropper(this.$refs.editImage, {
            autoCrop: false,
          })
          this.editFile = {
            ...this.editFile,
            cropper
          }
        })
      }
    },
    'addData.show'(show) {
      if (show) {
        this.addData.name = ''
        this.addData.type = ''
        this.addData.content = ''
      }
    }
  },
  methods: {
    inputFile: function (newFile, oldFile) {
      self = this;
      if (newFile && oldFile && !newFile.active && oldFile.active) {
        if (newFile.xhr) {
          if (newFile.xhr.status == 200) {
            newUploadedFile = JSON.parse(newFile.xhr.response);
            self.uploadedFiles.push(newUploadedFile);
            index = self.files.indexOf(newFile);
            self.files.splice(index, 1);
          }
        }
      }
    },
    inputFilter: function (newFile, oldFile, prevent) {
      if (newFile && !oldFile) {
        if (!/\.(jpeg|jpe|jpg|gif|png|webp)$/i.test(newFile.name)) {
          return prevent()
        }
      }

      // Automatic compression
      if (newFile.file && newFile.type.substr(0, 6) === 'image/' && this.autoCompress > 0 && this.autoCompress < newFile.size) {
        newFile.error = 'compressing'
        const imageCompressor = new ImageCompressor(null, {
          convertSize: Infinity,
          maxWidth: 1024,
          maxHeight: 1024,
        })
        imageCompressor.compress(newFile.file)
          .then((file) => {
            this.$refs.upload.update(newFile, { error: '', file, size: file.size, type: file.type })
          })
          .catch((err) => {
            this.$refs.upload.update(newFile, { error: err.message || 'compress' })
          })
      }

      newFile.blob = ''
      let URL = window.URL || window.webkitURL
      if (URL && URL.createObjectURL) {
        newFile.blob = URL.createObjectURL(newFile.file)
      }
    },
    validateBeforeSubmit(e) {
      if (this.method == 1) {
        this.$validator.validate('url').then((result) => {
          if (result) {
            $("#form").submit();
          }
        })
      }
      if (this.method == 2) {
        this.$validator.detach('url');
        this.$validator.validateAll().then((result) => {
          if (result) {
            form = document.getElementById('form');
            this.addFilesAndSubmit(form);
          }
          this.$validator.attach('url', 'required|url');
        })
      }
    },
    addFilesAndSubmit(form) {
      let data = new FormData(form);

      app.files.forEach(function(item) {
        data.append('photo', item.file);
      });

      if (this.isPositionChanged) {
        this.changePositions();
      }

      $.ajax({
        url: $("#form").attr('action'),
        data: data,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data) {
         validateVin
        }
      });
    },
    openDropdown(e) {
      $(e.target).parents('.dropdown').toggleClass('dropdown_open');
    },
    getFileBySortKey(sortKey) {
      for (var i = 0; i < this.uploadedFiles.length; i++) {
        if (this.uploadedFiles[i].sort_key == sortKey) return this.uploadedFiles[i];
      }
      return null;
    },
    moveUp(file) {
      $('.dropdown_open').removeClass('dropdown_open');
      this.isPositionChanged = true;
      prevFile = this.getFileBySortKey(file.sort_key - 1);
      if (prevFile) {
        prevFile.sort_key += 1;
        file.sort_key -= 1;
        this.resortUploadedFiles();
      }
    },
    moveDown(file) {
      $('.dropdown_open').removeClass('dropdown_open');
      this.isPositionChanged = true;
      prevFile = this.getFileBySortKey(file.sort_key + 1);
      if (prevFile) {
        prevFile.sort_key -= 1;
        file.sort_key += 1;
        this.resortUploadedFiles();
      }
    },
    resortUploadedFiles() {
      this.uploadedFiles = this.uploadedFiles.sort(function(a, b) {
        return a.sort_key - b.sort_key;
      });
    },
    onEditFileShow(file) {
      this.editFile = { ...file, show: true }
      this.$refs.upload.update(file, { error: 'edit' })
    },
    onEditUploadedFileShow(file) {
      this.editFile = { ...file, show: true, type: 'image/jpeg', name: file.id }
      this.$refs.upload.update(file, { error: 'edit' })
    },
    onEditorFile() {
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        this.editFile.show = false;
        return
      }

      let data = {
        name: this.editFile.name,
      }

      if (this.editFile.cropper) {
        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
        let arr = new Uint8Array(binStr.length);
        for (let i = 0; i < binStr.length; i++) {
          arr[i] = binStr.charCodeAt(i);
        }
        data.file = new File([arr], data.name, { type: this.editFile.type });
        data.size = data.file.size;
      }
      this.$refs.upload.update(this.editFile.id, data);
      this.editFile.error = '';
      this.editFile.show = false;
    },
    onEditorUploadedFile() {
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        this.editFile.show = false
        return
      }

      let data = {
        name: this.editFile.name,
      }

      if (this.editFile.cropper) {
        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
        let arr = new Uint8Array(binStr.length);
        for (let i = 0; i < binStr.length; i++) {
          arr[i] = binStr.charCodeAt(i);
        }
        data.file = new File([arr], data.name, { type: this.editFile.type });
        data.size = data.file.size;
      }

      this.editFile.error = '';
      this.editFile.show = false;
      this.updateUploadedFile(data.file, this.editFile.id);
    },
    // add folder
    onAddFolder() {
      if (!this.$refs.upload.features.directory) {
        this.alert('Your browser does not support')
        return
      }
      let input = this.$refs.upload.$el.querySelector('input')
      input.directory = true
      input.webkitdirectory = true
      this.directory = true
      input.onclick = null
      input.click()
      input.onclick = (e) => {
        this.directory = false
        input.directory = false
        input.webkitdirectory = false
      }
    },
    onAddData() {
      this.addData.show = false
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        return
      }
      let file = new window.File([this.addData.content], this.addData.name, {
        type: this.addData.type,
      })
      this.$refs.upload.add(file)
    },
    updateUploadedFile(file, fileId) {
      var self = this;
      let data = new FormData();
      data.append('csrfmiddlewaretoken', this.csrf);
      data.append('photo', file);

      $.ajax({
        url: `/auto/update-photo-auto/${fileId}/${file.sort_key}`,
        method: 'POST',
        data : data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
          let newUploadedFile = JSON.parse(data);
          let index = self.uploadedFiles.map(function(f) { return f.id; }).indexOf(fileId);
          self.uploadedFiles.splice(index, 1, newUploadedFile);
        },
        error: function (error) {
            console.log(error);
        }
      });
    },
    removeUploadedFile(file) {
      var self = this;

      $.ajax({
        url: '/auto/delete-photo-auto/' + file.id,
        method: 'POST',
        data : {csrfmiddlewaretoken: this.csrf},
        success: function (data) {
          let index = self.uploadedFiles.indexOf(file);
          self.uploadedFiles.splice(index, 1);
        },
        error: function (error) {
            console.log(error);
        }
      });
    },
    changePositions() {
      self = this;

      this.uploadedFiles.forEach(function(file) {
        $.ajax({
          url: `/auto/update-photo-auto-position/${file.id}/${file.sort_key}`,
          method: 'POST',
          data : {csrfmiddlewaretoken: self.csrf},
          success: function(data) {

          },
          error: function (error) {
              console.log(error);
          }
        });
      })

    }
  }
})

