function isScrolledIntoView(elem) {
  var docViewTop = $(window).scrollTop();
  var docViewBottom = docViewTop + $(window).height();
  var elemTop = $(elem).offset().top;
  var elemBottom = elemTop + $(elem).height();
  return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
}

var store = {
  state: {
    filter: ''
  }
}

var app = new Vue({
  delimiters: ['${', '}'],
  el: '.cars',
  data: {
    csrf: '',
    cars: [],
    carCount: 0,
    currentPage: 1,
    carsLoading: false,
    hasNextPage: true,
    sharedState: store.state,
    url: window.location.href,
    paginator: '',
    searchQuery: '',
    page: {
      num_pages: 1,
      number: 1,
      has_previous: false,
      has_next: false,
      has_other_pages: false,
      start_index: 1,
      end_index: 1
    }
  },
  mounted: function () {
     this.updateCars();
  },
//  created: function () {
//    window.addEventListener('scroll', this.handleScroll);
//  },
//  destroyed: function () {
//    window.removeEventListener('scroll', this.handleScroll);
//  },
  watch: {
    'sharedState.filter'(query) {
      let title = $(document).find("title").text();
      window.history.pushState(null, title, query);
      this.url = window.location.href.split('?')[0] + query
      this.updateCars();
    },
    'sharedState.cars'(cars) {
      this.cars = cars;
    },
    'searchQuery'(query) {
      let title = $(document).find("title").text();
      query = `?search=1&querystring=${query}`;
      window.history.pushState(null, title, query);
      this.url = `/auto/all-auto${query}`;
      this.updateCars();
    }
  },
  methods: {
    removeCar(car) {
      self = this;
      $.ajax({
        url : `/auto/delete-auto/${car.id}/`,
        type: "POST",
        data : {csrfmiddlewaretoken: this.csrf},
        success: function(carCount) {
          index = self.cars.indexOf(car);
          self.cars.splice(index, 1);
          self.carCount -= 1;
          self.updateCars();
          $('.dropdown_open').removeClass('dropdown_open');
        }
      });
    },
    openDropdown(e) {
      $(e.target).parents('.dropdown').toggleClass('dropdown_open');
    },
    updateCars() {
      if (this.carsLoading)
        return;

      var self = this;
      this.carsLoading = true

      $.ajax({
        url: this.url,
        cache: false,
        method: 'GET',
        success: function (data) {
          self.pageCount = data.num_pages;
          self.cars = data.cars;
          self.carCount = data.count;
          self.carsLoading = false;
          self.page = data.page;
        },
        error: function (error) {
          self.carsLoading = false;
          console.log(error);
        }
      });
    },
    isScrolledIntoView(elem) {
      var docViewTop = $(window).scrollTop();
      var docViewBottom = docViewTop + $(window).height();

      var elemTop = $(elem).offset().top;
      var elemBottom = elemTop + $(elem).height();

      return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
    },
//    handleScroll: function (event) {
//      elem = $(footer);
//      if (isScrolledIntoView(elem)) {
//        this.updateCars();
//      }
//    }
  }
})

