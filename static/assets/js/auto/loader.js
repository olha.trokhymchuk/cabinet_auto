function getModels (val, callback) {
  var $modelSelect = $('#model_auto');
  var $generationSelect = $('#generation');
  
  if (val) {
    console.log(val)
    $.get(`/auto/models/${val}`, function (data) {
      if(!window.location.href.includes("edit-auto")){
        $modelSelect.empty();
        $modelSelect.html('<option value="" selected=""></option>');
        $generationSelect.empty();
        $generationSelect.html('<option value="" selected=""></option>');
      }
      if (data["popular"].length > 0) {
        var choices = data["popular"].map(function (el) {
          var el = $('<option/>', {
            value: el.id,
          }).text(el.name);
          return el.get(0);
        })
        $modelSelect.append('<optgroup label="Популярные" class="padding10">');
        $modelSelect.append(choices);
        $modelSelect.append('</optgroup>');
        $modelSelect.parent().removeClass('hidden');
      }
      if (data["all_models"].length > 0) {
        var choices = data["all_models"].map(function (el) {
          var el = $('<option/>', {
            value: el.id,
          }).text(el.name);
          return el.get(0);
        })
        
        $modelSelect.append(choices);
        $modelSelect.append('</optgroup>');
        $modelSelect.parent().removeClass('hidden');
      }

      if (callback)
        callback();
    })
  } else {
    $modelSelect.empty();
    $generationSelect.empty();
    if (callback)
        callback();
  }

}

function getGenerations (val, callback) {
  var $generationSelect = $('#generation');

  if (val) {
    $.get(`/auto/generations/${val}`, function (data) {
      $generationSelect.empty();
      $generationSelect.html('<option value="" selected=""></option>');
      if (data.length > 0) {
        var choices = data.map(function (el) {
          var el = $('<option/>', {
            value: el.id,
          }).text(el.name);
          return el.get(0);
        })
        $generationSelect.append(choices)
      }
      if (callback)
        callback();
    })
  } else {
    $generationSelect.empty();
    if (callback)
        callback();
  }
}

function load (mark, model, callback) {
  getModels(mark, function () {
    getGenerations(model, callback);
  })
}

function bindLoaders () {
  $(document).on('change', '#mark_auto', function () {
    var $this = $(this);
    var val = $this.val();
    $('#model_auto').empty();
    $('#generation').empty();
    getModels(val, function () {
      getGenerations($('#model_auto').val());
    })
  });
  setTimeout(() => {
    //addModelAuto();
  },1500);
  function addModelAuto() {
    const $this = $('#mark_auto');
    const val = $this.val();
    getModels(val);
  }
  $(document).on('change', '#model_auto', function () {
    var $this = $(this);
    var val = $this.val();
    $('#generation').empty();
    getGenerations(val);
  })
}

function unBindLoaders () {
  $('#mark_auto').off('change');
  $('#model_auto').off('change');
}

$(document).ready(function () {
  bindLoaders();
})
