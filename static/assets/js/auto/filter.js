$(function() {
  $('#mark_auto').on('change', function() {
    filter.mark = $(this).val();
    filter.model = '';
    filter.models = [];
    filter.generation = '';
    filter.generations = [];
  });

  $('#model_auto').on('change', function() {
    filter.model = $(this).val();
    filter.generation = '';
    filter.generations = [];
  });

  $('#generation').on('change', function() {
    filter.generation = $(this).val();
  });
})

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}

var filter = new Vue({
  delimiters: ['${', '}'],
  el: '#filter',
  data: {
    marks: [],
    models: [],
    generations: [],
    mark: '',
    model: '',
    generation: '',
    sharedState: store.state,
  },
  mounted: function() {
    this.loadMarks();
  },
  watch: {
    mark: function(mark) {
      this.loadModels(mark);
      this.updateFilter()
    },
    model: function(model) {
      this.loadGenerations(model);
      this.updateFilter()
    },
    generation: function(generation) {
      this.updateFilter()
    }
  },
  methods: {
    loadMarks() {
      var self = this;
      $.get("/auto/marks/", function (data) {
        self.marks = data;
      });
    },
    loadModels(mark) {
      var self = this;
      $.get("/auto/models/" + mark, function (data) {
        self.models = data;
      });
    },
    loadGenerations(model) {
      var self = this;
      $.get("/auto/generations/" + model, function (data) {
        self.generations = data;
      });
    },
    updateFilter() {
      this.sharedState.filter = `mark_auto=${this.mark}&model_auto=${this.model}&generation=${this.generation}`;
      var uri = '';
      uri = updateQueryStringParameter(uri, 'mark_auto', this.mark);
      uri = updateQueryStringParameter(uri, 'model_auto', this.model);
      uri = updateQueryStringParameter(uri, 'generation', this.generation);
      uri = updateQueryStringParameter(uri, 'page', 1);
      this.sharedState.filter = uri;
    }
  }
})