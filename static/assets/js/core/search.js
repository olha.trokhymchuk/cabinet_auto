var leftBarSearchState = {
  query: '',
  cars: [],
  orders: [],
  reports: [],
  users: [],
}


var leftBarSearch = new Vue({
  delimiters: ['${', '}'],
  el: '#left-bar-search',
  data: {
    globalState: leftBarSearchState,
    query: '',
    timer: ''
  },
  watch: {
    'query'(value) {
      if (!value.replace(/\s/g, '').length) {
        this.globalState.cars = [];
        this.globalState.orders = [];
        this.globalState.reports = [];
        this.globalState.users = [];
      } else {
        this.globalState.query = this.query;
        this.search();
      }
    }
  },
  methods: {
    search() {
      var self = this;
      clearTimeout(this.timer);
      self.globalState.loading = true;

      this.timer = setTimeout(function() {
        self.searchData(`/order/list_search?&querystring=${self.query}`, 'orders');
        
        self.searchData(`/user/user-list/?&querystring=${self.query}`, 'users');
      }, 3000)
    },
    searchData(url, target) {
      self = this;
      this.getData(url, function(data) {
        self.globalState[target] = data;
        self.globalState.loading = false;
      })
    },
    getData(url, success) {
      return $.ajax({
        url: url,
        cache: false,
        method: 'GET',
        success: function(data) {
          success(data)
        },
        error: function (error) {
          console.log(error);
        }
      });
    }
  }
})