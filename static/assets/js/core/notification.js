
var leftBarNotificationState = {
  notification: []
}

var leftBarNotification = new Vue({
  delimiters: ['${', '}'],
  el: '#left-bar-notification',
  data: {
    notificationState: leftBarNotificationState,
    timer: ''
  },
  watch: {

  },
  methods: {
    get_notification() {
    	let self = this;
    	self.notificationState.loading = true;
    	let url = '/notifications/all_notification';
    	return $.ajax({
			url: url,
			cache: false,
			method: 'GET',
			success: function(data) {
				self.notificationState['notification'] = data;
        self.notificationState.loading = false;
			},
			error: function (error) {
				console.log(error);
			}
		});
	},
  }
})