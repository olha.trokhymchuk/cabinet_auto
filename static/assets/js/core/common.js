function leftBarToggleShow() {
    var bar = $('.left-bar');
    var page = $('#page');
    var leftBarButton = $('.left-bar__toggle_button-js');

    leftBarButton.click(function(){
        bar.toggleClass('hidden');
        page.toggleClass('left-bar-hide');

        localStorage.setItem('left-bar-show', !bar.hasClass('hidden'));
    })


    $(document).on('click', '.left-bar-user__toggle-slide-button__js', function (e) {
        $(e.target).toggleClass('active');
        $('.left-bar-user__toggle-slide').slideToggle(300);
    });
}



$(document).ready(function(){
    // var phone = $('#id_phone').mask('+7 (000) 000-0000');
    var phone = $('#id_phone');



    leftBarToggleShow();

});

