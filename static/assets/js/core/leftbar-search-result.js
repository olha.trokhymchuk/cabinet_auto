var leftBarSearchResult = new Vue({
  delimiters: ['${', '}'],
  el: '#autocomplete',
  data: {
    globalState: leftBarSearchState,
    carsData: {
      count: 0,
      objects: []
    },
    ordersData: {
      count: 0,
      objects: []
    },
    reportsData: {
      count: 0,
      objects: []
    },
    usersData: {
      count: 0,
      objects: []
    }
  },
  watch: {
    'globalState.cars'(data) {
      this.carsData = data;
    },
    'globalState.orders'(data) {
      this.ordersData = data;
    },
    'globalState.reports'(data) {
      this.reportsData = data;
    },
    'globalState.users'(data) {
      this.usersData = data;
    },
  },
})