var leftBarNotificationDataResult = new Vue({
  delimiters: ['${', '}'],
  el: '#notification',
  data: {
    notificationState: leftBarNotificationState,
    notificationData: {
      count: 0,
      objects: []
    },
  },
  watch: {
    'notificationState.notification'(data) {
      console.log('data');
      console.log(data);
      this.notificationData = data;
    }
  },
})