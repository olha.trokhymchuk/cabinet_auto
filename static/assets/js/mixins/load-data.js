var loadData = {
  data: {
    objects: [],
    count: 0,
    loading: false,
    page: {
      num_pages: 1,
      number: 1,
      has_previous: false,
      has_next: false,
      has_other_pages: false,
      start_index: 1,
      end_index: 1
    }
  },
  mounted: function () {
     this.updateObjects();
  },
  methods: {
    updateObjects() {
      if (this.loading)
        return;

      var self = this;
      this.loading = true

      $.ajax({
        url: window.location.href,
        cache: false,
        method: 'GET',
        success: function (data) {
          self.objects = data.objects;
          self.count = data.count;
          self.page = data.page;
          self.loading = false;
        },
        error: function (error) {
          self.loading = false;
          console.log(error);
        }
      });
    }
  }
}
