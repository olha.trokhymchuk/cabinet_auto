/* ------------------------------------------------------------------------------
*
*  # Media gallery
*
*  Specific JS code additions for Gallery pages
*
*  Version: 1.0
*  Latest update: Aug 1, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {

	// Initialize lightbox
    $('[data-popup="lightbox1"]').fancybox({
        padding: 3
    });
    $('[data-popup="lightbox2"]').fancybox({
        padding: 3
    });
    $('[data-popup="lightbox3"]').fancybox({
        padding: 3
    });
});
