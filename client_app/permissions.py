from rest_framework.permissions import BasePermission

from client_app.custom_api_exeptions import Http403
from client_app.models import TokenClientAppModel


class IsUserAuthenticated(BasePermission):
    def has_permission(self, request, view):

        if 'HTTP_AUTHORIZATION' not in request.META:
            raise Http403(detail="Нету токена доступа.")

        token = request.META['HTTP_AUTHORIZATION'].split(" ")[True]

        try:
            TokenClientAppModel.objects.get(token=token)
        except Exception:
            raise Http403(detail="Неверный токен.")

        return True
