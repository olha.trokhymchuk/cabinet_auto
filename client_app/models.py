from django.db import models
from core.models import User


# token for sign in users in client app
class TokenClientAppModel(models.Model):
    class Meta:
        verbose_name = "Доступ к клиентскому приложению"
        verbose_name_plural = "Доступы к клиентскому приложению"

    token = models.CharField(unique=True, max_length=255, null=False, blank=False,
                             verbose_name="Токен пользователя для клиентского приложения")
    user = models.OneToOneField(to=User,
                                on_delete=models.CASCADE, null=False, blank=False, related_name="client_app_token",
                                verbose_name="Пользователь")

    def __str__(self):
        return self.user.first_name + " " + self.user.last_name

