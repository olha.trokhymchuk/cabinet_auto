from django.db.models import Avg
from rest_framework import serializers
from core.models import (
    User
)
from client_app.models import TokenClientAppModel


class UserIdSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True, write_only=True)

    class Meta:
        model = User
        fields = ('id', )


class PhoneSerializer(serializers.Serializer):
    phone = serializers.CharField(required=True, min_length=11)


class CodeSerializer(serializers.ModelSerializer):
    code = serializers.CharField(required=True, min_length=4, write_only=True)
    id = serializers.IntegerField(required=True, write_only=True)

    class Meta:
        model = User
        fields = ('id', 'code')


class TokenSerializer(serializers.ModelSerializer):
    token = serializers.CharField(required=True, write_only=True)

    class Meta:
        model = TokenClientAppModel
        fields = ("token", )


class UserInfoSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'
        fields = ("avatar", #"avatar_thumbnail",
                  "first_name", "last_name", "patronymic",
                  "email", "phone",
                  )
