from rest_framework import serializers

from client_app.constants import TYPE_PHOTO
from core.models import Reviews
from order import order_constants
from order.models import Order
from client_app.serializers.serializers_user import UserInfoSerializer
from client_app.serializers.serializers_auto import AutoSerializer
from report.models import ReportPodbor


class ReviewSerializer(serializers.ModelSerializer):
    title = serializers.CharField(read_only=True)
    class Meta:
        model = Reviews
        fields = "__all__"


class OrderSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField("_status")
    order_type = serializers.SerializerMethodField("_order_type")
    client = UserInfoSerializer()
    expert = UserInfoSerializer(many=True)
    operator = UserInfoSerializer(many=True)
    cars = AutoSerializer(many=True)
    review = ReviewSerializer()

    def _status(self, obj):
        return dict(key=obj.status,
                    value=order_constants.STATUS[obj.status][1])

    def _order_type(self, obj):
        return order_constants.ORDER_TYPE_NAME.get(obj.order_type)

    class Meta:
        model = Order
        fields = '__all__'


class ReportSerializer(serializers.ModelSerializer):
    auto = AutoSerializer()
    executor = UserInfoSerializer()
    status = serializers.SerializerMethodField("_status")
    report_type = serializers.SerializerMethodField("_report_type")

    def _status(self, obj):
        return dict(key=obj.status,
                    value=obj.STATUS[obj.status][1])

    def _report_type(self, obj):
        return dict(key=obj.report_type,
                    value=obj.REPORT_TYPE[obj.report_type][1])

    class Meta:
        model = ReportPodbor
        exclude = ("order",  # TODO add later
                   'lk_id', "notifications", "data", "google_album", "google_album_url",
                   "upload_ftp_status", "success_upload_status")


class GetPhotoReportSerializers(serializers.ModelSerializer):
    id = serializers.IntegerField(required=True, write_only=True)
    type_photo = serializers.ChoiceField(required=True, write_only=True, choices=TYPE_PHOTO)

    class Meta:
        model = ReportPodbor
        fields = ("id", 'type_photo')
