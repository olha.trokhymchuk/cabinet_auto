from rest_framework import serializers

from auto import glossary
from auto.models import Auto, Generation, ModelAuto, MarkAuto, Year, BodyType


class BodyTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BodyType
        fields = "__all__"


class YearSerializer(serializers.ModelSerializer):
    class Meta:
        model = Year
        fields = "__all__"


class MarkAutoSerializer(serializers.ModelSerializer):
    class Meta:
        model = MarkAuto
        fields = "__all__"


class ModelAutoSerializer(serializers.ModelSerializer):
    class Meta:
        model = ModelAuto
        fields = ("name", "id")


class GenerationSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField("_name")

    def _name(self, obj):
        return obj.__str__()

    class Meta:
        model = Generation
        fields = ("name", "id")


class AutoSerializer(serializers.ModelSerializer):
    body_type_auto = BodyTypeSerializer()
    year_auto = YearSerializer()
    mark_auto = MarkAutoSerializer()
    generation = GenerationSerializer()
    model_auto = ModelAutoSerializer()

    transmission_type = serializers.SerializerMethodField("_transmission_type")
    engine_type = serializers.SerializerMethodField("_engine_type")
    drive_type = serializers.SerializerMethodField("_drive_type")
    author_type = serializers.SerializerMethodField("_author_type")
    engine_capacity = serializers.SerializerMethodField("_engine_capacity")

    def _transmission_type(self, obj):
        return dict(key=obj.transmission_type,
                    value=glossary.TRANSMISSION_TYPE[obj.transmission_type][1])

    def _engine_type(self, obj):
        return dict(key=obj.engine_type,
                    value=glossary.ENGINE_TYPE[obj.engine_type][1])

    def _drive_type(self, obj):
        return dict(key=obj.drive_type,
                    value=glossary.DRIVE_TYPE[obj.drive_type][1])

    def _author_type(self, obj):
        return dict(key=obj.author_type,
                    value=glossary.AUTHOR_TYPE[obj.author_type][1])

    def _engine_capacity(self, obj):
        return dict(key=obj.engine_capacity,
                    value=glossary.ENGINE_CAPACITY[int(float(obj.engine_capacity)*10)][1])

    class Meta:
        model = Auto
        exclude = ("data_lk", "notifications", "deleted")
