from django.conf.urls import url

from client_app.views.views_user import *
from client_app.views.views_order import *
from client_app.views.views_create_order import *

prefix = 'client_app_'

urlpatterns = [
    url(r'^check_phone/', CheckPhoneView.as_view(), name=prefix+"check_phone"),
    url(r'^create_code/', CreateCodeView.as_view(), name=prefix+"create_code"),
    url(r'^check_code/', CheckCodeView.as_view(), name=prefix+"check_code"),

    url(r'^get_user_info/', GetUserInfoView.as_view(), name=prefix+"get_user_info"),

    url(r'^order_list/', OrdersListView.as_view(), name=prefix+"order_list"),
    url(r'^order_retrieve/', OrderRetrieveView.as_view(), name=prefix+"order_retrieve"),

    url(r'^review_create/', ReviewCreateView.as_view(), name=prefix+"review_create"),

    url(r'^report_retrieve/', ReportRetrieveView.as_view(), name=prefix+"report_retrieve"),
    url(r'^report_photo/', PhotoReportView.as_view(), name=prefix+"report_photo"),

    url(r'^marks_auto/', MarkAutoListView.as_view(), name="marks_auto"),
    url(r'^models_auto/', ModelAutoListView.as_view(), name="models_auto"),
]
