from rest_framework import generics

from client_app.custom_api_exeptions import Http400
from client_app.serializers.serializers_auto import MarkAutoSerializer, ModelAutoSerializer
from auto.models import MarkAuto, ModelAuto
from client_app.permissions import IsUserAuthenticated


class MarkAutoListView(generics.ListAPIView):
    serializer_class = MarkAutoSerializer
    permission_classes = [IsUserAuthenticated]
    queryset = MarkAuto.objects.all()


class ModelAutoListView(generics.ListAPIView):
    serializer_class = ModelAutoSerializer
    permission_classes = [IsUserAuthenticated]

    def get_queryset(self):
        if 'mark_id' not in self.request.query_params:
            raise Http400(detail="mark_id - обязательное поле.")
        return ModelAuto.objects.filter(mark_auto=self.request.query_params.get("mark_id"))
