from rest_framework import generics, status
from rest_framework.response import Response
from client_app.permissions import IsUserAuthenticated
from client_app.custom_api_exeptions import *
from client_app.helper import (
    get_token_from_header, get_user_by_token,
    has_user_permission_to_order, has_user_permission_to_report,
    generate_photo_dict
)

from client_app.serializers.serializers_order import (
    OrderSerializer, ReviewSerializer, ReportSerializer,
    GetPhotoReportSerializers
)
from core.models import Reviews
from order.models import Order
from report.models import ReportPodbor


class OrdersListView(generics.ListAPIView):
    permission_classes = [IsUserAuthenticated]
    serializer_class = OrderSerializer

    def get_queryset(self):
        user = get_user_by_token(get_token_from_header(self.request))
        return Order.objects.filter(client=user)


class OrderRetrieveView(generics.RetrieveAPIView):
    permission_classes = [IsUserAuthenticated]
    serializer_class = OrderSerializer

    def get_object(self):
        if "id" not in self.request.query_params:
            raise Http400(detail="id - обязательное поле.")

        order = generics.get_object_or_404(Order.objects.all(), id=self.request.query_params.get("id"))
        has_user_permission_to_order(get_token_from_header(self.request),
                                     order)
        return order

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        data = serializer.data
        data['reports'] = [report.id for report in instance.reportpodbor_set.all()]
        return Response(data)


class ReviewCreateView(generics.CreateAPIView):
    permission_classes = [IsUserAuthenticated]
    serializer_class = ReviewSerializer

    def perform_create(self, serializer):
        if 'order_id' not in self.request.data:
            raise Http400(detail="order_id - обязательное поле.")
        order = generics.get_object_or_404(Order.objects.all(), id=self.request.data.get("order_id"))
        has_user_permission_to_order(get_token_from_header(self.request),
                                     order)

        if not order.status_closing:
            raise Http403("Нельзя оставить отзыв, заказ ещё не закрыт.")

        if order.review:
            raise Http403("Нельзя оставить больше одного отзыва.")
        serializer.save()
        instance = Reviews.objects.get(id=serializer.data.get("id"))
        instance.title = "клиентское приложение"
        instance.save()
        order.review = instance
        order.save()
        return instance

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        obj = self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(dict(id=obj.id), status=status.HTTP_201_CREATED, headers=headers)


class ReportRetrieveView(generics.RetrieveAPIView):
    permission_classes = [IsUserAuthenticated]
    serializer_class = ReportSerializer

    def get_object(self):
        if "id" not in self.request.query_params:
            raise Http400(detail="id - обязательное поле.")

        report = generics.get_object_or_404(ReportPodbor.objects.all(), id=self.request.query_params.get("id"))
        has_user_permission_to_report(get_token_from_header(self.request),
                                      report)
        return report


class PhotoReportView(generics.RetrieveAPIView):
    permission_classes = [IsUserAuthenticated]
    serializer_class = GetPhotoReportSerializers

    def get_object(self):
        serializer = self.get_serializer(data=self.request.query_params)
        serializer.is_valid(raise_exception=True)
        report = generics.get_object_or_404(ReportPodbor.objects.all(), id=self.request.query_params.get('id'))
        has_user_permission_to_report(get_token_from_header(self.request), report)
        return report

    def retrieve(self, request, *args, **kwargs):
        return Response(generate_photo_dict(report=self.get_object(),
                                            type_photo=request.query_params.get('type_photo')))



