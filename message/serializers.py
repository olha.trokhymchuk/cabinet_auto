import logging

from django.conf import settings

from rest_framework import serializers

from .models import ModelMessage

logger = logging.getLogger(__name__)


class MessageSerializer(serializers.ModelSerializer):
    text = serializers.CharField(required=False, allow_blank=True, max_length=100)
    created = serializers.DateTimeField(format=settings.DATE_FORMAT, required=False)

    class Meta:
        model = ModelMessage
        depth = 1
        fields = '__all__'

    def get_name(self, obj):
        name = '{}'.format(obj.name)
        return name

    def get_text(self, obj):
    	text = '{}'.format(obj.text)
    	return text


