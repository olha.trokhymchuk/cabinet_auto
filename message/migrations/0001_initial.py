# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-04-19 18:23
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import imagekit.models.fields
import message.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Audio',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=None, max_length=20, null=True, verbose_name='Название аудио')),
                ('audio', imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to=message.models.upload_audio, verbose_name='Audio')),
            ],
            options={
                'verbose_name': 'Аудио',
                'verbose_name_plural': 'Аудио',
                'ordering': ['name'],
            },
        ),
        migrations.CreateModel(
            name='ModelMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(blank=True, default='', verbose_name='Текст сообщения')),
                ('view', models.BooleanField(default=False, verbose_name='отображать/неотображать')),
                ('recieve', models.BooleanField(default=False, verbose_name='получено/неполучено')),
                ('read', models.BooleanField(default=False, verbose_name='прочитано/непрочитано')),
                ('date', models.DateTimeField(auto_now=True, db_index=True, verbose_name='Дата отправления')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Дата редактирования')),
                ('fid', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_fid', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь отправитель')),
            ],
            options={
                'verbose_name': 'Сообщение',
                'verbose_name_plural': 'Сообщения',
                'ordering': ['room'],
            },
        ),
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=None, max_length=20, null=True, verbose_name='Название аудио')),
                ('image', imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to=message.models.upload_photo, verbose_name='Фото')),
                ('message', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_photo', to='message.ModelMessage', verbose_name='Фото')),
            ],
            options={
                'verbose_name': 'Фото',
                'verbose_name_plural': 'Фото',
            },
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=None, max_length=20, null=True, verbose_name='Тема сообщения')),
                ('show', models.BooleanField(default=False, verbose_name='отображать/неотображать')),
                ('date', models.DateTimeField(auto_now_add=True, db_index=True, verbose_name='Дата создания')),
                ('modified', models.DateTimeField(auto_now=True, verbose_name='Дата редактирования')),
                ('locked', models.BooleanField(default=False)),
                ('current', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='current_user', to=settings.AUTH_USER_MODEL, verbose_name='Текущий не прочитанный пользователь')),
                ('rfid', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='room_user_fid', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь отправитель')),
                ('rsid', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='room_user_sid', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь получатель')),
            ],
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(blank=True, default=None, max_length=20, null=True, verbose_name='Название аудио')),
                ('video', imagekit.models.fields.ProcessedImageField(blank=True, null=True, upload_to=message.models.upload_video, verbose_name='Фото')),
                ('message', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_video', to='message.ModelMessage', verbose_name='Видео')),
            ],
            options={
                'verbose_name': 'Видео',
                'verbose_name_plural': 'Видео',
                'ordering': ['name'],
            },
        ),
        migrations.AddField(
            model_name='modelmessage',
            name='room',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='room_id', to='message.Room', verbose_name='Комната'),
        ),
        migrations.AddField(
            model_name='modelmessage',
            name='sid',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_sid', to=settings.AUTH_USER_MODEL, verbose_name='Пользователь получатель'),
        ),
        migrations.AddField(
            model_name='audio',
            name='message',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_audio', to='message.ModelMessage', verbose_name='Аудио'),
        ),
    ]
