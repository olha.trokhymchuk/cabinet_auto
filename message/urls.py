from django.conf.urls import url
from .views import *

app_name = 'message'

urlpatterns = [
    url(r'^view/$', MessageListView.as_view(), name='message_view'),
    url(r'^view/usermessage$', UserMessageListView.as_view(), name='message_detail'),
    url(r'^view/(?P<s_id>\d+)/$', RoomDetailView.as_view(), name='room_detail'),
    url(r'^view/go-room$', RoomDetailView.as_view(), name='go_room'),
    url(r'^send-message$', SendMessageView.as_view(), name='send_message'),
    url(r'^search-user$', SearchUserView.as_view(), name='search_user'),
]