import django_filters
from core.filters import ListFilter
from .models import ModelMessage


class MessageFilter(django_filters.FilterSet):
    date__gte = django_filters.DateTimeFilter(name='date')
    date__lte = django_filters.DateTimeFilter(name='date')

    class Meta:
        model = ModelMessage
        fields = ('date',)


