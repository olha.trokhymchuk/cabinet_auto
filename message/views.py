import logging

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.db.models import Q
from django.contrib.auth.decorators import login_required, permission_required
from django.core.files.base import ContentFile

from custom_admin.utils import FilteredFieldContextMixin

from message.serializers import MessageSerializer
from core.serializers import UserSerializer
from core.utils import ImprovedListView, ImprovedDetailView, ImprovedCreateView, ImprovedTemplateView
from message.filters import MessageFilter
from core.filters import ListFilter
from message.models import ModelMessage, Room, Photo
import datetime
from core.models import User, Filial

logger = logging.getLogger(__name__)

now = datetime.datetime.now()


class MessageListView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView):
    permission_required = 'message.view_room'
    raise_exception = True
    model = User
    template_name = 'message/message_list.html'
    
    serializer = UserSerializer
    json_objects = 'object_list' 
    ordering = '-id'

    def get_context_data(self, **kwargs):
        context = super(MessageListView, self).get_context_data()
        context.update({'html_title': 'Сообщения'})
        context.update({'users': context['user_list']})
        context.update({'count_message': self.count})
        
        return context

    def get_json_data(self, context):
        data = {}
        paginated = context.get('user_list')
        objects = self.serializer(paginated, many=True).data
        data[self.json_objects] = objects
        dict_paginator = self.get_dict_paginator(context)
        count = dict_paginator.get('count')
        self.paginate_by = self.request.POST.get('count_message')
        data.update({
            'page': dict_paginator,
            'count': count,
            'count_message': self.count
        })
        
        return data

    def get_queryset(self):
        user = self.request.user
        queryset = super(MessageListView, self).get_queryset()
        queryset = queryset.exclude(pk=user.pk).prefetch_related('user_sid', 'user_fid').order_by('user_sid__room__date', 'user_fid__room__date')
        if user.is_admin:
            pass
        elif user.is_leader:
            queryset = queryset.filter(filials__in=user.filials.all()) 
        elif user.is_operator:
            queryset = queryset.filter(user_op__in=user.user_op.all())
        elif user.is_client:
            queryset = queryset.filter(Q(user_ex__client=user)|Q(user_op__client=user))
        elif user.is_expert:
            queryset = queryset.filter(user_cl__expert_check=user)
        elif user.is_acounter or user.is_hr:
            queryset = queryset.exclude(groups__name='Клиент')

        room = Room.objects.filter(current=user)
        self.count = 0
        for r in room:
            messagequery = ModelMessage.filter(date__gt=r.modified)
            self.count = self.count + messagequery.count()
        self.create_object_list(queryset)
        return queryset.distinct()


class SearchUserView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedTemplateView):
    permission_required = 'message.view_room'
    raise_exception = True
    model = User
    filterset_class = MessageFilter
    serializer = MessageSerializer
    json_objects = 'objects'
    ordering = '-id'

    def get_context_data(self, **kwargs):
        context = super(SearchUserView, self).get_context_data()

        return context

    def get_object(self):
        object = self.model.objects.filter(first_name__contains=self.request.POST.get('search-name'))
        return object

    def get_json_data(self, context):
        data = [{'context': context}]
        return data


class RoomDetailView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedDetailView):
    permission_required = 'message.view_room'
    raise_exception = True
    model = Room
    pk_url_kwarg = 'room_id'
    template_name = 'message/message_detail.html'

    def get_object(self, queryset=None):
        user = self.request.user
        self.rsid = self.kwargs['s_id']
        try:
            self.rsid = User.objects.get(pk=self.rsid).pk
        except User.DoesNotExist:
            self.user_not_exist = "пользователь не найден"
            return []

        try:
            self.room = self.model.objects.get((Q(rsid_id=user.pk) & Q(rfid_id=self.rsid)) | (Q(rsid_id=self.rsid) & Q(rfid_id=user)))
        except Room.DoesNotExist:
            self.room = self.model.objects.create(rfid_id=user.pk, rsid_id=self.rsid)
        if(self.room == []):
            self.room = self.model.objects.create(rfid_id=user.pk, rsid_id=self.rsid)
        self.kwargs['room_id'] = self.room.pk

        return self.room


    def get_context_data(self, **kwargs):
        context = super(RoomDetailView, self).get_context_data(**kwargs)
        if(hasattr(self, 'user_not_exist')):
            context.update({'user_not_exist': self.user_not_exist})
            return context
        context.update({'html_title': 'Сообщение {}'.format(self.object)})
        self.rfid = self.room.rfid_id if self.room.rfid_id != self.rsid else self.room.rsid_id
        context.update({'fid': self.rfid})
        context.update({'sid': self.rsid})

        return context

    def get_json_data(self, context):
        messages = ModelMessage.objects.prefetch_related('user_photo').filter(room__pk=self.kwargs['room_id'])

        data = [{'text': message.text, 'fid': message.fid.first_name, 'sid': message.sid.first_name, 'img': self.get_photo(message)} for message in messages]
        return data

    def get_photo(self, message):
        photos = message.user_photo.all()
        result = []
        for photo in photos:
            result.append(photo.image.url)
        return result


# Просмотр cообщений пользователя
class UserMessageListView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView):
    permission_required = 'message.view_room'
    raise_exception = True
    model = ModelMessage
    template_name = 'message/message_detail.html'

    def get_queryset(self):
        user = self.request.user
        queryset = super(UserMessageListView, self).get_queryset()
        room = Room.objects.filter(current=user)
        self.count = 0
        for r in room:
            queryset = queryset.filter(date__gt=r.modified)
            self.count = self.count + queryset.count()
        return queryset

    def get_context_data(self, **kwargs):
        context = {}
        context.update({'html_title': 'Сообщения {}'.format('self.object_list')})

        return context

    def get_json_data(self, context):
        data = {'count': self.count}
        return data


# Добавить автомобиль в базу
class SendMessageView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedCreateView):
    permission_required = 'message.view_room'
    raise_exception = True
    model = ModelMessage
    action = 'create'
    fields = ['text']

    def get_context_data(self, **kwargs):
        context = super(SendMessageView, self).get_context_data(**kwargs)
        context.update({'html_title': 'Отправить Сообщение'})
        return context

    def get_object(self, **kwargs):
        object = {}
        self.update = False
        text = self.request.POST.get('text')
        fid = self.request.POST.get('fid')
        sid = self.request.POST.get('sid')
        pk = self.request.POST.get('id')
        self.load = self.request.POST.get('load') if self.request.POST.get('load') != 'undefined' else 0
        self.user = self.request.user
        img = self.request.FILES.get('img')
        try:
            data_img = img.read()
        except (TypeError, AttributeError):
            data_img = ''
        try:
            room = Room.objects.get((Q(rsid_id=sid) & Q(rfid_id=fid)) | (Q(rsid_id=fid) & Q(rfid_id=sid)))
        except Room.DoesNotExist:
            room = Room.objects.create(rfid_id=fid, rsid_id=sid)
        self.pk = room.pk

        # если в комнате есть текущий пользователь кому сообщение то удаляем его
        # и делаем пометку о прочтении
        if(room.current_id is not None and int(room.current_id) == int(self.user.pk)):
            self.update = True
            room.current_id=''
        if(int(self.load) == 3):
            # если нет текущего пользователя которому шлется сообщение записываем его
            if(room.current_id == '' or room.current_id == None or int(room.current_id) != int(sid)):
                room.current_id=int(sid)
                room.modified=now   
                room.save()
            # отправляем сообщение
            create = self.model.objects.create(text=text, fid_id=fid, sid_id=sid, room_id=self.pk, date=now)
            photo = Photo(message_id=create.pk)
            try:
                photo.image.save('', ContentFile(data_img))
            except OSError:
                pass
            else:
                img = photo.save()
                img = photo.image.url

        # отправляем сообщение и возвращаем на страницу объект сообщения
        # если нету пометки обновить чат
        if(self.update==False and int(self.load) == 3):
            object.update({'text': text})
            object.update({'fid': fid})
            object.update({'sid': sid})
            object.update({'pk': pk})
            object.update({'img': img})
            return object
        # дествие при загрузке страницы,  подтянуть все сообщения  
        elif(int(self.load) == 1):
            room.save()
            object = ModelMessage.objects.prefetch_related('user_photo').filter(room__pk=self.pk)
        # переодическое обновление если парамерт для подгрузки то получаем сообщение
        elif(int(self.load) == 2 and self.update==True):
            object = ModelMessage.objects.prefetch_related('user_photo').filter(room__pk=self.pk, date__gt=room.modified)
            room.save()
        return object

    def get_photo(self, message):
        photos = message.user_photo.all()
        result = []
        for photo in photos:
            result.append(photo.image.url)
        return result

    def get_json_data(self, context):
        if(self.update==False and int(self.load) == 3):
            user = User.objects.get(pk=int(context['object']['fid']))
            context['object']['fid'] = user.first_name
            user = User.objects.get(pk=context['object']['sid'])
            context['object']['sid'] = user.first_name      
            return [context['object']]
        else:
            try:
                return [{'text': message.text, 'fid': message.fid.first_name, 'sid': message.sid.first_name, 'img': self.get_photo(message)} for message in context['object']]
            except KeyError:
                return []


    def get_form(self):
        return '';


    def form_invalid(self, form):
        return JsonResponse(status=200, data={'errors': form.errors})

