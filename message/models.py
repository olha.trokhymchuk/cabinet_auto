from django.contrib.postgres.fields import IntegerRangeField
from django.contrib.postgres.validators import RangeMinValueValidator
from django.db import models
from imagekit.models import ProcessedImageField, ImageSpecField
from safedelete import HARD_DELETE
from safedelete.models import SafeDeleteModel

from auto.glossary import *
from core.utils import CustomFieldHistoryTracker

from report.utils import OriginalSpec, ThumbnailSpec
from core.models import User

from notifications.models import Notification


now = datetime.datetime.now()
year = now.strftime("%Y")
month = now.strftime("%m")
day = now.strftime("%d")
time = now.strftime("%H%M%S")

def upload_video(instance, filename):
    filebase, extension = filename.split(".")
    return "message/video/%s/%s/%s/id_%s/auto_%s.%s" % (year, month, day, instance.auto.id, time, extension)

def upload_audio(instance, filename):
    filebase, extension = filename.split(".")
    return "message/audio/%s/%s/%s/id_%s/auto_%s.%s" % (year, month, day, instance.auto.id, time, extension)

def upload_photo(instance, filename):
    filebase, extension = filename.split(".")
    return "message/photo/%s/%s/%s/id_%s/auto_%s.%s" % (year, month, day, instance.message.id, time, extension)


class Room(models.Model):
    name = models.CharField('Тема сообщения', max_length=20, blank=True, null=True, default=None)
    rsid = models.ForeignKey(User, blank=True, null=True, default=None, verbose_name='Пользователь получатель', related_name="room_user_sid")
    rfid = models.ForeignKey(User, blank=True, null=True, default=None, verbose_name='Пользователь отправитель', related_name="room_user_fid")
    show = models.BooleanField('отображать/неотображать', default=False)
    date = models.DateTimeField('Дата создания', auto_now_add=True, db_index=True)
    modified = models.DateTimeField('Дата редактирования', auto_now=True)
    locked = models.BooleanField(default = False)
    current = models.ForeignKey(User, blank=True, null=True, default=None, verbose_name='Текущий не прочитанный пользователь', related_name="current_user")



class ModelMessage(models.Model):
    room = models.ForeignKey(Room, verbose_name='Комната', related_name="room_id")
    sid = models.ForeignKey(User, blank=True, null=True, default=None, verbose_name='Пользователь получатель', related_name="user_sid")
    fid = models.ForeignKey(User, blank=True, null=True, default=None, verbose_name='Пользователь отправитель', related_name="user_fid")
    text = models.TextField(verbose_name='Текст сообщения', default='', blank=True)
    view = models.BooleanField('отображать/неотображать', default=False)
    recieve = models.BooleanField('получено/неполучено', default=False)
    read = models.BooleanField('прочитано/непрочитано', default=False)
    date = models.DateTimeField('Дата отправления', auto_now=True, db_index=True)
    modified = models.DateTimeField('Дата редактирования', auto_now=True)
    notifications = models.ManyToManyField(Notification, blank=True, default=None, verbose_name='Оповещения', related_name="mesege_notifications")

    class Meta:
        verbose_name = "Сообщение"
        verbose_name_plural = "Сообщения"
        ordering = ["room"]

    def __str__(self):
        return self.fid.first_name


class Video(models.Model):
    name = models.CharField('Название аудио', max_length=20, blank=True, null=True, default=None)
    video = ProcessedImageField(upload_to=upload_video, blank=True, null=True, verbose_name='Фото',
                                spec=OriginalSpec, autoconvert=None)
    message = models.ForeignKey(ModelMessage, blank=True, null=True, default=None, verbose_name='Видео', related_name="user_video")
    
    
    class Meta:
        verbose_name = "Видео"
        verbose_name_plural = "Видео"
        ordering = ["name"]

    def __str__(self):
        return self.name


class Audio(models.Model):
    name = models.CharField('Название аудио', max_length=20, blank=True, null=True, default=None)
    audio = ProcessedImageField(upload_to=upload_audio, blank=True, null=True, verbose_name='Audio',
                                spec=OriginalSpec, autoconvert=None)
    message = models.ForeignKey(ModelMessage, blank=True, null=True, default=None, verbose_name='Аудио', related_name="user_audio")
    
    
    class Meta:
        verbose_name = "Аудио"
        verbose_name_plural = "Аудио"
        ordering = ["name"]

    def __str__(self):
        return self.name


class Photo(models.Model):
    name = models.CharField('Название аудио', max_length=20, blank=True, null=True, default=None)
    image = ProcessedImageField(upload_to=upload_photo, blank=True, null=True, verbose_name='Фото',
                                spec=OriginalSpec, autoconvert=None)
    message = models.ForeignKey(ModelMessage, blank=True, null=True, default=None, verbose_name='Фото', related_name="user_photo")
    class Meta:
        verbose_name = "Фото"
        verbose_name_plural = "Фото"

