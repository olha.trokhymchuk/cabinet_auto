from django.contrib import admin
from safedelete.admin import highlight_deleted, SafeDeleteAdmin

from .models import ModelMessage, Room, Photo
from field_history.models import FieldHistory

admin.site.unregister(FieldHistory)


class AdminModelMessage(admin.ModelAdmin):
    list_display = ('id', 'fid', 'sid')
    exclude = ('notifications',)


class AdminRoom(admin.ModelAdmin):
    list_display = ('id', 'rfid', 'rsid')


admin.site.register(ModelMessage, AdminModelMessage)
admin.site.register(Room, AdminRoom)
admin.site.register(Photo)



