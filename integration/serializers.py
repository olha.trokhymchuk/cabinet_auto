import logging
import json
import copy
import six
import re 

from custom_admin.utils import get_redis

from .glosary import *
from auto.glossary import * 
import order.order_constants as order_const

from django.conf import settings
from django.db.utils import IntegrityError
from psycopg2 import IntegrityError as PgIntegrityError

from django.contrib.auth.base_user import BaseUserManager
from django.db import transaction

from rest_framework import serializers
from rest_framework.response import Response

from django.contrib.auth.models import Group

from core.models import User, Filial

from order.models import Order
from auto.models import MarkAuto, BodyType

from service.models import PodborAuto

from notifications.utils import send_notification
from notifications.models import Notification

logger = logging.getLogger(__name__)


class BodyTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = BodyType
        fields = ('id',)


class FilialSerializer(serializers.ModelSerializer):

    class Meta:
        model = Filial
        fields = '__all__'

class MarkAutoSerializer(serializers.ModelSerializer):

    class Meta:
        model = MarkAuto
        fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
	phone = serializers.CharField()
	email = serializers.CharField(allow_blank=True, allow_null=True, required=False)

	class Meta:
		model = User
		fields = ('first_name','phone', 
					'last_name', 'patronymic', 'email')

	def validate_phone(self, value):
		logger.error(value)
		return value

	def validate_username(self, value):
		return value

	def validate_email(self, email):
		return email

class ClientSerializer(UserSerializer):
	last_name = serializers.CharField(allow_blank=True, allow_null=True)


class ExpertSerializer(UserSerializer):
	phone = serializers.CharField(allow_blank=True, allow_null=True)
	
class OperatorSerializer(UserSerializer):
	phone = serializers.CharField(allow_blank=True, allow_null=True,)
	last_name = serializers.CharField(allow_blank=True, allow_null=True)
	first_name = serializers.CharField(allow_blank=True, allow_null=True)
		

class PodborAutoCreateUpdateSerializer(serializers.ModelSerializer):	
	engine_capacity = serializers.JSONField()
	year_auto = serializers.JSONField()
	cost = serializers.JSONField()
	number_of_hosts = serializers.CharField(allow_blank=True, allow_null=True, required=False)
	mileage = serializers.CharField(allow_blank=True, allow_null=True, required=False)

	horsepower = serializers.JSONField()
	body_type_auto = serializers.JSONField()

	def validate_body_type_auto(self, obj):
		return obj

	def validate_mileage(self, mileage):
		try:
			return int(mileage)
		except (ValueError,TypeError) as ex:
			logger.error(ex)
		return 0


	def validate_number_of_hosts(self, obj):
		if not obj:
			return 1
		return obj

	def validate_cost(self, cost):
		try:
			return int(cost)
		except ValueError as ex:
			logger.error(ex)
		return 0

	def validate_engine_capacity(self, obj):
		engine_capacity = [None, None]
		if obj:
			try:
				engine_capacity[0] = float(obj['lower'])
			except (KeyError, TypeError) as ex:
				pass
			try:
				engine_capacity[1] = float(obj['upper'])
			except (KeyError, TypeError) as ex:
				pass
		return engine_capacity

	def validate_horsepower(self, obj):
		horsepower = [None, None]
		if obj:
			try:
				horsepower[0] = int(obj['lower'])
			except (KeyError, TypeError) as ex:
				pass
			try:
				horsepower[1] = int(obj['upper'])
			except (KeyError, TypeError) as ex:
				pass
		return horsepower

	def validate_year_auto(self, obj):
		year_auto = [None, None]
		if obj:
			try:
				year_auto[0] = int(obj['lower'])
			except (KeyError,TypeError) as ex:
				pass
			try:
				year_auto[1] = int(obj['upper'])
			except (KeyError,TypeError) as ex:
				pass
		return year_auto

	class Meta:
		model = PodborAuto
		fields = ('mileage','year_auto','cost',
				  'number_of_hosts','comment',
				  'mark_auto','model_auto','generation',
				  'engine_capacity','year_auto','horsepower',
				  'transmission_type','engine_type','body_type_auto',
				  'color_auto','equipment','drive_type','color_salon',
				  'salon_auto'
				)

class OrderCreateUpdateSerializer(serializers.ModelSerializer):
	number_buh = serializers.CharField()
	podborauto_set = PodborAutoCreateUpdateSerializer(many=True)
	client = ClientSerializer(many=True)
	filial = FilialSerializer(many=True)
	operator = OperatorSerializer(many=True)
	expert = ExpertSerializer(many=True)
	status_closing = serializers.BooleanField(default=False)
	cost = serializers.IntegerField()

	TRY_SET_CLIENT = 5

	class Meta:
		model = Order
		fields = ('number_buh','cost', 'podborauto_set', 'client', 'operator', 
					'expert','filial','status','paid','order_type',
					'comment','status_closing')

	def validate_number_buh(self, number_buh):
		self.order = Order.objects.filter(number_buh=number_buh).first()
		return number_buh
	
	def validate_status(self, status):
		if status in order_const.SETCLOSING_WITH_STATUS:
			self.status_closing = True
		if status == order_const.REFUSAL and not self.order:
			raise serializers.ValidationError(order_const.REFUSAL)
		return status

	
	def get_filial(self):
		try:
			code = self.filial[0]['name']
			logger.error(code)
			return Filial.objects.get(code=code)
		except (KeyError, Filial.DoesNotExist) as ex:
			logger.error(ex)
			return False

	def set_client(self, client_data, validated_data):
		self.phone_client = self.phone_normalize(self.phone_client)
		logger.error(self.phone_client)
		try:
			client = User.objects.get(phone=self.phone_client)
		except (Exception, User.DoesNotExist) as ex:
			logger.error(ex)
			try:
				client = User(phone=self.phone_client, **client_data[0])
				client.save()
			except:
				while self.TRY_SET_CLIENT > 0:
					self.TRY_SET_CLIENT = self.TRY_SET_CLIENT - 1
					self.set_client(client_data, validated_data)
			else:
				self.notification_client(client)
				group_client = Group.objects.get(name='Клиент')
				client.groups.add(group_client)
		else:
			if client_data[0]['first_name'] != client.first_name:
				client.first_name = client_data[0]['first_name']
				client.save()
		validated_data.update({'client':client})
		try:
			if self.filial:
				client.save()
				client.filials.add(self.filial)
		except (PgIntegrityError, IntegrityError, NameError) as ex:
			logger.error(ex)

	def create_password(self):
		return User.objects.make_random_password(8, allowed_chars='1234567890')

	def phone_normalize(self, phone):
		phone = ''.join(re.findall(r'\d+', phone))
		if str(phone[0]) == str(8):
			phone = "7" + phone[1:]
		return phone

	def notification_client(self, user):
		password = self.create_password()
		while not password or password == None:
			password = self.create_password()

		user.set_password(password)
		user.save()
		data = {
			'name': user.get_full_name(),
			'username': user.phone,
			'password': password
		}
		channels = ['lk', 'sms', 'email']
		send_notification([user], Notification.REGISTRATION, data, channels=channels)

	
		

	def create(self, validated_data):
		with transaction.atomic():
			number_buh = validated_data.pop('number_buh')
			try:
				validated_data['status_closing'] = self.status_closing
			except AttributeError as ex:
				logger.error(ex)

			podbors_data = validated_data.pop('podborauto_set')
			client_data = validated_data.pop('client')
			operator_data = validated_data.pop('operator')
			experts_data = validated_data.pop('expert')
			self.filial = validated_data.pop('filial')
			self.filial = self.get_filial()
			logger.error(self.filial)
			
			try:
				phone_operator = self.phone_normalize(operator_data[0].pop('phone'))
			except TypeError as ex:
				logger.error(ex)
			try:
				phone_expert = self.phone_normalize(experts_data[0].pop('phone'))
			except TypeError as ex:
				logger.error(ex)
			group_operator = Group.objects.get(name='Оператор')
			group_expert = Group.objects.get(name='Эксперт')
			self.phone_client = re.sub('\D','', client_data[0].pop('phone'))
			self.set_client(client_data, validated_data)
			try:
				operator = User.objects.get(phone=str(phone_operator))
				operator.groups.add(group_operator)
			except User.DoesNotExist as ex:
				operator = User.objects.create(phone=str(phone_operator), **operator_data[0])
				operator.groups.add(group_operator)
			except NameError as ex:
				logger.error(ex)

			try:
				operator.filials.add(self.filial)
				operator.save()
			except (PgIntegrityError, IntegrityError, NameError) as ex:
				logger.error(ex)

			try:
				expert = User.objects.get(phone=str(phone_expert))
			except (User.DoesNotExist, NameError) as ex:
				try:
					expert = User.objects.create(phone=str(phone_expert), **experts_data[0])
				except (NameError) as ex:
					logger.error(ex)
				else:
					expert.groups.add(group_expert)
			else:
				expert.groups.add(group_expert)
			
			try:
				expert.filials.add(self.filial)
				expert.save()
			except (NameError) as ex:
				logger.error(ex)

			validated_data.update({'filial':self.filial, 'submit_from':'CRM'})
			logger.error(self.order)
			logger.error(number_buh)
			if not self.order:			
				self.order = Order.objects.create(number_buh=number_buh, **validated_data)
			else:						
				for k, v in six.iteritems(validated_data):
					setattr(self.order, k, v)
				self.order.save()
			if self.order:
				for e in self.order.expert.all():
					self.order.expert.remove(e.pk)
				try:
					self.order.expert.add(expert)
				except (NameError, IntegrityError) as ex:
					logger.error(ex)
				for o in self.order.operator.all():
					self.order.operator.remove(o.pk)
				try:
					self.order.operator.add(operator)
				except NameError as ex:
					logger.error(ex)
				self.order.podborauto_set.all().delete()

				for podbor_data in podbors_data:
					body_type_auto = podbor_data.pop('body_type_auto')
					podbor = PodborAuto.objects.create(order=self.order, **podbor_data)
					try:
						for body in body_type_auto:
							podbor.body_type_auto.add(body['id'])
							podbor.save()
					except (KeyError, IntegrityError) as ex:
						logger.error(ex)
			#r.delete(number_buh)
			return self.order
