import io
import os
import re
import json
import logging
import datetime

from json.decoder import JSONDecodeError

from django.conf import settings

from django.http import HttpResponse

from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.views import APIView

from rest_framework.permissions import IsAuthenticated

from .glosary import *
import order.order_constants as order_const

from .serializers import OrderCreateUpdateSerializer
from rest_framework import serializers

from order.models import Order
from auto.models import MarkAuto, ModelAuto, Generation, Year
from service.models import PodborAuto
from urllib import parse

logger = logging.getLogger(__name__)


class Integration(object):
    def get_key_value(self, key, response_str):
        first = None
        last = None
        for m in re.finditer('[&]+[\S]{,1}[\s]*[=]+', response_str):
            if key in str(m):
                t = m.span()
                first = t[1]
                continue
            if first:
                t = m.span()
                last = t[0]
                return response_str[first:last]
        if not last:
            return response_str[first:m.endpos]

    def get_integration_context(self):
        self.data_crm = {'podborauto_set': []}
        orderData = self.request.data['orderData']
        order_data_str = self.request.read().decode()
        logger.error(order_data_str)

        # orderData = re.split(r'orderData=', order_data_str)

        try:
            orderData = json.loads(r'''{}'''.format(orderData))
        except JSONDecodeError as ex:
            orderData = self.get_key_value('orderData', order_data_str)
            try:
                orderData = json.loads(orderData)
            except JSONDecodeError as ex:
                logger.error('stop')
                return False
        data_from_order = datetime.datetime.strptime(
            DATE_FROM_ORDER, "%Y-%m-%d %H:%M:%S")
        self.data_crm.update({'number_buh': orderData['id']})
        status = self.get_order_status(orderData['status'])
        order = Order.objects.filter(
            number_buh=self.data_crm['number_buh']).first()
        if not order and status == order_const.REFUSAL:
            logger.error('REFUSAL')
            return order_const.REFUSAL

        now_day_order = datetime.datetime.strptime(
            orderData['statusUpdatedAt'], "%Y-%m-%d %H:%M:%S")
        if data_from_order > now_day_order:
            logger.error("now_day_order")
            return False
        client = orderData['customer']
        manager = orderData['manager']
        try:
            offer = orderData['items'][0]['offer']
        except TypeError as ex:
            logger.error(ex)
            logger.error(500)
            raise serializers.ValidationError(
                "items is none error: {}".format(str(ex)))
            return 500
        customFields = orderData['customFields']
        self.data_crm.update({'comment': orderData['customerComment']})
        auto_param_to = 6
        self.order_type = self.get_order_type(offer['externalId'])
        if status not in order_const.GET_FROM_CRM:
            logger.error(status)
            logger.error("not in GET_FROM_CRM")
            return False
        if status in order_const.SETCLOSING_WITH_GETCRM:
            self.data_crm.update({'SETCLOSING_WITH_GETCRM': True})
            logger.error('SETCLOSING_WITH_GETCRM')
            logger.error(status)
            return False
        if self.order_type != PPK:
            auto_param_to = 2

        for i in range(1, auto_param_to):
            status_auto_param = self.set_auto_param(i, customFields, orderData)
            if status_auto_param == 'break' or not status_auto_param:
                break
        self.data_crm.update({'status': status})
        self.data_crm.update({'cost': orderData['totalSumm']})
        self.data_crm.update({'order_type': self.order_type})
        paid = ["holding", "paid"]
        not_payed = ["not-paid", "cancelled"]
        paid_sum = 0
        try:
            for pyment in orderData['payments']:
                if pyment['status'] in paid:
                    paid_sum += round(pyment['amount'])
            print(paid_sum)
            self.data_crm.update({'paid': paid_sum})
        except(TypeError, KeyError) as ex:
            logger.error(ex)
        try:
            firstName = client['firstName']
        except KeyError as ex:
            logger.error(ex)
            firstName = ' '
        try:
            lastName = client['lastName']
        except KeyError as ex:
            logger.error(ex)
            lastName = ' '
        try:
            patronymic = client['patronymic']
        except KeyError as ex:
            logger.error(ex)
            patronymic = ''
        try:
            email = client['email']
        except KeyError as ex:
            logger.error(ex)
            email = ''
        try:
            phone = self.check_phone(client['phones'])

        except KeyError as ex:
            logger.error(ex)
            phone = ''
        try:
            contragent = client['contragent']
            contragentType = contragent['contragentType']
            legalName = contragent['legalName']
            if (contragentType == "legal-entity" or contragentType == "enterpreneur") and legalName:
                firstName = legalName
                lastName = ''
                patronymic = ''
                pass
        except KeyError as ex:
            logger.error(ex)
        self.data_crm.update({'client': [{'first_name': firstName,
                                          'last_name': lastName,
                                          'patronymic': patronymic,
                                          'email': email,
                                          'phone': phone}
                                         ]})
        operator_phone = manager['phone']
        self.data_crm.update({'operator': [{'first_name': manager['firstName'],
                                            'last_name': manager['lastName'],
                                            'patronymic': manager['patronymic'],
                                            'phone': operator_phone,
                                            'email': manager['email']}
                                           ]})
        expert_num = customFields['expert_num']
        self.data_crm.update({'expert': [{'phone': expert_num}]})
        try:
            expert_name = customFields['expertname'].split()
            expert_first_name = expert_name[0]
            expert_last_name = expert_name[1]
            expert_patronymic = expert_name[2]
        except (IndexError, AttributeError) as ex:
            logger.error(ex)
            expert_first_name = ''
            expert_last_name = ''
            expert_patronymic = ''
        else:
            self.data_crm['expert'][0].update(
                {'first_name': expert_first_name})
        try:
            self.data_crm['expert'][0].update({'last_name': expert_last_name})
        except IndexError as ex:
            logger.error(ex)
        try:
            self.data_crm['expert'][0].update(
                {'patronymic': expert_patronymic})
        except IndexError:
            pass
        self.data_crm.update({'filial': [{'name': customFields['regions']}]})
        logger.error('order send')
        return True

    def check_phone(self, phones):
        default_phone = ''
        for x in range(len(phones)):
            phone = re.sub(r' ', '', phones[x]['number'])
            phone = ''.join(re.findall(r'\d+', phone))
            if x == 0:
                default_phone = phone
            if len(str(phone)) == 10 or len(str(phone)) == 11 or len(str(phone)) == 12:
                return phone
        return default_phone

    def set_auto_param_all(self, i, avto_param, orderData):
        engine_capacity = {}
        try:
            if avto_param['engineval-from'] != '' and avto_param['engineval-from'] != avto_param['engineval-to']:
                engine_capacity.update({"lower": avto_param['engineval-from']})

        except (KeyError, TypeError) as ex:
            logger.error(ex)
        try:
            if avto_param['engineval-to'] != '':
                engine_capacity.update({"upper": avto_param['engineval-to']})
        except (KeyError, TypeError) as ex:
            logger.error(ex)

        year_auto = {}
        try:
            if avto_param['year-from'] != '':
                year_auto.update({"lower": avto_param['year-from']})
        except (KeyError, TypeError) as ex:
            logger.error(ex)

        horsepower = {}
        try:
            if not avto_param['power-from'] == '':
                horsepower.update({"lower": avto_param['power-from']})
        except (KeyError, TypeError) as ex:
            logger.error(ex)
        try:
            if not avto_param['power-to'] == '':
                horsepower.update({"upper": avto_param['power-to']})
        except (KeyError, TypeError) as ex:
            logger.error(ex)
        try:
            color_auto = self.get_color_auto(avto_param['color'])
        except (KeyError, TypeError) as ex:
            color_auto = []
            logger.error(ex)
        try:
            transmission_type = self.get_transmission_type(
                avto_param['transmission'])
        except (KeyError, TypeError) as ex:
            transmission_type = []
            logger.error(ex)
        try:
            engine_type = self.get_engine_type(avto_param['engine'])
        except (KeyError, TypeError) as ex:
            engine_type = []
            logger.error(ex)
        try:
            color_salon = self.get_color_salon(avto_param['salon-color'])
        except (KeyError, TypeError) as ex:
            color_salon = []
            logger.error(ex)
        try:
            salon_auto = self.get_salon(avto_param['salon'])
        except (KeyError, TypeError) as ex:
            salon_auto = []
            logger.error(ex)
        try:
            drive_type = self.get_drive_type(avto_param['actuator'])
        except (KeyError, TypeError) as ex:
            drive_type = []
            logger.error(ex)

        try:
            equipment = avto_param['assembling']
        except (KeyError, TypeError) as ex:
            equipment = ''
            logger.error(ex)
        try:
            number_of_hosts = avto_param['owner']
        except (KeyError, TypeError) as ex:
            number_of_hosts = 0
        try:
            comment = avto_param['comment']
        except (KeyError, TypeError) as ex:
            comment = ''
            logger.error(ex)

        self.data_crm['podborauto_set'].append({
            'engine_capacity': engine_capacity,
            'horsepower': horsepower,
            'year_auto': year_auto,
            'transmission_type': transmission_type,
            'engine_type': engine_type,
            'color_auto': color_auto,
            'color_salon': color_salon,
            'salon_auto': salon_auto,
            'equipment': equipment,
            'drive_type': drive_type,
            'number_of_hosts': number_of_hosts,
            'comment': comment
        })
        self.get_mileage(i, avto_param)
        self.get_cost(i, avto_param)
        try:
            bodycar = {'body_type_auto': self.get_body_car(
                avto_param['bodycar'])}
        except (KeyError, TypeError) as ex:
            bodycar = {'body_type_auto': [{}]}

        self.data_crm['podborauto_set'][i].update(bodycar)
        mark_auto = self.get_mark(i, avto_param)
        try:
            model_auto = self.get_model(i, mark_auto, avto_param)
        except Exception as ex:
            logger.error(ex)
        try:
            self.get_generation(i, model_auto, avto_param)
        except Exception as ex:
            logger.error(ex)
        return True

    def get_mileage(self, i, avto_param):
        try:
            mileage = avto_param['km']
        except (KeyError, TypeError) as ex:
            mileage = ''
            logger.error(ex)
        self.data_crm['podborauto_set'][i].update({'mileage': mileage})

    def get_cost(self, i, avto_param):
        try:
            cost = avto_param['budget']
        except (KeyError, TypeError) as ex:
            cost = ''
            logger.error(ex)
        self.data_crm['podborauto_set'][i].update({'cost': cost})

    def get_generation(self, i, model_auto, avto_param):
        try:
            generation = Generation.objects.filter(name=avto_param['generation'],
                                                   model_auto=model_auto['model_auto']
                                                   ).first().pk
        except (KeyError, TypeError, NameError, AttributeError, Generation.DoesNotExist) as ex:
            logger.error(ex)
            if "has no attribute 'pk'" in str(ex.args):
                generation = Generation.objects.create(name=avto_param['generation'],
                                                       model_auto=model_auto_inst
                                                       )
                generation = {'generation': generation.pk}

        else:
            generation = {'generation': generation}
        self.data_crm['podborauto_set'][i].update(generation)
        return generation

    def get_mark(self, i, avto_param):
        try:
            logger.error(avto_param['mark'].lower())
            mark_auto_inst = MarkAuto.objects.get(
                name__iexact=avto_param['mark'].lower())
            mark_auto = mark_auto_inst.pk
        except (AttributeError, MarkAuto.DoesNotExist) as ex:
            try:
                mark_auto_inst = MarkAuto.objects.filter(
                    name__icontains=avto_param['mark'].lower()).first()
                mark_auto = mark_auto_inst.pk
            except (KeyError, TypeError, AttributeError, MarkAuto.DoesNotExist) as ex:
                return
            else:
                mark_auto = {'mark_auto': mark_auto}
                self.data_crm['podborauto_set'][i].update(mark_auto)
        else:
            mark_auto = {'mark_auto': mark_auto}
            self.data_crm['podborauto_set'][i].update(mark_auto)

        return mark_auto

    def get_model(self, i, mark_auto, avto_param):
        model = avto_param['model'].lower()
        try:
            model = integration_model[model]
        except KeyError as ex:
            logger.error(ex)
        try:
            model_auto_inst = ModelAuto.objects.filter(name=model,
                                                       mark_auto=mark_auto['mark_auto']
                                                       ).first()
            model_auto = model_auto_inst.pk
        except (KeyError, TypeError, NameError, AttributeError, ModelAuto.DoesNotExist) as ex:
            logger.error(ex.args)
            if "has no attribute 'pk'" in str(ex.args):
                model_auto_inst = ModelAuto.objects.filter(name__icontains=model,
                                                           mark_auto=mark_auto['mark_auto']
                                                           ).first()
                if not model_auto_inst:
                    model_auto_inst = ModelAuto.objects.create(name=avto_param['model'].lower(),
                                                               mark_auto=mark_auto_inst
                                                               )
                model_auto = {'model_auto': model_auto_inst.pk}
        else:
            try:
                model_auto = {'model_auto': model_auto}
            except:
                logger.error('model_auto')
                return False
        self.data_crm['podborauto_set'][i].update(model_auto)
        return model_auto

    def set_auto_param_comment_only(self, time_vd_ed):
        comment = self.data_crm['comment'] if self.data_crm['comment'] else ''
        self.data_crm.update({'comment': '{} {}'.format(time_vd_ed, comment)})

    def set_auto_param(self, i, customFields, orderData):
        count = i
        if count == 1:
            count = ''
        i -= 1
        if self.order_type != PPK:
            try:
                time_vd_ed = customFields['time_vd_ed']
            except (TypeError, KeyError) as ex:
                logger.error(ex)
                return 'break'
            self.set_auto_param_comment_only(time_vd_ed)
        else:
            try:
                avto_param = customFields['avto_param{}'.format(count)]
            except (TypeError, KeyError) as ex:
                logger.error(ex)
                logger.error('avto_param')
                return False
            if avto_param == None:
                logger.error('avto_param')
                return False
            if not self.set_auto_param_all(i, avto_param, orderData):
                logger.error('avto_param')
                return False
        return True

    def get_order_status(self, status):
        try:
            logger.error(status)
            return CRM_STATUS[status]
        except KeyError:
            pass
        return 0

    def get_transmission_type(self, trans):
        trans_type = []
        transmision_type = {
            "любая": 0,
            "АКПП": 1,
            "механика": 2,
            "робот": 3,
            "вариатор": 4
        }

        for tr in trans:
            try:
                trans_type.append(transmision_type[tr])
            except KeyError:
                pass
        return trans_type

    def get_engine_type(self, eng):
        eng_type = []
        engine_type = {
            "любой": 0,
            "бензин": 1,
            "дизель": 2,
            "гибрид": 3,
            "электро": 4
        }

        for en in eng:
            try:
                eng_type.append(engine_type[en])
            except KeyError:
                pass
        return eng_type

    def get_body_car(self, body):
        return [{'id': BODY_LK[b]} for b in body]

    def get_color_auto(self, colors):
        color = []
        auto_color = {
            "любой": 0, "черный": 1, "серебристый": 2, "белый": 3,
            "серый": 4, "синий": 5, "красный": 6, "зеленый": 7,
            "коричневый": 8, "бежевый": 9, "голубой": 10, "золотой": 11,
            "пурпурный": 12, "фиолетовый": 13, "желтый": 14, "оранжевый": 15,
            "розовый": 16, "кроме белого": 17, "кроме красного": 18, "кроме синего": 19,
            "кроме желтого": 20, "кроме черного": 21, "кроме зеленого": 22, "бордовый": 23
        }
        try:
            for cl in colors:
                try:
                    color.append(auto_color[cl.lower()])
                except KeyError:
                    pass
        except TypeError as ex:
            pass
        return color

    def get_color_salon(self, colors):
        color = []
        salon_color = {
            "любой": 0, "светлый": 1, "темный": 2
        }
        try:
            for cl in colors:
                try:
                    color.append(salon_color[cl.lower()])
                except KeyError:
                    pass
        except TypeError as ex:
            pass
        return color

    def get_salon(self, salon):
        salons = []
        salon_auto = {
            "любой": 0, "ткань": 1, "велюр": 2,
            "кожа": 3, "комбинированный": 4
        }
        try:
            for sl in salon:
                try:
                    salons.append(salon_auto[sl.lower()])
                except KeyError:
                    pass
        except TypeError as ex:
            pass
        return salons

    def get_drive_type(self, types):
        drive_type = []

        drive_types = {
            "любой": 0,
            "передний": 1,
            "задний": 2,
            "полный": 3
        }
        for tp in types:
            try:
                drive_type.append(drive_types[tp])
            except KeyError:
                pass
        return drive_type

    def get_order_type(self, order_type):
        otype = ""
        try:
            return ORDER_TYPE[order_type]
        except KeyError:
            pass
        return otype


class IndexIntegrate(APIView, Integration):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        return HttpResponse("Hello ЦРМ")

    def create(self, request, *args, **kwargs):
        integration_context = self.get_integration_context()
        logger.error("integration_context")
        logger.error(integration_context)
        logger.error("integration_context")
        if not integration_context or integration_context == order_const.REFUSAL:
            logger.error(integration_context)
            logger.error(
                "if not integration_context or integration_context == order_const.REFUSAL:")
            return Response({'status_code': 200})
        if integration_context == 500:
            logger.error(500)
            return Response({'status_code': 500})
        serializer = OrderCreateUpdateSerializer(data=self.data_crm)
        serializer.is_valid()
        try:
            res = serializer.save()
        except AssertionError as ex:
            logger.error(ex)
            if 'status' in serializer.errors:
                for status in serializer.errors['status']:
                    if order_const.REFUSAL == int(status):
                        logger.error("order_const.REFUSAL == int(status)")
                        return Response({'status_code': 200})
            return Response(serializer.errors)
        except (Exception, KeyError) as ex:
            logger.error(500)
            logger.error(ex)
            return Response({'status_code': 500, 'status_code': str(ex)})
        if serializer.errors:
            logger.error('ser error')
            logger.error(serializer.errors)
            return Response(serializer.errors)
        try:
            return Response({'status_code': 200, 'edit_url': '{}/order/edit/{}/'.format(settings.URL, str(res))})
        except NameError as ex:
            logger.error(ex)
            return Response(serializer.errors)
