import order.order_constants as order_const
import auto.glossary as auto_const

PPK = order_const.PPK
VZD = order_const.VZD
END = order_const.END
EXN = order_const.EXN
OTH = order_const.OTH

ORDER_TYPE = {
    "1-1": VZD,"1-2": VZD,"1-3": VZD,"1-4": VZD,"1-5": VZD,
    "1-6": VZD,"1-7": VZD,"1-8": VZD,"1-9": VZD,"1-10": VZD,
    "1-11": VZD,"1-12": VZD,"1-13": VZD,"1-14": VZD,"1-15": VZD,
    "1-16": VZD,"1-17": VZD,"1-18": VZD,"1-19": VZD,"1-20": VZD,
    "1-21": VZD,"1-22": VZD,"1-23": VZD,"1-24": VZD,"1-25": VZD,
    "1-26": VZD,"1-27": VZD,"1-28": VZD,"1-29": VZD,"1-30": VZD,
    "1-31": VZD,"1-32": VZD,"1-33": VZD,"1-34": VZD,"1-35": VZD,
    "1-36": VZD,"1-37": VZD,"1-38": VZD,"1-39": VZD,"1-40": VZD,
    "1-41": VZD,"1-42": VZD,"1-43": VZD,"1-44": VZD,"1-45": VZD,
    "1-46": VZD,"1-47": VZD,"1-48": VZD,"1-49": VZD,"1-50": VZD,
    "1-51": VZD,"1-52": VZD,"1-53": VZD,"1-54": VZD,"1-55": VZD,
    "1-56": VZD,"1-57": VZD,"1-58": VZD,"1-59": VZD,"1-60": VZD,
    "1-61": VZD,"1-62": VZD,"1-63": VZD,"1-64": VZD,"1-65": VZD,
    "1-66": VZD,"1-67": VZD,"1-68": VZD,"1-69": VZD,"1-70": VZD,
    "1-71": VZD,"1-72": VZD,"1-73": VZD,"1-74": VZD,"1-75": VZD,
    "1-76": VZD,"1-77": VZD,"1-78": VZD,"1-79": VZD,"1-80": VZD,
    "1-81": VZD,"1-82": VZD,"1-83": VZD,"1-84": VZD,"1-85": VZD,
    "1-86": VZD,"1-87": VZD,"1-88": VZD,"1-89": VZD,"1-90": VZD,
    "1-91": VZD,"1-92": VZD,"1-93": VZD,"1-94": VZD,"1-95": VZD,
    "1-96": VZD,"1-97": VZD,"1-98": VZD,"1-99": VZD,"1-100": VZD,
    "1-101": VZD,"1-102": VZD,"1-103": VZD,

    "4-1": OTH,"4-2": OTH,"4-3": OTH,"4-4": OTH,"4-5": OTH,
    "4-6": OTH,"4-7": OTH,"4-8": OTH,"4-9": OTH,"4-10": OTH,
    "4-11": OTH,"4-12": OTH,"4-13": OTH,"4-14": OTH,"4-15": OTH,
    "4-16": OTH,"4-15": OTH,"4-18": OTH,

    "3-1": PPK,"3-2": PPK,"3-3": PPK,"3-4": PPK,"3-5": PPK,
    "3-6": PPK,"3-7": PPK,"3-8": PPK,"3-9": PPK,"3-10": PPK,
    "3-11": PPK,"3-12": PPK,"3-13": PPK,"3-14": PPK,"3-15": PPK,

    "2-1": END,"2-2": END,"2-3": END,"2-4": END,"2-5": END,
    "2-6": END,"2-7": END,"2-8": END,"2-9": END,"2-10": END,
    "2-11": END,"2-12": END,"2-13": END,"2-14": END,"2-15": END,
    "2-16": END,"2-17": END,"2-18": END,"2-19": END,"2-20": END,
    "3-16": END,"3-17": END,"3-18": END,"3-19": END,"3-20": END,
    "3-21": END,"3-22": END,

    
    "3-23": EXN,"3-24": EXN,"3-25": EXN,
    "3-26": EXN,"3-27": EXN,"3-28": EXN,"3-29": EXN,"3-30": EXN,
    "3-31": EXN,"3-32": EXN,"3-33": EXN,"3-34": EXN,"3-35": EXN,
    "3-36": EXN,"3-37": EXN
}

CRM_STATUS = {
    "new": order_const.NEW,
    "send-to-delivery": order_const.IN_QUOE,
    "delivering": order_const.IN_WORK,
    "cancel-other": order_const.REFUSAL,
    "set-aside": order_const.SET_ASIDE,
    "complete": order_const.DONE,
    "refunded": order_const.RETDONE,
    "return-money": order_const.RETURN,
    "ready-transfer":order_const.READY_TRANSFER,
    "fail-call": order_const.FAIL_CALL,
    "konsultaziy": 44
}

BODY_LK = {
    "1": auto_const.WAGON5, #Универсал 5 дв. 
    "2": auto_const.SEDAN, #Седан
    "3": auto_const.HATCHBACK5, # Хэтчбек 5 дв.
    "4": auto_const.HATCHBACK3, # Хэтчбек 3 дв. 
    "5": auto_const.SEDANHARDTOP, # Седан-хардтоп
    "6": auto_const.SUV5, # Внедорожник 5 дв.
    "7": auto_const.COUPE, # Купе
    "8": auto_const.CABRIOLET, # Кабриолет
    "9": auto_const.SUV3, # Внедорожник 3 дв.
    "10": auto_const.SEDAN, # Компактвэн
    "11": auto_const.PICKUPDOUBL, # Пикап Двойная кабина
    "12": auto_const.MINIVAN, # Минивэн
    "13": auto_const.SUVOPEN, # Внедорожник открытый 
    "14": auto_const.LIFTBACK, # Лифтбек
    "15": auto_const.PICKUPMONO, # Пикап Одинарная кабина
    "16": auto_const.PICKUPMONOANDHALF, # Пикап Полуторная кабина
    "17": auto_const.COUPEHARDTOP, # Купе-хардтоп
    "18": auto_const.LIMOUSINE, # Лимузин
    "19": auto_const.RODSTER, # Родстер
    "20": auto_const.WAGON3, # Универсал 3 дв.
    "21": auto_const.WAGON3, # Фастбек 
    "22": auto_const.SEDANTWODOOR, # Седан 2 дв. 
    "23": auto_const.MICROVAN, # Микровэн. 
    "24": auto_const.WAGON5,  # Универсал
    "25": auto_const.SPIDSTER, # Спидстер
    "26": auto_const.PICKUPDOUBL,  # Пикап
    "27": auto_const.HATCHBACK4, # Хэтчбек 4 дв. 
    "28": auto_const.VAN, # Фургон
    "29": auto_const.TARGA, # Тарга
    "30": auto_const.FAETON, # Фаэтон
    "31": auto_const.SUV5, # внедорожник
}

DATE_FROM_ORDER = "2018-12-25 00:00:00"

integration_model = {
    "cee'd": "ceed"
}

REGIONS = {
    'msk':'Москва',
    'tul':'Тула',
    'spb':'Санкт-Петербург',
    'nsk':'Новосибирск',
    'kzn':'Казань',
    'rnd':'Ростов-на-Дону',
    'smr':'Самара',
    'ekb':'Екатеринбург',
    'vla':'Владивосток',
    'vrn':'Воронеж',
    'kra':'Краснодар',
    'kry':'Крым',
    'nno':'Нижний Новгород',
    'ufa':'Уфа'
}

REGIONS_TO_CRM = {
    'Москва':'msk',
    'Тула':'tul',
    'Санкт-Петербург':'spb',
    'Новосибирск':'nsk',
    'Казань':'kzn',
    'Ростов-на-Дону':'rnd',
    'Самара':'smr',
    'Екатеринбург':'ekb',
    'Владивосток':'vla',
    'Воронеж':'vrn',
    'Краснодар':'kra',
    'Крым':'kry',
    'Нижний Новгород':'nno',
    'Уфа':'ufa',
}
