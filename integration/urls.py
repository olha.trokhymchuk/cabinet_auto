from django.contrib import admin
from django.conf.urls import url
from .views import IndexIntegrate

app_name="integration"
urlpatterns = [
    url(r'crm_json/$', IndexIntegrate.as_view(), name='index')
 ]