import re
from datetime import datetime
from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.db.models import Q
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib.postgres.fields import JSONField
from field_history.tracker import FieldHistoryTracker
from imagekit.models import ProcessedImageField, ImageSpecField
from model_utils import Choices
from safedelete.models import SafeDeleteModel
from django.db.models import Avg
from core.utils import OriginalSpec, ThumbnailSpec

from notifications.utils import send_notification
from notifications.models import Notification

ORDER_COLUMN_CHOICES = Choices(
    ('0', 'id'),
    ('1', 'last_name'),
    ('2', 'first_name'),
    ('3', 'email'),
    ('4', 'phone'),
    ('5', 'date_joined'),
)


class Filial(SafeDeleteModel):
    name = models.CharField('Название', max_length=50,
                            default='Московский филиал')
    address = models.CharField('Адрес', max_length=200, blank=True, default='')
    email = models.EmailField('Email', blank=True, default='')
    email_for_complaints = models.EmailField('Почта для жалоб', blank=True, default='')
    phone = models.CharField('Телефон', max_length=50, blank=True, default='')
    code = models.CharField(
        'Код филиала', max_length=50, blank=True, default='')

    class Meta:
        verbose_name = "филиал"
        verbose_name_plural = "филиалы"
        ordering = ["name"]

    @property
    def leaders(self):
        return self.users.filter(groups__name='Руководитель')

    def __str__(self):
        # return "Объект номер {}".format(self.id)
        return self.name


class Osago(models.Model):
    text = models.TextField('Текст осаго', max_length=1500)

    class Meta:
        verbose_name = "Текст осаго"
        verbose_name_plural = "Текст осаго"
        ordering = ["text"]

    def __str__(self):
        return self.text

    def save(self, *args, **kwargs):
        if Osago.objects.count() == 1:
            Osago.objects.all()[:1][0].delete()
        super(Osago, self).save(*args, **kwargs)


class AutoHelp(models.Model):
    text = models.TextField('Текст "Помощь автоюриста"', max_length=1500)

    class Meta:
        verbose_name = 'Текст "Помощь автоюриста"'
        verbose_name_plural = 'Текст "Помощь автоюриста"'
        ordering = ["text"]

    def __str__(self):
        return self.text

    def save(self, *args, **kwargs):
        if AutoHelp.objects.count() == 1:
            AutoHelp.objects.all()[:1][0].delete()
        super(AutoHelp, self).save(*args, **kwargs)


class ServiceHelp(models.Model):
    text = models.TextField('Текст "Запись в сервис"', max_length=1500)

    class Meta:
        verbose_name = 'Текст "Запись в сервис"'
        verbose_name_plural = 'Текст "Запись в сервис"'
        ordering = ["text"]

    def __str__(self):
        return self.text

    def save(self, *args, **kwargs):
        if ServiceHelp.objects.count() == 1:
            ServiceHelp.objects.all()[:1][0].delete()
        super(ServiceHelp, self).save(*args, **kwargs)


class CarCheck(models.Model):
    title = models.TextField('Оглавление', max_length=500)
    text = models.TextField(
        'Текст "Проверка юридической чистоты"', max_length=1500)

    class Meta:
        verbose_name = 'Текст "Проверка юридической чистоты"'
        verbose_name_plural = 'Текст "Проверка юридической чистоты"'
        ordering = ["text"]

    def __str__(self):
        return self.text

    def save(self, *args, **kwargs):
        if CarCheck.objects.count() == 1:
            CarCheck.objects.all()[:1][0].delete()
        super(CarCheck, self).save(*args, **kwargs)


class User(AbstractUser):
    balance = models.PositiveIntegerField(
        'Баланс', blank=True, null=True, default=0)
    filials = models.ManyToManyField(
        Filial, related_name='users', blank=True, default=None, verbose_name='Филиалы')
    patronymic = models.CharField(
        'Отчество', blank=True, null=True, default='', max_length=50)
    sms_code = models.CharField(
        'Смс код', blank=True, null=True, default='NULL', max_length=8)
    phone = models.CharField('Телефон', max_length=50,
                             blank=True, null=True, default=None, unique=True)
    first_name = models.CharField(
        'first_name', max_length=150, null=True, blank=True, default='')
    last_name = models.CharField(
        'last name', max_length=150, null=True, blank=True, default='')
    avatar = ProcessedImageField(upload_to='user/avatar', blank=True,
                                 null=True, verbose_name='Аватар', spec=OriginalSpec, autoconvert=None)
    avatar_thumbnail = ImageSpecField(source='avatar', spec=ThumbnailSpec)
    modified = models.DateTimeField('Последнее изменение', auto_now=True)
    expert_level = models.PositiveSmallIntegerField(
        'Класс эксперта', blank=True, null=True)
    chat_telegram_id = models.CharField(
        'chat_telegram_id', max_length=50, null=True, blank=True, default='')
    message_telegram_id = models.CharField(
        'message_telegram_id', max_length=50, null=True, blank=True, default='')
    money = models.PositiveIntegerField(
        'Количество балов эксперта', blank=True, null=True, default=0)

#   Отправлять пользователю на почту ссобщения из раздела отправки письма руководителю
    is_send_email = models.BooleanField(default=False, blank=True)
    sms_logs = models.TextField('Смс логи', null=True, blank=True, default='')

    def get_absolute_url(self):
        return reverse_lazy('user:edit_user', kwargs={'user_id': self.id})

    @property
    def is_send_email_filial(self):
        return 1 if self.is_send_email else 0

    @property
    def full_name(self):
        patronymic = ""
        if self.patronymic:
            patronymic = self.patronymic
        last_name = ""
        if self.last_name:
            last_name = self.last_name
        return "{} {} {}".format(last_name, self.first_name, patronymic)

    @property
    def search_text(self):
        patronymic = ""
        if self.patronymic:
            patronymic = self.patronymic
        last_name = ""
        if self.last_name:
            last_name = self.last_name
        phone = ""
        if self.phone:
            phone = self.phone
        return "{} {} {} {}".format(last_name, self.first_name, patronymic, phone)

    @property
    def groups_names(self):
        return self.groups.values_list('name', flat=True)

    def can_edit_user(self, user):
        list_of_conditions = [self.is_admin, self.is_leader and user.is_expert,
                              self.is_operator and user.is_client,
                              self.is_client and self == user]
        return self.has_perm('core.change_user') and True in list_of_conditions

    @property
    def is_admin(self):
        return self.is_superuser or 'Администратор' in self.groups_names

    @property
    def is_leader(self):
        return 'Руководитель' in self.groups_names

    @property
    def is_expert(self):
        return 'Эксперт' in self.groups_names

    @property
    def is_operator(self):
        return 'Оператор' in self.groups_names

    @property
    def is_client(self):
        return 'Клиент' in self.groups_names

    @property
    def is_acounter(self):
        return 'Бухгалтер' in self.groups_names

    @property
    def is_hr(self):
        return 'Контент-менеджер' in self.groups_names

    @property
    def groups_user(self):
        return self.groups.all().values_list('name', flat=True)

    @property
    def report_exclude(self):
        for group in self.groups_user:
            if group in ['Клиент', 'Эксперт']:
                return True
        return False

    @property
    def order_exclude(self):
        for group in self.groups_user:
            if group in ['Клиент', 'Эксперт']:
                return True
        return False

    @property
    def view_auto(self):
        for group in self.groups_user:
            if group in ['Просмотр авто']:
                return True
        return False

    @property
    def expert_rating(self):
        expert_rating = Reviews.objects.filter(order_review__in=self.user_ex.all()).all().aggregate(Avg('rating'))
        return round(expert_rating['rating__avg'], 1)

    def __str__(self):
        return self.get_full_name()

    def save(self, *args, **kwargs):
        user_not_exists = not User.objects.filter(
            username=self.phone).count() > 0

        if self.phone and user_not_exists:
            phone = ''.join(re.findall(r'\d+', self.phone))
            if str(phone[0]) == str(8):
                self.username = self.phone = "7" + phone[1:]
            else:
                self.username = self.phone = phone

        if self.pk:
            old_user = User.objects.get(pk=self.pk)
            if old_user.avatar is not None and old_user.avatar != self.avatar:
                old_user.avatar.delete(False)

        super(User, self).save(*args, **kwargs)


def query_users_by_args(**kwargs):
    draw = int(kwargs.get('draw', None)[0])
    length = int(kwargs.get('length', None)[0])
    start = int(kwargs.get('start', None)[0])
    search_value = kwargs.get('search[value]', None)[0]
    order_column = kwargs.get('order[0][column]', None)[0]
    order = kwargs.get('order[0][dir]', None)[0]

    order_column = ORDER_COLUMN_CHOICES[order_column]
    # django orm '-' -> desc
    if order == 'desc':
        order_column = '-' + order_column

    if search_value:
        queryset = User.objects.filter(Q(id__icontains=search_value) |
                                       Q(last_name__icontains=search_value) |
                                       Q(first_name__icontains=search_value) |
                                       Q(email__icontains=search_value) |
                                       Q(phone__icontains=search_value) |
                                       Q(date_joined__icontains=search_value))
    else:
        queryset = User.objects
    count = queryset.count()
    queryset = queryset.order_by(order_column)[start:start + length]
    return {
        'items': queryset,
        'count': count,
        'draw': draw
    }


def query_clients_by_args(request, **kwargs):
    draw = int(kwargs.get('draw', None)[0])
    length = int(kwargs.get('length', None)[0])
    start = int(kwargs.get('start', None)[0])
    search_value = kwargs.get('search[value]', None)[0]
    order_column = kwargs.get('order[0][column]', None)[0]
    order = kwargs.get('order[0][dir]', None)[0]

    order_column = ORDER_COLUMN_CHOICES[order_column]
    # django orm '-' -> desc
    if order == 'desc':
        order_column = '-' + order_column

    if search_value:
        queryset = User.objects.filter(Q(id__icontains=search_value) |
                                       Q(last_name__icontains=search_value) |
                                       Q(first_name__icontains=search_value) |
                                       Q(email__icontains=search_value) |
                                       Q(phone__icontains=search_value) |
                                       Q(date_joined__icontains=search_value))
    else:
        queryset = User.objects.filter(groups__name='Клиент')
    queryset = queryset.filter(
        filials__in=request.user.filials.all()).distinct()
    count = queryset.count()
    queryset = queryset.filter(groups__name='Клиент').order_by(
        order_column)[start:start + length]
    return {
        'items': queryset,
        'count': count,
        'draw': draw
    }


def __str__(self):
    # return "Объект номер {}".format(self.id)
    # return self.first_name
    return '%s %s' % (self.first_name, self.last_name)


class AutoPodborGroup(Group):
    display_name = models.CharField(
        'Название группы', blank=True, null=True, default='', max_length=50)


class Reviews(models.Model):
    title = models.CharField('Заголовок', blank=True,
                             null=True, default='', max_length=30)
    text = models.CharField('Текст', blank=True,
                            null=True, default='', max_length=255)
    rating = models.PositiveIntegerField(
        'Рейтинг', blank=True, null=True, default=5)

    class Meta:
        verbose_name = "Отзывы"
        verbose_name_plural = "Отзывы"


class Equipment(models.Model):
    thickness_gauge = models.CharField('Толщиномер', max_length=250, null=True, blank=True, default='')
    launch = models.CharField('Лаунч', max_length=250, null=True, blank=True, default='')
    launch_password = models.CharField('Пароль от лаунч', max_length=250, null=True, blank=True, default='')
    comp_diagnostic = models.CharField('Компьютерная диагностика', max_length=250, null=True, blank=True, default='')
    gas_analyze = models.BooleanField('Газоанализатор', default=False, blank=True)
    endoscope = models.BooleanField('Эндоскоп', default=False, blank=True)
    straightedge = models.BooleanField('Линейка', default=False, blank=True)
    flashlight = models.BooleanField('Фонарик', default=False, blank=True)
    flashlight_ultra_violet = models.BooleanField('Фонарик ультра-фиолетовый', default=False, blank=True)
    mirror = models.BooleanField('Зеркало', default=False, blank=True)
    currency_detector = models.BooleanField('Детектор валют', default=False, blank=True)
    glu_protector_tire = models.BooleanField('Глубиномер протектора шин', default=False, blank=True)
    compression_tester = models.BooleanField('Компрессометр', default=False, blank=True)
    expert = models.ForeignKey(User, verbose_name='Експерт', null=True, blank=True, default=None, related_name="user_equipment")

    class Meta:
        verbose_name = 'Список оборудования'
        verbose_name_plural = 'Список оборудования'


class TelegramMessages(models.Model):
    telegram_messages_data = JSONField(default=dict, blank=True)
    message_text = JSONField(default=dict, blank=True)

    class Meta:
        verbose_name = "Телеграм сообщения"
        verbose_name_plural = "Телеграм сообщения"
