# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-04 20:36
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20170927_1456'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='user_rang',
        ),
    ]
