# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-02-07 19:57
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20171023_1101'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='patronymic',
            field=models.CharField(default=0, max_length=50, verbose_name='Отчество'),
            preserve_default=False,
        ),
    ]
