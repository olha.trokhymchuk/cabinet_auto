import logging

from django.conf import settings
from rest_framework import serializers
from django.db.models import Avg
from auto.models import Auto, MarkAuto, ModelAuto, Year
from order.models import Order
from report.models import ReportPodbor, Photo
from .models import User, Filial, Equipment, Reviews
import order.order_constants as order_const
import report.report_constants as report_constant
from message.serializers import MessageSerializer


logger = logging.getLogger(__name__)


class EquipmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Equipment
        fields = ('thickness_gauge', 'launch', 'launch_password', 'comp_diagnostic', 'gas_analyze',
                  'endoscope', 'straightedge', 'flashlight', 'flashlight_ultra_violet',
                  'mirror', 'currency_detector', 'glu_protector_tire', 'compression_tester',)


class UserSerializer(serializers.ModelSerializer):

    date_joined = serializers.DateTimeField(format=settings.DATETIME_FORMAT, required=False, allow_null=True)
    full_name = serializers.SerializerMethodField()
    filials = serializers.SerializerMethodField()
    groups = serializers.SerializerMethodField()
    avatar_thumbnail = serializers.SerializerMethodField()
    filial_in = serializers.SerializerMethodField()
    order = serializers.SerializerMethodField()
    report_non_read_count = serializers.SerializerMethodField()
    user_sid = MessageSerializer(many=True, required=False)
    user_fid = MessageSerializer(many=True, required=False)
    phone = serializers.CharField(required=False, allow_null=True)
    is_send_email_filial = serializers.CharField(required=False, allow_null=True)
    user_equipment = EquipmentSerializer(many=True, read_only=True)
    expert_rating = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 
                    'phone', 'date_joined', 'full_name', 
                    'avatar', 'avatar_thumbnail', 'filials', 
                    'groups', 'user_sid', 'user_fid', 'order', 'report_non_read_count',
                    'filial_in','is_send_email_filial', 'user_equipment', 'expert_rating'
                )

    def is_send_email_filial(self, obj):
        return obj.is_send_email_filial

    def get_full_name(self, obj):
        if obj.last_name is None:
            return '{}'.format(obj.first_name, obj.last_name)  
        return '{} {}'.format(obj.first_name, obj.last_name)  

    def get_filials(self, obj):
        filials = obj.filials.all()
        return [f.name for f in filials]

    def get_filial_in(self, obj):
        try:
            filials = obj.filials.all()
            filials = [f.pk for f in filials]
            return filials
        except BrokenPipeError as ex:
            logger.error(ex)
            return[]

    def get_groups(self, obj):
        groups = obj.groups.all()
        return [g.name for g in groups]

    def get_avatar_thumbnail(self, obj):
        try:
            return obj.avatar_thumbnail.url if obj.avatar else ''
        except FileNotFoundError:
            return ''

    def get_order(self, obj):
        return obj.user_ex.filter(status=order_const.IN_WORK, order_type='PPK', status_closing=False).count()

    def get_report_non_read_count(self, obj):
        return obj.reportpodbor_set.filter(is_read=False).count()

    def get_expert_rating(self, obj):
        rating = obj.user_ex.filter(review__isnull=False).aggregate(Avg('review__rating'))
        if rating['review__rating__avg']:
            return round(rating['review__rating__avg'], 1)
        return None

class FilialSerializerPure(serializers.ModelSerializer):

    class Meta:
        model = Filial
        fields = '__all__'
        
class FilialSerializer(FilialSerializerPure):
    leaders = UserSerializer(many=True)


class MarkAutoSerializer(serializers.ModelSerializer):

    class Meta:
        model = MarkAuto
        fields = ('id', 'name')


class ModelAutoSerializer(serializers.ModelSerializer):

    class Meta:
        model = ModelAuto
        fields = ('id', 'name')


class YearAutoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Year
        fields = ('id', 'year')


class AutoSerializerList(serializers.ModelSerializer):
    mark_auto = MarkAutoSerializer(required=False, allow_null=True)
    model_auto = ModelAutoSerializer(required=False, allow_null=True)
    year_auto = YearAutoSerializer(required=False, allow_null=True)

    class Meta:
        model = Auto
        fields = ['mark_auto', 'model_auto', 'year_auto']


class ReportListSerialializer(serializers.ModelSerializer):
    status = serializers.CharField(source='get_status_display')
    report_type = serializers.CharField(source='get_report_type_display')
    auto = AutoSerializerList()
    photos_google = serializers.SerializerMethodField()
    photos = serializers.SerializerMethodField()
    all_photos = serializers.SerializerMethodField()

    class Meta:
        model = ReportPodbor
        depth = 1
        fields = ('id', 'auto', 'status', 'report_type', 'photos_google', 'photos', 'cost_after', 'all_photos',)

    def get_photos_google(self, obj):
        photos = Photo.objects.filter(report_podbor=obj, photo_type=report_constant.type_photo['photo_front_views']).all()[:3]
        try:
            return [{'photos_google': photo.image_full_google} for photo in photos]
        except FileNotFoundError as ex:
            return ''

    def get_photos(self, obj):
        photos = Photo.objects.filter(report_podbor=obj, photo_type=report_constant.type_photo['photo_front_views']).all()[:3]
        try:
            raw = [{'id': photo.id, 'image': photo.image.url,
                    'image_small': photo.image_small.url} for photo in photos if photo.image]
            return raw
        except FileNotFoundError as ex:
            return ''

    def get_all_photos(self, obj):
        photos = Photo.objects.filter(report_podbor=obj).all()[:15]
        try:
            raw = [{'id': photo.id, 'image': photo.image.url,
                    'image_small': photo.image_small.url} for photo in photos if photo.image]
            return raw
        except FileNotFoundError as ex:
            return ''


class OrderSerializerList(serializers.ModelSerializer):
    client = UserSerializer()
    expert = UserSerializer(many=True)
    reportpodbor_set = ReportListSerialializer(many=True)
    order_type = serializers.SerializerMethodField()

    class Meta:
        model = Order
        depth = 1
        fields = ['id', 'number_buh', 'client', 'expert', 'reportpodbor_set', 'order_type', 'date_start', ]


    def get_order_type(self, obj):
        return obj.get_order_type_display()

class ReviewsSerializer(serializers.ModelSerializer):
    order_review = OrderSerializerList(many=True)

    class Meta:
        model = Reviews
        fields = ['title', 'text', 'rating', 'order_review', ]
