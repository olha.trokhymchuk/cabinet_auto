import re
import logging

from django.contrib.auth import get_user_model, views
from django.contrib.auth.backends import ModelBackend
from django.http import HttpResponseRedirect

logger = logging.getLogger(__name__)


class PhoneBackend(ModelBackend):
    def authenticate(self, username=None, password=None, request=None, **kwargs):
        UserModel = get_user_model()
        try:
            phone = ''.join(re.findall(r'\d+', username))
            user = UserModel.objects.get(phone=phone)
        except UserModel.DoesNotExist:
            return None
        else:
            if user.check_password(password):
                return user
        return None


def csrf_failure(request, reason=""):
    return HttpResponseRedirect(request.path)