import json
import logging
import os
import re
from functools import update_wrapper
from django.contrib.sites.shortcuts import get_current_site
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_exempt, csrf_protect
from urllib.parse import unquote
from rest_framework.response import Response
from PIL import Image as PilImage, ExifTags
from django.core.files.base import ContentFile

try:
    from StringIO import StringIO, BytesIO
except ImportError:
    from io import StringIO, BytesIO

from autopodbor.utils import send_simple_message_mailgun
from django.conf import settings
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import Group
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponseRedirect, HttpResponse, Http404, JsonResponse, HttpResponseForbidden
from django.urls import reverse_lazy, reverse
from django.contrib import auth
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib import messages
from django.views.generic import ListView
from django.views.generic.base import TemplateView
from django.views.generic.edit import CreateView, FormView, UpdateView
from field_history.models import FieldHistory

from django.contrib.auth.views import LoginView
from safedelete import SOFT_DELETE_CASCADE

from core.filters import UserFilter, ReviewsFilter
from core.utils import MultipleFormsMixin, ImprovedListView
from custom_admin.utils import FilteredFieldsMixin, FilteredFieldContextMixin
from notifications.models import Notification
from notifications.utils import send_notification
from order.models import Order
from order.serializers import OrderSerializer, OrderFieldHistorySerializer
from report.models import ReportPodbor, Photo
from report.serializers import ReportListSerialializer
from service.models import PodborAuto
from .models import Filial, User, query_clients_by_args, Osago, AutoHelp, ServiceHelp, CarCheck, Reviews, Equipment
from .serializers import UserSerializer, FilialSerializer, ReviewsSerializer, FilialSerializerPure
from .forms import AddUserForm, ResetPasswordForm

from core.forms import (
    EditUserForm,
    EditUserPasswordForm,
    EditFilialForm,
    CreateUserForm)

from core.workers.expert_to_crm import expertToCrm

logger = logging.getLogger(__name__)


# Страничка нового заказа клиента на почту


class NewOrderEmailView(LoginRequiredMixin, TemplateView):
    template_name = 'newOrderEmail/newOrderEmail.html'
    context_object_name = 'NewOrder'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['phone'] = request.user.phone
        context['name'] = request.user.first_name
        return self.render_to_response(context)


# Страничка осаго


class OsagoView(LoginRequiredMixin, TemplateView):
    template_name = 'osago/osago.html'
    context_object_name = 'osago'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        text_osago = Osago.objects.all()[:1]
        if len(text_osago):
            text_osago = text_osago[0]
        else:
            text_osago = 'no text'
        context['osago'] = text_osago
        return self.render_to_response(context)


# Страничка помощь автоюриста


class AutoHelpView(LoginRequiredMixin, TemplateView):
    template_name = 'autohelp/autohelp.html'
    context_object_name = 'AutoHelp'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        text_AutoHelp = AutoHelp.objects.all()[:1]
        if len(text_AutoHelp):
            text_AutoHelp = text_AutoHelp[0]
        else:
            text_AutoHelp = 'no text'
        context['AutoHelp'] = text_AutoHelp
        context['phone'] = request.user.phone
        return self.render_to_response(context)


# Страничка написать руководителю


class WriteToTheHead(LoginRequiredMixin, TemplateView):
    template_name = 'writeToTheHead/writeToTheHead.html'
    context_object_name = 'writeToTheHead'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        context['phone'] = request.user.phone
        context['orders'] = Order.objects.filter(client=request.user)
        return self.render_to_response(context)


# Страничка "Запись в сервис"


class ServiceHelpView(LoginRequiredMixin, TemplateView):
    template_name = 'ServiceHelp/ServiceHelp.html'
    context_object_name = 'ServiceHelp'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        text_ServiceHelp = ServiceHelp.objects.all()[:1]
        if len(text_ServiceHelp):
            text_ServiceHelp = text_ServiceHelp[0]
        else:
            text_ServiceHelp = 'no text'
        context['ServiceHelp'] = text_ServiceHelp
        context['phone'] = request.user.phone
        return self.render_to_response(context)


# Страничка "Проверка юридической чистоты"


class CarCheckView(LoginRequiredMixin, TemplateView):
    template_name = 'CarCheck/CarCheck.html'
    context_object_name = 'CarCheck'

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        text_CarCheck = CarCheck.objects.all()[:1]
        if len(text_CarCheck):
            text_CarCheck = text_CarCheck[0]
        else:
            text_CarCheck = 'no text'
        context['CarCheck'] = text_CarCheck
        return self.render_to_response(context)


# Список филиалов


class FilialList(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView):
    template_name = 'filial/filial-list.html'
    permission_required = 'core.view_filial'
    raise_exception = True
    model = Filial
    paginate_by = settings.PAGINATE_BY
    serializer = FilialSerializer
    search_fields = ['name', 'email', 'phone', 'address']

    def get_context_data(self, **kwargs):
        context = super(FilialList, self).get_context_data()
        context.update({'html_title': 'Филиалы'})
        return context

    def get_queryset(self):
        qs = super(FilialList, self).get_queryset()
        if self.request.user.is_leader:
            qs = qs.filter(pk__in=self.request.user.filials.all())
        return qs


# Информация о филиале (и заказы филиала)
class FilialDetailView(LoginRequiredMixin, TemplateView):
    template_name = 'filial/filial-detail.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_client:
            raise Http404
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


def get_user_filter(request):
    users = User.objects.all()
    filials = Filial.objects.filter(users__in=users).distinct()
    groups = Group.objects.filter(user__in=users).distinct()
    expert_levels = []

    return JsonResponse(data={
        'filials': [{'id': f.id, 'name': f.name} for f in filials],
        'groups': [{'name': g.name} for g in groups],
        'expert_levels': expert_levels
    })


# Создание отзыва


@login_required(login_url='user:login')
def review_create(request):
    text = request.POST.get('text', '')
    rating = int(request.POST.get('rating', '5'))
    order_id = request.POST.get('order_id', '')
    site = get_current_site(None).domain
    if text != '' and order_id != '':
        Review = Reviews.objects.create(title='Форма лк2.авто-подбор.рф', text=text, rating=rating)
        Review.save()
        order = Order.objects.get(id=order_id)
        order.review = Review
        order.save()
        if rating <= 3 and order.filial.email_for_complaints != '':
            send_simple_message_mailgun(
                'Новый отзыв',
                'Текст: ' + text + '; \n'
                'Рейтинг: ' + str(rating) + '\n'
                'Открыть заказ Вы может по ссылке: ' + str(site) + '/order/detail/' + str(order_id) + '\n',
                settings.DEFAULT_FROM_EMAIL,
                order.filial.email_for_complaints
            )
        else:
            pass
    return JsonResponse(data={
        'error': False,
        'message': 'Ok'
    })


# Список пользователейbase_info.html


class UserList(LoginRequiredMixin, ImprovedListView):
    template_name = 'user/user-list.html'
    permission_required = 'core.view_user'
    raise_exception = True
    model = User
    paginate_by = settings.PAGINATE_BY
    serializer = UserSerializer
    filterset_class = UserFilter
    search_fields = ['first_name', 'last_name', 'email', 'phone']

    def get_context_data(self, **kwargs):
        context = super(UserList, self).get_context_data()
        context.update({'html_title': 'Пользователи'})

        context.update({'user_count': User.objects.distinct('id').filter(
            filials__in=self.user_filials).filter(is_active=True).count()})
        return context

    def get_queryset(self):
        self.user_filials = self.request.user.filials.all()
        name = self.request.GET.get("name")
        queryset = super(UserList, self).get_queryset()
        if name or name is not None:
            queryset = queryset.filter(Q(first_name__icontains=unquote(name)) |
                                       Q(last_name__icontains=unquote(name)) |
                                       Q(phone__icontains=unquote(name)))
        queryset = queryset.filter(is_active=True)
        queryset = queryset.distinct('id').filter(
            filials__in=self.user_filials).order_by('-id')
        return queryset


@login_required(login_url='user:login')
@permission_required('core.delete_user', raise_exception=True)
def delete_user(request, user_id):
    if request.method == 'POST':
        user = get_object_or_404(User, id=user_id)
        user.is_active = False
        user.save()
        return HttpResponse(status=200)
    raise Http404


@login_required(login_url='user:login')
@permission_required('core.delete_filial', raise_exception=True)
def delete_filial(request, filial_id):
    if request.method == 'POST':
        filial = get_object_or_404(Filial, id=filial_id)
        filial.delete(force_policy=SOFT_DELETE_CASCADE)
        return HttpResponse(status=200)
    raise Http404


@login_required(login_url='user:login')
def update_expert_crm(request):
    if request.method == 'GET':
        expertToCrm()
        return HttpResponseRedirect('/admin')
        return HttpResponse(status=200)
    raise Http404


# Доступ запрещен
def error403(request):
    return render(request, 'errors/error403.html', {
        'html_title': 'Доступ запрещен!',
        'page_title_span': 'Доступ',
        'page_title': 'запрещен...'
    })


# Регистрация
class SignupNewUser(FormView):
    form_class = AddUserForm
    template_name = 'auth/signup.html'
    success_url = '/'

    def form_valid(self, form):
        form.save()
        messages.success(self.request, 'Пользователь был создан')
        return HttpResponseRedirect(self.get_success_url())


class AuthLogin(LoginView):
    template_name = 'auth/login.html'
    redirect_authenticated_user = True
    success_url = '/'


class ResetPassword(FormView):
    form_class = ResetPasswordForm
    template_name = 'account/password_reset.html'
    success_url = reverse_lazy('user:reset_password')

    def get_context_data(self, **kwargs):
        context = super(ResetPassword, self).get_context_data(**kwargs)
        context.update({"signup_url": reverse("account_signup")})
        return context

    def form_valid(self, form):
        phone = form.cleaned_data.get('phone')
        phone = ''.join(re.findall(r'\d+', phone))
        phones = [str(8) + phone[1:], str(7) + phone[1:]]
        logger.error(phones)
        user = get_object_or_404(User, username__in=phones)
        password = User.objects.make_random_password(
            allowed_chars='1234567890')
        user.set_password(password)
        user.save()
        data = {'name': user.get_full_name(), 'username': user.username,
                'password': password}
        messages.add_message(self.request, messages.INFO, 'success_restore')
        send_notification(recipients=[
            user], action=Notification.RESET_PASSWORD, data=data, channels=['sms', 'email'])
        return super(ResetPassword, self).form_valid(form)


# Выход из системы
def auth_logout(request):
    auth.logout(request)
    return redirect(settings.LOGIN_URL)


# Информация о пользователе (и заказы пользователя)
class UserDetailView(LoginRequiredMixin, TemplateView):
    template_name = 'user/user-detail.html'

    def get(self, request, *args, **kwargs):
        if request.user.is_client and int(kwargs['user_id']) != int(request.user.id):
            raise Http404
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


# Создание нового пользователя
class UserCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView, FilteredFieldsMixin):
    permission_required = 'core.add_user'
    raise_exception = True
    model = User
    form_class = CreateUserForm
    template_name = 'user/edit-user.html'
    action = 'create'

    def get_form(self, *args, **kwargs):
        form = super(UserCreateView, self).get_form(*args, **kwargs)
        if 'groups' in form.fields and not self.request.user.is_admin:
            if self.request.user.is_leader:
                form.fields['groups'].queryset = Group.objects.filter(Q(name='Эксперт') |
                                                                      Q(name='Оператор') |
                                                                      Q(name='Клиент'))
            elif self.request.user.is_acounter:
                form.fields['groups'].queryset = Group.objects.filter(Q(name='Эксперт') |
                                                                      Q(name='Клиент'))
            else:
                form.fields['groups'].queryset = Group.objects.filter(
                    name='Клиент')

        return form

    def get_success_url(self):
        return reverse('user:edit_user', args=(self.object.id,))

    def form_valid(self, form):
        self.object = form.save()
        form.save_m2m()
        # group = Group.objects.get(name='Клиент')
        user = User.objects.get(phone=self.object.username)
        # user.groups.add(group)
        user.is_staff = True
        user.save()
        password1 = form.cleaned_data['password1']
        data = {
            'name': self.object.get_full_name(),
            'username': self.object.username,
            'password': form.cleaned_data['password1']
        }

        messages.add_message(self.request, messages.INFO, 'success_add_to_lk')
        send_notification(
            recipients=[user], action=Notification.REGISTRATION, data=data, channels=['sms'])

        return JsonResponse(data={'redirect': self.get_success_url()})

    def form_invalid(self, form):
        return JsonResponse(status=200, data={'errors': form.errors})

    def get_context_data(self, **kwargs):
        context = super(UserCreateView, self).get_context_data(**kwargs)
        context.update({
            'html_title': 'Создание пользователя',
            'filials': Filial.objects.all(),
            'create': True,
            'expert_levels': range(1, 4)
        })

        return context


# Редактировать пользователя
class UserUpdateSelfView(LoginRequiredMixin, UpdateView, FilteredFieldsMixin, FilteredFieldContextMixin):
    template_name = 'user/edit-user.html'
    model = User
    form_class = EditUserForm
    action = 'update'

    def get_success_url(self):
        return reverse('user:edit_self')

    def get_object(self):
        return self.request.user

    def form_valid(self, form):
        if self.object.is_client:
            form.initial['filials'] = self.object.filials.all()
            form.initial.update({'user': self.object})
        super(UserUpdateSelfView, self).form_valid(form)
        redirect_to = JsonResponse(data={'redirect': self.get_success_url()})

        return redirect_to

    def form_invalid(self, form):
        return JsonResponse(status=200, data={'errors': form.errors})

    def get_context_data(self, **kwargs):
        context = super(UserUpdateSelfView, self).get_context_data(**kwargs)
        context.update({
            'password_change_form': PasswordChangeForm(user=self.get_object()),
            'html_title': 'Редактирование пользователя',
            'filials': Filial.objects.all(),
            'groups': Group.objects.all(),
            'expert_levels': range(1, 4),
        })
        return context

    def post(self, request, *args, **kwargs):
        if request.POST.get('old_password'):
            password_change_form = PasswordChangeForm(
                user=self.get_object(), data=request.POST)
            if password_change_form.is_valid():
                password_change_form.save()
                update_session_auth_hash(request, password_change_form.user)
            else:
                return JsonResponse(data={'errors': password_change_form.errors})
        if self.get_object().is_expert and request.POST.get('data'):
            data_request = json.loads(request.POST.get('data'))
            data = {
                'thickness_gauge': data_request['thickness_gauge'],
                'launch': data_request['launch'],
                'launch_password': data_request['launch_password'],
                'comp_diagnostic': data_request['comp_diagnostic'],
                'gas_analyze': data_request['gas_analyze'],
                'endoscope': data_request['endoscope'],
                'straightedge': data_request['straightedge'],
                'flashlight': data_request['flashlight'],
                'flashlight_ultra_violet': data_request['flashlight_ultra_violet'],
                'mirror': data_request['mirror'],
                'currency_detector': data_request['currency_detector'],
                'glu_protector_tire': data_request['glu_protector_tire'],
                'compression_tester': data_request['compression_tester'],
                'expert': self.get_object().id
            }

            obj, create = Equipment.objects.get_or_create(expert_id=self.get_object().id)
            if obj:

                Equipment.objects.filter(expert_id=self.get_object().id).update(**data)
            else:
                Equipment.objects.create(**data)
        return super(UserUpdateSelfView, self).post(request, *args, **kwargs)


# Редактировать пользователя
class UserUpdateView(PermissionRequiredMixin, UserUpdateSelfView):
    permission_required = 'core.change_user'
    raise_exception = True

    def get_success_url(self):
        return reverse('user:user_list')

    def get_object(self):
        user = get_object_or_404(User, id=self.kwargs.get('user_id'))
        return user

    def get_context_data(self, **kwargs):
        context = super(UserUpdateView, self).get_context_data(**kwargs)
        context.update({
            'user_id': self.kwargs.get('user_id')
        })

        return context


# Добавить филиал
class FilialCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView, FilteredFieldsMixin):
    permission_required = 'core.add_filial'
    raise_exception = True
    template_name = 'filial/filial-edit.html'
    form_class = EditFilialForm
    action = 'create'

    def get_context_data(self, **kwargs):
        context = super(FilialCreateView, self).get_context_data()
        context.update({'html_title': 'Создание филиала'})
        return context

    def get_success_url(self):
        return reverse('user:filial_edit', args=(self.object.id,))


# Редактировать филиал
class FilialUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView, FilteredFieldsMixin):
    permission_required = 'core.change_filial'
    raise_exception = True
    template_name = 'filial/filial-edit.html'
    form_class = EditFilialForm
    action = 'update'

    def get_context_data(self, **kwargs):
        context = super(FilialUpdateView, self).get_context_data()
        context.update({'html_title': 'Редактирование филиала'})
        return context

    def get_success_url(self):
        return reverse('user:filial_edit', args=(self.object.id,))

    def get_object(self, **kwargs):
        filial = get_object_or_404(Filial, id=self.kwargs.get('filial_id'))
        return filial


def change_image(request):
    if request.method == 'POST':
        try:
            item = Photo.objects.get(pk=request.POST['photo_id'])
            image = PilImage.open(item.image)
            rotated_image = image.rotate(-90)
            rotated_image.save(item.image.file.name, overwrite=True)
            # rotated_image.save(item.image_small.file.name, overwrite=True)
            item.image = item.image
            print("image_small")
            print(item.image_small)
            print(item.image.url)
            item.save()
            print(item.image_small)
            print(item.image.url)
            image_url = settings.BASE_DIR + item.image.url
            print(settings.BASE_DIR)
            image.save(image_url)
            image.close()

        except (AttributeError, KeyError, IndexError) as ex:
            print(ex)
    data = {"ok": 1, "image": item.image.url,
            "image_small": item.image_small.url
            }
    return JsonResponse(data=data)


@csrf_exempt
def update_employee_email_sender(request):
    data = {"ok": 1}
    if request.method == 'POST':
        try:
            user = get_object_or_404(User, id=request.POST['id'])
            user.is_send_email = True if request.POST['send_email'] == '1' else False
            user.save()
        except (AttributeError, KeyError, IndexError) as ex:
            print (ex)
    return JsonResponse(data=data)


def admin_view(view, cacheable=False):
    """
    Overwrite the default admin view to return 404 for not logged in users.
    """
    def inner(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise Http404()
        return view(request, *args, **kwargs)

    if not cacheable:
        inner = never_cache(inner)

    # We add csrf_protect here so this function can be used as a utility
    # function for any view, without having to repeat 'csrf_protect'.
    if not getattr(view, 'csrf_exempt', False):
        inner = csrf_protect(inner)

    return update_wrapper(inner, view)


@login_required
@permission_required('core.view_user', raise_exception=True)
def get_review_filter(request):
    if request.is_ajax():
        data = {}
        executors = User.objects.filter(groups__name='Эксперт', is_active=True)
        filials_queryset = Filial.objects.all()
        filials = FilialSerializerPure(filials_queryset, many=True).data

        data.update({
            'filials': filials,
            'executors': UserSerializer(executors, many=True).data})

        return JsonResponse(data)

    raise Http404


class UserReviewsList(LoginRequiredMixin, ImprovedListView):
    template_name = 'reviews/reviews-list.html'
    # TODO clarify the roles of visibility
    # permission_required = 'core.view_user'
    raise_exception = True
    model = Reviews
    paginate_by = settings.PAGINATE_BY
    serializer = ReviewsSerializer
    filterset_class = ReviewsFilter

    def get_context_data(self, **kwargs):
        context = super(UserReviewsList, self).get_context_data()

        context.update({'html_title': 'Отзывы'})
        reviews = Reviews.objects.all()
        filials = Filial.objects.all()
        context.update({"reviews": reviews,
                        'filials': filials})
        return context

    def get_queryset(self):
        queryset = super(UserReviewsList, self).get_queryset()
        user = self.request.user
        if user.is_expert:
            queryset = queryset.filter(order_review__in=user.user_ex.all()).order_by("-id")
        else:
            queryset = queryset.order_by("-id")
        return queryset