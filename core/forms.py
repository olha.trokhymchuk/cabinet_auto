import re
import json
import logging

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.utils.translation import pgettext, ugettext, ugettext_lazy as _

from django.contrib.auth.models import Group

from notifications.utils import send_notification
from notifications.models import Notification

from core.models import User, Filial
from custom_admin.forms import FilteredForm

from core import app_settings
from .app_settings import AuthenticationMethod

from allauth.utils import (
    build_absolute_uri,
    get_username_max_length,
    set_form_field_order
)

from allauth.account.adapter import get_adapter

from .utils import (
    perform_login
)

logger = logging.getLogger(__name__)


class PasswordField(forms.CharField):

    def __init__(self, *args, **kwargs):
        render_value = kwargs.pop('render_value',
                                  app_settings.PASSWORD_INPUT_RENDER_VALUE)
        kwargs['widget'] = forms.PasswordInput(render_value=render_value,
                                               attrs={'placeholder':
                                                      kwargs.get("label")})
        super(PasswordField, self).__init__(*args, **kwargs)


class MyModelForm(FilteredForm):
    error_css_class = 'class-error'
    required_css_class = 'class-required'

    def __init__(self, *args, **kwargs):
        super(MyModelForm, self).__init__(*args, **kwargs)
        # adding css classes to widgets without define the fields:
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

    def as_div(self):
        return self._html_output(
            normal_row=u'<div%(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
            error_row=u'<div class="error">%s</div>',
            row_ender='</div>',
            help_text_html=u'<div class="hefp-text">%s</div>',
            errors_on_separate_row=False)


class MyUserCreationForm(UserCreationForm):
    error_css_class = 'class-error'
    required_css_class = 'class-required'

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)
        # adding css classes to widgets without define the fields:
        for field in self.fields:
            self.fields[field].widget.attrs['class'] = 'form-control'

    def as_div(self):
        return self._html_output(
            normal_row=u'<div%(html_class_attr)s>%(label)s %(field)s %(help_text)s %(errors)s</div>',
            error_row=u'<div class="error">%s</div>',
            row_ender='</div>',
            help_text_html=u'<div class="hefp-text">%s</div>',
            errors_on_separate_row=False)


class EditUserForm(MyModelForm):
    def save(self, commit=True):
        # TODO finish soon
        # m = super(EditUserForm, self).save(commit=False)
        # # do custom stuff
        # if 'user' in self.initial and self.initial['user'].is_client:
        #     data = self.data.copy()
        #     data['filials'] = self.initial['filials']
        #     self.data = data
        # if commit:
        #     m.save()
        pass

    class Meta:
        model = User
        fields = ('is_send_email','first_name', 'last_name', 'patronymic', 'email', 'groups', 'filials', 'phone', 'avatar', 'expert_level')


class EditUserPasswordForm(MyUserCreationForm):
    username = forms.CharField(label='Логин')

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2')


class ResetPasswordForm(forms.Form):
    phone = forms.CharField(label='телефон', max_length=20, required=True)

    class Meta:
        model = User
        fields = ('phone',)

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        phone = ''.join(re.findall(r'\d+', phone))
        not_exist = User.objects.filter(username=phone).count() == 0

        if not_exist:
            raise forms.ValidationError("Пользователя с таким номером телефона не существует")
        return phone


class EditFilialForm(MyModelForm):
    class Meta:
        model = Filial
        fields = ('id', 'name', 'address', 'email', 'email_for_complaints', 'phone')


class AddUserForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('last_name', 'first_name', 'patronymic', 'email', 'phone', 'password1', 'password2')

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        return password1

    def clean_email(self):
        email = self.cleaned_data['email']
        exist = User.objects.filter(email=email).count() > 0
        if exist:
            raise forms.ValidationError("Пользователь с таким почтовым адресом существует")
        return email

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        phone = ''.join(re.findall(r'\d+', phone))
        exist = User.objects.filter(username=phone).count() > 0

        if exist:
            raise forms.ValidationError("Пользователь с таким телефоном существует")

        return phone

    def save(self, commit=True):
        m = super(AddUserForm, self).save(commit=False)
        # do custom stuff
        if commit:
            m.save()
        data = {
            'name': m.get_full_name(),
            'username': m.phone,
            'password': self.cleaned_data['password2']
        }
        channels = ['email', 'lk', 'sms']
        group = Group.objects.get(name='Клиент')
        object_user = User.objects.get(username=m.phone)
        object_user.groups.add(group)
        send_notification([object_user], Notification.REGISTRATION, data, channels=channels)
        
        return m


class AddUserShortForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('phone', 'password1', 'password2')

    def clean_password1(self):
        password1 = self.cleaned_data.get('password1')
        return password1

    def clean_phone(self):
        phone = self.cleaned_data['phone']
        phone = ''.join(re.findall(r'\d+', phone))
        exist = User.objects.filter(username=phone).count() > 0

        if exist:
            raise forms.ValidationError("Пользователь с таким телефоном существует")

        return phone

    def save(self, commit=True):
        m = super(AddUserShortForm, self).save(commit=False)
        if commit:
            m.save()
        data = {
            'username': m.phone,
            'password': self.cleaned_data['password2']
        }
        channels = ['email', 'lk', 'sms']
        group = Group.objects.get(name='Клиент')
        object_user = User.objects.get(username=m.phone)
        object_user.groups.add(group)
        send_notification([object_user], Notification.REGISTRATION, data, channels=channels)
        return m


class CreateUserForm(AddUserForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'patronymic', 'expert_level', 'email', 'groups', 'filials', 'phone', 'avatar', 'password1', 'password2')


class LoginForm(forms.Form):

    password = PasswordField(label=_("Password"))
    remember = forms.BooleanField(label=_("Remember Me"),
                                  required=False)

    user = None
    error_messages = {
        'account_inactive':
        _("This account is currently inactive."),

        'email_password_mismatch':
        _("The e-mail address and/or password you specified are not correct."),

        'username_password_mismatch':
        _("The username and/or password you specified are not correct."),
    }

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request', None)
        super(LoginForm, self).__init__(*args, **kwargs)
        data = kwargs.get('data', None)
        self.api = None

        if data and data.get('api'):
            self.api_login = data.get('phone')
            self.api = data.get('api')
            self.password = data.get('password')
            return

        if app_settings.AUTHENTICATION_METHOD == AuthenticationMethod.EMAIL:
            login_widget = forms.TextInput(attrs={'type': 'email',
                                                  'placeholder':
                                                  _('E-mail address'),
                                                  'autofocus': 'autofocus'})
            login_field = forms.EmailField(label=_("E-mail"),
                                           widget=login_widget)
        elif app_settings.AUTHENTICATION_METHOD \
                == AuthenticationMethod.USERNAME:
            login_widget = forms.TextInput(attrs={'placeholder':
                                                  _('Username'),
                                                  'autofocus': 'autofocus'})
            login_field = forms.CharField(
                label=_("Username"),
                widget=login_widget,
                max_length=get_username_max_length())
        else:
            assert app_settings.AUTHENTICATION_METHOD \
                == AuthenticationMethod.USERNAME_EMAIL
            login_widget = forms.TextInput(attrs={'placeholder':
                                                  _('Username or e-mail'),
                                                  'autofocus': 'autofocus'})
            login_field = forms.CharField(label=pgettext("field label",
                                                         "Login"),
                                          widget=login_widget)
        self.fields["login"] = login_field
        set_form_field_order(self, ["login", "password", "remember"])
        if app_settings.SESSION_REMEMBER is not None:
            del self.fields['remember']

    def user_credentials(self):
        """
        Provides the credentials required to authenticate the user for
        login.
        """

        credentials = {}
        if(self.api):
            credentials["username"] = self.api_login
            credentials["password"] = self.password
            return credentials 
        else:
            login = self.cleaned_data["login"]

        if app_settings.AUTHENTICATION_METHOD == AuthenticationMethod.EMAIL:
            credentials["email"] = login
        elif (
                app_settings.AUTHENTICATION_METHOD ==
                AuthenticationMethod.USERNAME):
            credentials["username"] = login
        else:
            if self._is_login_email(login):
                credentials["email"] = login
            credentials["username"] = login
        credentials["password"] = self.cleaned_data["password"]
        return credentials

    def clean_login(self):
        login = self.cleaned_data['login']
        return login.strip()

    def _is_login_email(self, login):
        try:
            validators.validate_email(login)
            ret = True
        except exceptions.ValidationError:
            ret = False
        return ret

    def clean(self):
        super(LoginForm, self).clean()
        if self._errors:
            return
        credentials = self.user_credentials()
        user = get_adapter(self.request).authenticate(
            self.request,
            **credentials)
        if user:
            self.user = user
        else:
            auth_method = app_settings.AUTHENTICATION_METHOD
            if auth_method == app_settings.AuthenticationMethod.USERNAME_EMAIL:
                login = self.cleaned_data['login']
                if self._is_login_email(login):
                    auth_method = app_settings.AuthenticationMethod.EMAIL
                else:
                    auth_method = app_settings.AuthenticationMethod.USERNAME
            raise forms.ValidationError(
                self.error_messages['%s_password_mismatch' % auth_method])
        return self.cleaned_data

    def login(self, request, redirect_url=None):
        ret = perform_login(request, self.user,
                            email_verification=app_settings.EMAIL_VERIFICATION,
                            redirect_url=redirect_url)
        remember = app_settings.SESSION_REMEMBER
        if remember is None:
            remember = self.cleaned_data['remember']
        if remember:
            request.session.set_expiry(app_settings.SESSION_COOKIE_AGE)
        else:
            request.session.set_expiry(0)
        return ret