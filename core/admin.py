import logging

from django.contrib.auth.admin import UserAdmin
from django.contrib import admin, messages
from .models import User, Filial, Osago, AutoHelp, ServiceHelp, CarCheck, Reviews, Equipment
from django.contrib.auth.forms import UserChangeForm
from django.utils.translation import ugettext_lazy as _

from notifications.utils import send_notification
from notifications.models import Notification
from django.shortcuts import get_object_or_404


logger = logging.getLogger(__name__)


class MyUserAdmin(UserAdmin):
    form = UserChangeForm
    list_display = ('username', 'last_name', 'first_name',
                    'is_staff', 'is_active', 'sms_logs')
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': (
                'first_name', 'last_name', 'patronymic', 'email', 'phone', 'avatar'
            )}),
        (_('Filials'), {'fields': ('filials',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        (_('Groups'), {'fields': ('groups',)}),
    )
    exclude = ('notifications',)

    def save_model(self, request, obj, form, change):
        if(set(['is_active']).issubset(form.changed_data) and request.POST.get('is_active') == 'on'):
            logger.error(request.POST)
            user = get_object_or_404(User, username=request.POST.get('phone'))
            password = User.objects.make_random_password(allowed_chars='bdfghjkmnpqrstuvwyz123456789')
            user.set_password(password)
            user.save()
            data = {'name': user.get_full_name(), 'username': user.username, 'password': password}
            messages.add_message(request, messages.INFO, 'success_add_to_lk')
            send_notification(recipients=[user], action=Notification.ADD_TO_LK, data=data, channels=['sms'])
        super().save_model(request, obj, form, change)


@admin.register(Reviews)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('title', 'text', 'rating', )


@admin.register(Equipment)
class ClientAdmin(admin.ModelAdmin):
    list_display = ('thickness_gauge', 'launch', 'comp_diagnostic', 'gas_analyze', 'endoscope', 'straightedge',
    'flashlight', 'flashlight_ultra_violet', 'mirror', 'currency_detector', 'expert',)

# Register your models here.
admin.site.register(Filial)
admin.site.register(Osago)
admin.site.register(AutoHelp)
admin.site.register(ServiceHelp)
admin.site.register(CarCheck)
admin.site.register(User, MyUserAdmin)
