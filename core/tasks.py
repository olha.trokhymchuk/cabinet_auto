from django.conf import settings

from autopodbor.celery import app
from core.workers.load_users_on_mobile import load_users_on_mobile
from core.workers.backup_database import backup_database
from core.workers.expert_to_crm import expertToCrm
from core.workers.monitoring import serverMonitoring

@app.task
def load_user_on_mobile():
    load_users_on_mobile()

@app.task
def backup_databas():
    backup_database()

@app.task
def export_expert():
	if settings.EXPERTTOCRM:
		expertToCrm()

@app.task
def server_monitoring():
	serverMonitoring()

