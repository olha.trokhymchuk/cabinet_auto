from django.core.management.base import BaseCommand

from core.workers.backup_database import backup_database

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        backup_database()