from django.core.management.base import BaseCommand

from core.workers.load_users_on_mobile import load_users_on_mobile

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        load_users_on_mobile()