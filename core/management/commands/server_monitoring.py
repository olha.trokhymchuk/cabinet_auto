from django.core.management.base import BaseCommand

from core.workers.monitoring import serverMonitoring

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        serverMonitoring()