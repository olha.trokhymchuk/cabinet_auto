from django.core.management.base import BaseCommand

from core.workers.add_client_to_user import addClientToUser

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        addClientToUser()