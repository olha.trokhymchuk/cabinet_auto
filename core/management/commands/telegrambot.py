import telebot
from django.contrib.auth import authenticate
from django.core.management.base import BaseCommand, CommandError
from core.models import User, TelegramMessages
from order.models import Order
from report.models import ReportPodbor
import order.order_constants as order_const
from django.conf import settings
from notifications.sms import send_sms

from core.utils import add_status

import logging

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'Start telegram Bot listener'

    def add_arguments(self, parser):
        pass

    bot = telebot.TeleBot(settings.TOKEN)
    phone = ''
    password = ''

    def handle(self, *args, **options):

        @self.bot.message_handler(content_types=['text'])
        def get_text_messages(message):
            print(message)
            CurrentUser = User.objects.filter(chat_telegram_id=message.from_user.id)

            if CurrentUser.count() == 0:
                if message.text == '/start':
                    self.bot.send_message(message.from_user.id, "Напишите свой номер телефона")
                    self.bot.register_next_step_handler(message, get_phone)
            else:
                self.bot.send_message(message.from_user.id, "Вы уже авторизированы в системе, уведомления активны")

        def get_phone(message):  # получаем телефон
            self.phone = message.text
            self.bot.send_message(message.from_user.id, 'Напишите пароль в систему')
            self.bot.register_next_step_handler(message, get_password)

        def get_password(message):  # получаем пароль
            self.password = message.text

            user = authenticate(username=self.phone, password=self.password)

            if user is not None:
                # if user.is_active and user.is_leader:
                if user.is_active:
                    self.bot.send_message(message.from_user.id,
                                          'Вы успешно авторизировались в системе, теперь будете получать уведомления об заказх')
                    user.chat_telegram_id = message.from_user.id
                    user.save()
                else:
                    self.bot.send_message(message.from_user.id, 'Пользователь не активирован в системе')
            else:
                self.bot.send_message(message.from_user.id, 'Пользователь не найден в системе')

        # Get chat id and message id that telebot send to leaders
        def get_chat_and_message_id(message_telegram_id, chat_telegram_id):
            data = {str(message_telegram_id): str(chat_telegram_id)}
            try:
                return TelegramMessages.objects.get(telegram_messages_data__contains=data)
            except (TelegramMessages.DoesNotExist, TypeError):
                return False

        @self.bot.callback_query_handler(func=lambda call: True)
        def callback_inline(call):
            chat_id_and_message_id_list = get_chat_and_message_id(message_telegram_id=call.message.message_id,
                                                                  chat_telegram_id=call.message.chat.id)
            print('chat_id_and_message_id_list : ', chat_id_and_message_id_list)
            try:
                data = call.data.split('_')
                order = Order.objects.get(id=data[1])
                if data[0] == 'yes':
                    order.status = order_const.READY_RECIEVE
                    order.save_from_bot = True
                    # if len( data ) > 3:
                    #     transferring_obj = User.objects.get(id = data[3])
                    #     order.transferring.clear()
                    #     order.transferring.add(transferring_obj)
                    order.save()
                    if len(data) > 2:
                        all_reports = order.reportpodbor_set.exclude(id=data[2])
                        for report in all_reports:
                            if report.status == 3:
                                report.status = 2
                                report.save()
                        report = ReportPodbor.objects.get(id=data[2])
                        report.status = 3
                        report.save()
                        print("report " + str(report.id) + ' change status')
                    else:
                        print("no report added")
                    self.bot.answer_callback_query(call.id, 'Заказ №' + str(order.number_buh) + ' переведен в статус "Готов к выдаче".', True)
                    add_status(order, 'change-status-crm', 'order')
                    logger.error("add statuses from bot")
                else:
                    if data[3]:
                        pass
                        # sms_to = User.objects.get(id=data[3])
                        # sms_body = str(sms_to) + ' в выдаче по заказу № ' + str(order.number_buh) + ' Вам отказано.'
                        # send_sms(sms_to.phone, sms_body)
                    order.transferring.clear()
                    self.bot.answer_callback_query(call.id, 'Статус заказа №' + str(order.number_buh) + ' переведен в статус "В работе".', True)
                if chat_id_and_message_id_list:
                    for message_id, chat_id in chat_id_and_message_id_list.telegram_messages_data.items():
                        if data[0] == 'yes':
                            self.bot.edit_message_text(
                                text=str(chat_id_and_message_id_list.message_text['text']) + 'Статус заказа №' + str(order.number_buh) + ' переведен в статус "Готов к выдаче". Руководитель: ' + call.from_user.first_name,
                                message_id=int(message_id),
                                chat_id=int(chat_id),
                                reply_markup=None
                            )
                        else:
                            self.bot.edit_message_text(
                                text=str(chat_id_and_message_id_list.message_text['text']) + 'Статус заказа №' + str(order.number_buh) + ' переведен в статус "В работе". Руководитель: ' + call.from_user.first_name,
                                message_id=int(message_id),
                                chat_id=int(chat_id),
                                reply_markup=None
                            )
                else:
                    pass
                chat_id_and_message_id_list.delete()
            except Exception as e:
                print('Error' + str(e.__class__))
                logger.error("critical error" + str(e.__class__))

        self.bot.polling(none_stop=True, interval=3)
