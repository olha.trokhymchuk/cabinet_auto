import django_filters
from django_filters.fields import Lookup

from .models import User, Reviews


class ListFilter(django_filters.Filter):
    def filter(self, qs, value):
        if not value:
            return qs
        value_list = value.split(u',')
        return super(ListFilter, self).filter(qs, Lookup(value_list, 'in'))


class UserFilter(django_filters.FilterSet):
    class Meta:
        model = User
        fields = ('filials__id', 'groups__name')


class ReviewsFilter(django_filters.FilterSet):
    filial = django_filters.NumberFilter(method='filial_filter')
    executor = django_filters.NumberFilter(method='executor_filter')
    rating__gte = django_filters.NumberFilter(
        name='rating', lookup_expr='gte')
    rating__lte = django_filters.NumberFilter(
        name='rating', lookup_expr='lte')

    class Meta:
        model = Reviews
        fields = '__all__'

    def filial_filter(self, queryset, name, value):
        queryset = queryset.filter(order_review__filial__id=value, order_review__isnull=False)
        return queryset

    def executor_filter(self, queryset, name, value):
        return queryset.filter(order_review__expert__id=value)