import logging

from autopodbor.utils import send_simple_message_mailgun
from django.conf import settings
from urllib.parse import unquote
from django.contrib.sites.shortcuts import get_current_site
from order.models import Order
from report.models import ReportPodbor
from core.models import User, Reviews
from order.serializers import OrderSerializer
from report.serializers import ReportSerializer
from core.serializers import UserSerializer, ReviewsSerializer
import order.order_constants as order_const
from rest_framework.generics import RetrieveAPIView, ListCreateAPIView
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from django.shortcuts import HttpResponseRedirect, get_object_or_404

from django.views.decorators.csrf import csrf_exempt
from core.models import User, Filial
from core.api_serializers import UserSerializer, FilialSerializer
from core.serializers import UserSerializer as UserSer

from field_history.models import FieldHistory
from order.serializers import OrderFieldHistorySerializer

from .pagination import LimitOffsetPagination, PageNumberPagination

from django.contrib.auth.decorators import login_required

import telebot
from telebot import types
from django.db.models import Q

logger = logging.getLogger(__name__)


class ApiUserDetail(RetrieveAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    def get_serializer_context(self):
        context = super(ApiUserDetail, self).get_serializer_context()
        context.update({
            "user_page": self.kwargs.get("pk")
        })
        return context


class ApiUserDetailActivity(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = FieldHistory.objects.all()
    serializer_class = OrderFieldHistorySerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        return self.queryset.filter(user=self.kwargs.get('pk'))

    def list(self, request, pk):
        queryset = self.get_queryset()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ApiFilialDetail(RetrieveAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = Filial.objects.all()
    serializer_class = FilialSerializer
    pagination_class = PageNumberPagination


class ApiFilialUserList(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = User.objects.all()
    serializer_class = UserSer
    pagination_class = PageNumberPagination
    ordering = '-id'

    def list(self, request, pk):
        queryset = self.get_queryset()
        queryset = queryset.filter(filials=pk, is_active=True).exclude(groups__name='Клиент')
        if self.request.user.is_leader:
            queryset = queryset.filter(groups__name='Эксперт').order_by('-id')
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ApiFilialDetailActivity(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = FieldHistory.objects.all()
    serializer_class = OrderFieldHistorySerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        return self.queryset.filter(user__filials=self.kwargs.get('pk'))

    def list(self, request, pk):
        queryset = self.get_queryset()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


def send_new_order_to_email(request):
    name = ''
    tel = ''
    if request.method == 'GET':
        name = request.GET['name']
        tel = request.GET['tel']
    elif request.method == 'POST':
        name = request.POST['name']
        tel = request.POST['tel']
    send_simple_message_mailgun(
        'Заказ с личного кабинета. Форма: Новый заказ',
        'Заказ с личного кабинета. <br> Имя: ' + name + '; Телефон: ' + tel + ';',
        'info@ap4u.ru',
        'orders@ap-sto.ru'
    )
    return JsonResponse(status=200, data={'error': 0})


def send_message_to_email(request):
    if request.method == 'GET':
        if request.GET['form'] == 'service_help':
            name = request.GET['name']
            tel = request.GET['tel']
            send_simple_message_mailgun(
                'Заказ с личного кабинета. Форма: Запишитесь на обслуживание и ремонт Вашего автомобиля',
                'Заказ с личного кабинета. <br> Имя: ' + name + '; Телефон: ' + tel + ';',
                'info@ap4u.ru',
                'orders@ap-sto.ru'
            )

        elif request.GET['form'] == 'autohelp':
            tel = request.GET['tel']
            text = request.GET['text']
            send_simple_message_mailgun(
                'Заказ с личного кабинета. <br> Заказ с личного кабинета. Форма: Помощь автоюриста',
                'Телефон: ' + tel + '; Текст: ' + text + ';',
                'info@ap4u.ru',
                'urist@podbor.org'
            )
        elif request.GET['form'] == 'write_to_the_head':
            tel = request.GET['tel']
            order = request.GET['order']
            text = request.GET['text']
            email = request.GET['email']
            client_email = request.user.email
            filials = request.user.filials.all()
            users_to_send = []
            for filial in filials:
                users_to_send += list(User.objects.filter(Q(filials__in=[filial.id], is_send_email=True)).all())
            for to_send in users_to_send:
                send_simple_message_mailgun(
                    'Сообщение с личного кабинета. <br> Заказ с личного кабинета. Форма: Написать руководителю',
                    'Телефон: ' + tel + '; Номер заказа: ' + order + '; Email: ' + email + '; Текст: ' + text + ';',
                    client_email,
                    to_send.email
                )

    elif request.method == 'POST':
        if request.POST['form'] == 'service_help':
            name = request.POST['name']
            tel = request.POST['tel']
            send_simple_message_mailgun(
                'Заказ с личного кабинета. <br> Заказ с личного кабинета. Форма: Запишитесь на обслуживание и ремонт Вашего автомобиля',
                'Имя: ' + name + '; Телефон: ' + tel + ';',
                'info@ap4u.ru',
                'orders@ap-sto.ru'
            )

        elif request.POST['form'] == 'autohelp':
            tel = request.POST['tel']
            text = request.POST['text']
            send_simple_message_mailgun(
                'Заказ с личного кабинета. <br> Заказ с личного кабинета. Форма: Помощь автоюриста',
                'Телефон: ' + tel + '; Текст: ' + text + ';',
                'info@ap4u.ru',
                'urist@podbor.org'
            )
        elif request.POST['form'] == 'write_to_the_head':
            tel = request.POST['tel']
            order = request.POST['order']
            text = request.POST['text']
            email = request.POST['email']

            get_order = Order.objects.filter(number_buh=order).first()
            send_simple_message_mailgun(
                'Сообщение с личного кабинета. <br> Заказ с личного кабинета. Форма: Написать руководителю',
                'Телефон: ' + tel + '; Номер заказа: ' + order + '; Email: ' + email + '; Текст: ' + text + ';',
                settings.DEFAULT_FROM_EMAIL,
                get_order.filial.email_for_complaints
            )
        elif request.POST['form'] == 'write_to_the_sto':
            tel = request.POST['tel']
            name = request.POST['name']
            vin = request.POST['vin']
            model = request.POST['model']
            mark = request.POST['mark']
            send_simple_message_mailgun(
                'Сообщение с личного кабинета. <br> Заказ с личного кабинета. Форма: Зпись на обслуживание',
                'Телефон: ' + tel + '; Имя: ' + name + '; VIN: ' + vin + '; Марка: ' + mark + '; Модель: ' + model,
                'info@ap4u.ru',
                'orders@ap-sto.ru'
            )

    return JsonResponse(status=200, data={'error': 0})


@login_required
def global_search(request):
    query = request.POST.getlist('query')
    ordersArray = {}
    reviews = None
    if len(query) > 0:
        query = query[0]
        if query != '':
            orders = Order.objects.filter(number_buh__icontains=query)[:100]
            reports = ReportPodbor.objects.filter(auto__vin__icontains=query)[:100]

            query_list = query.split(' ')
            users = User.objects.none()

            for element in query_list:
                users_by_name = User.objects.filter(Q(first_name__icontains=unquote(element)))

                users |= users_by_name

                users_by_last_name = User.objects.filter(Q(last_name__icontains=unquote(element)))

                users |= users_by_last_name

                users_by_patronymic = User.objects.filter(Q(patronymic__icontains=unquote(element)))

                users |= users_by_patronymic

                users_by_phone = User.objects.filter(phone__icontains=element)

                users |= users_by_phone

            userArray = []
            # print(len(users))
            for idx, user_tmp in enumerate(users):
                in_search_text = True
                for txt_query in query_list:
                    print(user_tmp.search_text.lower())
                    # print('=====')
                    # print(txt_query.lower())
                    if user_tmp.search_text.lower().find(txt_query.lower()) == -1:
                        in_search_text = False
                        break
                if in_search_text:
                    if len(userArray) <= 100:
                        userArray.append(UserSer(user_tmp).data)
                        reviews = Reviews.objects.filter(order_review__expert=user_tmp)[:100]

            ordersArray = []
            reportArray = []
            reviewsArray = []

            for order in orders:
                ordersArray.append(OrderSerializer(order).data)
            for report in reports:
                reportArray.append(ReportSerializer(report).data)
            if reviews:
                reviewsArray = ReviewsSerializer(reviews, many=True).data

            responseArray = {'orders': ordersArray, 'reports': reportArray, 'users': userArray, 'reviews': reviewsArray}
        else:
            responseArray = {}
    return JsonResponse(responseArray, safe=False)


@login_required
def transfertoissue(request):
    bot = telebot.TeleBot(settings.TOKEN)
    order_id = request.POST.get('order_id')
    report_id = request.POST.get('report_id')
    order = Order.objects.get(id=order_id)
    leaders = order.filial.leaders
    for tele_user in leaders:
        if tele_user.chat_telegram_id:
            message = ''
            site = get_current_site(None).domain
            message += "Заказ:\n"
            message += site + '/order/detail/' + order_id + '/' + "\n"
            message += "======================================\n"
            message += "отчет:\n"
            message += site + '/report/view/' + report_id + '/\n'
            message += "======================================"
            bot.send_message(tele_user.chat_telegram_id, message)
    data = {}
    data.update({
        'success': 'ok',
    })

    return JsonResponse(data)


@login_required
def order_to_issue(request):
    data = {}
    if request.method == 'POST':
        print('ok __ POST')
        order_id = request.POST['order']
        report_id = request.POST['report']
        expert_id = request.POST['expert']
        place_issuing = request.POST['place_issuing']
        from_salon = request.POST['salon'] == "true"
        order_obj = Order.all_objects.get(id=order_id)
        order_obj.transferring_id = expert_id
        order_obj.auto_from_salon = from_salon
        order_obj.place_issuing = place_issuing
        order_obj.status = order_const.READY_RECIEVE
        order_obj.save_from_bot = False
        order_obj.report_id = report_id
        order_obj.save()
        data.update({
            'success': 'true'
        })
    else:
        data.update({
            'success': 'false'
        })
    return JsonResponse(data)


@csrf_exempt
def change_user_avatar_by_mobile_app(request):
    if request.method == 'POST':
        try:
            user = User.objects.get(id=int(request.POST['user_id']))
        except (TypeError, KeyError, User.MultipleObjectsReturned, User.DoesNotExist):
            return JsonResponse(data={'errors': 404, 'describe': "Don't find any users"}, status=404)
        if request.FILES['avatar'].name and request.FILES['avatar']:
            user.avatar.save(request.FILES['avatar'].name, request.FILES['avatar'])
            return JsonResponse(user.avatar.url, safe=False, status=200)
        else:
            return JsonResponse(data={'errors': 404, 'describe': "Don't find photo avatar or name photo"}, status=404)
    if request.method == 'DELETE':
        try:
            user = User.objects.get(id=int(request.GET.get('user_id')))
        except (TypeError, KeyError, User.MultipleObjectsReturned, User.DoesNotExist):
            return JsonResponse(data={'errors': 404, 'describe': "Don't find any users"}, status=404)
        if user.avatar is not None:
            user.avatar.delete()
        else:
            return JsonResponse(data={'errors': 404, 'describe': "Don't find any avatar to delete"}, status=404)
        return JsonResponse(data={'status': 200, 'describe': "avatar deleted"}, safe=False, status=200)

    else:
        return JsonResponse(data={'errors': 'method not allowed'}, status=404)


class ApiReviewsByUserList(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = Reviews.objects.all()
    serializer_class = ReviewsSerializer
    pagination_class = PageNumberPagination

    def list(self, request, pk):
        user = User.objects.get(pk=pk)
        queryset = self.get_queryset()

        if user.is_expert:
            queryset = queryset.filter(order_review__in=user.user_ex.all()).order_by("-id")

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)