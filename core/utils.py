import os
import ftplib
import json
from copy import deepcopy

import logging
from functools import reduce

from constance import config

from django.db import models
from django.db.utils import DataError

from django.conf import settings
from django.core import serializers
from django.db.models import Q, F
from django.db.utils import IntegrityError
from django.test.runner import DiscoverRunner
from django.core.exceptions import ImproperlyConfigured
from django.http import HttpResponseRedirect, JsonResponse, Http404
from django.views.generic import ListView, DetailView, CreateView, TemplateView
from django_filters.views import BaseFilterView
from django.views.generic.base import ContextMixin, TemplateResponseMixin
from field_history.models import FieldHistory
from field_history.tracker import FieldHistoryTracker, FieldInstanceTracker, get_serializer_name

# GROUPS_SHORTCUTS = {
#     'AD': 'Администратор',
#     'BU': 'Бухгалтер',
#     'OP': 'Оператор',
#     'RP': 'Руководитель',
#     'EX': 'Эксперт',
#     'CL': 'Клиент',
#     'Контент-менеджер': 'Контент-менеджер'
# }
from imagekit import ImageSpec
from imagekit.utils import get_field_info
from pilkit.processors import ResizeToFit, ResizeToFill

from allauth.account.adapter import get_adapter
from allauth.exceptions import ImmediateHttpResponse
from . import signals
from django.contrib import messages
from django.contrib.contenttypes.models import ContentType

from lk.models import Statuses

logger = logging.getLogger(__name__)


class NoLoggingTestRunner(DiscoverRunner):
    def run_tests(self, test_labels, extra_tests=None, **kwargs):
        # Don't show logging messages while testing
        logging.disable(logging.CRITICAL)
        return super(NoLoggingTestRunner, self).run_tests(test_labels, extra_tests, **kwargs)


class MultipleFormsMixin(TemplateResponseMixin, ContextMixin):
    """
    A mixin that provides a way to show and handle multiple forms in a request.
    It's almost fully-compatible with regular FormsMixin
    """

    initial = {}
    forms_classes = []
    success_url = None
    prefix = None
    active_form_keyword = "selected_form"

    def get_initial(self):
        """
        Returns the initial data to use for forms on this view.
        """
        return self.initial.copy()

    def get_prefix(self):
        """
        Returns the prefix to use for forms on this view
        """
        return self.prefix

    def get_forms_classes(self):
        """
        Returns the forms classes to use in this view
        """
        return self.forms_classes

    def get_active_form_number(self):
        """
        Returns submitted form index in available forms list
        """
        if self.request.method in ('POST', 'PUT'):
            try:
                return int(self.request.POST[self.active_form_keyword])
            except (KeyError, ValueError):
                raise ImproperlyConfigured(
                    "You must include hidden field with field index in every form!")

    def get_forms(self, active_form=None):
        """
        Returns instances of the forms to be used in this view.
        Includes provided `active_form` in forms list.
        """
        all_forms_classes = self.get_forms_classes()
        all_forms = [
            form_class(**self.get_form_kwargs())
            for form_class in all_forms_classes]
        if active_form:
            active_form_number = self.get_active_form_number()
            all_forms[active_form_number] = active_form
        return all_forms

    def get_form(self):
        """
        Returns active form. Works only on `POST` and `PUT`, otherwise returns None.
        """
        active_form_number = self.get_active_form_number()
        if active_form_number is not None:
            all_forms_classes = self.get_forms_classes()
            active_form_class = all_forms_classes[active_form_number]
            return active_form_class(**self.get_form_kwargs(is_active=True))

    def get_form_kwargs(self, is_active=False):
        """
        Returns the keyword arguments for instantiating the form.
        """
        kwargs = {
            'initial': self.get_initial(),
            'prefix': self.get_prefix(),
        }

        if is_active:
            kwargs.update({
                'data': self.request.POST,
                'files': self.request.FILES,
            })
        return kwargs

    def get_success_url(self):
        """
        Returns the supplied URL.
        """
        if self.success_url:
            url = self.success_url.format(**self.object.__dict__)
        else:
            try:
                url = self.object.get_absolute_url()
            except AttributeError:
                raise ImproperlyConfigured(
                    "No URL to redirect to.  Either provide a url or define"
                    " a get_absolute_url method on the Model.")
        return url

    def form_valid(self, form):
        """
        If the form is valid, redirect to the supplied URL.
        """
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        """
        If the form is invalid, re-render the context data with the
        data-filled forms and errors.
        """
        return self.render_to_response(self.get_context_data(active_form=form))


class JsonListView(ListView):
    serializer = None
    json_objects = 'objects'

    def get_dict_paginator(self, context):
        data = {}
        paginator = context.get('paginator')
        if not paginator:
            return data

        page_obj = context.get('page_obj')
        data['num_pages'] = paginator.num_pages
        data['count'] = paginator.count
        data['number'] = page_obj.number
        data['has_previous'] = page_obj.has_previous()
        data['has_next'] = page_obj.has_next()
        data['has_other_pages'] = page_obj.has_other_pages()
        data['start_index'] = page_obj.start_index()
        data['end_index'] = page_obj.end_index()

        if data['has_previous']:
            data['previous_page_number'] = page_obj.previous_page_number()
        if data['has_next']:
            data['next_page_number'] = page_obj.next_page_number()

        return data

    def get_json_data(self, context):
        data = {}
        paginated = context.get('object_list')
        objects = self.serializer(paginated, many=True).data
        data[self.json_objects] = objects
        dict_paginator = self.get_dict_paginator(context)
        count = dict_paginator.get('count')

        data.update({
            'page': dict_paginator,
            'count': count
        })
        return data

    def get_json_response(self):
        context = self.get_context_data()
        data = self.get_json_data(context)
        return JsonResponse(data, status=200, safe=False)

    def get(self, request, *args, **kwargs):
        super(JsonListView, self).get(request, *args, **kwargs)
        return self.get_json_response()


class JsonDetailView(DetailView):
    serializer = None
    json_objects = 'objects'

    def get_dict_paginator(self, context):
        pass

    def get_json_data(self, context):
        data = {}

        return data

    def get_json_response(self):
        context = self.get_context_data()
        data = self.get_json_data(context)
        return JsonResponse(data, status=200, safe=False)


class JsonTemplateView(TemplateView):
    serializer = None
    json_objects = 'objects'

    def get_dict_paginator(self, context):
        pass

    def get_json_data(self, context):
        data = {}

        return data

    def get_json_response(self):
        dictionaries = [{
                        'name': obj.first_name, 
                        'last_name': obj.last_name} 
                        for obj in self.get_object() 
                    ]
                    
        data = self.get_json_data(dictionaries)
        return JsonResponse(data, status=200, safe=False)


class JsonCreateView(CreateView):
    serializer = None
    json_objects = 'objects'


    def get_json_data(self, context):
        data = {}

        return data

    def get_json_response(self):
        context = self.get_context_data()
        data = self.get_json_data(context)
        return JsonResponse(data, status=200, safe=False)

class SearchMixin(ListView):
    search_fields = []

    def get_searched_queryset(self, querystring):
        if querystring:
            queryset = self.get_queryset().filter(reduce(lambda x, y: x | y, [Q(**{'{}__iexact'.format(field): querystring}) for field in self.search_fields_all_match]))
            if not queryset.exists():
                queryset = self.get_queryset().filter(reduce(lambda x, y: x | y, [Q(**{'{}__icontains'.format(field): querystring}) for field in self.search_fields]))
        else:
            queryset = self.get_queryset()
        return queryset


# listview with filter, searching and json responses for ajax
class ImprovedListView(JsonListView, BaseFilterView, SearchMixin):
    def create_object_list(self, queryset):
        self.object_list = queryset
        allow_empty = self.get_allow_empty()
        if not allow_empty:
            if self.get_paginate_by(self.object_list) is not None and hasattr(self.object_list, 'exists'):
                is_empty = not self.object_list.exists()
            else:
                is_empty = self.object_list.count() == 0
            if is_empty:
                raise Http404(_("Empty list and '%(class_name)s.allow_empty' is False.") % {
                    'class_name': self.__class__.__name__,
                })

    def get(self, request, *args, **kwargs):  
        if 'querystring' in request.GET:
            queryset = self.get_searched_queryset(request.GET.get('querystring'))
        elif hasattr(self, 'filterset_class') and self.filterset_class is not None:
            filterset_class = self.get_filterset_class()
            self.filterset = self.get_filterset(filterset_class)
            queryset = self.filterset.qs
        else:
            queryset = self.get_queryset()

        sortkey = request.GET.get('sortkey')
        if sortkey:
            try:
                if '-' in sortkey:
                    sortkey = sortkey.replace('-', '')
                    queryset = queryset.order_by(F(sortkey).desc(nulls_last=True))
                else:
                    queryset = queryset.order_by(F(sortkey).asc(nulls_first=True))
            except AttributeError as ex:
                logging.warning(ex)

        self.create_object_list(queryset)

        if request.is_ajax():
            return self.get_json_response()

        context = self.get_context_data()
        return self.render_to_response(context)

    def post(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        if request.is_ajax():
            return self.get_json_response()
        context = self.get_context_data()
        return self.render_to_response(context)


# listview with filter, searching and json responses for ajax
class ImprovedDetailView(JsonDetailView): #, BaseFilterView, SearchMixin

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.is_ajax():
            return self.get_json_response()
        context = self.get_context_data()
        return self.render_to_response(context)


# listview with filter, searching and json responses for ajax
class ImprovedTemplateView(JsonTemplateView): #, BaseFilterView, SearchMixin

    def post(self, request, *args, **kwargs):
        if request.is_ajax():
            return self.get_json_response()
        context = self.get_context_data()
        return self.render_to_response(context)


# listview with filter, searching and json responses for ajax
class ImprovedCreateView(JsonCreateView): #, BaseFilterView, SearchMixin

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.is_ajax():
            if 'send_message' in request.POST:
                pass
            return self.get_json_response()
        context = self.get_context_data()
        return self.render_to_response(context)


class OriginalSpec(ImageSpec):
    processors = [ResizeToFit(1300, 1300)]
    format = 'JPEG'
    options = {'quality': 100}
    _options = options

    @property
    def options(self):
        options = self._options

        if self.source.size > 250000 and options['quality'] == 100:
            options['quality'] = 85

        return options


class ThumbnailSpec(ImageSpec):
    format = 'JPEG'
    options = {'quality': 80}

    @property
    def processors(self):
        return [ResizeToFill(config.THUMBNAIL_WIDTH, config.THUMBNAIL_HEIGHT)]


class CustomFieldHistoryTracker(FieldHistoryTracker):
    def has_changed(self, field, instance):
        previous = None
        history = FieldHistory.objects.filter(object_id=instance.pk, field_name=field).last()
        try:
            history = json.loads(history.serialized_data)
            previous = history[0]['fields'][field]
        except AttributeError as ex:
            logger.error(ex)
        current = self.tracker.get_field_value(field)
        try:
            current.pk
        except AttributeError as ex:
            logger.error(ex)
        else:
            current = current.pk
        if type(previous) == list:
            try:
                current = current.first().pk
            except AttributeError as ex:
                logger.error(ex)
                current = None
            if not current and previous:
                return True
            return current and current not in previous
        else:
            return current != previous
    
    def patch_save(self, instance):
        original_save = instance.save

        def save(**kwargs):
            self.tracker = getattr(instance, self.attname)
            
            is_new_object = instance.pk is None
            ret = original_save(**kwargs)
            field_histories = []
            user = self.get_field_history_user(instance)
            print("user exists")
            print(user)
            print("user exists")
            if not user:
                print("user no exists")
                print(user)
                try:
                    user = instance.user
                except AttributeError:
                    pass
                print(user)
                print("user no exists")
            if is_new_object:
                data = serializers.serialize(get_serializer_name(),
                                             [instance],
                                             fields=['id'])
                history = FieldHistory(
                    object=instance,
                    field_name='id',
                    serialized_data=data,
                    user=user,
                )
                field_histories.append(history)
            else:

                for field in self.fields:             
                    if self.has_changed(field, instance):
                        try:
                            data = serializers.serialize(get_serializer_name(),
                                                     [instance],
                                                     fields=[field])
                        except AttributeError as ex:
                            logger.error(field)
                            logger.error(ex)
                        else:
                            history = FieldHistory(
                                object=instance,
                                field_name=field,
                                serialized_data=data,
                                user=user,
                            )
                            field_histories.append(history)

            if field_histories:
                try:
                    FieldHistory.objects.bulk_create(field_histories)
                except IntegrityError as ex:
                    logger.error(ex)
            # Update tracker in case this model is saved again
            self._inititalize_tracker(instance)

            return ret
        instance.save = save

def perform_login(request, user, email_verification,
                  redirect_url=None, signal_kwargs=None,
                  signup=False):
    """
    Keyword arguments:

    signup -- Indicates whether or not sending the
    email is essential (during signup), or if it can be skipped (e.g. in
    case email verification is optional and we are only logging in).
    """
    # Local users are stopped due to form validation checking
    # is_active, yet, adapter methods could toy with is_active in a
    # `user_signed_up` signal. Furthermore, social users should be
    # stopped anyway.
    adapter = get_adapter(request)
    if not user.is_active:
        return adapter.respond_user_inactive(request, user)
    try:
        adapter.login(request, user)
        response = HttpResponseRedirect(
            get_login_redirect_url(request, redirect_url))

        if signal_kwargs is None:
            signal_kwargs = {}
        signals.user_logged_in.send(sender=user.__class__,
                                    request=request,
                                    response=response,
                                    user=user,
                                    **signal_kwargs)
        adapter.add_message(
            request,
            messages.SUCCESS,
            'account/messages/logged_in.txt',
            {'user': user})
    except ImmediateHttpResponse as e:
        response = e.response
    return response

def get_next_redirect_url(request, redirect_field_name="next"):
    """
    Returns the next URL to redirect to, if it was explicitly passed
    via the request.
    """
    redirect_to = get_request_param(request, redirect_field_name)
    if not get_adapter(request).is_safe_url(redirect_to):
        redirect_to = None
    return redirect_to


def get_login_redirect_url(request, url=None, redirect_field_name="next"):
    if url and callable(url):
        # In order to be able to pass url getters around that depend
        # on e.g. the authenticated state.
        url = url()
    redirect_url = (
        url or
        get_next_redirect_url(
            request,
            redirect_field_name=redirect_field_name) or
        get_adapter(request).get_login_redirect_url(request))
    return redirect_url

def get_request_param(request, param, default=None):
    return request.POST.get(param) or request.GET.get(param, default)

def getFieldHistory(object, model, field, params):
    content_type = ContentType.objects.get_for_model(model)
    return FieldHistory.objects.filter(object_id=object.id, content_type=content_type, field_name=field, **params)

def add_status(instance, name, typei):
    if typei and typei == "order" and instance.pk < 3000:
        return
    statuses, create = Statuses.objects.get_or_create(
                    name=name)
    instance.statuses.add(statuses)

