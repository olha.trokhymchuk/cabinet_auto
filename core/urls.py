from django.conf.urls import url
from .views import *
from .api.views import ApiUserDetail, ApiUserDetailActivity, ApiFilialDetail, ApiFilialUserList, \
    ApiFilialDetailActivity, send_message_to_email, global_search, transfertoissue, send_new_order_to_email, \
    change_user_avatar_by_mobile_app, ApiReviewsByUserList
from report.api.views import ApiReportByUserList, ApiReportByFilialList
from order.api.views import send_order_to_recheck


app_name = 'user'

urlpatterns = [
    url(r'^review_create/$', review_create, name='review_create'),
    url(r'^signup/$', SignupNewUser.as_view(), name='signup'),
    url(r'^error403/$', error403, name='error403'),
    url(r'^logout/$', auth_logout, name='logout'),
    url(r'^edit_self/$', UserUpdateSelfView.as_view(), name='edit_self'),
    url(r'^new_user/$', UserCreateView.as_view(), name='new_user'),
    url(r'^edit_user/(?P<user_id>\d+)/$',
        UserUpdateView.as_view(), name='edit_user'),
    url(r'^user-list/$', UserList.as_view(), name='user_list'),
    url(r'^filter/$', get_user_filter, name='user_filter'),
    url(r'^delete/(?P<user_id>\d+)/$', delete_user, name='delete_user'),
    url(r'^user_detail/(?P<user_id>\d+)/$',
        UserDetailView.as_view(), name='user_detail'),
    url(r'^update_employee_email_sender/$',
            update_employee_email_sender, name='update_employee_email_sender'),


    url(r'^reset-password/$', ResetPassword.as_view(), name='reset_password'),

    # user api
    url(r'^api/detail/(?P<pk>\d+)/$',
        ApiUserDetail.as_view(), name='user_detail_api'),
    url(r'^api/user_logs/(?P<pk>\d+)/$', ApiUserDetailActivity.as_view(),
        name='user_detail_api_activity'),
    url(r'^api/report_list/(?P<pk>\d+)/$',
        ApiReportByUserList.as_view(), name='user_reports'),
    url(r'^change_image/$', change_image, name='change_image'),
    url(r'^api/message/$', send_message_to_email, name='send_message_to_email'),
    url(r'^api/new-order-to-email/$',
        send_new_order_to_email, name='new-order-to-email'),

    url(r'^api/send_order_to_recheck/$',
        send_order_to_recheck, name='send_order_to_recheck'),

    # filial
    url(r'^filial_list/$', FilialList.as_view(), name='filial_list'),
    url(r'^filial-detail/(?P<filial_id>\d+)/$',
        FilialDetailView.as_view(), name='filial_detail'),
    url(r'^filial-edit/(?P<filial_id>\d+)/$',
        FilialUpdateView.as_view(), name='filial_edit'),
    url(r'^filial_add/$', FilialCreateView.as_view(), name='filial_add'),
    url(r'^delete-filial/(?P<filial_id>\d+)/$',
        delete_filial, name='delete_filial'),
    # filial api
    url(r'^api/filial_detail/(?P<pk>\d+)/$',
        ApiFilialDetail.as_view(), name='filial_detail_api'),
    url(r'^api/report_list_filial/(?P<pk>\d+)/$',
        ApiReportByFilialList.as_view(), name='filial_reports_api'),
    url(r'^api/user_list_filial/(?P<pk>\d+)/$',
        ApiFilialUserList.as_view(), name='filial_reports_api'),
    url(r'^api/filial_logs/(?P<pk>\d+)/$',
        ApiFilialDetailActivity.as_view(), name='filial_detail_api_activity'),
    url(r'^api/reviews_list/(?P<pk>\d+)/$',
        ApiReviewsByUserList.as_view(), name='user_reviews'),
    url(r'^user-reviews-list/$', UserReviewsList.as_view(), name='user_reviews_list'),
    url(r'^user-reviews/filter/$', get_review_filter, name='get_review_filter'),

    # search api
    url(r'^api/search/$', global_search, name='global_search_api'),

    url(r'^update_expert_crm/$', update_expert_crm, name='update_expert_crm'),

    # transfertoissue telebot
    url(r'^api/transfertoissue/$', transfertoissue, name='transfertoissue'),

    # change and delete user avatar from mobile app
    url(r'^change-user-avatar-by-mobile-app/$', change_user_avatar_by_mobile_app,
        name='change_user_avatar_by_mobile_app'),

]
