from django.conf import settings
from urllib.parse import quote
from urllib.request import urlopen
import requests
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponse


def recaptcha_check(function_to_decorate):
    def wrapper(request, *args, **kwargs):
        data = {'secret': settings.RE_CAPTCHA_SECRET_KEY, 'response': request.POST.get('recaptcha')}
        r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=data).json()
        if r["success"]:
            return function_to_decorate(request, *args, **kwargs)
        return HttpResponseBadRequest('recaptcha failed')

    return wrapper
