import logging

from allauth.account.signals import user_signed_up
from django.contrib.auth.models import Group
from django.core.exceptions import ObjectDoesNotExist
from django.dispatch import receiver, Signal

logger = logging.getLogger(__name__)


@receiver(user_signed_up)
def add_client_group(request, user, **kwargs):
    try:
        client_group = Group.objects.get(name='Клиент')
        user.groups.add(client_group)
        user.is_staff = True
        user.save()
    except ObjectDoesNotExist as ex:
        logger.error(ex)

user_logged_in = Signal(providing_args=["request", "user"])
