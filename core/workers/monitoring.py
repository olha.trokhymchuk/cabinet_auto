import subprocess
import logging
import os

from django.shortcuts import get_object_or_404

from autopodbor.utils import send_simple_message_mailgun
from core.models import User

from notifications.models import Notification
from notifications.utils import send_notification


from django.conf import settings

logger = logging.getLogger(__name__)


def serverMonitoring():	
	load_cpu = 0
	load_mem = 0
	load_system = []
	mem=str(os.popen('free -t -m').read())
	all_mem = mem.split()
	one_pircent = int(all_mem[7])/100
	remains_memory = int(all_mem[9])
	p = subprocess.Popen('ps -eo pid,ppid,cmd,%mem,%cpu', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
	for line in p.stdout.readlines()[1:]:
		try:
			process = line.split()
			cpu = 0
			mem = 0
			name = process[2].decode()
			ID = process[0].decode()
			try:
				cpu = float(process[4].decode())
				load_cpu += cpu
			except (IndexError, ValueError):
				pass
			try:
				mem = float(process[3].decode())
				load_mem += mem
			except (IndexError, ValueError):
				pass

		except UnicodeDecodeError:
			pass
		load_system.append({"ID": ID, "name": name, "mem": mem, "cpu": cpu})
	logger.error(one_pircent * settings.MONITOR_PERCENT_MEMORY)
	logger.error(remains_memory)
	if one_pircent * settings.MONITOR_PERCENT_MEMORY > remains_memory:
		load_mem_error =  "Общая Загрузка памяти: {}".format(remains_memory) 
		logger.error(load_system)
		load_cpu_error = "Общая Загрузка процессора: {}".format(int(load_cpu))
		logger.error(load_cpu_error)
		logger.error(load_mem_error)
		users = []
		data = {
			'name': "нагрузка системы",
			'error': str(load_mem_error)
		}
		user = get_object_or_404(User, username='79610577499')
		users.append(user)
		print(11111111111)
		#send_notification(recipients=users, action=Notification.MONITOR, data=data)
		send_simple_message_mailgun(
			"Мониторинг",
			"{}\n{}".format(load_cpu_error, load_mem_error),
			'info@ap4u.ru',
			"a.chernov@podbor.org"
        )
		load_system = []


	retval = p.wait()

