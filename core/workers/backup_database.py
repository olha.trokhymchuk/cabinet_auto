import os
import ftplib
import subprocess
from os import listdir

from django.conf import settings

import logging

logger = logging.getLogger(__name__)

def backup_database():
    server = settings.FTP_URL_BACKUP
    port = settings.FTP_PORT_BACKUP
    ftp = ftplib.FTP()
    ftp.connect(server, int(port))
    UID = settings.FTP_USER_BACKUP
    ftp.login(UID, settings.FTP_PSW_BACKUP)
    subprocess.call(['/home/aleksey/lk2backup.sh'])
    backap_files = listdir("/home/aleksey/backap")
    logger.error(1111111111111111)
    for filename in backap_files:
        source_file = "/home/aleksey/backap/{}".format(filename)
        try:
            ftp.storbinary("STOR " + filename, open(source_file, 'rb'))
        except Exception as ex:
            print(ex)
        else:
            os.remove(source_file)
    ftp.close()