from django.contrib.auth.models import Group
 
from .models import User
 
class addClientToUser:
	def __init__(self):
		self.add_client()

	def add_client(self):
		client_group = Group.objects.get(name='Клиент')
		users = User.objects.all()
		for user in users:
			user.is_staff = True
			user.save()
			if not user.groups.all():
				print(user)
				user.groups.add(client_group)
				user.save()
		print('done')

