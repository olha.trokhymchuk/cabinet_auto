import logging
import subprocess

from django.db import connections
from django.db.utils import OperationalError

from core.models import User

import integration.glosary as integration_glosary

logger = logging.getLogger(__name__)

class expertToCrm(object):
	"""docstring for expertToCrm"""
	def __init__(self):
		super(expertToCrm, self).__init__()
		try:
			self.cursor = connections['crm_db'].cursor()
		except (OperationalError, Exception) as ex:
			subprocess.call(['/home/aleksey/set_crm_port.sh'])
			self.cursor = connections['crm_db'].cursor()
		finally:
			self.set_experts()
			self.cursor.close()
		logger.error("expert uploaded")

	def set_experts(self):
		experts = User.objects.filter(groups__name='Эксперт').order_by('pk')
		for expert in experts:
			self.expert_to_crm(expert)

	def expert_to_crm(self, expert):
		for filial in expert.filials.all():
			region_code_value = filial.code
			if not expert.phone:
				continue
			res = self.select_query("phone", expert.phone, "region_code", region_code_value)
			if res:
				if expert.is_active:
					self.update_expert(expert, region_code_value)
				else:
					self.delete_expert(expert, region_code_value)

			else:
				print("insert")
				if expert.is_active and str(expert) != "Эксперт Авто-подбор.рф":
					self.insert_expert(expert, region_code_value)
		return True

	def display_all(self):
		self.cursor.execute("SELECT * FROM auto_experts")
		row = self.cursor.fetchall()
		logger.error(row)

	def select_query(self, param, value, region_code, region_code_value):
		sql = "SELECT * FROM auto_experts where {}={} AND {}='{}'".format(param, value, region_code, region_code_value)
		self.cursor.execute(sql)
		res = self.cursor.fetchall()
		return res

	def select_querys(self, param, value):
		logger.error(value)
		self.cursor.execute("SELECT * FROM auto_experts where {}={}".format(param, value))
		return self.cursor.fetchall()

	def insert_expert(self, expert, region_code):
		logger.error(expert.full_name)
		if expert.phone:
			sql = "INSERT INTO auto_experts values ('', '{}', '{}', '{}', '', '', '')".format(region_code, expert.full_name, expert.phone)
			print(sql)
			self.cursor.execute(sql) 
			return True

	def update_expert(self, expert, region_code):
		sql = "UPDATE auto_experts SET  name= '{}' WHERE phone = {} and region_code = '{}'".format(expert.full_name, expert.phone, region_code)
		self.cursor.execute(sql) 
		return True

	def delete_expert(self, expert, region_code):
		self.cursor.execute("DELETE FROM auto_experts  WHERE phone = {} and region_code = '{}'".format(expert.phone, region_code)) 
		return True

