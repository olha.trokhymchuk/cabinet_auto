import os
import ftplib
import json

from django.conf import settings

def load_users_on_mobile():
    from core.models import User
    filename = "users.json"
    file_user = "users.txt"
    users_txt = ""
    server = settings.FTP_URL
    ftp = ftplib.FTP(server)
    UID = settings.FTP_USER
    ftp.login(UID, settings.FTP_PSW)
    ftp.cwd("/mobile_app")
    users = User.objects.all()
    users_json = [{'id': user.pk, 'name': user.get_full_name(), 'phone': user.phone} for user in users if user.is_expert]
    for user in users:
        filials = ""
        for filial in user.filials.all():
            if len(filials) == 0:
                filials += "{}".format(filial)
            else:
                filials += ", {}".format(filial)
        if user.is_expert:
            full_name = "{} {} {}".format(user.last_name, user.first_name, user.patronymic)
            users_txt += "{} {} {} {} \n".format(full_name, filials, user.phone, user.email)
    users_json = json.dumps(users_json)
    text_file = open(filename, "w")
    text_file_txt = open(file_user, "w")
    text_file.write(users_json)
    text_file_txt.write(users_txt)
    text_file.close()
    text_file_txt.close()
    ftp.storbinary("STOR " + filename, open(filename, 'rb'))
    ftp.storbinary("STOR " + file_user, open(file_user, 'rb'))
    ftp.close()