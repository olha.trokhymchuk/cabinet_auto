import collections
import importlib
import inspect

import os
from django.apps import apps
from django.conf import settings
from django.contrib.auth.models import Permission
from django.contrib.contenttypes.models import ContentType
from django.test import TestCase
from django.views import View
from django.template.loaders.app_directories import get_app_template_dirs


class ViewTestCase(TestCase):
    def setUp(self):
        self.permissions = self.get_permissions()
        self.views = self.get_views()

    def get_permissions(self):
        permissions = []

        for app in apps.get_app_configs():
            for model in app.get_models():
                content_type = ContentType.objects.get_for_model(model)
                model_perms = Permission.objects.filter(content_type=content_type)
                model_perms = ['{}.{}'.format(app.name, perm.codename) for perm in model_perms]
                permissions.extend(model_perms)

        return permissions

    def get_views(self):
        project_apps = ['auto', 'core', 'order', 'report', 'service']
        views = []

        for app in project_apps:
            module = importlib.import_module('{}.views'.format(app))
            for name, obj in inspect.getmembers(module):
                if inspect.isclass(obj) and issubclass(obj, View) and hasattr(obj, 'permission_required'):
                    views.append(obj)

        return views

    def test_syntax(self):
        for view in self.views:
            if isinstance(view.permission_required, str):
                self.assertIn(view.permission_required, self.permissions)
            elif isinstance(view.permission_required, collections.Iterable):
                for perm in view.permission_required:
                    self.assertIn(perm, self.permissions)


# class TemplateTestCase(TestCase):
#     def setUp(self):
#         self.permissions = self.get_permissions()
#         self.permissions_used_in_template = self.get_permissions_used_in_template()
#
#     def get_permissions(self):
#         permissions = []
#
#         for app in apps.get_app_configs():
#             for model in app.get_models():
#                 content_type = ContentType.objects.get_for_model(model)
#                 model_perms = Permission.objects.filter(content_type=content_type)
#                 model_perms = ['perms.{}.{}'.format(app.name, perm.codename) for perm in model_perms]
#                 permissions.extend(model_perms)
#
#         return permissions
#
#     def get_permissions_used_in_template(self):
#         template_files = []
#         app_template_dirs = get_app_template_dirs('templates')
#         current_path = os.path.abspath(os.path.dirname(__file__))
#         print(current_path)
#
#         for template_dir in app_template_dirs:
#             if current_path in template_dir:
#                 for dir, dirnames, filenames in os.walk(template_dir):
#                     for filename in filenames:
#                         template_files.append(os.path.join(dir, filename))
#
#         print(template_files)
#         return template_files
#
#     def test_syntax(self):
#         for permission in self.permissions_used_in_template:
#             self.assertIn(permission, self.permissions)
