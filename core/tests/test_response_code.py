from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.test import Client, RequestFactory, TestCase
from django.urls import reverse
from field_history.models import FieldHistory


class _ResponseCodeTestCase(TestCase):
    fixtures = ['core/fixtures/Group.json', 'custom_admin/fixtures/permissions.json']

    def __init__(self, *args, **kwargs):
        super(_ResponseCodeTestCase, self).__init__(*args, **kwargs)
        self.factory = None
        self.request_client = None
        self.groups = ['Администратор', 'Руководитель', 'Бухгалтер', 'Контент-менеджер', 'Эксперт', 'Оператор', 'Клиент']
        self.clients = []
        self.url = ''
        self.codes = {}
        self.perm = ''

    def setUp(self):
        super(_ResponseCodeTestCase, self).setUp()
        self.factory = RequestFactory()

        for i, group_name in enumerate(self.groups):
            phone = '123456789{}'.format(i)
            user = get_user_model().objects.create_user(username='user{}'.format(i),
                                                        email='test{}@mail.test'.format(i),
                                                        password='test',
                                                        phone=phone)
            group = Group.objects.get(name=group_name)
            user.groups.add(group)
            client = Client()
            client.login(username=user.username, password='test')
            self.clients.append((user, client))

    def tearDown(self):
        super(_ResponseCodeTestCase, self).tearDown()
        FieldHistory.objects.all().delete()
        self.clients = []
        get_user_model().objects.all().delete()

    def test_response_codes(self):
        if not self.url:
            return

        for user, client in self.clients:
            response = client.get(self.url)
            group = user.groups.all()[0].name

            if user.has_perm(self.perm):
                expected_codes = [200]
            else:
                expected_codes = self.codes.get(group) if self.codes.get(group) else [302, 403, 404]

            self.assertIn(response.status_code,
                          expected_codes,
                          msg='URL {} code {} for {} not expected. Expected codes {}'.format(self.url,
                                                                                             response.status_code,
                                                                                             group,
                                                                                             expected_codes))

        client = Client()
        response = client.get(self.url)
        expected_codes = [302, 403, 404]
        self.assertIn(response.status_code,
                      expected_codes,
                      msg='URL {} code {} for {} not expected. Expected codes {}'.format(self.url,
                                                                                         response.status_code,
                                                                                         'anonymous',
                                                                                         expected_codes))


class UserUpdateSelfTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(UserUpdateSelfTestCase, self).__init__(*args, **kwargs)
        self.codes = {
            'Администратор': [200],
            'Руководитель': [200],
            'Бухгалтер': [200],
            'Контент-менеджер': [200],
            'Эксперт': [200],
            'Оператор': [200],
            'Клиент': [200],
            '*': [302, 403]
        }

        self.url = reverse('user:edit_self')


class UserCreateTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(UserCreateTestCase, self).__init__(*args, **kwargs)
        self.url = reverse('user:new_user')
        self.perm = 'core.add_user'


class UserUpdateTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(UserUpdateTestCase, self).__init__(*args, **kwargs)
        self.perm = 'core.change_user'

    def setUp(self):
        super().setUp()
        user = get_user_model().objects.create_user(username='test_user',
                                                    email='test_user@mail.test',
                                                    password='test',
                                                    phone='654646446546')

        self.url = reverse('user:edit_user', args=(user.id, ))


class UserListTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(UserListTestCase, self).__init__(*args, **kwargs)
        self.url = reverse('user:user_list')
        self.perm = 'core.view_user'


class UserDetailTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(UserDetailTestCase, self).__init__(*args, **kwargs)
        self.perm = 'core.view_user'

    def setUp(self):
        super().setUp()
        user = get_user_model().objects.create_user(username='tst_user',
                                                    email='tst_user@mail.test',
                                                    password='test',
                                                    phone='65464644665546')

        self.url = reverse('user:user_detail', args=(user.id, ))


class FilialListTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(FilialListTestCase, self).__init__(*args, **kwargs)
        self.url = reverse('user:filial_list')
        self.perm = 'core.view_filial'
