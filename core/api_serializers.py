import logging

from django.conf import settings
import order.order_constants as order_const
from django.db.models import Q
from django.db.models import Avg
from rest_framework import serializers

from .models import User, Filial, Reviews

from message.serializers import MessageSerializer

from custom_admin.utils import FilteredFieldApiMixin

from order.models import Order

from report.models import ReportPodbor

from field_history.models import FieldHistory

from .serializers import EquipmentSerializer

logger = logging.getLogger(__name__)

class UserSerializer(serializers.ModelSerializer, FilteredFieldApiMixin):
    field_perms_model = User
    def __init__(self, *args, **kwargs):
        super(UserSerializer, self).__init__(*args, **kwargs)
        self.perms = None
        self.get_perms()


    date_joined = serializers.DateTimeField(format=settings.DATETIME_FORMAT, required=False)
    full_name = serializers.SerializerMethodField()
    filials = serializers.SerializerMethodField()
    groups = serializers.SerializerMethodField()
    avatar_thumbnail = serializers.SerializerMethodField()
    order = serializers.SerializerMethodField()
    report_count = serializers.SerializerMethodField()
    selection_count = serializers.SerializerMethodField()
    outside_diagnose_count = serializers.SerializerMethodField()
    expert_day_count = serializers.SerializerMethodField()
    other_count = serializers.SerializerMethodField()
    transferringord_count = serializers.SerializerMethodField()
    user_logs_count = serializers.SerializerMethodField()
    user_sid = MessageSerializer(many=True, required=False)
    user_fid = MessageSerializer(many=True, required=False)
    last_name = serializers.SerializerMethodField()
    phone = serializers.SerializerMethodField()
    email = serializers.SerializerMethodField()
    user_equipment = EquipmentSerializer(many=True, read_only=True)
    expert_rating = serializers.SerializerMethodField()
    reviews_count = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'first_name', 'last_name', 'email', 
                    'phone', 'date_joined', 'full_name', 
                    'avatar', 'avatar_thumbnail', 'filials', 
                    'groups', 'user_sid', 'user_fid', 'order',
                    'report_count', 'user_logs_count', 'selection_count', 'transferringord_count', 
                    'outside_diagnose_count','expert_day_count','other_count', 'user_equipment',
                    'expert_rating', 'reviews_count',
                )

    def get_full_name(self, obj):
        if obj.last_name is None:
            return '{}'.format(obj.first_name, obj.last_name)  
        return '{} {}'.format(obj.first_name, obj.last_name)  
    
    def get_last_name(self, obj):
        if 'last_name' in self.perms['user']:
            return obj.last_name
        return ""

    def get_phone(self, obj):
        if 'phone' in self.perms['user']:
            return obj.phone
        return ""

    def get_email(self, obj):
        if 'email' in self.perms['user']:
            return obj.email
        return ""

    def get_filials(self, obj):
        if 'filials' in self.perms['user']:
            filials = obj.filials.all()
            return ", ".join([f.name for f in filials])
        return ""

    def get_groups(self, obj):
        if 'groups' in self.perms['user']:
            groups = obj.groups.all()
            return ", ".join([g.name for g in groups])
        return ""

    def get_avatar_thumbnail(self, obj):
        try:
            return obj.avatar_thumbnail.url if obj.avatar else ''
        except FileNotFoundError:
            return ''

    def get_order(self, obj):
        return obj.user_ex.filter(order_type='PPK', status_closing=False).count()

    def get_report_count(self, obj):
        return obj.reportpodbor_set.all().count()

    def get_user_logs_count(self, obj):
        return obj.fieldhistory_set.all().count()

    def get_selection_count(self, obj):
        try:
            self.order = Order.objects.filter( Q(operator=self.instance) |
                                        Q(expert=self.instance) |
                                        Q(expert_check=self.instance) |
                                        Q(transferring=self.instance) |
                                        Q(transferring=self.instance) |
                                        Q(client=self.instance))
            logger.error(self.order)
            if self.instance.is_acounter or self.instance.is_leader:
                self.order = self.order.filter(filial__in=self.filials.all())
                logger.error(self.order)
        except (AttributeError, KeyError) as ex:
            print(ex)
        try:
            logger.error(self.order)
            return self.order.filter(order_type='PPK').count()
        except AttributeError as ex:
            return 0

    def get_outside_diagnose_count(self, obj):
        logger.error(self.order);
        try:
            return self.order.filter(order_type='VZD').count()
        except AttributeError as ex:
            return 0

    def get_expert_day_count(self, obj):
        try:
            logger.error(self.order)
            return self.order.filter(order_type='END').count()
        except AttributeError as ex:
            return 0

    def get_other_count(self, obj):
        try:
            logger.error(self.order)
            return self.order.exclude(order_type__in=order_const.EXCLUDEORDERTYPE).count()
        except AttributeError as ex:
            return 

    def get_transferringord_count(self, obj):
        # try:
        #     logger.error(self.order)
        #     # self.order = Order.objects.filter(transferring=self.user_page)
        #     # if self.instance.is_acounter or self.instance.is_leader:
        #     #     logger.error(self.order)
        #     #     self.order = self.order.filter(filial__in=self.filials.all())
        # except (AttributeError, KeyError) as ex:
        #     print(ex)
        try:
            return self.order.filter(order_type='PPK').count()
        except AttributeError as ex:
            return 0

    def get_expert_rating(self, obj):
        rating = obj.user_ex.filter(review__isnull=False).aggregate(Avg('review__rating'))
        if rating['review__rating__avg']:
            return round(rating['review__rating__avg'], 1)
        return None

    def get_reviews_count(self, obj):
        try:
            return obj.user_ex.filter(review__isnull=False).count()
        except AttributeError as ex:
            return 0

class FilialSerializer(serializers.ModelSerializer, FilteredFieldApiMixin):
    field_perms_model = User
    leaders = None
    def __init__(self, *args, **kwargs):
        super(FilialSerializer, self).__init__(*args, **kwargs)
        self.perms = None
        self.get_perms()

    report_count = serializers.SerializerMethodField()
    order_count = serializers.SerializerMethodField()
    logs_count = serializers.SerializerMethodField()
    users_count = serializers.SerializerMethodField()

    class Meta:
        model = Filial
        fields = '__all__'


    def get_report_count(self, obj):
        return ReportPodbor.objects.filter(executor__filials__id=self.instance.pk).distinct().count()

    def get_order_count(self, obj):
        try:
            return Order.objects.filter(filial=self.instance.pk).count()
        except (AttributeError, KeyError) as ex:
            logger.error(ex)
        return 0

    def get_logs_count(self, obj):
        return FieldHistory.objects.filter(user__filials=self.instance.pk).count()

    def get_users_count(self, obj):
        queryset = obj.users.exclude(groups__name='Клиент')
        if self.context['request'].user.is_leader:
            queryset = queryset.filter(groups__name='Эксперт')
        return queryset.count()