from django.db import models
from .utils import delete_media_by_id, get_media_url_by_baseUrl, get_media_baseUrl
from django.utils import timezone
from datetime import timedelta

class GooglePhotoImage(models.Model):
    class Meta:
        verbose_name = "Картинка google"
        verbose_name_plural = "Картинки с google"

    imageId = models.CharField(
        'imageGoogleId', max_length=500, null=True, blank=True)
    albumId = models.CharField(
        'albumGoogleId', max_length=500, null=True, blank=True)
    baseUrl = models.TextField(
        'imageGoogleId', null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.baseUrl = get_media_baseUrl(self.imageId)
        else:
            if timezone.now() - self.updated_at > timedelta(minutes=40) or not self.baseUrl:
                self.baseUrl = get_media_baseUrl(self.imageId)
        super(GooglePhotoImage, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if self.imageId and self.albumId:
            delete_media_by_id(self.imageId, self.albumId)
        super(GooglePhotoImage, self).delete(*args, **kwargs)

    @property
    def get_small_url(self):
        if timezone.now() - self.updated_at > timedelta(minutes=40) or not self.baseUrl:
            self.baseUrl = get_media_baseUrl(self.imageId)
            self.save()
            return get_media_url_by_baseUrl(self.baseUrl, 150, 150)
        else:
            return get_media_url_by_baseUrl(self.baseUrl, 150, 150)

    @property
    def get_url(self):
        if timezone.now() - self.updated_at > timedelta(minutes=40) or not self.baseUrl:
            self.baseUrl = get_media_baseUrl(self.imageId)
            self.save()
            return get_media_url_by_baseUrl(self.baseUrl)
        else:
            return get_media_url_by_baseUrl(self.baseUrl)

    @property
    def get_base_url(self):
        if timezone.now() - self.updated_at > timedelta(minutes=40) or not self.baseUrl:
            self.baseUrl = get_media_baseUrl(self.imageId)
            self.save()
            return self.baseUrl
        else:
            return self.baseUrl
