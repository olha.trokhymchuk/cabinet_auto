from django.conf import settings
import pickle
import os
from datetime import datetime
from google_auth_oauthlib.flow import Flow, InstalledAppFlow
from googleapiclient.discovery import build
from google.auth.transport.requests import Request
from autopodbor.settings import API_NAME, API_VERSION, SCOPES, \
    CLIENT_SECRET_FILE, \
    CLIENT_SECRET_FILE_2, \
    CLIENT_SECRET_FILE_3, \
    CLIENT_SECRET_FILE_4, \
    CLIENT_SECRET_FILE_5, \
    CLIENT_SECRET_FILE_6, \
    CLIENT_SECRET_FILE_7, \
    CLIENT_SECRET_FILE_8, \
    CLIENT_SECRET_FILE_9, \
    CLIENT_SECRET_FILE_10, \
    CLIENT_SECRET_FILE_11, \
    CLIENT_SECRET_FILE_12, \
    CLIENT_SECRET_FILE_13, \
    CLIENT_SECRET_FILE_14, \
    CLIENT_SECRET_FILE_15, \
    CLIENT_SECRET_FILE_16, \
    CLIENT_SECRET_FILE_17, \
    CLIENT_SECRET_FILE_18, \
    CLIENT_SECRET_FILE_19, \
    CLIENT_SECRET_FILE_20

os.environ['DJANGO_SETTINGS_MODULE'] = 'autopodbor.settings'
GOOGLE_PHOTO_TOKENS_FOLDER = settings.GOOGLE_PHOTO_TOKENS_FOLDER


def Create_Service():
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format(API_NAME, API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None

def Create_Get_Service():
    print('getImages1')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_2():
    print('getImages2')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image2', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_2, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_3():
    print('getImages3')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image3', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_3, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_4():
    print('getImages4')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image4', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_4, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_5():
    print('getImages5')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image5', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_5, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_6():
    print('getImages6')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image6', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_6, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_7():
    print('getImages7')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image7', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_7, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_8():
    print('getImages8')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image8', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_8, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_9():
    print('getImages9')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image9', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_9, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_10():
    print('getImages10')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image10', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_10, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_11():
    print('getImages11')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image11', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_11, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_12():
    print('getImages12')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image12', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_12, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_13():
    print('getImages13')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image13', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_13, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_14():
    print('getImages14')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image14', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_14, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_15():
    print('getImages15')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image15', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_15, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_16():
    print('getImages16')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image16', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_16, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_17():
    print('getImages17')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image17', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_17, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_18():
    print('getImages18')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image18', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_18, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_19():
    print('getImages19')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image19', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_19, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def Create_Get_Service_20():
    print('getImages20')
    cred = None
    pickle_file = GOOGLE_PHOTO_TOKENS_FOLDER + 'token_{}_{}.pickle'.format('photoslibrary_get_image20', API_VERSION)
    if os.path.exists(pickle_file):
        with open(pickle_file, 'rb') as token:
            cred = pickle.load(token)
    if not cred or not cred.valid:
        if cred and cred.expired and cred.refresh_token:
            cred.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(CLIENT_SECRET_FILE_20, SCOPES)
            cred = flow.run_local_server()

        with open(pickle_file, 'wb') as token:
            pickle.dump(cred, token)
    try:
        service = build(API_NAME, API_VERSION, credentials=cred, cache_discovery=False)
        return service
    except Exception as e:
        print(e)
    return None


def convert_to_RFC_datetime(year=1900, month=1, day=1, hour=0, minute=0):
    dt = datetime.datetime(year, month, day, hour, minute, 0).isoformat() + 'Z'
    return dt
