from django.conf.urls import url
from .views import upload_photo, get_google_photo, get_google_photo_by_id

app_name = 'google_photo'

urlpatterns = [
    url(r'^upload_photo/$', upload_photo, name='upload_photo'),
    url(r'^google_images/(?P<photo_id>[A-Za-z0-9_-]+)/(?P<w>\d+)/(?P<h>\d+)/$', get_google_photo, name='get_google_photo_sizes'),
    url(r'^google_images/(?P<photo_id>[A-Za-z0-9_-]+)/$', get_google_photo, name='get_google_photo'),
    url(r'^google_images_obj/(?P<photo_id>[A-Za-z0-9_-]+)/(?P<w>\d+)/(?P<h>\d+)/$', get_google_photo_by_id, name='get_google_photo_sizes_by_id'),
    url(r'^google_images_obj/(?P<photo_id>[A-Za-z0-9_-]+)/$', get_google_photo_by_id, name='get_google_photo_by_id')
]
