import pickle
import random
from django.conf import settings
GOOGLE_PHOTO_TOKENS_FOLDER = settings.GOOGLE_PHOTO_TOKENS_FOLDER

import requests
from .Google import Create_Service, Create_Get_Service,\
    Create_Get_Service_2,\
    Create_Get_Service_3,\
    Create_Get_Service_4,\
    Create_Get_Service_5,\
    Create_Get_Service_6,\
    Create_Get_Service_7,\
    Create_Get_Service_8,\
    Create_Get_Service_9,\
    Create_Get_Service_10,\
    Create_Get_Service_11,\
    Create_Get_Service_12,\
    Create_Get_Service_13,\
    Create_Get_Service_14,\
    Create_Get_Service_15,\
    Create_Get_Service_16,\
    Create_Get_Service_17,\
    Create_Get_Service_18,\
    Create_Get_Service_19,\
    Create_Get_Service_20

from autopodbor.utils import get_watermark
from autopodbor.settings import GOOGLE_PHOTO_UPLOAD_URL


def creat_google_album(album_name):
    """
        Создает альбом с переданого имени

        :param: album_name

            Имя создаваемого альбома, если такой альбом существует то он не создастя

        :return: album

            Обект созданого альбома с его параметрами

    """
    service = Create_Service()
    request_body = {
        'album': {'title': str(album_name)}
    }
    return service.albums().create(body=request_body).execute()


def share_google_album(album_id):
    """

    Функция розшаривания альбома по его id

    :param album_id:

        id альбома который розшариваем

    :return:

        googlePhotoAPI object (ShareInfo)

    """
    service = Create_Service()
    request_body = {
        "sharedAlbumOptions": {
            "isCollaborative": False,
            "isCommentable": False
        }
    }
    try:
        shared_info = service.albums().share(body=request_body, albumId=album_id).execute()
    except:
        shared_info = service.albums().get(albumId=album_id).execute()
    return shared_info


def upload_images_by_tokens(tokens):
    """

    Выгрузка картинок на гугл бетч запросом по их токенах

    :param tokens:

        Токены

    :return:

        Словарь результатов

    """
    service = Create_Service()
    result = {}
    for item in tokens:
        upload_response = service.mediaItems().batchCreate(body=item).execute()
        result.update({item['albumId']: upload_response["newMediaItemResults"]})
    return result


def load_media_to_album_google(token, album_id, service):
    """

    Функци добавляет картинку по ее токену в указаный альбом гугла.

    :param token:

        Токен полученый при загрузки картинки на гугл

    :param album_id:

        id альбома в который мы грузим картинку

    :param service:

        Созданый обект сервиса

    :return:

        Список в котором хранится id картинки которую мы выгрузили

    """
    new_media_item = [{'simpleMediaItem': {'uploadToken': token}}]
    request_body = {
        "albumId": album_id,
        'newMediaItems': new_media_item
    }
    try:
        upload_response = service.mediaItems().batchCreate(body=request_body).execute()
    except:
        return False

    ids_list = [media_list['mediaItem']['id'] for media_list in upload_response["newMediaItemResults"]]
    return ids_list


def upload_media_data_to_google(image):
    """

    Выгрузка медии на гугл и получение токена

    :param image:
        Файл картинки
    :return:

        token_google,service токен картинки и созданый объект серввиса

    """
    service = Create_Service()
    token = pickle.load(open(GOOGLE_PHOTO_TOKENS_FOLDER + 'token_photoslibrary_v1.pickle', 'rb'))
    headers = {
        'Authorization': 'Bearer ' + token.token,
        'Content-type': 'application/octet-stream',
        'X-Goog-Upload-Protocol': 'raw',
    }
    img_bytes = get_watermark(image, from_google=True)
    response = requests.post(GOOGLE_PHOTO_UPLOAD_URL, data=img_bytes, headers=headers)
    token_google = response.content.decode('utf-8')
    return token_google, service


def upload_media_to_google(image, album_id):
    """
    Выгрузка картинки на гугл фото.

    :param image:
        Файл картинки
    :param album_id:
        Id альбома в который мы загружаем эту картинку
    :return:

        ids_list список в котором хранится id картинки

    """
    token_google, service = upload_media_data_to_google(image)
    ids_list = load_media_to_album_google(token_google, album_id, service)

    return ids_list


def get_media_baseUrl(imageId):
    """
        Получение базовой ссылки на картинку гугла

    :param imageId:
    :return:

        Ссылка на картинку (Базовая)

    """
    service_list = ['', '_2', '_3', '_4', '_5', '_6', '_7', '_8', '_9', '_10', '_11', '_12', '_13', '_14', '_15', '_16',
                    '_17', '_18', '_19', '_20']
    service = random.choice(service_list)

    service = globals()['Create_Get_Service' + service]()

    try:
        response = service.mediaItems().get(mediaItemId=imageId).execute()
        base_url = response.get('baseUrl')
        return base_url
    except:
        return '#none'


def get_media_url_by_baseUrl(url, w=3500, h=3500):
    """
        Получение ссылки на картинку по ее базовой ссылке и размерам

    :param url:
    :param w:
    :param h:
    :return:

        Сслка на картинку

    """
    photo_url = url + '=w{}-h{}'.format(w, h)
    return photo_url


def get_media_url_by_id(photo_id, w=3500, h=3500):
    """

        Функция для получения ссылки на картинку по ее id, с возможностью задать ширину и высоту фотографии.

    :param photo_id:

        id фотографии на гугл сервисе

    :param w:
        Ширина желаемой фотографии
    :param h:
        Высота желаемой фотографии
    :return:
        Ссылка на саму картинку ( не вечная )
    """
    service = Create_Get_Service()
    try:
        response = service.mediaItems().get(mediaItemId=photo_id).execute()
        base_url = response.get('baseUrl')
        photo_url = get_media_url_by_baseUrl(base_url)
        return photo_url
    except:
        return '#none'


def delete_media_by_id(photo_id, album_id):
    """

    Функция удаления фотографии с альбома по ее id и id альбома

    :param photo_id:

        id фотографии

    :param album_id:

        id альбома

    :return:

        JSON ответ от гугла

    """
    service = Create_Service()
    request_body = {
        'mediaItemIds': [photo_id]
    }
    return service.albums().batchRemoveMediaItems(body=request_body, albumId=album_id).execute()
