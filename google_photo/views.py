from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_exempt
from .utils import upload_media_to_google, get_media_url_by_id, get_media_url_by_baseUrl
from django.http import JsonResponse, Http404, HttpResponse, FileResponse
from pprint import pprint
import requests
from .models import GooglePhotoImage
from report.google_photo_utils import add_google_photo_to_report


@csrf_exempt
def upload_photo(request):
    """

    Функция для загрузки фотографии на гугл и подвязки ее к отчету.

    :param request:

        POST запрос с параметрами image: картинка, album_id: id альбома в который мы загружаем фотку,
            report_id: id репорта которому загружаем фотку, photo_type: тип фотографии

    :return:

        JSON ответ с ссылкой на фотку

    """
    if request.method == 'POST':
        result = upload_media_to_google(request.FILES['image'], request.POST['album_id'])
        if result and result[0]:
            report_id = request.POST['report_id']
            photo_type = request.POST['photo_type']
            add_google_photo_to_report(result[0], report_id, photo_type)
            return JsonResponse(['/google_images/' + result[0]], safe=False)
        else:
            return HttpResponse('Album not exist!', status=400)

    else:
        return HttpResponse('Method not allowed!', status=400)


def get_google_photo(request, photo_id, w=3500, h=3500):
    """

        Функция возврощает саму картинки по ее id и параметрам размера

    :param request:

        Объект запроса

    :param photo_id:

        id фотографии с гугла

    :param w:

        Ширина фотографии

    :param h:

        Высота фотографии

    :return:

        image byte

    """
    image_url = get_media_url_by_id(photo_id, w, h)
    image_response = requests.get(image_url)
    response = FileResponse(image_response)
    response['Content-Type'] = image_response.headers['content-type']
    return response


def get_google_photo_by_id(request, photo_id, w=3500, h=3500):
    image = GooglePhotoImage.objects.get(pk=int(photo_id))
    image_url = get_media_url_by_baseUrl(image.get_base_url, w, h)
    image_response = requests.get(image_url)
    response = FileResponse(image_response)
    response['Content-Type'] = image_response.headers['content-type']
    return response
