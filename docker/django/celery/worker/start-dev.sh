#!/bin/sh

set -o errexit
set -o nounset
set -o xtrace

celery worker -A autopodbor -c 2 --loglevel=INFO --queues=celery,default
