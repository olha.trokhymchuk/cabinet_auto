#!/bin/sh

set -o errexit
set -o nounset
set -o xtrace

rm -f './celerybeat.pid'
celery beat -A autopodbor -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler
