#!/bin/bash

DIR="autopodbor"
comand="-c"
START=$1
TEST=$1
FILE=$2
MODEL=$2
NUMBER=$3
first="start"
tests="tests"
test="test"
shell="shell"
deb="deb"
vertual="venv"
libs="libs"
static="static"
loaddata="loaddata"
loaddatareport="loaddatareport"
dumpdata="dumpdata"
train="train"
server="server"
PyTorStemPrivoxy="PyTorStemPrivoxy"
startapp="startapp"
help="help"
celery="celery"
complete_deploy="complete_deploy"
full_deploy="full_deploy"
complete_deploy_prod="complete_deploy_prod"
full_deploy_prod="full_deploy_prod"
complete_deploy_front="complete_deploy_front"
full_deploy_front="full_deploy_front"
echo $PWD
pg="pg"
ENV="autopodbor"
NOTEBOOK="notebook"
PGFULL="pgfull"
# PREFIX=/home/serrrgggeee/anaconda3
export PATH=~/anaconda3/bin:$PATH
# export LD_LIBRARY_PATH=/usr/local/cuda/lib64/
# export LD_LIBRARY_PATH=/usr/lib/nvidia-384
export TESSDATA_PREFIX="/usr/share/locale/tessdata"	

export LD_LIBRARY_PATH="/usr/local/lib:/usr/local/cuda/lib64:/usr/lib/x86_64-linux-gnu:/usr/lib/nvidia-384/"
echo $LD_LIBRARY_PATH

migrate="migrate"
migrate_only="migrate_only"

runserver="runserver"

if [ "$START" = "$deb" ]
	then
	filename='deb_requirements.txt'
	filelines=`cat $filename`
	echo Start
	for line in $filelines ; do
    	sudo apt-get install -y $line
	done
fi

if [ "$START" = "$pg" ]
	then
	sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main"
	wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
	sudo apt-get update
	filename='postgres_required.txt'
	filelines=`cat $filename`
	echo Start
	for line in $filelines ; do
    	sudo apt-get install -y $line
	done

	sudo gedit /etc/postgresql/9.6/main/pg_hba.conf
	sudo /etc/init.d/postgresql restart
	psql -U postgres
	sudo gedit /etc/postgresql/9.6/main/pg_hba.conf
	sudo /etc/init.d/postgresql restart

fi

if [ "$START" = "$vertual" ]
	then
	filename='virtual.txt'
	filelines=`cat $filename`
	echo Start
	for line in $filelines ; do
    	sudo apt-get install -y $line
	done
	export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
	source /usr/local/bin/virtualenvwrapper.sh
	sudo -H pip3 install virtualenv virtualenvwrapper
	mkvirtualenv --python=python3 "$DIR"
	workon
	workon "$DIR"

fi

if [ "$START" = "$first" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		pip3 install -r requirements.txt
		# cd "${PWD}/autopodbor"
		echo $PWD
		python manage.py migrate auth
		python manage.py migrate
		python manage.py makemigrations
		python manage.py migrate
		python manage.py createsuperuser
		python manage.py runserver
fi

if [ "$START" = "$libs" ]
	then
		echo $libs
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		pip3 install -r requirements.txt
		echo $PWD
fi

if [ "$START" = "$static" ]
	then
		echo $static
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $PWD
		python manage.py collectstatic
		python manage.py runserver
fi

if [ "$START" = "$loaddata" ]
	then
		echo $loaddata
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $PWD
		python manage.py loaddata ./auto/fixtures/MarkAuto.json
		python manage.py loaddata ./auto/fixtures/ModelAuto.json
		python manage.py loaddata ./auto/fixtures/Generation.json
		python manage.py loaddata ./core/fixtures/Group.json
		python manage.py loaddata ./core/fixtures/Site.json
		python manage.py loaddata ./notifications/fixtures/MailTemplate.json
		python manage.py loaddata ./auto/fixtures/celery-beat.json
		python manage.py loaddata ./auto/fixtures/Year.json
		python manage.py loaddata ./auto/fixtures/BodyType.json
		python manage.py loaddata ./custom_admin/fixtures/permissions.json
		python manage.py loaddata ./report/fixtures/steps.json
		python manage.py runserver
fi
if [ "$START" = "$loaddatareport" ]
	then
		echo $loaddatareport
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $PWD

		python manage.py loaddata ./custom_admin/fixtures/permissions.json
		python manage.py loaddata ./core/fixtures/GroupPermissions.json
		python manage.py loaddata ./core/fixtures/Group.json
		python manage.py loaddata ./report/fixtures/steps.json
		# python manage.py loaddata ./auto/fixtures/auto.json
		# python manage.py loaddata ./report/fixtures/mobile.json
		# python manage.py loaddata ./report/fixtures/report_podbor.json

fi

if [ "$START" = "$dumpdata" ]
	then
		echo $dumpdata
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $PWD	

		python manage.py dumpdata --format=json report.Steps > ./report/fixtures/steps.json
		# python manage.py dumpdata --format=json --natural-foreign --indent=4 report.ReportPodbor > ./report/fixtures/report_podbor.json
		# python manage.py dumpdata --format=json report.Photo > ./report/fixtures/inspection.json
		# python manage.py dumpdata --format=json report.SurrenderPhoto > ./report/fixtures/surrphoto.json
		# python manage.py dumpdata --format=json auto.Auto > ./auto/fixtures/auto.json
		# python manage.py dumpdata --format=json auto.BodyType > ./auto/fixtures/BodyType.json
		# python manage.py dumpdata --format=json core.User > ./core/fixtures/user.json
		python manage.py dumpdata --format=json custom_admin.Storage > ./custom_admin/fixtures/permissions.json
		python manage.py dumpdata --format=json auth.Group > ./core/fixtures/Group.json
		python manage.py dumpdata --format=json auth.Permission > ./core/fixtures/GroupPermissions.json
		python manage.py dumpdata --format=json dbmail.MailTemplate > ./notifications/fixtures/MailTemplate.json

fi

if [ "$START" = "$runserver" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"

		echo $PWD
		python manage.py runserver
fi

if [ "$START" = "$shell" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $PWD
		python manage.py shell
fi

if [ "$START" = "$NOTEBOOK" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		jupyter notebook
fi

if [ "$START" = "$PGFULL" ]
	then
		cd "${PWD}/autopodbor"
		echo $PWD
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		python manage.py update_search_field  pruduct Product
fi

if [ "$START" = "$migrate" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		# cd "${PWD}/autopodbor"
		echo $PWD
		python manage.py makemigrations
		python manage.py migrate $FILE
		python manage.py runserver
fi

if [ "$START" = "$migrate_only" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		# cd "${PWD}/autopodbor"
		echo $PWD
		python manage.py makemigrations --merge
		python manage.py migrate
		python manage.py runserver
fi

if [ "$START" = "$train" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		cd "${PWD}/autopodbor/textlearn/"
		echo $PWD
		python $FILE
fi

if [ "$START" = "$server" ]
	then
		if [ "$FILE" = "stop" ]
			then
				sudo /etc/init.d/nginx $FILE
				sudo /etc/init.d/mysql $FILE
				sudo /etc/init.d/elasticsearch $FILE
				sudo /etc/init.d/apache2 $FILE
				sudo /etc/init.d/postgresql $FILE
		fi
		if [ "$FILE" = "start" ]
			then
				
				sudo /etc/init.d/postgresql $FILE
				sudo service redis $FILE
		fi
fi


if [ "$START" = "$startapp" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $PWD
		python manage.py startapp $FILE
		python manage.py runserver
fi

if [ "$START" = "$help" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $PWD
		python manage.py migrate $MODEL $NUMBER
		# python manage.py migrate --help
fi

if [ "$START" = "$tests" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $PWD
		python manage.py tests
fi

if [ "$START" = "$test" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $PWD
		python manage.py test $TEST
fi

if [ "$START" = "$celery" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		# cd "${PWD}/autopodbor"
		echo $PWD
		celery -A autopodbor -Q default worker -l info
		# celery -A autopodbor worker --concurrency=1 -l info --queues=report,users,default --purge --beat

fi

if [ "$START" = "$complete_deploy" ]
	then
			
		fab dev complete_deploy
fi
if [ "$START" = "$full_deploy" ]
	then
		source ~/venv/fab/bin/activate
		fab dev full_deploy
fi

if [ "$START" = "$complete_deploy_prod" ]
	then
			
		fab prod complete_deploy
fi
if [ "$START" = "$full_deploy_prod" ]
	then
		source ~/venv/fab/bin/activate
		fab prod full_deploy
fi

if [ "$START" = "$complete_deploy_front" ]
	then
			
		fab front complete_deploy
fi
if [ "$START" = "$full_deploy_front" ]
	then
		source ~/venv/fab/bin/activate
		fab front full_deploy
fi


if [ "$START" = "$PyTorStemPrivoxy" ]
	then
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"

		echo $PWD
		python  auto/PyTorStemPrivoxy.py
fi

if [ "$START" = "$comand" ]
	then
		echo $comand
		export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3.5
		source /usr/local/bin/virtualenvwrapper.sh
		workon "$ENV"
		echo $2
		$2 $3 $4 $5 $6
		echo $PWD
fi

