from django.conf.urls import url
from .views import *

app_name = 'stat'

urlpatterns = [
    url(r'^expert_week/$', stat_expert_week, name='exp_week'),
]