from django.shortcuts import render
from django.contrib.auth.decorators import login_required



# Статистика по экспертам.
@login_required
def stat_expert_week(request):
    return render(request, "experts/users_week.html", {
        'html_title': 'Статистика по экспертам за неделю',
        'page_title_span': 'Статистика',
        'page_title': 'по экспертам за неделю'
    })

