# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-09-10 10:30
from __future__ import unicode_literals

import django.contrib.postgres.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('service', '0016_auto_20180422_1140'),
    ]

    operations = [
        migrations.AlterField(
            model_name='podborauto',
            name='color_auto',
            field=django.contrib.postgres.fields.ArrayField(base_field=models.PositiveSmallIntegerField(choices=[(0, 'любой'), (1, 'чёрный'), (2, 'серебристый'), (3, 'белый'), (4, 'серый'), (5, 'синий'), (6, 'красный'), (7, 'зелёный'), (8, 'коричневый'), (9, 'бежевый'), (10, 'голубой'), (11, 'золотистый'), (12, 'пурпурный'), (13, 'фиолетовый'), (14, 'жёлтый'), (15, 'оранжевый'), (16, 'розовый'), (17, 'кроме белого'), (18, 'кроме красного'), (19, 'кроме синего'), (20, 'кроме желтого'), (21, 'кроме черного'), (22, 'кроме зеленого')], default=0, verbose_name='Цвет'), blank=True, default=list, size=None, verbose_name='Цвет'),
        ),
    ]
