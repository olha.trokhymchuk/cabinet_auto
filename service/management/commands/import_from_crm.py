from django.core.management.base import BaseCommand

from service.import_from_crm import import_from_crm

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        import_from_crm()