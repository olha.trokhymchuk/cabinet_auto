from rest_framework import serializers

from auto.glossary import *
from service.models import PodborAuto


class ListOfChoices(serializers.Field):
    def __init__(self, choices):
        super(ListOfChoices, self).__init__()
        self.choices = choices

    def to_representation(self, obj):
        result = []
        for i in obj:
            for key, value in self.choices:
                if i == key:
                    result.append(value)
                    break
        return result


class PPKSerialializer(serializers.ModelSerializer):
    engine_capacity = serializers.SerializerMethodField()
    year_auto = serializers.SerializerMethodField()
    transmission_type = ListOfChoices(choices=TRANSMISSION_TYPE)
    engine_type = ListOfChoices(choices=ENGINE_TYPE)
    drive_type = ListOfChoices(choices=DRIVE_TYPE)
    color_auto = ListOfChoices(choices=COLOR_OF_AUTO)
    color_salon = ListOfChoices(choices=COLOR_SALON_TYPE)
    salon_auto = ListOfChoices(choices=SALON_AUTO)

    class Meta:
        model = PodborAuto
        depth = 1
        fields = '__all__'

    def get_engine_capacity(self, obj):
        capacity = {}
        if obj.engine_capacity:
            capacity.update({'lower': obj.engine_capacity.lower})
            capacity.update({'upper': obj.engine_capacity.upper})

        return capacity

    def get_year_auto(self, obj):
        year = {}
        if obj.year_auto:
            year.update({'lower': obj.year_auto.lower})
            year.update({'upper': obj.year_auto.upper})

        return year
