from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField, IntegerRangeField, FloatRangeField
from django.db import models
from django.db.models import Q
from safedelete import HARD_DELETE
from safedelete.models import SafeDeleteModel

from core.utils import CustomFieldHistoryTracker
from notifications.models import Notification
from notifications.utils import send_notification
from order.models import Order
from auto.models import MarkAuto, ModelAuto, Generation, BodyType
from auto.glossary import *

from psycopg2.extras import NumericRange


# Подбор под ключ.
class PodborAuto(SafeDeleteModel):
    _safedelete_policy = HARD_DELETE

    PODBOR_TYPE = (
        ('KR', 'Критерий'),
        ('PB', 'Подбор'),
    )
    created = models.DateTimeField('Дата создания', auto_now_add=True, db_index=True, )
    modified = models.DateTimeField('Последнее изменение', auto_now=True)
    order = models.ForeignKey(Order, blank=True, null=True, default=None, verbose_name='Заказ', db_index=True)
    mark_auto = models.ForeignKey(MarkAuto, blank=True, null=True, default=None, verbose_name='Марка')
    model_auto = models.ForeignKey(ModelAuto, blank=True, null=True, default=None, verbose_name='Модель')
    generation = models.ForeignKey(Generation, blank=True, null=True, default=None, verbose_name='Поколение')
    year_auto = IntegerRangeField('Год выпуска', null=True, blank=True)
    cost = models.PositiveIntegerField('Стоимость', blank=True, null=True, default=None)
    body_type_auto = models.ManyToManyField(BodyType, blank=True, default=None, verbose_name='Тип кузова')
    transmission_type = ArrayField(models.PositiveSmallIntegerField('Тип трансмиссии', choices=TRANSMISSION_TYPE, default=0), default=list, blank=True, verbose_name='Тип трансмиссии')
    engine_capacity = FloatRangeField('Объем двигателя', null=True, blank=True,validators=[])
    engine_type = ArrayField(models.PositiveSmallIntegerField('Тип двигателя', choices=ENGINE_TYPE, default=0), default=list, blank=True, verbose_name='Тип двигателя')
    drive_type = ArrayField(models.PositiveSmallIntegerField('Тип привода', choices=DRIVE_TYPE, default=0), default=list, blank=True, verbose_name='Тип привода')
    color_auto = ArrayField(models.PositiveSmallIntegerField('Цвет', choices=COLOR_OF_AUTO, default=0), default=list, blank=True, verbose_name='Цвет')
    salon_auto = ArrayField(models.PositiveSmallIntegerField('Салон', choices=SALON_AUTO, default=0), default=list, blank=True, verbose_name='Салон')
    color_salon = ArrayField(models.PositiveSmallIntegerField('Цвет салона', choices=COLOR_SALON_TYPE, default=0), default=list, blank=True, verbose_name='Цвет салона')
    horsepower = IntegerRangeField('Мощность', null=True, blank=True)
    mileage = models.PositiveIntegerField('Пробег', blank=True, null=True, default=None)
    equipment = models.CharField('Комплектация', max_length=2250, default='', null=True, blank=True)
    number_of_hosts = models.PositiveSmallIntegerField('Количество по ПТС', choices=NUMBER_OF_HOST_TYPE, blank=True, default=0)
    selection_period = models.CharField('Желаемый срок подбора', max_length=20, default='1 месяц', blank=True)
    comment = models.CharField('Комментарий к подбору', max_length=1024, blank=True, null=True, default=None)
    notifications = models.ManyToManyField(Notification, blank=True, default=None, verbose_name='Оповещения', related_name="service_notifications")

    field_history = CustomFieldHistoryTracker(['order', 'mark_auto', 'model_auto', 'generation', 'year_auto',
                                               'cost', 'body_type_auto', 'transmission_type', 'engine_capacity',
                                               'engine_type', 'drive_type', 'color_auto', 'salon_auto', 'mileage',
                                               'equipment', 'number_of_hosts', 'selection_period', 'comment'])

    class Meta:
        verbose_name = "Критерий поиска"
        verbose_name_plural = "Критерии поиска"
        ordering = ["created"]

    def __str__(self):
        return "%s: %s" % (self.order, self.mark_auto)
            
    def full_clean(self, exclude=None, validate_unique=False, *args, **kwargs):
        try:
            engine_capacity = self.engine_capacity
            if engine_capacity.lower == engine_capacity.upper:
                self.engine_capacity = NumericRange(engine_capacity.lower,engine_capacity.upper, bounds='[]', empty=False)
        except AttributeError:
            pass
        return self.clean(*args, **kwargs)