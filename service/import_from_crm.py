import ftplib
import json
import os

from django.conf import settings
from django.contrib.sites.models import Site

from .models import Order
from core.models import Filial


def import_from_crm():
	
	orders = Order.objects.all()
	file = os.path.join(os.getcwd(), "crm/order_crm_json.json")   	
	with open(file) as data_file:    
		for data in json.load(data_file):
			print("data") 
			print(data["fields"]["client"]["filials"]) 
	server = settings.FTP_URL
	ftp = ftplib.FTP(server)
	UID = settings.FTP_USER
	ftp.login(UID, settings.FTP_PSW)
	ftp.cwd("/mobile_app")
	host = Site.objects.get_current().domain

	#ftp.storbinary("STOR " + owf_file_txt, open(owf_file_txt, 'rb'))
	#ftp.close()

