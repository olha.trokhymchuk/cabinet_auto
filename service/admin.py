from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin, highlight_deleted
from django import forms


from .models import PodborAuto


class ProductAdminForm(forms.ModelForm):
    # def clean_transmission_type(self):
    #     transmission_type = self.cleaned_data['transmission_type']
    #     return [int(v) for v in transmission_type]

    # def clean_engine_type(self):
    #     engine_type = self.cleaned_data['engine_type']
    #     return [int(v) for v in engine_type]
    
    def clean(self):
        cleaned_data = self.cleaned_data
        array_field = ['transmission_type', 'engine_type', 'drive_type']
        for field_name in cleaned_data:
            if type(cleaned_data[field_name]) is list:
                print(cleaned_data[field_name])
                cleaned_data[field_name] = [int(v) for v in cleaned_data[field_name]] 
        return cleaned_data

class ReportPodborAdmin(SafeDeleteAdmin):
    form = ProductAdminForm
    list_display = (highlight_deleted, "id") + SafeDeleteAdmin.list_display
    list_filter = SafeDeleteAdmin.list_filter
    exclude = ('notifications',)



admin.site.register(PodborAuto, ReportPodborAdmin)
