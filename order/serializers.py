import inspect
import json
import logging
import collections
from time import strftime

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField, RangeField
from django.core.serializers.base import DeserializationError
from django.core.exceptions import FieldDoesNotExist
from django.db.models import ManyToManyField
from rest_framework import serializers
from field_history.models import FieldHistory
from rest_framework.generics import get_object_or_404

from core.serializers import UserSerializer
from core.models import User
from order.models import Order, Comment
from report.models import ReportPodbor
from report.serializers import ReportSerializer
from service.models import PodborAuto
from service.serializers import PPKSerialializer

logger = logging.getLogger(__name__)


class OrderSerializerList(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    client = UserSerializer()
    expert = UserSerializer(many=True)
    expert_check = UserSerializer(many=True)
    operator = UserSerializer(many=True)
    transferring = UserSerializer(many=True)
    podborauto_set = PPKSerialializer(many=True)
    order_type = serializers.SerializerMethodField()
    created = serializers.DateTimeField(format=settings.DATE_FORMAT, required=False)
    car_count = serializers.SerializerMethodField()
    reports_count = serializers.SerializerMethodField()
    reports_non_read_count = serializers.SerializerMethodField()
    status = serializers.CharField(source='get_status_display')
    status_closing_date = serializers.SerializerMethodField()
    status_pay = serializers.SerializerMethodField()
    order_type_code = serializers.SerializerMethodField()
    reports_is_published_count = serializers.SerializerMethodField()

    class Meta:
        model = Order
        depth = 1
        fields = '__all__'

    def get_order_type_code(self, obj):
        return obj.order_type

    def get_order_type(self, obj):
        return obj.get_order_type_display()

    def get_car_count(self, obj):
        return obj.cars.count()

    def get_reports_count(self, obj):
        return obj.reportpodbor_set.count()

    def get_reports_non_read_count(self, obj):
        return obj.reportpodbor_set.filter(is_read=False).count()

    def get_status_pay(self, obj):
        return obj.get_status_pay()

    def get_name(self, obj):
        try:
            return obj.number_buh
        except AttributeError:
            pass

    def get_status_closing_date(self, obj):
        return obj.status_closing_date.strftime('%d.%m.%Y') if obj.status_closing_date else False

    def get_reports_is_published_count(self, obj):
        return obj.reportpodbor_set.filter(published=True).count()



class OrderSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    client = UserSerializer()
    expert = UserSerializer(many=True)
    expert_check = UserSerializer(many=True)
    operator = UserSerializer(many=True)
    transferring = UserSerializer(many=True)
    podborauto_set = PPKSerialializer(many=True)
    reportpodbor_set = ReportSerializer(many=True)
    order_type = serializers.SerializerMethodField()
    created = serializers.DateTimeField(format=settings.DATE_FORMAT, required=False)
    car_count = serializers.SerializerMethodField()
    reports_count = serializers.SerializerMethodField()
    reports_non_read_count = serializers.SerializerMethodField()
    status = serializers.CharField(source='get_status_display')
    status_closing_date = serializers.SerializerMethodField()
    status_pay = serializers.SerializerMethodField()
    order_type_code = serializers.SerializerMethodField()
    reports_is_published_count = serializers.SerializerMethodField()

    class Meta:
        model = Order
        depth = 1
        fields = '__all__'
    def get_order_type_code(self, obj):
        return obj.order_type
        
    def get_order_type(self, obj):
        return obj.get_order_type_display()

    def get_car_count(self, obj):
        return obj.cars.count()

    def get_reports_count(self, obj):
        return obj.reportpodbor_set.count()
    
    def get_reports_non_read_count(self, obj):
        return obj.reportpodbor_set.filter(is_read=False).count()

    def get_status_pay(self, obj):
        return obj.get_status_pay()

    def get_name(self, obj):
        try:
            return obj.number_buh
        except AttributeError:
            pass

    def get_status_closing_date(self, obj):
        return obj.status_closing_date.strftime('%d.%m.%Y') if obj.status_closing_date else False

    def get_order_log(self, obj):
        queryset = self.get_logs(obj.id).order_by('-date_created')

        return

    def prepare_history(self, queryset, model_fields):
        queryset = queryset.filter(field_name__in=model_fields)
        return queryset

    def get_reports_is_published_count(self, obj):
        return obj.reportpodbor_set.filter(published=True).count()

    def get_logs(self, order_id):
        order = get_object_or_404(Order, id=order_id)
        related_reports = list(order.reportpodbor_set.values_list('id', flat=True))
        related_podbors = list(order.podborauto_set.values_list('id', flat=True))

        order_content_type = ContentType.objects.get_for_model(Order)
        report_content_type = ContentType.objects.get_for_model(ReportPodbor)
        podbor_content_type = ContentType.objects.get_for_model(PodborAuto)

        order_queryset = FieldHistory.objects.filter(object_id=order.id, content_type=order_content_type)
        report_queryset = FieldHistory.objects.filter(object_id__in=related_reports, content_type=report_content_type)
        podbor_queryset = FieldHistory.objects.filter(object_id__in=related_podbors, content_type=podbor_content_type)

        order_fields = [f.name for f in Order._meta.get_fields()]
        report_fields = [f.name for f in ReportPodbor._meta.get_fields()]
        podbor_fields = [f.name for f in PodborAuto._meta.get_fields()]

        queryset = self.prepare_history(order_queryset, order_fields) | \
                   self.prepare_history(report_queryset, report_fields) | \
                   self.prepare_history(podbor_queryset, podbor_fields)

        return queryset



class OrderFieldHistorySerializer(serializers.Serializer):
    user = UserSerializer()
    date_created = serializers.DateTimeField(format=settings.DATETIME_FORMAT, required=False)
    log_event = serializers.SerializerMethodField()

    class Meta:
        model = FieldHistory
        fields = ('user', 'date_created', 'log_event')

    def _get_choice_representation(self, obj, field_name):
        display_method_name = 'get_{}_display'.format(field_name)
        display_method = getattr(obj, display_method_name)
        return display_method()

    def _get_related_representation(self, obj, field_name):
        related_object = getattr(obj, field_name)
        field_value = related_object.__str__()
        return field_value

    def _get_many2many_representation(self, values):
        result = ""
        for value in values:
            result = '{}, {}'.format(result, value.__str__())
        return result

    def _get_arrayfield_representation(self, field, field_values):
        human_readable_values = []
        if field.base_field.choices:
            if not isinstance(field_values, collections.Iterable) or isinstance(field_values, str):
                field_values = [field_values]

            for field_value in field_values:
                for key, choice_value in field.base_field.choices:
                    if field_value == key:
                        human_readable_values.append(choice_value)
                        break
        else:
            human_readable_values = field_values

        human_readable_values = [str(v) for v in human_readable_values]
        representation = ", ".join(human_readable_values)
        return representation

    def _get_rangefield_represenatation(self, value):
        result = ""
        if value:
            if value.lower:
                result = value.lower
            if value.upper:
                result = "{} - {}".format(result, value.upper)

        return result

    def get_log_event(self, history):
        if not history.object:
            return ''
        event = ''

        try:
            field = history.object._meta.get_field(history.field_name)
            field_name = field.verbose_name

            if field.choices:
                field_value = self._get_choice_representation(history.object, history.field_name)
            elif isinstance(field, ManyToManyField):
                field_value = ""
                users = ['operator', 'transferring', 'expert_check', 'expert']
                current_field = json.loads(history.serialized_data)[0]["fields"]
                if history.field_name in users:
                    values = current_field[history.field_name]
                    user = User.objects.filter(pk__in=values)
                    field_value = self._get_many2many_representation(user)
            elif field.related_model:
                field_value = self._get_related_representation(history.object, history.field_name)
            elif isinstance(field, ArrayField):
                field_value = self._get_arrayfield_representation(field, history.field_value)
            elif isinstance(field, RangeField):
                field_value = self._get_rangefield_represenatation(history.field_value)
            elif isinstance(history.field_value, bool):
                field_value = 'да' if history.field_value else 'нет'
            else:
                field_value = history.field_value
            if history.field_name == 'id':
                event = 'добавил "{} {}"'.format(history.object._meta.verbose_name, history.object)
            elif isinstance(history.object, PodborAuto):
                event = 'изменил "{} {} {}" поле "{}" на "{}"'.format(history.object._meta.verbose_name,
                                                                      history.object.mark_auto,
                                                                      history.object.model_auto,
                                                                      field_name,
                                                                      field_value)
            elif not isinstance(history.object, Order):
                event = 'изменил "{} {}" поле "{}" на "{}"'.format(history.object._meta.verbose_name,
                                                                   history.object,
                                                                   field_name,
                                                                   field_value)
            else:
                event = 'изменил поле "{}" на "{}"'.format(field_name, field_value)
        except (DeserializationError, FieldDoesNotExist) as ex:
            logger.warning(ex)

        return event


class CommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comment
        fields = '__all__'


class SetOrderExpertSerializer(serializers.ModelSerializer):
    """docstring for SetOrderExpertSerializer"""
    expert = serializers.CharField()
    class Meta:
        model = Order
        fields = ('expert', )
        
