from django.template import Library
from django.conf import settings

from django.contrib.auth.models import Group

register = Library()

@register.simple_tag
def template_dir(this_object, its_name=""):
    if settings.DEBUG:
        output = dir(this_object)
        return "<pre>" + str(its_name) + " " + str(output) + "</pre>"
    return ""

@register.filter(name='has_group') 
def has_group(user, group_name):
    group =  Group.objects.get(name=group_name) 
    return group in user.groups.all() 