import re
from django import template
from django.template import TemplateSyntaxError, Node
from django.template.defaulttags import TemplateIfParser, IfNode

from django.contrib.humanize.templatetags.humanize import naturalday, intcomma


register = template.Library()


@register.filter
def white_space_humanize(value):
    return intcomma(re.sub(r",", "\s+", str(value)))