from django.template import Library
from django.conf import settings

from django.contrib.auth.models import Group
from django.utils.safestring import mark_safe

from order.models import Order

register = Library()

@register.simple_tag
def view_new_order_menu(clent_id):
    if Order.objects.filter( client = clent_id, status_closing = False).count():
        return ''
    else:
        return mark_safe('<div class="bar-nav__item child"><div class="bar-nav__item_holder"><div class="bar-nav__item_title"><a href="/new_order/">Новый заказ</a></div></div></div>')
