from django.conf.urls import url
from order.api.views import (ApiOrderSelectionList, ApiOrderOutsideDiagnose, ApiOrderExpertday, ApiOrderOther,
                             ApiSetOrderExpert, ApiOrderFililList, ApiOrderTransferringOrd, send_order_to_recheck,
                             get_order_documents, change_order_criteria)

from order.views import get_order_done_popup_info

app_name = 'lk'

urlpatterns = [
    url(r'^api/selection/(?P<pk>\d+)/$',
        ApiOrderSelectionList.as_view(), name='selection_api'),
    url(r'^api/order_filial/(?P<pk>\d+)/$',
        ApiOrderFililList.as_view(), name='selection_api'),
    url(r'^api/outside_diagnose/(?P<pk>\d+)/$',
        ApiOrderOutsideDiagnose.as_view(), name='outside_diagnose_api'),
    url(r'^api/expert_day/(?P<pk>\d+)/$',
        ApiOrderExpertday.as_view(), name='expert_day_api'),
    url(r'^api/other/(?P<pk>\d+)/$', ApiOrderOther.as_view(), name='other_api'),
    url(r'^api/transferringord/(?P<pk>\d+)/$',
        ApiOrderTransferringOrd.as_view(), name='transferringord_api'),
    url(r'^api/set_expert/(?P<pk>\d+)/$',
        ApiSetOrderExpert.as_view(), name='set_expert'),
    url(r'^api/get_order_done_popup_info/$',
        get_order_done_popup_info, name='get_order_done_popup_info'),
    url(r'^api/send_order_to_recheck/$',
        send_order_to_recheck, name='send_order_to_recheck'),
    url(r'^api/change_order_criteria/$', change_order_criteria, name='change_order_criteria'),
    url(r'^api/order_documents/$',
        get_order_documents, name='get_order_documents'),
]
