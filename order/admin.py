from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin, highlight_deleted

from .models import Order, Comment


class OrderAdmin(SafeDeleteAdmin):
    list_display = (highlight_deleted, "id") + SafeDeleteAdmin.list_display
    list_filter = SafeDeleteAdmin.list_filter
    exclude = ('notifications',)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        pk = request.resolver_match.args[0]
        order = Order.objects.get(pk=pk)
        if db_field.name == "cars":
            cars = order.cars.all()
            kwargs["queryset"] = cars
        if db_field.name == "statuses":
            statuses = order.statuses.all()
            kwargs["queryset"] = statuses
        if db_field.name == "operator":
            operator = order.operator.all()
            kwargs["queryset"] = operator
        if db_field.name == "expert":
            expert = order.expert.all()
            kwargs["queryset"] = expert
        if db_field.name == "expert_check":
            expert_check = order.expert_check.all()
            kwargs["queryset"] = expert_check
        if db_field.name == "transferring":
            transferring = order.transferring.all()
            kwargs["queryset"] = transferring
        return super(OrderAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


class CommentAdmin(admin.ModelAdmin):
    list_display = ('order', 'author', 'body')


admin.site.register(Order, OrderAdmin)
admin.site.register(Comment, CommentAdmin)
