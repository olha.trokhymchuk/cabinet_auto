import requests
import logging
import json

from django.conf import settings

import order.order_constants as order_const
from order.models import Order 
from order.helper import sendOrderEdit

from lk.models import Statuses

# logger = logging.getLogger(__name__)


# def send_status_crm(*arg, **kwargs):
# 	logger.error('send_status_to_crm')
# 	logger.error(arg[0][0])
# 	logger.error(arg)
# 	order =  Order.objects.get(pk=arg[0][0])
# 	send_status(order)
	
def send_statuses():
	status = 'change-status-crm'
	statuses = Statuses.objects.get_or_create(name=status)
	orders = Order.objects.filter(statuses__in=statuses)
	url = "{}/handlers/crm/input".format(settings.URLCRM)
	for order in orders:
		send_status(order, status, url)

	# TODO refused services
	# expert changed
	# status = 'crm-ready-recieve'
	# statuses = Statuses.objects.get_or_create(name=status)
	# orders = Order.objects.filter(statuses__in=statuses)
	# url = "{}/handlers/crm/input".format(settings.URLSCRINSUR)
	# for order in orders:
	# 	send_status(order, status, url)

	# order with recomend report
	# status = 'crm-recomend-report'
	# statuses = Statuses.objects.get_or_create(name=status)
	# orders = Order.objects.filter(statuses__in=statuses)
	# url = "{}/handlers/crm/input".format(settings.URLSCRINSUR)
	# for order in orders:
	# 	send_status(order, status, url)

	# status changed 
	status = 'change-stat-byexpert'
	statuses = Statuses.objects.get_or_create(name=status)
	orders = Order.objects.filter(statuses__in=statuses)
	url = "{}/handlers/crm/input".format(settings.URLCRM)
	for order in orders:
		if send_status(order, status, url):
			sendOrderEdit(order.number_buh)

def send_status(order, status, url):
	# logger.error("order.status")
	# logger.error(order.status)
	refund_amount = order.refund_amount
	compensation = order.expert_compensation

	if refund_amount == None:
		refund_amount = 0
	if compensation == None:
		compensation = 0

	orderData = {
		"id": int(order.number_buh),
		"status": "{}".format(order_const.CRM_TO_STATUS[order.status]),
		"auto_iz_salona": order.auto_from_salon,
		"refund_amount": refund_amount,
		"compensation": compensation
	}

	try:
		patronymic = ""
		if order.expert.all()[0].patronymic is not None:
			patronymic = order.expert.all()[0].patronymic
		# logger.error(order.expert.all()[0].phone)
		orderData.update({"expertname": "{} {} {}".format(order.expert.all()[0].last_name, order.expert.all()[0].first_name, patronymic),
			"expert_num": "{}".format(order.expert.all()[0].phone)
		})
	except IndexError as ex:
		# logger.error("expertname")
		# logger.error(ex)
		pass

	try:
		patronymic = ""
		if order.expert_check.all()[0].patronymic is not None:
			patronymic = order.expert_check.all()[0].patronymic
		orderData.update({"exper2tname": "{} {} {}".format(order.expert_check.all()[0].last_name, order.expert_check.all()[0].first_name, patronymic),
							"expert2_num": "{}".format(order.expert_check.all()[0].phone)
						})
	except IndexError as ex:
		# logger.error("exper2tname")
		# logger.error(ex)
		pass

	try:
		patronymic = ""
		if order.transferring.all()[0].patronymic is not None:
			patronymic = order.transferring.all()[0].patronymic
		orderData.update({"exper3tname": "{} {} {}".format(order.transferring.all()[0].last_name, order.transferring.all()[0].first_name, patronymic),
							"expert3_num": "{}".format(order.transferring.all()[0].phone)
						})
	except IndexError as ex:
		pass
		# logger.error("exper3tname")
		# logger.error(ex)
		# logger.error(url)
	# logger.error(orderData)
	r = requests.post(url, data={'key': 'XgqxftE}2w$HdSQOme1WlEo{', 'orderData': json.dumps(orderData)})
	# logger.error('---------------------------------------------------------------------')
	# logger.error('order pk')
	# logger.error(order.pk)
	# logger.error('r.status_code')
	# logger.error(r.status_code)
	# logger.error('r.reason')
	# logger.error(r.reason)
	# logger.error('r.text')
	# logger.error(r.text)
	statuses, create = Statuses.objects.get_or_create(name=status)
	if r.status_code == 200 and r.text == "true":
		# logger.error('remove from statuses')
		order.statuses.remove(statuses)
		return True
	else:
		order.statuses.add(statuses)
		# logger.error('add from statuses')
	# logger.error('---------------------------------------------------------------------')
	return False
