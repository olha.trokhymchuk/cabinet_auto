from core.models import User
from order.models import Order
from order.workers.auto_matching.auto_matching import autoMatching

from service.models import PodborAuto

from report.models import ReportPodbor

from auto.models import Auto, Year, BodyType

import auto.glossary as auto_glossary

from psycopg2.extras import NumericRange

from django.contrib.auth import authenticate


class testAutoMatching:
	def __init__(self, orders=None):
		self.user = authenticate(username='79610577499', password='ndld100719960nd4l')
		Order.user = self.user
		ReportPodbor.user = self.user
		PodborAuto.user = self.user
		BodyType.user = self.user
		self.testByAuto()
		self.testByPpk()

	def testByPpk(self):
		autos = []
		reports = []
		orders = Order.objects.exclude(podborauto=None)[:100]
		#orders = Order.objects.exclude(podborauto=None).filter(pk=16710)
		for order in orders:
			print(order)
			for podpor in order.podborauto_set.all():
				vin = "test" + str(podpor.pk) + str(order.pk)
				auto_data = {
					"vin": vin,
					"mark_auto": podpor.mark_auto,
					"model_auto": podpor.model_auto,
					"generation": podpor.generation,
					"cost": podpor.cost, 
					"mileage": podpor.mileage, 
					"equipment": podpor.equipment, 
					"owners": podpor.number_of_hosts,
				}
				try:
					year_auto = Year.objects.get(year=podpor.year_auto.lower)
					auto_data.update({"year_auto": year_auto})
				except (Year.DoesNotExist, AttributeError):
					pass
				if podpor.engine_type:
					try:
						auto_data.update({"engine_type": podpor.engine_type[0]})
					except IndexError:
						pass
				if podpor.body_type_auto:
					auto_data.update({"body_type_auto": podpor.body_type_auto.all().first()})

				if podpor.drive_type:
					auto_data.update({"drive_type": podpor.drive_type[0]})
				if podpor.transmission_type:
					try:
						auto_data.update({"transmission_type": podpor.transmission_type[0]})
					except IndexError:
						pass
				color_salon = self.get_color_salon(podpor)
				if color_salon:
					auto_data.update({"color_salon": color_salon})
				color_auto = self.get_color_auto(podpor)
				if color_auto:
					# print(color_auto)
					auto_data.update({"color_auto": color_auto})
				color_salon = self.get_salon_auto(podpor)
				if color_salon:
					auto_data.update({"salon_auto": color_salon})
				try:
					auto_data.update({"engine_capacity": podpor.engine_capacity.lower.to_eng_string()})
				except (IndexError, AttributeError):
					pass
				try:
					auto_data.update({"horsepower": podpor.horsepower.upper})
				except AttributeError:
					pass
				try:
					auto_data.update({"horsepower": podpor.horsepower.lower})
				except AttributeError:
					pass
				try:
					auto = Auto.objects.get(vin=vin)
					report = ReportPodbor.objects.filter(auto=auto)
					auto.delete()
					report.delete()
				except Auto.DoesNotExist:
					pass
				auto, created = Auto.objects.get_or_create(vin=vin, defaults=auto_data)
				print("auto")
				print(auto)
				print("auto")
				report = ReportPodbor.objects.create(auto=auto)
				print("report")
				print(report)
				print("report")
				assert auto.pk
				autos.append(auto.pk)
				reports.append(report.pk)
		autoMatching(orders)
		# Auto.objects.filter(pk__in=autos).delete()
		# ReportPodbor.objects.filter(pk__in=reports).delete()

	def testByAuto(self):
		autos = []
		reports = []
		autos = Auto.objects.all()[:100]
		orders = Order.objects.exclude(podborauto=None)[:100]
		#orders = Order.objects.exclude(podborauto=None).filter(pk=17036)

		i = 0
		for order in orders:
			print(order)
			# print(auto.pk)
			auto  = autos[i]
			i += 1
			ppk_data = {
				"order": order,
				"mark_auto": auto.mark_auto,
				"model_auto": auto.model_auto,
				"cost": auto.cost, 
				"mileage": auto.mileage,
			}
			if auto.equipment:
				ppk_data.update({"equipment": auto.equipment})
			if auto.owners:
				ppk_data.update({"number_of_hosts": auto.owners})
			if auto.generation:
				ppk_data.update({"generation": auto.generation})

			if auto.horsepower:
				horsepower = "[{}, {})".format(auto.horsepower, auto.horsepower)
				ppk_data.update({"horsepower": horsepower})
			if auto.year_auto:
				year_auto = "[{}, {})".format(auto.year_auto, auto.year_auto)
				ppk_data.update({"year_auto": year_auto})

			if auto.engine_capacity:
				engine_capacity = "[{},{})".format(auto.engine_capacity, auto.engine_capacity)
				ppk_data.update({"engine_capacity": engine_capacity})

			if auto.transmission_type:
				ppk_data.update({"transmission_type": str(set([auto.transmission_type]))})

			if auto.engine_type:
				ppk_data.update({"engine_type": str(set([auto.engine_type]))})

			if auto.drive_type: 
				ppk_data.update({"drive_type": str(set([auto.drive_type]))})

			if auto.color_auto:
				for color_of_auto in auto_glossary.COLOR_OF_AUTO:
					if color_of_auto[1].lower() == auto.color_auto:
						ppk_data.update({"color_auto": str(set([color_of_auto[0]]))})
						break
			if auto.salon_auto: 
				for salon_of_auto in auto_glossary.SALON_AUTO:
					if salon_of_auto[1].lower() == auto.salon_auto:
						ppk_data.update({"salon_auto": str(set([salon_of_auto[0]]))})
						break
			if auto.color_salon: 
				for color_of_salon in auto_glossary.COLOR_SALON_TYPE:
					if color_of_salon[1].lower() == auto.color_salon:
						ppk_data.update({"color_salon": str(set([color_of_salon[0]]))})
						break
			print(ppk_data)
			ppk = PodborAuto.objects.create(**ppk_data)
			if auto.body_type_auto and not ppk.body_type_auto.filter(pk=auto.body_type_auto.pk):
				ppk.body_type_auto.add(auto.body_type_auto)
			print(self.user)
			report = ReportPodbor.objects.filter(auto=auto)
			report.delete()
			report = ReportPodbor.objects.create(auto=auto)

			if not report.order.filter(pk=order.pk):
				report.order.add(order)
		autoMatching(orders)

	def get_color_salon(self, ppk):
		color_salon = []
		if ppk.color_salon:
			for color in ppk.color_salon:
				try:
					color_salon.append(auto_glossary.COLOR_SALON_TYPE[color][1].lower())
				except IndexError as ex:
					pass
					#lower.error(ex)
			if color_salon:
				return ', '.join(str(x) for x in color_salon)
		return False

	def get_salon_auto(self, ppk):
		salon_auto = []
		if ppk.salon_auto:
			for color in ppk.salon_auto:
				try:
					salon_auto.append(auto_glossary.SALON_AUTO[color][1].lower())
				except IndexError as ex:
					pass
			if salon_auto:
				return ', '.join(str(x) for x in salon_auto)
		return False

	def get_color_auto(self, ppk):
		colors_auto = []
		if ppk.color_auto and not auto_glossary.COLOR_OF_AUTO_ANY in ppk.color_auto:
			for color in ppk.color_auto:
				return auto_glossary.COLOR_OF_AUTO[color][1].lower()
				try:
					colors_auto.append(auto_glossary.COLOR_OF_AUTO[color][1].lower())
				except IndexError as ex:
					pass
					#lower.error(ex)

			if colors_auto:
				return ', '.join(str(x) for x in colors_auto)
		return False				


