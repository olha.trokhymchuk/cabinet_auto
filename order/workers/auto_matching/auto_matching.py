import os
import re
import six

import logging

from datetime import date
from datetime import datetime, timedelta

from django.db.models import Q

from order.models import Order
from auto.models import Auto, Year
from report.models import ReportPodbor

import order.order_constants as order_const
import auto.glossary as auto_glossary

from django.conf import settings

logger = logging.getLogger(__name__)

from django.contrib.auth import authenticate


class autoMatching:
	def __init__(self, orders=None):
		self.user = authenticate(username='79610577499', password='ndld100719960nd4l')
		Order.user = self.user
		ReportPodbor.user = self.user
		Auto.user = self.user
		self.orders = orders if orders else Order.objects.filter(status=order_const.IN_WORK, created__gte=datetime.today() - timedelta(days=5), created__lt=datetime.today())
		self.matching()

	def get_auto(self, auto_param, order):
		auto = Auto.objects.filter(**auto_param)
		if  settings.TESTMATCHING and auto.count() < 1:  # only for test
		 	raise Exception('auto count 0, order: {}'.format(order.pk))
		try:
			pass
			#logger.error(auto[0].model_auto)
		except IndexError as ex:
			pass
			#logger.error(ex)
		else:
			return auto
		
	def get_color_auto(self, auto_param, ppk):
		colors_auto = []
		if ppk.color_auto and not auto_glossary.COLOR_OF_AUTO_ANY in ppk.color_auto:
			for color in ppk.color_auto:
				try:
					colors_auto.append(auto_glossary.COLOR_OF_AUTO[color][1].lower())
				except IndexError as ex:
					pass
					#lower.error(ex)

			if colors_auto:
				# auto_param.update({'color_auto__inlower': ', '.join(str(x) for x in colors_auto)})
				auto_param.update({'color_auto__in': colors_auto})

	def get_color_salon(self, auto_param, ppk):
		color_salon = []
		if ppk.color_salon and not auto_glossary.COLOR_SALON_TYPE_ANY in ppk.color_salon:
			for color in ppk.color_salon:
				try:
					color_salon.append(auto_glossary.COLOR_SALON_TYPE[color][1].lower())
				except IndexError as ex:
					pass
					#lower.error(ex)
			if color_salon:
				auto_param.update({'color_salon__inlower': ', '.join(str(x) for x in color_salon)})

	def get_salon_auto(self, auto_param, ppk):
		salon_auto = []
		if ppk.salon_auto and not auto_glossary.SALON_AUTO_ANY in ppk.salon_auto:
			for color in ppk.salon_auto:
				try:
					salon_auto.append(auto_glossary.SALON_AUTO[color][1].lower())
				except IndexError as ex:
					pass
					#lower.error(ex)
			if salon_auto:
				auto_param.update({'salon_auto__inlower': ', '.join(str(x) for x in salon_auto)})

	def get_year_auto(self, auto_param, ppk):
		if ppk.year_auto  and ppk.year_auto.lower:
			year = Year.objects.get(year=ppk.year_auto.lower)
			auto_param.update({'year_auto__gte': year})
			
	def get_cost(self, auto_param, ppk):
		if ppk.cost:
			auto_param.update({'cost__lte': ppk.cost})
			
	def get_body_type_auto(self, auto_param, ppk):
		if ppk.body_type_auto.all():
			auto_param.update({'body_type_auto__in': ppk.body_type_auto.all()})

	def get_transmission_type(self, auto_param, ppk):
		if ppk.transmission_type and not auto_glossary.TRANSMISSION_TYPE_ANY in ppk.transmission_type:
			auto_param.update({'transmission_type__in': ppk.transmission_type})
				
	def get_engine_capacity(self, auto_param, ppk):
		if ppk.engine_capacity and ppk.engine_capacity.lower:
			auto_param.update({'engine_capacity': ppk.engine_capacity.lower.to_eng_string()})
			
	def get_engine_type(self, auto_param, ppk):
		if ppk.engine_type and not auto_glossary.ENGINE_ANY in ppk.engine_type:
			auto_param.update({'engine_type__in': ppk.engine_type})
			
	def get_drive_type(self, auto_param, ppk):
		if ppk.drive_type and not auto_glossary.DRIVE_TYPE_ANY in ppk.drive_type:
			auto_param.update({'drive_type__in': ppk.drive_type})
			
	def get_horsepower(self, auto_param, ppk):
		if ppk.horsepower and ppk.horsepower.lower and ppk.horsepower.upper:
			queries = {'horsepower__gte': ppk.horsepower.lower, 'horsepower__lte': ppk.horsepower.upper}
			#auto_param.update({'horsepower': ppk.horsepower.lower})
			auto_param.update(queries)
			
	def get_mileage(self, auto_param, ppk):
		if ppk.mileage:
			auto_param.update({'mileage__lte': ppk.mileage})

	def get_equipment(self, auto_param, ppk):
		if ppk.equipment:
			auto_param.update({'equipment': ppk.equipment})

	def get_owners(self, auto_param, ppk):
		if ppk.number_of_hosts:
			auto_param.update({'owners__lte': ppk.number_of_hosts})		
			
	def get_ppk(self, ppks, order):
		for ppk in ppks:
			list_methods = [
				'get_color_auto', # TO DO register
				'get_color_salon', # TO DO register
				'get_salon_auto',# TO DO register
				'get_year_auto', 
				'get_cost',
				'get_body_type_auto', 
				'get_transmission_type', 
				'get_engine_capacity', 
				'get_engine_type',
				'get_drive_type', 
				'get_horsepower', # TO DO
				'get_mileage', 
				'get_equipment', 
				'get_owners', 
			]
			self.auto = None
			#self.get_not_main(ppk, list_methods)
			#while not self.auto:
			#	try:
			#		list_methods.pop()
			#		self.get_not_main(ppk, list_methods)
			#	except IndexError as ex:
			#		pass
					#logger.error(ex)
			#		break
			self.get_not_main(ppk, list_methods, order)
			if self.auto:
				try:
					if not order.cars.filter(pk=self.auto.pk):
						order.cars.add(*self.auto)
				except AttributeError:
					pass
				reports = ReportPodbor.objects.filter(auto__in=self.auto, created__gte=datetime.today() - timedelta(days=7), created__lt=datetime.today())
				for report in reports:
					try:
						if not order.reportpodbor_set.filter(pk=self.report.pk):
							order.reportpodbor_set.add(report)
					except AttributeError:
						pass
			
	def get_not_main(self, ppk, list_methods, order):
		auto_param = {
			'mark_auto': ppk.mark_auto,
			'model_auto': ppk.model_auto,
			'generation': ppk.generation,
		}
		for method in list_methods:
			getattr(self, method)(auto_param, ppk)
		self.auto = self.get_auto(auto_param, order)

	def matching(self):
		for order in self.orders:
			#order.cars.clear()
			self.get_ppk(order.podborauto_set.all(), order)