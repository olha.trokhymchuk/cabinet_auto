import os
import ftplib
import json
import logging

from django.conf import settings

import order.order_constants as order_const

from order.models import Order

logger = logging.getLogger(__name__)


def load_orders_on_mobile():
    filename = "orders.json"
    server = settings.FTP_URL
    ftp = ftplib.FTP(server)
    UID = settings.FTP_USER
    ftp.login(UID, settings.FTP_PSW)
    ftp.cwd("/mobile_app")
    orders = Order.objects.filter(status_closing=False, status=order_const.IN_WORK)
    logger.error(orders.count())
    orders_json = []
    for order in orders:
        mark = ''
        model = ''
        generation = ''
        if order.cars.all().count() > 0:
            car = order.cars.all()[0]
            try:
                generation = car.generation.name
            except AttributeError as ex:
                logger.error(ex)
            try:
                mark = car.mark_auto.name
            except AttributeError as ex:
                logger.error(ex)
            try:
                model = car.model_auto.name
            except AttributeError as ex:
                logger.error(ex)
        for expert in order.expert.all():
            orders_json.append({expert.pk: 
                {
                'id': order.pk, 
                'number_buh': order.number_buh, 
                'марка':mark,
                'модель':model,
                'поколение':generation,
                }
            })

    orders_json = json.dumps(orders_json)
    text_file = open(filename, "w")
    text_file.write(orders_json)
    text_file.close()
    ftp.storbinary("STOR " + filename, open(filename, 'rb'))
    ftp.close()