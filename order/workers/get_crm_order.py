import os, sys, re, json
from .models import Order

from django.contrib.auth.models import Group
from core.models import User, Filial

from notifications.utils import send_notification
from notifications.models import Notification

from django.contrib.auth.base_user import BaseUserManager

def get_crm_order_from_log():
	file = open(os.path.join(sys.path[0], "order/crm_log.txt"), "r")
	lines = file.readlines()
	count = 0
	for line in lines:
		print("order start")
		#if count > 20:
		#	return
		print("count = {}".format(count))
		orderData = re.split(r'orderData=', line)
		try:
			orderData = json.loads(r'''{}'''.format(orderData[-1]))
			filial = orderData['customFields']['regions']
			filial = get_filial(filial)
			pho = orderData['customer']['phones']
			phone = check_phone(pho)
			lastName = orderData['customer']['lastName']
			patronymic = orderData['customer']['patronymic']
			firstName = orderData['customer']['firstName']
			email = orderData['customer']['email']
			group_client = Group.objects.get(name='Клиент')
			client_data = 	{'first_name': firstName, 
	            'last_name': lastName,
	            'patronymic': patronymic,
	            'email': email,  
	            'phone': phone}
			phone = check_phone(pho)
			order_buh = orderData['id']
			

			orders = Order.objects.filter(number_buh=order_buh)
			print(phone)
			print(order_buh)
			client, created = User.objects.get_or_create(phone=phone)
			print("password")
			#if client.password:
			#	continue
			print(client.password)
			#print(1111111111)
			#notification_client(client)
			client.first_name = firstName
			client.last_name = lastName
			client.email = email
			client.patronymic = patronymic
			try:
				client.groups.add(group_client)
			except Exception:
				pass
			try:
				client.filials.add(filial)
			except Exception:
				pass
			client.save()
			print(created)
			for order in orders:
				print(order.client)
				order.client_id = client.pk
				order.save()
				print(order)
		except Exception as ex:
			print(ex)
			pass
		else:
			count += 1
		print("order end")

def check_phone(phones):
        default_phone = ''
        for x in range(len(phones)):
            phone = re.sub(r' ', '', phones[x]['number'])
            phone = ''.join(re.findall(r'\d+', phone))
            if x == 0:
                default_phone = phone
            if len(str(phone)) == 10 or len(str(phone)) == 11:
                return phone
        return default_phone



def get_filial(filial):
		regions = {
			'msk':'Москва',
			'tul':'Тула',
			'spb':'Санкт-Петербург',
			'nsk':'Новосибирск',
			'kzn':'Казань',
			'rnd':'Ростов-на-Дону',
			'smr':'Самара',
			'ekb':'Екатеринбург',
			'vla':'Владивосток',
			'vrn':'Воронеж',
			'kra':'Краснодар',
			'kry':'Крым',
			'nno':'Нижний Новгород',
			'ufa':'Уфа'
		}

		try:
			return Filial.objects.get(name=regions[filial])
		except (KeyError, Filial.DoesNotExist) as ex:
			logger.error(ex)
			return False

def notification_client(user):
		print(1111111111111111111111)
		password = create_password()
		while not password or password == None:
			password = create_password()

		print(33333333333333333333)
		data = {
			'name': user.get_full_name(),
			'username': user.phone,
			'password': password
		}
		print(data)
		user.set_password(password)
		user.save()
		channels = ['email', 'lk', 'sms']
		send_notification([user], Notification.REGISTRATION, data, channels=channels)


def create_password():
		password = BaseUserManager().make_random_password(8)
		string = False
		number = False
		for char in password: 
			if not char.isdigit():
				string = True  
			if char.isdigit():
				number = True
		if string and number:
			return password