import ftplib

from django.conf import settings
from django.contrib.sites.models import Site

from .models import Order
from core.models import Filial


def set_filial_by_pref():
	prefix = {
		'МС':'Москва',
		'77':'Москва',
		'СП':'Санкт-Петербург',
		'НС':'Новосибирск',
		'РТ':'Казань',
		'РД':'Ростов-на-Дону',
		'СМ':'Самара',
		'ЕК':'Екатеринбург',
		'ВВ':'Владивосток',
		'ВР':'Воронеж',
		'КР':'Краснодар',
		'КМ':'Крым',
		'НН':'Нижний Новгород',
		'УФ':'Уфа'
	}
	orders = Order.objects.all()

	ordert_withou_filial = "order_without_filial.txt"
	order_wf = ""
	server = settings.FTP_URL
	ftp = ftplib.FTP(server)
	UID = settings.FTP_USER
	ftp.login(UID, settings.FTP_PSW)
	ftp.cwd("/mobile_app")
	owf_file_txt = open(ordert_withou_filial, "w")
	host = Site.objects.get_current().domain
	for order in orders:
		pref =order.number_buh.split("-")
		print(pref)
		print(pref[0])
		filial = None
		try:
			filial = Filial.objects.get(name=prefix[pref[0].upper()])
		except KeyError:
			pass
		else:
			order.filial = filial
			order.save()
		print(filial)
		if not filial:
			order_wf += "{} https://{}/order/detail/{}/ \n".format(order.number_buh, host, order.pk)
	print(order_wf)
	owf_file_txt.write(order_wf)
	owf_file_txt.close()
	#ftp.storbinary("STOR " + owf_file_txt, open(owf_file_txt, 'rb'))
	#ftp.close()

def get_empty_filial():
	order_wf = ""
	host = Site.objects.get_current().domain
	orders = Order.objects.filter(filial=None)
	for order in orders:
		order_wf += "{} https://{}/order/detail/{}/ \n".format(order.number_buh, host, order.pk)
	print(order_wf)

