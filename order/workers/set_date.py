import os
import logging

from datetime import datetime, timedelta, time


from django.conf import settings

import order.order_constants as order_const

from order.models import Order

from field_history.models import FieldHistory

logger = logging.getLogger(__name__)


def set_date():
	today = datetime.now().date()
	print(today)
	orders = Order.objects.filter(status_closing=True, modified__gte=today, status=order_const.DONE) 
	orders_json = []
	for order in orders:
		history_status = FieldHistory.objects.filter(object_id=order.pk, field_name="status").last()
		print(history_status.date_created)
		order.date_end = history_status.date_created
		print(order.date_end)
		order.save()

