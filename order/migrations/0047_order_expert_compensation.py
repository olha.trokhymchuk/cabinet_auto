# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2021-04-22 07:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0046_order_review'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='expert_compensation',
            field=models.PositiveIntegerField(blank=True, default=None, null=True, verbose_name='Компенсация эксперту'),
        ),
    ]
