# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2018-09-06 09:30
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0020_order_filial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='status',
            field=models.PositiveSmallIntegerField(blank=True, choices=[(0, 'новый'), (1, 'в очереди'), (2, 'в работе'), (3, 'отложен'), (4, 'выполнен'), (5, 'готов к выдаче')], default=0, verbose_name='Статус'),
        ),
    ]
