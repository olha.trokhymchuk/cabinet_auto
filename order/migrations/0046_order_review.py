# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2021-01-14 08:24
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0028_filial_email_for_complaints'),
        ('order', '0045_auto_20201027_1134'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='review',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='order_review', to='core.Reviews', verbose_name='Отзыв'),
        ),
    ]
