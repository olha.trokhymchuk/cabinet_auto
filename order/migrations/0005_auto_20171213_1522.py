# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-12-13 12:22
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0004_auto_20171212_1247'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='cost',
            field=models.DecimalField(blank=True, decimal_places=2, default=None, max_digits=10, null=True, verbose_name='Стоимость'),
        ),
        migrations.AddField(
            model_name='order',
            name='paid',
            field=models.DecimalField(blank=True, decimal_places=2, default=None, max_digits=10, null=True, verbose_name='Оплачено'),
        ),
    ]
