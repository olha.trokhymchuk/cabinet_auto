# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-04-19 08:33
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0030_auto_20190304_1311'),
    ]

    operations = [
        migrations.AddField(
            model_name='order',
            name='auto_from_salon',
            field=models.BooleanField(default=False, verbose_name='Авто из салона'),
        ),
    ]
