# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-02-07 10:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('order', '0028_auto_20190204_1906'),
    ]

    operations = [
        migrations.AlterField(
            model_name='order',
            name='statuses',
            field=models.ManyToManyField(blank=True, null=True, to='lk.Statuses', verbose_name='Статусы'),
        ),
    ]
