import math
from collections import OrderedDict

from django.conf import settings

from rest_framework.response import Response

from rest_framework.pagination import (
	LimitOffsetPagination,
	PageNumberPagination,
	)


class PageNumberPagination(PageNumberPagination):
	page_size = settings.PAGINATE_BY

	def get_paginated_response(self, data):
		return Response(OrderedDict([
			('allPage', self.page.paginator.count),
			('lastPage', math.ceil(self.page.paginator.count / self.page_size)),
			('countItemsOnPage', self.page_size),
			('current', self.page.number),
			('next', self.get_next_link()),
			('previous', self.get_previous_link()),
			('results', data)
		]))
		