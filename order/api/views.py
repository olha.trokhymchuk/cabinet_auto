import logging

from django.http import JsonResponse

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.views.decorators.csrf import csrf_exempt

import order.order_constants as order_const

from django.db.models import Q
from django.db.utils import IntegrityError

from rest_framework.generics import ListCreateAPIView, UpdateAPIView

from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication

from order.models import Order, Document
from report.models import ReportPodbor
from order.serializers import OrderSerializer, SetOrderExpertSerializer

from .pagination import LimitOffsetPagination, PageNumberPagination

from core.models import User, Filial

from core.utils import add_status

logger = logging.getLogger(__name__)


class ApiOrderByUserList(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    pagination_class = PageNumberPagination

    def list(self, request, pk):
        user = User.objects.get(pk=pk)
        queryset = self.get_queryset()
        queryset = queryset.filter(	Q(operator=user) |
                                    Q(expert=user) |
                                    Q(expert_check=user) |
                                    Q(transferring=user) |
                                    Q(transferring=user) |
                                    Q(client=user))
        if user.is_acounter or user.is_leader:
            queryset = queryset.filter(filial__in=user.filials.all())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = OrderSerializer(queryset, many=True)
        return Response(serializer.data)


class ApiOrderSelectionList(ApiOrderByUserList):
    def get_queryset(self):
        return self.queryset.filter(order_type='PPK').order_by('created').order_by('status_closing', '-id')


class ApiOrderTransferringOrd(ApiOrderByUserList):
    def get_queryset(self):
        page_user = self.kwargs["pk"]
        return self.queryset.filter(order_type='PPK', transferring=page_user).order_by('created').order_by('status_closing', '-id')


class ApiOrderOutsideDiagnose(ApiOrderByUserList):
    def get_queryset(self):
        return self.queryset.filter(order_type='VZD').order_by('created').order_by('status_closing', '-id')


class ApiOrderExpertday(ApiOrderByUserList):
    def get_queryset(self):
        return self.queryset.filter(order_type='END').order_by('created').order_by('status_closing', '-id')


class ApiOrderOther(ApiOrderByUserList):
    def get_queryset(self):
        return self.queryset.exclude(order_type__in=order_const.EXCLUDEORDERTYPE).order_by('created').order_by('status_closing', '-id')


class ApiSetOrderExpert(UpdateAPIView):
    authentication_classes = (SessionAuthentication, )
    queryset = Order.objects.all()
    serializer_class = SetOrderExpertSerializer

    def status_changed(self):
        self.object.save()
        add_status(self.object, 'change-status-crm', 'order')

    def update(self, request, *args, **kwargs):
        # Update the  instance
        self.object = self.get_object()
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if request.data['type_expert'] == 'expert':
            if not self.object.expert.all():
                self.object.status = order_const.IN_WORK
            try:
                self.object.expert.clear()
                self.object.expert.add(serializer.validated_data['expert'])
            except IntegrityError as ex:
                logger.error(ex)
            else:
                self.status_changed()
                logger.error('expert')

        if request.data['type_expert'] == 'expert_check':
            self.object.status = order_const.RECHECK
            try:
                self.object.expert_check.clear()
                self.object.expert_check.add(
                    serializer.validated_data['expert'])
            except IntegrityError as ex:
                logger.error(ex)
            else:
                self.status_changed()
                logger.error('expert_check')

        if request.data['type_expert'] == 'transferring':
            self.object.status = order_const.READY_RECIEVE
            try:
                self.object.transferring.clear()
                self.object.transferring.add(
                    serializer.validated_data['expert'])
            except IntegrityError as ex:
                logger.error(ex)
            else:
                self.status_changed()
                logger.error('transferring')

        return Response(serializer.validated_data)

    def expoert_change(self):
        for transferring in data:
            if not transferring in self.instance.transferring.all():
                self.transferring_change = True


class ApiOrderFililList(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    pagination_class = PageNumberPagination

    def get_queryset(self):
        return self.queryset.order_by('created').order_by('status_closing', '-id')

    def list(self, request, pk):
        filial = Filial.objects.get(pk=pk)
        queryset = self.get_queryset()
        if self.request.user.is_leader and not filial in self.request.user.filials.all():
            raise Http404("страница не найдена")
        queryset = queryset.filter(filial=filial)
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = OrderSerializer(queryset, many=True)
        return Response(serializer.data)


def send_order_to_recheck(request):
    order_id = request.POST.get('order_id', 0)
    report_id = request.POST.get('report_id', 0)
    expert_id = request.POST.get('expert_id', 0)
    if order_id != 0 and report_id != 0:
        order = Order.objects.get(id=order_id)
        report = ReportPodbor.objects.get(id=report_id)
        expert = User.objects.get(id=expert_id)
        order.status = order_const.RECHECK
        order.expert_check.clear()
        order.expert_check.add(expert)
        report.in_recheck = True
        report.save()
        order.save()
        return JsonResponse({'error': 0, 'message': 'ok'}, status=200)
    return JsonResponse({'error': 1, 'message': 'Error'}, status=200)


@csrf_exempt
def get_order_documents(request):
    hash = request.POST.get('hash', 0)
    number_buh = request.POST.get('number_buh', 0)
    password = "6b97bea1c41d87d446f42013ea4a666aa863a0f51f7fa5718cadca2e633fce05ae94959becbf076b9a6d1f1cd707e1fedbb8293140e833b1a81255689b8694b8"
    variable = {"a": 0, "b": 1, "c": 2, "d": 3}
    if hash == password and number_buh != 0:
        # a, b, c, d = "Договор", "Соглашение на изменение цены", "Соглашение на смену критериев", "Акт выполненных работ"
        order = get_object_or_404(Order, number_buh=number_buh)
        for type in request.FILES:
            num_type = variable.get(type)
            if num_type == 0 or num_type:
                for doc in request.FILES.getlist(type):
                    obj = Document.objects.create(order=order, doc_type=num_type, file=doc)

        return JsonResponse({'error': 0, 'message': 'Ok'}, status=200)
    return JsonResponse({'error': 1, 'message': 'Error'}, status=400)


@csrf_exempt
def change_order_criteria(request):
    if request.POST['form'] == 'change_order_status':
        status = request.POST.get('status', False)
        order_id = request.POST.get('order_id', False)
        comment = request.POST.get('comment', False)
        if status and order_id and comment:
            Order.objects.filter(pk=order_id).update(status=status, comment=comment)
            return JsonResponse({'error': 0, 'message': 'Ok'}, status=200)
        return JsonResponse({'error': 1, 'message': 'Error'}, status=400)

    if request.POST['form'] == 'change_order_date_of_inspection':
        order_id = request.POST.get('order_id', False)
        date_of_inspection = request.POST.get('date_of_inspection', False)
        if order_id and date_of_inspection:
            Order.objects.filter(pk=order_id).update(date_of_inspection=date_of_inspection)
            return JsonResponse({'error': 0, 'message': 'Ok'}, status=200)
        return JsonResponse({'error': 1, 'message': 'Error'}, status=400)

    if request.POST['form'] == 'change_order_inspection_place':
        order_id = request.POST.get('order_id', False)
        inspection_place = request.POST.get('inspection_place', False)
        if order_id and inspection_place:
            Order.objects.filter(pk=order_id).update(inspection_place=inspection_place)
            return JsonResponse({'error': 0, 'message': 'Ok'}, status=200)
        return JsonResponse({'error': 1, 'message': 'Error'}, status=400)