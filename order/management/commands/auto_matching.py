from django.core.management.base import BaseCommand

from order.workers.auto_matching.auto_matching import autoMatching

class Command(BaseCommand):
    help = 'mathing auto for order by ppk'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        autoMatching()