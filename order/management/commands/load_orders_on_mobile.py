from django.core.management.base import BaseCommand

from order.workers.export import load_orders_on_mobile

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        load_orders_on_mobile()

