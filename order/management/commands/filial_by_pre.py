from django.core.management.base import BaseCommand

from order.workers.filial_by_pref import set_filial_by_pref

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        set_filial_by_pref()