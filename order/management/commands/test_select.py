from django.core.management.base import BaseCommand
from django.db import connection

class Command(BaseCommand):
    help = 'test select'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        auto = {}
        with connection.cursor() as cursor:
            id = 19618
            cursor.execute("\
                SELECT \
                 auto_auto.vin AS auto_vin,\
                 auto_markauto.name AS mark_name, auto_modelauto.name AS model_name\
                FROM report_reportpodbor\
                LEFT JOIN report_reportpodbor_order ON report_reportpodbor_order.order_id =%s \
                LEFT JOIN auto_auto ON report_reportpodbor.auto_id = auto_auto.id \
                LEFT JOIN auto_markauto ON auto_auto.mark_auto_id = auto_markauto.id \
                LEFT JOIN auto_modelauto ON auto_auto.model_auto_id = auto_modelauto.id\
                WHERE report_reportpodbor_order.reportpodbor_id =  report_reportpodbor.id", [id])
            auto = cursor.fetchone()
        client = {}
        with connection.cursor() as cursor:
            id = 19618
            cursor.execute("\
                SELECT \
                 core_user.first_name AS client_first_name,\
                 core_user.phone AS client_phone\
                FROM order_order\
                LEFT JOIN core_user ON core_user.id = order_order.client_id \
                WHERE order_order.id =  %s", [id])
            client = cursor.fetchone()
        result = {}
        if len(auto) > 0:
            result = {
                'vin' : auto[0],
                'mark' : auto[1],
                'model' : auto[2]
            }
        if len(client)>0:
            result.update(
                {
                    'name' : client[0],
                    'phone' : client[1]
                }
            )
        print(result)