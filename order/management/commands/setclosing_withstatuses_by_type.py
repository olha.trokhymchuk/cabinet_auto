from datetime import datetime, timedelta

from django.core.management.base import BaseCommand
from django.db.models import Q

import order.order_constants as order_const
from order.models import Order


class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def handle(self, *args, **options):
		order_by_types = {}
		orders = Order.objects.exclude(Q(status_closing=True)) \
			.filter(Q(status=order_const.REFUSAL)| 
					Q(status=order_const.RETURN)|
					Q(status=order_const.RETDONE)
				)
		orders.update(status_closing=True)
		# for order in orders:
		# 	type_name = order_const.ORDER_TYPE_NAME[order.order_type]
		# 	try:
		# 		order_by_types[type_name]
		# 	except KeyError:
		# 		order_by_types[type_name] = []
		# 	order_by_types[type_name].append(order.pk)
		# 	print(order.pk)
		# 	order.status_closing = True
		# 	order.save()
		print(orders.count())
		# print(order_by_types)


