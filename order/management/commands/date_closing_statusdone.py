from django.core.management.base import BaseCommand

from order.workers.set_date import set_date

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        set_date()
