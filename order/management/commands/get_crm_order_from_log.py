from django.core.management.base import BaseCommand

from order.workers.get_crm_order import get_crm_order_from_log

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        get_crm_order_from_log()