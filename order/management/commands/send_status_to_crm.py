from django.core.management.base import BaseCommand

from order.workers.send_status_to_crm import send_status_from_command

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('order_id')

    def handle(self, *args, **options):
        params = options['order_id'].split(',')
        send_status_from_command(params)