from django.core.management.base import BaseCommand

from order.workers.send_status_to_crm import send_statuses

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        send_statuses()
