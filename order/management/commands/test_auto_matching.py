from django.core.management.base import BaseCommand

from order.workers.auto_matching.test_auto_matching import testAutoMatching

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        testAutoMatching()