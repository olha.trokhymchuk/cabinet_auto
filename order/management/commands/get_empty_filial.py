from django.core.management.base import BaseCommand

from order.workers.filial_by_pref import get_empty_filial

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        pass

    def handle(self, *args, **options):
        get_empty_filial()
