from datetime import datetime, timedelta, date

from django.core.management.base import BaseCommand
from django.db.models import Q

from core.utils import getFieldHistory, add_status

import order.order_constants as order_const
from order.models import Order

from report.models import ReportPodbor


class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def handle(self, *args, **options):
		today = datetime.now().date()
		days_to_subtract = 14
		day_before = today - timedelta(days=days_to_subtract)
		orders = Order.objects.exclude(Q(status_closing=True) & Q(status=order_const.DONE)) \
			.filter(created__gte=day_before, order_type__in=order_const.INWORKFORREPORT, status=order_const.IN_WORK, reportpodbor__isnull=False)
		print(orders.count())
		for order in orders:
			if order.order_type in order_const.TYPEWITHRECOMENDEDREPORT:
				self.sendRecomended(order)
			print(order.pk)
			print(order.order_type)
			order.status = order_const.DONE
			order.status_closing = True
			order.save()
			add_status(order, 'change-status-crm', 'order')


	def sendRecomended(self, order):
			yesterday = date.today() - timedelta(days=1)
			params = {
				"date_created__gte": yesterday 
			}
			for report in order.reportpodbor_set.filter(recommended=True):
				res = getFieldHistory(report, ReportPodbor, "order", params)
				if res:
					add_status(order, 'crm-recomend-report', 'order')
					return