from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.urls import reverse

from core.models import Filial
from core.tests.test_response_code import _ResponseCodeTestCase
from order.models import Order


class OrderListTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(OrderListTestCase, self).__init__(*args, **kwargs)
        self.perm = 'order.view_order'
        self.url = reverse('lk:order_list')


class OrderDetailTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(OrderDetailTestCase, self).__init__(*args, **kwargs)
        self.perm = 'order.view_order'

    def setUp(self):
        super(OrderDetailTestCase, self).setUp()
        filial = Filial.objects.create(name='Moscow')
        order = Order.objects.create()

        for user, client in self.clients:
            user.filials.add(filial)
            if user.is_expert:
                order.expert.add(user)
            elif user.is_operator:
                order.operator.add(user)
            elif user.is_client:
                order.client = user
                user.filials.add(filial)
        order.save()
        self.url = reverse('lk:order_detail', args=(order.id, ))

    def tearDown(self):
        Order.objects.all().delete()


class OrderCreateTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(OrderCreateTestCase, self).__init__(*args, **kwargs)
        self.perm = 'order.add_order'
        self.url = reverse('lk:order_add')


class OrderUpdateTestCase(_ResponseCodeTestCase):
    def __init__(self, *args, **kwargs):
        super(OrderUpdateTestCase, self).__init__(*args, **kwargs)
        self.perm = 'order.change_order'

    def setUp(self):
        super(OrderUpdateTestCase, self).setUp()
        order = Order.objects.create()
        self.url = reverse('lk:order_edit', args=(order.id, ))

    def tearDown(self):
        Order.objects.all().delete()
        super(OrderUpdateTestCase, self).tearDown()
