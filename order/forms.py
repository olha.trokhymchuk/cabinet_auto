import re
import logging

from celery.execute import send_task

import order.order_constants as order_const

from django.forms import Field, MultipleChoiceField, ChoiceField
from django.db.utils import IntegrityError

from auto.glossary import *
from auto.models import ModelAuto, Generation
from core.forms import MyModelForm
from django import forms
from .models import Order
from service.models import PodborAuto
from core.models import User
from lk.models import Statuses

logger = logging.getLogger(__name__)


class MyModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return '%s %s' % (obj.first_name, obj.last_name)


class OrderEditForm(MyModelForm):
    class Meta:
        model = Order
        exclude = ('cars', 'statuses')

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        super(OrderEditForm, self).__init__(*args, **kwargs)
        self.transferring_status = False
        try:
            if('cost' in self.data):
                val = int(re.sub(r'\D', '', str(self.data['cost'])))
                data = self.data.copy()
                data['cost'] = val
                self.data = data
        except ValueError as ex:
            logger.error(ex)
        try:
            if('paid' in self.data):
                val = int(re.sub(r'\D', '', str(self.data['paid'])))
                data = self.data.copy()
                data['paid'] = val
                self.data = data
        except ValueError as ex:
            logger.error(ex)
        try:
            if user:
                filials = user.filials.all()
                self.fields['client'].queryset = User.objects.filter(
                    groups__name='Клиент', filials__in=filials).distinct()
                self.fields['expert'].queryset = User.objects.filter(
                    groups__name='Эксперт', filials__in=filials).distinct()
                self.fields['operator'].queryset = User.objects.filter(
                    groups__name='Оператор', filials__in=filials).distinct()
        except ValueError as ex:
            logger.error(ex)

    def expert_changed(self):
        if self.transferring_change or self.expert_change or self.expert_check_change:
            logger.error('change')
            return True

    def clean(self):
        if self.instance.status_closing:
            return
        try:
            status = int(self.data['status'])
            if self.expert_changed() or status != self.instance.status and status in order_const.STATUSES_TO_CRM:
                statuses, create = Statuses.objects.get_or_create(
                    name='change-status-crm')
                self.instance.statuses.add(statuses)
        except (IntegrityError, NameError, ValueError) as ex:
            logger.error(ex)

    def clean_status_closing(self):
        status = int(self.data['status'])
        if status == 4 and self.data['paid'] >= self.data['cost']:
            return True
        elif status in order_const.SETCLOSING_WITH_STATUS and status != 4:
            return True
        return False

    def clean_transferring(self):
        data = self.cleaned_data['transferring']
        self.transferring_change = False
        if not data:
            logger.error(data)
            self.transferring_change = True
        else:
            print('tyt')
            # if self.id:
            #     for transferring in data:
            #         if not transferring in self.instance.transferring.all():
            #             self.transferring_change = True
            #             break
        status = int(self.data['status'])
        if not data and status == order_const.READY_RECIEVE:
            msg = "Експерт по сдаче назначен"
            self.add_error('status', msg)
        # if self.id:
        #     if data and not self.instance.transferring.all():
        #         self.transferring_status = True
        #         self.cleaned_data['status'] = order_const.READY_RECIEVE
        return data

    def clean_expert_check(self):
        data = self.cleaned_data['expert_check']
        self.expert_check_change = False
        if not data:
            self.expert_check_change = True
        else:
            for expert_check in data:
                print('tyt')
                # if self.id:
                #     if not expert_check in self.instance.expert_check.all():
                #         self.expert_check_change = True
                #         break
        status = int(self.data['status'])
        if not data and status == order_const.RECHECK:
            msg = "Эксперт по перепроверке  не назначен"
            self.add_error('status', msg)
        # if self.id:
        #     if data and not self.transferring_status and not self.instance.expert_check.all():
        #         self.cleaned_data['status'] = order_const.RECHECK
        return data

    def clean_expert(self):
        self.expert_change = False
        data = self.cleaned_data['expert']
        if not data:
            self.expert_change = True
        else:
            for expert in data:
                print('tyt')
                # if self.id:
                #     if not expert in self.instance.expert.all():
                #         self.expert_change = True
                #         break

        status = int(self.data['status'])
        if (not data and (status == order_const.IN_WORK or
                          status == order_const.RECHECK or
                          status == order_const.READY_RECIEVE)):
            msg = "Эксперт по подбору не назначен"
            self.add_error('status', msg)
        try:
            print('tyt')
            # if self.id:
            #     if data and not self.instance.expert.all():
            #         self.expert_change = True
            #         self.cleaned_data['status'] = order_const.IN_WORK
        except ValueError as ex:
            logger.error(ex)
        return data


class SelectArrayField(Field):
    def __init__(self, base_field, **kwargs):
        self.base_field = base_field
        self.base_field.choices = self.base_field.choices
        self.widget = self.base_field.widget
        super(SelectArrayField, self).__init__(**kwargs)

    def clean(self, value):
        value = self.base_field.clean(value)
        value = [int(v) for v in value]
        return value


class OrderAddForm(MyModelForm):
    class Meta:
        model = Order
        fields = ('number_buh', 'comment')


class PodborAddForm(MyModelForm):
    number_of_hosts = ChoiceField(choices=NUMBER_OF_HOST_TYPE, required=False)
    transmission_type = SelectArrayField(
        MultipleChoiceField(choices=TRANSMISSION_TYPE, required=False))
    engine_type = SelectArrayField(
        MultipleChoiceField(choices=ENGINE_TYPE, required=False))
    drive_type = SelectArrayField(
        MultipleChoiceField(choices=DRIVE_TYPE, required=False))
    salon_auto = SelectArrayField(
        MultipleChoiceField(choices=SALON_AUTO, required=False))
    color_auto = SelectArrayField(MultipleChoiceField(
        choices=COLOR_OF_AUTO, required=False))
    color_salon = SelectArrayField(MultipleChoiceField(
        choices=COLOR_SALON_TYPE, required=False))

    def __init__(self, *args, **kwargs):
        super(PodborAddForm, self).__init__(*args, **kwargs)
        if not args and self.instance and self.fields:
            try:
                if('cost' in self.data):
                    val = int(re.sub(r'\D', '', str(self.data['cost'])))
                    data = self.data.copy()
                    data['cost'] = val
                    self.data = data
            except ValueError as ex:
                logger.error(ex)
            if 'model_auto' in self.fields:
                self.fields['model_auto'].queryset = ModelAuto.objects.filter(
                    mark_auto=self.instance.mark_auto)
            if 'generation' in self.fields:
                self.fields['generation'].queryset = Generation.objects.filter(
                    model_auto=self.instance.model_auto)

    class Meta:
        model = PodborAuto
        fields = '__all__'
