import django_filters
from django.db.models import Q

from .models import Order


class OrderFilter(django_filters.FilterSet):
    executor = django_filters.NumberFilter(method='executor_filter')
    status_closing = django_filters.CharFilter(method='boolean_filter')
    status_pay = django_filters.CharFilter(method='status_pay_filter')
    start = django_filters.DateFilter(method='date_after_filter')
    end = django_filters.DateFilter(method='date_before_filter')
    date_end_start = django_filters.DateFilter(method='date_end_after_filter')
    date_end_end = django_filters.DateFilter(method='date_end_before_filter')
    status = django_filters.NumberFilter(method='status_filter')
    filial = django_filters.NumberFilter(method='filial_filter')

    class Meta:
        model = Order
        fields = '__all__'

    def boolean_filter(self, queryset, name, value):
        boolean_value = True if value in (True, 'True', 'true', '1') else False
        return queryset.filter(**{
            name: boolean_value,
        })

    def filial_filter(self, queryset, name, value):
        queryset = queryset.filter(filial=value)
        return queryset

    def status_pay_filter(self, queryset, name, value):
        if not value:
            return queryset

        filtered = []
        for q in queryset:
            if q.get_status_pay() == 'оплачен':
                filtered.append(q.id)

        queryset = queryset.filter(id__in=filtered)
        return queryset

    def status_filter(self, queryset, name, value):
        queryset = queryset.filter(status=value)
        return queryset

    def executor_filter(self, queryset, name, value):
        return queryset.filter(Q(expert=value) | Q(operator=value) | Q(transferring=value))

    def date_after_filter(self, queryset, name, value):
        return queryset.filter(created__gte=value)

    def date_before_filter(self, queryset, name, value):
        return queryset.filter(created__lte=value)

    def date_end_after_filter(self, queryset, name, value):
        return queryset.filter(date_end__gte=value)

    def date_end_before_filter(self, queryset, name, value):
        return queryset.filter(date_end__lte=value)
