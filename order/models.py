import logging
import json
import os
import telebot
from telebot import types
from django.contrib.sites.shortcuts import get_current_site
from django.db import connection

from celery.execute import send_task
from auto.glossary import *
import order.order_constants as order_const

from django.urls import reverse
from django.utils import timezone
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import models
from django.db.models import Q
from django.db.utils import IntegrityError
from safedelete import HARD_DELETE
from safedelete.models import SafeDeleteModel
from datetime import datetime
from dateutil.relativedelta import relativedelta


from auto.models import Auto
from auto.utils import get_choice_by_name
from core.models import User, Filial, Reviews, TelegramMessages
from core.utils import CustomFieldHistoryTracker

from notifications.models import Notification
from notifications.utils import send_notification

from lk.models import Statuses

logger = logging.getLogger(__name__)


class OrderExcepiton(Exception):
    pass


class Order(SafeDeleteModel):
    _safedelete_policy = HARD_DELETE

    SUBMIT_FROM = (
        ('LK', 'Заказ из лк'),
        ('CRM', 'Заказ из црм'),
    )
    save_from_bot = False

    report_id = 0

    transferring_id = 0
    is_read_by_client = models.BooleanField(
        'Прочитан клиентом после сдачи', default=True)
    status = models.PositiveSmallIntegerField(
        'Статус', blank=True, choices=order_const.STATUS, default=0)
    date_start = models.DateField(
        'Дата начала', default=timezone.now, blank=True)
    date_end = models.DateField('Дата сдачи', null=True, blank=True)
    number_buh = models.CharField(
        'Номер договора (Бух.)', max_length=50, unique=True, default='', blank=True, db_index=True)
    created = models.DateTimeField(
        'Дата создания', auto_now_add=True, db_index=True)
    modified = models.DateTimeField('Последнее изменение', auto_now=True)
    order_type = models.CharField('Тип заказа', max_length=3, blank=True,
                                  choices=order_const.ORDER_TYPE, default='PPK', db_index=True)
    submit_from = models.CharField('От куда заказ(лк, црм, ...)', max_length=3,
                                   blank=True, choices=SUBMIT_FROM, default='LK', db_index=True)
    operator = models.ManyToManyField(
        User, blank=True, default=None, verbose_name='Операторы', related_name="user_op")
    expert = models.ManyToManyField(
        User, blank=True, default=None, verbose_name='Эксперты', related_name="user_ex")
    expert_check = models.ManyToManyField(
        User, blank=True, default=None, verbose_name='Эксперты на перепроверке', related_name="user_ch")
    transferring = models.ManyToManyField(
        User, blank=True, default=None, verbose_name='Сдающие', related_name="user_sd")
    client = models.ForeignKey(User, blank=True, null=True,
                               default=None, verbose_name='Клиент', related_name="user_cl")
    status_closing = models.BooleanField('Заказ закрыт', default=False)
    status_closing_date = models.DateTimeField(
        'Заказ закрыт время', null=True, blank=True)
    auto_from_salon = models.BooleanField('Авто из салона', default=False)
    canceled = models.BooleanField('Заказ отменен', default=False)
    cost = models.PositiveIntegerField(
        'Стоимость', blank=True, null=True, default=None)
    paid = models.PositiveIntegerField('Оплачено', blank=True, default=0)
    with_client = models.BooleanField('В присутствии клиента', default=False)
    place_issuing = models.CharField(
        'Комментарий к заказу', max_length=300, blank=True, null=True, default=None)
    comment = models.CharField(
        'Комментарий к заказу', max_length=2000, blank=True, null=True, default=None)
    reason_refuse = models.CharField(
        'Причина отказа', max_length=2000, blank=True, null=True, default=None)
    cars = models.ManyToManyField(
        Auto, blank=True, default=None, verbose_name='Автомобили')
    reason_refuse = models.CharField(
        'Причина отказа', max_length=2000, blank=True, null=True, default=None)
    statuses = models.ManyToManyField(
        Statuses, blank=True, verbose_name='Статусы')
    filial = models.ForeignKey(Filial, blank=True, null=True, default=None,
                               verbose_name='Филиал', related_name="order_filial")
    notifications = models.ManyToManyField(
        Notification, blank=True, default=None, verbose_name='Оповещения', related_name="order_notifications")
    refund_amount = models.PositiveIntegerField('Сумма возврата', blank=True, null=True, default=None)
    expert_compensation = models.PositiveIntegerField('Компенсация эксперту', blank=True, null=True, default=None)
    review = models.ForeignKey(Reviews, blank=True, null=True, default=None,
                               verbose_name='Отзыв', related_name="order_review")
    date_of_inspection = models.DateTimeField(
        'Дата и время осмотра', blank=True, null=True)
    inspection_place = models.TextField(
        'Место осмотра', blank=True, null=True, default=None)
    link_url = models.TextField('Ссылка на объявление', null=True, blank=True, default=None)
    mark_model = models.TextField(
        'Марка и модель', blank=True, null=True, default=None)

    if settings.CUSTOM_FIELD_HISTORY:
        pass
    field_history = CustomFieldHistoryTracker(['number_buh', 'order_type', 'operator', 'expert', 'transferring',
                                               'client', 'canceled', 'status', 'status_closing', 'expert_check'])

    class Meta:
        verbose_name = "заказ"
        verbose_name_plural = "заказы"
        ordering = ["-created"]

    def get_last_document(self):
        all_doc = Document.objects.filter(order=self)
        list_doc = [all_doc.filter(doc_type=i).latest('id') for i in range(4) if all_doc.filter(doc_type=i)]
        return list_doc

    def get_last_report_id(self):
        row = {}
        with connection.cursor() as cursor:
            cursor.execute(
                "SELECT COUNT(id) FROM report_reportpodbor_order WHERE order_id = %s", [self.id])
            row['count'] = cursor.fetchone()[0]
            cursor.execute(
                "SELECT reportpodbor_id FROM report_reportpodbor_order WHERE order_id = %s", [self.id])
            row['ids'] = cursor.fetchone()
        return row

    def __str__(self):
        return "{}".format(self.id)

    def save(self, *args, **kwargs):




        statuses = [order_const.DONE, order_const.REFUSAL,
                    order_const.RETURN, order_const.RETDONE,
                    order_const.FAIL_CALL]
        if self.status_closing and self.status not in statuses:
            raise OrderExcepiton('message', 'статус')

        created = False if self.id else True
        status_changed_conditions = []
        telegram_data = {}
        message_text = {}
        status_paid_changed = False
        status_closing_changed = False
        status_changed = False
        amount = 0

        if created and self.order_type == 'PPK':
            self.date_end = self.date_start + relativedelta(months=2)

        # Создание логера интеграции по каждому заказу

        # integration_loger_order = logging.getLogger('integration_v2')
        #
        # if not os.path.exists('integration_loger/'):
        #     os.makedirs('integration_loger/')
        # f_handler = logging.FileHandler('integration_loger/integration_loger_order.log')
        # if not created:
        #     if not os.path.exists('integration_loger/' + str(self.id) + 'id/'):
        #         os.makedirs('integration_loger/' + str(self.id) + 'id/')
        #     f_handler = logging.FileHandler('integration_loger/' + str(self.id) + 'id/integration_loger_order.log')
        # f_handler.setLevel(logging.INFO)
        # f_format = logging.Formatter('%(asctime)s - %(module)s -%(filename)s - %(funcName)s - %(message)s')
        # f_handler.setFormatter(f_format)
        # integration_loger_order.addHandler(f_handler)
        # END Создание логера интеграции по каждому заказу

        # integration_loger_order.info('--- проверка на создание заказа ---')
        if not created:

            obj = Order.all_objects.get(id=self.id)

            if self.order_type == 'PPK' and obj.status == order_const.DONE and self.status == order_const.IN_WORK:
                self.date_end = self.date_start + relativedelta(months=2)
            status_changed_conditions = [
                self.status != obj.status,
                self.status_closing != self.status_closing,
                self.canceled != obj.canceled
            ]
            status_changed = self.status != obj.status
            status_paid_changed = self.paid and obj.paid and self.paid - obj.paid > 0 or \
                self.paid and self.paid > 0 and not obj.paid
            if status_paid_changed:
                prev_paid = obj.paid if obj.paid else 0
                amount = self.paid - prev_paid

            status_closing_changed = self.status_closing and obj.status_closing != self.status_closing

            if status_changed and self.status == order_const.DONE:
                self.is_read_by_client = False

            if status_changed and self.status != order_const.RECHECK and obj.status == order_const.RECHECK:
                self.reportpodbor_set.all().update(in_recheck=False)
        # integration_loger_order.info('---END проверка на создание заказа ---')
        if self.status_closing and status_closing_changed:
            self.status_closing_date = datetime.now()
        else:
            self.status_closing_date = None
        if status_changed and self.status == order_const.READY_RECIEVE and self.order_type == order_const.PPK:
            # статус изменен на готов к выдаче
            # тип заказа Подбор «под ключ»
            # integration_loger_order.info('---START Statuses.objects.get_or_create ---')
            statuses, create = Statuses.objects.get_or_create(
                name='crm-ready-recieve')
            self.statuses.add(statuses)
            # integration_loger_order.info('---END Statuses.objects.get_or_create ---')
            logger.error('crm-ready-recieve')
        if not self.save_from_bot and not created:
            # integration_loger_order.info('---IN TELEGRAM IF ---')
            saved_transfer = self.transferring.first()
            saved_transfer_id = 0
            if saved_transfer:
                saved_transfer_id = saved_transfer.id

            if (status_changed or int(self.transferring_id) != int(
                    saved_transfer_id)) and self.status == order_const.READY_RECIEVE:
                self.status = obj.status
                logger.error("self.status")
                logger.error(self.status)
                logger.error("self.status")
                bot = telebot.TeleBot(settings.TOKEN)
                leaders = self.filial.leaders
                for tele_user in leaders:
                    if tele_user.chat_telegram_id:
                        callback_data_str = str(self.id)
                        print('====teleuser=======')
                        print(tele_user.chat_telegram_id)
                        print('====teleuser=======')
                        message = "Номер заказа: " + self.number_buh + "\n" + "Эксперты по подбору:"
                        experts = self.expert.all()
                        for exp in experts:
                            message += " " + exp.first_name + " " + exp.last_name + "\n"
                        message += "Эксперты на сдаче:"
                        if self.transferring_id != 0:
                            transferring_obj = User.objects.get(
                                id=self.transferring_id)
                            message += " " + transferring_obj.first_name + \
                                " " + transferring_obj.last_name + "\n"
                            self.transferring.clear()
                            self.transferring.add(transferring_obj)
                        else:
                            transferring = self.transferring.all()
                            for trans in transferring:
                                message += " " + trans.first_name + " " + trans.last_name + "\n"
                        message += "===================================\n"
                        message += "Заказ:\n"
                        site = get_current_site(None).domain
                        message += site + '/order/detail/' + \
                            str(self.id)+'/'+"\n"
                        message += "===================================\n"
                        if self.report_id != 0:
                            message += "===================================\n"
                            message += "Отчет:\n"
                            site = get_current_site(None).domain
                            message += site + '/report/view/' + \
                                str(self.report_id)+'/'+"\n"
                            message += "===================================\n"
                            callback_data_str += "_"+str(self.report_id)
                        else:
                            reports = self.get_last_report_id()
                            if(reports['count'] > 0):
                                message += "===================================\n"
                                message += "Отчет:\n"
                                site = get_current_site(None).domain
                                message += site + '/report/view/' + \
                                    str(reports['ids'][0])+'/'+"\n"
                                message += "===================================\n"
                                callback_data_str += "_"+str(reports['ids'][0])
                        if self.transferring_id != 0:
                            callback_data_str += "_"+str(self.transferring_id)
                        key = types.InlineKeyboardMarkup()
                        key.add(types.InlineKeyboardButton("Подтвердить", callback_data="yes_"+callback_data_str),
                                types.InlineKeyboardButton("Отклонить", callback_data="no_"+callback_data_str))

                        send_message = bot.send_message(
                            tele_user.chat_telegram_id, message, reply_markup=key)
                        telegram_data.update({send_message.message_id: tele_user.chat_telegram_id})
                        message_text.update({'text': message})
                        tele_user.save()

        # write chat id, message id and text messages
        if len(telegram_data) > 0 and len(message_text) > 0:
            tel_message = TelegramMessages.objects.create(telegram_messages_data=telegram_data, message_text=message_text)
            tel_message.save()
        # integration_loger_order.info('---START super save ---')
        super(Order, self).save(*args, **kwargs)
        # integration_loger_order.info('---END super save ---')
        data = {
            'order_id': self.pk,
            'username': self.pk,
            'status': self.client.full_name,
            'number_buh': self.number_buh,
            'order_type': self.get_order_type_display(),
        }
        # integration_loger_order.info('---START FIND client  ---')
        client = get_user_model().objects.filter(id=self.client_id)
        # integration_loger_order.info('---END FIND client  ---')
        # if True in status_changed_conditions:
        #     recipients = (self.expert.all() | self.expert_check.all()
        #                   | self.transferring.all() | self.operator.all())
        #     if self.client:
        #         recipients = recipients | client
        #     recipients = recipients.distinct()
        #     integration_loger_order.info('---START status_changed_conditions send_notification  ---')
        #     send_notification(
        #         list(recipients), Notification.STATUS_UPDATE, data=data, channels=['lk'])
        #     integration_loger_order.info('---END status_changed_conditions send_notification  ---')

        # if status_paid_changed and self.client:
        #     data.update({'amount': amount})
        #     integration_loger_order.info('---START status_paid_changed send_notification  ---')
        #     send_notification([self.client], Notification.PAYMENT,
        #                       data=data, channels=['email', 'sms', 'lk'])
        #     integration_loger_order.info('---END status_paid_changed send_notification  ---')
        # if status_closing_changed:
        #     recipients = (
        #         User.objects.filter(filials__in=self.client.filials.all(), groups__name='Руководитель') |
        #         self.expert.all() |
        #         self.expert_check.all() |
        #         self.transferring.all() |
        #         self.operator.all()).distinct()
        #     integration_loger_order.info('---START status_closing_changed send_notification  ---')
        #     send_notification(
        #         recipients, Notification.CLOSE_ORDER, data=data, channels=['lk'])
        #     integration_loger_order.info('---END status_closing_changed send_notification  ---')

        # try:
        #     if self.status == order_const.READY_RECIEVE:
        #         integration_loger_order.info('---START order_const.READY_RECIEVE send_notification  ---')
        #         send_notification(
        #             client, Notification.TRANSF_ORDER, data=data, channels=['sms'])
        #         integration_loger_order.info('---END order_const.READY_RECIEVE send_notification  ---')
        # except AttributeError as ex:
        #     logger.error(ex)

        # if self.status == order_const.REFUSAL:
        #     users = User.objects.filter(filials=self.filial)
        #     for user in users:
        #         if user.is_acounter:
        #             send_notification(
        #                 [user], Notification.BUH_OTKAZ_LK, data=data, channels=['lk'])
        # if created and settings.CUSTOM_FIELD_HISTORY:
        #     integration_loger_order.info('---START settings.CUSTOM_FIELD_HISTORY send_notification  ---')
        #     send_notification([self.client], Notification.NEW_ORDER,
        #                       data=data, channels=['email', 'sms', 'lk'])
        #     integration_loger_order.info('---END settings.CUSTOM_FIELD_HISTORY send_notification  ---')

    # Взврощает инфу для отправки клиенту сообщения для техосмотра после выполнения заказа
    def get_order_done_info(self):
        auto = {}
        with connection.cursor() as cursor:
            id = self.id
            cursor.execute("\
                SELECT \
                 auto_auto.vin AS auto_vin,\
                 auto_markauto.name AS mark_name, auto_modelauto.name AS model_name\
                FROM report_reportpodbor\
                LEFT JOIN report_reportpodbor_order ON report_reportpodbor_order.order_id =%s \
                LEFT JOIN auto_auto ON report_reportpodbor.auto_id = auto_auto.id \
                LEFT JOIN auto_markauto ON auto_auto.mark_auto_id = auto_markauto.id \
                LEFT JOIN auto_modelauto ON auto_auto.model_auto_id = auto_modelauto.id\
                WHERE report_reportpodbor_order.reportpodbor_id =  report_reportpodbor.id", [id])
            auto = cursor.fetchone()
        client = {}
        with connection.cursor() as cursor:
            id = 19618
            cursor.execute("\
                SELECT \
                 core_user.first_name AS client_first_name,\
                 core_user.phone AS client_phone\
                FROM order_order\
                LEFT JOIN core_user ON core_user.id = order_order.client_id \
                WHERE order_order.id =  %s", [id])
            client = cursor.fetchone()
        result = {}
        if auto is not None:
            if len(auto) > 0:
                result = {
                    'vin': auto[0],
                    'mark': auto[1],
                    'model': auto[2]
                }
        if client is not None:
            if len(client) > 0:
                result.update(
                    {
                        'name': client[0],
                        'tel': client[1]
                    }
                )
        return result

    def get_status_pay(self):
        if not self.paid or self.paid <= 0:
            return 'не оплачен'
        if 0 < self.paid < self.cost:
            return 'частично оплачен'
        if self.paid >= self.cost:
            return 'оплачен'

    def get_completed_date(self):
        if self.get_status_display() == 'выполнен':
            status_history = self.get_status_history().order_by('-date_created')
            choice = get_choice_by_name(order_const.STATUS, 'выполнен')

            for history in status_history:
                if history.field_value == choice:
                    return history.date_created.date()
        return None

    @property
    def executors(self):
        return (self.expert.all() | self.expert_check.all() | self.transferring.all() | self.operator.all()).distinct()

    def get_absolute_url(self):
        return reverse('lk:order_detail', args=(self.id, ))


def upload_order_documents(instance, filename):
    return "documents/order/%s/%s/%s/" % (instance.order.number_buh, instance.doc_type, filename)


class Document(models.Model):
    DOC_CHOICES = (
        ("0", "Договор"),
        ("1", "Соглашение на изменение цены"),
        ("2", "Соглашение на смену критериев"),
        ("3", "Акт выполненных работ"),
    )

    order = models.ForeignKey(Order, verbose_name='Заказ', related_name='documents')
    file = models.FileField(blank=True, null=True, upload_to=upload_order_documents)
    url = models.URLField(blank=True, null=True)
    doc_type = models.PositiveSmallIntegerField('Тип', blank=True, choices=DOC_CHOICES)
    created = models.DateTimeField('Дата создания', auto_now_add=True, db_index=True)
    url = models.URLField(blank=True, null=True)

    class Meta:
        verbose_name = "Документи"
        verbose_name_plural = "Документ"

    def filename(self):
        return os.path.basename(self.file.name)

    def file_name_format(self):
        if self.doc_type == 0:
            file_name = "Договор.pdf"
        if self.doc_type == 1:
            file_name = "Соглашение на изменение цены.pdf"
        if self.doc_type == 2:
            file_name = "Соглашение на смену критериев.pdf"
        if self.doc_type == 3:
            file_name = "Акт выполненных работ.pdf"
        return file_name


class Comment(models.Model):
    created = models.DateTimeField(
        'Время создания', auto_now_add=True, db_index=True)
    order = models.ForeignKey(
        Order, verbose_name='Заказ', related_name='comments')
    author = models.ForeignKey(User, verbose_name='Автор')
    private = models.BooleanField('Виден всем', default=False, blank=False)
    body = models.TextField(
        verbose_name='Текст комментария', default='', blank=True)

    class Meta:
        verbose_name = "Комментарий"
        verbose_name_plural = "Комментарии"

    def save(self, *args, **kwargs):
        super(Comment, self).save(*args, **kwargs)
        recipients = (self.order.expert.all() | self.order.expert_check.all() |
                      self.order.transferring.all() | self.order.operator.all())

        if not self.private:
            recipients = recipients | User.objects.filter(
                id=self.order.client.id)

        recipients = recipients.distinct()

        data = {
            'time': self.order.created.strftime(settings.DATETIME_FORMAT),
            'user': self.author.username,
            'order': self.order.number_buh,
            'comment': self.body
        }

        send_notification(list(recipients), Notification.COMMENT,
                          data=data, channels=['lk'])
