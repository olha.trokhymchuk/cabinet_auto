import logging
logger = logging.getLogger(__name__)

NEW = 0
IN_QUOE = 1
IN_WORK = 2
SET_ASIDE = 3
DONE = 4
READY_RECIEVE = 5
RECHECK = 6
REFUSAL = 7
GIVE_CLIENT = 8
RETURN = 9
RETDONE = 10
PARTRET = 11
READY_TRANSFER = 12
RETURNMONEYDOC = 13
FAIL_CALL = 14

VZD = 'VZD'
END = 'END'
OTH = 'OTH'
PPK= 'PPK'
EXN = 'EXN'
PNA = 'PNA'
POA = 'POA'
PY4 = 'PY4'
SVG = 'SVG'
SSK = 'SSK'

EXCLUDEORDERTYPE = [PPK, VZD, END]

ORDERCLOSED = "ORDERCLOSED"

GET_FROM_CRM = [IN_QUOE, IN_WORK, DONE, REFUSAL, RETURN,RETDONE,PARTRET,READY_TRANSFER,FAIL_CALL]
SETCLOSING_WITH_GETCRM = []

SETCLOSING_WITH_STATUS = [DONE,REFUSAL, RETURN,RETDONE,FAIL_CALL]

STATUSES_TO_CRM = [DONE,IN_WORK,READY_RECIEVE,GIVE_CLIENT, RECHECK, REFUSAL, RETURN, SET_ASIDE,RETDONE,PARTRET,FAIL_CALL]

CRM_TO_STATUS = {
    NEW: "new",
    IN_QUOE: "send-to-delivery",
    IN_WORK: "delivering",
    SET_ASIDE: "set-aside",
    DONE: "complete",
    READY_RECIEVE: 'ready-transfer',
    RECHECK: 'rechecking',
    REFUSAL: 'cancel-other',
    RETURN: 'return-money',
    GIVE_CLIENT: 'transfer',
    RETDONE:'refunded',
    PARTRET:'half-return',
    READY_TRANSFER:'ready-transfer',
    RETURNMONEYDOC: 'returnmoneydoc',
    FAIL_CALL: 'fail-call'
}

STATUS = (
        (NEW, 'новый'),
        (IN_QUOE, 'в очереди'),
        (IN_WORK, 'в работе'),
        (SET_ASIDE, 'отложен'),
        (GIVE_CLIENT, 'выдан клиенту'),
        (DONE, 'выполнен'),
        (READY_RECIEVE, 'готов к выдаче'),
        (RECHECK, 'перепроверен'),
        (REFUSAL, 'отказ'),
        (RETURN, 'возврат'),
        (RETURNMONEYDOC, 'возврат документы получены'),
        (RETDONE, 'возврат произведен'),
        (PARTRET, 'частичный возврат'),
        (READY_TRANSFER,'готов к выдаче'),
        (FAIL_CALL, 'ложный вызов')
)


ORDER_TYPE = (
        (PPK, 'Подбор «под ключ»'),
        (VZD, 'Выездная диагностика'),
        (END, 'Эксперт на день'),
        (EXN, 'Экспресс подбор'),
        (PNA, 'Подбор нового авто'),
        (POA, 'Предпродажная оценка авто'),
        (PY4, 'Проверка юр. чистоты'),
        (SVG, 'Сопровождение в ГИБДД'),
        (SSK, 'Сопровождение сделки купли-продажи'),
        (OTH, 'Прочие услуги'),
    )

ORDER_TYPE_NAME = {
    PPK: 'Подбор «под ключ»',
    VZD: 'Выездная диагностика',
    END: 'Эксперт на день',
    EXN: 'Экспресс подбор',
    PNA: 'Подбор нового авто',
    POA: 'Предпродажная оценка авто',
    PY4: 'Проверка юр. чистоты',
    SVG: 'Сопровождение в ГИБДД',
    SSK: 'Сопровождение сделки купли-продажи',
    OTH: 'Прочие услуги'
}

INWORKFORREPORT = [VZD, OTH]
TYPEWITHRECOMENDEDREPORT = [VZD, END, EXN]