import json
import logging
import collections
from itertools import chain
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.contenttypes.models import ContentType
from django.core.serializers.base import DeserializationError
from django.db.models import Q
from django.http import Http404, HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, permission_required
from django.views.generic import UpdateView, CreateView, DetailView
from django.core import signing
from field_history.models import FieldHistory
from safedelete import SOFT_DELETE_CASCADE
from django.contrib.sites.shortcuts import get_current_site
from autopodbor.utils import send_simple_message_mailgun
from notifications.models import Notification

from django.conf import settings
from time import time

from auto.forms import AutoForm
from auto.glossary import *
from auto.models import Generation, ModelAuto, Year, Auto
from auto.serializers import AutoSerializer
from auto.views import AutoListView
from core.serializers import UserSerializer, FilialSerializerPure
from core.utils import ImprovedListView
from custom_admin.utils import FilteredFieldsMixin, FilteredFieldContextMixin
from order.filters import OrderFilter
from order.serializers import OrderSerializer, OrderFieldHistorySerializer, CommentSerializer, OrderSerializerList
from report.models import ReportPodbor
from report.serializers import ReportListSerialializer
from service.serializers import PPKSerialializer
from .models import Order, Comment, OrderExcepiton
from core.models import User, Filial
from service.models import PodborAuto
from .forms import OrderEditForm, PodborAddForm

import order.order_constants as order_const


from core.utils import add_status

logger = logging.getLogger(__name__)


# Список заказов
class OrderListView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView):
    permission_required = 'order.view_order'
    raise_exception = True
    model = Order
    paginate_by = settings.PAGINATE_BY
    template_name = 'order/order_list.html'
    filterset_class = OrderFilter
    search_fields = ['id', 'number_buh']
    serializer = OrderSerializerList
    json_objects = 'objects'
    #ordering = '-id'

    def get_context_data(self, **kwargs):
        context = super(OrderListView, self).get_context_data()
        context.update({'html_title': 'Заказы'})
        return context

    def get_queryset(self):
        queryset = super(OrderListView, self).get_queryset()
        user = self.request.user
        queryset = queryset.filter(filial__in=user.filials.all())
        if user.is_client:
            queryset = queryset.filter(client=user).distinct()

        if not {'Эксперт', 'Оператор'}.isdisjoint(user.groups_names):
            queryset = queryset.filter(Q(expert=user) |
                                       Q(expert_check=user) |
                                       Q(transferring=user) |
                                       Q(operator=user))

        return queryset.order_by('status_closing', '-id').distinct()


class OrderListSelectKeyView(OrderListView):
    def get_queryset(self):
        queryset = super(OrderListSelectKeyView, self).get_queryset()
        queryset = queryset.filter(order_type='PPK')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(OrderListView, self).get_context_data()
        context.update({'html_title': 'Подбор "под ключ"'})
        return context


class OrderListClosedView(OrderListView):
    def get_queryset(self):
        queryset = super(OrderListClosedView, self).get_queryset()
        queryset = queryset.filter(status_closing=True)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(OrderListClosedView, self).get_context_data()
        context.update({'html_title': 'Закрытые',
                        'reviews': context["object_list"].exists(),
                        })
        return context


class OrderListInWorkView(OrderListView):
    def get_queryset(self):
        queryset = super(OrderListInWorkView, self).get_queryset()
        self.queryset = queryset = queryset.filter(status_closing=False)
        return queryset

    def get(self, *args, **kwargs):
        super(OrderListInWorkView, self).get(*args, **kwargs)
        if self.queryset.count() == 1:
            redirect_to = reverse('lk:order_detail', kwargs={'order_id': self.queryset.first().pk})
            return redirect(redirect_to)

        return super(OrderListInWorkView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(OrderListInWorkView, self).get_context_data()
        context.update({'html_title': 'В работе'})
        return context


class OrderListOutsideDiagnose(OrderListView):
    def get_queryset(self):
        queryset = super(OrderListOutsideDiagnose, self).get_queryset()
        queryset = queryset.filter(order_type='VZD')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(OrderListView, self).get_context_data()
        context.update({'html_title': 'Выездная диагностика'})
        return context


class OrderListExpertDayView(OrderListView):
    def get_queryset(self):
        queryset = super(OrderListExpertDayView, self).get_queryset()
        queryset = queryset.filter(order_type='END')
        return queryset

    def get_context_data(self, **kwargs):
        context = super(OrderListView, self).get_context_data()
        context.update({'html_title': 'Эксперт на день'})
        return context


class OrderListOtherView(OrderListView):
    def get_queryset(self):
        queryset = super(OrderListOtherView, self).get_queryset()
        queryset = queryset.exclude(order_type__in=order_const.EXCLUDEORDERTYPE)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(OrderListView, self).get_context_data()
        context.update({'html_title': 'Прочее'})
        return context


@login_required
@permission_required('order.view_order', raise_exception=True)
def get_order_filter(request):
    if request.is_ajax():
        data = {}
        executors = User.objects.filter(groups__name='Эксперт', is_active=True)
        statuses = [(status_id, status) for status_id, status in order_const.STATUS]
        clients = User.objects.filter(groups__name='Клиент', is_active=True)
        if request.user.is_expert:
            filials_queryset = Filial.objects.filter(id__in=request.user.filials.all())
            filials = FilialSerializerPure(filials_queryset, many=True).data
        else:
            filials_queryset = Filial.objects.all()
            filials = FilialSerializerPure(filials_queryset, many=True).data

        data.update({
            'statuses': statuses,
            'filials': filials,
            'clients': None,
            'executors': UserSerializer(executors, many=True).data})

        return JsonResponse(data)

    raise Http404


# Просмотр заказа
class OrderDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView, FilteredFieldContextMixin):
    permission_required = 'order.view_order'
    model = Order
    pk_url_kwarg = 'order_id'
    template_name = 'order/order_detail.html'

    def get_context_data(self, **kwargs):
        context = super(OrderDetailView, self).get_context_data()
        order = get_object_or_404(Order, id=self.kwargs['order_id'])
        documents = get_object_or_404(Order, id=self.kwargs['order_id'])
        from_salon = order.auto_from_salon
        context.update({'html_title': 'Заказ {}'.format(self.object.number_buh),
                        'from_salon': from_salon,
                        'documents': documents
                        })
        return context

    def get_queryset(self):
        queryset = super(OrderDetailView, self).get_queryset()
        user = self.request.user
        queryset = queryset.filter(client__filials__in=user.filials.all())

        if user.is_client:
            return queryset.filter(client=user).distinct()

        if not {'Эксперт'}.isdisjoint(user.groups_names):
            queryset = queryset.filter(Q(expert=user) |
                                       Q(expert_check=user) |
                                       Q(transferring=user) |
                                       Q(operator=user))

        return queryset.distinct()

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        if request.user.is_leader and self.object.filial in request.user.filials.all():
            notifications = self.object.notifications.filter(viewed=False, recipient=request.user)
            for note in notifications:
                note.viewed = True
                note.save()
        if request.is_ajax():
            data = {}
            serializer = OrderSerializer(self.object)
            expert_not_set = ('id', 0), ('full_name', 'Не установлен'), ('groups', ['Эксперт'])
            expert_not_set = collections.OrderedDict(expert_not_set)
            data.update(serializer.data)
            queryset = LogListView().get_logs(self.kwargs['order_id']).order_by('-date_created')
            order_log = OrderFieldHistorySerializer(queryset, many=True).data

            auto_view = OrderAutoListView()
            auto_view.kwargs = {'order_id': self.kwargs['order_id']}
            auto_view.request = request

            data.update({'order_log': order_log})
            data.update({'proven_cars': auto_view.serializer(auto_view.get_queryset(), many=True).data})

            if not serializer.data['transferring']:
                data.update({'transferring': [expert_not_set]})
            if not serializer.data['expert']:
                data.update({'expert': [expert_not_set]})
            return JsonResponse(data=data)
        return super(OrderDetailView, self).get(request, *args, **kwargs)


# Количество отчетов, критериев, автомобилей и т.п в json-е
@login_required
@permission_required('order.view_order', raise_exception=True)
def get_order_stats(request, order_id):
    if not request.is_ajax():
        raise Http404

    order = get_object_or_404(Order, id=order_id)

    report_view = ReportListView()
    setattr(report_view, 'kwargs', {'order_id': order_id})
    report_view.request = request

    ppk_view = PPKListView()
    setattr(ppk_view, 'kwargs', {'order_id': order_id})
    ppk_view.request = request

    auto_view = OrderAutoListView()
    setattr(auto_view, 'kwargs', {'order_id': order_id})
    auto_view.request = request

    data = {
        'ppk_count': ppk_view.get_queryset().count(),
        'car_count': auto_view.get_queryset().count(),
        'reports_count': report_view.get_queryset().count(),
        'log_count': LogListView().get_logs(order.id).count()
    }

    return JsonResponse(data)


# Список критериев по заказу
class PPKListView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView, FilteredFieldContextMixin):
    permission_required = 'service.view_podborauto'
    raise_exception = True
    template_name = 'order/order_ppk_list.html'
    model = PodborAuto
    field_perms_model = Order
    serializer = PPKSerialializer

    def get_context_data(self, **kwargs):
        context = super(PPKListView, self).get_context_data(**kwargs)
        order = get_object_or_404(Order, id=self.kwargs['order_id'])
        context.update({'order': order})
        horsepower_lower = ''
        horsepower_upper = ''
        try:
            horsepower = self.object_list.values().all()[0]['horsepower']
            horsepower_lower = horsepower.lower
            horsepower_upper = horsepower.upper
        except IndexError:
            pass
        except AttributeError:
            pass
        try:
            year_auto = self.object_list.values().all()[0]['year_auto']
        except IndexError:
            year_auto_lower = year_auto_upper = ''
        else:
            try:
                year_auto_lower = year_auto.lower if year_auto.lower is not None else ''
            except AttributeError as ex:
                year_auto_lower = ""
            try:
                year_auto_upper = year_auto.upper if year_auto.upper is not None else ''
            except AttributeError as ex:
                year_auto_upper = ""
        context.update(
            {'year_auto_lower': year_auto_lower, 'year_auto_upper': year_auto_upper,
             'horsepower_lower': horsepower_lower, 'horsepower_upper': horsepower_upper}
        )
        if self.request.user.has_perm('service.add_podborauto'):
            context.update({
                'html_title': 'Критерии подбора',
                'action_button': {'name': 'Добавить критерий', 'url': reverse('lk:podbor_add', args=(order.id,))}
            })
        return context

    def get_queryset(self):
        queryset = super(PPKListView, self).get_queryset()
        user = self.request.user
        queryset = queryset.filter(order__client__filials__in=user.filials.all()).distinct()
        queryset = queryset.filter(order__id=self.kwargs['order_id']).order_by('created')
        return queryset


# Список автомобилей по заказу
class OrderAutoListView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView, FilteredFieldContextMixin):
    permission_required = 'auto.view_auto'
    raise_exception = True
    template_name = 'order/order_car_list.html'
    model = Auto
    field_perms_model = Order
    serializer = AutoSerializer

    def get_context_data(self, **kwargs):
        context = super(OrderAutoListView, self).get_context_data(**kwargs)
        order = get_object_or_404(Order, id=self.kwargs['order_id'])
        context.update({'order': order})

        if self.request.user.has_perm('auto.add_auto'):
            context.update({
                'action_button': {'name': 'Добавить автомобиль', 'url': reverse('lk:order_auto_add', args=(order.id,))},
                'html_title': 'Список автомобилей'
            })
        return context

    def get_queryset(self):
        order = get_object_or_404(Order, id=self.kwargs['order_id'])
        queryset = order.cars.all()
        return queryset


# Список отчетов по заказу
class ReportListView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView, FilteredFieldContextMixin):
    permission_required = 'report.view_reportpodbor'
    raise_exception = True
    template_name = 'order/order_reports_list.html'
    model = ReportPodbor
    field_perms_model = Order
    serializer = ReportListSerialializer

    def get_context_data(self, **kwargs):
        context = super(ReportListView, self).get_context_data(**kwargs)
        order = get_object_or_404(Order, id=self.kwargs['order_id'])
        context.update({'order': order})
        if self.request.user.has_perm('report.add_reportpodbor'):
            context.update({
                'action_button': {'name': 'Добавить отчет',
                                  'url': reverse('report:add_report_for_order', args=(order.id,))},
                'html_title': 'Список отчетов'
            })
        return context

    def get_queryset(self):
        queryset = super(ReportListView, self).get_queryset()
        user = self.request.user
        order = get_object_or_404(Order, id=self.kwargs.get('order_id'))
        queryset = queryset.filter(order=order)
        queryset = queryset.filter(order__client__filials__in=user.filials.all())
        return queryset.distinct()


# Список комментариев по заказу
class CommentListView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView):
    permission_required = 'order.view_order'
    raise_exception = True
    template_name = 'order/order_reports_list.html'
    model = Comment
    serializer = CommentSerializer
    paginate_by = settings.PAGINATE_BY
    ordering = '-id'

    def get_queryset(self):
        queryset = super(CommentListView, self).get_queryset()
        if self.request.user.is_client:
            queryset = queryset.exclude(private=True)
        return queryset


@login_required
@permission_required('order.view_order', raise_exception=True)
def add_comment(request, order_id):
    if request.method == 'POST':
        comment_body = request.POST.get('body')
        if comment_body:
            order = get_object_or_404(Order, id=order_id)
            is_private = True if request.POST.get('private') and not request.user.is_client else False
            Comment.objects.create(order=order, private=is_private, author=request.user, body=comment_body)

    raise Http404


# Логи по заказу
class LogListView(LoginRequiredMixin, PermissionRequiredMixin, ImprovedListView, FilteredFieldContextMixin):
    permission_required = 'order.view_order'
    raise_exception = True
    template_name = 'order/order_log.html'
    model = FieldHistory
    field_perms_model = Order
    serializer = OrderFieldHistorySerializer
    paginate_by = settings.PAGINATE_BY

    def get_context_data(self, **kwargs):
        context = super(LogListView, self).get_context_data(**kwargs)
        order = get_object_or_404(Order, id=self.kwargs['order_id'])
        context.update({'order': order, 'html_title': 'Лог событий'})
        return context

    # удаляет несуществующие поля
    def prepare_history(self, queryset, model_fields):
        queryset = queryset.filter(field_name__in=model_fields)
        return queryset

    def get_logs(self, order_id):
        order = get_object_or_404(Order, id=order_id)
        related_reports = list(order.reportpodbor_set.values_list('id', flat=True))
        related_podbors = list(order.podborauto_set.values_list('id', flat=True))

        order_content_type = ContentType.objects.get_for_model(Order)
        report_content_type = ContentType.objects.get_for_model(ReportPodbor)
        podbor_content_type = ContentType.objects.get_for_model(PodborAuto)

        order_queryset = FieldHistory.objects.filter(object_id=order.id, content_type=order_content_type)
        report_queryset = FieldHistory.objects.filter(object_id__in=related_reports, content_type=report_content_type)
        podbor_queryset = FieldHistory.objects.filter(object_id__in=related_podbors, content_type=podbor_content_type)

        order_fields = [f.name for f in Order._meta.get_fields()]
        report_fields = [f.name for f in ReportPodbor._meta.get_fields()]
        podbor_fields = [f.name for f in PodborAuto._meta.get_fields()]

        queryset = self.prepare_history(order_queryset, order_fields) | \
                   self.prepare_history(report_queryset, report_fields) | \
                   self.prepare_history(podbor_queryset, podbor_fields)

        return queryset

    def get_queryset(self):
        queryset = self.get_logs(self.kwargs['order_id']).order_by('-date_created')
        return queryset

    def get(self, *args, **kwargs):
        if self.request.user.is_client:
            raise Http404
        return super(LogListView, self).get(*args, **kwargs)


# список авто для добавления к заказу
class CarsForOrder(AutoListView):
    paginate_by = settings.PAGINATE_BY
    search_fields = ['vin', 'source']
    found = False

    def get_queryset(self):
        order = get_object_or_404(Order, id=self.kwargs['order_id'])
        queryset = super(CarsForOrder, self).get_queryset()
        order_cars = order.cars.all()
        queryset = queryset.exclude(id__in=order_cars)
        return queryset


# Редактировать список автомобилей заказа
@login_required
@permission_required('order.change_order', raise_exception=True)
def change_auto_from(request, order_id):
    if request.POST:
        order = get_object_or_404(Order, id=order_id)
        if order.auto_from_salon:
            order.auto_from_salon = False
        else:
            order.auto_from_salon = True
        order.save()
        return JsonResponse(status=200, data={'result': "ok"})
    return JsonResponse(status=200, data={'result': "not found"})


        # Редактировать список автомобилей заказа
@login_required
@permission_required('order.change_order', raise_exception=True)
def order_set_auto(request, order_id):
    if request.POST:
        order = get_object_or_404(Order, id=order_id)
        if request.POST.get('cars'):
            cars = json.loads(request.POST.get('cars'))
            for car_id in cars:
                auto = get_object_or_404(Auto, id=car_id)
                reports = ReportPodbor.objects.filter(auto=auto)
                if not auto.order_set.filter(id=order_id).exists():
                    order.cars.add(auto)
                    order.reportpodbor_set.add(*reports)
                    site = get_current_site(None).domain
                    if order.client.email:
                        for report in reports:
                            pdf = report.render_pdf()
                            send_simple_message_mailgun(
                                'В Ваш заказ добавлен новый отчет',
                                'Здравствуйте, ' + str(order.client.first_name) + '\n'
                                'По Вашему заказу: ' + str(order_id) + ' эксперт добавил новый отчет.\n'
                                'Ознакомится с отчетом Вы может по ссылке: ' + str(site) + '/report/view/' + str(order_id) + '\n'
                                'PDF версия отчета добавлена к этому во вложении;\n'
                                'Спасибо, что выбрали нас!\n\n',
                                settings.DEFAULT_FROM_EMAIL,
                                order.client.email,
                                pdf=pdf,
                            )

                    if order.order_type in order_const.INWORKFORREPORT:
                        order.status = order_const.DONE
                        order.status_closing = True
                        order.save()
                        add_status(order, 'change-status-crm', 'order')
                else:
                    order.cars.remove(auto)
                    order.reportpodbor_set.remove(*reports)

        else:
            form = AutoForm(request.POST)
            if form.is_valid():
                car = form.save()
                order.cars.add(car)
                return JsonResponse(status=200, data={'auto_id': car.id, 'order_id': order.pk, 'redirect': reverse('lk:order_detail', args=(order.id, ))})
        return JsonResponse(status=200, data={'redirect': reverse('lk:order_detail', args=(order.id, ))})

    form = AutoForm()
    return render(request, 'order/order_car_form.html', {'html_title': 'Добавление автомобиля', 'form': form, 'order_id': order_id})


# добавление авто к заказу по ссылке
@login_required
@permission_required('order.change_order', raise_exception=True)
def add_auto_from_auto_ru(request, order_id):
    if request.POST:
        url = request.POST.get('url')
        from auto.utils import get_data_from_auto_ru
        auto_ru_data = get_data_from_auto_ru(url)
        if auto_ru_data['status'] == 'error':
            return HttpResponse('', status=500)
        else:
            order = get_object_or_404(Order, id=order_id)
            auto = get_object_or_404(Auto, id=auto_ru_data.get('data').get('id'))
            order.cars.add(auto)
            redirect_to = reverse('lk:auto_list', args=(order_id,))
            return redirect(redirect_to)

    raise Http404


# Удалить автомобиль из заказа
@login_required
@permission_required('order.change_order', raise_exception=True)
def order_auto_delete(request, order_id, auto_id):
    if request.POST:
        order = get_object_or_404(Order, id=order_id)
        auto = get_object_or_404(Auto, id=auto_id)
        reports = ReportPodbor.objects.filter(auto=auto)
        order.cars.remove(auto)
        order.reportpodbor_set.remove(*reports)
        return HttpResponse(200)

    raise Http404


# Добавить заказ
class OrderCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView, FilteredFieldsMixin):
    permission_required = 'order.add_order'
    raise_exception = True
    model = Order
    form_class = OrderEditForm
    template_name = 'order/order_edit.html'
    action = 'create'

    def get_success_url(self):
        return reverse("lk:order_edit", args=(self.object.id, ))

    def form_valid(self, form):
        super(OrderCreateView, self).form_valid(form)
        return JsonResponse(status=200, data={'redirect': reverse("lk:order_detail", args=(self.object.id, ))})

    def form_invalid(self, form):

        super(OrderCreateView, self).form_invalid(form)
        return JsonResponse(status=200, data={'errors': form.errors})

    def post(self, request, *args, **kwargs):
        self.object = None
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    def get_context_data(self, **kwargs):
        context = super(OrderCreateView, self).get_context_data(**kwargs)
        filials = self.request.user.filials.all()

        context.update({
            'html_title': 'Добавление заказа',
            'filials': filials,
        })
        return context


# Редактировать заказ
class OrderUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView, FilteredFieldsMixin):
    permission_required = 'order.change_order'
    raise_exception = True
    model = Order
    form_class = OrderEditForm
    template_name = 'order/order_edit.html'
    action = 'update'

    def get_success_url(self):
        return reverse("lk:order_edit", args=(self.object.id, ))

    def form_valid(self, form):
        action = form.save(commit=False)
        action.submit_from = 'LK'
        form.save_m2m()
        try:
            super(OrderUpdateView, self).form_valid(form)
        except OrderExcepiton as e:
            status = ""
            for status_id, stat in order_const.STATUS:
                if status_id == self.object.status:
                    status = stat
                    break

            message = "Заказ не может быть закрыт в статусе {} . Для закрытия заказа необходимо установить статус Выполнен, Отказ или Возврат.".format(status)
            return JsonResponse(status=200, data={'errors': {"status": [message]}})
        return JsonResponse(status=200, data={'redirect': reverse("lk:order_detail", args=(self.object.id, ))})

    def form_invalid(self, form):
        super(OrderUpdateView, self).form_invalid(form)
        return JsonResponse(status=200, data={'errors': form.errors})

    def get_form(self, *args, **kwargs):
        form = super(OrderUpdateView, self).get_form(*args, **kwargs)
        if self.request.user.is_expert:
            try:
                for st in form.fields['status'].choices:
                    if st[1] == 'выполнен':
                        form.fields['status'].choices.remove(st)
            except KeyError as ex:
                logger.error(ex)
        return form

    def get_context_data(self, **kwargs):
        context = super(OrderUpdateView, self).get_context_data(**kwargs)
        order = OrderSerializer(self.get_object()).data
        filials = self.request.user.filials.all()
        context.update({
            'html_title': 'Редактирование заказа',
            'order': order,
            'filials': filials
        })
        return context

    def get_object(self, **kwargs):
        order = get_object_or_404(Order, id=self.kwargs.get('order_id'))
        return order


# удалить заказ
@login_required
@permission_required('order.delete_order', raise_exception=True)
def order_delete(request, order_id):
    if request.method == 'POST':
        order = get_object_or_404(Order, pk=order_id)
        order.delete(force_policy=SOFT_DELETE_CASCADE)
        return HttpResponse(status=200)
    raise Http404

# закрыть заказ
@login_required
@permission_required('order.change_order', raise_exception=True)
def order_close(request, order_id):
    if request.method == 'POST':
        order = get_object_or_404(Order, pk=order_id)
        order.status_closing = True
        order.status = order_const.DONE
        order.save()
        return HttpResponse(status=200)
    raise Http404

# удалить подбор под ключ
@login_required
@permission_required('service.delete_podborauto', raise_exception=True)
def podbor_delete(request, ppk_id):
    if request.method == 'POST':
        ppk = get_object_or_404(PodborAuto, pk=ppk_id)
        ppk.delete(force_policy=SOFT_DELETE_CASCADE)
        return HttpResponse(status=200)
    raise Http404

# удалить подбор под ключ
@login_required
@permission_required('order.change_order', raise_exception=True)
def report_remove_order(request, report_id, order_id):
    if request.method == 'POST':
        report = ReportPodbor.objects.get(pk=report_id)
        report.order.remove(order_id)
        order = Order.objects.get(pk=order_id)
        order.cars.remove(report.auto.pk)

        return HttpResponse(status=200)
    raise Http404


# Добавить критерий
class PpkCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView, FilteredFieldsMixin):
    permission_required = 'service.add_podborauto'
    raise_exception = True
    model = PodborAuto
    form_class = PodborAddForm
    template_name = 'order/ppk_edit.html'
    action = 'create'

    def get_success_url(self):
        return reverse("lk:podbor_edit", kwargs={"ppk_id": self.object.id})

    def form_valid(self, form):
        super(PpkCreateView, self).form_valid(form)
        return JsonResponse(status=200, data={'redirect': reverse("lk:ppk_list", args=(self.object.order.id, ))})

    def form_invalid(self, form):
        super(PpkCreateView, self).form_invalid(form)
        return JsonResponse(status=200, data={'errors': form.errors})

    def get_form(self, *args, **kwargs):
        form = super(PpkCreateView, self).get_form(*args, **kwargs)
        form.fields['order'].initial = get_object_or_404(Order, id=self.kwargs.get('order_id'))
        if self.request.method == 'POST':
            form.fields['generation'].queryset = Generation.objects.all()
            form.fields['model_auto'].queryset = ModelAuto.objects.all()
        return form

    def get_context_data(self, **kwargs):
        context = super(PpkCreateView, self).get_context_data(**kwargs)
        years = list(Year.objects.filter(year__gte=2005).order_by('year').values_list('year', flat=True))
        context.update({
            'html_title': 'Добавление критерия подбора',
            'years': json.dumps(years),
            'drive_types': DRIVE_TYPE,
            'salon_auto': SALON_AUTO,
            'engine_types': ENGINE_TYPE,
            'transmission_types': TRANSMISSION_TYPE,
            'horsepower': json.dumps([75, 100, 150, 200, 250]),
            'engine_capacities': json.dumps([value for value, text in ENGINE_CAPACITY]),
            'colors_auto': COLOR_OF_AUTO,
            'colors_salon': COLOR_SALON_TYPE,
            'mileage': range(20000, 210000, 10000)
        })

        return context

class PpkCopyView(PpkCreateView):
    permission_required = 'service.add_podborauto'
    raise_exception = True
    model = PodborAuto
    form_class = PodborAddForm
    template_name = 'order/ppk_edit.html'
    action = 'create'
    def get_initial(self):
        super(PpkCopyView, self).get_initial()
        self.object = self.get_object()
        self.object.pk = None
        return self.initial

    def get_object(self, queryset=None):
        return get_object_or_404(PodborAuto, id=self.kwargs.get('ppk_id'))

# Редактировать критерий
class PpkEditView(PpkCreateView, UpdateView):
    permission_required = 'service.change_podborauto'
    raise_exception = True
    model = PodborAuto
    form_class = PodborAddForm
    template_name = 'order/ppk_edit.html'
    action = 'update'
    success_url = "success"

    def get_form(self, *args, **kwargs):
        form = super(PpkCreateView, self).get_form(*args, **kwargs)
        if self.request.method == 'POST':
            self.form_class(data=self.request.POST)
            form.fields['generation'].queryset = Generation.objects.all()
            form.fields['model_auto'].queryset = ModelAuto.objects.all()
        return form

    def get_context_data(self, **kwargs):
        context = super(PpkEditView, self).get_context_data(**kwargs)
        context.update({'html_title': 'Редактирование критерия подбора'})
        return context

    def form_valid(self, form):
        super(PpkCreateView, self).form_valid(form)
        return JsonResponse(status=200, data={'redirect': reverse("lk:order_detail", kwargs={'order_id': self.object.order_id})})

    def get_object(self, queryset=None):
        return get_object_or_404(PodborAuto, id=self.kwargs.get('ppk_id'))

@login_required
def get_order_done_popup_info(request):
    if not request.is_ajax():
        raise Http404

    orders = Order.objects.filter( Q(is_read_by_client = False, client = request.user.id ) )[:1]
    data = {}
    for order in orders:
        data = order.get_order_done_info()
        order.is_read_by_client=True
        order.save()
    return JsonResponse(data)
