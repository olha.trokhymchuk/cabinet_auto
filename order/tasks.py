from autopodbor.celery import app
from django.conf import settings

from .workers.export import load_orders_on_mobile
from .workers.send_status_to_crm import  send_statuses
from .workers.auto_matching.auto_matching import autoMatching

@app.task
def load_order_on_mobile():
	load_orders_on_mobile()

@app.task
def send_statuses_to_crme():
	if not settings.TO_CRM:
		return False
	send_statuses()

@app.task
def set_ppk_matching():
	autoMatching()
