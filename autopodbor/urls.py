from django.conf.urls import url, include, handler403, handler404, handler500
from django.contrib import admin
from django.conf.urls.static import static, serve
from django.conf import settings
from django.views.generic import TemplateView
from lk.views import permission_denied, error404
from core.views import OsagoView, AutoHelpView, ServiceHelpView, CarCheckView, NewOrderEmailView, WriteToTheHead, admin_view
from autopodbor import utils
import environ

admin.site.admin_view = admin_view


env = environ.Env()

handler403 = permission_denied
handler404 = error404

urlpatterns = [
    url(r'^', include('lk.urls')),
    url(r'^admin/', include('custom_admin.urls', namespace="custom_admin")),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/', include('allauth.urls')),

    # aggregator/client_app
    url(r'^aggregator_api/', include('aggregator.urls')),
    url(r'^client_app_api/', include('client_app.urls')),
    url('api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^auto/', include('auto.urls')),
    url(r'^user/', include('core.urls')),
    url(r'^report/', include('report.urls')),
    url(r'^google_photo/', include('google_photo.urls')),
    url(r'^stat/', include('statistika.urls')),
    url(r'^notifications/', include('notifications.urls')),
    url(r'^message/', include('message.urls')),
    url(r'^developers/', TemplateView.as_view(template_name='developers.html')),
    url(r'^api/', include("integration.urls")),
    url(r'^osago/$', OsagoView.as_view(), name='osago'),
    url(r'^autohelp/$', AutoHelpView.as_view(), name='autohelp'),
    url(r'^servicehelp/$', ServiceHelpView.as_view(), name='servicehelp'),
    url(r'^carcheck/$', CarCheckView.as_view(), name='carcheck'),
    url(r'^new_order/$', NewOrderEmailView.as_view(), name='new_order'),
    url(r'^write_to_the_head/$', WriteToTheHead.as_view(), name='writetothehead'),
    url(r'^fail-payment/$',
        TemplateView.as_view(template_name='fail.html'), name='payment_fail'),
    url(r'^success-payment/$',
        TemplateView.as_view(template_name='success.html'), name='payment_success'),
    url(r'^media/(?P<path>.*)$', utils.serve, {'document_root': settings.MEDIA_ROOT}),
]
# ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if env('DOCKER', default=False):
    urlpatterns += url(r'^static/(?P<path>.*)$', serve,
                       {'document_root': settings.STATIC_ROOT}),
