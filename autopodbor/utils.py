"""
Views and functions for serving static files.
"""
import mimetypes
import posixpath
from functools import update_wrapper
import requests
import re
import os
from pathlib import Path

from django.contrib.admin import AdminSite
from django.http import (
    FileResponse, Http404, HttpResponse, HttpResponseNotModified,
)
from django.template import Context, Engine, TemplateDoesNotExist, loader
from django.utils._os import safe_join
from django.utils.http import http_date, parse_http_date
from django.utils.translation import gettext as _, gettext_lazy
import io
from PIL import Image
from django.conf import settings


def send_simple_message_mailgun(subject,message,from_email, to_email, pdf=None):
    if not pdf:
        attachment = None
    else:
        attachment = [("attachment", ('report.pdf', pdf))]
    return requests.post(
        "https://api.mailgun.net/v3/"+settings.MAILGUN_WEB_SITE+"/messages",
        auth=("api", settings.MAILGUN_PRIVATE_API_KEY),
        files=attachment,
        data={"from": from_email,
              "to": to_email,
              "subject": subject,
              "text": message},
    )


def resize_photo(im, size):
    im.thumbnail((size, size), Image.ANTIALIAS)
    buf = io.BytesIO()
    im.save(buf, format='JPEG')
    byte_im = buf.getvalue()
    return byte_im


def get_watermark(image_path, from_google=False):
    logo_path = settings.MEDIA_ROOT + '/watermarks/logo.png'
    logo = Image.open(logo_path)
    image_car = Image.open(image_path)
    #  resize logo
    if image_car:
        coefficient = 0.25
        image_car_width, image_car_height = image_car.size
        logo_width, logo_height = logo.size
        resize_logo_width = image_car_width * coefficient
        resize_logo_height = (resize_logo_width * logo_height) / logo_width
        logo = logo.resize((int(resize_logo_width), int(
            resize_logo_height)), Image.ANTIALIAS)
    image_copy = image_car.copy()
    position = ((image_copy.width - logo.width),
                (image_copy.height - logo.height))
    image_copy.paste(logo, position, logo)
    if from_google:
        image_copy = resize_photo(image_copy, 3500)
    return image_copy


def serve(request, path, document_root=None, show_indexes=False):
    """
    Custom rewrite function, take photo from MEDIA and add watermark
    """
    type_file = os.path.splitext(path)[1]
    path = posixpath.normpath(path).lstrip('/')
    if type_file != ".pdf":
        logo_path = settings.MEDIA_ROOT + '/watermarks/logo.png'
        print(logo_path)
        image_path = Path(safe_join(document_root, path))
        image_copy = get_watermark(image_path)
    fullpath = Path(safe_join(document_root, path))
    if fullpath.is_dir():
        if show_indexes:
            return directory_index(path, fullpath)
        raise Http404(_("Directory indexes are not allowed here."))
    if not fullpath.exists():
        raise Http404(_('"%(path)s" does not exist') % {'path': fullpath})
    # Respect the If-Modified-Since header.
    statobj = fullpath.stat()
    content_type, encoding = mimetypes.guess_type(str(fullpath))
    content_type = content_type or 'application/octet-stream'
    if type_file != ".pdf":
        response = HttpResponse(content_type=content_type)
        image_copy.save(response, "JPEG")
    else:
        response = FileResponse(fullpath.open('rb'), content_type=content_type)
    response["Last-Modified"] = http_date(statobj.st_mtime)
    if encoding:
        response["Content-Encoding"] = encoding
    return response


DEFAULT_DIRECTORY_INDEX_TEMPLATE = """
{% load i18n %}
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta http-equiv="Content-Language" content="en-us">
    <meta name="robots" content="NONE,NOARCHIVE">
    <title>{% blocktrans %}Index of {{ directory }}{% endblocktrans %}</title>
  </head>
  <body>
    <h1>{% blocktrans %}Index of {{ directory }}{% endblocktrans %}</h1>
    <ul>
      {% if directory != "/" %}
      <li><a href="../">../</a></li>
      {% endif %}
      {% for f in file_list %}
      <li><a href="{{ f|urlencode }}">{{ f }}</a></li>
      {% endfor %}
    </ul>
  </body>
</html>
"""
template_translatable = gettext_lazy("Index of %(directory)s")


def directory_index(path, fullpath):
    try:
        t = loader.select_template([
            'static/directory_index.html',
            'static/directory_index',
        ])
    except TemplateDoesNotExist:
        t = Engine(libraries={'i18n': 'django.templatetags.i18n'}).from_string(
            DEFAULT_DIRECTORY_INDEX_TEMPLATE)
        c = Context()
    else:
        c = {}
    files = []
    for f in fullpath.iterdir():
        if not f.name.startswith('.'):
            url = str(f.relative_to(fullpath))
            if f.is_dir():
                url += '/'
            files.append(url)
    c.update({
        'directory': path + '/',
        'file_list': files,
    })
    return HttpResponse(t.render(c))


def wrap(view, cacheable=False):
    def wrapper(*args, **kwargs):
        return AdminSite.admin_view(view, cacheable)(*args, **kwargs)

    wrapper.admin_site = AdminSite
    return update_wrapper(wrapper, view)