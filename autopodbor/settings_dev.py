from .settings import *

TOKEN = "851335790:AAGBAq9xOmw5J8z2U89aKgpP4btWwwgzPv8"
# CELERY_BROKER_URL = 'redis://redis:6379'
# CELERY_RESULT_BACKEND = 'redis://redis:6379'
DEBUG = True
COMPRESS_OFFLINE = False
COMPRESS_ENABLED = False

NOT_NOTIFICATION = True
URLCRM = "http://avto.rastafrj.beget.tech"
TO_CRM = False
EXPERTTOCRM = False


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'autopodbor',
        'USER': 'autopodbor',
        'PASSWORD': env('DJANGO_PG_PASSWORD', default='2l5g423dp5cjteO'),
        'HOST': env('DJANGO_PG_HOST', default='127.0.0.1'),
        'PORT': '5432',
    }
}


TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, "templates")],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.media',
                'lk.context_processors.common'
            ],
        },
    },
]

# TEMPLATES[0]['OPTIONS']['context_processors'].append('django.template.context_processors.media') # Думаю так не стоит

# ftp connect to upload report
UP_FTP_URL = "144.76.106.105"
UP_FTP_USER = "test_reports"
UP_FTP_PSW = "Lrp1Ab2lApB2PxIW"
UP_FTP_PORT = 21

# credentials to send report api
REPORT_API_USERNAME = 'autopodbor'
REPORT_API_PASSWORD = '2l5g423dp5cjteO'

# folder id to google drive backup
FOLDER_ID = '1SURMbv2XLr66WE5ovdm9IwjRlVuPGrgu'

# Google photo tokens folder DEV
GOOGLE_PHOTO_TOKENS_FOLDER = 'google_photo_library_tokens/'

# Google drive tokens folder DEV
GOOGLE_DRIVE_TOKENS_FOLDER = 'google_drive_library_tokens/'

# Stop run command before the upload photos run again (1 minutes 30 sec)
SLEEP_BEFORE_UPLOAD = 90

# Stop run command if not found photos to upload google api (1 hour)
SLEEP_AFTER_UPLOAD = 3600