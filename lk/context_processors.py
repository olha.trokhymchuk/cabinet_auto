from django.conf import settings
def common(request, **kwargs):
    instructions = [
    				{
    					"link": settings.OPERINST,
    					"name": "операторa",
    					"group_name": "Оператор"
    				},
    				{
    					"link": settings.LIEDINST,
    					"name": "руководителя",
    					"group_name": "Руководитель"
    				},
    				{
    					"link": settings.EXPERTINST,
    					"name": "эксперта",
    					"group_name": "Эксперт"
    				},
    				{
    					"link": settings.BUCHINST,
    					"name": "бухгалтера",
    					"group_name": "Бухгалтер"
    				},
    				{
    					"link": settings.COMANGINST,
    					"name": "контент-менеджера",
    					"group_name": "Контент-менеджер"
    				},
    				{
                        "link": settings.ADMININST,
                        "name": "администратора",
                        "group_name": "Администратор"
                    },
                    {
    					"link": settings.CLIENT,
    					"name": "клиента",
    					"group_name": "Клиент"
    				}
    			]
    return {
        'INSTRUCTIONS': instructions,
		'RE_CAPTCHA_SITE_KEY' : settings.RE_CAPTCHA_SITE_KEY
    }