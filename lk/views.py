from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.template import Context, loader
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.base import TemplateView
from order.models import Order

# Личный кабинет - главная страница
from django.urls import reverse


@login_required
def lk(request):
    redirect_to = reverse('lk:order_list')
    check_count_orders_by_id = Order.objects.filter(client=request.user.id).all()
    if request.user.is_leader:
        redirect_to = reverse('user:filial_detail', kwargs={'filial_id': request.user.filials.all().first().pk}) + "?tab=user_list_filial"
    if request.user.is_client:
        if check_count_orders_by_id.count() == 1:
            redirect_to = reverse('lk:order_detail', kwargs={'order_id': check_count_orders_by_id[0]})
        else:
            redirect_to = reverse('lk:order_list_in_work')
    return HttpResponseRedirect(redirect_to)

def permission_denied(request):
    redirect_to = '/accounts/login/'
    return HttpResponseRedirect(redirect_to)

def error404(request):
    template = loader.get_template('404.htm')

    context = {
    }
    return HttpResponse(template.render(context, request))
  
#Страничка поиска
class GlobalSearch(LoginRequiredMixin, TemplateView):
    template_name = 'search/search.html'