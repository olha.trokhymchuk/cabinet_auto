from django.db import models


class Statuses(models.Model):
    name = models.CharField('Стутус', max_length=20, blank=True, null=True, default=None)

    class Meta:
        verbose_name = "Стутус"
        verbose_name_plural = "Стутусы"
        ordering = ["name"]

    def __str__(self):
        return self.name

