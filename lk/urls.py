from django.conf.urls import url, include
from .views import *
from order.views import *
from core.api.views import order_to_issue

app_name = 'lk'

urlpatterns = [
    url(r'^$', lk, name='home'),
    url(r'^order/', include('order.urls')),
    url(r'^order/list_search/$', OrderListView.as_view(), name='list_search'),
    url(r'^order/list/$', OrderListSelectKeyView.as_view(), name='order_list'),
    url(r'^order/order_list_selection/$', OrderListSelectKeyView.as_view(), name='order_list_selection'),
    url(r'^order/order_list_outside_diagnose/$', OrderListOutsideDiagnose.as_view(), name='order_list_outside_diagnose'),
    url(r'^order/order_list_expert_day/$', OrderListExpertDayView.as_view(), name='order_list_expert_day'),
    url(r'^order/order_list_other/$', OrderListOtherView.as_view(), name='order_list_other'),
    url(r'^order/order_list_in_work/$', OrderListInWorkView.as_view(), name='order_list_in_work'),
    url(r'^order/order_list_closed/$', OrderListClosedView.as_view(), name='order_list_closed'),
    url(r'^order/detail/(?P<order_id>\d+)/$', OrderDetailView.as_view(), name='order_detail'),
    url(r'^order/edit/(?P<order_id>\d+)/$', OrderUpdateView.as_view(), name='order_edit'),
    url(r'^order/add/$', OrderCreateView.as_view(), name='order_add'),
    url(r'^order/delete/(?P<order_id>\d+)/$', order_delete, name='order_delete'),
    url(r'^order/close/(?P<order_id>\d+)/$', order_close, name='order_close'),
    url(r'^order/filter/$', get_order_filter, name='get_order_filter'),
    url(r'^order/stats/(?P<order_id>\d+)/$', get_order_stats, name='get_order_stats'),
    url(r'^order/podbor_list/(?P<order_id>\d+)/$', PPKListView.as_view(), name='ppk_list'),
    # url(r'^order/cars/(?P<order_id>\d+)/$', OrderAutoListView.as_view(), name='auto_list'),
    url(r'^order/report-list/(?P<order_id>\d+)/$', ReportListView.as_view(), name='report_list'),
    # url(r'^order/log/(?P<order_id>\d+)/$', LogListView.as_view(), name='order_log'),
    url(r'^order/add-comment/(?P<order_id>\d+)/$', add_comment, name='add_comment'),
    url(r'^order/add-auto/(?P<order_id>\d+)/$', order_set_auto, name='order_auto_add'),
    url(r'^order/add-auto-from-auto-ru/(?P<order_id>\d+)/$', add_auto_from_auto_ru, name='add_auto_from_auto_ru'),
    url(r'^order/cars-for-order/(?P<order_id>\d+)/$', CarsForOrder.as_view(), name='cars_for_order'),
    url(r'^order/delete-auto/(?P<order_id>\d+)/(?P<auto_id>\d+)/$', order_auto_delete, name='order_auto_delete'),
    url(r'^order/report-remove-order/(?P<report_id>\d+)/(?P<order_id>\d+)/$', report_remove_order, name='report_remove_order'),
    url(r'^order/change_auto_from/(?P<order_id>\d+)/$', change_auto_from, name='change_auto_from'),
    url(r'^podbor-add/(?P<order_id>\d+)/$', PpkCreateView.as_view(), name='podbor_add'),
    url(r'^podbor-copy/(?P<order_id>\d+)/(?P<ppk_id>\d+)/$', PpkCopyView.as_view(), name='podbor_copy'),
    url(r'^podbor-edit/(?P<ppk_id>\d+)/$', PpkEditView.as_view(), name='podbor_edit'),
    url(r'^podbor-delete/(?P<ppk_id>\d+)/$', podbor_delete, name='podbor_delete'),
    url(r'^search/$', GlobalSearch.as_view(), name='global_search'),
    url(r'^to_issue/$', order_to_issue, name='order_to_issue')
]