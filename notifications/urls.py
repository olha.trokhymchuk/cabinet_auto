from django.conf.urls import url
from .views import notification_list, sms_callback, all_notification

app_name = 'notifications'

urlpatterns = [
    url(r'^$', notification_list, name='notifications'),
    url(r'^sms-callback', sms_callback, name='sms_callback'),
    url(r'^all', all_notification, name='all_notification')
]
