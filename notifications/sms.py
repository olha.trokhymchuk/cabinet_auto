import logging
import json, xmltodict
from urllib.parse import quote
from urllib.request import urlopen
from django.conf import settings

from notifications.models import Sms

logger = logging.getLogger(__name__)


service_codes = {
    100: "Сообщение принято к отправке",
    101: "Сообщение передается оператору",
    102: "Сообщение отправлено (в пути)",
    103: "Сообщение доставлено",
    104: "Не может быть доставлено: время жизни истекло",
    105: "Не может быть доставлено: удалено оператором",
    106: "Не может быть доставлено: сбой в телефоне",
    107: "Не может быть доставлено: неизвестная причина",
    108: "Не может быть доставлено: отклонено",
    110: "Сообщение прочитано",
    200: "Неправильный api_id",
    201: "Не хватает средств на лицевом счету",
    202: "Неправильно указан получатель, либо на него нет маршрута",
    203: "Нет текста сообщения",
    204: "Имя отправителя не согласовано с администрацией",
    205: "Сообщение слишком длинное (превышает 8 СМС)",
    206: "Будет превышен или уже превышен дневной лимит на отправку сообщений",
    207: "На этот номер нет маршрута для доставки сообщений",
    208: "Параметр time указан неправильно",
    209: "Вы добавили этот номер (или один из номеров) в стоп-лист",
    210: "Используется GET, где необходимо использовать POST",
    211: "Метод не найден",
    212: "Текст сообщения необходимо передать в кодировке UTF-8",
    220: "Сервис временно недоступен, попробуйте чуть позже.",
    230: "Превышен общий лимит количества сообщений на этот номер в день",
    231: "Превышен лимит одинаковых сообщений на этот номер в минуту",
    232: "Превышен лимит одинаковых сообщений на этот номер в день",
    300: "Неправильный token (возможно истек срок действия, либо ваш IP изменился)",
    301: "Неправильный пароль, либо пользователь не найден",
    500: "Ошибка на сервере. Повторите запрос.",
    901: "Callback: URL неверный (не начинается на http://)",
    902: "Callback: Обработчик не найден (возможно был удален ранее)"
}


def send_sms(to, msg):
    url = 'https://www.mcommunicator.ru/m2m/m2m_api.asmx/SendMessage?msid=%s&message=%s&naming=%s&login=%s&password=%s' % (to, quote(msg), settings.SMS_API_NAMING, settings.SMS_API_LOGIN,settings.SMS_API_PASS)
    res = urlopen(url)
    logger.error(to);
    logger.error("send_sms");
    response_data = {'status': 'ERROR', 'status_code': 500}
    try:
        res = res.read()
        logger.error(res)
        o = xmltodict.parse(res)
        response_data = o['long']['#text']
    except json.decoder.JSONDecodeError as ex:
        logger.info(ex)
        logger.info('-----------------------------------')
        logger.info(res.read())
        logger.info('-----------------------------------')
        response_data.update({'status_text':ex})
        logger.info('-----------------------------------')
        logger.info('not send ' + to)
        logger.info('-----------------------------------')
    else:
        logger.error(response_data)
        logger.error('-----------------------------------')
        logger.error('send ' + to)
        logger.error('-----------------------------------')
    return response_data


def get_balance():
    url = 'https://sms.ru/my/balance?api_id=%s&json=1' % settings.SMS_API_ID
    res = urlopen(url)
    response_data = json.loads(res.read().decode())
    logger.info(response_data)
    if response_data['status'] == 'OK' and 'balance' in response_data:
        return response_data['balance']
    return None


def send(sms_to, sms_body, **kwargs):
    if not settings.SMS_API_ID or not sms_body or not sms_to:
        return False

    response = send_sms(sms_to, sms_body)
    logger.error(response)
    sms = Sms.objects.get(pk=kwargs['sms_pk'])

    if not response:
        logger.info('-----------------------------------')
        logger.info(response)
        logger.info('-----------------------------------')
        sms.status_code = 500
        sms.status_text = response
        sms.save()
        return False

    sms.message_id = response
    sms.save()
    return True
