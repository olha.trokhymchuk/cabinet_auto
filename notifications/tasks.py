from antigate import AntiGate
from django.contrib.auth import get_user_model
from autopodbor.celery import app
from notifications.models import Notification
from notifications import sms
from notifications.utils import send_notification
from django.conf import settings


def send_balance_to_admins(service, balance):
    data = {
        'service': service,
        'balance': balance
    }

    admins = [get_user_model().objects.get(email=email) for fullname, email in settings.ADMINS]
    send_notification(admins, Notification.BALANCE, data=data, channels=['email'])


@app.task
def check_balance():
    sms_balance = sms.get_balance()
    if sms_balance and sms_balance < settings.SMS_BALANCE_FOR_NOTIFYING:
        send_balance_to_admins('sms.ru', sms_balance)

    antigate_balance = float(AntiGate(settings.ANTICAPTCHA_API).balance())
    if antigate_balance and antigate_balance < settings.ANTICAPTCHA_BALANCE_FOR_NOTIFYING:
        send_balance_to_admins('anticaptcha', antigate_balance)
