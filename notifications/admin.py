from django.contrib import admin
from .models import Notification, LK, Email, Sms


class NotificationAdmin(admin.ModelAdmin):
    list_display = ('id', 'recipient', 'action', 'data')
    raw_id_fields = ('recipient',)
    list_filter = ('action', )


class EmailAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_recipient', 'get_action', 'notification', 'delivered', 'created')
    raw_id_fields = ('notification',)
    list_filter = ('delivered', )

    def get_recipient(self, obj):
        return obj.notification.recipient

    def get_action(self, obj):
        return obj.notification.get_action_display()


class LKAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_recipient', 'get_action', 'notification', 'viewed', 'created')
    raw_id_fields = ('notification',)
    list_filter = ('viewed', )

    def get_recipient(self, obj):
        return obj.notification.recipient

    def get_action(self, obj):
        return obj.notification.get_action_display()


class SmsAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_recipient', 'get_action', 'notification', 'delivered', 'created', 'status_code', 'status_text', 'message_id')
    raw_id_fields = ('notification',)
    list_filter = ('delivered', )

    def get_recipient(self, obj):
        return obj.notification.recipient

    def get_action(self, obj):
        return obj.notification.get_action_display()


admin.site.register(Notification, NotificationAdmin)
admin.site.register(Sms, SmsAdmin)
admin.site.register(LK, LKAdmin)
admin.site.register(Email, EmailAdmin)
