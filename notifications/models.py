import logging

from dbmail import send_db_mail
from dbmail import send_db_sms
from dbmail.models import MailTemplate
from django.conf import settings
from django.contrib.postgres.fields import JSONField
from django.core import signing
from django.db import models
from dbmail.backends.mail import Sender as SenderBase
from model_utils.models import TimeStampedModel


logger = logging.getLogger(__name__)


class Notification(TimeStampedModel):
    REGISTRATION = 'reg'
    RESET_PASSWORD = 'reset'
    NEW_ORDER = 'new_ord'
    STATUS_UPDATE = 'stat_upd'
    NEW_REPORT = 'new_rep'
    ADVICE_REPORT = 'adv_rep'
    NOT_ADVICE_REPORT = 'nadv_rep'
    CHECK_ORDER = 'check_ord'
    PAYMENT = 'payment'
    COMPLETE_ORDER = 'comp_ord'
    CLOSE_ORDER = 'close_ord'
    TRANSF_ORDER = 'trans_ord'
    DONE_ORDER = 'done_ord'
    ERROR = 'error'
    BALANCE = 'balance'
    COMMENT = 'comment'
    ADD_TO_LK = 'add_to_lk'
    REPORT_MOB = 'report_mob'
    BUH_OTKAZ = 'buh-otkaz'
    BUH_OTKAZ_LK = 'b-otkaz_lk'
    MONITOR = 'monitor'
    ACTION_CHOICES = (
        (REGISTRATION, 'Регистрация'),
        (RESET_PASSWORD, 'Восстановление пароля'),
        (NEW_ORDER, 'Создание заказа'),
        (STATUS_UPDATE, 'Смена статуса работы'),
        (NEW_REPORT, 'Появление нового отчета по заказу'),
        (ADVICE_REPORT, 'Рекомендованный отчет оповещение руководителю'),
        (NOT_ADVICE_REPORT, 'Не рекомендованный отчет оповещение руководителю'),
        (CHECK_ORDER, 'Перепроверка заказа'),
        (PAYMENT, 'Поступление оплаты по заказу'),
        (TRANSF_ORDER, 'Готов к выдаче'),
        (DONE_ORDER, 'Заказ выполнен'),
        (COMPLETE_ORDER, 'Сдача заказа'),
        (CLOSE_ORDER, 'Закрытие заказа'),
        (ERROR, 'Ошибки в системе'),
        (BALANCE, 'Текущий баланс'),
        (COMMENT, 'Комментарий к заказу'),
        (ADD_TO_LK, 'добавление пользователя в лк'),
        (REPORT_MOB, 'отчет с телефона'),
        (BUH_OTKAZ, 'Зактрытие заказа'),
        (MONITOR, 'Мониторинг Системы'),
    )
    recipient = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name='Получатель')
    action = models.CharField(max_length=10, choices=ACTION_CHOICES)
    data = JSONField(default=dict)
    viewed = models.BooleanField(default=False)
    pub_date = models.DateField('Дата просмотра', blank=True, null=True)

    def __str__(self):
        return "{action} {data}".format(action=self.action, data=self.data)

    class Meta(TimeStampedModel.Meta):
        db_table = 'notification'


class Email(TimeStampedModel):
    # Для нахождения нужного шаблона в дб мейлере. Ищем по слагу
    TEMPLATES = {

    }
    notification = models.ForeignKey(Notification, verbose_name='Уведомление')
    delivered = models.DateTimeField(blank=True, null=True)

    class Meta(TimeStampedModel.Meta):
        db_table = 'notification_email'

    def send_to_recipient(self):
        template_slug = self.template_slug
        email = self.notification.recipient.email
        data = signing.loads(self.notification.data)
        if email:
            try:
                logger.error('template_slug ' + str(template_slug))
                logger.error('email ' + str(email))
                send_db_mail(template_slug, email, data)
            except MailTemplate.DoesNotExist as ex:
                logger.error(ex)
                logger.error('template_slug ' + str(template_slug))

    @property
    def template_slug(self):
        action_to_slug = {
            Notification.REGISTRATION: 'reg',
            Notification.STATUS_UPDATE: 'stat_upd',
            Notification.PAYMENT: 'payment',
            Notification.TRANSF_ORDER: 'trans_ord',
            Notification.DONE_ORDER: 'done_ord',
            Notification.NEW_REPORT: 'new_rep',
            Notification.ADVICE_REPORT: 'adv_rep',
            Notification.NOT_ADVICE_REPORT: 'nadv_rep',
            Notification.CLOSE_ORDER: 'close_ord',
            Notification.BALANCE: 'balance',
            Notification.COMMENT: 'comment',
            Notification.REPORT_MOB: 'report_mob',
            Notification.BUH_OTKAZ: 'buh-otkaz',
            Notification.MONITOR: 'monitor',
            Notification.RESET_PASSWORD: 'reset_email',
        }
        if self.notification.action == Notification.NEW_ORDER:
            action_to_slug[Notification.NEW_ORDER] = self.new_order_template_slug
        return action_to_slug[self.notification.action]

    @property
    def new_order_template_slug(self):
        if self.notification.recipient.is_leader:
            pass
            ret = 'n_orlid_lk'
            return
        elif self.notification.recipient.is_client:
            ret = 'n_ordcl_lk'
        else:
            raise ValueError('У уведомления указан тип "Создание заказа", но получатель не является Клиентом или '
                             'Руководителем, чтобы получить это уведомление.')
        return ret

    def save(self, *args, **kwargs):
        created = False if self.id else True
        super(Email, self).save(*args, **kwargs)
        if created:
            self.send_to_recipient()


class Sms(TimeStampedModel):
    notification = models.ForeignKey(Notification, verbose_name='Уведомление')
    delivered = models.DateTimeField(blank=True, null=True)
    message_id = models.CharField(max_length=30, null=True, blank=True)
    status_code = models.IntegerField(null=True, blank=True)
    status_text = models.TextField(null=True, blank=True)

    class Meta(TimeStampedModel.Meta):
        db_table = 'notification_sms'

    def send_to_recipient(self):
        template_slug = self.template_slug
        phone = self.notification.recipient.phone
        data = signing.loads(self.notification.data)
        if phone:
            if not phone.startswith('+'):
                phone = '+{}'.format(phone)
            logger.error(data)

            if not settings.NOT_NOTIFICATION:
                logger.info(phone)
                send_db_sms(template_slug, phone, data, provider='notifications.sms', sms_pk=self.pk)

    @property
    def template_slug(self):
        action_to_slug = {
            Notification.REGISTRATION: 'reg_sms',
            Notification.ADD_TO_LK: 'dobavit_polzovatelya_sms',
            Notification.RESET_PASSWORD: 'reset_sms',
            Notification.PAYMENT: 'payment_sms',
            Notification.TRANSF_ORDER: 'trs_or_sms',
            Notification.DONE_ORDER: 'don_or_sms',
            Notification.NEW_ORDER: 'new_ord_sms',
            Notification.NEW_REPORT: 'new_rep_sms',
            Notification.ADVICE_REPORT: 'adv_rep',
            Notification.NOT_ADVICE_REPORT: 'nadv_rep',
            Notification.CLOSE_ORDER: 'close_ord_sms',
            Notification.COMMENT: 'comment_sms',
            Notification.REPORT_MOB: 'report_mob',
            Notification.BUH_OTKAZ: 'buh-otkaz_sms',
            Notification.MONITOR: 'monitor'
        }

        return action_to_slug[self.notification.action]

    def save(self, *args, **kwargs):
        created = False if self.id else True
        super(Sms, self).save(*args, **kwargs)
        if created:
            self.send_to_recipient()


class LK(TimeStampedModel):
    notification = models.ForeignKey(Notification, verbose_name='Уведомление')
    viewed = models.BooleanField(default=False)

    class Meta(TimeStampedModel.Meta):
        db_table = 'notification_lk'

    @property
    def template_slug(self):
        action_to_slug = {
            Notification.REGISTRATION: 'reg_lk',
            Notification.STATUS_UPDATE: 'stat_upd_lk',
            Notification.PAYMENT: 'payment_lk',
            Notification.NEW_REPORT: 'new_rep_lk',
            Notification.ADVICE_REPORT: 'adv_rep_lk',
            Notification.NOT_ADVICE_REPORT: 'nadv_rep',
            Notification.CLOSE_ORDER: 'close_ord_lk',
            Notification.COMMENT: 'comment_lk',
            Notification.CHECK_ORDER: 'check_ord_lk',
            Notification.BUH_OTKAZ: 'b-otkaz_lk',
        }
        if self.notification.action == Notification.NEW_ORDER:
            action_to_slug[Notification.NEW_ORDER] = self.new_order_template_slug
        try:
            return action_to_slug[self.notification.action]
        except KeyError as ex:
            pass

    @property
    def new_order_template_slug(self):
        if self.notification.recipient.is_leader:
            ret = 'new_ordld_lk'
            return
        elif self.notification.recipient.is_client:
            ret = 'new_ordcl_lk'
        else:
            raise ValueError('У уведомления указан тип "Создание заказа", но получатель не является Клиентом или '
                             'Руководителем, чтобы получить это уведомление.')
        return ret

    def save(self, *args, **kwargs):
        logger.error(self.notification.action)
        template = MailTemplate.get_template(slug=self.notification.action)
        if template.is_active:
            logger.error(self.template_slug)
            super(LK, self).save(*args, **kwargs)


class Render(SenderBase):
    def get_message(self):
        return self._message


def render_template(slug, data, *args, **kwargs):
    kwargs['backend'] = None
    return Render(slug, [None], data, *args, **kwargs).get_message()
