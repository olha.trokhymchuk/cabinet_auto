import time

from django.contrib.auth.models import Group
from django.core import signing
from django.test import TestCase, override_settings
from core.models import User, Filial
from notifications.models import Email, Sms, Notification, LK
from order.models import Order
from report.models import ReportPodbor
from service.models import PodborAuto


def create_test_user(username='testuser'):
    user = User.objects.create_user(
        username=username,
        email='teesttesccgkhdjkvbdvvflvdfv@lasf{}.cocasd'.format(int(time.time())),
        password=time.time() * 2,
        phone='76113354{}'.format(time.time() * 2)
    )
    return user

#
# class SMSCallbackTestCase(TestCase):
#     fixtures = ['MailTemplate']
#
#     def setUp(self):
#         test_user = create_test_user()
#         self.message_id = '201744-1000178'
#         data = {'a': 'a'}
#         data = signing.dumps(data)
#         notification = Notification.objects.create(recipient=test_user, action=Notification.REGISTRATION, data=data)
#         Sms.objects.create(notification=notification, message_id=self.message_id)
#
#     def test_sms_callback(self):
#         response = self.client.post('/notifications/sms-callback', {
#             'data[0]': ['sms_status\n201744-1000178\n102', ],
#             'data[1]': ['sms_status\n201744-1000178\n103', ]
#         })
#
#         sms = Sms.objects.get(message_id=self.message_id)
#         self.assertEqual(sms.status_code, 103)
#         self.assertEqual(response.content, b'100')


@override_settings(DEBUG=True)
class OrderNotificationTestCase(TestCase):
    fixtures = ['core/fixtures/Group.json', 'custom_admin/fixtures/permissions.json', 'MailTemplate']

    def setUp(self):
        filial = Filial.objects.create(name='moscow')
        client = create_test_user('client')
        expert = create_test_user('expert')
        leader = create_test_user('leader')
        operator = create_test_user('operator')

        client.filials.add(filial)
        expert.filials.add(filial)
        leader.filials.add(filial)
        operator.filials.add(filial)

        leader_group = Group.objects.get(name='Руководитель')
        client_group = Group.objects.get(name='Клиент')
        expert_group = Group.objects.get(name='Эксперт')
        operator_group = Group.objects.get(name='Оператор')

        client.groups.add(client_group)
        leader.groups.add(leader_group)
        expert.groups.add(expert_group)
        operator.groups.add(operator_group)

        self.order = Order.objects.create(client=client, cost=100)
        self.order.expert.add(expert)
        self.order.operator.add(operator)

        self.leader = leader
        self.operator = operator
        self.expert = expert

    def tearDown(self):
        Order.objects.all().delete()
        PodborAuto.objects.all().delete()
        Notification.objects.all().delete()

    def test_create_ppk(self):
        ppk = PodborAuto.objects.create(order=self.order)

        all_notifications = Notification.objects.all()
        all_sms = Sms.objects.all()
        all_email = Email.objects.all()
        all_lk = LK.objects.all()

        self.assertEqual(all_notifications.count(), 2)
        self.assertEqual(all_email.count(), 2)
        self.assertEqual(all_lk.count(), 1)
        self.assertEqual(all_sms.count(), 1)

        for sms in all_sms:
            self.assertEqual(sms.notification.recipient, self.order.client)

        for email in all_email:
            self.assertIn(email.notification.recipient, [self.order.client, self.leader])

        for lk in all_lk:
            self.assertEqual(lk.notification.recipient, self.order.client)

    def test_change_work_status(self):
        self.order.status = 1
        self.order.save()

        all_notifications = Notification.objects.all()
        all_sms = Sms.objects.all()
        all_email = Email.objects.all()
        all_lk = LK.objects.all()

        self.assertEqual(all_notifications.count(), 3)
        self.assertEqual(all_sms.count(), 0)
        self.assertEqual(all_email.count(), 0)
        self.assertEqual(all_lk.count(), 3)

        for notification in all_notifications:
            self.assertEqual(notification.action, Notification.STATUS_UPDATE)

        groups = ['Клиент', 'Эксперт', 'Оператор']
        for lk in all_lk:
            group_name = lk.notification.recipient.groups.all()[0].name
            self.assertIn(group_name, groups)
            groups.remove(group_name)

        self.assertEqual(len(groups), 0)

    def test_payments(self):
        self.order.paid = 50
        self.order.save()

        all_notifications = Notification.objects.all()
        all_sms = Sms.objects.all()
        all_email = Email.objects.all()
        all_lk = LK.objects.all()

        self.assertEqual(all_notifications.count(), 1)
        self.assertEqual(all_sms.count(), 1)
        self.assertEqual(all_email.count(), 1)
        self.assertEqual(all_lk.count(), 1)

        for notification in all_notifications:
            self.assertEqual(notification.action, Notification.PAYMENT)

        for email in all_email:
            self.assertEqual(email.notification.recipient, self.order.client)

        for sms in all_sms:
            self.assertEqual(sms.notification.recipient, self.order.client)

        for lk in all_lk:
            self.assertEqual(lk.notification.recipient, self.order.client)

    def test_status_closing(self):
        self.order.status_closing = True
        self.order.save()

        all_notifications = Notification.objects.all()
        all_sms = Sms.objects.all()
        all_email = Email.objects.all()
        all_lk = LK.objects.all()

        self.assertEqual(all_notifications.count(), 3)
        self.assertEqual(all_sms.count(), 0)
        self.assertEqual(all_email.count(), 0)
        self.assertEqual(all_lk.count(), 3)

        groups = ['Оператор', 'Эксперт', 'Руководитель']
        for lk in all_lk:
            group_name = lk.notification.recipient.groups.all()[0].name
            self.assertIn(group_name, groups)
            groups.remove(group_name)

        self.assertEqual(len(groups), 0)


@override_settings(DEBUG=True)
class ReportNotificationTestCase(TestCase):
    fixtures = ['core/fixtures/Group.json', 'custom_admin/fixtures/permissions.json', 'MailTemplate']

    def setUp(self):
        filial = Filial.objects.create(name='moscow')
        client = create_test_user('client')
        expert = create_test_user('expert')
        leader = create_test_user('leader')
        operator = create_test_user('operator')
        buh = create_test_user('buh')
        hr = create_test_user('hr')

        client.filials.add(filial)
        expert.filials.add(filial)
        leader.filials.add(filial)
        operator.filials.add(filial)
        buh.filials.add(filial)
        hr.filials.add(filial)

        leader_group = Group.objects.get(name='Руководитель')
        client_group = Group.objects.get(name='Клиент')
        expert_group = Group.objects.get(name='Эксперт')
        operator_group = Group.objects.get(name='Оператор')
        buh_group = Group.objects.get(name='Бухгалтер')
        hr_group = Group.objects.get(name='Контент-менеджер')

        client.groups.add(client_group)
        leader.groups.add(leader_group)
        expert.groups.add(expert_group)
        operator.groups.add(operator_group)
        buh.groups.add(buh_group)
        hr.groups.add(hr_group)

        self.order = Order.objects.create(client=client, cost=100)
        self.order.expert.add(expert)
        self.order.operator.add(operator)

        self.leader = leader
        self.operator = operator
        self.expert = expert

        self.report = ReportPodbor.objects.create(executor=expert)
        self.report.order.add(self.order)

    def tearDown(self):
        Order.objects.all().delete()
        PodborAuto.objects.all().delete()
        Notification.objects.all().delete()
        ReportPodbor.objects.all().delete()

    def test_recheck(self):
        self.report.report_type = 1
        self.report.save()

        all_notifications = Notification.objects.all()
        all_sms = Sms.objects.all()
        all_email = Email.objects.all()
        all_lk = LK.objects.all()

        self.assertEqual(all_notifications.count(), 1)
        self.assertEqual(all_sms.count(), 0)
        self.assertEqual(all_email.count(), 0)
        self.assertEqual(all_lk.count(), 1)

        groups = ['Эксперт']
        for lk in all_lk:
            group_name = lk.notification.recipient.groups.all()[0].name
            self.assertIn(group_name, groups)
            groups.remove(group_name)

        self.assertEqual(len(groups), 0)

    def test_closing(self):
        self.report.report_type = 2
        self.report.save()

        all_notifications = Notification.objects.all()
        all_sms = Sms.objects.all()
        all_email = Email.objects.all()
        all_lk = LK.objects.all()

        self.assertEqual(all_notifications.count(), 2)
        self.assertEqual(all_sms.count(), 0)
        self.assertEqual(all_email.count(), 0)
        self.assertEqual(all_lk.count(), 2)

        groups = ['Бухгалтер', 'Контент-менеджер']
        for lk in all_lk:
            group_name = lk.notification.recipient.groups.all()[0].name
            self.assertIn(group_name, groups)
            groups.remove(group_name)

        self.assertEqual(len(groups), 0)
