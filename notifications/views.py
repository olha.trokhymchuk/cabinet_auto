import json
import logging

from dbmail.models import MailTemplate
from django.contrib.auth.decorators import login_required
from django.core import signing
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, JsonResponse
from django.utils import timezone
import datetime
from django.db.models import Q

from django.template.loader import get_template
from django.views.decorators.csrf import csrf_exempt

from notifications.models import LK, Sms, render_template, Notification
from notifications.sms import service_codes

from auto.models import Auto, MarkAuto, Generation, ModelAuto
from report.models import ReportPodbor
from core.models import User
from order.models import Order

from auto.glossary import *

logger = logging.getLogger(__name__)


def get_lk_notifications(user):
    # queryset = LK.objects.filter(notification__recipient=user, viewed=False)
    notifications = []

    # for n in queryset:
    #     notification_data = signing.loads(n.notification.data)
    #     try:
    #         content = render_template(n.template_slug, notification_data)
    #     except MailTemplate.DoesNotExist as ex:
    #         logger.error(n.template_slug)
    #         logger.error(ex)
    #         continue
    #     except ValueError as ex:
    #         logger.error(ex)
    #         continue
    #
    #     notifications.append({
    #         'id': n.id,
    #         'title': n.notification.get_action_display(),
    #         'created': n.notification.created,
    #         'content': content
    #     })

    return notifications

def get_tmp(nots):
    data = signing.loads(nots.data)
    try:
        data['vin'] = Auto.objects.get(pk=data['vin'])
    except (KeyError, ValueError, Auto.DoesNotExist) as ex:
        pass
    try:
        data['mark_auto'] = MarkAuto.objects.get(pk=data['mark_auto'])
    except (KeyError, ValueError, MarkAuto.DoesNotExist) as ex:
        data['mark_auto'] =''
        pass
    try:
        data['model_auto'] = ModelAuto.objects.get(pk=data['model_auto'])
    except (KeyError, ValueError, ModelAuto.DoesNotExist) as ex:
        data['model_auto'] =''
        pass
    try:
        data['generation'] = Generation.objects.get(pk=data['generation'])
    except (KeyError, ValueError, Generation.DoesNotExist) as ex:
        data['generation'] = ''
        pass

    try:
        drive_type = ""
        for d_k, d_v in DRIVE_TYPE:
            if d_k in data['drive_type']:
                drive_type += d_v
        data['drive_type'] = drive_type
    except (KeyError, ValueError) as ex:
        data['drive_type'] = ''
        pass

    try:
        report = ReportPodbor.objects.get(pk=str(data['report_url']))
        data['order_url_name'] = report
        # if report.recommended:
        #     nots.viewed=True
        #     nots.save()
    except (KeyError, ValueError, ReportPodbor.DoesNotExist) as ex:
        pass
    try:
        data['username']  = User.objects.get(phone=data['client'])
    except (KeyError, ValueError, User.DoesNotExist) as ex:
        pass
    try:
        order  = Order.objects.get(pk=data['order_id'])
        data['number_buh'] = order.number_buh
        # if order.status_closing:
        #     nots.viewed=True
        #     nots.save()
    except (KeyError, ValueError, Order.DoesNotExist) as ex:
        pass
    try:
        return render_template(nots.action, data)
    except MailTemplate.DoesNotExist as ex:
        print(nots.action)
        print(ex)


@login_required
def all_notification(request):
    notifications = Notification.objects.filter(pub_date__day__isnull=True, recipient=request.user)
    notifications.update(pub_date = datetime.datetime.now())
    notifications = Notification.objects.filter(viewed=False, pub_date__gte=timezone.now() - datetime.timedelta(days=5), recipient=request.user).order_by('-pk')
    notis = [{'id': nots.pk, 'pub_date' :nots.pub_date, 'data': get_tmp(nots), 'action': nots.action} for nots in notifications]   
    return JsonResponse({
        'objects': notis,
        'unread_count': len(notifications)
    })

@login_required
def notification_list(request):
    template = get_template('notifications/notification-list.html')
    notifications = get_lk_notifications(request.user)
    html = template.render({'notifications': notifications})

    return HttpResponse(json.dumps({
        'html': html,
        'unread_count': len(notifications)
    }))


@csrf_exempt
def sms_callback(request):
    if request.method == 'POST':
        data = request.POST
        logger.info(dict(data))
        data_count = len(data)

        for i in range(0, data_count):
            key = 'data[{}]'.format(i)
            status = data[key]

            if isinstance(status, list):
                lines = status[0].split('\n')
            elif isinstance(status, str):
                if status == 'test':
                    return HttpResponse('100')
                lines = status.split('\n')

            status_type, sms_id, status_code = lines[0], lines[1], int(lines[2])

            if status_type == 'sms_status':
                try:
                    sms = Sms.objects.get(message_id=sms_id)
                    sms.status_code = status_code
                    sms.status_text = service_codes[status_code]
                    sms.save()
                except ObjectDoesNotExist:
                    logger.info('sms {} does not exist'.format(sms_id))

    return HttpResponse('100')
