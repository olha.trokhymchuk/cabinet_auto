# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-18 06:21
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0003_lk'),
    ]

    operations = [
        migrations.RenameField(
            model_name='notification',
            old_name='type',
            new_name='action',
        ),
    ]
