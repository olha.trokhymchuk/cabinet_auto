# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-10-18 09:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('notifications', '0004_auto_20171018_0921'),
    ]

    operations = [
        migrations.AddField(
            model_name='sms',
            name='message_id',
            field=models.CharField(blank=True, max_length=30, null=True),
        ),
        migrations.AddField(
            model_name='sms',
            name='status_code',
            field=models.IntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='sms',
            name='status_text',
            field=models.TextField(blank=True, null=True),
        ),
    ]
