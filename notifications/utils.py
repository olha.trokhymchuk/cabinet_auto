import logging
from django.core import signing

from django.db.utils import IntegrityError

from django.conf import settings

logger = logging.getLogger(__name__)


def send_notification(recipients, action, data, model=[] ,channels=('email', )):
    from notifications.models import Notification, Email, Sms, LK
    encoded_data = signing.dumps(data)
    for recipient in recipients:
        try:
            notif_obj = Notification.objects.create(recipient=recipient, action=action, data=encoded_data)
        except IntegrityError as ex:
            pass
        else:
            for mod in model:
                mod.notifications.add(notif_obj)
                mod.save()
            if 'email' in channels:
                Email.objects.create(notification=notif_obj)
            if 'sms' in channels:
                status_text = "ok"
                Sms.objects.create(notification=notif_obj, status_text=status_text)
            if 'lk' in channels:
                LK.objects.create(notification=notif_obj)

