from datetime import datetime, timezone, timedelta
import logging
import os.path
from django.conf import settings
from django.forms.models import model_to_dict
from rest_framework import serializers
from rest_framework.fields import ImageField

from auto.serializers import AutoSerializer, PreviewAutoSerializer, AutoCreateSerializer, \
    ExportMobileAutoCreateSerializer, AutoCreateSerializerByReport
import auto.glossary as auto_glossary

from core.serializers import UserSerializer
from order.models import Order
from report.models import ReportPodbor, Photo, SurrenderPhoto
from auto.models import Auto
from core.models import User

import report.report_constants as report_constant
import auto.glossary as auto_glossary
from service.serializers import PPKSerialializer

logger = logging.getLogger(__name__)


class OrderToReportSerializerList(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    client = UserSerializer()
    expert = UserSerializer(many=True)
    expert_check = UserSerializer(many=True)
    operator = UserSerializer(many=True)
    transferring = UserSerializer(many=True)
    order_type = serializers.SerializerMethodField()
    created = serializers.DateTimeField(format=settings.DATE_FORMAT, required=False)
    car_count = serializers.SerializerMethodField()
    reports_count = serializers.SerializerMethodField()
    reports_non_read_count = serializers.SerializerMethodField()
    status = serializers.CharField(source='get_status_display')
    status_closing_date = serializers.SerializerMethodField()
    status_pay = serializers.SerializerMethodField()
    order_type_code = serializers.SerializerMethodField()
    podborauto_set = PPKSerialializer(many=True)

    class Meta:
        model = Order
        depth = 1
        fields = '__all__'

    def get_order_type_code(self, obj):
        return obj.order_type

    def get_order_type(self, obj):
        return obj.get_order_type_display()

    def get_car_count(self, obj):
        return obj.cars.count()

    def get_reports_count(self, obj):
        return obj.reportpodbor_set.count()

    def get_reports_non_read_count(self, obj):
        return obj.reportpodbor_set.filter(is_read=False).count()

    def get_status_pay(self, obj):
        return obj.get_status_pay()

    def get_name(self, obj):
        try:
            return obj.number_buh
        except AttributeError:
            pass

    def get_status_closing_date(self, obj):
        return obj.status_closing_date.strftime('%d.%m.%Y') if obj.status_closing_date else False


class ReportListSerialializer(serializers.ModelSerializer):
    created = serializers.DateTimeField(
        format=settings.DATE_FORMAT, required=False)
    executor = UserSerializer()
    status = serializers.CharField(source='get_status_display')
    report_type = serializers.CharField(source='get_report_type_display')
    thumbnail = serializers.SerializerMethodField()
    auto = AutoSerializer()
    name = serializers.SerializerMethodField()
    percent = serializers.SerializerMethodField()
    check_report_to_upload = serializers.SerializerMethodField()
    is_one_month_report = serializers.SerializerMethodField()

    class Meta:
        model = ReportPodbor
        depth = 1
        fields = ('id', 'vin', 'created', 'executor', 'auto', 'name', 'thumbnail',
                  'status', 'report_type', 'recommended', 'published', 'percent', 'check_report_to_upload',
                  'is_one_month_report')

    def get_name(self, obj):
        return '{} {}'.format(obj.created.strftime(settings.DATE_FORMAT), obj.auto)

    def get_thumbnail(self, obj):
        photo = Photo.objects.filter(report_podbor=obj).first()
        if photo:
            try:
                return photo.image_small_google
            except FileNotFoundError as ex:
                return '#none'
        else:
            return '#none'

    def get_percent(self, obj):
        field_count = 1
        not_empty = 1 if obj.auto else 0

        try:
            for step in obj.data:
                for group in step.get('groups', []):
                    for field in group.get('fields', []):
                        field_count += 1
                        if field.get('value') or isinstance(field.get('value'), bool):
                            not_empty += 1

            percent = int(not_empty / field_count * 100)
            return '{}'.format(percent)
        except AttributeError as ex:
            logger.error(ex)
            return ''

    def get_check_report_to_upload(self, obj):
        get_reports = ReportPodbor.objects.filter(id=obj.id,
                                                  upload_ftp_status=True,
                                                  success_upload_status=False,
                                                  executor__isnull=False,
                                                  executor__filials__isnull=False)
        photo_front_view = obj.photos_auto.filter(image__exact='',
                                                photo_type=report_constant.type_photo['photo_front_views']).all()
        if get_reports and photo_front_view:
            return True
        else:
            return False

    def get_is_one_month_report(self, report):
        time_between_insertion = datetime.now() - report.created
        if time_between_insertion.days > 30:
            return False
        else:
            return True


class ReportSerializer(ReportListSerialializer):
    executor = UserSerializer()
    auto = AutoSerializer()
    order = serializers.SerializerMethodField()
    order_select = serializers.SerializerMethodField()
    report_type = serializers.SerializerMethodField()
    photos = serializers.SerializerMethodField()
    surrphotos = serializers.SerializerMethodField()
    is_one_month_report = serializers.SerializerMethodField()

    class Meta:
        model = ReportPodbor
        fields = (
            'id', 'vin', 'order_select', 'created', 'report_type', 'mileage', 'cost_before', 'cost_after',
            'cost_estimated', 'number_of_hosts',
            'executor', 'order', 'auto', 'pluses', 'minuses', 'category', 'recommended', 'published', 'result_expert',
            'comment_expert', 'comment_manager', 'photos', 'surrphotos', 'status', 'in_recheck', 'photos',
            'is_one_month_report'
        )
        depth = 1

    def get_order(self, report):
        orders = report.order.all()
        return [order.id for order in orders]

    def get_order_select(self, report):
        orders = report.order.all()
        return [{'id': order.id, 'text': order.number_buh} for order in orders]

    def get_report_type(self, obj):
        return obj.report_type

    def get_photos(self, obj):
        # return ''
        photos = Photo.objects.filter(report_podbor=obj).first()
        try:
            return photos.image_full_google
        except (FileNotFoundError, AttributeError) as ex:
            return ''

    def get_surrphotos(self, obj):
        photos = SurrenderPhoto.objects.filter(surrender_podbor=obj)
        try:
            raw = [{'id': photo.id, 'image': photo.image.url,
                    'image_small': photo.image_small.url} for photo in photos if photo.image]
            return raw
        except FileNotFoundError as ex:
            return ''

    def get_is_one_month_report(self, report):
        time_between_insertion = datetime.now() - report.created
        if time_between_insertion.days > 30:
            return False
        else:
            return True


class PreviewReportSerializer(serializers.ModelSerializer):
    auto = PreviewAutoSerializer()
    photos_count = serializers.SerializerMethodField()

    general_information = serializers.SerializerMethodField()
    legal_audit = serializers.SerializerMethodField()
    body_car = serializers.SerializerMethodField()
    engine_and_transmission = serializers.SerializerMethodField()
    steering = serializers.SerializerMethodField()
    computer_diagnostics = serializers.SerializerMethodField()
    electronics_and_security = serializers.SerializerMethodField()
    salon = serializers.SerializerMethodField()
    plates_and_markings_vin = serializers.SerializerMethodField()
    lift_inspection = serializers.SerializerMethodField()
    optional_equipment = serializers.SerializerMethodField()
    expert_opinion = serializers.SerializerMethodField()

    class Meta:
        model = ReportPodbor
        fields = (
            'id', 'auto', 'photos_count', 'created', 'modified',
            'general_information', 'legal_audit', 'body_car', 'engine_and_transmission', 'steering',
            'computer_diagnostics', 'electronics_and_security', 'salon', 'plates_and_markings_vin', 'lift_inspection',
            'optional_equipment', 'expert_opinion',
        )
        depth = 1

    @staticmethod
    def check_obj_fields(obj, fields):
        obj_dict = model_to_dict(obj)
        if [obj_dict.get(f) for f in fields if obj_dict.get(f)]:
            return True
        else:
            return False

    def get_photos_count(self, obj):
        photos_count = Photo.objects.filter(report_podbor=obj).count()
        return photos_count

    def get_general_information(self, obj):
        return True

    def get_legal_audit(self, obj):
        return True

    def get_body_car(self, obj):
        return True

    def get_engine_and_transmission(self, obj):
        fields = ["postor_shumi", "postor_shumi_com", "zapotev_techi", "zapotev_techi_com", "ravnom_rab",
                  "ravnom_rab_com", "ur_masla", "ur_masla_com", "ur_ohl_zhidk", "ur_ohl_zhidk_com", "tr_obsl",
                  "tr_obsl_com", "sost_priv_rem", "sost_priv_rem_com", "rabota_kpp", "rabota_kpp_com",
                  "dvig_diag_comment"]
        return self.check_obj_fields(obj, fields)

    def get_steering(self, obj):
        fields = ["izn_ptk", "izn_ptk_range", "izn_ptk_com", "izn_ztk", "izn_ztk_range", "izn_ztk_com", "izn_ptd",
                  "izn_ptd_range", "izn_ptd_com", "izn_ztd", "izn_ztd_range", "izn_ztd_com", "izn_rez_leto",
                  "izn_rez_leto_range", "izn_rez_leto_com", "izn_rez_zima", "izn_rez_zima_range", "izn_rez_zima_com",
                  "ur_tormoz_zh", "ur_tormoz_zh_com", "ur_gur_zh", "ur_gur_zh_com", "sost_st_torm", "sost_st_torm_com",
                  "sost_rez", "sost_rez_com", "sost_torm_zh", "sost_torm_zh_com", "stuki_skr", "stuki_skr_com",
                  "otkl_pr_dvig", "otkl_pr_dvig_com", "luft_rul_uprav", "luft_rul_uprav_com", "rul_uprav_comment"]
        return self.check_obj_fields(obj, fields)

    def get_computer_diagnostics(self, obj):
        fields = ["read_vin", "read_vin_com", "connect_ebu", "connect_ebu_com", "error_ebu", "error_ebu_com",
                  "number_key_ebu", "number_key_ebu_com", "comp_diag_comment"]
        return self.check_obj_fields(obj, fields)

    def get_electronics_and_security(self, obj):
        fields = ["svet_indik", "svet_indik_com", "steklopod", "steklopod_com", "omyvatel", "omyvatel_com",
                  "el_priv_sal", "el_priv_sal_com", "el_komp_other", "el_komp_other_com", "ispr_srs", "ispr_srs_com",
                  "remni_bezopas", "remni_bezopas_com", "ispr_ac", "ispr_ac_com", "elektro_comment"]
        return self.check_obj_fields(obj, fields)

    def get_salon(self, obj):
        fields = ["salon_defects ", "salon_defects_com ", "salon_clear ", "salon_clear_com ", "sost_salon_comment"]
        return self.check_obj_fields(obj, fields)

    def get_plates_and_markings_vin(self, obj):
        return True

    def get_lift_inspection(self, obj):
        fields = ["lufts", "lufts_com", "silentblocks", "silentblocks_com", "tormoz_defects", "tormoz_defects_com",
                  "sost_opor", "sost_opor_com", "zapotev_tech", "zapotev_tech_com", "sost_pylnikov",
                  "sost_pylnikov_com", "sost_vyhlop", "sost_vyhlop_com", "silov_el", "silov_el_com",
                  "podyemnik_comment"]
        return self.check_obj_fields(obj, fields)

    def get_optional_equipment(self, obj):
        fields = ["dop_oborudovanie", "dop_oborudovanie_com"]
        return self.check_obj_fields(obj, fields)

    def get_expert_opinion(self, obj):
        return True


class PhotoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Photo
        fields = (
            'id', 'image', 'embed_url', 'photo_type'
        )
        depth = 1


class PhotoSurrenderSerializer(serializers.ModelSerializer):
    class Meta:
        model = SurrenderPhoto
        fields = (
            'id', 'image'
        )
        depth = 1


class ReportSerializerV2(serializers.ModelSerializer):
    executor = UserSerializer()
    auto = AutoSerializer()
    name = serializers.SerializerMethodField()
    executor_name = serializers.SerializerMethodField()
    created = serializers.DateTimeField(format=settings.DATE_FORMAT)
    photos_auto = PhotoSerializer(many=True, read_only=True)
    surrphotos_auto = PhotoSurrenderSerializer(many=True, read_only=True)

    class Meta:
        model = ReportPodbor
        fields = (
            'id', 'created', 'executor_name', 'name', 'photos_auto', 'surrphotos_auto',
            'vin', 'report_type', 'mileage', 'cost_before', 'cost_after', 'cost_estimated', 'number_of_hosts',
            'executor', 'order', 'auto', 'pluses', 'minuses', 'category', 'recommended', 'published', 'result_expert',
            'comment_expert', 'comment_manager', 'data', 'in_recheck'
        )
        depth = 1

    def get_name(self, obj):
        return obj.__str__()

    def get_executor_name(self, obj):
        if type(obj.executor) is UserSerializer:
            return obj.executor.get_full_name()
        return ''


class ReportSerializerV3(serializers.ModelSerializer):
    executor = UserSerializer(read_only=True)
    auto = AutoSerializer()
    name = serializers.SerializerMethodField()
    executor_name = serializers.SerializerMethodField()
    photos = serializers.SerializerMethodField()
    created = serializers.DateTimeField(format=settings.DATE_FORMAT)
    google_album_url = serializers.SerializerMethodField()
    check_photos_is_local = serializers.SerializerMethodField()
    order = OrderToReportSerializerList(many=True)
    is_one_month_report = serializers.SerializerMethodField()

    class Meta:
        model = ReportPodbor
        exclude = ('lk_id', 'modified', 'status', 'links', 'data',)
        depth = 3

    def get_check_photos_is_local(self, obj):
        if obj.photos_auto.filter(image__exact='').count():
            return True
        return False

    def get_google_album_url(self, obj):
        if obj.google_album_url:
            return obj.google_album_url
        else:
            return ''

    def get_name(self, obj):
        return obj.__str__()

    def get_photos(self, obj):
        photos = {}
        for photo in obj.photos_auto.all():
            photo_type = report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]

            if photo.image_google:
                try:
                    photos[photo_type]
                except KeyError as ex:
                    photos[photo_type] = []
                photos[photo_type].append({
                    "image_small": '/google_photo/google_images/' + photo.image_google + '/150/150/',
                    "image": '/google_photo/google_images/' + photo.image_google,
                    "image_google": photo.image_google,
                    "from_google": 'from-google',
                    "pk": photo.pk,
                })
            if photo.googlePhoto:
                try:
                    photos[photo_type]
                except KeyError as ex:
                    photos[photo_type] = []
                photos[photo_type].append({
                    "image_small": '/google_photo/google_images_obj/' + str(photo.googlePhoto.id) + '/150/150/',
                    "image": '/google_photo/google_images_obj/' + str(photo.googlePhoto.id),
                    "image_google": photo.googlePhoto.imageId,
                    "from_google": 'from-google',
                    "pk": photo.pk,
                })
            if photo.image:
                path_to_photo = photo.image.path
                if not os.path.exists(path_to_photo):
                    photo.delete()
                    continue
                path_to_image_small = photo.image_small.path
                if not os.path.exists(path_to_image_small):
                    photo.image_small.generate()
                image_full = "/media/" + str(photo.image)
                try:
                    photo.image_small.url
                    photo.image_small.url.split("/")[3]
                    image_small = "/media/" + str(photo.image_small)
                except:
                    if not photo.embed_url:
                        continue
                    else:
                        image_full = image_small = photo.embed_url
                try:
                    photos[photo_type]
                except KeyError as ex:
                    photos[photo_type] = []
                photos[photo_type].append({
                    "image_small": image_small,
                    "image": image_full,
                    "image_google": '',
                    "from_google": 'from-local',
                    "pk": photo.pk,
                })
        return photos

    @staticmethod
    def split(arr, size):
        arrs = []
        while len(arr) > size:
            pice = arr[:size]
            arrs.append(pice)
            arr = arr[size:]
        arrs.append(arr)
        return arrs

    def get_executor_name(self, obj):
        if type(obj.executor) is UserSerializer:
            return obj.executor.get_full_name()
        return ''

    def get_is_one_month_report(self, report):
        time_between_insertion = datetime.now() - report.created
        if time_between_insertion.days > 30:
            return False
        else:
            return True


class ReportSerializerCreateUpdateV3(serializers.ModelSerializer):
    executor = UserSerializer(required=False, allow_null=True)
    photos = serializers.SerializerMethodField()
    auto = AutoCreateSerializer(required=False, allow_null=True)
    executor_name = serializers.SerializerMethodField(
        required=False, allow_null=True)

    def get_photos(self, obj):
        photos = {}
        try:
            for photo in obj.photos_auto.all().order_by('-pk'):
                photo_type = report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]
                try:
                    photos[photo_type].append({"image_small": "/media/" + str(photo.image_small),
                                               "image": "/media/" + str(photo.image),
                                               "pk": photo.pk,
                                               })
                except KeyError as ex:
                    photos[photo_type] = []
                    photos[photo_type].append({"image_small": "/media/" + str(photo.image_small),
                                               "image": "/media/" + str(photo.image),
                                               "pk": photo.pk,
                                               })
        except AttributeError:
            pass
        return photos

    def create_auto(self, instance, auto_data):
        if auto_data:
            auto_serialized_data = AutoCreateSerializer(data=auto_data)
            auto_serialized_data.is_valid()
            logger.error("auto_serialized_data.errors")
            logger.error(auto_serialized_data.errors)
            logger.error("auto_serialized_data.errors")
            auto_data = auto_serialized_data.data
            try:
                vin = auto_data["vin"]
                auto = Auto.objects.get(vin=vin)
                auto_serialized_data.update(auto, auto_data)
                instance.auto = auto
            except (KeyError, Auto.DoesNotExist, AssertionError) as ex:
                logger.error(ex)
                auto_serialized_data.create(auto_serialized_data.data)
                auto = Auto.objects.get(vin=vin)
                instance.auto = auto
            except Auto.MultipleObjectsReturned as ex:
                logger.error(ex)
            try:
                self.validated_data["auto"]
            except KeyError as ex:
                self.validated_data["auto"] = auto_serialized_data.validated_data
                self.validated_data["auto"]["auto"] = {}
            self.validated_data["auto"]["id"] = auto.pk
            try:
                self.validated_data["auto"]["auto"] = auto_serialized_data.data["auto"]
            except KeyError as ex:
                pass

            self.validated_data["auto"]["thumbnail"] = auto_serialized_data.data["thumbnail"]
            try:
                self.validated_data["auto"]["engine_type"] = self.get_engine_type(
                    auto_serialized_data.data["engine_type"])
            except KeyError:
                pass
            try:
                self.validated_data["auto"]["transmission_type"] = self.get_transmission_type(
                    auto_serialized_data.data["transmission_type"])
            except KeyError:
                pass

        return instance

    def get_engine_type(self, engine_type):
        return auto_glossary.ENGINE_TYPE[engine_type][1] if isinstance(engine_type, int) else engine_type

    def get_transmission_type(self, transmission_type):
        return auto_glossary.TRANSMISSION_TYPE[transmission_type][1] if isinstance(transmission_type,
                                                                                   int) else transmission_type

    def create(self, validated_data):
        auto_data = validated_data.pop('auto', None)
        order = validated_data.pop('order', None)
        photos = validated_data.pop('photos', None)
        executor = validated_data.pop('executor', None)
        try:
            vin = validated_data["vin"]
        except KeyError:
            vin = None
        instance = ReportPodbor.objects.create(**validated_data)
        instance = self.create_executor(instance, executor)
        logger.error("instance")
        logger.error(instance)
        instance = self.create_auto(instance, auto_data)
        instance.save()
        validated_data["auto"] = auto_data
        return instance

    def create_executor(self, instance, executor):
        if executor:
            try:
                instance.executor = User.objects.get(phone=executor["phone"])
            except (User.DoesNotExist, TypeError):
                pass
        if executor and not instance.executor:
            try:
                instance.executor = User.objects.get(pk=executor["id"])
            except (TypeError, KeyError):
                pass
        return instance

    def update(self, instance, validated_data):
        auto_data = validated_data.pop('auto', None)
        executor = validated_data.pop('executor', None)
        instance = self.create_executor(instance, executor)
        instance = self.create_auto(instance, auto_data)
        for attr, value in validated_data.items():
            if (attr != "created"):
                try:
                    setattr(instance, attr, value)
                except (TypeError, ValueError):
                    pass
        validated_data["auto"] = auto_data
        instance.save()
        return instance

    def get_executor_name(self, obj):
        try:
            if type(obj.executor) is UserSerializer:
                return obj.executor.get_full_name()
        except AttributeError:
            pass
        return ''

    class Meta:
        model = ReportPodbor
        exclude = ('lk_id', 'modified', 'status', 'links', 'data',)
        depth = 1


class ExportMobileReportSerializerCreateUpdateV3(ReportSerializerCreateUpdateV3):
    auto = ExportMobileAutoCreateSerializer(required=False, allow_null=True)

    def create_auto(self, instance, auto_data):
        if auto_data:
            auto_serialized_data = ExportMobileAutoCreateSerializer(
                data=auto_data)
            auto_serialized_data.is_valid()
            logger.error("auto_serialized_data.errors")
            logger.error(auto_serialized_data.errors)
            logger.error("auto_serialized_data.errors")
            auto_data = auto_serialized_data.data
            try:
                vin = auto_data["vin"]
                auto = Auto.objects.get(vin=vin)
                auto_serialized_data.update(auto, auto_data)
                instance.auto = auto
            except (KeyError, Auto.DoesNotExist, AssertionError) as ex:
                logger.error(ex)
                auto_serialized_data.create(auto_serialized_data.data)
                auto = Auto.objects.get(vin=vin)
                instance.auto = auto
            except Auto.MultipleObjectsReturned as ex:
                logger.error(ex)
            try:
                self.validated_data["auto"]
            except KeyError as ex:
                self.validated_data["auto"] = auto_serialized_data.validated_data
                self.validated_data["auto"]["auto"] = {}
            self.validated_data["auto"]["id"] = auto.pk
            try:
                self.validated_data["auto"]["auto"] = auto_serialized_data.data["auto"]
            except KeyError as ex:
                pass

            self.validated_data["auto"]["thumbnail"] = auto_serialized_data.data["thumbnail"]
            try:
                self.validated_data["auto"]["engine_type"] = self.get_engine_type(
                    auto_serialized_data.data["engine_type"])
            except KeyError:
                pass
            try:
                self.validated_data["auto"]["transmission_type"] = self.get_transmission_type(
                    auto_serialized_data.data["transmission_type"])
            except KeyError:
                pass

        return instance


class ReportSerializerCreateUpdateV4(ReportSerializerCreateUpdateV3):
    auto = AutoCreateSerializerByReport(required=False, allow_null=True)

    def create_auto(self, instance, auto_data):
        if auto_data:
            auto_serialized_data = AutoCreateSerializerByReport(
                data=auto_data)
            auto_serialized_data.is_valid()
            logger.error("auto_serialized_data.errors")
            logger.error(auto_serialized_data.errors)
            logger.error("auto_serialized_data.errors")
            auto_data = auto_serialized_data.data
            try:
                vin = auto_data["vin"]
                auto = Auto.objects.get(vin=vin)
                auto_serialized_data.update(auto, auto_data)
                instance.auto = auto
            except (KeyError, Auto.DoesNotExist, AssertionError) as ex:
                logger.error(ex)
                auto_serialized_data.create(auto_serialized_data.data)
                auto = Auto.objects.get(vin=vin)
                instance.auto = auto
            except Auto.MultipleObjectsReturned as ex:
                logger.error(ex)
            try:
                self.validated_data["auto"]
            except KeyError as ex:
                self.validated_data["auto"] = auto_serialized_data.validated_data
                self.validated_data["auto"]["auto"] = {}
            self.validated_data["auto"]["id"] = auto.pk
            try:
                self.validated_data["auto"]["auto"] = auto_serialized_data.data["auto"]
            except KeyError as ex:
                pass

            self.validated_data["auto"]["thumbnail"] = auto_serialized_data.data["thumbnail"]
            try:
                self.validated_data["auto"]["engine_type"] = self.get_engine_type(
                    auto_serialized_data.data["engine_type"])
            except KeyError:
                pass
            try:
                self.validated_data["auto"]["transmission_type"] = self.get_transmission_type(
                    auto_serialized_data.data["transmission_type"])
            except KeyError:
                pass

        return instance


class ReportSerializerCreateWithApi(serializers.ModelSerializer):
    executor = UserSerializer(required=False, allow_null=True)
    photos = serializers.SerializerMethodField()
    auto = AutoCreateSerializer(required=False, allow_null=True)
    executor_name = serializers.SerializerMethodField(
        required=False, allow_null=True)

    def get_photos(self, obj):
        photos = {}
        try:
            for photo in obj.photos_auto.all().order_by('-pk'):
                photo_type = report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]
                try:
                    photos[photo_type].append({"image_small": "/media/" + str(photo.image_small),
                                               "image": "/media/" + str(photo.image),
                                               "pk": photo.pk,
                                               })
                except KeyError as ex:
                    photos[photo_type] = []
                    photos[photo_type].append({"image_small": "/media/" + str(photo.image_small),
                                               "image": "/media/" + str(photo.image),
                                               "pk": photo.pk,
                                               })
        except AttributeError:
            pass
        return photos

    def create_auto(self, instance, auto_data):
        if auto_data:
            auto_serialized_data = AutoCreateSerializer(data=auto_data)
            auto_serialized_data.is_valid()
            logger.error("auto_serialized_data.errors")
            logger.error(auto_serialized_data.errors)
            logger.error("auto_serialized_data.errors")
            auto_data = auto_serialized_data.data
            try:
                vin = auto_data["vin"]
                auto = Auto.objects.get(vin=vin)
                auto_serialized_data.update(auto, auto_data)
                instance.auto = auto
            except (KeyError, Auto.DoesNotExist, AssertionError) as ex:
                logger.error(ex)
                auto_serialized_data.create(auto_serialized_data.data)
                auto = Auto.objects.get(vin=vin)
                instance.auto = auto
            except Auto.MultipleObjectsReturned as ex:
                logger.error(ex)
            try:
                self.validated_data["auto"]
            except KeyError as ex:
                self.validated_data["auto"] = auto_serialized_data.validated_data
                self.validated_data["auto"]["auto"] = {}
            self.validated_data["auto"]["id"] = auto.pk
            try:
                self.validated_data["auto"]["auto"] = auto_serialized_data.data["auto"]
            except KeyError as ex:
                pass

            self.validated_data["auto"]["thumbnail"] = auto_serialized_data.data["thumbnail"]
            try:
                self.validated_data["auto"]["engine_type"] = self.get_engine_type(
                    auto_serialized_data.data["engine_type"])
            except KeyError:
                pass
            try:
                self.validated_data["auto"]["transmission_type"] = self.get_transmission_type(
                    auto_serialized_data.data["transmission_type"])
            except KeyError:
                pass

        return instance

    def get_engine_type(self, engine_type):
        return auto_glossary.ENGINE_TYPE[engine_type][1] if isinstance(engine_type, int) else engine_type

    def get_transmission_type(self, transmission_type):
        return auto_glossary.TRANSMISSION_TYPE[transmission_type][1] if isinstance(transmission_type,
                                                                                   int) else transmission_type

    def create(self, validated_data):
        auto_data = validated_data.pop('auto', None)
        order = validated_data.pop('order', None)
        photos = validated_data.pop('photos', None)
        executor = validated_data.pop('executor', None)
        try:
            vin = validated_data["vin"]
        except KeyError:
            vin = None
        instance = ReportPodbor.objects.create(**validated_data)
        instance = self.create_executor(instance, executor)
        logger.error("instance")
        logger.error(instance)
        instance = self.create_auto(instance, auto_data)
        instance.save()
        validated_data["auto"] = auto_data
        return instance

    def create_executor(self, instance, executor):
        if executor:
            try:
                instance.executor = User.objects.get(phone=executor["phone"])
            except (User.DoesNotExist, TypeError):
                pass
        if executor and not instance.executor:
            try:
                instance.executor = User.objects.get(pk=executor["id"])
            except (TypeError, KeyError):
                pass
        return instance

    def get_executor_name(self, obj):
        try:
            if type(obj.executor) is UserSerializer:
                return obj.executor.get_full_name()
        except AttributeError:
            pass
        return ''

    class Meta:
        model = ReportPodbor
        exclude = ('lk_id', 'modified', 'status', 'links', 'data',)
        depth = 1