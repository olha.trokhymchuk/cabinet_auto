from django.core.management.base import BaseCommand

from report.workers.export_lk.export_report_lk import lkParser


class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def add_arguments(self, parser):
		parser.add_argument('lk_id')

	def handle(self, *args, **options):
		if options['lk_id'] == "by_last":
			params = options['lk_id']
		else:
			params = options['lk_id'].split(',')
		lkParser(params)

