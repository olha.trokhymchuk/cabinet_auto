from django.core.management.base import BaseCommand

from report.workers.load_photos_to_google import loadPhotosToGoogle
from time import sleep

class Command(BaseCommand):
    help = 'Load Photos to GooglePhoto'

    def handle(self, *args, **options):
        loadPhotosToGoogle()
