from django.core.management.base import BaseCommand

from report.workers.upload_daily_reports_to_auto_library import upload_daily_reports


class Command(BaseCommand):
    help = 'Upload daily reports to auto library from 31 day'

    def handle(self, *args, **options):
        upload_daily_reports()
