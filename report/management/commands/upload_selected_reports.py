from django.core.management.base import BaseCommand

from report.workers.upload_selected_reports import get_reports


class Command(BaseCommand):
    help = 'Upload selected reports to auto library'

    def handle(self, *args, **options):
        get_reports()