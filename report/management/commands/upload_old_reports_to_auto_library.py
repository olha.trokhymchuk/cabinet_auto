from django.core.management.base import BaseCommand

from report.workers.upload_old_reports_to_auto_library import upload_old_reports


class Command(BaseCommand):
    help = 'Upload old reports to auto library'

    def handle(self, *args, **options):
        for x in range(10):
            upload_old_reports()