from django.core.management.base import BaseCommand

from report.workers.export_order import get_reports_from_mobile, download_from_ftp
from report.workers.export_mobile import ExportMobile

class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def add_arguments(self, parser):
		parser.add_argument('remote_dir', nargs='+', type=str)

	def handle(self, *args, **options):
		remote_dir = options['remote_dir']
		name = "export from mobie app by ftp"
		ExportMobile(name, remote_dir=remote_dir[0])