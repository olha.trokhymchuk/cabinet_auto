from django.core.management.base import BaseCommand

from report.workers.export_order import get_reports_from_mobile, download_from_ftp
from report.workers.export_mobile_field import ExportMobileField

class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def add_arguments(self, parser):
		parser.add_argument("--remote_dir", nargs='+',  type=str)
		parser.add_argument("--field", nargs='+', type=str)
		parser.add_argument("--field_mobile", nargs='+', type=str)

	def handle(self, *args, **options):
		remote_dir = options['remote_dir']
		field = options['field']
		field_mobile = options['field_mobile']
		name = "export from mobie app by ftp"
		ExportMobileField(name, remote_dir=remote_dir[0], field=field[0], field_mobile=field_mobile[0])