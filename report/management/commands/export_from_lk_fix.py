from django.core.management.base import BaseCommand

from report.workers.export_lk.export_report_lk import lkParser

from report.models import ReportPodbor
from report.workers.parse_from_json_to_row import setRow


class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        try:
            parser.add_argument("--id", type=str)
        except ValueError:
            pass

    def handle(self, *args, **options):
        try:
            pk = options['id'].split(',')
        except (AttributeError, KeyError):
            pk = None
        print(pk)
        if pk:
            if len(pk) == 1:
                r = ReportPodbor.objects.get(pk=pk[0])
                print(r.pk)
                setRow([r.pk])

            if len(pk) == 2:
                for i in range(int(pk[0]), int(pk[1])):
                    try:
                        r = ReportPodbor.objects.get(pk=i)
                        setRow([r.pk])
                    except ReportPodbor.DoesNotExist:
                        pass
        else:
            r = ReportPodbor.objects.exclude(auto__data_lk={})
            for res in  r.values("pk"):
                print(res["pk"])
                setRow([res["pk"]])

