import xlrd

from django.core.management.base import BaseCommand
from auto.models import Auto


class Command(BaseCommand):
	help = 'Closes the specified poll for voting'

	def add_arguments(self, parser):
		pass

	def handle(self, *args, **options):
		workbook = xlrd.open_workbook('report/workers/Цвета.xlsx')
		worksheet = workbook.sheet_by_index(0)
		for col in range(worksheet.ncols):
			for row in range(1, worksheet.nrows):
					print("--------------------------")
					color_lk = worksheet.cell_value(row,0)
					fix_color = worksheet.cell_value(row,1).rstrip().lstrip()
					# if type(color_lk) == str:
					# 	color_lk = color_lk.lower().rstrip().lstrip()
					# else:
					# 	color_lk = str(int(color_lk)).lower().rstrip().lstrip()

					auto = Auto.objects.filter(color_auto__icontains=color_lk)
					print(color_lk)
					print(auto)
					print(fix_color)
					auto.update(color_auto = fix_color)
	
					print("--------------------------")
