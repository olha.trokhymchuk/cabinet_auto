from django.core.management.base import BaseCommand

from report.models import Photo
from time import sleep


def delete_photos(photos, report):
    for photo in photos:
        print (photo.id)
        photo.delete()
    photos = Photo.objects.filter(report_podbor_id=int(report))[:10000]
    if photos:
        sleep(1)
        delete_photos(photos, report)


class Command(BaseCommand):

    def handle(self, *args, **options):
        report = input('Введите id report: ')
        photos = Photo.objects.filter(report_podbor_id=int(report))[:10000]
        delete_photos(photos, report)