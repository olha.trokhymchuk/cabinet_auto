from django.core.management.base import BaseCommand

from report.workers.parse_from_json_to_row import setRow

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def add_arguments(self, parser):
        parser.add_argument('id')

    def handle(self, *args, **options):
        params = options['id'].split(',')
        setRow(params)

