from django.core.management.base import BaseCommand

from report.workers.set_published_values_to_report import start_runner_change_published_report


class Command(BaseCommand):
    help = 'Set published values to report'

    def handle(self, *args, **options):
        start_runner_change_published_report()