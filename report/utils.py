import os
import base64
import json

from django.conf import settings
from constance import config
from django.core.files.base import ContentFile
from imagekit import ImageSpec
from pilkit.processors import ResizeToFit, ResizeToFill

class Base64Error(Exception):
    pass


def base64_to_content_file(data, allowed_mimes=None):
    try:
        data_format, encoded_data = data.split(';base64,')
    except ValueError:
        raise Base64Error('base64 delimiter not found')

    if data_format.startswith('data:'):
        data_format = data_format[5:]

    if allowed_mimes is not None and data_format not in allowed_mimes:
        raise Base64Error('{} is not allowed'.format(data_format))

    extension = data_format.split('/')[-1]

    _file = ContentFile(
        base64.b64decode(encoded_data),
        name='temp.' + extension
    )

    return _file


class OriginalSpec(ImageSpec):
    processors = [ResizeToFit(1300, 1300)]
    format = 'JPEG'
    options = {'quality': 100}
    _options = options

    @property
    def options(self):
        options = self._options
        if self.source.size > 250000 and options['quality'] == 100:
            options['quality'] = 85

        return options

    @options.setter
    def options(self, value):
        self._options = value


class ThumbnailSpec(ImageSpec):
    format = 'JPEG'
    options = {'quality': 80}

    @property
    def processors(self):
        return [ResizeToFill(config.THUMBNAIL_WIDTH, config.THUMBNAIL_HEIGHT)]

