# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-07-09 08:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0047_remove_reportpodbor_is_read'),
    ]

    operations = [
        migrations.AddField(
            model_name='reportpodbor',
            name='is_read',
            field=models.BooleanField(default=True, verbose_name='Прочитан отчет руководителем'),
        ),
    ]
