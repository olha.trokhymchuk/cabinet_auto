# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-01-17 17:43
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0033_auto_20190109_1635'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reportpodbor',
            name='data_lk',
        ),
    ]
