# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-10-18 06:42
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0053_remove_reportpodbor_is_read_by_client'),
        ('report', '0053_auto_20190924_0856'),
    ]

    operations = [
    ]
