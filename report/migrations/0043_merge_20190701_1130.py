# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-07-01 08:30
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0039_auto_20190603_1617'),
        ('report', '0042_merge_20190628_0841'),
    ]

    operations = [
    ]
