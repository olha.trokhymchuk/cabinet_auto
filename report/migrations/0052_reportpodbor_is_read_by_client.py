# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2019-10-14 07:19
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0051_merge_20190829_1755'),
    ]

    operations = [
        migrations.AddField(
            model_name='reportpodbor',
            name='is_read_by_client',
            field=models.BooleanField(default=True, verbose_name='Прочитан клиентом после сдачи'),
        ),
    ]
