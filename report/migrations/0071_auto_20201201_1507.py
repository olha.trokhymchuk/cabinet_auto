# -*- coding: utf-8 -*-
# Generated by Django 1.11.10 on 2020-12-01 12:07
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0070_auto_20201026_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_auto_kod',
            field=models.NullBooleanField(verbose_name='Проверка по Автокоду'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_auto_kod_com',
            field=models.CharField(blank=True, default=None, max_length=2000, null=True, verbose_name='Комментарий'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_dop_service',
            field=models.NullBooleanField(verbose_name='Проверка по доп.сервисам'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_dop_service_com',
            field=models.CharField(blank=True, default=None, max_length=2000, null=True, verbose_name='Комментарий'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_fssp',
            field=models.NullBooleanField(verbose_name='Проверка по ФССП'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_fssp_com',
            field=models.CharField(blank=True, default=None, max_length=2000, null=True, verbose_name='Комментарий'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_gbdd',
            field=models.NullBooleanField(verbose_name='Проверка по ГИБДД'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_gbdd_com',
            field=models.CharField(blank=True, default=None, max_length=2000, null=True, verbose_name='Комментарий'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_pts_original',
            field=models.NullBooleanField(verbose_name='ПТС оригинал'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_pts_original_com',
            field=models.CharField(blank=True, default=None, max_length=2000, null=True, verbose_name='Комментарий'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_res_zalog',
            field=models.NullBooleanField(verbose_name='Проверка по реестру залогов'),
        ),
        migrations.AddField(
            model_name='reportpodbor',
            name='jur_res_zalog_com',
            field=models.CharField(blank=True, default=None, max_length=2000, null=True, verbose_name='Комментарий'),
        ),
    ]
