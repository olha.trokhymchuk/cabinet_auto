import datetime
import logging
import weasyprint
import os
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.sites.models import Site
from django.db import models
from django.db.models import Q
from django.core.mail import EmailMessage
from django.urls import reverse
from django.template.loader import get_template
from django.conf import settings

from django.db.utils import DataError

from imagekit.models import ImageSpecField, ProcessedImageField
from safedelete import HARD_DELETE
from safedelete.models import SafeDeleteModel
from jsoneditor.fields.django_jsonfield import JSONField as JSONField2
from django.contrib.postgres.fields import JSONField

from core.models import User, Filial
from core.utils import CustomFieldHistoryTracker
from notifications.models import Notification
from notifications.utils import send_notification
from order.models import Order
from auto.models import Auto, AutoPhoto
from auto.glossary import NUMBER_OF_HOST_TYPE
from report.utils import OriginalSpec, ThumbnailSpec
from django.dispatch import receiver

import report.report_constants as report_constant
from django.template import Context, Template
from weasyprint import HTML, CSS
from google_photo.utils import creat_google_album, delete_media_by_id, share_google_album, get_media_url_by_id
from google_photo.models import GooglePhotoImage

logger = logging.getLogger(__name__)

now = datetime.datetime.now()
year = now.strftime("%Y")
month = now.strftime("%m")
day = now.strftime("%d")
time = now.strftime("%H%M%S")


def upload_auto(instance, filename):
    if len(filename.split('.')) > 2:
        filebase, extension = filename.split(".")[-2:]
    else:
        filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/auto_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_gibdd(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/gibdd_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_fssp(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/fssp_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_zalog(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/zalog_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_mycreditinfo(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/mycreditinfo_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_avtokod(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/avtokod_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_dopserv(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/dopserv_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_photo_vin1(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin1_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_photo_vin2(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin2_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_photo_vin3(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin3_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_photo_vin_table(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin_table_%s.%s" %(year, month, day, instance.id, time, extension)


def upload_photo_vin_glass(instance, filename):
    filebase, extension = filename.split(".")
    return "reports/ppk/%s/%s/%s/id_%s/photo_vin_glass_%s.%s" %(year, month, day, instance.id, time, extension)

# Отчет подбор под ключ.
class ReportPodbor(SafeDeleteModel):
    _safedelete_policy = HARD_DELETE

    REPORT_TYPE = (
        (0, 'Подбор'),
        (1, 'Перепроверка'),
        (2, 'Сдача'),
    )

    STATUS = (
        (0, 'в работе'),
        (1, 'ожидает проверки'),
        (2, 'проверен'),
        (3, 'Готов к выдаче')
    )

    is_read = models.BooleanField('Прочитан отчет руководителем', default=True)
    in_recheck = models.BooleanField('На перепроверке', default=False)
    lk_id = models.PositiveIntegerField('Id со старого лк', blank=True, null=True, default=None)
    created = models.DateTimeField('Дата создания', auto_now_add=True, db_index=True)
    modified = models.DateTimeField('Последнее изменение', auto_now=True)
    vin = models.CharField('VIN', max_length=17, blank=True, null=True, default=None, db_index=True)
    status = models.PositiveSmallIntegerField('Статус', blank=True, choices=STATUS, default=0)
    report_type = models.PositiveSmallIntegerField('Тип', blank=True, choices=REPORT_TYPE, default=0)
    executor = models.ForeignKey(User, blank=True, null=True, verbose_name='Исполнитель')
    # Общая информация
    order = models.ManyToManyField(Order, blank=True, default=None, verbose_name='Заказ')
    auto = models.ForeignKey(Auto, verbose_name='Авто', null=True, blank=True, db_index=True)
    google_album = models.TextField('google_album', blank=True, null=True, default=None)
    google_album_url = models.TextField('google_album_url', blank=True, null=True, default=None)
    links = models.CharField('Ссылки на объявления', max_length=1000, blank=True, null=True, default='')
    mileage = models.PositiveIntegerField('Пробег', blank=True, null=True, default=None)
    number_of_hosts = models.IntegerField('Количество по ПТС', blank=True, choices=NUMBER_OF_HOST_TYPE, default=0)
    # Юридическая проверка

    jur_pts_original = models.NullBooleanField('ПТС оригинал', null=True)
    jur_pts_original_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    jur_gbdd = models.NullBooleanField('Проверка по ГИБДД', null=True)
    jur_gbdd_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    jur_fssp = models.NullBooleanField('Проверка по ФССП', null=True)
    jur_fssp_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    jur_res_zalog = models.NullBooleanField('Проверка по реестру залогов', null=True)
    jur_res_zalog_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    jur_auto_kod = models.NullBooleanField('Проверка по Автокоду', null=True)
    jur_auto_kod_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    jur_dop_service = models.NullBooleanField('Проверка по доп.сервисам', null=True)
    jur_dop_service_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    jur_comment = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    # Кузов

    # Внешние элементы
    krisha = models.NullBooleanField('Крыша', null=True)
    krisha_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    krisha_okrasheno = models.BooleanField('Крыша Окрашено', default=False)
    krisha_skol_carapina = models.BooleanField('Крыша Скол/царапина', default=False)
    krisha_vmyatina = models.BooleanField('Крыша Вмятина', default=False)
    krisha_corozia_rgavchina = models.BooleanField('Крыша Коррозия/ржавчина', default=False)
    krisha_zameneno = models.BooleanField('Крыша Заменено', default=False)

    kapot = models.NullBooleanField('Капот', null=True)
    kapot_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    kapot_okrasheno = models.BooleanField('Капот Окрашено', default=False)
    kapot_skol_carapina = models.BooleanField('Капот Скол/царапина', default=False)
    kapot_vmyatina = models.BooleanField('Капот Вмятина', default=False)
    kapot_corozia_rgavchina = models.BooleanField('Капот Коррозия/ржавчина', default=False)
    kapot_zameneno = models.BooleanField('Капот Заменено', default=False)

    bagagnik = models.NullBooleanField('Багажник', null=True)
    bagagnik_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    bagagnik_okrasheno = models.BooleanField('Багажник Окрашено', default=False)
    bagagnik_skol_carapina = models.BooleanField('Багажник Скол/царапина', default=False)
    bagagnik_vmyatina = models.BooleanField('Багажник Вмятина', default=False)
    bagagnik_corozia_rgavchina = models.BooleanField('Багажник Коррозия/ржавчина', default=False)
    bagagnik_zameneno = models.BooleanField('Багажник Заменено', default=False)

    peredni_bamper = models.NullBooleanField('Передний бампер', null=True)
    peredni_bamper_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    peredni_bamper_okrasheno = models.BooleanField('Передний бампер Окрашено', default=False)
    peredni_bamper_skol_carapina = models.BooleanField('Передний бампер Скол/царапина', default=False)
    peredni_bamper_vmyatina = models.BooleanField('Передний бампер Вмятина', default=False)
    peredni_bamper_corozia_rgavchina = models.BooleanField('Передний бампер Коррозия/ржавчина', default=False)
    peredni_bamper_zameneno = models.BooleanField('Передний бампер Заменено', default=False)

    zadni_bamper = models.NullBooleanField('Задний бампер', null=True)
    zadni_bamper_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zadni_bamper_okrasheno = models.BooleanField('Задний бампер Окрашено', default=False)
    zadni_bamper_skol_carapina = models.BooleanField('Задний бампер Скол/царапина', default=False)
    zadni_bamper_vmyatina = models.BooleanField('Задний бампер Вмятина', default=False)
    zadni_bamper_corozia_rgavchina = models.BooleanField('Задний бампер Коррозия/ржавчина', default=False)
    zadni_bamper_zameneno = models.BooleanField('Задний бампер Заменено', default=False)

    perednee_levoe_krilo = models.NullBooleanField('Переднее левое крыло', null=True)
    perednee_levoe_krilo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    perednee_levoe_krilo_okrasheno = models.BooleanField('Переднее левое крыло Окрашено', default=False)
    perednee_levoe_krilo_skol_carapina = models.BooleanField('Переднее левое крыло Скол/царапина', default=False)
    perednee_levoe_krilo_vmyatina = models.BooleanField('Переднее левое крыло Вмятина', default=False)
    perednee_levoe_krilo_corozia_rgavchina = models.BooleanField('Переднее левое крыло Коррозия/ржавчина', default=False)
    perednee_levoe_krilo_zameneno = models.BooleanField('Переднее левое крыло Заменено', default=False)

    perednee_pravoe_krilo = models.NullBooleanField('Переднее правое крыло', null=True)
    perednee_pravoe_krilo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    perednee_pravoe_krilo_okrasheno = models.BooleanField('Переднее правое крыло Окрашено', default=False)
    perednee_pravoe_krilo_skol_carapina = models.BooleanField('Переднее правое крыло Скол/царапина', default=False)
    perednee_pravoe_krilo_vmyatina = models.BooleanField('Переднее правое крыло Вмятина', default=False)
    perednee_pravoe_krilo_corozia_rgavchina = models.BooleanField('Переднее правое крыло Коррозия/ржавчина', default=False)
    perednee_pravoe_krilo_zameneno = models.BooleanField('Переднее правое крыло Заменено', default=False)

    peredneya_levaya_dver = models.NullBooleanField('Передняя левая дверь', null=True)
    peredneya_levaya_dver_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    peredneya_levaya_dver_okrasheno = models.BooleanField('Передняя левая дверь Окрашено', default=False)
    peredneya_levaya_dver_skol_carapina = models.BooleanField('Передняя левая дверь Скол/царапина', default=False)
    peredneya_levaya_dver_vmyatina = models.BooleanField('Передняя левая дверь Вмятина', default=False)
    peredneya_levaya_dver_corozia_rgavchina = models.BooleanField('Передняя левая дверь Коррозия/ржавчина', default=False)
    peredneya_levaya_dver_zameneno = models.BooleanField('Передняя левая дверь Заменено', default=False)

    peredneya_pravaya_dver = models.NullBooleanField('Передняя правая дверь', null=True)
    peredneya_pravaya_dver_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    peredneya_pravaya_dver_okrasheno = models.BooleanField('Передняя правая дверь Окрашено', default=False)
    peredneya_pravaya_dver_skol_carapina = models.BooleanField('Передняя правая дверь Скол/царапина', default=False)
    peredneya_pravaya_dver_vmyatina = models.BooleanField('Передняя правая дверь Вмятина', default=False)
    peredneya_pravaya_dver_corozia_rgavchina = models.BooleanField('Передняя правая дверь Коррозия/ржавчина', default=False)
    peredneya_pravaya_dver_zameneno = models.BooleanField('Передняя правая дверь Заменено', default=False)

    zadnya_levaya_dver = models.NullBooleanField('Задняя левая дверь', null=True)
    zadnya_levaya_dver_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zadnya_levaya_dver_okrasheno = models.BooleanField('Задняя левая дверь Окрашено', default=False)
    zadnya_levaya_dver_skol_carapina = models.BooleanField('Задняя левая дверь Скол/царапина', default=False)
    zadnya_levaya_dver_vmyatina = models.BooleanField('Задняя левая дверь Вмятина', default=False)
    zadnya_levaya_dver_corozia_rgavchina = models.BooleanField('Задняя левая дверь Коррозия/ржавчина', default=False)
    zadnya_levaya_dver_zameneno = models.BooleanField('Задняя левая дверь Заменено', default=False)

    zadnya_pravaya_dver = models.NullBooleanField('Задняя правая дверь', null=True)
    zadnya_pravaya_dver_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zadnya_pravaya_dver_okrasheno = models.BooleanField('Задняя правая дверь Окрашено', default=False)
    zadnya_pravaya_dver_skol_carapina = models.BooleanField('Задняя правая дверь Скол/царапина', default=False)
    zadnya_pravaya_dver_vmyatina = models.BooleanField('Задняя правая дверь Вмятина', default=False)
    zadnya_pravaya_dver_corozia_rgavchina = models.BooleanField('Задняя правая дверь Коррозия/ржавчина', default=False)
    zadnya_pravaya_dver_zameneno = models.BooleanField('Задняя правая дверь Заменено', default=False)

    zadnee_levoe_krilo = models.NullBooleanField('Заднее левое крыло', null=True)
    zadnee_levoe_krilo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zadnee_levoe_krilo_okrasheno = models.BooleanField('Заднее левое крыло Окрашено', default=False)
    zadnee_levoe_krilo_skol_carapina = models.BooleanField('Заднее левое крыло Скол/царапина', default=False)
    zadnee_levoe_krilo_vmyatina = models.BooleanField('Заднее левое крыло Вмятина', default=False)
    zadnee_levoe_krilo_corozia_rgavchina = models.BooleanField('Заднее левое крыло Коррозия/ржавчина', default=False)
    zadnee_levoe_krilo_zameneno = models.BooleanField('Заднее левое крыло Заменено', default=False)

    zadnee_pravoe_krilo = models.NullBooleanField('Заднее правое крыло', null=True)
    zadnee_pravoe_krilo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zadnee_pravoe_krilo_okrasheno = models.BooleanField('Заднее правое крыло Окрашено', default=False)
    zadnee_pravoe_krilo_skol_carapina = models.BooleanField('Заднее правое крыло Скол/царапина', default=False)
    zadnee_pravoe_krilo_vmyatina = models.BooleanField('Заднее правое крыло Вмятина', default=False)
    zadnee_pravoe_krilo_corozia_rgavchina = models.BooleanField('Заднее правое крыло Коррозия/ржавчина', default=False)
    zadnee_pravoe_krilo_zameneno = models.BooleanField('Заднее правое крыло Заменено', default=False)

    levii_porog = models.NullBooleanField('Левый порог', null=True)
    levii_porog_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    levii_porog_okrasheno = models.BooleanField('Левый порог Окрашено', default=False)
    levii_porog_skol_carapina = models.BooleanField('Левый порог Скол/царапина', default=False)
    levii_porog_vmyatina = models.BooleanField('Левый порог Вмятина', default=False)
    levii_porog_corozia_rgavchina = models.BooleanField('Левый порог Коррозия/ржавчина', default=False)
    levii_porog_zameneno = models.BooleanField('Левый порог Заменено', default=False)

    pravii_porog = models.NullBooleanField('Правый порог', null=True)
    pravii_porog_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    pravii_porog_okrasheno = models.BooleanField('Правый порог Окрашено', default=False)
    pravii_porog_skol_carapina = models.BooleanField('Правый порог Скол/царапина', default=False)
    pravii_porog_vmyatina = models.BooleanField('Правый порог Вмятина', default=False)
    pravii_porog_corozia_rgavchina = models.BooleanField('Правый порог Коррозия/ржавчина', default=False)
    pravii_porog_zameneno = models.BooleanField('Правый порог Заменено', default=False)

    lobovoe_steklo = models.NullBooleanField('Лобовое стекло', null=True)
    lobovoe_steklo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    lobovoe_steklo_skol = models.BooleanField('Лобовое стекло Скол', default=False)
    lobovoe_steklo_treshina = models.BooleanField('Лобовое стекло Трещина', default=False)
    lobovoe_steklo_zapotevanie = models.BooleanField('Лобовое стекло Запотевание', default=False)
    lobovoe_steklo_zameneno = models.BooleanField('Лобовое стекло Заменено', default=False)

    perednee_levoe_steklo = models.NullBooleanField('Переднее левое стекло', null=True)
    perednee_levoe_steklo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    perednee_levoe_steklo_skol = models.BooleanField('Переднее левое стекло Скол', default=False)
    perednee_levoe_steklo_treshina = models.BooleanField('Переднее левое стекло Трещина', default=False)
    perednee_levoe_steklo_zapotevanie = models.BooleanField('Переднее левое стекло Запотевание', default=False)
    perednee_levoe_steklo_zameneno = models.BooleanField('Переднее левое стекло Заменено', default=False)

    perednee_pravoe_steklo = models.NullBooleanField('Переднее правое стекло', null=True)
    perednee_pravoe_steklo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    perednee_pravoe_steklo_skol = models.BooleanField('Переднее правое стекло Скол', default=False)
    perednee_pravoe_steklo_treshina = models.BooleanField('Переднее правое стекло Трещина', default=False)
    perednee_pravoe_steklo_zapotevanie = models.BooleanField('Переднее правое стекло Запотевание', default=False)
    perednee_pravoe_steklo_zameneno = models.BooleanField('Переднее правое стекло Заменено', default=False)

    zadnee_levoe_steklo = models.NullBooleanField('Заднее левое стекло', null=True)
    zadnee_levoe_steklo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zadnee_levoe_steklo_skol = models.BooleanField('Заднее левое стекло Скол', default=False)
    zadnee_levoe_steklo_treshina = models.BooleanField('Заднее левое стекло Трещина', default=False)
    zadnee_levoe_steklo_zapotevanie = models.BooleanField('Заднее левое стекло Запотевание', default=False)
    zadnee_levoe_steklo_zameneno = models.BooleanField('Заднее левое стекло Заменено', default=False)

    zadnee_pravoe_steklo = models.NullBooleanField('Заднее правое стекло', null=True)
    zadnee_pravoe_steklo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zadnee_pravoe_steklo_skol = models.BooleanField('Заднее правое стекло Скол', default=False)
    zadnee_pravoe_steklo_treshina = models.BooleanField('Заднее правое стекло Трещина', default=False)
    zadnee_pravoe_steklo_zapotevanie = models.BooleanField('Заднее правое стекло Запотевание', default=False)
    zadnee_pravoe_steklo_zameneno = models.BooleanField('Заднее правое стекло Заменено', default=False)

    zadnee_steklo = models.NullBooleanField('Заднее стекло', null=True)
    zadnee_steklo_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zadnee_steklo_skol = models.BooleanField('Заднее стекло Скол', default=False)
    zadnee_steklo_treshina = models.BooleanField('Заднее стекло Трещина', default=False)
    zadnee_steklo_zapotevanie = models.BooleanField('Заднее стекло Запотевание', default=False)
    zadnee_steklo_zameneno = models.BooleanField('Заднее стекло Заменено', default=False)

    perednie_fonari = models.NullBooleanField('Передние фонари', null=True)
    perednie_fonari_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    perednie_fonari_skol = models.BooleanField('Передние фонари Скол', default=False)
    perednie_fonari_treshina = models.BooleanField('Передние фонари Трещина', default=False)
    perednie_fonari_zapotevanie = models.BooleanField('Передние фонари Запотевание', default=False)
    perednie_fonari_zameneno = models.BooleanField('Передние фонари Заменено', default=False)

    zadnie_fonari = models.NullBooleanField('Задние фонари', null=True)
    zadnie_fonari_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zadnie_fonari_skol = models.BooleanField('Задние фонари Скол', default=False)
    zadnie_fonari_treshina = models.BooleanField('Задние фонари Трещина', default=False)
    zadnie_fonari_zapotevanie = models.BooleanField('Задние фонари Запотевание', default=False)
    zadnie_fonari_zameneno = models.BooleanField('Задние фонари Заменено', default=False)

    # Силовые элементы
    stoika_perednya_levaya = models.NullBooleanField('Стойка передняя левая', null=True)
    stoika_perednya_levaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    stoika_perednya_levaya_okrasheno = models.BooleanField('Стойка передняя левая Окрашено', default=False)
    stoika_perednya_levaya_skol_carapina = models.BooleanField('Стойка передняя левая Скол/царапина', default=False)
    stoika_perednya_levaya_vmyatina = models.BooleanField('Стойка передняя левая Вмятина', default=False)
    stoika_perednya_levaya_corozia_rgavchina = models.BooleanField('Стойка передняя левая Коррозия/ржавчина', default=False)
    stoika_perednya_levaya_zameneno = models.BooleanField('Стойка передняя левая Заменено', default=False)

    stoika_centralynaya_levaya = models.NullBooleanField('Стойка центральная левая', null=True)
    stoika_centralynaya_levaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    stoika_centralynaya_levaya_okrasheno = models.BooleanField('Стойка центральная левая Окрашено', default=False)
    stoika_centralynaya_levaya_skol_carapina = models.BooleanField('Стойка центральная левая Скол/царапина', default=False)
    stoika_centralynaya_levaya_vmyatina = models.BooleanField('Стойка центральная левая Вмятина', default=False)
    stoika_centralynaya_levaya_corozia_rgavchina = models.BooleanField('Стойка центральная левая Коррозия/ржавчина', default=False)
    stoika_centralynaya_levaya_zameneno = models.BooleanField('Стойка центральная левая Заменено', default=False)

    stoika_zadnaya_levaya = models.NullBooleanField('Стойка задняя левая', null=True)
    stoika_zadnaya_levaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    stoika_zadnaya_levaya_okrasheno = models.BooleanField('Стойка задняя левая Окрашено', default=False)
    stoika_zadnaya_levaya_skol_carapina = models.BooleanField('Стойка задняя левая Скол/царапина', default=False)
    stoika_zadnaya_levaya_vmyatina = models.BooleanField('Стойка задняя левая Вмятина', default=False)
    stoika_zadnaya_levaya_corozia_rgavchina = models.BooleanField('Стойка задняя левая Коррозия/ржавчина', default=False)
    stoika_zadnaya_levaya_zameneno = models.BooleanField('Стойка задняя левая Заменено', default=False)

    stoika_perednaya_pravaya = models.NullBooleanField('Стойка передняя правая', null=True)
    stoika_perednaya_pravaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    stoika_perednaya_pravaya_okrasheno = models.BooleanField('Стойка передняя правая Окрашено', default=False)
    stoika_perednaya_pravaya_skol_carapina = models.BooleanField('Стойка передняя правая Скол/царапина', default=False)
    stoika_perednaya_pravaya_vmyatina = models.BooleanField('Стойка передняя правая Вмятина', default=False)
    stoika_perednaya_pravaya_corozia_rgavchina = models.BooleanField('Стойка передняя правая Коррозия/ржавчина', default=False)
    stoika_perednaya_pravaya_zameneno = models.BooleanField('Стойка передняя правая Заменено', default=False)

    stoika_centralnaya_pravaya = models.NullBooleanField('Стойка центральная правая', null=True)
    stoika_centralnaya_pravaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    stoika_centralnaya_pravaya_okrasheno = models.BooleanField('Стойка центральная правая Окрашено', default=False)
    stoika_centralnaya_pravaya_skol_carapina = models.BooleanField('Стойка центральная правая Скол/царапина', default=False)
    stoika_centralnaya_pravaya_vmyatina = models.BooleanField('Стойка центральная правая Вмятина', default=False)
    stoika_centralnaya_pravaya_corozia_rgavchina = models.BooleanField('Стойка центральная правая Коррозия/ржавчина', default=False)
    stoika_centralnaya_pravaya_zameneno = models.BooleanField('Стойка центральная правая Заменено', default=False)

    stoika_zadnyaya_pravaya = models.NullBooleanField('Стойка задняя правая', null=True)
    stoika_zadnyaya_pravaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    stoika_zadnyaya_pravaya_okrasheno = models.BooleanField('Стойка задняя правая Окрашено', default=False)
    stoika_zadnyaya_pravaya_skol_carapina = models.BooleanField('Стойка задняя правая Скол/царапина', default=False)
    stoika_zadnyaya_pravaya_vmyatina = models.BooleanField('Стойка задняя правая Вмятина', default=False)
    stoika_zadnyaya_pravaya_corozia_rgavchina = models.BooleanField('Стойка задняя правая Коррозия/ржавчина', default=False)
    stoika_zadnyaya_pravaya_zameneno = models.BooleanField('Стойка задняя правая Заменено', default=False)

    perednii_longeron_levii = models.NullBooleanField('Передний лонжерон левый', null=True)
    perednii_longeron_levii_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    perednii_longeron_levii_okrasheno = models.BooleanField('Передний лонжерон левый Окрашено', default=False)
    perednii_longeron_levii_skol_carapina = models.BooleanField('Передний лонжерон левый Скол/царапина', default=False)
    perednii_longeron_levii_vmyatina = models.BooleanField('Передний лонжерон левый Вмятина', default=False)
    perednii_longeron_levii_corozia_rgavchina = models.BooleanField('Передний лонжерон левый Коррозия/ржавчина', default=False)
    perednii_longeron_levii_zameneno = models.BooleanField('Передний лонжерон левый Заменено', default=False)

    perednii_longeron_pravii = models.NullBooleanField('Передний лонжерон правый', null=True)
    perednii_longeron_pravii_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    perednii_longeron_pravii_okrasheno = models.BooleanField('Передний лонжерон правый Окрашено', default=False)
    perednii_longeron_pravii_skol_carapina = models.BooleanField('Передний лонжерон правый Скол/царапина', default=False)
    perednii_longeron_pravii_vmyatina = models.BooleanField('Передний лонжерон правый Вмятина', default=False)
    perednii_longeron_pravii_corozia_rgavchina = models.BooleanField('Передний лонжерон правый Коррозия/ржавчина', default=False)
    perednii_longeron_pravii_zameneno = models.BooleanField('Передний лонжерон правый Заменено', default=False)

    chahka_perednaya_levaya = models.NullBooleanField('Чашка передняя левая', null=True)
    chahka_perednaya_levaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    chahka_perednaya_levaya_okrasheno = models.BooleanField('Чашка передняя левая Окрашено', default=False)
    chahka_perednaya_levaya_skol_carapina = models.BooleanField('Чашка передняя левая Скол/царапина', default=False)
    chahka_perednaya_levaya_vmyatina = models.BooleanField('Чашка передняя левая Вмятина', default=False)
    chahka_perednaya_levaya_corozia_rgavchina = models.BooleanField('Чашка передняя левая Коррозия/ржавчина', default=False)
    chahka_perednaya_levaya_zameneno = models.BooleanField('Чашка передняя левая Заменено', default=False)

    chahka_perednaya_pravaya = models.NullBooleanField('Чашка передняя правая', null=True)
    chahka_perednaya_pravaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    chahka_perednaya_pravaya_okrasheno = models.BooleanField('Чашка передняя правая Окрашено', default=False)
    chahka_perednaya_pravaya_skol_carapina = models.BooleanField('Чашка передняя правая Скол/царапина', default=False)
    chahka_perednaya_pravaya_vmyatina = models.BooleanField('Чашка передняя правая Вмятина', default=False)
    chahka_perednaya_pravaya_corozia_rgavchina = models.BooleanField('Чашка передняя правая Коррозия/ржавчина', default=False)
    chahka_perednaya_pravaya_zameneno = models.BooleanField('Чашка передняя правая Заменено', default=False)

    motornii_chit = models.NullBooleanField('Моторный щит', null=True)
    motornii_chit_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    motornii_chit_okrasheno = models.BooleanField('Моторный щит Окрашено', default=False)
    motornii_chit_skol_carapina = models.BooleanField('Моторный щит Скол/царапина', default=False)
    motornii_chit_vmyatina = models.BooleanField('Моторный щит Вмятина', default=False)
    motornii_chit_corozia_rgavchina = models.BooleanField('Моторный щит Коррозия/ржавчина', default=False)
    motornii_chit_zameneno = models.BooleanField('Моторный щит Заменено', default=False)

    centralnii_tonel = models.NullBooleanField('Центральный тоннель', null=True)
    centralnii_tonel_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    centralnii_tonel_okrasheno = models.BooleanField('Центральный тоннель Окрашено', default=False)
    centralnii_tonel_skol_carapina = models.BooleanField('Центральный тоннель Скол/царапина', default=False)
    centralnii_tonel_vmyatina = models.BooleanField('Центральный тоннель Вмятина', default=False)
    centralnii_tonel_corozia_rgavchina = models.BooleanField('Центральный тоннель Коррозия/ржавчина', default=False)
    centralnii_tonel_zameneno = models.BooleanField('Центральный тоннель Заменено', default=False)

    pol = models.NullBooleanField('Пол', null=True)
    pol_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    pol_okrasheno = models.BooleanField('Пол Окрашено', default=False)
    pol_skol_carapina = models.BooleanField('Пол Скол/царапина', default=False)
    pol_vmyatina = models.BooleanField('Пол Вмятина', default=False)
    pol_corozia_rgavchina = models.BooleanField('Пол Коррозия/ржавчина', default=False)
    pol_zameneno = models.BooleanField('Пол Заменено', default=False)

    chahka_zadnaya_levaya = models.NullBooleanField('Задняя чашка левая', null=True)
    chahka_zadnaya_levaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    chahka_zadnaya_levaya_okrasheno = models.BooleanField('Задняя чашка левая Окрашено', default=False)
    chahka_zadnaya_levaya_skol_carapina = models.BooleanField('Задняя чашка левая Скол/царапина', default=False)
    chahka_zadnaya_levaya_vmyatina = models.BooleanField('Задняя чашка левая Вмятина', default=False)
    chahka_zadnaya_levaya_corozia_rgavchina = models.BooleanField('Задняя чашка левая Коррозия/ржавчина', default=False)
    chahka_zadnaya_levaya_zameneno = models.BooleanField('Задняя чашка левая Заменено', default=False)

    chahka_zadnaya_pravaya = models.NullBooleanField('Задняя чашка правая', null=True)
    chahka_zadnaya_pravaya_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    chahka_zadnaya_pravaya_okrasheno = models.BooleanField('Задняя чашка правая Окрашено', default=False)
    chahka_zadnaya_pravaya_skol_carapina = models.BooleanField('Задняя чашка правая Скол/царапина', default=False)
    chahka_zadnaya_pravaya_vmyatina = models.BooleanField('Задняя чашка правая Вмятина', default=False)
    chahka_zadnaya_pravaya_corozia_rgavchina = models.BooleanField('Задняя чашка правая Коррозия/ржавчина', default=False)
    chahka_zadnaya_pravaya_zameneno = models.BooleanField('Задняя чашка правая Заменено', default=False)

    pol_bagagnik = models.NullBooleanField('Пол багажника', null=True)
    pol_bagagnik_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    pol_bagagnik_okrasheno = models.BooleanField('Пол багажника Окрашено', default=False)
    pol_bagagnik_skol_carapina = models.BooleanField('Пол багажника Скол/царапина', default=False)
    pol_bagagnik_vmyatina = models.BooleanField('Пол багажника Вмятина', default=False)
    pol_bagagnik_corozia_rgavchina = models.BooleanField('Пол багажника Коррозия/ржавчина', default=False)
    pol_bagagnik_zameneno = models.BooleanField('Пол багажника Заменено', default=False)

    kuzov_comment = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)


    # Двигатель и трансмиссия
    postor_shumi = models.NullBooleanField('Посторонние шумы', null=True)
    postor_shumi_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zapotev_techi = models.NullBooleanField('Запотевание/течи', null=True)
    zapotev_techi_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    ravnom_rab = models.NullBooleanField('Работа ДВС', null=True)
    ravnom_rab_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    ur_masla = models.NullBooleanField('Уровень масла', null=True)
    ur_masla_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    ur_ohl_zhidk = models.NullBooleanField('Уровень жидкости охлаждающей системы', null=True)
    ur_ohl_zhidk_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    tr_obsl = models.NullBooleanField('Обслуживание ДВС', null=True)
    tr_obsl_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    sost_priv_rem = models.NullBooleanField('Состояние приводных ремней', null=True)
    sost_priv_rem_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    rabota_kpp = models.NullBooleanField('Работа КПП', null=True)
    rabota_kpp_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    dvig_diag_comment = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    # Рулевое управление, подвеска и тормозная система
    izn_ptk = models.NullBooleanField('Остаток ПТК', null=True)
    izn_ptk_range = models.PositiveSmallIntegerField('Остаток ПТК', blank=True, null=True)
    izn_ptk_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    izn_ztk = models.NullBooleanField('Остаток ЗТК', null=True)
    izn_ztk_range = models.PositiveSmallIntegerField('Остаток ЗТК', blank=True, null=True)
    izn_ztk_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    izn_ptd = models.NullBooleanField('Остаток ПТД', null=True)
    izn_ptd_range = models.PositiveSmallIntegerField('Остаток ПТД', blank=True, null=True)
    izn_ptd_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    izn_ztd = models.NullBooleanField('Остаток ЗТД', null=True)
    izn_ztd_range = models.PositiveSmallIntegerField('Остаток ЗТД', blank=True, null=True)
    izn_ztd_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    izn_rez_leto = models.NullBooleanField('Остаток летних шин', null=True)
    izn_rez_leto_range = models.PositiveSmallIntegerField('Остаток летних шин', blank=True, null=True)
    izn_rez_leto_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    izn_rez_zima = models.NullBooleanField('Остаток зимних шин', null=True)
    izn_rez_zima_range = models.PositiveSmallIntegerField('Остаток зимних шин', blank=True, null=True, default=None)
    izn_rez_zima_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    ur_tormoz_zh = models.NullBooleanField('Уровень тормозной жидкости', null=True)
    ur_tormoz_zh_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    ur_gur_zh = models.NullBooleanField('Уровень жидкости ГУР', null=True)
    ur_gur_zh_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    sost_st_torm = models.NullBooleanField('Стояночный тормоз', null=True)
    sost_st_torm_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    sost_rez = models.NullBooleanField('Состояние шин', null=True)
    sost_rez_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    sost_torm_zh = models.NullBooleanField('Тормозная жидкость', null=True)
    sost_torm_zh_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    stuki_skr = models.NullBooleanField('Посторонние стуки и скрипы', null=True)
    stuki_skr_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    otkl_pr_dvig = models.NullBooleanField('Прямолинейность движения', null=True)
    otkl_pr_dvig_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    luft_rul_uprav = models.NullBooleanField('Люфты в рулевом управлении', null=True)
    luft_rul_uprav_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    rul_uprav_comment = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    # Компьютерная диагностика
    read_vin = models.NullBooleanField('Чтение VIN из ЭБУ', null=True)
    read_vin_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    connect_ebu = models.NullBooleanField('Связь со всеми блоками ЭБУ', null=True)
    connect_ebu_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    error_ebu = models.NullBooleanField('Ошибки в ЭБУ', null=True)
    error_ebu_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    number_key_ebu = models.NullBooleanField('Соответствие прописанных ключей с фактическим', null=True)
    number_key_ebu_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    comp_diag_comment = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    # Электрика и безопасность/отопление и вентиляция
    svet_indik = models.NullBooleanField('Освещение и индикация', null=True)
    svet_indik_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    steklopod = models.NullBooleanField('Стеклоподъемники', null=True)
    steklopod_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    omyvatel = models.NullBooleanField('Очиститель/омыватель', null=True)
    omyvatel_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    el_priv_sal = models.NullBooleanField('Эл. привод (сидений, зеркал и т.д.)/Подогрев', null=True)
    el_priv_sal_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    el_komp_other = models.NullBooleanField('Прочие электронные компоненты и устройства', null=True)
    el_komp_other_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    ispr_srs = models.NullBooleanField('Система SRS (подушки безопасности)', null=True)
    ispr_srs_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    remni_bezopas = models.NullBooleanField('Ремни безопасности', null=True)
    remni_bezopas_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    ispr_ac = models.NullBooleanField('Система A/C (кондиционер)', null=True)
    ispr_ac_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    elektro_comment = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    # Состояние салона
    salon_defects = models.NullBooleanField('Дефекты', null=True)
    salon_defects_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    salon_clear = models.NullBooleanField('Чистота', null=True)
    salon_clear_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    sost_salon_comment = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    # Таблички и маркировки VIN
    vin_comment = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    # Осмотр на подьемнике
    lufts = models.NullBooleanField('Люфты (шаровые, рулевые наконечники, тяги, подшипники)', null=True)
    lufts_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    silentblocks = models.NullBooleanField('Состояние сайлентблоков', null=True)
    silentblocks_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    tormoz_defects = models.NullBooleanField('Тормозная система (шланги, трубки, суппорта)', null=True)
    tormoz_defects_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    sost_opor = models.NullBooleanField('Состояние опор (ДВС, КПП, раздаточной КПП, редуктора и т.д.)', null=True)
    sost_opor_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    zapotev_tech = models.NullBooleanField('Запотевания', null=True)
    zapotev_tech_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    sost_pylnikov = models.NullBooleanField('Состояние пыльников', null=True)
    sost_pylnikov_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    sost_vyhlop = models.NullBooleanField('Состояние выхлопной системы', null=True)
    sost_vyhlop_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    silov_el = models.NullBooleanField('Состояние силовых элементов кузова', null=True)
    silov_el_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)
    podyemnik_comment = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    # Дополнительное оборудование
    dop_oborudovanie = models.NullBooleanField('Дополнительное оборудование', null=True)
    dop_oborudovanie_com = models.CharField('Комментарий', max_length=2000, blank=True, null=True, default=None)

    data = JSONField('Данные', default=dict, blank=True)

    cost_before = models.PositiveIntegerField('Стоимость до торга', blank=True, null=True, default=None)
    cost_after = models.PositiveIntegerField('Стоимость после торга', blank=True, null=True, default=None)
    cost_estimated = models.PositiveIntegerField('Оценочная стоимость', blank=True, null=True, default=None)
    pluses = models.TextField('Плюсы', blank=True, default='')
    minuses = models.TextField('Минусы', blank=True, default='')
    recommended = models.NullBooleanField('Рекомендован', blank=True, null=True)
    published = models.NullBooleanField('Опубликован', blank=True, null=True)
    category = models.IntegerField('Категория авто', blank=True, null=True, default=None)
    result_expert = models.TextField('Вывод эксперта', blank=True, default='')
    comment_expert = models.TextField('Комментарий эксперта', blank=True, default='')
    comment_manager = models.TextField('Комментарий руководителя', blank=True, default='')
    notifications = models.ManyToManyField(Notification, blank=True, default=None, verbose_name='Оповещения', related_name="report_notifications")

    # статус выгрузки ftp
    upload_ftp_status = models.BooleanField('Статус выгрузки', blank=True, default=True)
    # статус успешной загрузки ftp
    success_upload_status = models.BooleanField('Успех загрузки', blank=True, default=False)

    field_history = CustomFieldHistoryTracker(
        ['auto', 'order', 'links', 'mileage', 'cost_before', 'cost_after', 'cost_estimated', 'number_of_hosts',
         'recommended', 'status', 'report_type', 'executor',
         'pluses', 'minuses', 'published', 'category', 'result_expert', 'comment_expert', 'comment_manager'])

    class Meta:
        verbose_name = "отчет"
        verbose_name_plural = "отчеты"
        ordering = ["-created"]

    def auto_add_photo_from_report(self, photo):
        try:
            if photo.image:
                auto_photo = AutoPhoto(auto_id=self.auto.id, image=photo.image)
                auto_photo.save()
            if photo.embed_url:
                auto_photo = AutoPhoto(auto_id=self.auto.id, embed_url=photo.embed_url)
                auto_photo.save()
        except AttributeError as ex:
            pass

    def get_auto_avatar(self):
        photo = AutoPhoto.objects.filter(auto_id=self.auto.id).last()
        return photo.image_google

    def get_first_photo(self):
        photo = self.photos_auto.last()
        return photo

    def get_media_base(self):
        return get_current_site(None).domain

    def get_all_photos(self):
        return self.photos_auto.all()

    def set_google_album_url(self):
        """

        Розшаривает альбом и сохраняет ссылку на него

        :used module:
            google_photo.utils function share_google_album
        """
        if not self.google_album_url and self.google_album:
            self.google_album_url = share_google_album(self.google_album)['shareInfo']['shareableUrl']
            self.save()


    def set_google_album(self):
        """
        Создает и добавляет id гугл альбома если он еще не установлен для отчета. Вызывает self.save()

        :used module:
            google_photo.utils function creat_google_album

        """
        if not self.google_album and self.id:
            self.google_album = creat_google_album(str(self.id))['id']
            self.save()

    def save(self, *args, **kwargs):
        created = False if self.id else True
        old_obj = ReportPodbor.all_objects.get(id=self.id) if not created else None

        super(ReportPodbor, self).save(*args, **kwargs)

        #Добавление гугл альбома
        self.set_google_album()
        self.set_google_album_url()

        try:
            photo = self.auto.photos_auto.all().values()[0]['id']
        except (KeyError, IndexError, AttributeError) as ex:
            try:
                photo_report = self.photos_auto.filter(photo_type=report_constant.PHOTOFRONTVIEWS).first()
            except (IndexError, AttributeError) as ex:
                print(ex)
        try:
            if self.published and not old_obj.published:
                orders = self.order.all()
                for order in orders:
                    data = {
                        'order_id': order.number_buh,
                        'order_url': self.get_full_url()
                    }
                    send_notification([order.client], Notification.NEW_REPORT, data=data, channels=['email', 'sms', 'lk'])
        except AttributeError as ex:
            logger.error(ex)

        if not created:
            recheck = self.report_type != old_obj.report_type and \
                      self.get_report_type_display() == 'перепроверка'

            closing = self.report_type != old_obj.report_type and \
                      self.get_report_type_display() == 'сдача'

            if recheck and self.executor:
                orders = self.order.all()
                for order in orders:
                    data = {
                        'order_id': order.number_buh,
                        'order_url': self.get_full_url()
                    }

                    send_notification([self.executor], Notification.CHECK_ORDER, data=data, channels=['lk'])

            if closing:
                orders = self.order.all()
                recipients = User.objects.filter(Q(filials__in=self.executor.filials.all(), groups__name='Бухгалтер') |
                                                 Q(filials__in=self.executor.filials.all(), groups__name='Контент-менеджер'))
                for order in orders:
                    data = {'order_id': order.number_buh}
                    send_notification(recipients, Notification.CLOSE_ORDER, data=data, channels=['lk'])

            try:
                filials = self.executor.filials.all()
            except AttributeError:
                pass
            else:
                mark = None
                try:
                    mark = self.auto.mark_auto.name
                except AttributeError as ex:
                    logger.error(ex)
                model = None
                try:
                    model = self.auto.model_auto.name
                except AttributeError as ex:
                    logger.error(ex)

                data = {
                    'order_url_name': '{}'.format(self.get_full_url()),
                    'mark': mark,
                    'model': model
                }

                if self.recommended and not old_obj.recommended:
                    for filial in filials:
                        for lider in filial.leaders:
                            logger.error(lider)
                            send_notification([lider], Notification.ADVICE_REPORT, data=data, channels=['email', 'lk'])
                if  not self.recommended and closing:
                    for filial in filials:
                        for lider in filial.leaders:
                            send_notification([lider], Notification.NOT_ADVICE_REPORT, data=data, channels=['lk'])

    def __str__(self):
        # return self.id
        return self.auto.__str__() if self.auto else "{}".format(self.id)

    def get_absolute_url(self):
        return reverse('report:report_view', kwargs={'report_id': self.id})

    def get_full_url(self):
        host = Site.objects.get_current().domain
        return 'https://{host}{abs_url}'.format(host=host, abs_url=self.get_absolute_url())

    def img_url_fetcher(self,url):
        if url.startswith('file:') and url.find(settings.BASE_DIR) < 0 :
            return weasyprint.default_url_fetcher('file://'+settings.BASE_DIR+url[5:])
        return weasyprint.default_url_fetcher(url)

    def check_photos_is_local(self):
        if self.photos_auto.filter(image__exact='').count():
            return False
        return True

    def render_pdf( self ):
        template = get_template('report/pdf_report.html')
        render_html = template.render({"report": self, 'site': settings.URL, 'check_photos_is_local': self.check_photos_is_local()})
        pdf_file = HTML(string=render_html,url_fetcher=self.img_url_fetcher,base_url='').write_pdf(stylesheets=[CSS(settings.BASE_DIR +  '/static/css/report_pdf.css')],zoom=1.1)
        return pdf_file

    def get_photo_type_list(self):
        photo_type_number = self.photos_auto.values_list('photo_type', flat=True).order_by('photo_type')
        return {'photo_type_number': photo_type_number,
                'report_constant': report_constant}


class Steps(models.Model):
    data = JSONField2(verbose_name="шаги", null=True, blank=True)

    class Meta:
        verbose_name = "Шаги отчета"
        verbose_name_plural = "Шаги отчета"


class Photo(models.Model):
    photo_type = models.PositiveSmallIntegerField('Тип', blank=True, choices=report_constant.PHOTO_TYPE, default=0)
    report_podbor = models.ForeignKey(ReportPodbor, blank=True, null=True, default=None, verbose_name='Отчёт', related_name='photos_auto')
    embed_url = models.URLField('embed url', max_length=256, default='')
    image_google = models.TextField('google id', blank=True, null=True, default='')
    image = ProcessedImageField(upload_to=upload_auto, blank=True, null=True, verbose_name='Фото (несколько)',
                                spec=OriginalSpec, autoconvert=None, max_length=250)
    image_small = ImageSpecField(source='image', spec=ThumbnailSpec)
    from_inspection = models.BooleanField('С места осмотра', blank=True, default=False)

    googlePhoto = models.ForeignKey(GooglePhotoImage, blank=True, null=True, default=None, verbose_name='Картинка Google', related_name='photos_google')
    created = models.DateTimeField('Дата создания', auto_now_add=True, db_index=True)

    class Meta:
        verbose_name = "Фото отчета"
        verbose_name_plural = "Фото отчетов"

    @property
    def image_full_google(self):
        if self.image_google:
            return get_media_url_by_id(self.image_google, 3500, 3500)
        else:
            if self.googlePhoto:
                return '/google_photo/google_images_obj/'+str(self.googlePhoto.id)+'/'
            else:
                if self.image:
                    return self.image.url
                else:
                    return '#none'

    @property
    def image_small_google(self):
        if self.image_google:
            return get_media_url_by_id(self.image_google, 150, 150)
        else:
            if self.googlePhoto:
                return '/google_photo/google_images_obj/'+str(self.googlePhoto.id)+'/150/150/'
            else:
                if self.image_small:
                    return self.image_small.url
                else:
                    return '#none'

    def delete(self, *args, **kwargs):
        if self.image_google:
            delete_media_by_id(self.image_google, self.report_podbor.google_album)
        if self.googlePhoto:
            self.googlePhoto.delete()
        super(Photo, self).delete(*args, **kwargs)
        pass

@receiver(models.signals.post_delete, sender=Photo, weak=False)
def delete_photo(sender, instance, **kwargs):
    path_to_photo = None
    try:
        path_to_photo = instance.image.path
    except:
        print('no file ')
        path_to_photo = None
    path_to_image_small = None
    try:
        path_to_image_small = instance.image_small.path
    except:
        print('no cache file ')
        path_to_image_small = None
    if path_to_photo is not None:
        print(str(path_to_photo) + ' ___ try_to_delete')
        if os.path.exists(path_to_photo):
            if os.path.isfile(path_to_photo):
                print(str(path_to_photo) + '  ____deleted')
                os.remove(path_to_photo)
            else:
                print ('not a file')
        else:
            print ('not exist')
    if path_to_image_small is not None:
        print(str(path_to_image_small) + ' ___ try_to_delete')
        if os.path.exists(path_to_image_small):
            if os.path.isfile(path_to_image_small):
                print(str(path_to_image_small) + '  ____deleted')
                os.remove(path_to_image_small)
            else:
                print ('not a file')
        else:
            print ('not exist')


class SurrenderPhoto(models.Model):
    surrender_podbor = models.ForeignKey(ReportPodbor, blank=True, null=True, default=None, verbose_name='Отчёт со сдачи', related_name='surrphotos_auto')
    image = ProcessedImageField(upload_to=upload_auto, blank=True, null=True, verbose_name='Фото (несколько)',
                                spec=OriginalSpec, autoconvert=None)
    image_small = ImageSpecField(source='image', spec=ThumbnailSpec)
    from_inspection = models.BooleanField('со сдачи', blank=True, default=False)

    class Meta:
        verbose_name = "Фото со сдачи"
        verbose_name_plural = "Фото со сдачи"

class OrderReport (models.Model):

    class Meta:
        verbose_name = "Заказы отчетов"

    report = models.ForeignKey(
        ReportPodbor, blank=True, null=True, default=None, verbose_name='Отчет')
    created = models.DateTimeField(
        'Дата создания', auto_now_add=True, db_index=True)
    email = models.CharField(
        'email', max_length=55, default='', null=False, blank=False)
    name = models.CharField(
        'Имя', max_length=55, default='', null=False, blank=False)
    phone = models.CharField(
        'Телефон', max_length=55, default='', null=False, blank=False)
    price = models.PositiveIntegerField('Стоимость за которую продали', blank=False, null=False, default=0)
    status = models.CharField(
        'Статус платежа', max_length=55, default='prepare', null=False, blank=False)

    def __str__(self):
        return self.email
    def save(self, *args, **kwargs):
        if self.status == 'success':
            report = ReportPodbor.objects.get(id=self.report.id)
            if report:
                balance = report.executor.balance
                balance = balance + ( settings.ECOMMPAY_REPORT_PRICE / 100 ) * ( settings.EXECUTOR_PERSENT / 100 )
                report.executor.balance = balance
                report.executor.save()
                pdf = report.render_pdf()
                message = EmailMessage(
                    'Отчет по автомобилю',
                    'Отчет прикреплен к письму.',
                    'info@ap4u.ru',
                    [self.email],
                )
                message.attach('report.pdf', pdf, 'application/pdf')
                message.send()
        super(OrderReport, self).save(*args, **kwargs)


class ReportAutoLibrary(models.Model):

    class Meta:
        verbose_name = "Отчеты в автотеке"
        verbose_name_plural = "Отчеты в автотеке"

    created = models.DateTimeField(
        'Дата создания', auto_now_add=True, db_index=True)
    reports_data = JSONField(default=dict, blank=True)
    file_name = models.CharField('Название файла', max_length=150, blank=True, null=True, default=None, db_index=True)

    def __str__(self):
        return self.file_name + ' (Время отправки: {})'.format(self.created.strftime(settings.DATETIME_FORMAT))