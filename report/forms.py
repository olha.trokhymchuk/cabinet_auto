import re

from django import forms
from core.forms import MyModelForm


from .models import ReportPodbor


class PodborForm(MyModelForm):
    photos_auto = forms.ImageField(label='Фотографии', widget=forms.FileInput(attrs={'multiple': 'multiple'}), required=False)
    class Meta:
        model = ReportPodbor
        exclude = ('executor', )
            
    def __init__(self, *args, **kwargs):
        super(PodborForm, self).__init__(*args, **kwargs)
        try:
            if('cost_after' in self.data):
                val = int(re.sub(r'\D', '', str(self.data['cost_after'])))
                data = self.data.copy()
                data['cost_after'] = val
                self.data = data
        except ValueError as ex:
            print(ex)
        try:
            if('cost_before' in self.data):
                val = int(re.sub(r'\D', '', str(self.data['cost_before'])))
                data = self.data.copy()
                data['cost_before'] = val
                self.data = data
        except ValueError as ex:
            print(ex)
        try:
            if('cost_estimated' in self.data):
                val = int(re.sub(r'\D', '', str(self.data['cost_estimated'])))
                data = self.data.copy()
                data['cost_estimated'] = val
                self.data = data
        except ValueError as ex:
            print(ex)
        try:
            if('mileage' in self.data):
                val = int(re.sub(r'\D', '', str(self.data['mileage'])))
                data = self.data.copy()
                data['mileage'] = val
                self.data = data
        except ValueError as ex:
            print(ex)


