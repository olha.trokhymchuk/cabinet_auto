from report.models import Photo, ReportPodbor
import report.report_constants as report_constant
from django.shortcuts import get_object_or_404


def add_google_photo_to_report(image_id, report_id, photo_type):
    photo = Photo()
    photo.photo_type = report_constant.type_photo[photo_type]
    photo.report_podbor = get_object_or_404(ReportPodbor, id=report_id)
    photo.image_google = image_id
    photo.from_inspection = True if photo_type == 'photo_inspection' else False
    photo.save()
