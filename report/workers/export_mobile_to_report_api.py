import os
import ftplib
import json
import re
import shutil
import logging
import time
import datetime
from io import BytesIO
import io
from autopodbor.settings import BASE_DIR

from autopodbor.utils import send_simple_message_mailgun
from PIL import Image, ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True

from django.core.files.uploadedfile import InMemoryUploadedFile
from django.conf import settings
from django.shortcuts import get_object_or_404
from django.core.files import File
from django.core.files.base import ContentFile
from django.db.utils import IntegrityError

from order.models import Order
import order.order_constants as order_const
from report.models import ReportPodbor, Photo
from report.serializers import ExportMobileReportSerializerCreateUpdateV3

from core.models import User

import report.report_constants as report_constant

from auto.models import AutoPhoto, MarkAuto, ModelAuto, Generation
from auto.serializers import MarkAutoSerializer, ModelAutoSerializer, GenerationAutoSerializer
import auto.glossary as auto_glossary

from report.workers.ftpHelper import FtpHelper

from core.utils import add_status

from google_photo.utils import upload_media_to_google

logger = logging.getLogger(__name__)

report_init = {
    "jur_comment": 'Комментарий1',
    "krisha": "Крыша",
    "kapot": 'Капот',
    "bagagnik": 'Багажник',
    "peredni_bamper": 'Передний бампер',
    "zadni_bamper": 'Задний бампер',
    "perednee_levoe_krilo": 'Переднее левое крыло',
    "perednee_pravoe_krilo": 'Переднее правое крыло',
    "peredneya_levaya_dver": 'Передняя левая дверь',
    "peredneya_pravaya_dver": 'Передняя правая дверь',
    "zadnya_levaya_dver": 'Задняя левая дверь',
    "zadnya_pravaya_dver": 'Задняя правая дверь',
    "zadnee_levoe_krilo": 'Заднее левое крыло',
    "zadnee_pravoe_krilo": 'Заднее правое крыло',
    "levii_porog": 'Левый порог',
    "pravii_porog": 'Правый порог',
    "lobovoe_steklo": 'Лобовое стекло',
    "perednee_levoe_steklo": 'Переднее левое стекло',
    "perednee_pravoe_steklo": 'Переднее правое стекло',
    "zadnee_levoe_steklo": 'Заднее левое стекло',
    "zadnee_pravoe_steklo": 'Заднее правое стекло',
    "zadnee_steklo": 'Заднее стекло',
    "perednie_fonari": 'Передние фонари',
    "zadnie_fonari": 'Задние фонари',

    # Силовые элементы
    "stoika_perednya_levaya": 'Стойка передняя левая',
    "stoika_centralynaya_levaya": 'Стойка центральная левая',
    "stoika_zadnaya_levaya": 'Стойка задняя левая',
    "stoika_perednaya_pravaya": 'Стойка передняя правая',
    "stoika_centralnaya_pravaya": 'Стойка центральная правая',
    "stoika_zadnyaya_pravaya": 'Стойка задняя правая',
    "perednii_longeron_levii": 'Передний лонжерон левый',
    "perednii_longeron_pravii": 'Передний лонжерон правый',
    "chahka_perednaya_levaya": 'Чашка передняя левая',
    "chahka_perednaya_pravaya": 'Чашка передняя правая',
    "motornii_chit": 'Моторный щит',
    "centralnii_tonel": 'Центральный тоннель',
    "pol": 'Пол',
    "chahka_zadnaya_levaya": 'Задняя чашка левая',
    "chahka_zadnaya_pravaya": 'Задняя чашка правая',
    "pol_bagagnik": 'Пол багажника',
    "kuzov_comment": 'Комментарий2',

    # Двигатель и трансмиссия
    "postor_shumi": 'Посторонние шумы',
    "zapotev_techi": 'Запотевание/течи',
    "ravnom_rab": 'Работа ДВС',
    "ur_masla": 'Уровень масла',
    "ur_ohl_zhidk": 'Уровень жидкости охлаждающей системы',
    "tr_obsl": 'Обслуживание ДВС',
    "sost_priv_rem": 'Состояние приводных ремней',
    "rabota_kpp": 'Работа КПП',
    "dvig_diag_comment": 'Комментарий3',

    # Рулевое управление, подвеска и тормозная система
    "izn_ptk": 'Остаток ПТК',
    "izn_ztk": 'Остаток ЗТК',
    "izn_ptd": 'Остаток ПТД',
    "izn_ztd": 'Остаток ЗТД',
    "izn_rez_leto": 'Остаток летних шин',
    "izn_rez_zima": 'Остаток зимних шин',
    "ur_tormoz_zh": 'Уровень тормозной жидкости',
    "ur_gur_zh": 'Уровень жидкости ГУР',
    "sost_st_torm": 'Стояночный тормоз',
    "sost_rez": 'Состояние шин',
    "sost_torm_zh": 'Тормозная жидкость',
    "stuki_skr": 'Посторонние стуки и скрипы',
    "otkl_pr_dvig": 'Прямолинейность движения',
    "luft_rul_uprav": 'Люфты в рулевом управлении',
    "rul_uprav_comment": 'Комментарий4',

    # Компьютерная диагностика
    "read_vin": 'Чтение Vin из ЭБУ',
    "connect_ebu": 'Связь со всеми блоками ЭБУ',
    "error_ebu": 'Ошибки в ЭБУ',
    "number_key_ebu": 'Соответствие прописанных ключей с фактическим',
    "comp_diag_comment": 'Комментарий5',

    # Электрика и безопасность/отопление и вентиляция
    "svet_indik": 'Освещение и индикация',
    "steklopod": 'Стеклоподъемники',
    "omyvatel": 'Очиститель/омыватель',
    "el_priv_sal": 'Эл. привод (сидений, зеркал и т.д.)/Подогрев',
    "el_komp_other": 'Прочие электронные компоненты и устройства',
    "ispr_srs": 'Исправность/ Наличие системы SRS',
    "remni_bezopas": 'Ремни безопасности',
    "ispr_ac": 'Исправность системы A/C',
    "elektro_comment": 'Комментарий6',

    # Состояние салона
    "salon_defects": 'Дефекты',
    "salon_clear": 'Чистота',
    "sost_salon_comment": 'Комментарий7',

    # Таблички и маркировки VIN
    "vin_comment": 'Комментарий8',

    # Осмотр на подьемнике
    "lufts": 'Люфты (шаровые, рулевые наконечники, тяги, подшипники)',
    "silentblocks": 'Состояние сайлентблоков',
    "tormoz_defects": 'Тормозная система (шланги, трубки, суппорта)',
    "sost_opor": 'Состояние опор (ДВС, КПП, раздаточной КПП, редуктора и т.д.)',
    "zapotev_tech": 'Запотевания',
    "sost_pylnikov": 'Состояние пыльников',
    "sost_vyhlop": 'Состояние выхлопной системы',
    "silov_el": 'Состояние силовых элементов кузова',
    "podyemnik_comment": 'Комментарий9',

    # Дополнительное оборудование
    "dop_oborudovanie": 'Дополнительное оборудование',

    "cost_before": 'Стоимость до торга',
    "cost_after": 'Стоимость после торга',
    "cost_estimated": 'Оценочная стоимость',
    'mileage': 'Пробег',
    "pluses": 'Плюсы',
    "minuses": 'Минусы',
    "recommended": 'Рекомендуем',
    "published": '?',
    "category": 'Категория авто',
    "result_expert": 'Вывод эксперта',
    "comment_expert": '?',
    "comment_manager": '?',
    "vin": 'Vin',
    "order": 'Заказы',
    "executor": 'phone',
    "id": 'id',

    "auto": {
        'vin': 'Vin',
        'mark_auto': 'Марка',
        'model_auto': 'Модель',
        'generation': 'Поколение',
        'engine_capacity': 'Объем',
        'salon_auto': 'Салон',
        'drive_type': 'Привод',
        'year_auto': 'Дата выпуска',
        'owners': 'Количество владельцев',
        'body_type_auto': 'Кузов',
        'color_auto': 'Цвет',
        'equipment': 'Комплектация',
        'engine_type': 'Двигатель',
        'horsepower': 'Мощность',
        'transmission_type': 'Трансмиссия',
        'color_salon': 'Цвет салона',
        'cost': 'Цена',
        'comment': 'Описание1',
        'author_type': 'Тип продавца',
        'author': 'Имя',
        'phone': 'Телефон',
        'location': 'Местоположение',
        'source': 'Источник',
        'resell': 'Перекуп'
    },
    # Юридическая проверка
    "jur_pts_original": 'ПТС оригинал',
    "jur_pts_original_com": 'Комментарий10',
    "jur_gbdd": 'Проверка по ГИБДД',
    "jur_gbdd_com": 'Комментарий11',
    "jur_fssp": 'Проверка по ФССП',
    "jur_fssp_com": 'Комментарий12',
    "jur_res_zalog": 'Проверка по реестру залогов',
    "jur_res_zalog_com": 'Комментарий13',
    "jur_auto_kod": 'Проверка по Автокоду',
    "jur_auto_kod_com": 'Комментарий14',
    "jur_dop_service": 'Проверка по доп. сервисам',
    "jur_dop_service_com": 'Комментарий15',

}

even_data = ['cost_before', 'jur_comment', 'vin', 'pluses', 'vin_comment',
             'elektro_comment', 'rul_uprav_comment', 'comp_diag_comment',
             'result_expert', 'dvig_diag_comment', 'kuzov_comment',
             'podyemnik_comment', 'sost_salon_comment',
             'category', 'cost_after', 'minuses', 'cost_estimated', 'recommended',
             'jur_pts_original_com', 'jur_gbdd_com', 'jur_fssp_com', 'jur_res_zalog_com',
             'jur_auto_kod_com', 'jur_dop_service_com', 'mileage']


def send_warning(message):
    send_simple_message_mailgun(
        "export_mobile",
        "{}".format(message),
        'info@ap4u.ru',
        "a.chernov@podbor.org"
    )


class EportMobileReportApi():

    def prepare_data(self, data):
        report = {
            "auto": {}
        }
        print (data)
        for item in report_init:
            print ('------')
            print(item)
            print(report_init[item])
            print ('------')
            if type(report_init[item]) == dict:
                for item1 in report_init[item]:
                    try:
                        report[item][item1] = data[report_init[item][item1]]
                    except (KeyError, TypeError) as ex:
                        pass
            else:
                try:
                    if data[report_init[item]] == 'ОК':
                        report[item] = True
                    elif item in even_data:
                        print(data[report_init[item]])
                        report[item] = data[report_init[item]]

                    else:
                        report[item] = False
                        if type(data[report_init[item]]) == str and len(data[report_init[item]]) > 0:
                            report[item + "_com"] = data[report_init[item]]
                        try:
                            report[item + "_okrasheno"] = data[report_init[item]][0]["Окрашено"]
                        except (KeyError, TypeError) as ex:
                            pass
                        try:
                            report[item + "_skol_carapina"] = data[report_init[item]][0]["Скол/Царапина"]
                        except (KeyError, TypeError) as ex:
                            pass
                        try:
                            report[item + "_vmyatina"] = data[report_init[item]][0]["Вмятина"]
                        except (KeyError, TypeError) as ex:
                            pass
                        try:
                            report[item + "_corozia_rgavchina"] = data[report_init[item]][0]["Коррозия/Ржавчина"]
                        except (KeyError, TypeError) as ex:
                            pass
                        try:
                            report[item + "_skol"] = data[report_init[item]][0]["Скол"]
                        except (KeyError, TypeError) as ex:
                            pass
                        try:
                            report[item + "_treshina"] = data[report_init[item]][0]["Трещина"]
                        except (KeyError, TypeError) as ex:
                            pass
                        try:
                            report[item + "_zapotevanie"] = data[report_init[item]][0]["Запотевание"]
                        except (KeyError, TypeError) as ex:
                            pass
                        try:
                            report[item + "_zameneno"] = data[report_init[item]][0]["Заменено"]
                        except (KeyError, TypeError) as ex:
                            pass
                        try:
                            report[item + "_range"] = data[report_init[item]]
                            report[item + "_range"] = data[report_init[item]][0]["Значение"]
                            report[item] = False
                        except (KeyError, TypeError, AttributeError) as ex:
                            pass
                        try:
                            report[item + "_com"] = data[report_init[item]][0]["Коментарий"]
                            if len(data[report_init[item]][0]["Коментарий"]) > 0:
                                pass
                        except (KeyError, TypeError) as ex:
                            continue

                except (KeyError, TypeError) as ex:
                    pass
        report["executor"] = {"phone": data["phone"], "id": data["id"]}
        try:
            report["auto"]["body_type_auto"] = {"name": report["auto"]["body_type_auto"]}
        except KeyError:
            pass
        try:
            report["auto"]["year_auto"] = {"year": report["auto"]["year_auto"]}
        except KeyError:
            pass
        mark_auto = None
        model_auto = None
        generate = None
        try:
            mark_auto = MarkAuto.objects.get(name__iexact=report["auto"]["mark_auto"].lower())
        except MarkAuto.DoesNotExist:
            return False
        try:
            mark_auto_serializer = MarkAutoSerializer(mark_auto, {"name": mark_auto.name})
            mark_auto_serializer.is_valid()
            self.mark_auto_validated_data = mark_auto_serializer.validated_data

        except KeyError:
            pass

        if mark_auto:
            try:
                model_auto = ModelAuto.objects.get(name=report["auto"]["model_auto"], mark_auto=mark_auto)
            except ModelAuto.DoesNotExist:
                return False
            try:
                model_auto_serializer = MarkAutoSerializer(model_auto,
                                                           {"name": model_auto.name, "mark_auto": {"pk": mark_auto.pk}})
                model_auto_serializer.is_valid()
                self.model_auto_validated_data = model_auto_serializer.validated_data
            except KeyError:
                self.model_auto_validated_data = None

        try:
            if model_auto and report["auto"]["generation"] != "None":
                generation = Generation.objects.filter(name=report["auto"]["generation"], model_auto=model_auto).first()
                generation_serializer = MarkAutoSerializer(generation, {"name": generation.name,
                                                                        "model_auto": {"pk": model_auto.pk}})
                generation_serializer.is_valid()
                self.generation_validated_data = generation_serializer.validated_data
        except KeyError:
            self.generation_validated_data = None
        return report

    def create_report(self, data):
        print('create')
        self.mobile_data = data
        data = self.prepare_data(data)
        print(data)
        try:
            self.vin = data["vin"]
        except KeyError as ex:
            print('vin error')
            massage = "str - {}\n {}".format(473, str(ex))
            send_warning(massage)
            return False
        try:
            report = ReportPodbor.objects.get(vin=self.vin)
        except ReportPodbor.DoesNotExist:
            report = None
        except ReportPodbor.MultipleObjectsReturned:
            report = ReportPodbor.objects.filter(vin=self.vin).first()
        for item in auto_glossary.choise_data:
            try:
                data["auto"][item] = auto_glossary.choise_data[item][str(data["auto"][item]).lower()]
            except KeyError:
                print('auto failed')
                pass
        try:
            if data["auto"]["engine_capacity"] == 0:
                data["auto"]["engine_capacity"] = 0.0
        except KeyError:
            print('engine failed')
            pass
        data["published"] = True
        serializer = ExportMobileReportSerializerCreateUpdateV3(instance=report, data=data)
        is_valid = serializer.is_valid()
        if is_valid:
            print("valid")
            print(self.vin)
            try:
                serializer.validated_data["auto"]["mark_auto"] = self.mark_auto_validated_data
                serializer.validated_data["auto"]["model_auto"] = self.model_auto_validated_data
                serializer.validated_data["auto"]["generation"] = self.generation_validated_data
            except AttributeError:
                pass

            report = serializer.create(serializer.validated_data)
            self.add_order(report)
            return report
        else:
            print(serializer.errors)
            data = {
                'error': str(serializer.errors)
            }
            massage = "str - {}\n {}".format(520, str(data))
            send_warning(massage)
            return False

    def add_order(self, rep):
        try:
            for o in str(self.mobile_data['Заказы']).split(','):
                try:
                    order = Order.objects.get(pk=int(o))
                    if order:
                        rep.order.add(order)
                        order_const.INWORKFORREPORT
                        rep.save()
                        order.cars.add(rep.auto)
                        if order.order_type in order_const.INWORKFORREPORT:
                            order.status = order_const.DONE
                            order.status_closing = True
                        order.save()
                        add_status(order, 'change-status-crm', 'order')

                except Order.DoesNotExist as ex:
                    logger.error(ex)
        except KeyError as ex:
            massage = "str - {}\n {}".format(579, str(ex))
            send_warning(massage)
        except ValueError as ex:
            massage = "str - {}\n {}".format(582, str(ex))
            send_warning(massage)
        except:
            pass
