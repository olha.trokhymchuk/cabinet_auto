import json
import tempfile
from datetime import datetime, timedelta
from report.models import ReportPodbor, ReportAutoLibrary
from report.workers.create_json_file import ftp_connect, sftp_connect, create_json_report_file


def get_date_now():
    date_now = datetime.now()
    return date_now


def get_reports():
    print('----- start -----')
    reports_id_list = []
    get_report_id_from_auto_library = ReportAutoLibrary.objects.all()
    for report in get_report_id_from_auto_library:

        for data in report.reports_data:
            reports_id_list.append(data['report_id'])

        get_reports = ReportPodbor.objects.filter(id__in=reports_id_list).all()

        date = get_date_now().strftime("%Y%m%d-%H%M%S")
        get_data = create_json_report_file(get_reports)

        if get_data['json_data'] and get_data['dict_for_photos']:
            with tempfile.TemporaryDirectory() as tmp_dirname:
                with tempfile.NamedTemporaryFile(mode='w', suffix='.json',
                                                 prefix=date, dir=tmp_dirname, encoding='utf-8') as out_file:
                    out_file.write(json.dumps(get_data['dict_to_upload']))
                    full_name = report.file_name
                    out_file.seek(0)
                    get_file = open(out_file.name, 'rb')
                    conn_ftp = ftp_connect()
                    conn_ftp.storbinary('STOR ' + 'pub/' + full_name, get_file)
                    conn_ftp.close()
                    print('file name: ', full_name)
                    print('Process upload to ftp finished _______________')
                    sftp_connect().put(out_file.name, '/pub/' + full_name)
                    out_file.close()
                    sftp_connect().close()
                    print('Process upload to sftp finished _______________')
                    ReportAutoLibrary.objects.create(reports_data=report.reports_data, file_name=full_name)
                    return True
        else:
            print('not valid reports')
            pass

        reports_id_list.clear()
    print(' --- finished --- ')