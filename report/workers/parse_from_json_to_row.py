import report.report_constants as report_constant

from report.models import Steps, ReportPodbor, Photo

data_to_row = {
	# Юридическая проверка
	"Фото ГИБДД":  {
		"name": "photo_gibdd",
		"value": "image",
		"photo_type": report_constant.PHOTOGIBDD
	},
	"Фото ФССП":  {
		"name": "photo_fssp",
		"value": "image",
		"photo_type": report_constant.PHOTOFSSP
	},
	"Фото с реестра залогов":  {
		"name": "photo_reestr_zalog",
		"value": "image",
		"photo_type": report_constant.PHOTOREESTR
	},
	"Фото MyCreditInfo":  {
		"name": "photo_mycreditinfo",
		"value": "image",
		"photo_type": report_constant.PHOTOCHANGE
	},
	"Фото Автокод":  {
		"name": "photo_avtokod",
		"value": "image",
		"photo_type": report_constant.PHOTOAVTOKOD
	},
	"Фото Доп. сервисы":  {
		"name": "photo_dopserv",
		"value": "image",
		"photo_type": report_constant.PHOTODOPSERVER
	},

	"Юридическая проверка Комментарий":  "jur_comment",

	# Кузов
	# Внешние элементы
	"Крыша": {
		"name": "krisha",
		"value": "value",
		"comment": "krisha_com",
		"negative": {
			"Окрашено": "krisha_okrasheno",
			"Скол/царапина": "krisha_skol_carapina",
			"Вмятина": "krisha_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "krisha_zameneno"
		}
	},
	"Капот": {
		"name": "kapot",
		"value": "value",
		"comment": "kapot_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "kapot_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Багажник": {
		"name": "bagagnik",
		"value": "value",
		"comment": "bagagnik_com",
		"negative": {
			"Окрашено": "bagagnik_okrasheno",
			"Скол/царапина": "bagagnik_skol_carapina",
			"Вмятина": "bagagnik_vmyatina",
			"Коррозия/ржавчина": "bagagnik_corozia_rgavchina",
			"Заменено": "bagagnik_zameneno"
		}
	},
	"Передний бампер": {
		"name": "peredni_bamper",
		"value": "value",
		"comment": "peredni_bamper_com",
		"negative": {
			"Окрашено": "peredni_bamper_okrasheno",
			"Скол/царапина": "peredni_bamper_skol_carapina",
			"Вмятина": "peredni_bamper_vmyatina",
			"Коррозия/ржавчина": "peredni_bamper_corozia_rgavchina",
			"Заменено": "peredni_bamper_zameneno"
		}
	},
	"Задний бампер": {
		"name": "zadni_bamper",
		"value": "value",
		"comment": "zadni_bamper_com",
		"negative": {
			"Окрашено": "zadni_bamper_okrasheno",
			"Скол/царапина": "zadni_bamper_skol_carapina",
			"Вмятина": "zadni_bamper_vmyatina",
			"Коррозия/ржавчина": "zadni_bamper_corozia_rgavchina",
			"Заменено": "zadni_bamper_zameneno"
		}
	},
	"Переднее левое крыло": {
		"name": "perednee_levoe_krilo",
		"value": "value",
		"comment": "perednee_levoe_krilo_com",
		"negative": {
			"Окрашено": "perednee_levoe_krilo_okrasheno",
			"Скол/царапина": "perednee_levoe_krilo_skol_carapina",
			"Вмятина": "perednee_levoe_krilo_vmyatina",
			"Коррозия/ржавчина": "perednee_levoe_krilo_corozia_rgavchina",
			"Заменено": "perednee_levoe_krilo_zameneno"
		}
	},
	"Переднее правое крыло": {
		"name": "perednee_pravoe_krilo",
		"value": "value",
		"comment": "perednee_pravoe_krilo_com",
		"negative": {
			"Окрашено": "perednee_pravoe_krilo_okrasheno",
			"Скол/царапина": "perednee_pravoe_krilo_skol_carapina",
			"Вмятина": "perednee_pravoe_krilo_vmyatina",
			"Коррозия/ржавчина": "perednee_pravoe_krilo_corozia_rgavchina",
			"Заменено": "perednee_pravoe_krilo_zameneno"
		}
	},
	"Передняя левая дверь": {
		"name": "peredneya_levaya_dver",
		"value": "value",
		"comment": "peredneya_levaya_dver_com",
		"negative": {
			"Окрашено": "peredneya_levaya_dver_okrasheno",
			"Скол/царапина": "peredneya_levaya_dver_skol_carapina",
			"Вмятина": "peredneya_levaya_dver_vmyatina",
			"Коррозия/ржавчина": "peredneya_levaya_dver_corozia_rgavchina",
			"Заменено": "peredneya_levaya_dver_zameneno"
		}
	},
	"Передняя правая дверь": {
		"name": "peredneya_pravaya_dver",
		"value": "value",
		"comment": "peredneya_pravaya_dver_com",
		"negative": {
			"Окрашено": "peredneya_pravaya_dvert_okrasheno",
			"Скол/царапина": "peredneya_pravaya_dver_skol_carapina",
			"Вмятина": "peredneya_pravaya_dver_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "peredneya_pravaya_dver_zameneno"
		}
	},
	"Задняя левая дверь": {
		"name": "zadnya_levaya_dver",
		"value": "value",
		"comment": "zadnya_levaya_dver_com",
		"negative": {
			"Окрашено": "zadnya_levaya_dver_okrasheno",
			"Скол/царапина": "zadnya_levaya_dver_skol_carapina",
			"Вмятина": "zadnya_levaya_dver_vmyatina",
			"Коррозия/ржавчина": "zadnya_levaya_dver_corozia_rgavchina",
			"Заменено": "zadnya_levaya_dver_zameneno"
		}
	},
	"Задняя правая дверь": {
		"name": "zadnya_pravaya_dver",
		"value": "value",
		"comment": "zadnya_pravaya_dver_com",
		"negative": {
			"Окрашено": "zadnya_pravaya_dver_okrasheno",
			"Скол/царапина": "zadnya_pravaya_dver_skol_carapina",
			"Вмятина": "zadnya_pravaya_dver_vmyatina",
			"Коррозия/ржавчина": "zadnya_pravaya_dver_corozia_rgavchina",
			"Заменено": "zadnya_pravaya_dver_zameneno"
		}
	},
	"Заднее левое крыло": {
		"name": "zadnee_levoe_krilo",
		"value": "value",
		"comment": "zadnee_levoe_krilo_com",
		"negative": {
			"Окрашено": "zadnee_levoe_krilo_okrasheno",
			"Скол/царапина": "zadnee_levoe_krilo_skol_carapina",
			"Вмятина": "zadnee_levoe_krilo_vmyatina",
			"Коррозия/ржавчина": "zadnee_levoe_krilo_corozia_rgavchina",
			"Заменено": "zadnee_levoe_krilo_zameneno"
		}
	},
	"Заднее правое крыло": {
		"name": "zadnee_pravoe_krilo",
		"value": "value",
		"comment": "zadnee_pravoe_krilo_com",
		"negative": {
			"Окрашено": "zadnee_pravoe_krilo_okrasheno",
			"Скол/царапина": "zadnee_pravoe_krilo_skol_carapina",
			"Вмятина": "zadnee_pravoe_krilo_vmyatina",
			"Коррозия/ржавчина": "zadnee_pravoe_krilo_corozia_rgavchina",
			"Заменено": "zadnee_pravoe_krilo_zameneno"
		}
	},
	"Левый порог": {
		"name": "levii_porog",
		"value": "value",
		"comment": "levii_porog_com",
		"negative": {
			"Окрашено": "levii_porog_okrasheno",
			"Скол/царапина": "levii_porog_skol_carapina",
			"Вмятина": "levii_porog_vmyatina",
			"Коррозия/ржавчина": "levii_porog_corozia_rgavchina",
			"Заменено": "levii_porog_zameneno"
		}
	},
	"Правый порог": {
		"name": "pravii_porog",
		"value": "value",
		"comment": "pravii_porog_com",
		"negative": {
			"Окрашено": "pravii_porog_okrasheno",
			"Скол/царапина": "pravii_porog_skol_carapina",
			"Вмятина": "pravii_porog_vmyatina",
			"Коррозия/ржавчина": "pravii_porog_corozia_rgavchina",
			"Заменено": "pravii_porog_zameneno"
		}
	},
	"Лобовое стекло": {
		"name": "lobovoe_steklo",
		"value": "value",
		"comment": "lobovoe_steklo_com",
		"negative": {
			"Скол": "lobovoe_steklo_skol",
			"Трещина": "lobovoe_steklo_treshina",
			"Запотевание": "lobovoe_steklo_zapotevanie",
			"Заменено": "lobovoe_steklo_zameneno"
		}
	},
	"Переднее левое стекло": {
		"name": "perednee_levoe_steklo",
		"value": "value",
		"comment": "perednee_levoe_steklo_com",
		"negative": {
			"Скол": "perednee_levoe_steklo_skol",
			"Трещина": "perednee_levoe_steklo_treshina",
			"Запотевание": "perednee_levoe_steklo_zapotevanie",
			"Заменено": "perednee_levoe_steklo_zameneno"
		}
	},
	"Переднее правое стекло": {
		"name": "perednee_pravoe_steklo",
		"value": "value",
		"comment": "perednee_pravoe_steklo_com",
		"negative": {
			"Скол": "perednee_pravoe_steklo_skol",
			"Трещина": "perednee_pravoe_steklo_treshina",
			"Запотевание": "perednee_pravoe_steklo_zapotevanie",
			"Заменено": "perednee_pravoe_steklo_zameneno"
		}
	},
	"Заднее левое стекло": {
		"name": "zadnee_levoe_steklozadnee_levoe_steklo",
		"value": "value",
		"comment": "zadnee_levoe_steklo_com",
		"negative": {
			"Скол": "zadnee_levoe_steklo_skol",
			"Трещина": "zadnee_levoe_steklo_treshina",
			"Запотевание": "zadnee_levoe_steklo_zapotevanie",
			"Заменено": "zadnee_levoe_steklo_zameneno"
		}
	},
	"Заднее правое стекло": {
		"name": "zadnee_pravoe_steklo",
		"value": "value",
		"comment": "zadnee_pravoe_steklo_com",
		"negative": {
			"Скол": "zadnee_pravoe_steklo_skol",
			"Трещина": "zadnee_pravoe_steklo_treshina",
			"Запотевание": "zadnee_pravoe_steklo_zapotevanie",
			"Заменено": "zadnee_pravoe_steklo_zameneno"
		}
	},
	"Заднее стекло": {
		"name": "zadnee_steklo",
		"value": "value",
		"comment": "zadnee_steklo_com",
		"negative": {
			"Скол": "zadnee_steklo_skol",
			"Трещина": "zadnee_steklo_treshina",
			"Запотевание": "zadnee_steklo_zapotevanie",
			"Заменено": "zadnee_steklo_zameneno"
		}
	},
	"Передние фонари": {
		"name": "perednie_fonari",
		"value": "value",
		"comment": "perednie_fonari_com",
		"negative": {
			"Скол": "perednie_fonari_skol",
			"Трещина": "perednie_fonari_treshina",
			"Запотевание": "perednie_fonari_zapotevanie",
			"Заменено": "perednie_fonari_zameneno"
		}
	},
	"Задние фонари": {
		"name": "zadnie_fonari",
		"value": "value",
		"comment": "zadnie_fonari_com",
		"negative": {
			"Скол": "zadnie_fonari_skol",
			"Трещина": "zadnie_fonari_treshina",
			"Запотевание": "zadnie_fonari_zapotevanie",
			"Заменено": "zadnie_fonari_zameneno"
		}
	},

	# Силовые элементы
	"Стойка передняя левая": {
		"name": "stoika_perednya_levaya",
		"value": "value",
		"comment": "stoika_perednya_levaya_com",
		"negative": {
			"Окрашено": "stoika_perednya_levaya_okrasheno",
			"Скол/царапина": "stoika_perednya_levaya_skol_carapina",
			"Вмятина": "stoika_perednya_levaya_vmyatina",
			"Коррозия/ржавчина": "stoika_perednya_levaya_corozia_rgavchina",
			"Заменено": "stoika_perednya_levaya_zameneno"
		}
	},
	"Стойка центральная левая": {
		"name": "stoika_centralynaya_levaya",
		"value": "value",
		"comment": "stoika_centralynaya_levaya_com",
		"negative": {
			"Окрашено": "stoika_centralynaya_levaya_okrasheno",
			"Скол/царапина": "stoika_centralynaya_levaya_skol_carapina",
			"Вмятина": "stoika_centralynaya_levaya_vmyatina",
			"Коррозия/ржавчина": "stoika_centralynaya_levaya_corozia_rgavchina",
			"Заменено": "stoika_centralynaya_levaya_zameneno"
		}
	},
	"Стойка задняя левая": {
		"name": "stoika_zadnaya_levaya",
		"value": "value",
		"comment": "stoika_zadnaya_levaya_com",
		"negative": {
			"Окрашено": "stoika_zadnaya_levaya_okrasheno",
			"Скол/царапина": "stoika_zadnaya_levaya_skol_carapina",
			"Вмятина": "stoika_zadnaya_levaya_vmyatina",
			"Коррозия/ржавчина": "stoika_zadnaya_levaya_corozia_rgavchina",
			"Заменено": "stoika_zadnaya_levaya_zameneno"
		}
	},

	"Стойка передняя правая": {
		"name": "stoika_perednaya_pravaya",
		"value": "value",
		"comment": "stoika_perednaya_pravaya_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Стойка центральная правая": {
		"name": "stoika_centralnaya_pravaya",
		"value": "value",
		"comment": "stoika_centralnaya_pravaya_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Стойка задняя правая": {
		"name": "stoika_zadnyaya_pravaya",
		"value": "value",
		"comment": "stoika_zadnyaya_pravaya_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Передний лонжерон левый": {
		"name": "perednii_longeron_levii",
		"value": "value",
		"comment": "perednii_longeron_levii_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Передний лонжерон правый": {
		"name": "perednii_longeron_pravii",
		"value": "value",
		"comment": "perednii_longeron_pravii_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Чашка передняя левая": {
		"name": "chahka_perednaya_levaya",
		"value": "value",
		"comment": "chahka_perednaya_levaya_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Чашка передняя правая": {
		"name": "chahka_perednaya_pravaya",
		"value": "value",
		"comment": "chahka_perednaya_pravaya_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Моторный щит": {
		"name": "motornii_chit",
		"value": "value",
		"comment": "motornii_chit_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Центральный тоннель": {
		"name": "centralnii_tonel",
		"value": "value",
		"comment": "centralnii_tonel_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Пол": {
		"name": "pol",
		"value": "value",
		"comment": "pol_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Задняя чашка левая": {
		"name": "chahka_zadnaya_levaya",
		"value": "value",
		"comment": "chahka_zadnaya_levaya_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Задняя чашка правая": {
		"name": "chahka_zadnaya_pravaya",
		"value": "value",
		"comment": "chahka_zadnaya_pravaya_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Пол багажника": {
		"name": "pol_bagagnik",
		"value": "value",
		"comment": "pol_bagagnik_com",
		"negative": {
			"Окрашено": "kapot_okrasheno",
			"Скол/царапина": "kapot_skol_carapina",
			"Вмятина": "kapot_vmyatina",
			"Коррозия/ржавчина": "krisha_corozia_rgavchina",
			"Заменено": "kapot_zameneno"
		}
	},
	"Кузов Комментарий":  "comment_kuzov",

	# Двигатель и трансмиссия
	"Посторонние шумы": {
		"name": "postor_shumi",
		"value": "value",
		"comment": "postor_shumi_com",
	},
	"Запотевание/течи": {
		"name": "zapotev_techi",
		"value": "value",
		"comment": "zapotev_techi_com",
	},
	"Работа ДВС": {
		"name": "ravnom_rab",
		"value": "value",
		"comment": "ravnom_rab_com",
	},
	"Уровень масла": {
		"name": "ur_masla",
		"value": "value",
		"comment": "ur_masla_com",
	},
	"Уровень жидкости охлаждающей системы": {
		"name": "ur_ohl_zhidk",
		"value": "value",
		"comment": "ur_ohl_zhidk_com",
	},
	"Обслуживание ДВС": {
		"name": "tr_obsl",
		"value": "value",
		"comment": "tr_obsl_com",
	},
	"Обслуживание ДВС": {
		"name": "tr_obsl",
		"value": "value",
		"comment": "tr_obsl_com",
	},
	"Состояние приводных ремней": {
		"name": "sost_priv_rem",
		"value": "value",
		"comment": "sost_priv_rem_com",
	},
	"Работа КПП": {
		"name": "rabota_kpp",
		"value": "value",
		"comment": "rabota_kpp_com",
	},
	"Двигатель и трансмиссия Комментарий":  "comment_dvig_diag",
	
	# Рулевое управление, подвеска и тормозная система
	"Остаток ПТК": {
		"name": "izn_ptk",
		"value": "value",
		"range": "range",
		"comment": "izn_ptk_com",
	},
	"Остаток ЗТК": {
		"name": "izn_ztk",
		"value": "value",
		"range": "range",
		"comment": "izn_ztk_com",
	},
	"Остаток ПТД": {
		"name": "izn_ptd",
		"value": "value",
		"range": "range",
		"comment": "izn_ptd_com",
	},
	"Остаток ЗТД": {
		"name": "izn_ztd",
		"value": "value",
		"range": "range",
		"comment": "izn_ztd_com",
	},
	"Остаток летних шин": {
		"name": "izn_rez_leto",
		"value": "value",
		"range": "range",
		"comment": "izn_rez_leto_com",
	},
	"Остаток летних шин": {
		"name": "izn_rez_zima",
		"value": "value",
		"range": "range",
		"comment": "izn_rez_zima_com",
	},
	"Уровень тормозной жидкости": {
		"name": "ur_tormoz_zh",
		"value": "value",
		"range": "range",
		"comment": "ur_tormoz_zh_com",
	},
	"Уровень жидкости ГУР": {
		"name": "ur_gur_zh",
		"value": "value",
		"range": "range",
		"comment": "ur_gur_zh_com",
	},
	"Стояночный тормоз": {
		"name": "sost_st_torm",
		"value": "value",
		"range": "range",
		"comment": "sost_st_torm_com",
	},
	"Состояние шин": {
		"name": "sost_rez",
		"value": "value",
		"range": "range",
		"comment": "sost_rez_com",
	},
	"Тормозная жидкость": {
		"name": "sost_torm_zh",
		"value": "value",
		"range": "range",
		"comment": "sost_torm_zh_com",
	},
	"Посторонние стуки и скрипы": {
		"name": "stuki_skr",
		"value": "value",
		"range": "range",
		"comment": "stuki_skr_com",
	},
	"Прямолинейность движения": {
		"name": "otkl_pr_dvig",
		"value": "value",
		"range": "range",
		"comment": "otkl_pr_dvig_com",
	},
	"Люфты в рулевом управлении": {
		"name": "luft_rul_uprav",
		"value": "value",
		"range": "range",
		"comment": "luft_rul_uprav_com",
	},
	"Рулевое управление, подвеска и тормозная система Комментарий":  "comment_rul_uprav",
	
	# Компьютерная диагностика
	"Чтение VIN из ЭБУ": {
		"name": "read_vin",
		"value": "value",
		"comment": "read_vin_com",
	},
	"Связь со всеми блоками ЭБУ": {
		"name": "connect_ebu",
		"value": "value",
		"comment": "connect_ebu_com",
	},
	"Ошибки в ЭБУ": {
		"name": "error_ebu",
		"value": "value",
		"comment": "error_ebu_com",
	},
	"Соответствие прописанных ключей с фактическим": {
		"name": "number_key_ebu",
		"value": "value",
		"comment": "number_key_ebu_com",
	},
	"Компьютерная диагностика Комментарий":  "comment_comp_diag",

	# Электрика и безопасность/отопление и вентиляция
	"Освещение и индикация": {
		"name": "svet_indik",
		"value": "value",
		"comment": "svet_indik_com",
	},
	"Стеклоподъемники": {
		"name": "steklopod",
		"value": "value",
		"comment": "steklopod_com",
	},
	"Очиститель/омыватель": {
		"name": "omyvatel",
		"value": "value",
		"comment": "omyvatel_com",
	},
	"Эл. привод (сидений, зеркал и т.д.)/Подогрев": {
		"name": "el_priv_sal",
		"value": "value",
		"comment": "el_priv_sal_com",
	},
	"Прочие электронные компоненты и устройства": {
		"name": "el_komp_other",
		"value": "value",
		"comment": "el_komp_other_com",
	},
	"Система SRS (подушки безопасности)": {
		"name": "ispr_srs",
		"value": "value",
		"comment": "ispr_srs_com",
	},
	"Ремни безопасности": {
		"name": "remni_bezopas",
		"value": "value",
		"comment": "remni_bezopas_com",
	},
	"Система A/C (кондиционер)": {
		"name": "ispr_ac",
		"value": "value",
		"comment": "ispr_ac_com",
	},
	"Электрика и безопасность/отопление и вентиляция Комментарий":  "comment_elektro",

	# Состояние салона
	"Дефекты": {
		"name": "salon_defects",
		"value": "value",
		"comment": "salon_defects_com",
	},
	"Чистота": {
		"name": "salon_clear",
		"value": "value",
		"comment": "salon_clear_com",
	},
	"Состояние салона Комментарий":  "comment_sost_salon",

	# Таблички и маркировки VIN
	"Vin код на кузове/раме №1":  {
		"name": "photo_dopserv",
		"value": "image",
		"photo_type": report_constant.PHOTOVIN1
	},
	"Vin код на кузове/раме №2":  {
		"name": "photo_dopserv",
		"value": "image",
		"photo_type": report_constant.PHOTOVIN2
	},
	"Vin код на кузове/раме №3":  {
		"name": "photo_dopserv",
		"value": "image",
		"photo_type": report_constant.PHOTOVIN3
	},
	"Таблички и маркировки Vin":  {
		"name": "photo_dopserv",
		"value": "image",
		"photo_type": report_constant.PHOTOVINTABLE
	},
	"Vin код под лобовым стеклом":  {
		"name": "photo_dopserv",
		"value": "image",
		"photo_type": report_constant.PHOTOVINGLASS
	},
	"Vin код на табличке":  {
		"name": "photo_vin_table",
		"value": "image",
		"photo_type": report_constant.PHOTOVINTABLE
	},

	"Таблички и маркировки VIN Комментарий":  "comment_vin",

	# Осмотр на подьемнике
	"Люфты (шаровые, рулевые наконечники, тяги, подшипники)": {
		"name": "salon_lufts",
		"value": "value",
		"comment": "lufts_com",
	},
	"Состояние сайлентблоков": {
		"name": "silentblocks",
		"value": "value",
		"comment": "silentblocks_com",
	},
	"Тормозная система (шланги, трубки, суппорта)": {
		"name": "tormoz_defects",
		"value": "value",
		"comment": "tormoz_defects_com",
	},
	"Запотевания": {
		"name": "zapotev_tech",
		"value": "value",
		"comment": "zapotev_tech_com",
	},
	"Состояние пыльников": {
		"name": "zapotev_tech",
		"value": "value",
		"comment": "zapotev_tech_com",
	},
	"Состояние опор (ДВС, КПП, раздаточной КПП, редуктора и т.д.)": {
		"name": "sost_pylnikov",
		"value": "value",
		"comment": "sost_pylnikov_com",
	},
	"Состояние выхлопной системы": {
		"name": "sost_vyhlop",
		"value": "value",
		"comment": "sost_vyhlop_com",
	},
	"Состояние силовых элементов кузова": {
		"name": "silov_el",
		"value": "value",
		"comment": "silov_el_com",
	},
	"Осмотр на подьемнике Комментарий":  "comment_podyemnik",

	# Осмотр на подьемнике
	"Дополнительное оборудование": {
		"name": "dop_oborudovanie",
		"value": "value",
		"comment": "dop_oborudovanie_com",
		"comment_val": "dop_oborudovanie_com",
	},
}


class setRow:
	def __init__(self, ids):
		if len(ids) == 1:
			self.id = ids[0]
			try:
				self.renameField()
			except (IndexError, ReportPodbor.DoesNotExist) as ex:
				pass
				#print(ex)
		if len(ids) == 2:
			for i in range(int(ids[0]), int(ids[1])):
				self.id = i
				try:
					self.renameField()
				except (IndexError, ReportPodbor.DoesNotExist) as ex:
					pass
					#print(ex)


	def renameField(self):
		stacks = ReportPodbor.objects.get(pk=self.id)
		for steps in stacks.data:

			try:
				comment_name = "{} Комментарий".format(steps['name'])
				name = data_to_row[comment_name]
				value = steps['comment']
				setattr(stacks, name, value)
				
				
			except (Exception, KeyError) as ex:
				pass
			if 'groups' in steps:
				for group in steps['groups']:
					for field in group['fields']:
						try:
							data_field = data_to_row[field['label']]
						except Exception as ex:
							#print(ex)
							continue
						name = data_field['name']
						try:
							value = field[data_field['value']]
							setattr(stacks, name, value)
						except Exception as ex:
							pass
						try:
							comment = data_field['comment']
							comment_value = field['comment']
							setattr(stacks, comment, comment_value)
						except Exception as ex:
							pass
						try:
							comment = data_field['comment_val']
							comment_value = field['value']
							value = True if comment_value else False
							setattr(stacks, name, value)
							setattr(stacks, comment, comment_value)
						except Exception as ex:
							pass
						try:
							negative = data_field['negative']
							for item in field['negative']['items']:
								name=negative[item['label']]
								value=item['value']
								setattr(stacks, name, value)
						except Exception as ex:
							pass
						try:
							range_v  = float(field[data_field['range']][0])
							value  = field[data_field['value']]
							setattr(stacks, name, value)
							setattr(stacks, "{}_range".format(name), int(range_v))
						except Exception as ex:
							pass
						try:
							value = field['value'][0][data_field['value']]
							res = value.split("/")
							res.pop(0)
							res.pop(0)
							value = "/".join(res)
							name = data_field['name']
							setattr(stacks, name, value)
						except Exception as ex:
								pass
						try:
							photo_type = data_field['photo_type']
							value = field['value']
							for item in value:
								image = item['image']
								print("image")
								print(image)
								#image_small = item['image_small']
								image = "/".join(image.split('/')[2:])
								print(image)
								print("image")
								#print(image_small)
								photo, create = Photo.objects.get_or_create(report_podbor=stacks, photo_type=photo_type, image=image)
								if create:
									print(photo.pk)
								try:
									if create:
										photo.image_small.generate()
								except FileNotFoundError:
									pass
						except KeyError as ex:
							#print(ex)
							pass
						

		stacks.save()