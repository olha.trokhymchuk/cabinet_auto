import os, sys
import ftplib
import json
import six
import re
import shutil
import logging

from auto.glossary import *

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.core.exceptions import MultipleObjectsReturned
from notifications.models import Notification
from notifications.utils import send_notification
from django.http.response import Http404
from report.models import ReportPodbor, Photo, SurrenderPhoto
from django.db.utils import IntegrityError

from report.models import ReportPodbor, Photo, SurrenderPhoto
from auto.models import Auto, MarkAuto, ModelAuto, Generation, Year, BodyType, AutoPhoto
from order.models import Order
import time

from core.models import User

logger = logging.getLogger(__name__)

class StopException(Exception):
    def __init___(self, dErrArguments):
        Exception.__init__(self,"Ошибка экспорта{0}".format(dErrArguments))
        

class Reader:
    def __init__(self):
        self.data = ""
    def __call__(self,s):
        self.data += s.decode('UTF-8').replace('\\', '')
        self.data = json.loads(self.data.encode('utf-8'))

def get_reports_from_mobile(remote_dir='order', parent_dir='report'):
    """
        Имя фото формируются, как xx_yy_zz-вин номер.jpg 
        xx_yy – уникальный идентификатор описывающий к чему принадлежит фото 
        zz – номер фото 
        вин номер – вин номер данного авто 
        Основная информация->фото ->1_20_ zz-вин номер.jpg

        Общая информация->фотографии с осмотра->2_1_ zz-вин номер.jpg 
        Общая информация->фотографии со сдачи->2_2_ zz-вин номер.jpg 
        Юридическая проверка ->фото ГИБДД->3_1_ zz-вин номер.jpg 
        Юридическая проверка ->фото MyCreditInfo->3_2_ zz-вин номер.jpg 
        Юридическая проверка ->фото ФССП->3_3_ zz-вин номер.jpg 
        Юридическая проверка ->фото с реестра залогов->3_4_ zz-вин номер.jpg 
        Юридическая проверка ->фото Автокод->3_5_ zz-вин номер.jpg 
        Юридическая проверка ->фото Доп. сервесы->3_6_ zz-вин номер.jpg 
        Таблички и маркировки Vin ->Vin код на кузове/раме№1->10_1_ zz-вин номер.jpg 
        Таблички и маркировки Vin ->Vin код на кузове/раме№2->10_2_ zz-вин номер.jpg 
        Таблички и маркировки Vin ->Vin код на кузове/раме№3->10_3_ zz-вин номер.jpg 
        Таблички и маркировки Vin ->Vin код табличке->10_4_ zz-вин номер.jpg 
        Таблички и маркировки Vin ->Vin код под лобовым стеклом->10_5_ zz-вин номер.jpg 
    """
    download_from_ftp(remote_dir, parent_dir)
    for name in get_names_from_dir(remote_dir, parent_dir):
        valide_vin = bool(re.match("^[A-Za-z0-9]*$", name))
        if not valide_vin:
            logger.error("not valide vin")
            logger.error(name)
            continue
            
        data = get_files_from_local(parent_dir, name, remote_dir)
        if data == False:
            continue
        try:
            import_reports_from_mobile(data, name, parent_dir, remote_dir)
        except StopException as ex:
            print(11111)
            print(ex)
            continue
        name = os.path.join(os.getcwd(), parent_dir, remote_dir, name)
        try:
            shutil.rmtree(name)
        except FileNotFoundError as ex:
            logger.error(ex)

def get_names_from_dir(remote_dir, parent_dir):
    return[f.path.split('/')[2] for f in os.scandir(parent_dir +'/'+ remote_dir) if f.is_dir()]

def upload_auto(isinstance, filename, path, chapter_photo, type_photo, i):
    now = datetime.datetime.now()
    year = now.strftime("%Y")
    month = now.strftime("%m")
    day = now.strftime("%d")
    time = now.strftime("%H%M%S%f")
    try:
        filebase, extension = filename.split(".")
    except ValueError as ex:
        logger.error(ex)
        logger.error('Error on line {}'.format(sys.exc_info()[-1].tb_lineno))
        raise StopException({
                'except' : ex
       })
    return "%s/%s/%s/%s/id_%s/%s_%s_%s_%s_%s.%s" % (path, year, month, day, isinstance.id, path, chapter_photo, type_photo, i, time, extension)

def open_file(filename):
    data = open(filename, 'r+b')
    return data.write, data.read()

def dir_time_create(ftp_handle, name):
    name = name + "/end.txt"     
    try: 
        datetimeftp = ftp_handle.sendcmd('MDTM ' + name)
    except ftplib.error_perm as ex:
        logger.error('end.txt not exist')
        logger.error(ex)
        return False
    modifiedTimeFtp = datetime.datetime.strptime(datetimeftp[4:], "%Y%m%d%H%M%S")
    now = datetime.datetime.now()
    logger.error('modifiedTimeFtp')
    logger.error(modifiedTimeFtp.minute)
    logger.error('now')
    logger.error(now.minute)
    minute_count = now.minute - modifiedTimeFtp.minute
    logger.error(minute_count)
    if abs(minute_count) < settings.TIME_AFTER_LOAD:
        logger.error(minute_count)
        logger.error('not')
        return False
    return True

def download_from_ftp(remote_dir, parent_dir):
    ftp = ftp_connect()
    download_ftp_tree(ftp, remote_dir, parent_dir)

def _is_ftp_dir(ftp_handle, name, guess_by_extension=True):
    if guess_by_extension is True:
        if len(name) >= 4:
            if name[-4] == '.':
                return False

    original_cwd = ftp_handle.pwd()     # remember the current working directory
    try:
        ftp_handle.cwd(name)            # try to set directory to new name
        ftp_handle.cwd(original_cwd)    # set it back to what it was
        return True
    
    except ftplib.error_perm as e:
        return False
    
    except:
        return False

def _make_parent_dir(fpath):
    dirname = os.path.dirname(fpath)
    while not os.path.exists(dirname):
        try:
            os.makedirs(dirname)
            print("created {0}".format(dirname))
        except:
            _make_parent_dir(dirname)

def _download_ftp_file(ftp_handle, name, dest, overwrite):
    _make_parent_dir(dest.lstrip("/"))
    if not os.path.exists(dest) or overwrite is True:
        try:
            with open(dest, 'wb') as f:
                ftp_handle.retrbinary("RETR {0}".format(name), f.write)
            print("downloaded: {0}".format(dest))
        except FileNotFoundError:
            print("FAILED: {0}".format(dest))
    else:
        print('pwd')
        print(os.getcwd())
        print("already exists: {0}".format(dest))

def _mirror_ftp_dir(ftp_handle, name, overwrite, guess_by_extension):
    for item in ftp_handle.nlst(name):
        if _is_ftp_dir(ftp_handle, item, guess_by_extension):
            if not dir_time_create(ftp_handle, item):
                print('not time')
                continue
            _mirror_ftp_dir(ftp_handle, item, overwrite, guess_by_extension)
        else:
            if not _download_ftp_file(ftp_handle, item, item, overwrite):
                continue


def download_ftp_tree(ftp_handle, path, parent_dir, overwrite=False, guess_by_extension=True):
    path = path.lstrip("/")
    original_directory = os.getcwd()    # remember working directory before function is executed
    os.chdir(parent_dir)               # change working directory to ftp mirror directory
    print(os.getcwd())
    _mirror_ftp_dir(ftp_handle, path, overwrite, guess_by_extension)
    os.chdir(original_directory)        # reset working directory to what it was before function exec

def get_file(ftp, filename):
    try:
        ftp.retrbinary('RETR '  + filename, open(filename, 'wb').write)
    except json.decoder.JSONDecodeError:
        print('error decode')
        get_files_from_ftp(filename)
    except ftplib.error_reply:
        print('error_reply')
        get_files_from_ftp(filename)
    else:
        return

def ftp_connect():
    server = settings.FTP_URL
    ftp = ftplib.FTP(server)
    UID = settings.FTP_USER
    ftp.login(UID, settings.FTP_PSW)
    # ftp.cwd("/order")
    return ftp

def get_files_from_ftp(filename):
    ftp = ftp_connect()
    r = get_file(ftp, filename)
    try:
        data = r
    except AttributeError:
        print('has no attribute data')
        r = get_file(ftp,filename)
    ftp.close()
    return data

def get_files_from_local(parent_dir, name, remote_dir):
    current = "{}/{}/{}/".format(parent_dir, remote_dir, name)
    filename = "{}{}.json".format(current, name)
    end_uplaod = "{}end.txt".format(current)
    end_uplaod = os.path.join(os.getcwd(), end_uplaod)
    if not os.path.exists(end_uplaod):
        shutil.rmtree(current)
        error = "end.txt файл не найден"
        data = {
            'name': name,
            'error': error
        }
        users = []
        user = get_object_or_404(User, username='79688018880')
        users.append(user)
        user = get_object_or_404(User, username='79610577488')
        users.append(user)
        send_notification(recipients=users, action=Notification.REPORT_MOB, data=data)
        move_file_by_resul(parent_dir, name, 'unsuccess', remote_dir, 'not_end')
        return False
    try:
        with open(filename, "r", encoding="utf-8") as file:
            data = file.read().replace('\\"', ' ')
            try:
                data = json.loads(data.replace("\\", r"\\"))
            except json.decoder.JSONDecodeError as ex:
                logger.error('JSONDecodeError')
                logger.error(ex)
                shutil.rmtree(parent_dir+'/'+name)
                move_file_by_resul(parent_dir, name, 'unsuccess', remote_dir, 'json_decod')
                return False
            return data
    except FileNotFoundError as ex:
        logger.error(ex)
        shutil.rmtree(parent_dir+'/'+name)
        move_file_by_resul(parent_dir, name, 'unsuccess', remote_dir, 'json_decod')
        return False

def get_mobile_data(filename):
    report_data_model = {} 
    with open(filename) as file:
        return json.load(file)

def move_image(data, i):
    model_data = data[1]['model_data']
    filename = data[1]['filename']
    remote_dir = data[1]['remote_dir']
    parent_dir = data[1]['parent_dir']
    type_photo = data[1]['type_photo']
    chapter_photo = data[1]['chapter_photo']
    path = data[1]['path']
    image_name = "{}.jpg".format(filename)
    print('image_name')
    print(image_name)
    image_phone = "{}/{}/{}/{}_{}_{}-{}".format(parent_dir, remote_dir, filename, chapter_photo, type_photo,  i, image_name) 
    print('image_phone')
    print(image_phone)
    if os.path.exists(image_phone):
        print('image_phone exist')
        image = upload_auto(model_data, image_name, path, chapter_photo, type_photo, i)
        if not os.path.exists(os.path.dirname("media/" + image)):
            try:
                os.makedirs(os.path.dirname("media/" + image))
            except OSError as exc: # Guard against race condition
                if exc.errno != errno.EEXIST:
                    raise
        if not os.path.exists("media/" + image):
            os.rename(image_phone, "media/" + image)
        return image
    else:
        return False

def save_photo(model, data):
    image_exist = True
    i = 0
    model_data = data[1]['model_data']
    filename = data[1]['filename']
    parent_dir = data[1]['parent_dir']
    remote_dir = data[1]['remote_dir']
    type_photo = data[1]['type_photo']
    chapter_photo = data[1]['chapter_photo']
    path = data[1]['path']
    image_name = "{}.jpg".format(filename)

    photos_data = []
    while image_exist:
        image = move_image(data, i)
        if image and isinstance(image, str) and os.path.isfile("media/" + image):
            photo_data = data[0]
            photo_data.update({'image': image})
            photos = model(**photo_data)
            photos.save()
            i += 1
            photos_data.append(photos)
            logger.error(image_name)
        else:
            image_exist = False
            return photos_data

def normalize_data(mobile_data):
    d1 = {}
    for data in mobile_data:
        d1.update({"{}".format(data): mobile_data[data]})

def import_reports_from_mobile(mobile_data, filename, parent_dir, remote_dir):
    steps_data = os.path.join(os.getcwd(), "report/fixtures/mobile.json")       
    with open(steps_data) as steps_file:
        order_data = json.load(steps_file)

    out_side = [
        "Крыша", "Задняя правая дверь","Переднее правое крыло",
        "Передняя правая дверь","Переднее правое стекло","Заднее правое стекло","Правый порог",
        "Заднее правое крыло","Багажник","Заднее стекло",
        "Задний бампер","Задние фонари","Заднее левое крыло","Левый порог",
        "Задняя левая дверь","Заднее левое стекло","Передняя левая дверь","Переднее левое стекло",
        "Переднее левое крыло","Капот","Лобовое стекло",
        "Передний бампер","Передние фонари","Стойка передняя левая",
        "Стойка центральная левая","Стойка задняя правая","Стойка центральная правая",
        "Стойка задняя левая","Стойка передняя правая","Передний лонжерон левый",
        "Передний лонжерон правый","Чашка передняя левая","Чашка передняя правая",
        "Моторный щит","Центральный тоннель","Пол","Задняя чашка левая",
        "Задняя чашка правая","Пол багажника","Комментарий2"    
    ]

    ur = [
        "Остаток ПТК","Остаток ПТД","Остаток ЗТК","Остаток ЗТД",
        "Остаток летних шин","Остаток зимних шин","Уровень тормозной жидкости",
        "Уровень жидкости ГУР","Стояночный тормоз",
        "Состояние шин","Состояние шин","Наличие стуков, скрипов",
        "Прямолинейность движения","Люфты в рулевом управлении",
        "Посторонние стуки и скрипы", "Тормозная жидкость"
    ]
    cd = [
        "Чтение Vin из ЭБУ","Связь со всеми блоками ЭБУ",
        "Соответствие прописанных ключей с фактическим","Ошибки в ЭБУ"
    ]
    elb = [
        "Освещение и индикация","Стеклоподъемники","Очиститель/омыватель",
        "Эл. привод (сидений, зеркал и т.д.)/Подогрев","Прочие электронные компоненты и устройства",
        "Система SRS (подушки безопасности)","Система A/C (кондиционер)","Ремни безопасности"  
    ]
    salon = [
        "Дефекты","Чистота"
    ]
    lift = [
        "Люфты (шаровые, рулевые наконечники, тяги, подшипники)",
        "Состояние сайлентблоков","Тормозная система (шланги, трубки, суппорта)",
        "Состояние опор (ДВС, КПП, раздаточной КПП, редуктора и т.д.)","Запотевания","Состояние пыльников",
        "Состояние выхлопной системы","Состояние силовых элементов кузова"
    ]
    ing_trans = [
        "Посторонние шумы","Запотевание/течи","Работа ДВС","Уровень масла",
        "Уровень жидкости охлаждающей системы","Обслуживание ДВС","Состояние приводных ремней",
        "Работа КПП"
    ]
    dop = [
        "Дополнительное оборудование"
    ]


    step_ok_range = {
        "Компьютерная диагностика":cd,
        "Рулевое управление, подвеска и тормозная система":ur,
        "Электрика и безопасность/отопление и вентиляция":elb,
        "Состояние салона":salon,
        "Осмотр на подъемнике":lift,
        "Дополнительное оборудование":dop

    }

    auto = {
        'vin':'Vin', 
        'mark_auto':'Марка', 
        'model_auto':'Модель',
        'generation':'Поколение',
        'engine_capacity':'Объем',
        'salon_auto':'Салон',
        'drive_type':'Привод',
        'year_auto':'Дата выпуска',
        'owners':'Количество владельцев',
        'body_type_auto':'Кузов',
        'color_auto':'Цвет',
        'equipment':'Комплектация',
        'engine_type':'Двигатель',
        'horsepower':'Мощность',
        'transmission_type':'Трансмиссия',
        'color_salon':'Цвет салона',
        'mileage':'Пробег',
        'cost':'Цена',
        'comment':'Описание1',
        'author_type':'Тип продавца',
        'author':'Имя',
        'phone':'Телефон',
        'location':'Местоположение',
        'source':'Источник',
        'resell':'Перекуп'
    }  
    auto_choices = {
        'engine_capacity':ENGINE_CAPACITY,
        'author_type':AUTHOR_TYPE
    }
    auto_choices_byname = {
        'engine_type':ENGINE_TYPE,
        'drive_type':DRIVE_TYPE,
        'transmission_type':TRANSMISSION_TYPE,
    }
    auto_modeles = {
        'mark_auto':MarkAuto,
        'model_auto':ModelAuto,
        'year_auto':Year,
        'generation':Generation,
        'body_type_auto':BodyType
    }

    auto_data_mobile = {}
    year_list = ['year_auto']
    generation_list = ['generation']
    for d in auto:
        try:
            mobile_data[auto[d]]
        except KeyError:
            # print(auto[d])
            continue
        if d in auto_modeles:
            name = mobile_data[auto[d]]
            if d in generation_list:
                name = re.sub(r'\\+', '', name)
                logger.error(name)
            select = {'name': name}
            if d in year_list:
                select = {'year': name}  
            try:
                auto_d = get_object_or_404(auto_modeles[d], **select)
                logger.error('auto_d')
                logger.error(auto_d)
                mobile_data[auto[d]]  = auto_d

            except MultipleObjectsReturned:
                mobile_data[auto[d]]  = auto_modeles[d].objects.filter(**select)[0]
            except ModelAuto.DoesNotExist:
                mobile_data[auto[d]]  = None
            except Http404:
                mobile_data[auto[d]]  = None
        if d in auto_choices:
            logger.error(d)  
            for choice in auto_choices[d]:
                if(str(choice[0]).lower() == str(mobile_data[auto[d]]).lower()):
                    logger.error(choice[0])
                    logger.error(type(choice[0]))
                    try:   
                        auto_data_mobile.update({d: int(choice[0])})
                    except ValueError as ex:
                        logger.error(ex)
                    else:
                       break 
                    try: 
                        auto_data_mobile.update({d: float(choice[0])})
                    except ValueError as ex:
                        logger.error(ex)
                    else:
                       break 

                    continue
        if d in auto_choices_byname:
            logger.error(d)  
            for choice in auto_choices_byname[d]:
                if(str(choice[1]).lower() == str(mobile_data[auto[d]]).lower()):
                    logger.error(choice[0])
                    logger.error(type(choice[0]))
                    try:   
                        auto_data_mobile.update({d: int(choice[0])})
                    except ValueError as ex:
                        logger.error(ex)
                    else:
                       break 
                    try: 
                        auto_data_mobile.update({d: float(choice[0])})
                    except ValueError as ex:
                        logger.error(ex)
                    else:
                       break 

                    continue
            if  isinstance(mobile_data[auto[d]], int):
                pass
            elif isinstance(mobile_data[auto[d]], float):
                pass
            else:
                mobile_data[d] = 0
                continue
        auto_data_mobile.update({d: mobile_data[auto[d]]})
    vin = None
    try:
        vin = auto_data_mobile['vin']
    except KeyError:
        vin = filename
    auto_data_mobile['model_auto'] = get_model(auto_data_mobile)
    auto_data_mobile['generation'] = get_generation(auto_data_mobile)
    if not vin:
        move_file_by_resul(parent_dir, filename, 'unsuccess', remote_dir, 'not_vin')
    auto_data, created = Auto.objects.get_or_create(vin=vin, defaults=auto_data_mobile)

    if not created:
        for k, v in six.iteritems(auto_data_mobile):
            setattr(auto_data, k, v)
        auto_data.save()

    rep_datas = {}
    other_datas = {
        'filename': filename, 
        'parent_dir': parent_dir, 
        'remote_dir': remote_dir, 
        'model_data': auto_data,
        'path': 'reports/ppk',
        'chapter_photo': 3,
        'type_photo': 1
    }

    data = []
    data.append(rep_datas)
    data.append(other_datas)
    # TODO: Try to understand this code, and delete them
    report_photos_id = []
    photo_gibdd = save_photo(Photo, data)
    data[1].update({'type_photo': 2})
    photo_mycreditinfo = save_photo(Photo, data)
    data[1].update({'type_photo': 3})
    photo_fssp = save_photo(Photo, data)
    data[1].update({'type_photo': 4})
    photo_reestr_zalog = save_photo(Photo, data)
    data[1].update({'type_photo': 5})
    photo_avtokod = save_photo(Photo, data)
    data[1].update({'type_photo': 6})
    photo_dopserv = save_photo(Photo, data)

    data[1].update({'chapter_photo': 10})
    data[1].update({'type_photo': 1})
    photo_vin1 = save_photo(Photo, data)
    data[1].update({'type_photo': 2})
    photo_vin2 = save_photo(Photo, data)
    data[1].update({'type_photo': 3})
    photo_vin3 = save_photo(Photo, data)
    data[1].update({'type_photo': 4})
    photo_vin_table = save_photo(Photo, data)
    data[1].update({'type_photo': 5})
    photo_vin_glass = save_photo(Photo, data)
    for photo in [photo_gibdd, photo_mycreditinfo, photo_fssp, photo_reestr_zalog,
                        photo_avtokod, photo_dopserv, photo_vin1, photo_vin2, photo_vin3,
                        photo_vin_table, photo_vin_glass]:
        for p in photo:
            if p:               
                report_photos_id.append(p.pk)
    yr= {
        "Фото ГИБДД": photo_gibdd, 
        "Фото MyCreditInfo":photo_mycreditinfo, 
        "Фото ФССП":photo_fssp, 
        "Фото с реестра залогов":photo_reestr_zalog, 
        "Фото Автокод":photo_avtokod, 
        "Фото Доп. сервисы":photo_dopserv, 
    }

    vin= {
        "Vin код на кузове/раме №1": photo_vin1, 
        "Vin код на кузове/раме №2":photo_vin2, 
        "Vin код на кузове/раме №3":photo_vin3, 
        "Vin код на табличке":photo_vin_table, 
        "Vin код под лобовым стеклом":photo_vin_glass
    }
    photo_yr_vin = {
        "Таблички и маркировки Vin":vin,
        "Юридическая проверка": yr
    }



    for step in order_data:
        if step['name'] in step_ok_range:
            for param in step['groups'][0]['fields']:
                for u in step_ok_range[step['name']]:
                    label = re.sub(r"\s+", ' ', param['label'])
                    if label == u:

                        try:
                            mobile_param = mobile_data[u]
                            logger.error(mobile_param)
                            if u=="Дополнительное оборудование":
                                # print(mobile_param)
                                pass
                        except KeyError as ex:
                            # print(ex)
                            continue
                        if mobile_param == "ОК":
                            mobile_param = True
                            param['value'] = False

                        if isinstance(mobile_param, str) or isinstance(mobile_param, int):
                            if param["type"] == 'range':
                                param["range"] = [
                                    str(mobile_param)
                                ]
                                param['value'] = True
                                continue
                            if param["type"] == 'boolean':
                                if mobile_param == True:
                                    param['value'] = True
                                else:
                                    param['value'] = False
                                    param['comment'] = mobile_param
                                continue
                        if param["type"] == 'range': 
                            print("type no str")   
                            param["range"] = [
                                str(mobile_param[0]['Значение'])
                            ]
                            param['value'] = False
                        if param["type"] == 'text':    
                            param['value'] = mobile_param

                        try:
                            param['comment'] = mobile_param[0]['Коментарий']    
                            param['value'] = False
                        except TypeError:
                            pass


        if step['name']=='Двигатель и трансмиссия':
            for params in step['groups']:
                for param in params['fields']:
                    for i_t in ing_trans:
                        if param['label'] == i_t:
                            try:

                                mobile_param = mobile_data[str(i_t)]
                            except KeyError as ex:
                                continue
                            if isinstance(mobile_param, str) or isinstance(mobile_param, int):  
                                if mobile_param == "ОК" or mobile_param == False:
                                    mobile_param = False
                                    param['value'] = True
                                    param['label'] = str(i_t)
                                    logger.error(str(i_t))
                                    logger.error(mobile_param)
                                    logger.error(param)
                                else:
                                    param['value'] = False
                                    param['comment'] = mobile_param
                                    param['label'] = str(i_t)
                                    logger.error(str(i_t))
                                    logger.error(mobile_param)
                                    logger.error(param)
        if step['name']=='Кузов':
            for params in step['groups']:
                for param in params['fields']:
                    for o in out_side:
                        if param['label'] == o:
                            try:
                                mobile_param = mobile_data[o]
                            except KeyError as ex:
                                continue
                            if mobile_param == "ОК" or mobile_param == False:
                                mobile_param = []
                                param['value'] = True
                                continue
                            for par in param['negative']['items']:
                                for mpar in mobile_param[0]:
                                    param['value'] = False  
                                    res = re.search(str(par['label']).split('/')[0], str(mpar), re.IGNORECASE)
                                    if res:
                                        par['value'] = mobile_param[0][str(mpar)]
                                    if mpar == 'Коментарий':
                                        param['comment'] = mobile_param[0]['Коментарий']

        if step['name'] in photo_yr_vin:
            for params in step['groups']:
                for param in params['fields']:
                    for y in photo_yr_vin[step['name']]:
                        values = photo_yr_vin[step['name']][y]
                        for value in values:
                            try:
                                value.image
                            except AttributeError:
                                continue
                            if param['label'] == y:
                                param['value'] = [
                                    {
                                      "id": value.pk,
                                      "image": "/media/"+str(value.image),
                                      "image_small": "/media/"+str(value.image_small)
                                    }
                                ]
                               
        if step['name']=='Юридическая проверка':
            try:
                step['comment'] = mobile_data['Комментарий1']
            except KeyError as ex:
                pass
        if step['name']=='Кузов':
            try:
                step['comment'] = mobile_data['Комментарий2']
            except KeyError as ex:
                pass
        if step['name']=='Двигатель и трансмиссия':
            try:
                step['comment'] = mobile_data['Комментарий3']
            except KeyError as ex:
                pass
        if step['name']=='Рулевое управление, подвеска и тормозная система':
            try:
                step['comment'] = mobile_data['Комментарий4']
            except KeyError as ex:
                pass
        if step['name']=='Компьютерная диагностика':
            try:
                step['comment'] = mobile_data['Комментарий5']
            except KeyError as ex:
                pass
        if step['name']=='Электрика и безопасность/отопление и вентиляция':
            try:
                step['comment'] = mobile_data['Комментарий6']
            except KeyError as ex:
                pass
        if step['name']=='Состояние салона':
            try:
                step['comment'] = mobile_data['Комментарий7']
            except KeyError as ex:
                pass
        if step['name']=='Таблички и маркировки Vin':
            try:
                step['comment'] = mobile_data['Комментарий8']
            except KeyError as ex:
                pass
        if step['name']=='Осмотр на подъемнике':
            try:
                step['comment'] = mobile_data['Комментарий9']
            except KeyError as ex:
                pass
    
    auto_datas = {'auto': auto_data}
    data[0].clear()
    data[0].update({'auto': auto_data})
    data[1].update({'path': 'auto'})
    data[1].update({'chapter_photo': 1})
    data[1].update({'type_photo': 20})
    save_photo(AutoPhoto, data)
    report_model = {}
    cost_before = 0
    cost_after = 0
    cost_estimated = 0
    pluses = ''
    minuses = ''
    recommended = ''
    result_expert = ''
    try:
        cost_before = mobile_data['Стоимость до торга']
    except KeyError as ex:
        pass
    try:
        cost_after = mobile_data['Стоимость после торга']
    except KeyError as ex:
        pass
    try:
        cost_estimated = mobile_data['Оценочная стоимость']
    except KeyError as ex:
        pass
    try:
        pluses = mobile_data['Плюсы']
    except KeyError as ex:
        pass
    try:
        minuses = mobile_data['Минусы']
    except KeyError as ex:
        pass
        # print(ex)
    try:
        recommended = mobile_data['Рекомендуем']
    except KeyError as ex:
        recommended = None
        # print(ex)
    try:
        category = mobile_data['Категория авто']
        report_model.update({"category":category})
    except KeyError as ex:
        pass
        # print(ex)
    try:
        result_expert = mobile_data['Вывод эксперта']
    except KeyError as ex:
        pass
        # print(ex)
    try:
        executor_id = mobile_data['id'] # to do не заполняется исполнитель
        report_model.update({"executor_id": executor_id})
    except KeyError as ex:
        pass
        # print(ex)
    if order_data == '':
        order_data = None
    if auto_data == '':
        auto_data = None

    report_model.update({ 
        "auto": auto_data, 
        "data" :order_data,
        "cost_before":cost_before,
        "cost_after":cost_after,
        "cost_estimated":cost_estimated,
        "pluses":pluses,
        "minuses":minuses,
        "recommended":recommended,
        "result_expert":result_expert

    })
    rep = ReportPodbor.objects.filter(auto__vin=report_model['auto'].vin).first()
    if not rep:
        rep = ReportPodbor(**report_model)
    else:
        logger.error("report update")
        #rep = ReportPodbor.objects.filter(auto__vin=report_model['auto'].vin).exclude(pk=rep.first().pk)
        #rep.delete()
        #rep = rep.first()                                 
        for k, v in six.iteritems(report_model):
            setattr(rep, k, v)
        logger.error("report preupdate")
    try:
        rep.save()
    except Exception as ex:
        logger.error(ex)
    else:
        logger.error("report updated")
        Photo.objects.filter(pk__in=report_photos_id).update(report_podbor= rep)

        data[0].clear()
        data[0].update({'report_podbor': rep})
        data[1].update({'type_photo': 1})
        data[1].update({'model_data': rep})
        data[1].update({'chapter_photo': 2})
        save_photo(Photo, data)

        data[1].update({'type_photo': 2})
        data[0].clear()
        data[0].update({'surrender_podbor': rep})

        save_photo(SurrenderPhoto, data)
        try:
            for o in str(mobile_data['Заказы']).split(','):
                try:
                    order = Order.objects.get(pk=o)
                    if order:
                        rep.order.add(order)
                        rep.save()
                        order.cars.add(auto_data)
                        order.save()
                except Order.DoesNotExist as ex:
                    pass
                    # print(ex)
        except KeyError as ex:
            pass
            # print(ex)
        except ValueError as ex:
            pass
            # print(ex)
        except:
            pass
            # print("Unexpected error:", sys.exc_info()[0])
        move_file_by_resul(parent_dir, filename, 'success', remote_dir, 'ok')
  
def get_generation(auto_data_mobile):
    try:
        g = Generation.objects.get(model_auto=auto_data_mobile['model_auto'], name=auto_data_mobile['generation'].name)
        logger.error(g)
        return g
    except (Generation.DoesNotExist, KeyError, Exception) as ex:
        logger.error(ex)
        return None

def get_model(auto_data_mobile):
    try:
        return ModelAuto.objects.get(mark_auto__name=auto_data_mobile['mark_auto'], name=auto_data_mobile['model_auto'].name)
    except (ModelAuto.DoesNotExist, Exception) as ex:
        return None

def move_file_by_resul(parent_dir, filename, path, remote_dir='order', what_happend=''):
    logger.error('what_happend')
    logger.error(what_happend)
    ftp = ftp_connect()
    print(parent_dir)
    local_dir = parent_dir +"/"+remote_dir
    print(remote_dir)
    try:
        ftp.rename(remote_dir + '/' + filename, path + '/'+ filename)
    except ftplib.error_perm as ex:
        print(ex)
        now = datetime.datetime.now()
        year = now.strftime("%Y")
        month = now.strftime("%m")
        day = now.strftime("%d")
        time = now.strftime("%H%M%S")
        newfilename = '{}_{}_{}'.format(filename, day, time)
        current = "{}/{}/{}/".format(parent_dir, remote_dir, filename)
        try:
            ftp.rename(remote_dir + '/' + filename, path +'/'+ newfilename)
        except ftplib.error_perm as ex:
            try:
                ftp.rename(remote_dir + '/' + filename, 'unsuccess/'+ newfilename)
            except:
                shutil.rmtree(current)




