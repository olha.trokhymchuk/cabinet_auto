import ftplib
from datetime import datetime
import paramiko
from django.conf import settings
from core.models import Filial, User
from report.workers.sort_reportpodbor_dict import sort_reportpodbor_dict


def get_date_now():
    date_now = datetime.now()
    return date_now


def ftp_connect():
    server = settings.UP_FTP_URL
    UID = settings.UP_FTP_USER
    FTP_PSW = settings.UP_FTP_PSW
    port = 21

    ftp = ftplib.FTP()
    ftp.connect(host=server, port=port)
    ftp.login(user=UID, passwd=FTP_PSW)
    return ftp


def sftp_connect():
    server = settings.UP_SFTP_URL
    UID = settings.UP_SFTP_USER
    FTP_PSW = settings.UP_SFTP_PSW
    port = settings.UP_SFTP_PORT

    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    trans = paramiko.Transport((server, port))
    trans.connect(username=UID, password=FTP_PSW)
    sftp = paramiko.SFTPClient.from_transport(trans)
    return sftp


def create_json_report_file(reports):
    dict_to_upload = []
    # date = get_date_now().strftime("%Y-%m-%d")

    print('reports : ', reports.count())

    for report in reports:
        dict_for_damages = []
        dict_for_issues = []
        dict_for_photos = []
        print('report_id: ', report.id)
        auto = report.auto
        sort_reportpodbor_dict(dict_for_damages=dict_for_damages, dict_for_issues=dict_for_issues, report=report,
                               dict_for_photos=dict_for_photos)

        price = report.cost_after
        chp = auto.horsepower
        report_mileage = report.mileage

        if report_mileage == None:
            report_mileage = 0

        if price == None:
            price = 0

        if chp == None:
            chp = 0

        json_data = {

        }

        try:
            json_data = {
                "id": "{}".format(report.id),
                "et": 12,
                "v": auto.vin,
                "dt": report.created.strftime("%Y-%m-%d"),
                "er": report.order.first().filial.name,
                "ec": report.order.first().filial.name,
                "m": report_mileage,
                "cbrand": auto.mark_auto.name,
                "cmodel": auto.model_auto.name,
                "y": auto.year_auto.year,
                "chp": chp,
                "photos": dict_for_photos,
                "condition": {
                    "stars": [
                        {
                            "label": "-",
                            "rating": 1
                        },
                    ],
                    "issues": dict_for_issues
                },
                "price": price,

                "damages": dict_for_damages
            }
        except:
            pass
        dict_to_upload.append(json_data)
    return {
        'json_data': json_data,
        'reports': reports,
        'filial_id': report.order.first().filial.id,
        'dict_for_photos': dict_for_photos,
        'dict_to_upload': dict_to_upload
    }


