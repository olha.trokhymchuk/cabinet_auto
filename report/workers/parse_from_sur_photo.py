import report.report_constants as report_constant

from report.models import Steps, ReportPodbor, Photo
from report.models import  SurrenderPhoto

from imagekit.exceptions import MissingSource

class setSurPhoto:
	def __init__(self, ids):
		if len(ids) == 1:
			self.id = ids[0]
			try:
				self.movePhoto()
			except (IndexError) as ex:
				pass
				#print(ex)
		if len(ids) == 2:
			for i in range(int(ids[0]), int(ids[1])):
				self.id = i
				try:
					self.movePhoto()
				except (IndexError) as ex:
					pass
					#print(ex)

	def movePhoto(self):
		photos = SurrenderPhoto.objects.filter(surrender_podbor=self.id)
		for photo in photos:
			image = photo.image

			print(image)
			photo, create = Photo.objects.get_or_create(report_podbor_id=self.id, photo_type=report_constant.PHOTOCHANGE, image=str(image))
			if create:
				print(photo.pk)
			try:
				if create:
					photo.image_small.generate()
			except (MissingSource, OSError, FileNotFoundError):
				pass

