import report.report_constants as report_constant
from django.contrib.sites.shortcuts import get_current_site


def sort_reportpodbor_dict(dict_for_damages, dict_for_issues, report, dict_for_photos):
    site = get_current_site(None).domain
    report_dict = report.__dict__
    for key, value in report_dict.items():
        # Кузов

        # Внешние элементы
        if 'krisha_' in key and value == True and value != None:
            report_type = report.krisha_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name

            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'kapot_' in key and value == True and value != None:
            report_type = report.kapot_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'bagagnik_' in key and value == True and value != None:
            report_type = report.bagagnik_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'peredni_bamper_' in key and value == True and value != None:
            report_type = report.peredni_bamper_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zadni_bamper_' in key and value == True and value != None:
            report_type = report.zadni_bamper_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'perednee_levoe_krilo_' in key and value == True and value != None:
            report_type = report.perednee_levoe_krilo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'perednee_pravoe_krilo_' in key and value == True and value != None:
            report_type = report.perednee_pravoe_krilo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'peredneya_levaya_dver_' in key and value == True and value != None:
            report_type = report.peredneya_levaya_dver_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'peredneya_pravaya_dver_' in key and value == True and value != None:
            report_type = report.peredneya_pravaya_dver_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zadnya_levaya_dver_' in key and value == True and value != None:
            report_type = report.zadnya_levaya_dver_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zadnya_pravaya_dver_' in key and value == True and value != None:
            report_type = report.zadnya_pravaya_dver_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zadnee_levoe_krilo_' in key and value == True and value != None:
            report_type = report.zadnee_levoe_krilo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zadnee_pravoe_krilo_' in key and value == True and value != None:
            report_type = report.zadnee_pravoe_krilo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'levii_porog_' in key and value == True and value != None:
            report_type = report.levii_porog_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'pravii_porog_' in key and value == True and value != None:
            report_type = report.pravii_porog_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'lobovoe_steklo_' in key and value == True and value != None:
            report_type = report.lobovoe_steklo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'perednee_levoe_steklo_' in key and value == True and value != None:
            report_type = report.perednee_levoe_steklo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'perednee_pravoe_steklo_' in key and value == True and value != None:
            report_type = report.perednee_pravoe_steklo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zadnee_levoe_steklo_' in key and value == True and value != None:
            report_type = report.zadnee_levoe_steklo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zadnee_pravoe_steklo_' in key and value == True and value != None:
            report_type = report.zadnee_pravoe_steklo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zadnee_steklo_' in key and value == True and value != None:
            report_type = report.zadnee_steklo_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'perednie_fonari_' in key and value == True and value != None:
            report_type = report.perednie_fonari_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zadnie_fonari_' in key and value == True and value != None:
            report_type = report.zadnie_fonari_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        # Силовые элементы
        elif 'stoika_perednya_levaya_' in key and value == True and value != None:
            report_type = report.stoika_perednya_levaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'stoika_centralynaya_levaya_' in key and value == True and value != None:
            report_type = report.stoika_centralynaya_levaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'stoika_zadnaya_levaya_' in key and value == True and value != None:
            report_type = report.stoika_zadnaya_levaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'stoika_perednaya_pravaya_' in key and value == True and value != None:
            report_type = report.stoika_perednaya_pravaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'stoika_centralnaya_pravaya_' in key and value == True and value != None:
            report_type = report.stoika_centralnaya_pravaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'stoika_zadnyaya_pravaya_' in key and value == True and value != None:
            report_type = report.stoika_zadnyaya_pravaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'perednii_longeron_levii_' in key and value == True and value != None:
            report_type = report.perednii_longeron_levii_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'perednii_longeron_pravii_' in key and value == True and value != None:
            report_type = report.perednii_longeron_pravii_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'chahka_perednaya_levaya_' in key and value == True and value != None:
            report_type = report.chahka_perednaya_levaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'chahka_perednaya_pravaya_' in key and value == True and value != None:
            report_type = report.chahka_perednaya_pravaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'motornii_chit_' in key and value == True and value != None:
            report_type = report.motornii_chit_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'centralnii_tonel_' in key and value == True and value != None:
            report_type = report.centralnii_tonel_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        elif 'chahka_zadnaya_levaya_' in key and value == True and value != None:
            report_type = report.chahka_zadnaya_levaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'chahka_zadnaya_pravaya_' in key and value == True and value != None:
            report_type = report.chahka_zadnaya_pravaya_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        # Двигатель и трансмиссия
        elif 'postor_shumi' in key and value == False and value != None:
            report_type = report.postor_shumi_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zapotev_techi' in key and value == False and value != None:
            report_type = report.zapotev_techi_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'ravnom_rab' in key and value == False and value != None:
            report_type = report.ravnom_rab_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'ur_masla' in key and value == False and value != None:
            report_type = report.ur_masla_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'ur_ohl_zhidk' in key and value == False and value != None:
            report_type = report.ur_ohl_zhidk_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'tr_obsl' in key and value == False and value != None:
            report_type = report.tr_obsl_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'sost_priv_rem' in key and value == False and value != None:
            report_type = report.sost_priv_rem_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'rabota_kpp' in key and value == False and value != None:
            report_type = report.rabota_kpp_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        elif 'ur_tormoz_zh' in key and value == False and value != None:
            report_type = report.ur_tormoz_zh_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'ur_gur_zh' in key and value == False and value != None:
            report_type = report.ur_gur_zh_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'sost_st_torm' in key and value == False and value != None:
            report_type = report.sost_st_torm_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'sost_rez' in key and value == False and value != None:
            report_type = report.sost_rez_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'sost_torm_zh' in key and value == False and value != None:
            report_type = report.sost_torm_zh_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'stuki_skr' in key and value == False and value != None:
            report_type = report.stuki_skr_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'otkl_pr_dvig' in key and value == False and value != None:
            report_type = report.otkl_pr_dvig_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'luft_rul_uprav' in key and value == False and value != None:
            report_type = report.luft_rul_uprav_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        # Компьютерная диагностика
        elif 'read_vin' in key and value == False and value != None:
            report_type = report.read_vin_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'connect_ebu' in key and value == False and value != None:
            report_type = report.connect_ebu_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'error_ebu' in key and value == False and value != None:
            report_type = report.error_ebu_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'number_key_ebu' in key and value == False and value != None:
            report_type = report.number_key_ebu_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        # Электрика и безопасность/отопление и вентиляция
        elif 'svet_indik' in key and value == False and value != None:
            report_type = report.svet_indik_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'steklopod' in key and value == False and value != None:
            report_type = report.steklopod_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'omyvatel' in key and value == False and value != None:
            report_type = report.omyvatel_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'el_priv_sal' in key and value == False and value != None:
            report_type = report.el_priv_sal_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'el_komp_other' in key and value == False and value != None:
            report_type = report.el_komp_other_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'ispr_srs' in key and value == False and value != None:
            report_type = report.ispr_srs_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'remni_bezopas' in key and value == False and value != None:
            report_type = report.remni_bezopas_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'ispr_ac' in key and value == False and value != None:
            report_type = report.ispr_ac_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        # Состояние салона
        elif 'salon_defects' in key and value == False and value != None:
            report_type = report.salon_defects_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        elif 'salon_clear' in key and value == False and value != None:
            report_type = report.salon_clear_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        # Осмотр на подьемнике
        elif 'lufts' in key and value == False and value != None:
            report_type = report.lufts_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'silentblocks' in key and value == False and value != None:
            report_type = report.silentblocks_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'tormoz_defects' in key and value == False and value != None:
            report_type = report.tormoz_defects_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'sost_opor' in key and value == False and value != None:
            report_type = report.sost_opor_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'zapotev_tech' in key and value == False and value != None:
            report_type = report.zapotev_tech_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'sost_pylnikov' in key and value == False and value != None:
            report_type = report.sost_pylnikov_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'sost_vyhlop' in key and value == False and value != None:
            report_type = report.sost_vyhlop_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)
        elif 'silov_el' in key and value == False and value != None:
            report_type = report.silov_el_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

        # Дополнительное оборудование
        elif 'dop_oborudovanie' in key and value == False and value != None:
            report_type = report.dop_oborudovanie_com
            if not report_type:
                report_type = report._meta.get_field(key).verbose_name
            data = {
                "part": report._meta.get_field(key).verbose_name,
                "type": report_type,
            }
            dict_for_damages.append(data)

    for key, value in report_dict.items():
        if key == 'pluses' and value != None and value != '':
            name_issue = report._meta.get_field(key).verbose_name
            data_issues = value
            dict_for_issues.append(name_issue + ' : ' + data_issues)
        if key == 'minuses' and value != None and value != '':
            name_issue = report._meta.get_field(key).verbose_name
            data_issues = value
            dict_for_issues.append(name_issue + ' : ' + data_issues)

    if not dict_for_issues:
        dict_for_issues.append('Заключение эксперта не указано')

    for photo in report.photos_auto.filter(photo_type=report_constant.type_photo['photo_front_views'])[:15]:
        photo_type = report_constant.PHOTO_TYPE[photo.photo_type][1]
        if photo.googlePhoto:
            get_url = 'https://' + site + '/google_photo/google_images_obj/' + str(photo.googlePhoto.id)

            data = {
                "name": photo_type,
                "url": get_url,
            }
            dict_for_photos.append(data)

        if photo.image_google:
            get_url = 'https://' + site + '/google_photo/google_images/' + photo.image_google,

            data = {
                "name": photo_type,
                "url": get_url,
            }
            dict_for_photos.append(data)

    return dict_for_damages, dict_for_issues, dict_for_photos