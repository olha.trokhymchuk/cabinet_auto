import os, sys
import ftplib
import json

class Reader:
    def __init__(self):
        self.data = ""
    def __call__(self,s):
        try:
            self.data += s.decode('UTF-8').replace('\\', '')
        except UnicodeDecodeError:
            pass

class FtpHelper(object):
	"""docstring fos FtpHelper"""
	remote_dir = ""
	parent_dir = ""
	search_file = ""
	def __init__(self, arg):
		super (FtpHelper, self).__init__()
		self.ftp_connect()
		self.arg = arg

	#def ftp_connect():
		#server = settings.FTP_URL
		#self.ftp = ftplib.FTP(server)
		#UID = settings.FTP_USER
		#self.ftp.login(UID, settings.FTP_PSW)

	def download_from_ftp(self):
		self.download_ftp_tree(self)

	def download_ftp_tree(self, overwrite=False, guess_by_extension=True):
		path = self.parent_dir.lstrip("/")
		original_directory = os.getcwd()    # remember working directory before function is executed
		os.chdir(self.parent_dir)               # change working directory to ftp mirror directory
		self._mirror_ftp_dir(self.remote_dir, overwrite, guess_by_extension)
		os.chdir(original_directory)  

	def display_ftp_file(self, filename):
		self.search_file = "{}/{}".format(self.parent_dir, filename)
		original_directory = os.getcwd()    # remember working directory before function is executed
		try:
			try:
				return self._display_ftp_file(self.search_file)
			except (ftplib.error_perm, UnicodeEncodeError):
				pass
			os.chdir(original_directory)        # reset working directory to what it was before function exec
		except FileNotFoundError as ex:
			print(ex)

	def _get_dir_items(self, name):
		return self.ftp.nlst(name)
	def get_dir_items(self, name):
		return self._get_dir_items(name)

	def _mirror_ftp_dir(self, name, overwrite, guess_by_extension):
		for item in self._get_dir_items(name):
			if self._is_ftp_dir(item, guess_by_extension):
				if not self.dir_time_create(item):
					print('not time')
					continue
				self._mirror_ftp_dir(item, overwrite, guess_by_extension)
			else:
				if not self._download_ftp_file(item, item, overwrite):
					continue

	def _display_ftp_file(self, filename):
		r = Reader()
		self.ftp.retrbinary('RETR {}'.format(filename), r)
		return r.data

	def _is_ftp_dir(self, name, guess_by_extension=True):
		if guess_by_extension is True:
			if len(name) >= 4:
				if name[-4] == '.':
					return False
		original_cwd = self.ftp.pwd()     # remember the current working directory
		try:
			self.ftp.cwd(name)            # try to set directory to new name
			self.ftp.cwd(original_cwd)    # set it back to what it was
			return True
		except ftplib.error_perm as e:
			return False  	
		except:
			return False

	def get_names_from_dir(self):
		return[f.path.split('/')[2] for f in os.scandir(self.parent_dir +'/'+ self.remote_dir) if f.is_dir()]

	def _make_parent_dir(self, fpath):
		dirname = os.path.dirname(fpath)
		while not os.path.exists(dirname):
			try:
				os.makedirs(dirname)
				#print("created {0}".format(dirname))
			except:
				self._make_parent_dir(dirname)

	def _download_ftp_file(self, name, dest, overwrite):
		self._make_parent_dir(dest.lstrip("/"))
		if not os.path.exists(dest) or overwrite is True:
			try:
				with open(dest, 'wb') as f:
					self.ftp.retrbinary("RETR {0}".format(name), f.write)
				#print("downloaded: {0}".format(dest))
			except FileNotFoundError:
				print("FAILED: {0}".format(dest))
		else:
			pass
			#print("already exists: {0}".format(dest))

	def get_file(self, filename):
		try:
			self.ftp.retrbinary('RETR '  + filename, open(filename, 'wb').write)
		except json.decoder.JSONDecodeError:
			self.get_files_from_ftp(filename)
		except ftplib.error_reply:
			self.get_files_from_ftp(filename)
		else:
			return

	def ftp_connect(self):
		server = settings.FTP_URL
		ftp = ftplib.FTP(server)
		UID = settings.FTP_USER
		ftp.login(UID, settings.FTP_PSW)
		return ftp

	def get_files_from_ftp(self, filename):
		print(filename)
		r = self.get_file(filename)
		try:
			data = r
		except AttributeError:
			#print('has no attribute data')
			r = self.get_file(filename)
		ftp.close()
		return data

