import json
import tempfile
from datetime import datetime, timedelta
from report.models import ReportPodbor, ReportAutoLibrary
from report.workers.create_json_file import ftp_connect, sftp_connect, create_json_report_file


def get_date_now():
    date_now = datetime.now()
    return date_now


def get_reports():
    reports = ReportPodbor.objects.filter(upload_ftp_status=True,
                                          success_upload_status=False,
                                          executor__isnull=False,
                                          executor__filials__isnull=False,
                                          created__date=datetime.today() - timedelta(days=32),
                                          photos_auto__photo_type=14,
                                          photos_auto__image__exact='',
                                          auto__model_auto__isnull=False,
                                          auto__mark_auto__isnull=False,
                                          auto__year_auto__isnull=False).order_by('id').distinct('id').reverse()
    return reports


def upload_daily_reports():
    reports_data = []
    name = 'Avtopodbor_'
    et = '_12_'
    date = get_date_now().strftime("%Y%m%d-%H%M%S")
    format = '.json'
    get_data = create_json_report_file(get_reports())

    if get_data['json_data'] and get_data['dict_for_photos']:
        with tempfile.TemporaryDirectory() as tmp_dirname:
            with tempfile.NamedTemporaryFile(mode='w', suffix='.json',
                                             prefix=date, dir=tmp_dirname, encoding='utf-8') as out_file:
                out_file.write(json.dumps(get_data['dict_to_upload']))
                filial_id = get_data['filial_id'][0]
                full_name = name + str(filial_id) + et + date + format
                out_file.seek(0)
                get_file = open(out_file.name, 'rb')
                conn_ftp = ftp_connect()
                conn_ftp.storbinary('STOR ' + 'pub/' + full_name, get_file)
                conn_ftp.close()
                print('file name: ', full_name)
                print('Process upload to ftp finished _______________')
                sftp_connect().put(out_file.name, '/pub/' + full_name)
                out_file.close()
                sftp_connect().close()
                print('Process upload to sftp finished _______________')
                reports = get_data['reports']
                for report in reports:
                    reports_data.append({'report_id': report.id})
                    report.success_upload_status = True
                    report.save()
                print('reports status updates ')
                ReportAutoLibrary.objects.create(reports_data=reports_data, file_name=full_name)
                return full_name
    else:
        print('not valid reports')
        pass
