import os
import ftplib
import json
import re
import shutil
import logging
import time
import datetime
from io import BytesIO
import io

from PIL import Image, ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True
from django.core.files.uploadedfile import InMemoryUploadedFile

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.core.files import File
from django.core.files.base import ContentFile
from django.db.utils import IntegrityError

from notifications.models import Notification
from notifications.utils import send_notification

from order.models import Order
from report.models import ReportPodbor, Photo
from report.serializers import ReportSerializerCreateUpdateV3

from core.models import User

import report.report_constants as report_constant

from auto.models import AutoPhoto
import auto.glossary as auto_glossary

from report.workers.ftpHelper import FtpHelper

logger = logging.getLogger(__name__)


class ExportMobileField(FtpHelper):
	def __init__(self, *args, **kwargs):
		self.remote_dir = 'success'
		self.field = kwargs["field"]
		self.field_mobile = kwargs["field_mobile"]
		self.parent_dir ='report'
		FtpHelper.__init__(self, *args)
		self.name = ""
		self.get_reports_from_mobile()

	def ftp_connect(self):
		server = settings.FTP_URL
		self.ftp = ftplib.FTP(server)
		UID = settings.FTP_USER
		self.ftp.login(UID, settings.FTP_PSW)     

	def get_reports_from_mobile(self):
		reports = ReportPodbor.objects.filter(executor=None)
		for report in reports:
			if report.auto:
				self.parent_dir = "/success/{}".format(report.auto)
				filename = "{}.json".format(report.auto)
				self.create_report(self.display_ftp_file(filename))


	def dir_time_create(self, name):
		return True

		return report

	def create_report(self, data):
		try:
			data = json.loads(data)
			report = ReportPodbor.objects.get(vin=data["Vin"])
			print("report.pk")
			print(report.pk)

			setattr(report, self.field, data[self.field_mobile])
			report.save()
		except (IntegrityError, ReportPodbor.DoesNotExist, KeyError, TypeError, json.decoder.JSONDecodeError) as ex:
			print(ex)
