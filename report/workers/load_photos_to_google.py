from datetime import datetime, timedelta

from report.models import Photo
from google_photo.models import GooglePhotoImage
from google_photo.utils import upload_media_data_to_google, upload_images_by_tokens
from django.core.files.base import ContentFile
from pprint import pprint
import os.path
import logging
import time
from django.core.exceptions import ValidationError
from django.conf import settings

SLEEP_BEFORE_UPLOAD = settings.SLEEP_BEFORE_UPLOAD
SLEEP_AFTER_UPLOAD = settings.SLEEP_AFTER_UPLOAD
logger = logging.getLogger(__name__)


class loadPhotosToGoogle():
    def __init__(self, *args, **kwargs):
        self.currentPhoto = None
        self.tokenList = {}
        self.start_runner_upload_photo()

    def uploadMedia(self):
        with open(self.currentPhoto.image.path, "rb") as f:
            data = ContentFile(f.read())
            token, service = upload_media_data_to_google(data)
            if not self.currentPhoto.report_podbor.google_album in self.tokenList:
                self.tokenList.update({self.currentPhoto.report_podbor.google_album: {}})
            if not 'newMediaItems' in self.tokenList[self.currentPhoto.report_podbor.google_album]:
                self.tokenList[self.currentPhoto.report_podbor.google_album].update({'newMediaItems': []})
            self.tokenList[self.currentPhoto.report_podbor.google_album]['newMediaItems'].append(
                {
                    "simpleMediaItem": {
                        "uploadToken": token
                    }
                }
            )
            return {'token': token, 'albumId': self.currentPhoto.report_podbor.google_album, 'photo': self.currentPhoto}

    def convertTokenList(self):
        listUploadItems = []
        for albumId in self.tokenList:
            listUploadItems.append(
                {
                    "albumId": albumId,
                    "newMediaItems": self.tokenList[albumId]['newMediaItems']
                }
            )
        self.tokenList = listUploadItems

    def uploadToGoogle(self, photos):
        logger.error('==== Start process =====')
        tokens = {}
        for photo in photos:
            self.currentPhoto = photo
            if photo.image:
                if not os.path.exists(photo.image.path):
                    logger.error("deleted image ====== "+str(photo.id))
                    print("deleted image ====== "+str(photo.id))
                    print(photo.image.path)
                    photo.delete()
                else:
                    if photo.report_podbor:
                        pprint(photo.image.path)
                        if not photo.report_podbor.google_album:
                            photo.report_podbor.set_google_album()
                        if photo.report_podbor.google_album in tokens:
                            tokens[photo.report_podbor.google_album].append(self.uploadMedia())
                        else:
                            tokens.update({photo.report_podbor.google_album: []})
                            tokens[photo.report_podbor.google_album].append(self.uploadMedia())
            if not photo.image and not photo.image_google:
                logger.error("deleted image ( no image, no google image) ====== " + str(photo.id))
                print("deleted image ( no image, no google image) ====== " + str(photo.id))
                photo.delete()
        self.convertTokenList()
        uploadResult = upload_images_by_tokens(self.tokenList)
        for uploadAlbumId in tokens:
            for uploadItems in tokens[uploadAlbumId]:
                token = uploadItems['token']
                if uploadAlbumId in uploadResult:
                    uploadedItems = uploadResult[uploadAlbumId]
                    for uploadItem in uploadedItems:
                        if uploadItem['status']['message'] == 'Success' and uploadItem['uploadToken'] == token:
                            photo = uploadItems['photo']
                            photoId = uploadItem['mediaItem']['id']
                            googlePhoto = GooglePhotoImage(imageId=photoId, albumId=uploadAlbumId)
                            googlePhoto.save()
                            photo.googlePhoto = googlePhoto
                            photo.image.delete()
                            if photo.image_small:
                                if not os.path.exists(photo.image_small.path):
                                    photo.image_small.delete()
                            photo.save()
                            logger.error('==== uploaded img google =====')
                            logger.error(photo.googlePhoto.id)
                            logger.error('==== END uploaded img google =====')
                            pprint(photo.googlePhoto.id)

        logger.error('==== End process =====')

    def get_photos_to_upload(self):
        how_many_days = 30
        return Photo.objects.filter(googlePhoto__isnull=True, image_google__exact='',
                                    created__lt=datetime.now()-timedelta(days=how_many_days)).order_by('-id')[:45]

    def start_runner_upload_photo(self):
        photos = self.get_photos_to_upload()
        while photos:
            self.uploadToGoogle(photos)
            time.sleep(SLEEP_BEFORE_UPLOAD)
            self.tokenList = {}
            photos = self.get_photos_to_upload()
        time.sleep(SLEEP_AFTER_UPLOAD)
