import time
from report.models import ReportPodbor


def get_reports_to_published():
    return ReportPodbor.objects.filter(published__isnull=True).order_by('-id')[:100]


def change_published_status(reports):
    for report in reports:
        report.published = True
        report.save()
        print('report save: ', report.id)


def start_runner_change_published_report():
    reports = get_reports_to_published()
    while reports:
        change_published_status(reports)
        time.sleep(10)
        reports = get_reports_to_published()
    print('Finished !')
    return True