import requests
import os
import json
import time
import signal
import sys
import subprocess
from phpserialize import serialize, unserialize
import re
import six
import datetime
from urllib.parse import urlparse

import logging

import auto.glossary as auto_gloss
import report.workers.export_lk.lk_constants as lk_const

from django.conf import settings
from django.shortcuts import get_object_or_404
from django.core.exceptions import MultipleObjectsReturned
from django.contrib.sites.models import Site

from django.db.utils import OperationalError, DataError
from django.db import connections

from notifications.models import Notification
from notifications.utils import send_notification


from report.models import ReportPodbor, Photo
from report.workers.parse_from_json_to_row import setRow

from auto.models import Auto, MarkAuto, ModelAuto, Year, BodyType, AutoPhoto
import time
import imagekit

from bs4 import BeautifulSoup

from core.models import User


user_instance = User.objects.get(username="70000000000")
ReportPodbor.user = user_instance
Photo.user = user_instance
Auto.user = user_instance
Auto.author_type = 4
MarkAuto.user = user_instance
ModelAuto.user = user_instance
Year.user = user_instance
BodyType.user = user_instance
AutoPhoto.user = user_instance

logger = logging.getLogger(__name__)

MARK = {
	"кия": "Kia"
}
MODEL = {
	"сид": "Ceed"
}

class lkParser:
	select_by = "list"
	def __init__(self, lk_ids):

		try:
			self.cursor = connections['lk'].cursor()
		except (OperationalError, Exception) as ex:
			subprocess.call(['/home/aleksey/set_crm_port.sh'])
			self.cursor = connections['lk'].cursor()
		self.data_lk = {"photos":[]}
		p = subprocess.Popen('ps aux | grep   parsing_lk by_last', shell=True, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
		length = p.stdout.readlines()
		if len(length) > 1:
			print(len(length))
			os.kill(os.getpid(), signal.SIGTERM)
			return
		if lk_ids == "by_last":
			self.select_by = "by_last"
			last_report = ReportPodbor.objects.filter(lk_id__isnull=False).order_by("pk").last()
			self.lk_id_sesond = 29800
			for i in range(last_report.lk_id + 1, 29800):
					self.lk_id = i
					try:
						self.parsing()
					except IndexError as ex:
						print(ex)
		print(type(lk_ids))
		if type(lk_ids) == list and len(lk_ids) == 1:
			self.lk_id = lk_ids[0]
			try:
				self.parsing()
			except IndexError as ex:
				print(ex)
		if type(lk_ids) == list and len(lk_ids) == 2:
			print("type")
			self.lk_id_sesond = lk_ids[1]
			print(type(lk_ids))
			for i in range(int(lk_ids[0]), int(lk_ids[1])):
				self.lk_id = i
				try:
					self.parsing()
				except IndexError as ex:
					print(ex)

	def select_query(self, param, value):
		logger.error(value)
		self.cursor.execute("SELECT update_user, new_data, last_data FROM cars where {}={}".format(param, value))
		logger.error(value)
		return self.cursor.fetchone()	

	def get_page(self):
		s = requests.Session()
		# result = s.get('https://xn--80aabz.xn----7sbecl2dbcfoo.xn--p1ai/base/car/28857?parsing_11_8999=333333')
		result = s.get('https://xn--80aabz.xn----7sbecl2dbcfoo.xn--p1ai/base/car/{}?parsing_11_8999=333333'.format(self.lk_id))
		return result.text

	def parsing(self):
		page = self.get_page()
		data = False
		while data is False:
			try:
				data = self.select_query("id", self.lk_id)
			except OperationalError:
				time.sleep(5)
				self.cursor = connections['lk'].cursor()
				subprocess.call(['python', settings.BASE_DIR  + '/manage.py', 'parsing_lk', "{},{}".format(str(self.lk_id), str(self.lk_id_sesond))])
				# if self.select_by == "by_last":
				# 	subprocess.call(['python', settings.BASE_DIR  + '/manage.py', 'parsing_lk', "{},{}".format(self.select_by)])
				# else:
				# 	subprocess.call(['python', settings.BASE_DIR  + '/manage.py', 'parsing_lk', "{},{}".format(str(self.lk_id), str(self.lk_id_sesond))])

				os.kill(os.getpid(), signal.SIGTERM)
				return
		try:
			if data[0] != 1:
				new_data = data[2];
				data = data[1];
				print(1111111111)
			else:
				data = data[2];
			last_data = unserialize(data.encode())
			last_date = last_data[b'create'].decode()
			self.created = time.strftime("%Y-%m-%d %H:%M:%S.0000Z", time.localtime(int(last_date)))
		except (TypeError, KeyError) as ex:
			pass
		except (ValueError) as ex:
			pass
		soup = BeautifulSoup(page, "html.parser")
		self.get_auto(soup)
		self.get_pluses_minuses(soup)
		rep_id = self.get_report(soup)
		self.save_photo(soup)

	def prepare_string(self, string):
		return string.lower().rstrip().lstrip()

	def get_pluses_minuses(self, soup):
		self.pluses = ''
		self.minuses = ''
		result = soup.find(True, {'class':['rew-car-page']}).find(True, {'class':['left-block']}).find_all(True, {'class':['item-rew']})
		for plus in result:
			if self.pluses:
				self.pluses += '\n{}'.format(plus.text.strip())
			else:
				self.pluses += '{}'.format(plus.text.strip())

		result = soup.find(True, {'class':['rew-car-page']}).find(True, {'class':['right-block']}).find_all(True, {'class':['item-rew']})
		for minus in result:
			if self.minuses:
				self.minuses += '\n{}'.format(minus.text.strip())
			else:
				self.minuses += '{}'.format(minus.text.strip())

		self.data_lk.update({
			'pluses': self.pluses, 'minus': self.minuses
		})

	def get_body_type(self):
		body_type = self.res[3].text.split('\n')[2]
		self.data_lk.update({'body_type_auto': body_type})
		logger.error('body_type')
		logger.error(body_type)
		try:
			b_t = self.prepare_string(body_type)
			logger.error(b_t)
			body_type = lk_const.BODYTYPE[b_t]

		except KeyError as ex:
			body_type = body_type.capitalize()

		logger.error(body_type)
		try:
			body_type_auto = BodyType.objects.get(name=body_type)
			logger.error(body_type_auto)
		except BodyType.DoesNotExist:
			body_type_auto = None
		self.auto_data_lk.update({'body_type_auto': body_type_auto})

	def get_mileage(self):
		mileage = self.res[3].text.split('\n')[4]
		mileage = self.numbers_from_string(mileage)
		try:
			self.auto_data_lk.update({'mileage': mileage[-1]})
		except ValueError as ex:
			logger.error(ex)
		self.data_lk.update({'mileage': mileage})

	def get_engine_type(self):
		engine_type_parse = self.res[6].text.split('\n')[2]
		engine_type = 0
		for choice in lk_const.ENGINE_TYPE:  
			if str(choice[1]).lower() == self.prepare_string(engine_type_parse):
				engine_type = int(choice[0])
				break
		self.auto_data_lk.update({'engine_type': engine_type})
		self.data_lk.update({'engine_type': engine_type_parse })
	
	def get_vin(self):
		self.vin = self.res[7].text.split('\n')[2]
		self.auto_data_lk.update({'vin': self.vin})
		self.data_lk.update({'vin': self.vin})

	def get_drive_type(self):
		drive_type_parse = self.res[0].text.split('\n')[4]
		drive_type = 0
		self.data_lk.update({'drive_type': drive_type_parse})
		
		for choice in auto_gloss.DRIVE_TYPE:
			if str(choice[1]).lower() == self.prepare_string(drive_type_parse) or self.prepare_string(drive_type_parse) in str(choice[1]).lower():
				drive_type = int(choice[0])
				break
		self.auto_data_lk.update({'drive_type': drive_type})
		self.data_lk.update({'drive_type': drive_type})

	def get_color_auto(self):
		color_auto = self.res[1].text.split('\n')[4]
		self.auto_data_lk.update({'color_auto':color_auto})
		self.data_lk.update({'color_auto': color_auto})

	def get_source(self):
		try:
			source = self.res[2].find('a')['href']
			self.auto_data_lk.update({'source':source})
			self.data_lk.update({'source': source})
		except TypeError:
			pass

	def get_transmission_type(self):
		transmission_type = 0
		transmission_type_parse = self.res[4].text.split('\n')[2]
		transmission_type_parse = re.sub(r'[?|$|.|!]',r'',transmission_type_parse).strip()
		for choice in auto_gloss.TRANSMISSION_TYPE_LK:  
			logger.error(str(choice[1]).lower())
			logger.error(len(str(choice[1]).lower()))
			logger.error(str(transmission_type_parse))
			logger.error(len(str(transmission_type_parse)))
			if str(choice[1]).lower() == transmission_type_parse.lower():
				transmission_type = int(choice[0])
				logger.error(transmission_type)
				break
		self.auto_data_lk.update({'transmission_type':transmission_type})
		self.data_lk.update({'transmission_type': transmission_type_parse})
	
	def get_engine_capacity(self):
		engine_capacity_parse = self.res[5].text.split('\n')[2].replace(",", ".").replace("л", "")
		logger.error(engine_capacity_parse)
		engine_capacity = 0
		for choice in auto_gloss.ENGINE_CAPACITY:  
			if str(choice[0]).lower() == engine_capacity_parse.lower():
				engine_capacity = float(choice[0])
				break
		self.auto_data_lk.update({'engine_capacity': engine_capacity})
		self.data_lk.update({'engine_capacity': engine_capacity_parse})
	
	def get_year(self):
		year = self.res[1].text.split('\n')[2]
		year = self.numbers_from_string(year)[-1]
		self.data_lk.update({'year': year})
		logger.error(year)
		try:
			year_auto = Year.objects.filter(year=int(year)).first()
		except (Year.DoesNotExist, DataError, ValueError) as ex:
			logger.error(ex)
		else:
			self.auto_data_lk.update({'year_auto': year_auto})
		self.data_lk.update({'year_auto': year})

	def get_salon_auto(self):
		salon_auto = self.res[2].text.split('\n')[4]
		if len(salon_auto) > 50:
			return
		self.auto_data_lk.update({'salon_auto': salon_auto})
		self.data_lk.update({'salon_auto': salon_auto})

	def get_equipment(self):
		equipment = self.res[4].text.split('\n')[4]
		try:
			self.auto_data_lk.update({'equipment': equipment})
		except ValueError as ex:
			logger.error(ex)
		self.data_lk.update({'equipment': equipment})

	def get_owners(self):
		owners = 0
		owners = self.res[5].text.split('\n')[4]
		owners = self.numbers_from_string(owners)
		try:
			if owners != []:
				owners = owners[-1]
			else:
				owners = 0
		except (ValueError, KeyError) as ex:
			logger.error(ex)
			return
		if len(str(owners)) < 3:
			self.auto_data_lk.update({'owners': owners})


	def get_cost_after(self):
		try:
			self.cost_after_str = self.res[6].text.split('\n')[4]
			self.cost_after = self.numbers_from_string(self.cost_after_str)	
		except ValueError as ex:
			logger.error(ex)
		if len(str(self.cost_after)) <= 6:
			self.cost_after
			self.data_lk.update({'cost_after': self.cost_after})
		else:
			self.cost_after = None

	def get_auto(self, soup):
		self.auto_data_lk = {}
		results = soup.find_all(True, {'class':['car']})
		self.res = results[0].find_all("tr")
		# ['', 'Марка, модель:', 'Hyundai Tucson I ', 'Привод:', 'Полный', '']
		# ['', 'Год выпуска:', '2008', 'Цвет:', 'Серебристый металлик', '']
		# ['', 'Ссылка на обьявление:', 'Перейти ', 'Салон:', 'Ткань', '']
		# ['', 'Тип кузова:', 'Внедорожник ', 'Пробег:', '85730', '']
		# ['', 'Тип кпп:', 'АКПП', 'Комплектация:', '', '']
		# ['', 'Обьем:', '2.0', 'Кол-во хозяев:', '1', '']
		# ['', 'Тип двигателя:', 'Дизель', 'Стоимость после торга:', '580000', '']
		# ['', 'Vin-код:', 'KMHJM81VP9U035951', '', '', '']
		self.getMarkModel()
		self.get_year()
		self.get_salon_auto()
		self.get_body_type()
		self.get_mileage()
		self.get_equipment()
		self.get_owners()
		self.get_engine_type()
		self.get_cost_after()
		self.get_vin()
		self.get_drive_type()
		self.get_color_auto()
		self.get_source()
		self.get_transmission_type()
		self.get_engine_capacity()
		self.auto_data = Auto.objects.filter(vin=self.vin).first()
		try:
			if len(self.auto_data_lk["source"]) > 250:
				self.auto_data_lk["source"] = ""
		except KeyError:
			pass
		print(self.auto_data_lk)
		if not self.auto_data:
			self.auto_data = Auto(**self.auto_data_lk)
		else:
			for k, v in six.iteritems(self.auto_data_lk):
				try:
					setattr(self.auto_data, k, v)
				except AttributeError as ex:
					logger.error(ex)
		self.auto_data.data_lk = self.data_lk
		self.auto_data.author_type = 4
		print(self.auto_data)
		self.auto_data.save()
		logger.error(self.auto_data.pk)

	def getMarkModel(self):
		mark_name = ''
		model_name = ''
		self.m_m_g = self.res[0].text
		m_m_g = self.m_m_g.split('\n')[2].split()
		try:
			mark_name = m_m_g[0]
		except IndexError as ex:
			print(ex)
		try:	
			model_name = m_m_g[1]
			logger.error(model_name)
		except IndexError as ex:
				print(ex)
		self.data_lk.update({
			'mark': mark_name, 'model': model_name
		})
		try:
			mark_name = MARK[mark_name.lower()]
			logger.error(mark_name)
		except KeyError as ex:
			pass
		logger.error(model_name)
		mark = None
		try:
			mark = MarkAuto.objects.filter(name=mark_name).first()
			self.auto_data_lk.update({'mark_auto': mark})
			logger.error(mark)
		except MarkAuto.DoesNotExist as ex:
			logger.error(ex)
			
		try:
			model_name = MODEL[model_name.lower()]
		except KeyError as ex:
			model_name = model_name.capitalize()

		logger.error('mark')
		logger.error(mark)
		try:
			model = ModelAuto.objects.filter(name=model_name, mark_auto=mark).first()
			self.auto_data_lk.update({'model_auto': model})
		except ModelAuto.DoesNotExist as ex:
			logger.error(ex)
		logger.error('model')
		logger.error(model)

	def numbers_from_string(self, from_str):
		try:
			return [int(from_str)]
		except ValueError as ex:
			logger.error(ex)
		numbers = []
		from_str = from_str.replace(" ", "")
		#from_str = re.sub(r'\b.\b', '', from_str)
		logger.error(from_str)
		from_str = re.split(r'\D', from_str)
		logger.error(from_str)
		for number in from_str:
			logger.error(number)
			try:
				numbers.append(int(number))		
			except ValueError:
				pass
		if numbers == []:
			try:
				numbers.append(int(re.sub(r'\D', '', from_str)))
			except TypeError as ex:
				logger.error(ex)
		return numbers

	def report_step(self, soup):
		steps_data = os.path.join(os.getcwd(), "report/fixtures/mobile.json")   	
		with open(steps_data) as steps_file:
			order_data = json.load(steps_file)
		left_block = soup.find(True, {'class':['block-car']}).find(True, {'class':['left-block']})
		right_block = soup.find(True, {'class':['block-car']}).find(True, {'class':['right-block']})
		
		car_element = {} 
		left_part= {
			1: 'капот',
			2: 'правый порог',
			3: 'заднее правое крыло',
			4: 'задняя правая дверь',
			5: 'передняя правая дверь',
			6: 'передний бампер',
			7: 'переднее правое крыло',
			8: 'крыша',
			9: 'колесо заднее правое', # нету
			10: 'колесо передняе правое', # нету
			11: 'заднее правое малое стекло', # нету
			12: 'заднее правое стекло',
			13: 'переднее правое стекло',
			14: 'лобовое стекло',
			15: 'стойка передняя правая',
		}

		right_part = {
			1: 'переднее левое крыло',
			2: 'левый порог',
			3: 'крышал',
			4: 'передняя левая дверь',
			5: 'багажник',
			6: 'заднее левое крыло',
			7: 'задний бампер',
			8: 'задняя левая дверь',
			9: 'стойка передняя левая',
			10: 'переднее левое стекло',
			11: 'заднее левое стекло',
			12: 'заднее левое малое стекло',
			13: 'заднее стекло',
			14: 'колесо левое переднее',
			15: 'колесо левое заднее',
		}

		for i in range(1, 16):
			block_left= left_block.find('div', {'class':['block-' + str(i)]})
			block_right = right_block.find('div', {'class':['block-' + str(i)]})
			
			try:
				cleanString = re.sub('\W+',' ', block_left.text )
				if 'g' in block_left.attrs['class']:
					car_element[left_part[i]] = {'value': True, 'comment': cleanString}
					self.data_lk.update({str(left_part[i]): str({'value': True, 'comment': cleanString})})
				elif 'r' in block_left.attrs['class']:
					car_element[left_part[i]] = {'value': False, 'comment': cleanString}
					self.data_lk.update({str(left_part[i]): str({'value': False, 'comment': cleanString})})
				elif 'y' in block_left.attrs['class']:
					car_element[left_part[i]] = {'value': False, 'comment': cleanString}
					self.data_lk.update({str(left_part[i]): str({'value': False, 'comment': cleanString})})
				else:
					pass
			except AttributeError:
				pass
			try:	
				if 'g' in block_right.attrs['class']:
					car_element[right_part[i]] = {'value': True, 'comment': block_right.text}
					self.data_lk.update({str(right_part[i]): str({'value': True, 'comment': cleanString})})
					logger.error(car_element)
				elif 'r' in block_right.attrs['class']:
					car_element[right_part[i]] = {'value': False, 'comment': block_right.text}
					self.data_lk.update({str(right_part[i]): str({'value': False, 'comment': cleanString})})
				elif 'y' in block_right.attrs['class']:
					car_element[right_part[i]] = {'value': False, 'comment': block_right.text}
					self.data_lk.update({str(right_part[i]): str({'value': False, 'comment': cleanString})})
				else:
					pass
			except AttributeError:
				pass
			logger.error(car_element)
		try:
			car_element['стойка центральная левая'] = car_element['стойка передняя левая']
			self.data_lk.update({'стойка центральная левая': car_element['стойка передняя левая']})
		except KeyError as ex:
			pass
		try:
			car_element['стойка задняя левая'] = car_element['стойка передняя левая']
			self.data_lk.update({'стойка задняя левая': car_element['стойка передняя левая']})
		except KeyError as ex:
			pass
		try:
			car_element['стойка центральная правая'] = car_element['стойка передняя правая']
			self.data_lk.update({'стойка центральная правая': car_element['стойка передняя правая']})
		except KeyError as ex:
			pass
		try:
			car_element['стойка задняя левая'] = car_element['стойка передняя правая']
			self.data_lk.update({'стойка задняя левая': car_element['стойка передняя правая']})
		except KeyError as ex:
			pass

		for step in order_data:
			if step['name']=='Кузов':
				for params in step['groups']:
					for param in params['fields']:
						for o in car_element:
							if param['label'].lower() == o:
								try:
									mobile_param = car_element[o]
								except KeyError as ex:
									print(ex)
									continue
								param['value'] = mobile_param['value']
								param['comment'] = mobile_param['comment']
		return order_data

	def result_expert(self, soup):
		result_expert = {}
		status = soup.find('div', {'class':['text-car-block-exp']})
		try:
			result = status.find(True, {'class':['text']})
		except AttributeError as er:
			pass
		else:	
			result_expert.update({'text': '<p>{}</p>'.format(result.text)})
		try:
			if 'g' in status.attrs['class']:
				result_expert.update({'recommended': True})
			elif 'r' in status.attrs['class']:
				result_expert.update({'recommended': False})
			elif 'y' in status.attrs['class']:
				result_expert.update({'recommended': False})
		except AttributeError as ex:
			pass
		try:
			self.data_lk.update({'recommended': result_expert['recommended']})
		except KeyError as ex:
			logger.error(ex)
		return result_expert

	def get_report(self, soup):
		report_data = self.report_step(soup)
		result_expert = self.result_expert(soup)
		report_model = { 
			"auto": self.auto_data, 
			"data" :report_data,
			"pluses":self.pluses,
			"minuses":self.minuses,
		}
		prefix = '(Загружено из лк.авто-подбор.рф (старый формат отчета))'
		report_model.update({"comment_expert":prefix})
		try:
			report_model.update({"result_expert":result_expert['text']})
		except (ValueError,IndexError, KeyError) as ex:
			pass
		try:
			self.data_lk.update({'result_expert': result_expert['text']})
		except KeyError as ex:
			pass
		try:
			report_model.update({"recommended":result_expert['recommended']})
		except (ValueError,IndexError, KeyError) as ex:
			pass
		try:
			report_model.update({"cost_after":self.cost_after[-1]})
		except (TypeError, ValueError,IndexError, KeyError) as ex:
			logger.error(ex)
		
		report_model.update({"executor": user_instance})
		try:
			report_model.update({"created": self.created})
		except AttributeError:
			pass
		logger.error("autoautoautoauto")
		logger.error(report_model)
		self.rep, created = ReportPodbor.objects.get_or_create(lk_id=int(self.lk_id), defaults=report_model)
		if created:
			logger.error('created')
			pass
		else:
			logger.error('update {}'.format(self.rep.pk))												
			for k, v in six.iteritems(report_model):
				setattr(self.rep, k, v)
			self.rep.save()
			print(self.rep.pk)
			logger.error("report preupdate")
		if created:
			try:
				self.rep.created = self.created
				self.rep.save()
			except AttributeError:
				pass
		return self.rep

	def save_photo(self, soup):
		image_from = ""
		urls = soup.find_all(True, {'class':['mini-photo-item']})
		for url in urls:
			url = url.find('img')['src']
			o = urlparse(url)
			defaults = {
				'report_podbor': self.rep, 
				'from_inspection': True
			}
			if o.netloc == 'xn--j1ab.xn----7sbecl2dbcfoo.xn--p1ai':
				path = o.path.split('/')
				path[1] = 'lk_image'
				path = [x for x in path if x != 'xn--j1ab.xn----7sbecl2dbcfoo.xn--p1ai' and x != '' and x != 'images']
				new_path = '/'.join(path)
				if new_path.endswith('/'):
					new_path = new_path[:-1]
				host = Site.objects.get_current().domain
				url = 'reports/{new_path}'.format(host=host, new_path=new_path)
				url.encode('utf-8')
				image_from = "lk"
				defaults['image'] = url
			if o.netloc ==  'img-fotki.yandex.ru':
				image_from = "ya"
				defaults['embed_url'] = url

			if image_from == "ya":
				photos = Photo.objects.filter(embed_url=url).first()	
			else:
				photos = Photo.objects.filter(image=url).first()	
			if not photos:
				photos = Photo(**defaults)
				photos.save()
			try:
				photos.image_small.generate()
			except (IsADirectoryError, FileNotFoundError, imagekit.exceptions.MissingSource) as ex:
				pass
			self.data_lk['photos'].append(url)
			self.rep.save()
			setRow([self.rep.pk])

# ------ нету ------	
# 'передние фонари'
# 'задние фонари'
# 'передний лонжерон левый'
# 'передний лонжерон правый'
# 'чашка передняя левая'
# 'чашка передняя правая'
# 'моторный щит'
# 'центральный тоннель'
# 'пол'
# 'задняя чашка левая'
# 'задняя чашка правая'
# 'пол багажника'
# ------ совместить ------
