import auto.glossary as auto_gloss

BODYTYPE = {
	"универсал": "Универсал 5 дв.",
	"хетчбэк 3 дв ": "Хэтчбек 3 дв.",
	"седан": "Седан",
	"хетчбэк": "Хэтчбек 5 дв.",
	"хетчбек": "Хэтчбек 5 дв.",
	"хэтчбек": "Хэтчбек 5 дв.",
	"лифтбэк": "Лифтбек"
}

ENGINE_TYPE = (
    (auto_gloss.ENGINE_ANY, 'любой'),
    (auto_gloss.ENGINE_PETROL, 'бензин'), (auto_gloss.ENGINE_DIESEL, 'дизель'),
    (auto_gloss.ENGINE_PETROL, 'бензиновый'),
    (auto_gloss.ENGINE_GEBRID, 'гибрид'), (auto_gloss.ENGINE_ELECTRO, 'электро'),
)