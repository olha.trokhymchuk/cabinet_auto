import io
import json
import re
import logging
import json
from core.decorators import recaptcha_check
from django.http.response import HttpResponseRedirect
from payment_page_sdk.gate import Gate
from payment_page_sdk.payment import Payment
from django.shortcuts import render
from django.template import RequestContext
from django.template.loader import get_template
from django.core.mail import EmailMessage
import auto.glossary as auto_glossary
from auto.models import AutoPhoto, Generation, ModelAuto, Year, Auto

from django.conf import settings
from django.db.models import Q
from django.shortcuts import get_object_or_404

from django.contrib.auth.decorators import login_required, permission_required

from django.http import JsonResponse, Http404, HttpResponse

from rest_framework.generics import ListCreateAPIView
from rest_framework.views import APIView

from rest_framework.response import Response
from rest_framework import status
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.renderers import TemplateHTMLRenderer

from core.forms import AddUserShortForm
from report.models import ReportPodbor, Photo, OrderReport
from report.serializers import ReportListSerialializer, ReportSerializerV3, ReportSerializerCreateUpdateV3, \
    ReportSerializer, PreviewReportSerializer, ReportSerializerCreateWithApi, PhotoSerializer, \
    ReportSerializerCreateUpdateV4
from rest_framework import serializers
from .pagination import LimitOffsetPagination, PageNumberPagination

from core.models import User
from core.api_serializers import UserSerializer
import report.report_constants as report_constant
from django.views.decorators.csrf import csrf_exempt
from ..decorators import basic_auth_report_api
from ..workers.export_mobile_to_report_api import EportMobileReportApi
from ..workers.upload_daily_reports_to_auto_library import upload_daily_reports

logger = logging.getLogger(__name__)


class ApiReportByUserList(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = ReportPodbor.objects.all()
    serializer_class = ReportListSerialializer
    pagination_class = PageNumberPagination

    def list(self, request, pk):
        user = User.objects.get(pk=pk)
        queryset = self.get_queryset()

        if user.is_client:
            queryset = queryset.filter(order__client=user).distinct()
        else:
            queryset = queryset.filter(Q(executor=user) |
                                       Q(order__expert=user) |
                                       Q(order__expert_check=user) |
                                       Q(order__transferring=user) |
                                       Q(order__operator=user))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ApiReportByFilialList(ListCreateAPIView):
    authentication_classes = (SessionAuthentication, BasicAuthentication)
    queryset = ReportPodbor.objects.all()
    serializer_class = ReportListSerialializer
    pagination_class = PageNumberPagination

    def list(self, request, pk):
        queryset = self.get_queryset()
        queryset = queryset.filter(executor__filials__id=pk).distinct()

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ReportDetaiApi(APIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    authentication_classes = (SessionAuthentication, BasicAuthentication)

    def queryset_by_user(self):
        queryset = ReportPodbor.objects.all()
        if self.request.user.is_client:
            queryset = queryset.filter(order__client=self.request.user)
        try:
            queryset = queryset.get(id=self.kwargs.get('report_id'))
            return queryset
        except ReportPodbor.DoesNotExist:
            raise Http404

    def get(self, request, report_id, format=None):
        report = self.queryset_by_user()
        serializer = ReportSerializerV3(report)
        return Response(serializer.data)

    def put(self, request, report_id=None):
        cusmom_error = False
        collections_photo = {}
        if report_id:
            saved_report = self.queryset_by_user()
        else:
            saved_report = None
        auto_set_or_create = False
        data = json.loads(request.POST["data"])
        try:
            for field in ["mileage", "cost_before"]:
                number = ''.join(re.findall(r'\d+', data["auto"][field]))
                data["auto"][field] = number
        except (KeyError, TypeError) as ex:
            pass
        deleted = request.POST.get("deleted", None)
        logger.error("deleted")
        logger.error(deleted)
        logger.error("deleted")
        if deleted:
            for delet in json.loads(deleted):
                try:
                    Photo.objects.get(pk=delet).delete()
                except Photo.DoesNotExist as ex:
                    print(ex)
        try:
            for item in auto_glossary.choise_data:
                try:
                    data["auto"][item] = auto_glossary.choise_data[item][str(
                        data["auto"][item]).lower()]
                except (KeyError, TypeError):
                    pass
        except KeyError:
            pass
        mark_auto = None
        try:
            if data["auto"]["mark_auto"]:
                mark_auto = data["auto"]["mark_auto"]
                data["auto"]["mark_auto"] = {}
            else:
                data["auto"]["mark_auto"] = None
        except (KeyError, TypeError) as ex:
            pass
        if mark_auto:
            try:
                data["auto"]["mark_auto"].update({"id": mark_auto["id"]})
            except (KeyError, TypeError) as ex:
                data["auto"]["mark_auto"].update({"id": mark_auto})
                logger.error(ex)
            try:
                data["auto"]["mark_auto"].update({"id": mark_auto["id"]})
            except (KeyError, TypeError) as ex:
                logger.error(ex)
        model_auto = None
        try:
            if data["auto"]["model_auto"]:
                model_auto = data["auto"]["model_auto"]
                data["auto"]["model_auto"] = {}
            else:
                data["auto"]["model_auto"] = None
        except (KeyError, TypeError) as ex:
            pass
        if model_auto:
            try:
                data["auto"]["model_auto"].update({"id": model_auto["id"]})
            except (KeyError, TypeError) as ex:
                data["auto"]["model_auto"].update({"id": model_auto})
                logger.error(ex)
            try:
                data["auto"]["model_auto"].update(
                    {"mark_auto": mark_auto["id"]})
            except (KeyError, TypeError) as ex:
                data["auto"]["model_auto"].update(
                    {"mark_auto": mark_auto})
                logger.error(ex)
        generation = None
        try:
            if data["auto"]["generation"]:
                generation = data["auto"]["generation"]
                data["auto"]["generation"] = {}
            else:
                data["auto"]["generation"] = None
        except (KeyError, TypeError) as ex:
            pass
        if generation:
            try:
                data["auto"]["generation"].update({"id": generation["id"]})
            except (KeyError, TypeError):
                data["auto"]["generation"].update({"id": generation})
                pass
            try:
                data["auto"]["generation"].update(
                    {"model_auto": model_auto["id"]})
            except (KeyError, TypeError):
                data["auto"]["generation"].update(
                    {"model_auto": model_auto})
                pass
        try:
            logger.error(data["auto"]["engine_capacity"])
            data["auto"]["engine_capacity"] = str(
                data["auto"]["engine_capacity"]).split()[0]
        except (KeyError, TypeError, IndexError, AttributeError):
            pass
        try:
            logger.error(data["executor"].pop("date_joined"))
            logger.error(data["executor"].pop("avatar"))
        except (KeyError, TypeError, IndexError, AttributeError):
            pass
        try:
            data["auto"]["body_type_auto"] = {"id": data["auto"]["body_type_auto"]}
        except KeyError:
            pass
        try:
            data["auto"]["year_auto"] = {"id": data["auto"]["year_auto"]}
        except KeyError:
            pass

        serializer = ReportSerializerCreateUpdateV3(
            instance=saved_report, data=data)
        is_valid = serializer.is_valid()
        errors_report = serializer.errors
        logger.error("errors_report")
        logger.error(errors_report)
        logger.error("errors_report")
        try:
            exclude = ["vin"]
            for item in errors_report["auto"]:
                if item not in exclude:
                    data["auto"].pop(item, None)
            errors_report.pop("auto")
            cusmom_error = True
        except KeyError:
            pass
        try:
            for item in errors_report:
                if type(errors_report[item]) != dict and "A valid integer is required." in errors_report[item]:
                    data[item] = int(''.join(re.findall(r'\d+', data[item])))
            cusmom_error = True
        except KeyError:
            pass
        if cusmom_error:
            serializer = ReportSerializerCreateUpdateV3(
                instance=saved_report, data=data)
            is_valid = serializer.is_valid()
        if not is_valid:
            raise serializers.ValidationError(serializer.errors)
        if saved_report:
            report_saved = serializer.update(saved_report, data)
        else:
            report_saved = serializer.create(data)
            collections_photo = {}
        type_list = []
        if deleted:
            for photo in Photo.objects.filter(report_podbor=report_saved):
                try:
                    image = photo.image.url
                    image_small = photo.image_small.url
                    image_google = photo.image_google
                    collections_photo[report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]].append(
                        {"image": image, "image_small": image_small, "image_google": image_google})
                except (KeyError):
                    collections_photo[report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]] = [
                    ]
                    collections_photo[report_constant.PHOTO_TYPE_RESPONSE[photo.photo_type]].append(
                        {"image": image, "image_small": image_small, "image_google": image_google})
                except (ValueError, FileNotFoundError) as ex:
                    logger.error(ex)
            serializer.validated_data.update({"photos": collections_photo})

        if report_saved.auto and report_saved.auto.pk:
            auto_set_or_create = True
        if is_valid:
            return Response({"success": "Report '{}' updated successfully".format(report_saved.pk, auto_set_or_create),
                             "auto_set_or_create": auto_set_or_create, "report": serializer.validated_data,
                             "id": report_saved.pk, "errors": errors_report, "google_album": report_saved.google_album
                             })
        else:
            return Response(
                {"success": "Report '{}' updated unsuccessfully".format(report_saved.pk, auto_set_or_create),
                 "auto_set_or_create": auto_set_or_create, "report": serializer.validated_data,
                 "id": report_saved.pk, "errors": errors_report, "google_album": report_saved.google_album
                 })


# report preview for not authentication users
class ReportPreviewApi(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'report/report_preview.html'

    def get(self, request):
        if request.is_ajax() and request.GET.get('vin'):
            vin = request.GET.get('vin')
            report = ReportPodbor.objects.filter(auto__vin__icontains=vin).order_by('-created').first()
            reportArray = []
            if report:
                reportArray.append(PreviewReportSerializer( report ).data)
                responseArray = {'reports': reportArray}
                return JsonResponse(responseArray, safe=False)
            else:
                return JsonResponse(data={'errors': 404, 'describe': "Don't find any report"})
        else:
            return Response({'profiles': "queryset"})


def ajax_registration(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    if request.method == 'POST':
        form = AddUserShortForm(request.POST)
        if form.is_valid():
            form.save()
            return JsonResponse(data={'status': 200, 'massage': 'Пользователь был создан'})
        else:
            return JsonResponse(data={'errors': 500, 'errors': dict(form.errors.items())})
    else:
        return JsonResponse(data={'errors': 500, 'errors': 'this is not Post request'})


# Получение отчета в пдф
def get_pdf(request):
    report = ReportPodbor.objects.all()
    report = report.filter(id=29048).get()
    html_pdf = get_template('report/pdf_report.html')

    return HttpResponse(html_pdf.render({"report": report}))
    return HttpResponse('<html></html>')


def search_pay_reports(request):
    context = RequestContext(request)
    query = request.GET.get('q', '')
    if query != '':
        auto = Auto.objects.filter(vin__iexact=request.GET['q']).first()
        if auto:
            reports = ReportPodbor.objects.filter(auto=auto.id)
            if reports:
                html_repots = get_template('report/search_pay_reports.html')
                return render(request, 'report/search_pay_reports.html', {'reports': reports, 'auto': auto})
    return render(request, 'report/search_pay_fail.html', {})


# Получение урла на платежку
@recaptcha_check
def get_pay_report_url(request):
    report_id = request.POST.get('report-id', 0)
    user_email = request.POST.get('mail', '')
    user_phone = request.POST.get('phone', '')
    user_name = request.POST.get('name', '')
    print(report_id)
    print(user_email)
    print(user_phone)
    print(user_name)
    if report_id != 0 and user_email != '':
        report = ReportPodbor.objects.get(id=report_id)
        order_report = OrderReport.objects.create(
            report=report, email=user_email, name=user_name, phone=user_phone,
            price=int(settings.ECOMMPAY_REPORT_PRICE / 100))
        order_report.save()
        payment = Payment(settings.ECOMMPAY_PROJECT_ID,
                          str(order_report.id))
        payment.payment_amount = settings.ECOMMPAY_REPORT_PRICE
        payment.payment_currency = 'RUB'
        payment.payment_description = 'Покупка отчета по id: ' + str(report_id)
        gate = Gate(settings.ECOMMPAY_SECRET)
        payment_url = gate.get_purchase_payment_page_url(payment)
        return JsonResponse({'error': 0, 'message': 'ok', 'url': payment_url}, status=200)
    return JsonResponse({'error': 1, 'message': 'null fields'}, status=400)


def report_to_email(request):
    report_id = request.POST.get('report-id', 0)
    user_email = request.POST.get('mail', '')
    print(report_id)
    print(user_email)
    if report_id != 0 and user_email != '':
        report = ReportPodbor.objects.get(id=report_id)
        if report:
            pdf = report.render_pdf()
            message = EmailMessage(
                'Отчет по автомобилю',
                'Отчет прикреплен к письму.',
                'info@ap4u.ru',
                [user_email],
            )
            message.attach('report.pdf', pdf, 'application/pdf')
            message.send()
    return JsonResponse({'error': 0, 'message': 'ok'}, status=200)


def get_report_request(request):
    name = request.POST.get('name', '')
    phone = request.POST.get('phone', '')
    if name != '' and phone != '':
        message = EmailMessage(
            'Заявка с виджета на подбор',
            'Имя = ' + name + 'Телефон = ' + phone,
            'info@ap4u.ru',
            ['avtopodbor2015@yandex.ru'],
        )
        message.send()
    return JsonResponse({'error': 0, 'message': 'ok'}, status=200)


@csrf_exempt
@basic_auth_report_api
def create_reports_by_mobile(request):
    if request.method == 'POST':
        data = request.FILES['data'].read().decode('utf-8')
        data_json = json.loads(data)

        prepare_data = EportMobileReportApi().prepare_data(data_json)
        if prepare_data:
            report = EportMobileReportApi().create_report(data_json)
            if report:
                EportMobileReportApi().add_order(report)
                return JsonResponse({'status': 'success', 'report_id': report.id}, status=status.HTTP_201_CREATED)
            else:
                return JsonResponse(data={'errors': 400, 'describe': "json not valid"})
        else:
            return JsonResponse(data={'errors': 400, 'describe': "json not valid"})
    return HttpResponse('Method not allowed!', status=status.HTTP_400_BAD_REQUEST)


@csrf_exempt
@basic_auth_report_api
def delete_report_by_mobile(request):
    if request.method == 'DELETE':
        try:
            report = ReportPodbor.objects.get(id=int(request.GET.get('report_id')))
        except (TypeError, KeyError, ReportPodbor.MultipleObjectsReturned, ReportPodbor.DoesNotExist):
            return JsonResponse(data={'errors': 404, 'describe': "Don't find any report"}, status=404)
        if report is not None:
            report.delete()
        else:
            return JsonResponse(data={'errors': 404, 'describe': "Don't find any report to delete"}, status=404)
        return JsonResponse(data={'status': 200, 'describe': "report deleted"}, safe=False, status=200)


@csrf_exempt
@basic_auth_report_api
def create_photos_by_mobile(request):
    if request.method == 'POST':
        data = request.POST
        report = get_object_or_404(ReportPodbor, pk=int(data['report_id']))
        photo = Photo.objects.create(photo_type=report_constant.type_photo[data['photo_type']],
                                     report_podbor=report)
        photo.image.save(request.FILES['image'].name, request.FILES['image'])
        photo.save()
        photo.image_small.generate()
        return JsonResponse([photo.image.url, data], safe=False)
    return HttpResponse('Method not allowed!', status=400)


# reports by mobile app
def report_detail_mobile_app(request):
    if request.method == "GET":
        if request.GET.get('report_id'):
            report_id = request.GET.get('report_id')
            try:
                report = ReportPodbor.objects.get(id=report_id)
            except (TypeError, KeyError, ReportPodbor.MultipleObjectsReturned, ReportPodbor.DoesNotExist):
                return JsonResponse(data={'errors': 404, 'describe': "Don't find any reports"}, status=404)

            if report:
                return render(request, 'report/report_by_mobile_app.html', {'report': report})
            else:
                return JsonResponse(data={'errors': 404, 'describe': "Don't find any reports"}, status=404)
        else:
            return Response({'error': "Don't find report id"}, status=404)
    else:
        return JsonResponse(data={'errors': 'method not allowed'}, status=404)


def upload_reports_to_auto_library(request):
    if request.method == "GET":
        upload_file = upload_daily_reports()
        if upload_file:
            return JsonResponse({'status': 'success', 'file_name': upload_file}, status=status.HTTP_201_CREATED)
        else:
            print('tyt')
            return JsonResponse({'status': 'error'}, status=status.HTTP_400_BAD_REQUEST)
    else:
        return JsonResponse(data={'errors': 'method not allowed'}, status=404)
