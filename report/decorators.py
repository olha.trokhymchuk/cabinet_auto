import base64

from django.conf import settings
from django.http import JsonResponse, HttpResponseBadRequest, HttpResponse


def basic_auth_report_api(function_to_decorate):
    def wrapper(request, *args, **kwargs):
        auth_header = request.META.get('HTTP_AUTHORIZATION', '')
        token_type, _, credentials = auth_header.partition(' ')
        encode = base64.b64decode(credentials).decode()

        username, password = encode.split(':')

        if settings.REPORT_API_USERNAME == username and settings.REPORT_API_PASSWORD == password:
            return function_to_decorate(request, *args, **kwargs)
        return HttpResponseBadRequest('Authorization failed')
    return wrapper