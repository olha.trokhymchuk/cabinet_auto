import django_filters
from core.filters import ListFilter
from .models import ReportPodbor


class ReportFilter(django_filters.FilterSet):
    created__gte = django_filters.DateTimeFilter(method='created_gte')
    created__lte = django_filters.DateTimeFilter(method='created_lte')
    order__client__filials = django_filters.NumberFilter()
    status = django_filters.NumberFilter()
    executor = django_filters.NumberFilter()
    auto__body_type_auto = ListFilter()
    auto__color_auto = ListFilter()
    auto__engine_type = ListFilter()
    auto__transmission_type = ListFilter()
    auto__drive_type = ListFilter()
    auto__salon_auto = ListFilter()
    auto__color_salon = ListFilter()
    auto__year__gte = django_filters.NumberFilter(name='auto__year_auto__year', lookup_expr='gte')
    auto__year__lte = django_filters.NumberFilter(name='auto__year_auto__year', lookup_expr='lte')
    auto__mileage__lt = django_filters.NumberFilter(name='auto__mileage', lookup_expr='lt')
    auto__cost__lte = django_filters.NumberFilter(name='auto__cost', lookup_expr='lt')
    auto__engine_capacity__gte = django_filters.NumberFilter(name='auto__engine_capacity', lookup_expr='gte')
    auto__engine_capacity__lte = django_filters.NumberFilter(name='auto__engine_capacity', lookup_expr='lte')
    auto__horsepower__gte = django_filters.NumberFilter(name='auto__horsepower', lookup_expr='gte')
    auto__horsepower__lte = django_filters.NumberFilter(name='auto__horsepower', lookup_expr='lte')
    auto__owners__lte = django_filters.NumberFilter(name='auto__owners', lookup_expr='lte')
    recommended = django_filters.CharFilter(method='boolean_filter')
    ready_to_upload = django_filters.NumberFilter()
    success_upload_status = django_filters.NumberFilter()
    not_ready_to_upload = django_filters.NumberFilter()

    class Meta:
        model = ReportPodbor
        fields = (
            'auto',
            'auto__mark_auto',
            'auto__model_auto',
            'auto__generation',
            'auto__equipment',
        )

    def boolean_filter(self, queryset, name, value):
        boolean_value = True if value in (True, 'True', 'true', '1') else False
        return queryset.filter(**{
            name: boolean_value,
        })

    def created_gte(self, queryset, name, value):
        queryset = queryset.filter(created__gte=value)
        return queryset

    def created_lte(self, queryset, name, value):
        queryset = queryset.filter(created__lte=value)
        return queryset
