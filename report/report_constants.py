PHOTOINSPECTION=0
PHOTOCHANGE=1
PHOTOGIBDD=2
PHOTOFSSP=3
PHOTOREESTR=4
PHOTOAVTOKOD=5
PHOTODOPSERVER=6
PHOTOVIN1=7
PHOTOVIN2=8
PHOTOVIN3=9
PHOTOVINTABLE=10
PHOTOVINGLASS=11
PHOTOAUTONUMBER=12
PHOTOVIEWS=13
PHOTOFRONTVIEWS=14
AUTOPHOTO='1_20'
AUTOPHOTOMNEM='1_20'
PHOTOINSPECTIONMNEM='2_1'

type_photo = {
    'photo_views':PHOTOVIEWS,
    'photo_inspection':PHOTOINSPECTION,
    'photo_change':PHOTOCHANGE,
    "photo_gibdd":PHOTOGIBDD,
    "photo_fssp":PHOTOFSSP,
    'photo_reestr_zalog':PHOTOREESTR,
    'photo_avtokod':PHOTOAVTOKOD,
    'photo_dopserv':PHOTODOPSERVER,
    'photo_vin1':PHOTOVIN1,
    'photo_vin2':PHOTOVIN2,
    'photo_vin3':PHOTOVIN3,
    'photo_vin_table':PHOTOVINTABLE,
    'photo_vin_glass':PHOTOVINGLASS,
    'photo_auto_number':PHOTOAUTONUMBER,
    'photo_front_views':PHOTOFRONTVIEWS
}


PHOTO_TYPE = (
    (PHOTOINSPECTION, 'С места осмотра'),
    (PHOTOCHANGE, 'Cо сдачи'),
    (PHOTOGIBDD, 'ГИБДД'),
    (PHOTOFSSP, 'ФССП'),
    (PHOTOREESTR, 'C реестра залогов'),
    (PHOTOAVTOKOD, 'Автокод'),
    (PHOTODOPSERVER, 'Доп. сервисы'),
    (PHOTOVIN1, 'VIN код на кузове/раме №1'),
    (PHOTOVIN2, 'VIN код на кузове/раме №2'),
    (PHOTOVIN3, 'VIN код на кузове/раме №3'),
    (PHOTOVINTABLE, 'VIN код на табличке'),
    (PHOTOVINGLASS, 'VIN код под лобовым стеклом'),
    (PHOTOAUTONUMBER, 'Номер двигателя'),
    (PHOTOVIEWS, 'Фото диаг. листа/акта осмотра'),
    (PHOTOFRONTVIEWS, 'Внешний вид авто')
)

PHOTO_TYPE_RESPONSE = {
    PHOTOVIEWS: 'photo_views',
    PHOTOINSPECTION: 'photo_inspection',
    PHOTOCHANGE: 'photo_change',
    PHOTOGIBDD: 'photo_gibdd',
    PHOTOFSSP: 'photo_fssp',
    PHOTOREESTR: 'photo_reestr_zalog',
    PHOTOAVTOKOD: 'photo_avtokod',
    PHOTODOPSERVER: 'photo_dopserv',
    PHOTOVIN1: 'photo_vin1',
    PHOTOVIN2: 'photo_vin2',
    PHOTOVIN3: 'photo_vin3',
    PHOTOVINTABLE: 'photo_vin_table',
    PHOTOVINGLASS: 'photo_vin_glass',   
    PHOTOAUTONUMBER: 'photo_auto_number',
    PHOTOFRONTVIEWS: 'photo_front_views'
}
