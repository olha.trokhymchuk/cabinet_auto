from autopodbor.celery import app
from report.workers.export_mobile import ExportMobile
from .workers.load_photos_to_google import loadPhotosToGoogle


@app.task
def get_report_from_mobile():
    name = "export from mobie app by ftp"
    remote_dir = "order"
    ExportMobile(name, remote_dir=remote_dir)


@app.task
def load_photos_to_google():
    loadPhotosToGoogle()
