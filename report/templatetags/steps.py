from django import template

register = template.Library()


@register.filter
def group_is_empty(group):
    for field in group.get('fields', []):
        if field.get('value') not in ['', None, []]:
            return False
    return True


@register.filter
def step_is_empty(step, report):
    if report:
        if step.get('name') == 'Общая информация':
            return False
        elif (step.get('name') == 'Заключение эксперта' and report.pluses != '' and report.minuses != '' and
                      report.recommended is not None and report.category is not None and report.result_expert != ''):
            return False

    for group in step.get('groups', []):
        if not group.get('fields'):
            return True
        for field in group.get('fields'):
            # if field.get('value'):
            #     return False
            if field:
                return False
    return True
