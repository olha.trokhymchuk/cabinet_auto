from django.contrib import admin
from safedelete.admin import SafeDeleteAdmin, highlight_deleted

from .models import ReportPodbor, Steps, Photo, SurrenderPhoto, ReportAutoLibrary


class ReportPodborAdmin(SafeDeleteAdmin):
    list_display = (highlight_deleted, "id") + SafeDeleteAdmin.list_display
    list_filter = SafeDeleteAdmin.list_filter
    #exclude = ('notifications',)
    exclude = ('notifications','data','auto','executor',)

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        pk = request.resolver_match.args[0]
        report = ReportPodbor.objects.get(pk=pk)
        if db_field.name == "order":
            order = report.order.all()
            kwargs["queryset"] = order
        return super(ReportPodborAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


class PhotoAdmin(SafeDeleteAdmin):
    list_display = ('id', 'report_podbor', )
    list_filter = ('report_podbor', )
    

class SurrenderPhotoAdmin(SafeDeleteAdmin):
    list_display = ('id', 'surrender_podbor', )
    list_filter = ('surrender_podbor', )


admin.site.register(Steps)

admin.site.register(Photo, PhotoAdmin)
admin.site.register(SurrenderPhoto, SurrenderPhotoAdmin	)
admin.site.register(ReportPodbor, ReportPodborAdmin)
admin.site.register(ReportAutoLibrary)
