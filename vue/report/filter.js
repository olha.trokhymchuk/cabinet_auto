import Vue from 'vue';
import Datepicker from 'vuejs-datepicker';
import select2 from '../vue/select2-directive';
import filterMixin from '../mixins/filter';
import range from '../mixins/range';
import {ru} from 'vuejs-datepicker/dist/locale'

export default Vue.component('filter-component', {
  delimiters: ['${', '}'],
  mixins: [filterMixin, range],
  data() {
    return {
      initialization: true,
      order__client__filials: '',
      status: '',
      created__gte: '',
      created__lte: '',
      executor: '',
      marks: [],
      models: [],
      generations: [],
      auto__mark_auto: '',
      auto__model_auto: '',
      auto__generation: '',
      auto__cost__lte: '',
      auto__body_type_auto: [],
      auto__transmission_type: [],
      auto__color_auto: [],
      auto__engine_type: [],
      auto__drive_type: [],
      auto__salon_auto: [],
      auto__color_salon: [],
      auto__mileage__lt: '',
      auto__year__gte: '',
      auto__year__lte: '',
      auto__capacity__gte: '',
      auto__capacity__lte: '',
      auto__engine_capacity__gte: '',
      auto__engine_capacity__lte: '',
      auto__horsepower__gte: '',
      auto__horsepower__lte: '',
      auto__equipment: '',
      auto__owners__lte: '',
      datepickerLang: ru,
      filter_recommended: false,
      check_report_to_upload: '',
      yearsRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
      capacitiesRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
      horsepowersRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
      filterValues: {},
      filterFields: ['auto__mark_auto', 'auto__model_auto', 'auto__generation', 'auto__body_type_auto',
                     'auto__color_auto', 'auto__engine_type', 'auto__drive_type', 'auto__cost__lte', 'auto__equipment',
                     'auto__salon_auto', 'auto__color_salon', 'auto__mileage__lt', 'auto__year__gte', 'auto__year__lte',
                     'auto__engine_capacity__gte', 'auto__engine_capacity__lte', 'auto__horsepower__gte', 'auto__horsepower__lte',
                     'auto__transmission_type', 'auto__owners__lte', 'recommended', 'order__client__filials',
                     'status', 'created__gte', 'created__lte', 'executor', 'check_report_to_upload',
                     ],
      rangeFields: ['yearsRange', 'capacitiesRange', 'horsepowersRange'],
      startDatepickerState: {
        disabled: {
          to:  null,
          from: null
        }
      },
      endDatepickerState: {
        disabled: {
          to:  null,
          from: null
        }
      },
      loading: false,
    }
  },
  components: {
    Datepicker,
  },
  computed: {
    recommended: {
      get: function() {
        return this.filter_recommended ? 1 : '';
      },
      set: function(value) {
        if (value == 1)
          this.filter_recommended = 1;
      }
    }
  },
  watch: {
    auto__year__gte: function(year) {
      this.yearsRange.select0 = parseInt(year);
    },
    auto__year__lte: function(year) {
      this.yearsRange.select1 = parseInt(year);
    },
    auto__engine_capacity__gte: function(capacity) {
      this.capacitiesRange.select0 = parseFloat(capacity);
    },
    auto__engine_capacity__lte: function(capacity) {
      this.capacitiesRange.select1 = parseFloat(capacity);
    },
    auto__horsepower__gte: function(horsepower) {
      this.horsepowersRange.select0 = parseInt(horsepower);
    },
    auto__horsepower__lte: function(horsepower) {
      this.horsepowersRange.select1 = parseInt(horsepower);
    },
    auto__mark_auto: function(newMark) {
      if (!this.initialization) {
        this.auto__model_auto = '';
        this.auto__generation = '';
      }
    },
    auto__model_auto: function(newModel) {
      if (!this.initialization) {
        this.auto__generation = '';
      }
    }
  },
  mounted: function() {
    this.loadInitial();
  },
  directives: {
    select2: select2
  },
  methods: {
    onChangeDatepickerStart(newDate) {
      this.created__gte = `${newDate.getFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`;
      this.endDatepickerState.disabled.to = newDate;
    },
    onChangeDatepickerEnd(newDate) {
      this.created__lte = `${newDate.getFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`;
      this.startDatepickerState.disabled.from = newDate;
    },
    loadInitial() {
      var self = this;
      self.loading = true;
      $('body').css('overflow','hidden');
      $.ajax({
        url: '/report/filter/' + this.uri,
        cache: false,
        method: 'GET',
        success: function (data) {
          self.filterValues = data;
          self.loading = false;
          $('body').css('overflow','visible');
          self.yearsRange.init = data.years;
          self.yearsRange.select0 = self.auto__year__gte;
          self.yearsRange.options0 = data.years;
          self.yearsRange.select1 = self.auto__year__lte;
          self.yearsRange.options1 = data.years;

          self.capacitiesRange.init = data.capacities;
          self.capacitiesRange.select0 = parseFloat(self.auto__engine_capacity__gte);
          self.capacitiesRange.options0 = data.capacities;
          self.capacitiesRange.select1 = parseFloat(self.auto__engine_capacity__lte);
          self.capacitiesRange.options1 = data.capacities;

          self.horsepowersRange.init = data.horsepowers;
          self.horsepowersRange.select0 = parseInt(self.auto__horsepower__gte);
          self.horsepowersRange.options0 = data.horsepowers;
          self.horsepowersRange.select1 = parseInt(self.auto__horsepower__lte);
          self.horsepowersRange.options1 = data.horsepowers;
          self.initialization = false;
        },
        error: function (error) {
          self.loading = false;
          $('body').css('overflow','visible');
          console.log(error);
        }
      });
    }
  }
})
