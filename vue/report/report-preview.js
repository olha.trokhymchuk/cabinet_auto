import Vue from 'vue';
import axios from 'axios';


var reports = new Vue({
    delimiters: ['${', '}'],
    el: '#report-preview',
    data: {
        querySearch: '',
        csrf: csrf,
        auto: null,
        pageNotFound: false,
        enterFormData: {
            phoneNumber: '',
            password: '',
        },
        clearPhone:'',
        RegistrationFormData: {
            phoneNumber: '',
            password: '',
            passwordRepeat: '',
        },
        errorRequestMessage: null,
    },
    created() {
        this.loadPage();
        $('.pop-up__order-error').hide();
    },
    methods: {
        searchRequest(e) {
            const options = {
                headers: {'Content-Type': 'application/json'}
            };
            e.preventDefault();
            let self = this;
            $('#loader-wrapper').show();
            $.ajax({
                type: 'GET',
                url: 'http://localhost:8000/report/preview/',
                data: "vin=" + this.querySearch,
                // data: "vin="+this.querySearch,
                success: function (data) {
                    $('#loader-wrapper').hide();
                    if (data.errors) {
                        self.pageNotFound = true;
                    } else {
                        self.auto = data.reports[0];
                        self.pageNotFound = false;
                        self.catchNormalDate();
                    }
                },
            });
        },
        enterForm() {
            if (this.enterFormData.phoneNumber != '' && this.enterFormData.password != '') {
                let auth = {
                    tel: this.enterFormData.phoneNumber,
                    password: this.enterFormData.password,
                }
                axios.post('../../report/preview/signup', auth).then(response => {
                    console.log(response);
                });
            } else {
                if (this.enterFormData.phoneNumber == '') {
                    $('.popup-input__enter-tel').addClass('error');
                } else {
                    $('.popup-input__enter-tel').removeClass('error');
                }
                if (this.enterFormData.password == '') {
                    $('.popup-input__enter-pass').addClass('error');
                } else {
                    $('.popup-input__enter-pass').removeClass('error');
                }
            }
        },
        registartionForm() {
            this.errorRequestMessage = null;
            let phoneNumb = this.RegistrationFormData.phoneNumber.replace(/[^.\d]+/g,"").replace( /^([^\.]*\.)|\./g, '$1' );
            if (this.RegistrationFormData.phoneNumber != '' && this.RegistrationFormData.password != '' && this.RegistrationFormData.passwordRepeat != '') {

                let auth = new FormData();
                auth.append('phone', phoneNumb);
                auth.append('password1', this.RegistrationFormData.password);
                auth.append('password2', this.RegistrationFormData.passwordRepeat);
                auth.append('csrfmiddlewaretoken', this.csrf);
                axios.post('../../report/preview/registration/', auth )
                    .then(response => {
                        if(response.data.errors){
                            this.errorRequestMessage = response.data.errors;
                        }
                        else if(response.data.massage){
                            this.enterFormData.phoneNumber = this.RegistrationFormData.phoneNumber;
                            this.enterFormData.password = this.RegistrationFormData.password;
                            setTimeout(()=>{
                                $('.login').submit();
                            },1000);
                        }
                    });
            } else {
                if (this.RegistrationFormData.phoneNumber == '') {
                    $('.popup-input__reg-tel').addClass('error');
                } else {
                    $('.popup-input__reg-tel').removeClass('error');
                }
                if (this.RegistrationFormData.password == '') {
                    $('.popup-input__reg-pass').addClass('error');
                } else {
                    $('.popup-input__reg-pass').removeClass('error');
                }
                if (this.RegistrationFormData.passwordRepeat == '') {
                    $('.popup-input__reg-pass2').addClass('error');
                } else {
                    $('.popup-input__reg-pass2').removeClass('error');
                }
            }
        },
        catchNormalDate() {
            if (this.auto.created) {
                let created = new Date(this.auto.created);
                this.auto.created = created.getDate() + '.' + created.getMonth() + '.' + created.getFullYear();
            }
            if (this.auto.modified) {
                let modified = new Date(this.auto.modified);
                this.auto.modified = modified.getDate() + '.' + modified.getMonth() + '.' + modified.getFullYear();
            }
        },
        loadPage() {
            $('.report-preview').removeClass('deactive');
        },
    }
});