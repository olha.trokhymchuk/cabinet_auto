import Vue from 'vue';
import { bus } from './report-bus';
import VModal from 'vue-js-modal';
Vue.use(VModal);

import VueRouter from 'vue-router'
Vue.use(VueRouter)

import axios from 'axios';

import carEditFormNew from '../components/auto-edit-form';
import searchCar from '../auto/search-car-component';
import gallery from '../components/gallery';
import func from '../mixins/funcs';

const dictionary = {
  ru: {
    messages: {
      unique_source: function () {
        return "Такой автомобиль уже существует"
      },
      unique_url: function () {
        return "Такой автомобиль уже существует"
      },
      unique_vin: function () {
        return "Такой автомобиль уже существует"
      }
    }
  }
}
import ru from "vee-validate/dist/locale/ru";
VeeValidate.Validator.addLocale(ru);
VeeValidate.Validator.updateDictionary(dictionary);
Vue.use(VeeValidate, {locale: 'ru'});

import funcs from '../mixins/funcs';
import photos_mix from '../mixins/photos_mix';
import step_mix from '../mixins/step_mix';

import { step_button, select2, slider, boolean_field, 
      submit_step, control_auto_element,
      comment_step, shema_edit_popup, 
      scheme_edit_marker,
      info_field_base,
      modal_alert } from '../components/report_add';

var imgCount = 0;
var alertFlag = true;
var report = new Vue({
  delimiters: ['${', '}'],
  el: '#page  ',
  mixins: [funcs, photos_mix, step_mix],
  components: {
    carEditFormNew,
    searchCar,
    gallery,
    step_button,
    select2,
    slider,
    boolean_field,
    submit_step,
    control_auto_element,
    comment_step,
    scheme_edit_marker,
    shema_edit_popup,
    info_field_base,
    modal_alert
  },
  data: function(){
    return{
      csrf: csrf,
      reportTypes: reportTypes,
      numbersOfHosts: numbersOfHosts,
      carEditVisible: store.carEditVisible,
      changeAutoVisible: false,
      stepsUrl: '/report/get-report-steps/',
      store: store,
      baseSteps: [{name: 'Привязка автомобиля'}],
      initialSteps: [],
      currentStep: "auto",
      photo_permition: false,
      auto_set_or_create:false,
      clear_files: false,
      typePhoto: {},
      deletedPhotos: [],
      report: {},
      result: {},
      scheme: false,
      isReportChanged: false,
      isPhotoDelete: false,
      add_hide: true,

      step_chanched: false,
      negativeData: {},
      fieldss:{
        auto:null
      },
      photo_views:[],
      photo_inspection:[],
      photo_front_views: [],
      photo_change:[],
      photo_gibdd:[],
      photo_fssp:[],
      photo_reestr_zalog:[],
      photo_avtokod:[],
      photo_dopserv:[],
      photo_vin1:[],
      photo_vin2:[],
      photo_vin3:[],
      photo_vin_table:[],
      photo_vin_glass:[],
      photo_auto_number:[],
      photos_count_upload: 0,
      photos_count_uploaded: 0,
      put_url_report:'',
      count_steps: 0,
      alert_photo_upload: false,
      photo_packages: {},
      errorsInfo: {
        auto:{}
      },
      alert_report_error: false,
      errors_text: "",
      upload_disabled:"",
      uploaded_message: "Загрузить изображения",
      max_uploade_photo: 200,
      all_Photos: 0,
      all_Photos_after: 0,
      report_token:[],
      all_Load_Photos: 0
    }
  },
  watch: {
    'store.carEditVisible'(newValue) {
      this.carEditVisible = newValue;
    },
    'photos_count_uploaded'(newValue) {
          document.getElementById("curent_load").innerHTML=this.photos_count_uploaded;
          document.getElementById("all_count").innerHTML=this.photos_count_upload;
          if(newValue == this.photos_count_upload){
            this.typePhoto = [];
            this.property = false;
          }
    },
    'photos_count_upload'(newValue) {
        if(newValue > this.max_uploade_photo) {
          this.upload_disabled = "disabled";
          this.uploaded_message = "количество превышено 200";
        } else {
          this.uploaded_message = "Загрузить изображения";
          this.upload_disabled = "";
        }
    },  
    'scheme'(scheme) {
      if (scheme) {
        $('.report-field').hide();
      } else {
        $('.report-field').show();
      }

      $('.report-field').attr('style', '');
    },
  },
  ready() {

    let topBarHeight = $('.top-bar').height();
    $('.report-edit__progress').stick_in_parent({
      parent: '.report-edit__content',
      offset_top: topBarHeight
    })
    .on('sticky_kit:stick', function () {
        $('.report-edit__step').appendTo('.top-bar');
    })
    .on('sticky_kit:unstick', function () {
        $('.report-edit__step').insertAfter('.report-edit__header h2');
    });

    $(document).on('mq_tablet', function () {
      $('.report-nav').trigger('sticky_kit:detach').appendTo('.right-bar__content');
    });

    $(document).on('mq_tablet_', function () {
      $('.report-nav').appendTo('.report-edit__nav').stick_in_parent({offset_top: 40});
    });
  },
  mounted: function() {
    if (report_id){
      this.report_id = report_id;
    }
    this.loadInitial();
    this.remove_empty();
    this.count_steps = Object.keys(this.steps).length;
    // window.beforeunload = window.onunload = window.onbeforeunload = this.confirmExit;
  },
  created: function() {
    bus.$on('event_auto_data', (field, value, name)  => {
      this.setAutoField(field, value, name);
    });

    bus.$on('event_select_auto', (car)  => {
      Vue.set(this.fieldss, "auto", car);
      this.step_chanched = true;
      for(let item in car){
        this.addDefindeCount(item);
      }
      this.steps["base"]["totalFields"].forEach(item => {
         this.addDefindeCount(item, "base");
      });
    });
    bus.$on('event_negative_data', (data_id, label, del)  => {
      try{
        if(del) {
          delete this.negativeData[data_id];
        } else {
          this.negativeData[data_id]=label;
        }
      }catch(e){
        
      }
    });
  },
  methods: {
    // confirmExit(e){
    //   if(this.photos_count_uploaded < this.photos_count_upload) {
    //     this.alert_photo_upload = true;
    //     return "Имеются не загруженные фото, покинуть страницу?";
    //   }
    // },
    loadInitial(callback) {
      if (report_id) {
        this.updateReport(report_id);
      }
      this.toggleStep(this.currentStep);

    },
    updateReport(report_id) {
      document.getElementById("loader-wrapper").style.display = "block";
      axios.get('/report/view-api3/' + report_id).then((response) => {
        let auto = this.fieldss.auto;

        this.fieldss = response.data;
        for (let item in auto) {
          try {
            Vue.set(this.fieldss.auto, item, auto[item])
          } catch (e) { 
            Vue.set(this.fieldss, "auto", {});
            Vue.set(this.fieldss.auto, item,auto[item])
          }
        }
        if(this.fieldss.auto) {
          this.auto_set_or_create = true;
        } 
        this.progressCalcForAll();
        for (let type in this.fieldss.photos) {
          this[type] = this.fieldss.photos[type];
          Vue.set(this, type, this.fieldss.photos[type]);
        }
        setTimeout(function () {
          document.getElementById("loader-wrapper").style.display = "none";
        },1000);
      });
    },
    updatePhotos() {
      this.photo_views = [];
      this.photo_inspection = [];
      this.photo_front_views = [],
      this.photo_change = [];
      this.photo_gibdd = [];
      this.photo_fssp = [];
      this.photo_gibdd = [];
      this.photo_reestr_zalog = [];
      this.photo_avtokod = [];
      this.photo_dopserv = [];
      this.photo_vin1 = [];
      this.photo_vin2 = [];
      this.photo_vin3 = [];
      this.photo_vin_table = [];
      this.photo_vin_glass = [];
      this.photo_auto_number = [];
      for (let type in this.fieldss.photos) {
        this[type] = this.fieldss.photos[type];
        Vue.set(this, type, this.fieldss.photos[type]);
      }
    },
    getNameIndex() {
      return this.indexSteps[this.steps[this.currentStep].groupIndex + 1];
    },
    toggleStep(currentStep) {
      this.clear_files = false;
      if(this.fieldss.auto != null || currentStep === 'auto'){
        if (this.step_chanched) {
          this.validateBeforeSubmit(currentStep);
        } else {
          if(this.checkNegativeData()){
            return
          }
          this.currentStep = currentStep?currentStep: this.getNameIndex();
          this.activateTab(this.currentStep);
        }
      }

    },
    activateTab(tab) {
      for (let item in this.steps) {
        try {
          this.steps[item]["show"] = false;
        } catch (e) { console.error(e) }
      }
      try {
        this.steps[tab]["show"] = true;
      } catch (e) { 
        this.currentStep = "auto";
        this.steps[this.currentStep]["show"] = true;
      }
      window.scrollTo(0, 0);
    },
    photoChange() {
      this.step_chanched = true;
      this.isPhotoChanged = true;
    },
    photoDelete(files) {
      for (let file in files) {
        if(this.deletedPhotos.indexOf(files[file].pk) < 0) {
          this.deletedPhotos.push(files[file].pk);
          this.step_chanched = true;
          this.isPhotoDelete = true;

        }
      }
    },
    checkNegativeData(){
      try{
        if(Object.keys(this.negativeData).length > 0) {
          return true;
        }
      }catch(e){}
      return false;
    },
    validateBeforeSubmit(currentStep) {
      let vinInput = document.querySelectorAll('[name="vin"]')[0];
      let vinUpperCaseValue = '';

      if(vinInput !== undefined){
        vinUpperCaseValue = vinInput.value.toUpperCase();
        vinInput.value = vinUpperCaseValue;
      }

      if(this.checkNegativeData()){
        return
      }

      if(this.step_chanched) {
        if(this.isPhotoDelete && this.isReportChanged && this.fieldss.auto && this.isPhotoChanged) {
          this.$validator.validateAll().then((result) => {
            if (result) {
              this.submit(currentStep);        
            }
          })
        } else {
          this.submit(currentStep);          
        }
      } else {
        if(this.fieldss.auto != null || currentStep === 'auto'){
          this.currentStep = this.getNameIndex();
        }
      }
      if(this.fieldss.auto != null || currentStep === 'auto'){
        this.activateTab(this.currentStep);
      }
     
    },
    submit(currentStep) {
      var $this = this;

      switch($this.currentStep){
        case 'base':
        case 'vin':
        case 'jur':
          if($('.loaded').length > 0){
            $('.load-photo-before').fadeIn(0);
            $this.send($this.currentStep);
          }else{
            $this.send(currentStep);
          }
        break;
        default:
          $this.send(currentStep);
        break;  
      }
    },
    send(currentStep){
      var $this = this;
      document.getElementById("loader-wrapper").style.display = "block";
      let formData = new FormData();
      if($this.deletedPhotos.length > 0) {
          formData.append("deleted", JSON.stringify($this.deletedPhotos))
      }
      let headers = {
        "X-CSRFToken": $this.csrf,
        'Content-Type': 'multipart/form-data'
      };
      const onUploadProgress =  progressEvent => {
        let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
        $('#process').text(Math.ceil(percentCompleted * 100) + '%')
      }

      if($this.report_id){
        $this.put_url_report = '/report/view-api3/' + $this.report_id + '/';
      }else{
        $this.put_url_report = '/report/view-api3/';
      }

      if($this.fieldss.auto.vin !== undefined){
        $this.fieldss.auto.vin = $this.transliterate($this.fieldss.auto.vin);
      }
      
      formData.append("data", JSON.stringify($this.fieldss));

      axios.put($this.put_url_report, formData, {withCredentials: true, headers,
                  onUploadProgress: onUploadProgress})
      .then(response => {
        const data = response.data;
        if(response.status == 200 && data.success) {
          $this.clear_files = true;
          $this.google_album = data.google_album;
          $this.deletedPhotos = [];
          $this.auto_set_or_create = data.auto_set_or_create;
          $this.step_chanched = false;
          let photos = data.report.photos?data.report.photos:$this.fieldss.photos;
          $this.fieldss = data.report;
          $this.fieldss.photos = photos;
          $this.isReportChanged = false;
          // console.log(response.errors);
          $this.errorsInfo = response.errors;
          if(!$this.errorsInfo || ($this.errorsInfo && !$this.errorsInfo.auto)) {
            $this.errorsInfo = {};
            this.errorsInfo.auto = {};
          }
            document.getElementById("curent_load").innerHTML=$this.photos_count_uploaded;
            document.getElementById("all_count").innerHTML=$this.photos_count_upload;
        
            if($this.isPhotoChanged || $this.isPhotoDelete) {
              $this.updatePhotos();
            }
            if(!$this.report_id){
              $this.report_id = data.id?data.id:response.id;
              let url = `/report/report-edit/${$this.report_id}`;
              window.history.pushState({path:url},'',url);
            }
          
            if($this.isPhotoChanged) {
              $this.UploadPhotos();
            }else{
              setTimeout(function () {
                document.getElementById("loader-wrapper").style.display = "none";
              },1000);
              $this.currentStep = currentStep?currentStep:$this.getNameIndex();
              $this.activateTab($this.currentStep);
            }
            $this.isPhotoChanged = false;
            $this.isPhotoDelete = false;
            $this.clear_files = false;

        }
      })
      .catch(error => {
          setTimeout(function () {
            document.getElementById("loader-wrapper").style.display = "none";
          },1000)
        $this.errorsInfo = error.response.data;
        // console.log(error.response.data);
        if(!$this.errorsInfo || ($this.errorsInfo && !$this.errorsInfo.auto)) {
            $this.errorsInfo = {};
            $this.errorsInfo.auto = {};
        }
        // console.log($this.errorsInfo);
      });
    },
    cutFiles(files){
      let returnFiles = files.slice(0);
      let frontSectionLoadedImagesLength = $('.photo_front_views .uploadegallery-item').length;
      let frontSectionUploadImagesLength = files.length;
      let accessImageCount = 15;

      if((frontSectionUploadImagesLength + frontSectionLoadedImagesLength) > accessImageCount){
        let counterFrom = frontSectionUploadImagesLength;
        let counterTo = accessImageCount - frontSectionLoadedImagesLength;

        if(alertFlag){
          alert('Вы можете загрузить не больше 15 фотографий!');
          alertFlag = false;
        }

        if(frontSectionLoadedImagesLength == 0){
          counterTo = accessImageCount;
        }
        returnFiles.splice(counterTo, counterFrom);
      }

      return returnFiles;
    },
    photoUpload(files, type_photo) {
      if(type_photo === 'photo_front_views'){
        files = this.cutFiles(files, type_photo);
      } 

      this.typePhoto[type_photo] = [];
      files.forEach(item =>{
        let extension = item.file.name.split('.')[1];
        let newFile = new File([item.file], {type: item.type, type_photo: type_photo});
        try{
          this.typePhoto[type_photo].push(newFile);

        }catch(e){
          this.typePhoto[type_photo] = []; 
          this.typePhoto[type_photo].push(newFile);
        }
        this.isPhotoChanged = true;
        this.step_chanched = true;
        this.addDefindeCount(type_photo);
      });
      this.photos_count_upload = 0;
      this.photos_count_uploaded = 0;
      this.all_Photos = 0;
      this.all_Photos_after = 0;
      for (let type in this.typePhoto) {
        this.photos_count_upload += +this.typePhoto[type].length;
        this.all_Photos += +this.typePhoto[type].length;
      }
    },
    isInfoField(field) {
      return ['select', 'input', 'radio', 'checkbox', 'image', 'text'].includes(field.type);
    },
    isEmpty(field) {
      if(typeof(field.value) !== "boolean"){
        return 'empty';
      }
    },
    isExist : function(array, key, value){
      for(var i=0; i < array.length; i++){
        if( array[i][key] && array[i][key]  == value){
          return true
        }
      }
      return false
    },
    clearFieldStyles() {
      $('.report-field').attr('style', '').hide();
    },
    onUncheckField(evt, data_id, groupIndex, fieldIndex) {

    },
    notEmptyGroup(group) {
      if(group != 'undefined' && group !== undefined && typeof group != 'undefined' && group !== undefined && group.length !==undefined &&
          group.length != 'undefined'){
        for (let i = 0; i < group.fields.length; i++) {
          if (group.fields[i] !== undefined && typeof group.fields[i] != 'undefined') {
            return true;
          }
        }
      }
      return false;
    },
    setField(field, value) {
      const ExcludeAddDefineCount = ["com","okrasheno","carapina","vmyatina",
        "rgavchina","skol","treshina","zapotevanie","zameneno"];
      this.fieldss[field] = value;
      this.isReportChanged = true;
      this.step_chanched = true;
      if(value == null) {
        this.removeDefindeCount(field);
      }else{
        if (field != undefined && !ExcludeAddDefineCount.includes(field.split(/[_ ]+/).pop())){
          this.addDefindeCount(field);
        }
      }
    },
    setProperty(property, value) {
      this[property] = value;
    },
    addAuto(cars) {
      let self = this;
      cars.forEach(function (car) {
        self.fieldss = car;
      });
    },
    setAutoField(field, value, name) {

      if(this.fieldss.auto === null || this.fieldss.auto === undefined){
        Vue.set(this.fieldss, "auto", {});
      }
      Vue.set(this.fieldss.auto, field, value)
      this.step_chanched = true;
      this.addDefindeCount(field);
    },
    setScheme(scheme) {
      this.scheme = scheme;
    },

    progressCalcForAll() {
      for (let step in this.steps) {
        this.getDefinedCount(step);
        this.progressCalc(step);
      }
    },
    removeDefindeCount(val, step){
      step = step?step:this.currentStep;
      if(this.steps[step].definedCount.includes(val)) {
        this.steps[step].definedCount.pop(val);
        this.progressCalc(step);
      }
    },
    addDefindeCount(val, step){
      step = step?step:this.currentStep;
      if(!val.includes("range") && !this.steps[step].definedCount.includes(val)) {
        this.steps[step].definedCount.push(val);
        this.progressCalc(step);
      }
    },
    getDefinedCount(step) {
      this.steps[step].totalFields.forEach(val => {
        try{
          if(this.fieldss && (this.fieldss[val] === false || (this.fieldss[val] !== "" && this.fieldss[val] != null) || this.fieldss.auto[val])) {
            this.steps[step].definedCount.push(val)
          }
        }catch(e){}
        if(this.fieldss.photos && this.fieldss.photos[val]) {
          this.steps[step].definedCount.push(val)
        }
      });
    },
    progressCalc(step) {
      let reportProgressBar = $(`.report-nav__progress .progress__mask:eq(${this.steps[step].groupIndex})`);
      let progress = "translateX(" + 100 / this.steps[step].totalFields.length * this.steps[step].definedCount.length + '%' + ")";
      reportProgressBar.css("transform", progress);
    },
    resizeImages(file, complete) {
        var reader = new FileReader();
          reader.onload = function(e) {
              var img = new Image();
              img.onload = function() {
               complete(resizeInCanvas(img));
              };
              img.src = e.target.result;
            }
          reader.readAsDataURL(file);

      function resizeInCanvas(img){
        var canvas = $("<canvas>")[0];
        var max_size = 700,
        width = img.width,
        height = img.height;
        if (width > height) {
            if (width > max_size) {
                height *= max_size / width;
                width = max_size;
            }
        } else {
            if (height > max_size) {
                width *= max_size / height;
                height = max_size;
            }
        }
        canvas.width = width;
        canvas.height = height;
        var ctx = canvas.getContext("2d");
        ctx.drawImage(img, 0,0,canvas.width, canvas.height);
        return canvas.toDataURL();
      }
      
    },
    UploadPhotos() {
      this.add_hide = false;
      let formData = new FormData();
      let i = 0;
      var $this = this;
      var resizeImg = null;
      var frontViewsImagesMarkup = $('.photo_front_views .loaded');

      for (let type in $this.typePhoto) {
        var value = $this.typePhoto[type];

        if(type === 'photo_front_views' && value.length > 0){
          var splicedMarkup = frontViewsImagesMarkup.splice(value.length, frontViewsImagesMarkup.length);

          splicedMarkup.forEach(function(elem, _){
            $(elem).addClass('disabled')
          })
        }

        var loadImgs = $('.loaded:not(.disabled)').length;

        if (Array.isArray(value)) {
          if(value.length > 0) {
            value.forEach((val) => {
              resizeImg = new Promise(function(resolve, reject){
                $this.resizeImages(val, function( dataUrl ){
                  function dataURLtoFile(dataurl, filename) {
                    var arr = dataurl.split(','),
                        mime = arr[0].match(/:(.*?);/)[1],
                        bstr = atob(arr[1]), 
                        n = bstr.length, 
                        u8arr = new Uint8Array(n);
                    while(n--){
                        u8arr[n] = bstr.charCodeAt(n);
                    }
        
                    return new File([u8arr], filename, {type:mime});
                  }
                  var file = dataURLtoFile(dataUrl);
                  resolve(file)
                })
              });
              resizeImg.then((minImg) => {
                formData = new FormData();
                formData.append('image', minImg);
                formData.append('album_id', $this.google_album);
                formData.append('photo_type', type);
                formData.append('report_id', $this.report_id);
                $this.photo_packages[i] = formData;
                i++;
                if(i == loadImgs){
                  this.sendPhoto(0, i-1);
                } 
              });
            });
          }
        }
      }
    },
    sendPhoto(i, num_packages){
      $('.load-photo-before').hide();
      this.all_Photos_after = 0;
      this.all_Photos_after = this.all_Photos;
      let url = `/report/upload_photo/`;
      var photo_number = '';
      let try_count = 0;
      let headers = {
        "X-CSRFToken": this.csrf,
        'Content-Type': 'multipart/form-data'
      };
      const onUploadProgress =  progressEvent => {
        let percentCompleted = Math.floor((progressEvent.loaded * 100) / progressEvent.total);
        $('#process').text(Math.ceil(percentCompleted * 100) + '%')
      }
      if(!this.fieldss.photos){
        this.fieldss.photos = [];
      }
      let self = this;

      axios.post(url, this.photo_packages[i], {withCredentials: true, headers, onUploadProgress: function(progressEvent) {
        let uploadPercentage = parseInt(Math.round(( progressEvent.loaded / progressEvent.total) * 100));

        let ready = ((i)*100) + uploadPercentage;
        if(ready <= 0){
          ready = 0;
        }
        let col = (num_packages + 1) * 100;
        let onePercent =  col / self.all_Photos;
        self.all_Load_Photos = (ready / onePercent).toFixed(0);

      }})
      .then(response => {
          if(i < num_packages){
            this.sendPhoto(i+1, num_packages);
          }else{
            //this.updateReport(this.report_id);
            this.typePhoto = [];
            $('.load-photo').hide();
            this.currentStep = this.getNameIndex();
            this.activateTab(this.currentStep);
            imgCount = 0;
            setTimeout(function () {
              document.getElementById("loader-wrapper").style.display = "none";
            },1000);
          }
          $('#curent_load').html(this.photos_count_uploaded);
          if (this.photos_count_uploaded >= this.all_Photos_after) {
              this.uploaded_message = "Загрузить изображения";
              this.upload_disabled = "";
          }
      })
      .catch((error) => {
        this.updateReport(this.report_id);
        if(try_count >= 10){
            this.photo_permition = true;
            try_count = 0;
        } else{
          if(error == 'Error: Request failed with status code 500'){
            console.log('Request failed with status code 500');
            $('.load-photo p').html('Ошибка при загрузке фото');
            setTimeout(function(){
                $('.load-photo').css('display','none');
            },3000)
          }
          else if(error.status != 'Pending') {
            try_count += 1;
          }
        }
      })
    },
    getAutoField(auto, name){
      try{
        return auto[name];
      } catch(e){
        return "";
      }
    },
  }
})
