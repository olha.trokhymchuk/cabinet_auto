import Vue from 'vue';
import VModal from 'vue-js-modal';
import list from '../mixins/list';
import funcs from '../mixins/funcs';
import ru from "vee-validate/dist/locale/ru";
import search from '../mixins/search';
import createReport from '../components/create-report';
import filter from './filter';
import myEvent  from '../vue/bus';



Vue.use(VModal);

const dictionary = {
  ru: {
    messages: {
      unique_source: function () {
        return "Такой автомобиль уже существует"
      },
      unique_url: function () {
        return "Такой автомобиль уже существует"
      },
      unique_vin: function () {
        return "Такой автомобиль уже существует"
      }
    }
  }
}

VeeValidate.Validator.addLocale(ru);
VeeValidate.Validator.updateDictionary(dictionary);
Vue.use(VeeValidate);

Vue.component('filter-component', {
  delimiters: ['${', '}'],
  template: '#filter-component',
  mixins: [filter],
  props: [
  ],
})

var reports = new Vue({
  delimiters: ['${', '}'],
  el: '#filter-list',
})

var reports = new Vue({
  delimiters: ['${', '}'],
  el: '#report-list',
  props: ['createReport'],
  mixins: [list, search, funcs],
  data: {
    deleteUrl: '/report/delete',
    clickCount: 0,
  },
  components: {
    createReport
  },
  methods: {
    saveAndAddPreReport() {
      document.dispatchEvent(myEvent);
    },
    uploadReports(e){
      let url = window.location.origin;

      $(e.target).attr('disabled', 'disabled');

      let uploadConfirm = confirm('Выгрузить в автотеку?');
      if(uploadConfirm){
        $.ajax({
          url : url + `/report/upload-reports-to-auto-library/`,
          type: "GET",
          success: function(response) {
            if(response.status === 'success'){
              alert("Файл успешно выгружен в автотеку! \n\n " + "Названия файла: " + response.file_name);
              $(e.target).removeAttr('disabled');
            }
          },
          error: function(response) {
            alert("Нет подходящего файла для выгрузки в автотеку!");
            $(e.target).removeAttr('disabled');
          }
        });
      }
    },
    isOrderIdExist(){
      return this.getQuerystring('order') || false;
    },
    submitToIssue(report_id){
      var order_id = this.getQuerystring('order');
      var self = this;
      var fd = new FormData();   
      fd.append('report_id', report_id);
      fd.append('order_id',order_id);
      $.ajax({
        url : `/user/api/transfertoissue/`,
        data: fd,
        type: "POST",
        headers: {'X-CSRFToken': this.csrf },
        processData: false,
        contentType: false,
        success: function() {
          $('dropdown').removeClass('dropdown_open');
          alert('Заказ передан на выдачу!')
        }
      });
    },
  },
})