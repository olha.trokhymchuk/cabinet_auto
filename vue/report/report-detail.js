import Vue from 'vue';

Vue.config.silent = true;

import markup from '../mixins/markup';
import report_detail from '../mixins/report-detail';
import funcs from '../mixins/funcs';
import DateRange from '../components/date-range';
import {checker, checker_glass, checker2, range, vin, hidden_image, shema, shema_glass,
		shema_glass_twise} from '../components/report_detail';

var reports = new Vue({
	delimiters: ['${', '}'],
	el: '#single-rew',
	mixins: [markup, report_detail, funcs],
	components: {
	    checker,
		'date-range': DateRange,
	    checker_glass,
	    checker2,
	    range,
	    vin,
	    hidden_image,
	    shema,
	    shema_glass,
	    shema_glass_twise
	},
	data: function(){
		return{
			fields: null,
			not_found:false,
			csrf: csrf,
			topImageSlider: null,
			title_part: {
				top_title: '',
				name: '',
				date: '',
				executor_full_name: ''
			},
			gallary: [],
			final: {
				textSummary: '',
				label: true,
				shortText: '',
				fullRew: '',
				categoryCar: '1',
				pluses: [],
				minus: []
			},
			mainInfo: {
				car: [],
				price: [],
				className: ''
			},
			legal: false,
			power: false,
			power_active: false,
			external: false,
			external_active: true,
			engine: false,
			rpt: false,
			diagnostic: false,
			electrick: false,
			salon: false,
			vin: false,
			looking: false,
			dop: false,
			/* this data is not load */
			pageLoading: true,
			popupSwitch: false,
			popupData: {
				name: '',
				value: '',
				list: [],
				description: ''
			},
			popupSwitchPower: false,
			popupDataPower: {
				name: '',
				value: '',
				list: [],
				description: ''
			},
		}
	},
	computed: {
		maxVeiwElementsInSlider: function () {
			if ($(window).width() > 767) {
				return 12;
			} else {
				return this.gallary.length;
			}
		},
		hasLastText: function () {
			if (this.gallary.length > this.maxVeiwElementsInSlider) {
				return this.gallary.length - this.maxVeiwElementsInSlider;
			} else {
				return;
			}
		},
		isLastItemLess: function () {
			return {
				lastItem: this.gallary.length > 12
			}
		},
		getUnicExternal: function () {
			return this.body.external.filter((obj, pos, arr) => {
				return arr.map(mapObj => mapObj['name']).indexOf(obj['name']) === pos;
			});
		}
	},
	created: function () {
		this.init();

		var interval = setInterval(function(){
			if($('.fancybox-button--dw').length == 0){
				$.fancybox.defaults.btnTpl.dw = '<button data-fancybox-dw class="fancybox-button fancybox-button--dw download-button" title="Download">' +
				'<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 479 479" style="enable-background:new 0 0 479 479;" xml:space="preserve">' +
				'<g> <g>' + 
				'<path d="M158.4,196.8c-5.3,5.3-5.3,13.8,0,19.1l71.6,71.6c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4l71.6-71.6c5.3-5.3,5.3-13.8,0-19.1s-13.8-5.3-19.1,0L253,245.3V13.5C253,6,247,0,239.5,0S226,6,226,13.5v231.8l-48.5-48.5C172.2,191.5,163.6,191.5,158.4,196.8z" style="fill: rgb(241, 186, 70);"></path>' + 
				'<path d="M460.2,307.4l-47-170c-1.1-3.9-3.8-7-7.4-8.7l-1.3-0.6c-1.8-0.8-3.7-1.2-5.6-1.2h-84.7c-7.5,0-13.5,6-13.5,13.5s6,13.5,13.5,13.5h75.6l39.7,143.8h-105c-7.5,0-13.5,6-13.5,13.5v12.2c0,17.9-14.5,32.4-32.4,32.4h-82.4c-17.9,0-32.4-14.5-32.4-32.4v-12.2c0-7.5-6-13.5-13.5-13.5H49.4l39.9-144.1h75.6c7.5,0,13.5-6,13.5-13.5s-6-13.5-13.5-13.5H80.1c-1.9,0-3.8,0.4-5.6,1.2l-1.3,0.6c-3.6,1.7-6.4,4.8-7.4,8.7l-47,170c-0.3,1.2-0.5,2.4-0.5,3.6v70.9c0,53.7,43.7,97.4,97.4,97.4h247.6c53.7,0,97.4-43.7,97.4-97.4V311C460.7,309.7,460.6,308.5,460.2,307.4z M433.7,381.6c0,38.8-31.6,70.4-70.4,70.4H115.7c-38.8,0-70.4-31.6-70.4-70.4v-56.9H137c0.7,32.1,27.1,58,59.4,58h82.4c32.3,0,58.7-25.9,59.4-58h95.7v56.9H433.7z" style="fill: rgb(241, 186, 70);"></path>' + 
				'</g> </g>' +
				'</svg>' +
				'</button>';
				$('[data-fancybox="images"]').fancybox({
					buttons : [
					'dw',
					'slideShow',
					'zoom',
					'close'
					]
				});
			}else{
				clearTimeout(interval);
			}
		}, 1000)
	},
	mounted(){
		$(document).on('click touchstart', '.download-button', function (event) {
			try {
				var sliderHolder = $(event.target).closest('.slider-popup');
				var imgSrc = sliderHolder.find('.top-slider .swiper-slide-active img').attr('data-src');
				var a = document.createElement('a');
				a.href = imgSrc;
				a.download = 'image.png';
				a.click();
			} catch (e) { throw e }
		});
		$('[data-fancybox-close]').on('click touchstart', function (e) {
			$.fancybox.close();
		});
	},
	beforeDestroy() {
		$(document).off('click touchstart', '.download-button');
	},
	methods: {
		init: function () {
			this.loadData();
		},
		loadData: function () {
			let iCheck = val => {
				return val ? val : '-'
			};

			//TODO: remove this dendrofecalniy(c) method
			let link = window.location.href;
			let id = link.split('/');
			id = id[id.length - 2];
			$('#loader-wrapper').css('display','block');
			$.get('/report/view-api3/' + id).done((response) => {
				$('#loader-wrapper').css('display','none');
				if(Object.keys(response).length < 1){
					this.fields = null;
					this.not_found = true;
					console.log(this.not_found);
					return;
				}
				this.pageLoading = false;
				this.title_part.name = response.name;
				this.title_part.top_title = this.generateReportTitle(response.auto);
				if(response.executor){
					this.title_part.executor_full_name = response.executor.full_name;
				}
				this.title_part.date = response.created; 
				this.fields = response;
				this.gallary = [];
				try {
					for (let type in response.photos) {
						if(type !== 'photo_front_views'){
							for (let item of response.photos[type]) {
								this.gallary.push({
									full: item.image || item.embed_url,
									thumbnail: item.image || item.embed_url,
									alt: '',
									googlePhoto: item.from_google,
								});
							}
						}
						else{
							let reversePhotosArray = response.photos[type].reverse();

							for (let item of reversePhotosArray) {
								this.gallary.unshift({
									full: item.image || item.embed_url,
									thumbnail: item.image || item.embed_url,
									alt: '',
									googlePhoto: item.from_google,
								});
							}
						}

					}
				} catch (err) {
					console.log(err);
				}
				try {
					if (response.auto.length) {
						for (let item of response.auto.photos_auto) {
							this.gallary.push({
								full: item.image || item.embed_url,
								thumbnail: item.image || item.embed_url,
								alt: ''
							});
						}
					}
				} catch (err) {
					console.log(err);
				}

				this.final.textSummary = response.result_expert;
				this.final.label = response.recommended;

				this.final.shortText = response.result_expert.substring(0, 200) + '...';
				this.final.fullRew = response.result_expert;
				this.final.categoryCar = response.category;
				try {
					for (let item of response.pluses.split("\n")) {
						this.final.pluses.push(item)
					}
				} catch (err) {
					console.log(err);
				}
				try {
					for (let item of response.minuses.split("\n")) {
						this.final.minus.push(item)
					}
				} catch (err) {
					console.log(err);
				}
				this.mainInfo = {
					vinƒPdf: iCheck(response.auto.vin),
					car: [
						{
							name: "VIN:",
							value: iCheck(response.auto.vin)
						},
						{
							name: "Пробег:",
							value: iCheck(response.mileage != undefined ? response.mileage : response.auto.mileage)
						},
						{
							name: "Владельцев по ПТС:",
							value: iCheck(response.auto.owners)
						},
						{
							name: "",
							value: "",
							className:'border',
						},
						{
							name: "Кузов:",
							value: iCheck(response.auto.body_type_auto.name)
						},
						{
							name: "Цвет:",
							value: iCheck(response.auto.color_auto)
						},
						{
							name: "",
							value: "",
							className:'border',
						},
						{
							name: "Двигатель:",
							value: response.auto.engine_capacity + ". / " + (response.auto.horsepower ? response.auto.horsepower + " л.с. / " : '') + " " + response.auto.engine_type
						},
						{
							name: "",
							value: "",
							className:'border',
						},
						{
							name: "Трансмиссия:",
							value: iCheck(response.auto.transmission_type)
						},
						{
							name: "Привод:",
							value: iCheck(response.auto.drive_type)
						},
						{
							name: "",
							value: "",
							className:'border',
						},
						{
							name: "Салон:",
							value: iCheck(response.auto.salon_auto)
						},
						{
							name: "Цвет салона:",
							value: iCheck(response.auto.color_salon)
						},
					],
					price: [
						{
							name: "Стоимость до торга:",
							value: iCheck(response.cost_before) + " ₽"
						},
						{
							name: "Стоимость после торга:",
							value: iCheck(response.cost_after) + " ₽"
						},
						{
							name: "Оценочная стоимость:",
							value: iCheck(response.cost_estimated) + " ₽"
						}
					]
				};
				let finder = (items, find) => {
					return items.filter(obj => {
						return obj.label === find + " статус"
					})
				};

				//when data loads from server - init some sliders and fancybox
				setTimeout(() => {
					if ($('.side-cars').length) {

						this.topImageSlider = new Swiper('.side-cars .topper-slider', {
							loop:false,
							navigation: {
								nextEl: ".swiper-button-next",
								prevEl: ".swiper-button-prev"
							  }
						});
					}
				}, 300)
			})
			.fail((xhr, status, error) =>{
				this.fields = null;
				this.not_found = true;
				console.log(this.not_found);
				console.log(error);
				console.log(status);
    		}).then(() => {
				$('.top-tabs li').each(function(index, elem){
					let id = $(elem).attr('id');
					let badCounter = $('.tab[data-tab="' + id + '"]').find('.con.bad').length;

					$(elem).find('.body__fails_count').text(' ' + badCounter);
				});
			})

		},
		handleResize() {
            return window.innerWidth;
        }, 
		scrollToCurrent(index, isTopperSlide) {
			if (index < this.maxVeiwElementsInSlider - 1 && !isTopperSlide) {
				this.topImageSlider.slideTo(index+1);
			} else {
				initDetailImgSliders(index);
			}
		},

		openPopupScheme(item) {
			this.popupData.name = item.name;
			this.popupData.value = item.value;
			this.popupData.description = item.description;
			this.popupSwitch = true;
			this.popupData.list = []
			for (let key in item.list) {
				if(item.list[key]) {
					this.popupData.list.push(key);
				}
			}
		},

		openPopupSchemePower(item) {
			this.popupDataPower.name = item.name;
			this.popupDataPower.value = item.value;
			this.popupDataPower.list = item.list;
			this.popupDataPower.description = item.description;
			this.popupSwitchPower = true;
			this.popupData.list = []
			for (let key in item.list) {
				if(item.list[key]) {
					this.popupData.list.push(key);
				}
			}

		},


		closePopupScheme() {
			this.popupSwitch = false;
		},
		closePopupSchemePower() {
			this.popupSwitchPower = false;
		},

		describeChanger(anchor) {
			if ($(window).width() < 767) {

				$('.expert-row').slideUp(300);
				$('.options-row').slideUp(300);
				$('#single-rew').addClass('hideHR');
				$('.order_report').fadeOut(0)

				$('.grouped-elements').each(function () {
					if ($(this).attr('data-anchor') != anchor) {
						$(this).slideUp(300);
					} else {
						$(this).slideDown(300).find('.content').slideDown(300);
					}
				});

			} else {
				return;
			}
		},

		describeToAll(anchor) {

			if ($(window).width() < 767) {
				$('.expert-row').slideDown(300);
				$('.options-row').slideDown(300)
				$('#single-rew').removeClass('hideHR');
				$('.order_report').fadeIn(0)
				$('.grouped-elements').each(function () {
					if ($(this).attr('data-anchor') != anchor) {
						$(this).slideDown(300);
					} else {
						$(this).find('.content').slideUp(300, function () {
							var target = $(this).closest('.grouped-elements').offset().top;
							$('html,body').animate({ scrollTop: target }, 500);
						});
					}
				});

			} else {
				return;
			}
		},

		generateReportTitle(auto) {
			if (!auto || (auto instanceof Object !== true)) return '';
			try {
				const { year_auto, mark_auto, model_auto } = auto;
				const year = year_auto ? year_auto.year : '';
				const autoMark = mark_auto ? mark_auto.name : '';
				const autoModel = model_auto ? model_auto.name : '';
				return autoMark + ' ' + autoModel + ', ' + year + 'г.';
			} catch (e) {
				console.log(e)
			}
		},
		downloadPdfFile() {
			try {
				$.redirect('/report/get-pdf/', {
						csrfmiddlewaretoken: this.csrf,
						report_id: this.fields.id,
					 }, "POST", "_blank");
			} catch (e) { console.log(e) }
		},
		removeHtmlTagsFromString(string){
			//from mixins
			return this.removeHtmlTags(string);
		},
		setGroupVisible(group) {
			this[group] = true;
		},
		nullFieldstate(value, group){
			if((typeof value === 'number' || typeof value === 'string' || typeof value === 'object') && value != null ) {
				this.setGroupVisible(group);
				return value;
			};
			if(value == true) {
				this.setGroupVisible(group);
				return true;
			};
			if(value == false) {
				this.setGroupVisible(group);
				return false;
			};
			return null;
		},
		activateTab(tab, deactivate={}) {
			this[tab] = true;
			for (let item of deactivate) {
				this[item] = false;
			}
		},
		isOrderHasTwoCars(podborauto_set = []) {
			return podborauto_set.length > 1;
		},
		getOrderCost(cost) {
			if (cost) {
				return this.getHumanReadableNumber(cost)
			}
			return 0;
		},
		getOrderPaymentStatus(status_pay) {
			var status_pay_class = '';
			switch (status_pay) {
				case 'оплачен':
					status_pay_class = 'status_payment_full'
					break;
				case 'частично оплачен':
					status_pay_class = 'status_payment_prepaid'
					break;
				case 'не оплачен':
					status_pay_class = 'status_payment_none'
					break;
				default:
					break;
			}
			return {
				[status_pay_class]: true
			}
		},
		getOrderStatusClass: function (status) {
			var orderClass = '';
			switch (status) {
				case 'новый':
					orderClass = 'status_new'
					break;
				case 'в очереди':
					orderClass = 'status_waiting'
					break;
				case 'в работе':
					orderClass = 'status_progress'
					break;
				case 'отложен':
					orderClass = 'status_paused'
					break;
				case 'выполнен':
					orderClass = 'status_done'
					break;
				case 'частичный возврат':
				case 'возврат произведен':
				case 'выдан клиенту':
				case 'возврат':
					orderClass = 'status_to_client'
					break;
				case 'отказ':
					orderClass = 'status_renunciation'
					break;
				case 'ложный вызов':
					orderClass = 'fail_call'
					break;
				case 'готов к выдаче':
					orderClass = 'status_ready_extradition'
					break;
				case 'перепроверен':
					orderClass = 'status_recheck'
					break;
				case 'возврат документы получены':
					orderClass = 'returnmoneydoc'
					break;
				default:
					break;
			}
			return {
				[orderClass]: true
			}
		},
		openFancyboxTwoSliders(src){
			console.log('openFancyboxTwoSliders',src)
			initDetailImgSliders(0,{src})
		}
	}
})

function initZoomImgInSlider() {
	try {
		$('.slider-popup .top-slider .swiper-slide .gallary_popup_img').each(function (i, imgHolder) {
			var imgH = $(imgHolder);
			var imgWrap = imgH.find('.gallary_popup_img_holder img')[0];


			if ($(window).width() < 767) {
				// var zm = new Zoom(imgWrap, {
				// 	rotate: false,
				// });
			}else {
				//imgWrap.zoom({ magnify: 1.5 });
				wheelzoom(imgWrap);
			}
	})
	} catch (e) { console.error(e) }
}

function destroyZoomImgInSlider() {
	try {
		$('.slider-popup .top-slider .gallary_popup_img').each(function (i, imgHolder) {
			var imgH = $(imgHolder);
			var imgWrap = imgH.find('.gallary_popup_img_holder img')[0];
			imgWrap.dispatchEvent(new CustomEvent('wheelzoom.destroy'));
			//imgWrap.trigger('zoom.destroy')
		})
	} catch (e) { console.error(e) }
}

function initDetailImgSliders(initialIndex,config) {

	config = config || {
		src:'#gallary_popup'
	}
	initialIndex = initialIndex || 0;

	var topSlider,navSlider;

	$.fancybox.open({
		src: config.src,
		type: 'inline',
		opts: {
			animationEffect: "fase",
			animationDuration: 500,
			touch: false, 
			afterLoad: function (instance, current) {
				topSlider = new Swiper(config.src+' .top-slider',{
					keyboard: {
						enabled: true,
					},
					initialSlide:initialIndex,
					//loop:true,
					navigation: {
						nextEl: config.src+' .swiper-button-next',
						prevEl: config.src+' .swiper-button-prev',
					},
					on: {
						init: function () {
							initZoomImgInSlider();
						},
					},
				});

				navSlider = new Swiper(config.src+' .nav-slider',{
					initialSlide:initialIndex,
					//loop:true,
					centeredSlides:true,
					grabCursor:true,
					freeMode:true,
					freeModeSticky:true,
					slideToClickedSlide:true,
					slidesPerView: 'auto',
					freeModeMomentumRatio:0.3,
				});

				navSlider.controller.control = topSlider;
				topSlider.controller.control = navSlider;

			},
			afterClose: function () {
				topSlider.destroy(true, true);
				navSlider.destroy(true, true);
				topSlider = null;
				navSlider = null;
				$(config.src+' .top-slider').unbind('init');
				destroyZoomImgInSlider();
			}
		}
	});
}

$(document).ready(function () {

});
