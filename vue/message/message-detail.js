import Vue from 'vue';


var userMessage = new Vue({
  delimiters: ['${', '}'],
  messageRefreshPeriod: 3000,
  el: '#user-message',
  data: {
    csrf: csrf
  },
  mounted: function() {
        // this.showDialog(sid);
        this.sendMessage(sid, 1);
        setInterval(function () {
          this.sendMessage(sid, 2);
        }.bind(this), 15000);  
  },
  methods: {
    showDialog(id) {
      self = this;
      $.ajax({
        url : `/message/view/${id}/?param=1`,
        type: "POST",
        headers: {'X-CSRFToken': this.csrf },
        processData: false,
        contentType: false,
        dataType    :'json',
        success: function(data) {
          for (var j = 0; j < data.length; j++){
            $('.add_messages').append(this.tovar_template(data[j]));
          }
        }.bind(this),
        error: function (error) {
          self.loading = false;
        }
      });
    },
    tovar_template(message) {
    	var template = 
      "<div class='message'>" +
          "<p>сообщение: " + message.text + "</p>" +
          "<p>отправитель: " + message.sid + "</p>" +
          "<p>получатель: " + message.fid + "</p>" +
    	    "<p>изображение</p><img src=" + message.img + ">  "
      "</div>";      
    	return template;
    },
    sendMessage(id, load) {
      self = this;
      var text = $('#send_message').val();
      var img = $('#img_message').prop('files')[0];
      let data = new FormData();
      data.append('id', id);
      data.append('sid', sid);
      data.append('fid', fid);
      data.append('text', text);
      data.append('img', img);
      data.append('load', load);
      $.ajax({
        url : `/message/send-message`,
        type: "POST",
        headers: {'X-CSRFToken': this.csrf},
        processData: false,
        contentType: false,
        dataType    :'json',
        data: data,
        success: function(data) {
          for (var j = 0; j < data.length; j++){
            $('.add_messages').append(this.tovar_template(data[j]));
          }
        }.bind(this),
        error: function (error) {
          self.loading = false;
        }
      });
    },
  }
});