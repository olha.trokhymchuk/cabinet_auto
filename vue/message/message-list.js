import Vue from 'vue';


var userMessage = new Vue({
  delimiters: ['${', '}'],
  messageRefreshPeriod: 3000,
  el: '#messages',
  data: {
    csrf: csrf,
    count: 0,
    searchUser: '',
  },
  mounted: function() {
        this.showDialog();
  },
  methods: {
    showDialog() {
      self = this;
      let data = new FormData();
      this.count = this.count + 25
      data.append('count', self.count);
      $.ajax({
        url : `/message/view/`,
        type: "GET",
        headers: {'X-CSRFToken': this.csrf },
        processData: false,
        contentType: false,
        dataType    :'json',
        success: function(data) {
            $('.add_messages').append(this.tovar_template(data));
        }.bind(this),
        error: function (error) {
          self.loading = false;
        }
      });
    },
    tovar_template(data) {
    	this.count = data.count;
	    var template = 
	      "<div class='message'>" +
	          "<p>сообщение: " + data.count + "</p>" +
	      "</div>";      
	    return template;
    },
    search_user() {
    	self = this;
    	let data = new FormData();
      	data.append('search-name', self.searchUser);
	    $.ajax({
	        url : `/message/search-user`,
	        type: "POST",
	        headers: {'X-CSRFToken': this.csrf },
	        processData: false,
	        contentType: false,
	        dataType    :'json',
	        data: data,
	        success: function(data) {
	        	for(let user in data[0].context){
	        		$('.searchuser').append(this.user_template(data[0].context[user]));
	        	}
	        }.bind(this),
	        error: function (error) {
	          self.loading = false;
	        }
	    });
    },
    user_template(user) {
	    var template = 
	      "<div class='message'>" +
	          "<p>" + user.name + "</p>" +
	          "<p>" + user.last_name + "</p>" +
	      "</div>";      
	    return template;
    },
  }
});