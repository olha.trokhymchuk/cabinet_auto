import Vue from 'vue';
import list from '../mixins/list';
import funcs from '../mixins/funcs';
import search from '../mixins/search';
import select2 from '../vue/select2-directive';

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  } else {
    return uri + separator + key + "=" + value;
  }
}

new Vue({
    delimiters: ['${', '}'],
    el: '#reviews',
    mixins: [list, search, funcs],
    mounted() {
      let self = this;
      
      window.addEventListener('load', () => {
        self.getRaiting();
      });
    },
    data: {
      sortSelect: '',
      sortList: '',
      reviewsObj: ''
    },
    watch: {
      objects(){
        let self = this;

        setTimeout(function(){
          self.getRaiting();
        }, 100)
      }
    },
    directives: {
      select2: select2
    },
    methods:{
    }
  });