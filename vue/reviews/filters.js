import Vue from 'vue';
import select2 from '../vue/select2-directive';
import filterMixin from '../mixins/filter';
import range from '../mixins/range';

Vue.directive('select', {
    twoWay: true,
    bind: function (el, binding, vnode) {
        $(el).select2().on("select2:select", (e) => {
            el.dispatchEvent(new Event('change', { target: e.target }));
        });
    },
});

var filter = new Vue({
  delimiters: ['${', '}'],
  el: '#filter',
  mixins: [filterMixin, range],
  data() {
    return {
      filterTitle: 'Фильтры',
      filials: [],
      executors: [],
      filial: '',
      executor: '',
      rating__gte: '',
      rating__lte: '',
      reviewsRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
      filterFields: ['filial', 'executor','rating__gte', 'rating__lte'],
      rangeFields: ['reviewsRange'],
      sortkey:'',
    }
  },
  watch: {
    rating__gte: function(reviewNum) {
      this.reviewsRange.select0 = parseInt(reviewNum);
    },
    rating__lte: function(reviewNum) {
      this.reviewsRange.select1 = parseInt(reviewNum);
    },
  },
  mounted: function() {
    let self = this;

    setTimeout(() => {
      self.loadInitial();
    }, 300)
  },
  directives: {
    select2: select2
  },
  methods: {
    loadInitial() {
      var self = this;
      $('body').css('overflow','hidden');
      $.ajax({
        url: '/user/user-reviews/filter/',
        cache: false,
        method: 'GET',
        success: function (data) {
          var url = new URL(window.location.href);
          self.filials = data.filials;
          self.executors = data.executors;
          self.reviewsRange.init = [1, 2, 3, 4, 5];
          self.reviewsRange.select0 = self.rating__gte;
          self.reviewsRange.options0 = [1, 2, 3, 4, 5];
          self.reviewsRange.select1 = self.rating__lte;
          self.reviewsRange.options1 = [1, 2, 3, 4, 5];
          self.loading = false;
      $('body').css('overflow','visible');
        },
        error: function (error) {
          self.loading = false;
      $('body').css('overflow','visible');
          console.log(error);
        }
      });
    },
    afterFilterChange() {
      this.loadInitial();
    }
  }
})


// /user/user-reviews/filter/