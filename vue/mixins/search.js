function getQuerystring(key) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] == key) {
      return pair[1];
    }
  }
}

const globalSearchUrl = '/search';

export default {
  data: {
    searchQuery: '',
    timer: null,

    searchQueryGlobal:'',
  },
  watch: {
    'searchQuery'(query) {
      var self = this;
      // old code - find works in current page
      clearTimeout(this.timer);
      this.timer = setTimeout(function() {
        var title = $(document).find("title").text();
        var query = `?querystring=${self.searchQuery}`;
        var sortkey = getQuerystring('sortkey');
        if (sortkey)
          query += `&sortkey=${sortkey}`;

        window.history.pushState(null, title, query);
        self.url = self.objectsUrl.split('?')[0];
        self.url = self.url.replace(/\/\/+/g, '/');
        self.url = `${self.url}${query}`;
        self.updateObjects();
      }, 1000)
    }
  },
  methods: {
    handleGlobalSearchQuery(query){
      if(this.searchQueryGlobal.trim().length > 0){
        const searchUrl = `${globalSearchUrl}?query=${this.searchQueryGlobal}`;
        window.location.href = searchUrl;
      }
    }
  }
}
