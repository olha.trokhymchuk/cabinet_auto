import { bus } from '../report/report-bus';

export default {
  data() {
    return {
      search_user: ''
    }
  },
  computed: {
    auto_from_salon() {
      return (from_salon) =>`${ from_salon?"Авто из салона": "Авто из под частника" }`
    }

  },
  mounted: function(){
    $('.validateVin').closest('.fieldset__item').find('label').addClass('required_label');
  },
  methods: {
    getFullDate(date){
      return date.toLocaleDateString("en-GB", {
        year: "numeric",
        month: "2-digit",
        day: "2-digit",
      });
    },
    setTimeZero(time){
      if(('' + time).length <= 1){
        return '0' + time
      }
      
      return time;
    },
    getRaiting(){
      let raitingContainerList = document.querySelectorAll('.review__raiting');

      raitingContainerList.forEach(function(raitingElem, _){
        let raitingCount = raitingElem.getAttribute('data-raiting');
        let raitingImages = raitingElem.querySelectorAll('svg');

        raitingImages.forEach(function(raitingSvg, index){
          raitingSvg.classList.remove('active');
          if(index < raitingCount){

            raitingSvg.classList.add('active');
          }
        });
      })
    },
    loadToSelect2(target, source, query, optionValue, optionText, exclude=[], config = {}) {
      var self = this;
      $(target).select2({
        ajax: {
          url: source,
          data: function (params) {
            let name = $('.select2-search--dropdown input.select2-search__field').val();
            query['page'] = params.page || 1;
            query['name'] = name || "";
            return query;
          },
          processResults: function (data) {
            var results = []
            for (var i = 0; i < data.objects.length; i++) {
              var id = data.objects[i][optionValue];
              var text = data.objects[i][optionText];
              if (!exclude.includes(id))
                results.push({id: id, text: text})
            }

            return {
              'results': results,
              'pagination': {
                'more': data.page.has_next
              }
            };
          },
          cache: true
        },
        ...config,
      });
    },
    getAutoName(ppk) {
      var name = "";
      if(ppk == "" || typeof ppk === "undefined" || ppk == undefined){
          return name;
      }
      if (ppk.mark_auto && ppk.mark_auto.name)
        name = `${ppk.mark_auto.name}`;

      if (ppk.model_auto && ppk.model_auto.name)
        name = `${name} ${ppk.model_auto.name}`;

      if (ppk.generation) {
        if (ppk.generation.name) {
          name = `${name} ${ppk.generation.name}`;
        } else if (ppk.generation.years) {
          if (ppk.generation.years.upper)
            name = `${name} ${ppk.generation.years.upper}`;
          if (ppk.generation.years.lower)
            name = `${name} ${ppk.generation.years.lower}`;
        }
      }
      return name;
    },

    getAutoPpk(ppk) {
      var name = "";
      if(ppk == "" || typeof ppk === "undefined" || ppk == undefined){
          return name;
      }
      var mark = ``;
      if (ppk.mark_auto && ppk.mark_auto.name) {
        mark = ppk.mark_auto.name;
      }

      var model = ``;
      if (ppk.model_auto && ppk.model_auto.name) {
        model = ppk.model_auto.name;
      }

      var generation = ``;
      if (ppk.generation) {
        if (ppk.generation.name) {
          generation = ppk.generation.name;
        }
      }

      var year_auto_lower = ``;
      if (ppk.year_auto) {
        if (ppk.year_auto.lower) {
          year_auto_lower = `от ${ppk.year_auto.lower}`;
        }
        if (ppk.year_auto.upper) {
          year_auto_lower = year_auto_lower + ` до ${ppk.year_auto.upper}`;
        }
      }

      var mileage = ``;
      if (ppk.mileage) {
        mileage = `Пробег до ${this.getHumanReadableNumber(ppk.mileage)} км.`;
      }

      var cost = ``;
      if (ppk.cost) {
        cost = `до ${this.getHumanReadableNumber(ppk.cost)} ₽`;
      }

      var equipment = ``;
      if (ppk.equipment) {
        equipment = `Компл: ${ppk.equipment}`;
      }

      var number_of_hosts = ``;
      if (ppk.number_of_hosts) {
        number_of_hosts = `Не более ${ppk.number_of_hosts}х владельцев`;
      }

      var engine_capacity_lower = ``;
      var engine_capacity_upper = ``;
      var engine_capacity = ``;
      if (ppk.engine_capacity) {
        if (ppk.engine_capacity.lower) {
          engine_capacity_lower = `от ${ppk.engine_capacity.lower} л.`;
          engine_capacity = `${engine_capacity_lower} ${engine_capacity_upper};`;
        }
        if (ppk.engine_capacity.upper) {
          engine_capacity_upper = `до ${ppk.engine_capacity.upper} л.`;
          engine_capacity = `${engine_capacity_lower} ${engine_capacity_upper}`;
        }
      }

      var horsepower_lower = ``;
      var horsepower_upper = ``;
      var horsepower = ``;
      if (ppk.horsepower) {
        var horsepowerObj = ppk.horsepower;
        if(typeof horsepowerObj === 'string'){
          horsepowerObj = JSON.parse(`${horsepowerObj}`)
        }
        if (horsepowerObj.lower) {
          horsepower_lower = `от ${horsepowerObj.lower} л.с.`;
          horsepower = `${horsepower_lower} ${horsepower_upper}`;
        }
        if (horsepowerObj.upper) {
          horsepower_upper = `до ${horsepowerObj.upper}`;
          horsepower = `${horsepower_lower} ${horsepower_upper} л.с.`;
        }

      }

      var transmission = ``;
      if (ppk.transmission_type && ppk.transmission_type.length > 0) {
        ppk.transmission_type.forEach(function(item) {
          transmission = `${transmission} ${item}`;
        });
        transmission = `${transmission}`;

      }

      var color_salon = ``;
      if (ppk.color_salon && ppk.color_salon.length > 0) {
        color_salon = `цвет салона`;
        ppk.color_salon.forEach(function(item) {
          color_salon = `${color_salon} ${item}`;
        });
        color_salon = `${color_salon}`;

      }

      var drive_type = ``;
      if (ppk.drive_type && ppk.drive_type.length > 0) {
        ppk.drive_type.forEach(function(item) {
          drive_type = `${drive_type} ${item}`;
        });
        drive_type = `${drive_type}`;

      }
      var salon_auto = ``;
      if (ppk.salon_auto && ppk.salon_auto.length > 0) {
        salon_auto = `салон:`;
        ppk.salon_auto.forEach(function(item) {
          salon_auto = `${salon_auto} ${item}`;
        });
        salon_auto = `${salon_auto},`;

      }

      var engine_type = ``;
      if (ppk.engine_type && ppk.engine_type.length > 0) {
        ppk.engine_type.forEach(function(item) {
          engine_type = `${engine_type} ${item}`;
        });
        engine_type = `${engine_type}`;

      }

      var color_auto = this.getAutoColorMarkup(ppk.color_auto);

      var body_type_auto = ``;
      if (ppk.body_type_auto && ppk.body_type_auto.length > 0) {
        ppk.body_type_auto.forEach(function(item) {
          body_type_auto = `${body_type_auto} ${item.name}`;
        });
        body_type_auto = `${body_type_auto}`;
      }

      var autobaseLink = ``;

      if(mark && model){
	try{
        	let link = `/auto/auto-database/?mark_auto=${ppk.mark_auto.id ||''}&model_auto=${ppk.model_auto.id||''}&generation=${ppk.generation.id||''}`
        	autobaseLink = `<a href="${link}" target="_blank" class="btn btn_xs btn_primary"><span>База знаний</span></a>`
      	}catch(e){}
       }

      return`
        <p><b>${mark} ${model} ${generation} ${year_auto_lower}</b></p>
        ${color_auto}
        <p>${body_type_auto}</p>
        <p>${transmission}</p>
        <p>${engine_capacity ? `${engine_capacity} /`:''} ${horsepower ? `${horsepower} /`:''}  ${engine_type} </p>
        <p>${drive_type}</p>
        <p>${equipment}</p>
        <p>${mileage}</p>
        <p>${number_of_hosts}</p>
        <p>${salon_auto} ${color_salon}</p>
        <b>${cost}</p>
      `;1
    },
    getAutoColorMarkup(color_auto = [], description = 'Цвет: '){
      if(color_auto.length === 0){
        return `<p>${description} Любой</p>`;
      }
      var colorsList = color_auto.map(c => {
        if(c === 'любой'){
          return `<li class="anything-color">${description} Любой</li>`
        }
        return `<li style="background:${this.getColorFromText(c)}"></li>`
      }).join('')
      return `<ul class="auto-color-list">${colorsList}</ul>`
    },
    getColorFromText(colorText = '') {
      var lowCaseText = colorText.toLowerCase();
      switch (lowCaseText) {
        case 'чёрный':
          return '#000'
        case 'серебристый':
          return 'linear-gradient( 90deg, rgb(155,154,152) 0%, #ebebeb 100%)'
        case 'белый':
          return '#fff'
        case 'серый':
          return '#9b9a98'
        case 'синий':
          return '#354dff'
        case 'красный':
          return '#fc4728'
        case 'зеленый':
          return '#38b830'
        case 'коричневый':
          return '#926546'
        case 'бежевый':
          return '#f0d9b0'
        case 'голубой':
          return '#36a1ff'
        case 'золотой':
          return 'linear-gradient( 90deg, rgb(233,150,0) 0%, rgb(253,232,9) 100%)'
        case 'бордовый':
          return '#c5172e'
        case 'фиолетовый':
          return '#9966cc'
        case 'желтый':
          return '#e8da19'
        case 'оранжевый':
          return '#ff9c03'
        case 'розовый':
          return '#ffc0c9'
      }
    },
    getAutoNameFromTemplate(ppk) {
      var name = "";
      ppk = JSON.parse(ppk);
      if(ppk == "" || typeof ppk === "undefined" || ppk == undefined){
          return name;
      }
      if (ppk.mark_auto && ppk.mark_auto.name)
        name = `${ppk.mark_auto.name}`;

      if (ppk.model_auto && ppk.model_auto.name)
        name = `${name} ${ppk.model_auto.name}`;

      if (ppk.generation) {
        if (ppk.generation.name) {
          name = `${name} ${ppk.generation.name}`;
        } else if (ppk.generation.years) {
          if (ppk.generation.years.upper)
            name = `${name} ${ppk.generation.years.upper}`;
          if (ppk.generation.years.lower)
            name = `${name} ${ppk.generation.years.lower}`;
        }
      }
     return name;
    },
    normalize: function(str){
      try{
        if(str.length==0 || str=='NaN'){
          return 0;
        }
      } catch (err) {
        console.log(err);
      }
      return str;
    },
    extract_number(str, humanized){
        if(!humanized){
          return str;
        }
        let number="";
        try{
          let matches = str.match(/\d+/g);
          matches.forEach(function(key) {
            number += key.toString();
          });
        } catch (err) {
          console.log(err);
          return str;
        }
        return number;
    },
    changeHandler: function(value, name) {
      if ( typeof(value) !== "undefined" && value !== null ) {
        value = this.normalize(value.toString());
      }else{
        return 0;
      }
      if(name){
        return this.numberWithSpaces(value, name)
      }
      return this.toWithSpaces(value);
    },
    numberWithSpaces: function(value, name) {
      this[name] = this.toWithSpaces(value);
    },
    toWithSpaces(value){
      if(value==0){
        return 0;
      }else{
        value = parseInt(value.replace(/[^0-9]+/g, '')).toLocaleString().replace(/,/g, " ");
        return value;
      }
    },
    inObject(obj, in_obj){
      for(var prop in in_obj) {
        if(obj.hasOwnProperty(prop)){
          return true;
        }else{
          return false;
        }
      }
    },
    remove_empty(){
      setTimeout(function(){
        $(".report-step__content").each(function( index ) {
          if($.trim($(this).text()).length <= 0){
            $(this).parent().remove();
              // $(this).parent().css("display", "none");
          }
        });
      },3000)
    },
    getQuerystring(key) {
      var query = window.location.search.substring(1);
       var vars = query.split("&");
       for (var i = 0; i < vars.length; i++) {
         var pair = vars[i].split("=");
         if (pair[0] == key) {
           return pair[1];
         }
       }
    },
    getQuerystringBySlash() {
      var arr = ['user_detail', 'tab', 'filial-detail'];
      var q = {'params': {}};
      var query = window.location.href;
      var vars = query.replace(/\/\s*$/,'').split('/');
      var i_qparam = 0;
      for (var i = 0; i < vars.length; i++) {
        i_qparam = i;
        if (arr.indexOf(vars[i]) != -1) {
          q[vars[i]] = vars[i + 1];
        }

      }
      try {
        var params = vars[i_qparam].split('?')[1].split('&');
        for (var i = 0; i < params.length; i++) {
          var param = params[i].split('=');
          q['params'][param[0]] = param[1];
        }
      } catch (err) {
        console.log(err);
      }
      return q;
    },
    transliterate(word){
      var translit = {
        "А": "A",
        "В": "B",
        "С": "C",
        "Е": "E",
        "К": "K",
        "М": "M",
        "Н": "H",
        "О": "O",
        "Р": "P",
        "Т": "T",
        "Х": "X",
      };
        
      var string = word.toUpperCase();
      return string.split('').map(function (char) {
        return translit[char] || char; 
      }).join("");
    },
    updateQueryStringParameter(uri, key, value) {
      var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
      var separator = uri.indexOf('?') !== -1 ? "&" : "?";
      if (uri.match(re)) {
        return uri.replace(re, '$1' + key + "=" + value + '$2');
      }
      else {
        return uri + separator + key + "=" + value;
      }
    },
    getHumanReadableNumber(value) {
      var str_value = value.toString();
      var reversed_value = str_value.split("").reverse().join("");
      var humanized = "";

      for (var i = 0, len = reversed_value.length; i < len; i++) {
        if (i % 3 == 0) {
          humanized += ' ';
          humanized += reversed_value[i];
        } else {
          humanized += reversed_value[i];
        }
      }

      humanized = humanized.split("").reverse().join("");
      return humanized;
    },
    getHumanReadableDate(date){
      if( new Date(date) === "Invalid Date" || isNaN(new Date(date)) ) return '';
      var now = new Date(date);
      var todayUTC = new Date(Date.UTC(now.getFullYear(), now.getMonth(), now.getDate()));
      return todayUTC.toISOString().slice(0, 10).split("-").reverse().join('.')
    },
    modalOpen(expert, order) {
        this.order = order;
        this.type_expert = expert;
        this.$modal.show(expert);
    },
    modalClosed() {
        this.$modal.hide(this.type_expert);
    },
    openDropdown(e) {
      $('.dropdown_open').removeClass('dropdown_open');
      $(e.target).parents('.dropdown').toggleClass('dropdown_open');
    },
    getReports(order) {
      return `${order.car_count}/${order.reports_count}`;
    },
    changedExpert() {
      this.getUserOrders(this.res['user_detail']);
    },
    isActive(value) {
      return typeof(value) === "boolean";
    },
    isPositive(value) {
      return value === true || value == true ;
    },
    isNegative(value) {
      return value === false || value == false ;
    },
    notField(field, label, com, data_id) {
      let exclude_required = ['Остаток ПТК', 'Остаток ЗТК', 'Остаток ПТД',
                              'Остаток ЗТД', 'Остаток летних шин',
                              'Остаток зимних шин'
                            ];
      if(field == false && !com && !exclude_required.includes(label)){
          bus.$emit('event_negative_data', data_id, label);
          return true;
      }else{
          bus.$emit('event_negative_data', data_id, label, true);
      }
      return false;
    },
    extend(obj, src) {
        Object.keys(src).forEach(function(key) { obj[key] = src[key]; });
        return obj;
    },
    onMarkerClick(dataId) {
      let marker = $(`.car-scheme [data-id="${dataId}"]`);
      let stylevalue = marker.attr('style');
      $('.report-field').attr('style', '').hide();
      $('.report-field[data-id="' + dataId + '"]').attr('style', stylevalue).show()
    },
    markerIsNegative(value, field) {

      if (value == false) {
        return true;
      }
    },
    markerIsPositive(value, field) {
      if (value == true) {
        return true;
      }
    },
    unchec(value) {
      if(this.inside_value == value){
        this.inside_value = null;
        this.comment = "";
        this.okrasheno = false;
        this.skol_carapina = false;
        this.vmyatina = false;
        this.corozia_rgavchina = false;
        this.skol = false;
        this.treshina = false;
        this.zapotevanie = false;
        this.zameneno = false;
      }
    },
    translater(message) {
        let messages = {
          "This field may not be null.": "Поле не может быть пустым",
          "This field may not be blank.": "Поле не может быть пустым",
          "Ensure this field has no more than 17 characters.": "Vin превышает 17 символов",
        }
        try {
          if(messages[message] !== undefined) {
            return messages[message];
          } else {
            return message;
          }
        }catch(e){
          return message;
        }
   },
   getError(error, name){
      try{
        return error[name];
      } catch(e){}
    },
  }
}



