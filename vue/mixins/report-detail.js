export default {
  	methods: {
		detectEmptyMarkup(markup){
			if(typeof markup === 'string' && markup === '') return false;
			if(typeof markup === "undefined") return false;
			if(markup == null) return false;
			return true;
		},
		calculateColor(percent) {
			if (percent < 50) {
				return '#f1825f';
			}
			if (percent >= 50 && percent < 75) {
				return '#f9b001';
			}
			if (percent >= 75) {
				return '#00b157';
			}
		},
	}
}