import Vue from 'vue';


export default {
  data() {
      return {
      rangeFields: [],
    }
  },
  created: function() {
    this.bayWatch(this.rangeFields, this.getSelect0Updater.bind(this), this.getSelect1Updater.bind(this));
  },
  methods: {
    bayWatch: function(props, select0Watcher, select1Watcher) {
      var iterator = function(prop) {
        this.$watch(`${prop}.select0`, select0Watcher(prop, select0Watcher));
        this.$watch(`${prop}.select1`, select1Watcher(prop, select1Watcher));
      };

      props.forEach(iterator, this);
    },
    getSelect0Updater(name, value) {

      return (value) => {
        let range = this[name];

        if(range.select0.toString().indexOf('.') == -1){
          range.select0 = parseInt(range.select0);
        }

        let index = range.init.indexOf(range.select0);

        if (index >= 0){ 
          let step = typeof range.step !== 'undefined'? range.step: 1;

          range.options1 = range.init.slice(index + step, range.init.length);
          if(name === 'yearsRange' && name !== 'reviewsRange'){
            range.options1 = range.init.slice(0, index);
          }
        }
        this.$nextTick(function() {
          $('select').select2({});
        });
      }
    },
    getSelect1Updater(name, value) {
      return (value) => {
        let range = this[name];
        let index = range.init.indexOf(range.select1);

        if (index >= 0){
          let step = typeof range.step !== 'undefined'? range.step: 1;

          if(step == 0){
            index++;
          }
          if(name === 'yearsRange' && name !== 'reviewsRange'){
            range.options0 = range.init.slice(index + 1, range.init.length);
          }

          if(name === 'horsepowerRange'){
            range.options0 = range.init.slice(0, index);
          }
        }
        this.$nextTick(function() {
          $('select').select2({});
        });
      }
    }
  }
}
