import axios from "axios";
import funcs from '../mixins/funcs';

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  } else {
    return uri + separator + key + "=" + value;
  }
}


export default {
  mixins: [funcs],
  data: {
    objects: [],
    count: 0,
    loading: false,
    csrf: csrf,
    loadDataAfterMount: true,
    sharedState: store.state,
    objectsUrl: window.location.pathname.split('?')[0],
    url: window.location.href,
    deleteUrl: '',
    page: {
      num_pages: 1,
      number: 1,
      has_previous: false,
      has_next: false,
      has_other_pages: false,
      start_index: 1,
      end_index: 1
    },
    count_load: 0
  },
  mounted: function () {
     var part_url = this.url.split('/');
     if(part_url[4] == "detail-auto" || part_url[4] == "edit-auto"){
        this.url =  window.location.protocol + "//" + window.location.host + "/report/list-by-car/" + part_url[5];
     }
     if (this.loadDataAfterMount)
       this.updateObjects();
  },
  watch: {
    'sharedState.filter'(query) {
      var title = $(document).find("title").text();
      window.history.pushState(null, title, query);
      this.url = this.objectsUrl.split('?')[0] + query;
      this.updateObjects();
    }
   
  },
  computed: {
    filterCount: function() {
      return this.sharedState && this.sharedState.filterCount ? this.sharedState.filterCount : 0;
    }
  },
  methods: {
    salesObject(obj) {
      var isConfirmedAction = confirm('Вы уверены что хотите пометить авто как “Проданное”?')
      if(isConfirmedAction){
        var self = this;
        $('#loader-wrapper').show();
        $.ajax({
          url : `/auto/sales-auto/${obj.id}/`,
          type: "POST",
          data : {csrfmiddlewaretoken: this.csrf},
          success: function(count) {
            $('#loader-wrapper').hide();
            $('.dropdown_open').removeClass('dropdown_open');
            location.reload();
          },
          error:function(e){
            $('#loader-wrapper').hide();
            $('.dropdown_open').removeClass('dropdown_open');
          }
        });
      }
    },

    removeObject(obj, url) {
      var isConfirmedAction = confirm('Вы уверены что хотите удалить ?')
      if(isConfirmedAction){
        var self = this;
        $('#loader-wrapper').show();
        //$('.loade_stage').show();
        if (url){  }
        else{ url = `${self.deleteUrl}/${obj.id}/` }
        $.ajax({
          url : `${self.deleteUrl}/${obj.id}/`,
          type: "POST",
          data : {csrfmiddlewaretoken: this.csrf},
          success: function(count) {
            try {
              var index = self.objects.indexOf(obj);
              self.objects.splice(index, 1);
              self.count -= 1;
              self.updateObjects();
            } catch (error) {
              console.log(error)
            }

            $('#loader-wrapper').hide();
            //$('.loade_stage').hide();
            $('.dropdown_open').removeClass('dropdown_open');
          },
          error:function(e){
            $('#loader-wrapper').hide();
            //$('.loade_stage').hide();
            $('.dropdown_open').removeClass('dropdown_open');
          }
        });
      }
    },
    removeReportFromOrder(obj) {
      var self = this;
      console.log(obj);
      console.log(order_id);
      $('#loader-wrapper').show();
      //$('.loade_stage').show();
      $.ajax({
        url : `${self.deleteUrl}/${obj.id}/${order_id}/`,
        type: "POST",
        data : {csrfmiddlewaretoken: this.csrf},
        success: function(count) {
          var index = self.objects.indexOf(obj);
          self.objects.splice(index, 1);
          self.count -= 1;
          self.updateObjects();
          $('#loader-wrapper').hide();
          //$('.loade_stage').hide();
          $('.dropdown_open').removeClass('dropdown_open');
        }
      });
    },
    closeObject(obj) {
      var self = this;
      $.ajax({
        url : `${self.closeUrl}/${obj.id}/`,
        type: "POST",
        data : {csrfmiddlewaretoken: this.csrf},
        success: function(count) {
          var index = self.objects.indexOf(obj);
          self.objects.splice(index, 1);
          self.count -= 1;
          self.updateObjects();
          $('.dropdown_open').removeClass('dropdown_open');
        }
      });
    },
    getLinkByPageNumber(pageNumber) {
      return updateQueryStringParameter(this.url, 'page', pageNumber);
    },
    toPage(pageNumber) {
      this.sharedState.filter = window.location.href.split('?').length == 2 ? `?${window.location.href.split('?')[1]}` : '';
      this.sharedState.filter = updateQueryStringParameter(this.sharedState.filter, 'page', pageNumber);
      $(window).scrollTop(0);
    },
    updateObjects() {
      if (this.loading)
        return;
      $('#loader-wrapper').show();
      var self = this;
      this.loading = true;
      let requestPhotoId = [];

      $.ajax({
        url: this.url,
        cache: false,
        method: 'GET',
        success: function (data) {
          self.objects = data.objects;
          self.count = data.count;
          self.page = data.page;
          self.loading = false;
          $('body').css("overflow",'auto');
          if(self.objects){
            self.objects.map( (item)=> {
              if(item.auto){
                if(item.last_report_id){
                  requestPhotoId.push(item.last_report_id)
                }else{
                  requestPhotoId.push(item.id);
                }
              }else{
                  requestPhotoId.push(item.id);
              }
            });
          };
          if(self.objects){
            self.objects.map((item)=>{
                const editTime = new Date(item.created);
                let month = editTime.getMonth() +1;
                let dey = editTime.getDate();
                if(month <= 9){
                    month = '0'+ month;
                }
                if(dey <= 9){
                    dey = '0'+ dey;
                }
            });
          }
          if($('#orders').length > 0){
            data.objects.map((elem) => {
              if(elem.date_of_inspection !== undefined && elem.date_of_inspection !== null){
                let date = new Date(elem.date_of_inspection);
                var fullDate = self.getFullDate(date);
    
                elem.date_of_inspection_converted =  'Дата и время: ' + fullDate.replaceAll('/', '.') + ' в ' + self.setTimeZero(date.getHours() - 3) + ':' + self.setTimeZero(date.getMinutes())
              }
            });
          }

          if(typeof data.objects != 'undefined' && data.objects.length <= 0 && self.count_load <= 10){
            self.count_load += 1;
            self.updateObjects();
          }else{
            self.count_load = 0;
            $('#loader-wrapper').hide();

            if(self.objects){
              setTimeout(function(){
                if($('.review__block').length > 0){
                  self.defReviewsList = data.objects;
                  self.getRaiting();
                }
              }, 1000)
            }
          }
        },
        error: function (error) {
          self.loading = false;
          console.log(error);
        }
      });
    },
  }
}
