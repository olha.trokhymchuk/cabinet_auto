import Vue from 'vue';


function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  } else {
    return uri + separator + key + "=" + value;
  }
}



function getParams (url) {
  var params = {};
  var parser = document.createElement('a');
  parser.href = url;
  var query = parser.search.substring(1);
  var vars = query.split('&');

  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    params[pair[0]] = decodeURIComponent(pair[1]);
  }

  return params;
};


export default {
  data: function() {
    return {
      timer: null,
      filterFields: [],
      sharedState: store.state,
      uri: window.location.href.split('?').length == 2 ? `?${window.location.href.split('?')[1]}` : '',
      loading: true,
      timeOut: 1000,
      queue: [],
      sortkey: '',
    }
  },
  mounted: function() {
    var url = new URL(window.location.href);
    var params = getParams(url);
    for (var key in params) {
      var value = params[key];
      if (this.hasOwnProperty(key)) {
        if (value.includes(',')) {
          this[key] = value.split(',');
        } else if (this[key].constructor === Array) {
          this[key] = [value];
        } else {
          this[key] = value;
        }
      }
    }

    this.filterFields.push('sortkey');
    this.baywatch(this.filterFields, this.getFilterUpdater.bind(this));
    this.setCountOfFilters();

    $('select').select2({});
  },
  methods: {

    closeFilter(){
        const rightbar = $('.right-bar');
        const rightbarBox = $('.right-bar__box');
        const rightbarBtnToggle = $('.right-bar-toggle');
        rightbarBtnToggle.removeClass('btn_active');
        $('html').removeClass('html_right-bar');
        rightbar.removeClass('right-bar_open').addClass('right-bar_out');
        rightbarBox.one(animationsEvents, function () {
          rightbar.removeClass('right-bar_out');
        });
      },
    baywatch: function(props, watcher) {
      var iterator = function(prop) {
        this.$watch(prop, watcher(prop, watcher));
      };

      props.forEach(iterator, this);
    },
    in_array(arr, el) {
      return arr.includes(parseInt(el))?true:false;
    },
    setCountOfFilters() {
      var filterCount = 0;
      for (var i = 0; i < this.filterFields.length; i++) {
        var field = this.filterFields[i];
        if (typeof this[field] !== 'undefined' && this[field].length > 0 && this[field] !== null) {
          filterCount += 1;
        }
      }

      this.sharedState.filterCount = filterCount;
    },
    getFilterUpdater(name, value) {
      var self = this;
      return (value) => {
        this.queue.push({'name': name, 'value': value});
        if (!this.loading) {
          clearTimeout(this.timer);
          var self = this;
          this.timer = setTimeout(function() {
            if (name == 'sortkey')
              self.uri = window.location.href.split('?').length == 2 ? `?${window.location.href.split('?')[1]}` : '';
            for (var i = 0; i < self.queue.length; i++) {
              self.uri = updateQueryStringParameter(self.uri, self.queue[i].name, self.queue[i].value);
              self.uri = updateQueryStringParameter(self.uri, 'page', 1);
            }

            self.setCountOfFilters();
            self.queue = [];
            self.sharedState.filter = self.uri;
          }, this.timeOut)
        }
      }
    },
  }
}
