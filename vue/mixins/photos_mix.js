export default {
	data(){
		return{
			modalId: 0
		}
	},
	props: ['autoAction', 'multiple', 'manualAction', 'uploaded', 'updatePhotoUrl', 'deletePhotoUrl'],
  	methods: {
  		base64ImageToFile(str) {
	      let pos = str.indexOf(';base64,');
	      let type = str.substring(5, pos);
	      let b64 = str.substr(pos + 8);

	      let imageContent = atob(b64);
	      let buffer = new ArrayBuffer(imageContent.length);
	      let view = new Uint8Array(buffer);

	      for (let n = 0; n < imageContent.length; n++) {
	        view[n] = imageContent.charCodeAt(n);
	      }

	      return new File([buffer], {type: type});
	    },
		inputFile: function (newFile, oldFile) {
			self = this;
			$('#loader-wrapper').show();
			$('.loade_stage').show();
			try {
				if (newFile && oldFile && !newFile.active && oldFile.active) {
					if (newFile.xhr) {
						if (newFile.xhr.status == 200) {
							var newUploadedFile = JSON.parse(newFile.xhr.response);
							self.uploadedFiles.push(newUploadedFile);
							var index = self.files.indexOf(newFile);
							self.files.splice(index, 1);
						}
					}
				}
			} catch (e) {
				$('#loader-wrapper').hide();
				$('.loade_stage').hide();
			}
			try {
				if (!newFile.active) {
					$('#loader-wrapper').hide();
					$('.loade_stage').hide();

				}
			} catch (e) {
				$('#loader-wrapper').hide();
				$('.loade_stage').hide();
			}
			try {
				if (newFile.xhr) {
					$('#loader-wrapper').hide();
					$('.loade_stage').hide();

				}
			} catch (e) {
				$('#loader-wrapper').hide();
				$('.loade_stage').hide();
			}
	    },

	    inputFilter: function (newFile, oldFile, prevent) {
	      if (newFile && !oldFile) {
	        if (!/\.(jpeg|jpe|jpg|gif|png|webp)$/i.test(newFile.name)) {
	          return prevent()
	        }
	      }


	      // Automatic compression Ffmpeg
	      if (false && newFile.file && newFile.type.substr(0, 6) === 'image/' && this.autoCompress > 0 && this.autoCompress < newFile.size) {
	         newFile.error = 'compressing'
	         const imageCompressor = new ImageCompressor(null, {
	           convertSize: Infinity,
	           maxWidth: 1024,
	           maxHeight: 1024,
	         })
	         imageCompressor.compress(newFile.file)
	           .then((file) => {
	            this.$refs.upload.update(newFile, { error: '', file, size: file.size, type: file.type })
	           })
	           .catch((err) => {
	             this.$refs.upload.update(newFile, { error: err.message || 'compress' })
	           })
	      	}
	      if (!this.multiple && typeof this.uploaded !== 'undefined' && this.uploaded.length > 0) {
	        this.changed = [];
	        this.deleted = this.uploaded;
	      }
	      if (newFile) {
	        newFile.blob = '';
	        let URL = window.URL || window.webkitURL;
	        if (URL && URL.createObjectURL) {
	        	let URL = window.URL || window.webkitURL;
	          	newFile.blob = URL.createObjectURL(newFile.file)
	        }
	      }

	    },
	    onEditorFile(numb_el) {
	        this.editFile.show = false;
	      if (!this.$refs.upload.features.html5) {
	        this.alert('Your browser does not support')
	        this.editFile.show = false;
	        return
	      }

	      let data = {
	        name: this.editFile.name,
	      }

	      if (this.editFile.cropper) {
	        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
	        let arr = new Uint8Array(binStr.length);
	        for (let i = 0; i < binStr.length; i++) {
	          arr[i] = binStr.charCodeAt(i);
	        }
	        data.file = new File([arr], data.name, { type: this.editFile.type });
	        data.size = data.file.size;
	      }
	      this.$refs.upload.update(this.editFile.pk, data);
	      this.editFile.error = '';
	      this.editFile.show = false;
	    },
	    onCover(file) {
	    	let dropdown_content = document.querySelectorAll('.dropdown_open');
	    	this.updateAutoCover(file, report_id);
	    	 dropdown_content.forEach(item => {
		    	item.classList.remove("dropdown_open");
		    });
	    },
	    onEditorUploadedFile() {
	      $(".v--modal-overlay.scrollable").css({"display":"none"});
	      $("body").removeClass('v--modal-block-scroll');
	        this.editFile.show = false;
	      if (!this.$refs.upload.features.html5) {
	        this.alert('Your browser does not support')
	        this.editFile.show = false;
	        return
	      }

	      let data = {
	        name: this.editFile.name,
	      }

	      if (this.editFile.cropper) {
	      	console.log("cropper");
	        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
	        let arr = new Uint8Array(binStr.length);
	        for (let i = 0; i < binStr.length; i++) {
	          arr[i] = binStr.charCodeAt(i);
	        }
	        data.file = new File([arr], data.name, { type: this.editFile.type });
	        data.size = data.file.size;
	      }

	      this.editFile.error = '';
	      this.editFile.show = false;
	      let URL = window.URL || window.webkitURL
	      if (URL && URL.createObjectURL) {
	        this.changed.push({'id': this.editFile.pk, 'file': data.file, 'image_small': URL.createObjectURL(data.file)});
	      }
	      if(this.updateUploadedFile){
	      	this.updateUploadedFile(data.file, this.editFile.pk, this.editFile.sort_key);
	      }
	    },
	    // add folder
	    onAddFolder() {
	      if (!this.$refs.upload.features.directory) {
	        this.alert('Your browser does not support')
	        return
	      }
	      let input = this.$refs.upload.$el.querySelector('input')
	      input.directory = true
	      input.webkitdirectory = true
	      this.directory = true
	      input.onclick = null
	      input.click()
	      input.onclick = (e) => {
	        this.directory = false
	        input.directory = false
	        input.webkitdirectory = false
	      }
	    },
	    onAddData() {
	      this.addData.show = false
	      if (!this.$refs.upload.features.html5) {
	        this.alert('Your browser does not support')
	        return
	      }
	      let file = new window.File([this.addData.content], this.addData.name, {
	        type: this.addData.type,
	      })
	      this.$refs.upload.add(file)
	    },
	    openDropdown(e) {
	      $(e.target).parents('.dropdown').toggleClass('dropdown_open');
	    },
	    modalOpened() {
	      var editImage = document.getElementById('edit-image1');
	      if (!editImage) {
	        editImage = document.getElementById('edit-image2');
	      }
	      this.$refs.editImage = editImage;
	      this.editFile = { ...this.editFile, show: true }
	    },
	    modalClosed() {
	      this.editFile.show = false;
	    },
	    onEditFileShow(file) {
	      this.editFile = { ...file}
	      this.$refs.upload.update(file, { error: 'edit' })
	      this.$modal.show('edit-modal');

	    },
	    onEditUploadedFileShow(file,numb_el) {
      console.log('file +');
      console.log('file')
	      this.editFile = { ...file, type: 'image/jpeg', name: file.pk }
	      this.$refs.upload.update(file, { error: 'edit' })
      		console.log(numb_el);
	      this.$modal.show(numb_el);
	      $(".v--modal-overlay.scrollable").css({"display":"block"});
	    },
	    onRotateFile(file, degree, editFile) {
	      	let data = new FormData();
			data.append('csrfmiddlewaretoken', this.csrf);
			data.append('image', file.image);
			data.append('image_small', file.image_small);
			data.append('photo_id', file.id);
	      	var self = this;
	      	$.ajax({
				url: "/user/change_image/",
				method: 'POST',
				data : data,
				cache: false,
				contentType: false,
				processData: false,
				success: function (data) {

					var img = $("#" + file.pk)
					var get_image = "https://" + window.location.hostname  + data["image"];
					var xhr = new XMLHttpRequest();
					xhr.open("GET", get_image);
					xhr.responseType = "arraybuffer";
					xhr.onload = function(e) {
						var blob = new Blob([xhr.response], {type: "image/png"});
					    var url = URL.createObjectURL(blob);
						var img = $("#" + file.id)
						img[0].src = url;
						file.image_small = url;
						this.$refs.changed.update(file, { error: 'edit' })
					}
					xhr.send();
				},
				error: function (error) {
				    console.log(error);
				}
			});
	    },

   	}

}