export const step_dict = {
    "": {
        "Проверка по ПТС" : "Проверка по ПТС",
        "Проверка по ГИБДД" : "Проверка по ГИБДД",
        "Проверка по ФССП" : "Проверка по ФССП",
        "Проверка по реестру залогов" : "Проверка по реестру залогов",
        "Проверка по Автокоду" : "Проверка по Автокоду",
        "Проверка по ГИБДД" : "Проверка по ГИБДД",
        "Проверка по доп. сервисам": "Проверка по доп. сервисам",
        "Посторонние шумы": "Посторонние шумы",
        "Запотевание/течи": "Запотевание/течи",
        "Уровень масла": "Уровень масла",
        "Уровень жидкости охлаждающей системы": "Уровень жидкости охлаждающей системы",
        "Обслуживание ДВС": "Обслуживание ДВС",
        "Состояние приводных ремней": "Состояние приводных ремней",
        "Работа КПП": "Работа КПП",

        "Остаток летних шин": "Остаток летних шин",
        "Остаток зимних шин": "Остаток зимних шин",
        "Уровень тормозной жидкости": "Уровень тормозной жидкости",
        "Уровень жидкости ГУР": "Уровень жидкости ГУР",
        "Стояночный тормоз": "Стояночный тормоз",
        "Состояние шин": "Состояние шин",
        "Тормозная жидкость": "Тормозная жидкость",
        "Посторонние стуки и скрипы": "Посторонние стуки и скрипы",
        "Прямолинейность движения": "Прямолинейность движения",
        "Люфты в рулевом управлении": "Люфты в рулевом управлении",

        "Связь со всеми блоками ЭБУ": "Связь со всеми блоками ЭБУ",
        "Ошибки в ЭБУ": "Ошибки в ЭБУ",
        "Соответствие прописанных ключей с фактическим": "Соответствие прописанных ключей с фактическим",

        "Система SRS (подушки безопасности)": "Система SRS (подушки безопасности)",
        "Система A/C (кондиционер)": "Система A/C (кондиционер)",

        "Дефекты": "Дефекты",
        "Чистота": "Чистота",

        "Люфты (шаровые, рулевые наконечники, тяги, подшипники)": "Люфты (шаровые, рулевые наконечники, тяги, подшипники)",
        "Тормозная система (шланги, трубки, суппорта)": "Тормозная система (шланги, трубки, суппорта)",
        "Запотевания": "Запотевания",
    },
    "true": {
        "Проверка по ПТС" : "ПТС оригинал",
        "Посторонние шумы": "Посторонние шумы отсутствуют",
        "Запотевание/течи": "Запотевание/течи отсутствуют",
        "Уровень масла": "Уровень масла в норме",
        "Уровень жидкости охлаждающей системы": "Уровень жидкости охлаждающей системы в норме",
        "Обслуживание ДВС": "Обслуживание ДВС не требуется",
        "Состояние приводных ремней": "Состояние приводных ремней в норме",
        "Работа КПП": "КПП работает исправно",

        "Остаток летних шин": "Остаток летних шин",
        "Остаток зимних шин": "Остаток зимних шин",
        "Уровень тормозной жидкости": "Уровень тормозной жидкости в норме",
        "Уровень жидкости ГУР": "Уровень жидкости ГУР в норме",
        "Стояночный тормоз": "Стояночный тормоз в норме",
        "Состояние шин": "Состояние шин",
        "Тормозная жидкость": "Тормозная жидкость",
        "Посторонние стуки и скрипы": "Посторонние стуки и скрипы отсутствуют",
        "Прямолинейность движения": "Прямолинейность движения сохраняется",
        "Люфты в рулевом управлении": "Люфты в рулевом управлении отсутствуют",

        "Связь со всеми блоками ЭБУ": "Связь со всеми блоками ЭБУ",
        "Ошибки в ЭБУ": "Ошибки в ЭБУ отсутствуют",
        "Соответствие прописанных ключей с фактическим": "Соответствие прописанных ключей с фактическим",

        "Система SRS (подушки безопасности)": "Система SRS (подушки безопасности) исправна",
        "Система A/C (кондиционер)": "Система A/C (кондиционер) исправна",

        "Дефекты": "Дефекты отсутствуют",
        "Чистота": "Чистый",

        "Люфты (шаровые, рулевые наконечники, тяги, подшипники)": "Люфты отсутствуют (шаровые, рулевые наконечники, тяги, подшипники)",
        "Тормозная система (шланги, трубки, суппорта)": "Тормозная система (шланги, трубки, суппорта) исправна",
        "Запотевания": "Запотевания отсутствуют",
    },
    "false": {
        "Проверка по ПТС" : "ПТС не оригинал",
        "Посторонние шумы": "Присутствуют посторонние шумы",
        "Запотевание/течи": "Присутствуют запотевание/течи",
        "Уровень масла": "Уровень масла",
        "Уровень жидкости охлаждающей системы": "Уровень жидкости охлаждающей системы",
        "Обслуживание ДВС": "Требуется обслуживание ДВС",
        "Состояние приводных ремней": "Состояние приводных ремней",
        "Работа КПП": "Неисправность КПП",

        "Остаток летних шин": "Остаток летних шин",
        "Остаток зимних шин": "Остаток зимних шин",
        "Уровень тормозной жидкости": "Уровень тормозной жидкости",
        "Уровень жидкости ГУР": "Уровень жидкости ГУР",
        "Стояночный тормоз": "Стояночный тормоз",
        "Состояние шин": "Состояние шин",
        "Тормозная жидкость": "Тормозная жидкость",
        "Посторонние стуки и скрипы": "Посторонние стуки и скрипы",
        "Прямолинейность движения": "Прямолинейность движения",
        "Люфты в рулевом управлении": "Присутствуют люфты в рулевом управлении",

        "Связь со всеми блоками ЭБУ": "Отсутствует связь с блоками ЭБУ",
        "Ошибки в ЭБУ": "Ошибки в ЭБУ",
        "Соответствие прописанных ключей с фактическим": "Несоответствие прописанных ключей с фактическим",

        "Система SRS (подушки безопасности)": "Система SRS (подушки безопасности) неисправна",
        "Система A/C (кондиционер)": "Система A/C (кондиционер) неисправна",

        "Дефекты": "Есть дефекты",
        "Чистота": "Загрязнен",

        "Люфты (шаровые, рулевые наконечники, тяги, подшипники)": "Люфты (шаровые, рулевые наконечники, тяги, подшипники)",
        "Тормозная система (шланги, трубки, суппорта)": "Тормозная система (шланги, трубки, суппорта) неисправна",
        "Запотевания": "Присутствуют запотевания",
    }
}

export const base = {
    "vin": {"name": "VIN", "validate": "max: 17, regex: /^[^*]*$/, unique_vin: true", "type": "text"},
    "mileage": {"name": "Пробег", "validate": "", "type": "text", "humanize": "true"},
    "owners": {"name": "Количество по ПТС", "validate": "", "type": "number"},
}

export const jur = {
    "jur_pts_original": {"name": "Проверка по ПТС", "com": "jur_pts_original_com"},
    "jur_gbdd": {"name": "Проверка по ГИБДД", "com": "jur_gbdd_com"},
    "jur_fssp": {"name": "Проверка по ФССП", "com": "jur_fssp_com"},
    "jur_res_zalog": {"name": "Проверка по реестру залогов", "com": "jur_res_zalog_com"},
    "jur_auto_kod": {"name": "Проверка по Автокоду", "com": "jur_auto_kod_com"},
    "jur_dop_service": {"name": "Проверка по доп. сервисам", "com": "jur_dop_service_com"},
}

export const inside = {
    "krisha": {
        "name": "Крыша", "com": "krisha_com", "top": 50, "left": 50,
        "okrasheno": "krisha_okrasheno", "skol_carapina": "krisha_skol_carapina",
        "vmyatina": "krisha_vmyatina", "corozia_rgavchina": "krisha_corozia_rgavchina",
        "zameneno": "krisha_zameneno",
    },
    "kapot": {
        "name": "Капот", "com": "kapot_com", "top": 28, "left": 50,
        "okrasheno": "kapot_okrasheno", "skol_carapina": "kapot_skol_carapina",
        "vmyatina": "kapot_vmyatina", "corozia_rgavchina": "kapot_corozia_rgavchina",
        "zameneno": "kapot_zameneno",
    },
    "bagagnik": {
        "name": "Багажник", "com": "bagagnik_com", "top": 75, "left": 50,
        "okrasheno": "bagagnik_okrasheno", "skol_carapina": "bagagnik_skol_carapina",
        "vmyatina": "bagagnik_vmyatina", "corozia_rgavchina": "bagagnik_corozia_rgavchina",
        "zameneno": "bagagnik_zameneno",
    },
    "peredni_bamper": {
        "name": "Передний бампер", "com": "peredni_bamper_com", "top": 12, "left": 50,
        "okrasheno": "peredni_bamper_okrasheno", "skol_carapina": "peredni_bamper_skol_carapina",
        "vmyatina": "peredni_bamper_vmyatina", "corozia_rgavchina": "peredni_bamper_corozia_rgavchina",
        "zameneno": "peredni_bamper_zameneno",
    },
    "zadni_bamper": {
        "name": "Задний бампер", "com": "zadni_bamper_com", "top": 86, "left": 50,
        "okrasheno": "zadni_bamper_okrasheno", "skol_carapina": "zadni_bamper_skol_carapina",
        "vmyatina": "zadni_bamper_vmyatina", "corozia_rgavchina": "zadni_bamper_corozia_rgavchina",
        "zameneno": "zadni_bamper_zameneno",
    },
    "perednee_levoe_krilo": {
        "name": "Переднее левое крыло", "com": "perednee_levoe_krilo_com", "top": 30, "left": 25,
        "okrasheno": "perednee_levoe_krilo_okrasheno", "skol_carapina": "perednee_levoe_krilo_skol_carapina",
        "vmyatina": "perednee_levoe_krilo_vmyatina", "corozia_rgavchina": "perednee_levoe_krilo_corozia_rgavchina",
        "zameneno": "perednee_levoe_krilo_zameneno",
    },
    "perednee_pravoe_krilo": {
        "name": "Переднее правое крыло", "com": "perednee_pravoe_krilo_com", "top": 30, "left": 75,
        "okrasheno": "perednee_pravoe_krilo_okrasheno", "skol_carapina": "perednee_pravoe_krilo_skol_carapina",
        "vmyatina": "perednee_pravoe_krilo_vmyatina", "corozia_rgavchina": "perednee_pravoe_krilo_corozia_rgavchina",
        "zameneno": "perednee_pravoe_krilo_zameneno",
    },
    "peredneya_levaya_dver": {
        "name": "Передняя левая дверь", "com": "peredneya_levaya_dver_com", "top": 44, "left": 22,
        "okrasheno": "peredneya_levaya_dver_okrasheno", "skol_carapina": "peredneya_levaya_dver_skol_carapina",
        "vmyatina": "peredneya_levaya_dver_vmyatina", "corozia_rgavchina": "peredneya_levaya_dver_corozia_rgavchina",
        "zameneno": "peredneya_levaya_dver_zameneno",
    },
    "peredneya_pravaya_dver": {
        "name": "Передняя правая дверь", "com": "peredneya_pravaya_dver_com", "top": 44, "left": 78,
        "okrasheno": "peredneya_pravaya_dver_okrasheno", "skol_carapina": "peredneya_pravaya_dver_skol_carapina",
        "vmyatina": "peredneya_pravaya_dver_vmyatina", "corozia_rgavchina": "peredneya_pravaya_dver_corozia_rgavchina",
        "zameneno": "peredneya_pravaya_dver_zameneno",
    },
    "zadnya_levaya_dver": {
        "name": "Задняя левая дверь", "com": "zadnya_levaya_dver_com", "top": 56, "left": 22,
        "okrasheno": "zadnya_levaya_dver_okrasheno", "skol_carapina": "zadnya_levaya_dver_skol_carapina",
        "vmyatina": "zadnya_levaya_dver_vmyatina", "corozia_rgavchina": "zadnya_levaya_dver_corozia_rgavchina",
        "zameneno": "zadnya_levaya_dver_zameneno",
    },
    "zadnya_pravaya_dver": {
        "name": "Задняя правая дверь", "com": "zadnya_pravaya_dver_com", "top": 56, "left": 78,
        "okrasheno": "zadnya_pravaya_dver_okrasheno", "skol_carapina": "zadnya_pravaya_dver_skol_carapina",
        "vmyatina": "zadnya_pravaya_dver_vmyatina", "corozia_rgavchina": "zadnya_pravaya_dver_corozia_rgavchina",
        "zameneno": "zadnya_pravaya_dver_zameneno",
    },
    "zadnee_levoe_krilo": {
        "name": "Заднее левое крыло", "com": "zadnee_levoe_krilo_com", "top": 70, "left": 25,
        "okrasheno": "zadnee_levoe_krilo_okrasheno", "skol_carapina": "zadnee_levoe_krilo_skol_carapina",
        "vmyatina": "zadnee_levoe_krilo_vmyatina", "corozia_rgavchina": "zadnee_levoe_krilo_corozia_rgavchina",
        "zameneno": "zadnee_levoe_krilo_zameneno",
    },
    "zadnee_pravoe_krilo": {
        "name": "Заднее правое крыло", "com": "zadnee_pravoe_krilo_com", "top": 70, "left": 75,
        "okrasheno": "zadnee_pravoe_krilo_okrasheno", "skol_carapina": "zadnee_pravoe_krilo_skol_carapina",
        "vmyatina": "zadnee_pravoe_krilo_vmyatina", "corozia_rgavchina": "zadnee_pravoe_krilo_corozia_rgavchina",
        "zameneno": "zadnee_pravoe_krilo_zameneno",
    },
    "levii_porog": {
        "name": "Левый порог", "com": "levii_porog_com", "top": 50, "left": 15,
        "okrasheno": "levii_porog_okrasheno", "skol_carapina": "levii_porog_skol_carapina",
        "vmyatina": "levii_porog_vmyatina", "corozia_rgavchina": "levii_porog_corozia_rgavchina",
        "zameneno": "levii_porog_zameneno",
    },
    "pravii_porog": {
        "name": "Правый порог", "com": "pravii_porog_com", "top": 50, "left": 85,
        "okrasheno": "pravii_porog_okrasheno", "skol_carapina": "pravii_porog_skol_carapina",
        "vmyatina": "pravii_porog_vmyatina", "corozia_rgavchina": "pravii_porog_corozia_rgavchina",
        "zameneno": "pravii_porog_zameneno",
    },
    "lobovoe_steklo": {
        "name": "Лобовое стекло", "com": "lobovoe_steklo_com", "top": 40, "left": 50,
        "skol": "lobovoe_steklo_skol", "treshina": "lobovoe_steklo_treshina",
        "zapotevanie": "lobovoe_steklo_zapotevanie", "zameneno": "lobovoe_steklo_zameneno",
    },
    "perednee_levoe_steklo": {
        "name": "Переднее левое стекло", "com": "perednee_levoe_steklo_com", "top": 48, "left": 32,
        "skol": "perednee_levoe_steklo_skol", "treshina": "perednee_levoe_steklo_treshina",
        "zapotevanie": "perednee_levoe_steklo_zapotevanie", "zameneno": "perednee_levoe_steklo_zameneno",
    },
    "perednee_pravoe_steklo": {
        "name": "Переднее правое стекло", "com": "perednee_pravoe_steklo_com", "top": 48, "left": 68,
        "skol": "perednee_pravoe_steklo_skol", "treshina": "perednee_pravoe_steklo_treshina",
        "zapotevanie": "perednee_pravoe_steklo_zapotevanie", "zameneno": "perednee_pravoe_steklo_zameneno",
    },
    "zadnee_levoe_steklo": {
        "name": "Заднее левое стекло", "com": "zadnee_levoe_steklo_com", "top": 57, "left": 32,
        "skol": "zadnee_levoe_steklo_skol", "treshina": "zadnee_levoe_steklo_treshina",
        "zapotevanie": "zadnee_levoe_steklo_zapotevanie", "zameneno": "zadnee_levoe_steklo_zameneno",
    },
    "zadnee_pravoe_steklo": {
        "name": "Заднее правое стекло", "com": "zadnee_pravoe_steklo_com", "top": 57, "left": 68,
        "skol": "zadnee_pravoe_steklo_skol", "treshina": "zadnee_pravoe_steklo_treshina",
        "zapotevanie": "zadnee_pravoe_steklo_zapotevanie", "zameneno": "zadnee_pravoe_steklo_zameneno",
    },
    "zadnee_steklo": {
        "name": "Заднее стекло", "com": "zadnee_steklo_com", "top": 68, "left": 50,
        "skol": "zadnee_steklo_skol", "treshina": "zadnee_steklo_treshina",
        "zapotevanie": "zadnee_steklo_zapotevanie", "zameneno": "zadnee_steklo_zameneno",
    },
    "perednie_fonari": {
        "name": "Передние фонари", "label": "perednie_fonari", "com": "perednie_fonari_com",
        "lefts": {"top": 16, "left": 62}, "rights": {"top": 16, "left": 38},
        "skol": "perednie_fonari_skol", "treshina": "perednie_fonari_treshina",
        "zapotevanie": "perednie_fonari_zapotevanie", "zameneno": "perednie_fonari_zameneno",
    },
    "zadnie_fonari": {
        "name": "Задние фонари", "label": "zadnie_fonari", "com": "zadnie_fonari_com",
        "lefts": {"top": 82, "left": 40}, "rights": {"top": 82, "left": 60},
        "skol": "zadnie_fonari_skol", "treshina": "zadnie_fonari_treshina",
        "zapotevanie": "zadnie_fonari_zapotevanie", "zameneno": "zadnie_fonari_zameneno",
    },
}


export const power = {
    "stoika_perednya_levaya": {
        "name": "Стойка передняя левая", "com": "stoika_perednya_levaya_com", "top": 39, "left": 25,
        "okrasheno": "stoika_perednya_levaya_okrasheno", "skol_carapina": "stoika_perednya_levaya_skol_carapina",
        "vmyatina": "stoika_perednya_levaya_vmyatina", "corozia_rgavchina": "stoika_perednya_levaya_corozia_rgavchina",
        "zameneno": "stoika_perednya_levaya_zameneno",
    },
    "stoika_centralynaya_levaya": {
        "name": "Стойка центральная левая",
        "com": "stoika_centralynaya_levaya_com",
        "top": 53,
        "left": 20,
        "okrasheno": "stoika_centralynaya_levaya_okrasheno",
        "skol_carapina": "stoika_centralynaya_levaya_skol_carapina",
        "vmyatina": "stoika_centralynaya_levaya_vmyatina",
        "corozia_rgavchina": "stoika_centralynaya_levaya_corozia_rgavchina",
        "zameneno": "stoika_centralynaya_levaya_zameneno",
    },
    "stoika_zadnyaya_pravaya": {
        "name": "Стойка задняя правая",
        "com": "stoika_zadnyaya_pravaya_com",
        "top": 70,
        "left": 75,
        "okrasheno": "stoika_zadnyaya_pravaya_okrasheno",
        "skol_carapina": "stoika_zadnyaya_pravaya_skol_carapina",
        "vmyatina": "stoika_zadnyaya_pravaya_vmyatina",
        "corozia_rgavchina": "stoika_zadnyaya_pravaya_corozia_rgavchina",
        "zameneno": "stoika_zadnyaya_pravaya_zameneno",
    },
    "stoika_centralnaya_pravaya": {
        "name": "Стойка центральная правая",
        "com": "stoika_centralnaya_pravaya_com",
        "top": 53,
        "left": 80,
        "okrasheno": "stoika_centralnaya_pravaya_okrasheno",
        "skol_carapina": "stoika_centralnaya_pravaya_skol_carapina",
        "vmyatina": "stoika_centralnaya_pravaya_vmyatina",
        "corozia_rgavchina": "stoika_centralnaya_pravaya_corozia_rgavchina",
        "zameneno": "stoika_centralnaya_pravaya_zameneno",
    },
    "stoika_zadnaya_levaya": {
        "name": "Стойка задняя левая", "com": "stoika_zadnaya_levaya_com", "top": 70, "left": 25,
        "okrasheno": "stoika_zadnaya_levaya_okrasheno", "skol_carapina": "stoika_zadnaya_levaya_skol_carapina",
        "vmyatina": "stoika_zadnaya_levaya_vmyatina", "corozia_rgavchina": "stoika_zadnaya_levaya_corozia_rgavchina",
        "zameneno": "stoika_zadnaya_levaya_zameneno",
    },
    "stoika_perednaya_pravaya": {
        "name": "Стойка передняя правая",
        "com": "stoika_perednaya_pravaya_com",
        "top": 39,
        "left": 75,
        "okrasheno": "stoika_perednaya_pravaya_okrasheno",
        "skol_carapina": "stoika_perednaya_pravaya_skol_carapina",
        "vmyatina": "stoika_perednaya_pravaya_vmyatina",
        "corozia_rgavchina": "stoika_perednaya_pravaya_corozia_rgavchina",
        "zameneno": "stoika_perednaya_pravaya_zameneno",
    },
    "perednii_longeron_levii": {
        "name": "Передний лонжерон левый",
        "com": "perednii_longeron_levii_com",
        "top": 16,
        "left": 41,
        "okrasheno": "perednii_longeron_levii_okrasheno",
        "skol_carapina": "perednii_longeron_levii_skol_carapina",
        "vmyatina": "perednii_longeron_levii_vmyatina",
        "corozia_rgavchina": "perednii_longeron_levii_corozia_rgavchina",
        "zameneno": "perednii_longeron_levii_zameneno",
    },
    "perednii_longeron_pravii": {
        "name": "Передний лонжерон правый",
        "com": "perednii_longeron_pravii_com",
        "top": 16,
        "left": 59,
        "okrasheno": "perednii_longeron_pravii_okrasheno",
        "skol_carapina": "perednii_longeron_pravii_skol_carapina",
        "vmyatina": "perednii_longeron_pravii_vmyatina",
        "corozia_rgavchina": "perednii_longeron_pravii_corozia_rgavchina",
        "zameneno": "perednii_longeron_pravii_zameneno",
    },
    "chahka_perednaya_levaya": {
        "name": "Чашка передняя левая",
        "com": "chahka_perednaya_levaya_com",
        "top": 22,
        "left": 40,
        "okrasheno": "chahka_perednaya_levaya_okrasheno",
        "skol_carapina": "chahka_perednaya_levaya_skol_carapina",
        "vmyatina": "chahka_perednaya_levaya_vmyatina",
        "corozia_rgavchina": "chahka_perednaya_levaya_corozia_rgavchina",
        "zameneno": "chahka_perednaya_levaya_zameneno",
    },
    "chahka_perednaya_pravaya": {
        "name": "Чашка передняя правая",
        "com": "chahka_perednaya_pravaya_com",
        "top": 22,
        "left": 60,
        "okrasheno": "chahka_perednaya_pravaya_okrasheno",
        "skol_carapina": "chahka_perednaya_pravaya_skol_carapina",
        "vmyatina": "chahka_perednaya_pravaya_vmyatina",
        "corozia_rgavchina": "chahka_perednaya_pravaya_corozia_rgavchina",
        "zameneno": "chahka_perednaya_pravaya_zameneno",
    },
    "motornii_chit": {
        "name": "Моторный щит", "com": "motornii_chit_com", "top": 25, "left": 50,
        "okrasheno": "motornii_chit_okrasheno", "skol_carapina": "motornii_chit_skol_carapina",
        "vmyatina": "motornii_chit_vmyatina", "corozia_rgavchina": "motornii_chit_corozia_rgavchina",
        "zameneno": "motornii_chit_zameneno",
    },
    "centralnii_tonel": {
        "name": "Центральный тоннель", "com": "centralnii_tonel_com", "top": 35, "left": 50,
        "okrasheno": "centralnii_tonel_okrasheno", "skol_carapina": "centralnii_tonel_skol_carapina",
        "vmyatina": "centralnii_tonel_vmyatina", "corozia_rgavchina": "centralnii_tonel_corozia_rgavchina",
        "zameneno": "centralnii_tonel_zameneno",
    },
    "pol": {
        "name": "Пол", "com": "pol_com", "top": 40, "left": 46,
        "okrasheno": "pol_okrasheno", "skol_carapina": "pol_skol_carapina",
        "vmyatina": "pol_vmyatina", "corozia_rgavchina": "pol_corozia_rgavchina",
        "zameneno": "pol_zameneno",
    },
    "chahka_zadnaya_levaya": {
        "name": "Задняя чашка левая", "com": "chahka_zadnaya_levaya_com", "top": 69, "left": 40,
        "okrasheno": "chahka_zadnaya_levaya_okrasheno", "skol_carapina": "chahka_zadnaya_levaya_skol_carapina",
        "vmyatina": "chahka_zadnaya_levaya_vmyatina", "corozia_rgavchina": "chahka_zadnaya_levaya_corozia_rgavchina",
        "zameneno": "chahka_zadnaya_levaya_zameneno",
    },
    "chahka_zadnaya_pravaya": {
        "name": "Задняя чашка правая", "com": "chahka_zadnaya_pravaya_com", "top": 69, "left": 60,
        "okrasheno": "chahka_zadnaya_pravaya_okrasheno", "skol_carapina": "chahka_zadnaya_pravaya_skol_carapina",
        "vmyatina": "chahka_zadnaya_pravaya_vmyatina", "corozia_rgavchina": "chahka_zadnaya_pravaya_corozia_rgavchina",
        "zameneno": "chahka_zadnaya_pravaya_zameneno",
    },
    "pol_bagagnik": {
        "name": "Пол багажника", "com": "pol_bagagnik_com", "top": 76, "left": 50,
        "okrasheno": "pol_bagagnik_okrasheno", "skol_carapina": "pol_bagagnik_skol_carapina",
        "vmyatina": "pol_bagagnik_vmyatina", "corozia_rgavchina": "pol_bagagnik_corozia_rgavchina",
        "zameneno": "pol_bagagnik_zameneno",
    }
}

export const engine = {
    "postor_shumi": {"name": "Посторонние шумы", "com": "postor_shumi_com"},
    "zapotev_techi": {"name": "Запотевание/течи", "com": "zapotev_techi_com"},
    "ravnom_rab": {"name": "Работа ДВС", "com": "ravnom_rab_com"},
    "ur_masla": {"name": "Уровень масла", "com": "ur_masla_com"},
    "ur_ohl_zhidk": {"name": "Уровень жидкости охлаждающей системы", "com": "ur_ohl_zhidk_com"},
    "tr_obsl": {"name": "Обслуживание ДВС", "com": "tr_obsl_com"},
    "sost_priv_rem": {"name": "Состояние приводных ремней", "com": "sost_priv_rem_com"},
    "rabota_kpp": {"name": "Работа КПП", "com": "rabota_kpp_com"},
}

export const rpt_range = {
    "izn_ptk": {"name": "Остаток ПТК", "com": "izn_ptk_com", "range": "izn_ptk_range"},
    "izn_ztk": {"name": "Остаток ЗТК", "com": "izn_ztk_com", "range": "izn_ztk_range"},
    "izn_ptd": {"name": "Остаток ПТД", "com": "izn_ptd_com", "range": "izn_ptd_range"},
    "izn_ztd": {"name": "Остаток ЗТД", "com": "izn_ztd_com", "range": "izn_ztd_range"},
    "izn_rez_leto": {"name": "Остаток летних шин", "com": "izn_rez_leto_com", "range": "izn_rez_leto_range"},
    "izn_rez_zima": {"name": "Остаток зимних шин", "com": "izn_rez_zima_com", "range": "izn_rez_zima_range"},
}

export const rpt_bool = {
    "ur_tormoz_zh": {"name": "Уровень тормозной жидкости", "com": "ur_tormoz_zh_com"},
    "ur_gur_zh": {"name": "Уровень жидкости ГУР", "com": "ur_gur_zh_com"},
    "sost_st_torm": {"name": "Стояночный тормоз", "com": "sost_st_torm_com"},
    "sost_rez": {"name": "Состояние шин", "com": "sost_rez_com"},
    "sost_torm_zh": {"name": "Тормозная жидкость", "com": "sost_torm_zh_com"},
    "stuki_skr": {"name": "Посторонние стуки и скрипы", "com": "stuki_skr_com"},
    "otkl_pr_dvig": {"name": "Прямолинейность движения", "com": "otkl_pr_dvig_com"},
    "luft_rul_uprav": {"name": "Люфты в рулевом управлении", "com": "luft_rul_uprav_com"},
}

export const diagnostic = {
    "read_vin": {"name": "Чтение VIN из ЭБУ", "com": "read_vin_com"},
    "connect_ebu": {"name": "Связь со всеми блоками ЭБУ", "com": "connect_ebu_com"},
    "error_ebu": {"name": "Ошибки в ЭБУ", "com": "error_ebu_com"},
    "number_key_ebu": {"name": "Соответствие прописанных ключей с фактическим", "com": "number_key_ebu_com"},
}

export const electrick = {
    "svet_indik": {"name": "Освещение и индикация", "com": "svet_indik_com"},
    "steklopod": {"name": "Стеклоподъемники", "com": "steklopod_com"},
    "omyvatel": {"name": "Очиститель/омыватель", "com": "omyvatel_com"},
    "el_priv_sal": {"name": "Эл. привод (сидений, зеркал и т.д.)/Подогрев", "com": "el_priv_sal_com"},
    "el_komp_other": {"name": "Прочие электронные компоненты и устройства", "com": "el_komp_other_com"},
    "ispr_srs": {"name": "Система SRS (подушки безопасности)", "com": "ispr_srs_com"},
    "remni_bezopas": {"name": "Ремни безопасности", "com": "remni_bezopas_com"},
    "ispr_ac": {"name": "Система A/C (кондиционер)", "com": "ispr_ac_com"},
}

export const looking = {
    "lufts": {"name": "Люфты (шаровые, рулевые наконечники, тяги, подшипники)", "com": "lufts_com"},
    "silentblocks": {"name": "Состояние сайлентблоков", "com": "silentblocks_com"},
    "tormoz_defects": {"name": "Тормозная система (шланги, трубки, суппорта)", "com": "tormoz_defects_com"},
    "sost_opor": {"name": "Состояние опор (ДВС, КПП, раздаточной КПП, редуктора и т.д.)", "com": "sost_opor_com"},
    "zapotev_tech": {"name": "Запотевания", "com": "zapotev_tech_com"},
    "sost_pylnikov": {"name": "Состояние пыльников", "com": "sost_pylnikov_com"},
    "sost_vyhlop": {"name": "Состояние выхлопной системы", "com": "sost_vyhlop_com"},
    "silov_el": {"name": "Состояние силовых элементов кузова", "com": "silov_el_com"},
}

export const conclusion_expert = {
    "cost_before": {"name": "Стоимость до торга", "type": "text", "validate": "", "humanize": "true"},
    "cost_after": {"name": "Стоимость после торга", "type": "text", "validate": "", "humanize": "true"},
    "cost_estimated": {"name": "Оценочная стоимость", "type": "text", "validate": "", "humanize": "true"},
    "mileage": {"name": "Пробег", "type": "text", "validate": "", "humanize": "true"},
    "category": {"name": "Категория авто", "type": "number", "validate": ""},
}
export const conclusion_expert_textarea = {
    "pluses": {"name": "Плюсы", "required": true, "type": "textarea", "validate": ""},
    "minuses": {"name": "Минусы", "required": true, "type": "textarea", "validate": ""},
    "result_expert": {"name": "Вывод эксперта", "required": false, "type": "textarea", "validate": ""},
}
export const conclusion_expert_checkbox = {
    "recommended": {"name": "Рекомендуем", "validate": ""},
    "published": {"name": "Опубликовать", "validate": ""},
}

let totalFieldsJur = []
for (let item in jur) {
    totalFieldsJur.push(item)
}
totalFieldsJur.push(...['comp_jur_comment'])

let totalFieldsKuzom = []
for (let item in inside) {
    totalFieldsKuzom.push(item)
}

for (let item in power) {
    totalFieldsKuzom.push(item)
}
totalFieldsKuzom.push(...['kuzov_comment'])

let totalFieldsEngine = []
for (let item in engine) {
    totalFieldsEngine.push(item)
}
totalFieldsEngine.push(...['dvig_diag_comment'])

let totalFieldsRpt = []
for (let item in rpt_range) {
    totalFieldsRpt.push(item)
}
for (let item in rpt_bool) {
    totalFieldsRpt.push(item)
}
totalFieldsRpt.push(...['rul_uprav_comment'])

let totalFieldsDiagnostic = []
for (let item in diagnostic) {
    totalFieldsDiagnostic.push(item)
}
totalFieldsDiagnostic.push(...['comp_diag_comment'])

let totalFieldsElectrick = []
for (let item in electrick) {
    totalFieldsElectrick.push(item)
}
totalFieldsElectrick.push(...['elektro_comment'])

let totalFieldsLoking = []
for (let item in looking) {
    totalFieldsLoking.push(item)
}

let totalFieldsBase = []
for (let item in base) {
    totalFieldsBase.push(item)
}

totalFieldsBase.push(...['photo_inspection', 'photo_change', 'photo_views']);

let baseSteps = {
    auto: {
        name: 'Привязка автомобиля',
        show: false,
        groupIndex: 0,
        totalFields: ['vin', 'mileage', 'owners'],
        definedCount: [],
    },
    base: {
        name: 'Общая информация',
        show: false,
        groupIndex: 1,
        totalFields: totalFieldsBase,
        definedCount: []
    },
    jur: {
        name: 'Юридическая проверка',
        show: false,
        groupIndex: 2,
        totalFields: totalFieldsJur.concat(['photo_gibdd', 'photo_fssp', 'photo_reestr_zalog', 'photo_avtokod', 'photo_dopserv', 'jur_comment']),
        definedCount: []
    },
    kuzov: {
        name: 'Кузов',
        show: false,
        groupIndex: 3,
        totalFields: totalFieldsKuzom,
        definedCount: []
    },
    engine: {
        name: 'Двигатель и трансмиссия',
        show: false,
        groupIndex: 4,
        totalFields: totalFieldsEngine,
        definedCount: []
    },
    rpt: {
        name: 'Рулевое управление, подвеска и тормозная система',
        show: false,
        groupIndex: 5,
        totalFields: totalFieldsRpt,
        definedCount: []
    },
    diagnostic: {
        name: 'Компьютерная диагностика',
        show: false,
        groupIndex: 6,
        totalFields: totalFieldsDiagnostic,
        definedCount: []
    },
    electrick: {
        name: 'Электрика и безопасность/отопление и вентиляция',
        show: false,
        groupIndex: 7,
        totalFields: totalFieldsElectrick,
        definedCount: []
    },
    salon: {
        name: 'Состояние салона',
        show: false,
        groupIndex: 8,
        totalFields: ['salon_defects', 'salon_clear', 'sost_salon_comment'],
        definedCount: []
    },
    vin: {
        name: 'Таблички и маркировки Vin',
        show: false,
        groupIndex: 9,
        totalFields: ['photo_vin1', 'photo_vin2', 'photo_vin3', 'photo_vin_table', 'photo_vin_glass', 'photo_auto_number'],
        definedCount: []
    },
    looking: {
        name: 'Осмотр на подъемнике',
        show: false,
        groupIndex: 10,
        totalFields: totalFieldsLoking,
        definedCount: []
    },
    dop: {
        name: 'Дополнительное оборудование',
        show: false,
        groupIndex: 11,
        totalFields: ['dop_oborudovanie_com'],
        definedCount: []
    },
    conclusion_expert: {
        name: 'Заключение эксперта',
        show: false,
        groupIndex: 12,
        totalFields: ['cost_before', 'cost_after', 'cost_estimated', 'category',
            'pluses', 'minuses', 'comment_expert', 'published', 'recommended'],
        definedCount: []
    }
};
let indexSteps = ["auto", "base", "jur", "kuzov", "engine", "rpt",
    "diagnostic", "electrick", "salon", "vin",
    "looking", "dop", "conclusion_expert"];


export default {
    data: function () {
        return {
            step_dict: step_dict,
            steps: baseSteps,
            indexSteps: indexSteps,
            jur: jur,
            inside: inside,
            power: power,
            engine: engine,
            rpt_range: rpt_range,
            rpt_bool: rpt_bool,
            diagnostic: diagnostic,
            electrick: electrick,
            looking: looking,
            conclusion_expert: conclusion_expert,
            conclusion_expert_textarea: conclusion_expert_textarea,
            conclusion_expert_checkbox: conclusion_expert_checkbox,
            base: base
        }
    },
    computed: {
        stepByValue() {
            return (label, value) => {
                try {
                    if (step_dict[value][label] !== undefined) {
                        return step_dict[value][label];
                    } else {
                        return label;
                    }
                } catch (e) {
                    return label;
                }
            }

        }
    },
}