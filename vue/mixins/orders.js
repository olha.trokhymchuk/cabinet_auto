import axios from 'axios';
import funcs from '../mixins/funcs';

export default {
	mixins: [funcs],
	data: function () {
		return {
			isSlidersActive: false,
			sliderHolder: null
		}

	},
	mounted(){
		let self = this;

		$(document).on('keypress', '#change_order_date_of_inspection input', function(e) {
		  if(e.keyCode < 47 || e.keyCode > 57) {
			e.preventDefault();
		  }
	
		  if($(this).attr('name') === 'date'){
			self.dateMask(e);
		  }else{
			self.timeMask(e);
		  }
		 
		});
	  },
	methods: {
		dateMask(e){
			var value = e.target.value;
	  
			if (value.match(/^\d{2}$/) !== null) {
			  e.target.value = value + '.';
			} else if (value.match(/^\d{2}\.\d{2}$/) !== null) {
			  e.target.value = value + '.';
			}
		  },
		  timeMask(e){
			var value = e.target.value;
	  
			if (value.match(/^\d{2}$/) !== null) {
			  e.target.value = value + ':';
			} 
		  },
		  sendForm(e, action){
			e.preventDefault();
	  
			let formData = new FormData();
			let form = $(e.target);
			let data = form.find('input, textarea').not('[name="date"], [name="time"]').serializeArray();
			
			formData.append('csrfmiddlewaretoken', this.csrf);
	  
			if(action === 'change_order_date_of_inspection'){
			  let date = form.find('[name="date"]').val().split('.').reverse().join('-');
			  let time = form.find('[name="time"]').val();
	  
			  data.map((elem) => {
				if(elem.name === 'date_of_inspection'){
				  elem.value = date + ' ' + time + ':00';
				}
			  })
			}
	  
			data.map((elem) => {
			  formData.append(elem.name, elem.value)
			})
	  
			axios.post('/order/api/change_order_criteria/', formData)
			.then( 
				function (response) {
					if(response.data.message === 'Ok'){
						location.reload();
					}
				},
				function (e) {
					form.find('.error-msg').fadeIn(0);
					console.log('e',e)
				}
			)    
		  },
		  openOrderModal(action, id = ''){
			$.fancybox.open($('#' + action));
			$('.error-msg').fadeOut(0);
			if(action === 'change_order_date_of_inspection-' + id){

			  let dateOrder = $('#change_order_date_of_inspection-' + id + ' [name="date_of_inspection"]').val();
			  if(dateOrder !== null && dateOrder.length > 0){
				let date = new Date(dateOrder);
				let dateInput = $('.order-popup__input-block [name="date"]');
				let timeInput = $('.order-popup__input-block [name="time"]');
				var fullDate = this.getFullDate(date);
  
				timeInput.val(this.setTimeZero(date.getHours() - 3) + ':' + this.setTimeZero(date.getMinutes()));
				dateInput.val(fullDate.replaceAll('/', '.'));
			  }
			}
		  },
		// use in order-detail page to add report in proven cars
		addReport(carId, orderId) {
			let url = `/report/add-report-for-auto/${carId}/`;
			if (typeof orderId != "undefined") {
				url = `/report/add-report-for-auto/${carId}/${orderId}/`;
			}
			$.ajax({
				url: url,
				headers: { 'X-CSRFToken': this.csrf ? this.csrf : csrf },
				processData: false,
				contentType: false,
				type: 'POST',
				success: function (data) {
					if (data.redirect) {
						window.location.href = data.redirect;
					} else if (data.errors) {
						console.log(data.errors)
					}
				}
			})
		},

		setExpertToOrder(expertId, expertType) {
			var url = "/order/api/set_expert/" + this.order.id + "/";
			let data = { 'expert': expertId, 'type_expert': expertType };
			console.log('setExpertToOrder data', data)
			return $.ajax({
				url,
				data,
				method: 'PUT',
				xhrFields: { withCredentials: true },
				headers: { "X-CSRFToken": this.csrf ? this.csrf : csrf }
			})
		},
		removeOrderObject(url, obj = null) {
			var self = this;
			var deleteUrl = url;
			if (obj && obj.id) {
				deleteUrl = `${url}/${obj.id}/`
			}
			return new Promise(function (resolve, reject) {
				$.ajax({
					url: deleteUrl,
					type: "POST",
					data: { csrfmiddlewaretoken: self.csrf },
					success: function (response) {
						resolve(response)
					},
					error: function (e) {
						reject(e)
					}
				});
			})
		},
		getOrderStatusClass: function (status) {
			var orderClass = '';
			switch (status) {
				case 'новый':
					orderClass = 'status_new'
					break;
				case 'в очереди':
					orderClass = 'status_waiting'
					break;
				case 'в работе':
					orderClass = 'status_progress'
					break;
				case 'отложен':
					orderClass = 'status_paused'
					break;
				case 'выполнен':
					orderClass = 'status_done'
					break;
				case 'частичный возврат':
				case 'возврат произведен':
				case 'выдан клиенту':
				case 'возврат':
					orderClass = 'status_to_client'
					break;
				case 'отказ':
					orderClass = 'status_renunciation'
					break;
				case 'ложный вызов':
					orderClass = 'fail_call'
					break;
				case 'готов к выдаче':
					orderClass = 'status_ready_extradition'
					break;
				case 'перепроверен':
					orderClass = 'status_recheck'
					break;
				case 'возврат документы получены':
					orderClass = 'returnmoneydoc'
					break;
				default:
					break;
			}
			return {
				[orderClass]: true
			}
		},
		getOrderCost(cost) {
			if (cost) {
				return this.getHumanReadableNumber(cost)
			}
			return 0;
		},
		getOrderPaymentStatus(status_pay) {
			var status_pay_class = '';
			switch (status_pay) {
				case 'оплачен':
					status_pay_class = 'status_payment_full'
					break;
				case 'частично оплачен':
					status_pay_class = 'status_payment_prepaid'
					break;
				case 'не оплачен':
					status_pay_class = 'status_payment_none'
					break;
				default:
					break;
			}
			return {
				[status_pay_class]: true
			}
		},
		initOrderCriteriesSlider() {
			try {
				var windowWidth = window.innerWidth;
				if (windowWidth <= 768) {
					if (this.sliderHolder !== null) return false;
					this.isSlidersActive = true;
					this.sliderHolder = $('.ord__criteria_item_twise');
					this.sliderHolder.on('afterChange', function (event, slick, currentSlide) {
						var sliderMarkup = slick.$slider[0];
						var activeSlide = currentSlide + 1;
						$(sliderMarkup).prev('.ord__criteria_counter-mobile').find('span').text(activeSlide);
					});
					this.sliderHolder.slick({
						dots: true,
						arrows: false,
						infinite: true,
						slidesToShow: 1,
					})
				} else {
					if (this.sliderHolder !== null) {
						this.isSlidersActive = false;
						this.sliderHolder.slick('unslick');
						this.sliderHolder = null;
					}
				}
			} catch (e) { console.error(e) }
		},
		updateOrderCriteriesSlider() {
			this.$nextTick(function () {
				setTimeout(() => {
					if (this.sliderHolder !== null) {
						this.sliderHolder.slick('unslick');
						this.sliderHolder = null;
					}
					this.initOrderCriteriesSlider();
				}, 500)
			})
		},

		isOrderHasTwoCars(podborauto_set = []) {
			return podborauto_set.length > 1;
		},
		onUpdateOrder(event) {
			this.$forceUpdate()
		},


		//check if object is empty
		isEmpty(obj) {
			for (var key in obj) {
				if (obj.hasOwnProperty(key))
					return false;
			}
			return true;
		},
		isOrderHasTwoCars(podborauto_set = []) {
			return podborauto_set.length > 1;
		},
		markOrderAsUnreviewed(id = '') {
			var self = this;
			return new Promise(function (resolve, reject) {
				$.ajax({
					url: '/report/set-unread-report/' + id,
					type: "POST",
					data: { csrfmiddlewaretoken: self.csrf },
					success: function (response) {
						resolve(response)
					},
					error: function (e) {
						reject(e)
					}
				});
			})
		},
		changeExecutorReportInOrder(executorId,reportId,orderId,salonStatus, placeIssuing){
			var self = this;
			return new Promise(function (resolve, reject) {
				$.ajax({
					url: '/to_issue/',
					type: "POST",
					data: { csrfmiddlewaretoken: self.csrf, order:orderId, report:reportId, expert:executorId, place_issuing: placeIssuing, salon: salonStatus },
					success: function (response) {
						resolve(response)
					},
					error: function (e) {
						reject(e)
					}
				});
			})
		},
		changeReportStatusToRecheck(executorId,reportId,orderId){
			var self = this;
			return new Promise(function (resolve, reject) {
				$.ajax({
					url: '/user/api/send_order_to_recheck/',
					type: "POST",
					data: { csrfmiddlewaretoken: self.csrf, order_id:orderId, report_id:reportId, expert_id:executorId },
					success: function (response) {
						resolve(response)
					},
					error: function (e) {
						reject(e)
					}
				});
			})
		}
	}
}
