import axios from 'axios';


export default {
	props: ['manualAction','createReport'],
	data: function() {
	    return {
	      edit_auto_url: '',
	      uploadedFiles: store.uploadedFiles,
	      autoRuFiles: [],
	    }
	},
	watch: {
		'edit_auto_url'(edit_auto_url) {
	      console.log(edit_auto_url);
	    }
	},
  	methods: {
		validateBeforeSubmit(callback) {
		  let vinInput = document.querySelectorAll('[name="vin"]')[0];
		  let self = this;

		  if(vinInput !== undefined){
			let vinUpperCaseValue = vinInput.value.toUpperCase();
			vinInput.value = vinUpperCaseValue;
		  }
		  
	      this.$validator.detach('url');
	      this.$validator.validateAll().then((result) => {
	        if (result) {
	          $("#form").attr('action', self.manualAction);
	          let form = document.getElementById('form');
	          self.addFilesAndSubmit(form, callback);
	          $('#loader-wrapper').show();
	          $('.loade_stage').hide();
	        }
	        this.$validator.attach('url', 'required|url');
	      })
	    },
	    saveAndAddReport(url) {
	      this.edit_auto_url = url;
	      this.validateBeforeSubmit(this.addReport);
	    },
	    addReport(carId, orderId) {
	    	let url = `/report/add-report-for-auto/${carId}/`;

			if(window.location.href.includes('orderId=')){
				orderId = window.location.href.split('=')[1];
			}

	    	if(typeof orderId != "undefined"){
	    		url = `/report/add-report-for-auto/${carId}/${orderId}/`;
	    	}

		    $.ajax({
		       	url: url,
		        headers: {'X-CSRFToken': this.csrf? this.csrf: csrf },
		        processData: false,
		        contentType: false,
		        type: 'POST',
		        success: function(data) {
		          if (data.redirect) {
		            window.location.href = data.redirect;
		          } else if (data.errors) {
		            console.log(data.errors)
		          }
		        }
		    })
	    },
	    addFilesAndSubmit(form, callback) {
	    	let self = this;
	     	let data = new FormData(form);
	     	let csrf_token = this.csrf?this.csrf:csrf
	     	data.append('X-XSRF-TOKEN', csrf_token);
	    	if (this.isPositionChanged) {
	    		this.changePositions();
	    	}
			let url = typeof self.edit_auto_url !== 'undefined' && self.edit_auto_url != '' ? self.edit_auto_url : window.location.pathname;
	      	var config = {
			  headers: {'Accept': 'application/json', 'Cache': 'no-cache',
						'csrfmiddlewaretoken': csrf_token}
			};
	      	axios.post(url, data, config)
			.then(function(response) {
				let data = response.data;
			    	if (data.redirect) {
		            	let photos = self.autoRuFiles.concat(self.files);
		            	self.addPhotos(data.auto_id, photos, function () {
			              	if (callback) {
			                	callback(data.auto_id, data.order_id);
			              	} else {
			                	window.location.href = data.redirect;
			              	}
		            	})
		          	} else if (data.errors) {
						$('#loader-wrapper').hide();
						self.$validator.errors.clear();
			            for (let field in data.errors) {
			              for (let i = 0; i < data.errors[field].length; i++) {
			                self.$validator.errors.add(field, data.errors[field][i], 'server');
			              }
			            }
		          	}
			   })
			  .catch(function(err) {
				    console.log(err);
			  });
	    },
	    addPhotos(autoId, photos, callback) {
	      $('#loader-wrapper').show();
	      $('.loade_stage').show();
	      if ((photos && photos.length === 0) || !photos[0]) {
	        callback();
			if($('.redirect-clicked').length < 0){
				$('#loader-wrapper').hide();
				$('.loade_stage').hide();
			}

	        return;
	      }

	      let self = this;
	      let url = `/auto/add-photo-auto/${autoId}`;

	      (function send(i) {
	        if (i === photos.length) {
	          callback();
	          return;
	        }

	        let data = new FormData();
	        data.append('photo', photos[i].file);
	        data.append('csrfmiddlewaretoken', self.csrf);

	        $.ajax({
	          url: url,
	          data: data,
	          processData: false,
	          contentType: false,
	          type: 'POST',
	          error: function(error) {
	            console.log(error);
	          }
	        }).always(function() {
	          send(i + 1);
	        });
	      })(0);
	    },
	    changePositions() {
	      self = this;

	      this.uploadedFiles.forEach(function(file) {
	        $.ajax({
	          url: `/auto/update-photo-auto-position/${file.id}/${file.sort_key}`,
	          method: 'POST',
	          data : {csrfmiddlewaretoken: self.csrf},
	          success: function(data) {

	          },
	          error: function (error) {
	            console.log(error);
	          }
	        });
	      })

	    },
   	}

}