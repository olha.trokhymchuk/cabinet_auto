import Vue from 'vue';
import Datepicker from 'vuejs-datepicker';
import {ruDate} from 'vuejs-datepicker/dist/locale'
import select2 from '../vue/select2-directive';
import serverValidation from '../mixins/server-validation';
import funcs from '../mixins/funcs';
import ru from "vee-validate/dist/locale/ru";
import executor from '../components/executor';

VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});


var app = new Vue({
  delimiters: ['${', '}'],
  el: '#order-edit',
  mixins: [funcs, serverValidation],
  data: {
    client: 0,
    state: globalState,
    order_id: order_id,
    order: {},
    filial: globalState.filial,
    csrf: csrf,
    start: date_start,
    end: date_end,
    date_end_start: date_start,
    date_end_end: date_end,
    datepickerLang: ruDate,
    startDatepickerState: {
      disabled: {
        to:  null,
        from: null
      }
    },
    endDatepickerState: {
      disabled: {
        to:  null,
        from: null
      }
    }, 
    date_end_startDatepickerState: {
      disabled: {
        to:  null,
        from: null
      }
    },
    date_end_endDatepickerState: {
      disabled: {
        to:  null,
        from: null
      }
    },
    cost: typeof globalState.cost !=='undefined' ? globalState.cost: '',
    paid: typeof globalState.paid !=='undefined' ? globalState.paid: '',
  },
  components: {
    Datepicker,
    executor

  },
  directives: {
    select2: select2
  },
  mounted: function() {
    console.log("this.start");
    console.log(this.start);
    if (this.order_id) {
      let self = this;
      $.ajax({
        cache: false,
        method: 'GET',
        url: `/order/detail/${this.order_id}`,
        success: function (data) {
          self.order = data;
          let $newOption = $("<option></option>").val(self.order.client.id).text(self.order.client.full_name)
          $('#client').append($newOption).trigger('change');
        },
        error: function (error) {
          console.log(error);
        }
      });
    }

    $('select').select2({});
    this.filial = $('#filial').val();
    this.endDatepickerState.disabled.to = this.start;
  },
  watch: {
    filial: function(newFilial) {
      this.state.filial = this.filial;
      this.loadToSelect2('#client', '/user/user-list/', {groups__name: 'Клиент', filials__id: newFilial}, 'id', 'full_name');
    },
    'cost'(newValue) {
      this.changeHandler(newValue, 'cost');
    },
    'paid'(newValue) {
      this.changeHandler(newValue, 'paid');
    },
  },
  methods: {
    addExecutor() {
      this.state.newExecutors.push({id: Math.floor(Math.random() * 1000000)});
    },
    onChangeDatepickerStart(newDate) {
      this.endDatepickerState.disabled.to = newDate;
    },
    onChangeDatepickerEnd(newDate) {
      this.startDatepickerState.disabled.from = newDate;
    },
    onChangeDatepickerDate_end_End(newDate) {
      this.date_end_endDatepickerState.disabled.to = newDate;
    },
    onChangeDatepickerDate_end_Start(newDate) {
      this.date_end_startDatepickerState.disabled.from = newDate;
    },
    validateBeforeSubmit(e) {
      this.checkStatusValidate();

      this.$validator.validateAll().then((result) => {
        if (result) {
          var formElement = document.querySelector("form");
          var formData = new FormData(formElement);

          for (var key in this.state.executors) {
            if (this.state.executors[key].role && this.state.executors[key].user)
              formData.append(this.state.executors[key].role, this.state.executors[key].user)
          }

          formData.append('csrfmiddlewaretoken', this.csrf);
          var url = $("#main-form").attr('action');
          this.submit(url, formData);
        }
      })
    },
    onRemoveExecutor(source, i) {
      this.order[source].splice(i, 1);
    },
    onRemoveNewExecutor(i) {
      this.state.newExecutors.splice(i, 1);
    },
    checkStatusValidate(){
      var selected = $('select[name="status"] option:selected');
      var selectedText = selected.text();
      var validArr = ['возврат', 'возврат документы получены', 'возврат произведен'];
      var $this = this;
      var validArrChecked = validArr.every(function(elem){
        return elem != selectedText
      });

      this.$validator.reset();

      if(validArrChecked && selectedText != 'отказ'){
        removeRequired(['reason_refuse', 'refund_amount']);
      }else if(selectedText == 'отказ'){
        removeRequired(['refund_amount']);
      }else{
        removeRequired(false);
      }

      function removeRequired(inputName){
        $this.$validator.fields.items.forEach(function(elem){
          if(Array.isArray(inputName)){
            inputName.forEach(function(name){
              if(name == elem.name){
                elem.rules = '';
              }
            });
          }else{
            elem.rules = {required: []};
          }
        });
      }
    }
  },
  created: function(){
    var $this = this;
    $(document).on('change', '[name="status"]', () => {
      this.$validator.clean();
    });
  }
})


