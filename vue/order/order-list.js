import Vue from 'vue';
import ru from "vee-validate/dist/locale/ru";
import VModal from 'vue-js-modal';


Vue.use(VModal);
VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});

import list from '../mixins/list';
import search from '../mixins/search';
import funcs from '../mixins/funcs';
import orders from '../mixins/orders';
import DateRange from '../components/date-range';
import dropdown from '../components/dropdown';
import {OrderChangeExecutor} from './order-detail-components'

import set_expert from '../components/set_expert';
import executor from '../components/executor';
import filter from './filter';
import axios from "axios";


const isFilterExist = document.getElementById('filter-list')

Vue.component('filter-component', {
  delimiters: ['${', '}'],
  template: '#filter-component',
  mixins: [filter],
  props: [],
})


if(document.getElementById('filter-list')){
  var reports = new Vue({
    delimiters: ['${', '}'],
    el: '#filter-list',
  })
}

var app = new Vue({
  delimiters: ['${', '}'],
  el: '#orders',
  mixins: [list, search, funcs,orders],
  components: {
    'date-range': DateRange,
    'order-change-executor': OrderChangeExecutor,
    dropdown,
    set_expert,
    executor
  },
  data: {
    deleteUrl: '/order/delete',
    closeUrl: '/order/close',
    order: {},
  },
  mounted(){
    this.$nextTick(function() {
      window.addEventListener('resize', this.initOrderCriteriesSlider);
    })
  },
  destroyed(){
    window.removeEventListener('resize', this.initOrderCriteriesSlider);
  },
  watch: {
    objects: function (newObjects, oldObjects) {
      if(this.objects && newObjects.length > 0){
        //new data is loaded
        this.updateOrderCriteriesSlider();
      }
    }
  },

  methods: {
    getUserFullName(user) {
      return `${user.first_name} ${user.last_name}`;
    },
    changedExpert() {
      this.updateObjects();
    },
    changeAutoFrom(id) {
      const self = this;
      console.log(id);
      $.ajax({
        url : `/order/change_auto_from/${id}/`,
        type: "POST",
        data : {csrfmiddlewaretoken: this.csrf},
        success: function(response) {
          console.log(response);
          self.updateObjects();
          console.log("changeAutoFrom");
        }
      });
    },
  }
})




