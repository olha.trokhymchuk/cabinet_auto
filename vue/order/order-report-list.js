// OLD FUNCTIONS ! - should be removed later

import Vue from 'vue';
import list from '../mixins/list';
import funcs from '../mixins/funcs';
import search from '../mixins/search';


var reports = new Vue({
  delimiters: ['${', '}'],
  el: '#reports',
  mixins: [list, funcs],
  data: {
    deleteUrl: '/order/report-remove-order'
  },
  methods: {
    getReportName(report) {
      if(typeof report === "undefined" || typeof report.auto === "undefined"){
          return "";
      }
      return `${report.auto.mark_auto.name} ${report.auto.model_auto.name} ${report.auto.generation.name} ${report.auto.vin}`;
    }
  }
});
