import Vue from 'vue';
import list from '../mixins/list';


new Vue({
  mixins: [list],
  delimiters: ['${', '}'],
  el: '#box',
  data: {
    newComment: '',
  },
  methods: {
    addComment() {
      if (!this.newComment)
        return

      let url = `/order/add-comment/${order_id}`;
      let data = new FormData();
      let self = this;

      data.append('body', this.newComment);
      data.append('csrfmiddlewaretoken', this.csrf);

      $.ajax({
        url: url,
        processData: false,
        contentType: false,
        type: 'POST',
        data: data,
        success: function(data) {
          self.updateObjects();
          if (data.redirect) {
            window.location.href = data.redirect;
          } else if (data.errors) {
            console.log(data.errors)
          }
        }
      }).always(function () {
        self.this.newComment = '';
      })
    }
  }
})
