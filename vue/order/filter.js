import Vue from 'vue';
import Datepicker from 'vuejs-datepicker';
import {ru} from 'vuejs-datepicker/dist/locale'
import select2 from '../vue/select2-directive';
import filterMixin from '../mixins/filter';

//   /Users/sheepfish/Documents/avtopodbor_new/node_modules/vuejs-datepicker/dist/vuejs-datepicker.js

Vue.directive('select', {
    twoWay: true,
    bind: function (el, binding, vnode) {
        $(el).select2().on("select2:select", (e) => {
            el.dispatchEvent(new Event('change', { target: e.target }));
        });
    },
});
export default {
  delimiters: ['${', '}'], 
  mixins: [filterMixin],
  data() {
    return {
      filterTitle: 'Фильтры',
      filials: [],
      order_types: [],
      statuses: [],
      clients: [],
      executors: [],
      filial: '',
      order_type: '',
      status: '',
      executor: '',
      client: '',
      start: '',
      end: '',
      datepickerLang: ru,
      date_end_start: '',
      date_end_end: '',
      closing: false,
      payed: false,
      dates: {init: [], options0: [], options1: [], select0: 0, select1: 0},
      filterFields: ['filial', 'order_type', 'status', 'executor', 'client', 'start', 'end','date_end_start', 'date_end_end','status_pay', 'status_closing'],
      startDatepickerState: {
        disabled: {
          to:  null,
          from: null
        }
      },
      endDatepickerState: {
        disabled: {
          to:  null,
          from: null
        }
      },
      date_end_startDatepickerState: {
        disabled: {
          to:  null,
          from: null
        }
      },
      date_end_endDatepickerState: {
        disabled: {
          to:  null,
          from: null
        }
      },
      openFilial: false,
      openSort: false,
      openStatus: false,
      sortkey:'',
      openCalendarOne: false,
      openCalendarTwo: false,
      openCalendarThree: false,
      openCalendarFour: false,
    }
  },
  mounted: function() {
    let self = this;

    setTimeout(() => {
      self.loadInitial();
    }, 300)

    this.endDatepickerState.disabled.to = this.start;
  },
  computed: {
    status_pay: function() {
      return this.payed ? true : '';
    },
    status_closing: function() {
      return this.closing ? true : false;
    }
  },
  components: {
    Datepicker,
  },
  directives: {
    select2: select2
  },
  methods: {
    statusOpen(status){
      this.openStatus = status;
    },
    filialOpen(status){
      this.openFilial = status;
    },
    sortOpen(status){
      this.openSort = status;
    },
    mChangeSort(sort){
      this.sortkey = sort;
      this.openSort = false;
        $('select').select2('destroy');
        setTimeout(function () {
        $('select').select2();
        },200);
      },
    mChangeFilial(filial){
      this.filial = filial;
      this.openFilial = false;
        $('select').select2('destroy');
        setTimeout(function () {
        $('select').select2();
        },200);
      },
    mChangeStatus(status){
      this.status = status;
      this.openStatus = false;
        $('select').select2('destroy');
        setTimeout(function () {
        $('select').select2();
        },200);
      },
    onChangeDatepickerStart(newDate) {
      this.openCalendarOne = false;
      this.start = `${newDate.getFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`;
      this.endDatepickerState.disabled.to = newDate;
    },
    onChangeDatepickerEnd(newDate) {
      this.openCalendarTwo = false;
      this.end = `${newDate.getFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`;
      this.startDatepickerState.disabled.from = newDate;
    },
    onChangeDatepickerDate_end_Start(newDate) {
      this.openCalendarThree = false;
      this.date_end_start = `${newDate.getFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`;
      this.date_end_endDatepickerState.disabled.to = newDate;
    },
    onChangeDatepickerDate_end_End(newDate) {
      this.openCalendarFour = false;
      this.date_end_end = `${newDate.getFullYear()}-${newDate.getMonth() + 1}-${newDate.getDate()}`;
      this.date_end_startDatepickerState.disabled.from = newDate;
    },
    loadInitial() {
      console.log('test loadInitial');
      var self = this;
      $('body').css('overflow','hidden');
      $.ajax({
        url: '/order/filter/' + this.uri,
        cache: false,
        method: 'GET',
        success: function (data) {
          var url = new URL(window.location.href);
          self.filials = data.filials;
          self.order_types = data.order_types;
          self.statuses = data.statuses;
          self.clients = data.clients;
          self.executors = data.executors;
          self.loading = false;
      $('body').css('overflow','visible');
        },
        error: function (error) {
          self.loading = false;
      $('body').css('overflow','visible');
          console.log(error);
        }
      });
    },
    afterFilterChange() {
      this.loadInitial();
    }
  }
}
