import Vue from 'vue';
import list from '../mixins/list';
import funcs from '../mixins/funcs';
import auto from '../mixins/auto-mix';


var cars = new Vue({
  delimiters: ['${', '}'],
  el: '#cars',
  mixins: [list, auto, funcs],
  data: {
  	order_id: order_id?order_id:'',
    deleteUrl: `/order/delete-auto/${order_id}`
  },
})
