import Vue from 'vue';
import funcs from '../mixins/funcs';
import orders from '../mixins/orders';
import dropdown from '../components/dropdown';
import {OrderDetailCriteria,OrderChangeExecutor} from './order-detail-components';

import axios from 'axios';

Vue.config.devtools = true

var orderDetail = new Vue({
  delimiters: ['${', '}'],
  el: '#order-detail',
  mixins: [funcs,orders],
  components: {
    dropdown,
    OrderDetailCriteria,
    'order-change-executor': OrderChangeExecutor,
  },
  data: {
    csrf: csrf,
    order_id: order_id,
    order_path: order_path,
    deleteAutoUrl: `/order/delete-auto/${order_id}`,
    deleteCriteriesUrl: '/podbor-delete',

    loading:true,
    isError:false,
    orderMarkupApplied:false,
    order:null,
  },
  mounted(){
    this.getOrderDetail();
    this.$nextTick(function() {
      window.addEventListener('resize', this.initCriteriesMobileSlider);
    })
  },
  destroyed(){
    window.removeEventListener('resize', this.initCriteriesMobileSlider);
  },
  updated(){
    //check when vue applied new data to markup
    if(this.orderMarkupApplied){
      this.setEqualHeightToCriteries();
      this.initProvenCarSlider();
      this.initCriteriesMobileSlider();
      this.detectAnchorLink();
      this.orderMarkupApplied = false;
      this.initSuggestions();
    }
  },
	methods: {
    googleRequestPhotoId(requestIds){

      var self = this;
      $.ajax({
        url: '/google_photo/reports-cover-photo/',
        data : {
          csrfmiddlewaretoken: this.csrf,
          requestIds: JSON.stringify(requestIds),
        },
        method: 'POST',
        success: function (data) {
          let key;
          for(key in data){
           self.order.reportpodbor_set.map( function(item) {
             if(item.id == key){
               item.auto.image = data[key]
             }
          })
          }
        },
        error: function (error) {
          self.loading = false;
          console.log(error);
        }
      });
    },
      initSuggestions(){
        $("#address").suggestions({
            token: "538ba4aeeab0a7ad5845959de640c77d47f6ccd0",
            type: "ADDRESS",
            /* Вызывается, когда пользователь выбирает одну из подсказок */
            onSelect: function(suggestion) {
            }
        });
      },
		getOrderDetail(){
      let requestPhotoId = [];
      var instance = this;
			$.ajax({
        cache: false,
        method: 'GET',
        url: order_path,
        success: function(data) {
          // console.log(data)
          instance.order = data;
          instance.orderMarkupApplied = true;
          instance.order.reportpodbor_set.map( (item)=> {
                requestPhotoId.push(item.id)
          })

          if(instance.order.date_of_inspection !== undefined && instance.order.date_of_inspection !== null){
            let date = new Date(instance.order.date_of_inspection);
            var fullDate = instance.getFullDate(date);

            instance.order.date_of_inspection_converted = fullDate.replaceAll('/', '.') + ' в ' + instance.setTimeZero(date.getHours() - 3) + ':' + instance.setTimeZero(date.getMinutes())
          }

          if( requestPhotoId.length != 0){
            instance.googleRequestPhotoId(requestPhotoId)
          }
      },
        error: function(error) {
          instance.isError = true;
          console.error(error);
        },
        complete: function(){
          instance.toggleLoadingPage();
        }
      });
    },
    //hide/show preloader
    toggleLoadingPage(){
      this.$refs.loadingStub.classList.toggle('active')
    },
    getTranslatedDate(date) {
      try {
        if (!date) return '';
        var dateTranslated = date.split('-').reverse();
        if (dateTranslated.lenth < 3) return '';
        var availableMonths = ["января", "февраль", "марта", "апреля", "мая", "июня", "июля", "августа", "сентября", "октября", "ноября", "декабря"];

        var day = dateTranslated[0];
        var month = availableMonths[parseFloat(dateTranslated[1]) - 1] //get moth
        var year = dateTranslated[2];
        return `${day} ${month} ${year} г.`;
      } catch (error) {
        return ''
      }
    },
    getNormalCarHorsePower(horsepower){
      var horsepowerObj = horsepower;
      if(typeof horsepowerObj === 'string'){
        horsepowerObj = JSON.parse(`${horsepowerObj}`)
      }
      return horsepowerObj;
    },
    removeOrder(id) {
      var isConfirmedAction = confirm('Вы уверены что хотите удалить заказ ?')
      if(isConfirmedAction){
        self = this;
        $.ajax({
          url : `/order/delete/${id}/`,
          type: "POST",
          data : {csrfmiddlewaretoken: this.csrf},
          success: function() {
            window.location = '/order/list';
          }
        });
      }
    },
    setEqualHeightToCriteries(){
      try{
        if($(window).width() >= 768){
          var $holder = $('[data-criteries-holder]');
          if($holder.children().length > 1){
            var $first = $($holder.children()[0]);
            var $second =  $($holder.children()[1]);
            $first.children('[data-criteria-row]').each((i,item) => {
              var $item = $(item);
              var $oppositeItem = $second.children('[data-criteria-row]').eq(i);
              var maxHeight = $item.outerHeight() > $oppositeItem.outerHeight() ? $item.outerHeight() : $oppositeItem.outerHeight();
              $oppositeItem.css('min-height',maxHeight)
              $item.css('min-height',maxHeight)
            })
          }
        }
      }catch(e){
        console.log(e)
      }
    },
    pressOnToggleBlockSection(e){
      var $target = $(e.target);
      var $holder = $target.closest('[data-block-holder]');
      var $body = $holder.find('[data-block-body]');
      $holder.toggleClass('hidden');
      $body.slideToggle();
    },
    pressOnPopupBlockSection(e){
      var $target = $(e.target);
      var $holder = $target.closest('[data-block-holder]');
      if(!$holder.hasClass('active-popup')){
        $('html,body').animate({scrollTop: 0}, 500);
      }
      $holder.toggleClass('active-popup');
    },
    pressOnRemoveReport(report,e){
      //func from orders mixin
      var that = this;
      var isConfirmedAction = confirm('Вы уверены что хотите удалить отчет ?')
      if(isConfirmedAction){
        let headers = {
          "X-CSRFToken": this.csrf,
          'Content-Type': 'multipart/form-data'
        };
        let formData = new FormData();
        formData.append("data", JSON.stringify({"order_id": this.order_id}));
        console.log(headers);
        const url = '/order/report-remove-order/' + report.id + '/'+ this.order_id + '/';
        axios.post(url, formData, {withCredentials: true, headers})
          .then(
            function (response) {
              var $target = $(e.target);
              var $itemHolder = $target.closest('[data-proven-car-holder]');
              var $item = $target.closest('[data-proven-car-item]');
              var index = $item.attr("data-slick-index");
              console.log(that.order.reportpodbor_set);
              var indexInCarArray = that.order.reportpodbor_set.findIndex(x => x.id === report.id);

              $itemHolder.slick('slickRemove', index, false).slick('refresh');
              that.$delete(that.order.reportpodbor_set, indexInCarArray)
            },
            function (e) {
              console.log('e',e)
            }
          )
      }
    },
    pressOnMarkAsUnreviewed(car,e){
      //func from orders mixin
      var that = this;
      $(e.target).parents('.dropdown').toggleClass('dropdown_open');
      this.markOrderAsUnreviewed(car.id)
        .then(
          function (response) {
            car.has_non_read_reports = true;
          },
          function (e) {
            console.log('e',e)
          }
        )
    },
    pressOnRemoveCriteriesCar(car){
      var isConfirmedAction = confirm('Вы уверены что хотите удалить ?')
      if(isConfirmedAction){
      var that = this;
        this.removeOrderObject(this.deleteCriteriesUrl,car)
          .then(
            function (response) {
              var indexInCarArray = that.order.podborauto_set.findIndex(x => x.id === car.id);
              that.$delete(that.order.podborauto_set, indexInCarArray)
            },
            function (e) {
              console.log('e',e)
            }
          )
      }
    },
    initProvenCarSlider(){
      var $holder = $('[data-proven-car-holder]');
      $holder.slick({
        dots: true,
        arrows: false,
        infinite: false,
        slidesToShow: 3,
        //autoplay: true,
        //autoplaySpeed: 4000,
        responsive: [
          {
            breakpoint: 960,
            settings: {
              slidesToShow: 2
            }
          },
          {
            breakpoint: 666,
            settings: {
              slidesToShow: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1.05
            }
          }
        ]
      });
    },
    initCriteriesMobileSlider(){
      try{
        var windowWidth = window.innerWidth;
        var sliderHolder = $('[data-criteries-holder]');
        if(windowWidth <= 768){
          if(!sliderHolder.hasClass('slick-initialized')){
            sliderHolder.slick({
              dots: true,
              arrows: false,
              infinite: true,
              slidesToShow: 1
            })
          }
        }else {
          if(sliderHolder.hasClass('slick-initialized')){
            sliderHolder.slick('unslick');
            this.setEqualHeightToCriteries();
          }
        }
      }catch(e){console.error(e)}
    },
    detectAnchorLink(){
      try {
        if(window.location.hash) {
          var hash = window.location.hash;
          if($(hash).length){
            $('html,body').animate({scrollTop: $(hash).offset().top},300);
          }
        }
      } catch (error) {console.error(error)}
    },
    generateProvenCarsLink(car, order) {
      let link = '';
      try {
        if (car && car.last_report_id) {
          link = '/report/view/' + car.last_report_id
        } else if (car && order) {
          link = '/auto/detail-auto/' + car.vin + '?order=' + order.id
        }
      } catch (error) {}
      return link;
    },
    pressOnReportChangeExecutor(reportObj,callback) {
      var that = this;
      var selectString = '#order-change-executor-select'
      var selectRef = $(selectString);
      var orderFilialId = this.order.filial ? this.order.filial.id : null;

      $.fancybox.open({
        src: "#order-report-to-issue",
        opts: {
          touch: false,
          afterShow: function (instance, current) {
            //init plugin select2 and load ecsperts
            that.loadToSelect2(
              selectString,
              '/user/user-list/',
              {groups__name: 'Эксперт', filials__id: orderFilialId},
              'id',
              'full_name',
              [],
              // select2 config
              {
                dropdownParent: $("#order-report-to-issue")
              }
            );
            selectRef.select2('open');//opent select initially
            //when user chooise from select
            selectRef.on('select2:select',(e) => {
              //callback when user choose from select
              reportObj.salon = $('.salon-checked').prop( "checked" );
              reportObj.place_issuing = $('.place_issuing').val();
              callback(e,reportObj)

              $.fancybox.close();
              $('.dropdown').removeClass('dropdown_open')
              location.reload();
            })
          },
          beforeClose: function (instance, current) {
            //destroy slect2 plugin
            selectRef.select2("destroy");
            selectRef.off('select2:select');
          },
        }
      })
    },
    onUpdateReportChangeExecutor(e,report){
      var dataFromSelect = e.params.data;
      var {id:executorId,text:executorFullName} = dataFromSelect;
      this.changeExecutorReportInOrder(executorId,report.id,this.order.id, report.salon, report.place_issuing)
        .then(response => {
          // select exprt on handing to page
          if(response.success === 'true' && this.order.transferring){
            this.order.transferring.length = 0;
            this.order.transferring.push({id:executorId,full_name:executorFullName})
          }
        })
    },
    onUpdateReportReCheck(e,report){
      var dataFromSelect = e.params.data;
      var {id:executorId} = dataFromSelect;
      //func from mixin
      this.changeReportStatusToRecheck(executorId,report.id,this.order.id)
        .then(response => {
          if(response.message === 'ok'){
            this.order.reportpodbor_set.forEach(elem => {
              if(elem.id === report.id){
                elem.in_recheck = true;
                this.order.status = 'перепроверен';//update status to order
              }
            })
          }
        })
    }
  },
  computed:{
    getPodborAvtoFromOrder(){
      var podborauto = [];
      if(this.order){
        var podborauto_set = [...this.order.podborauto_set];
        if(podborauto_set.length === 1){
          podborauto = podborauto_set
        }else {
          podborauto = podborauto_set.splice(0,2)

        }
      }
      return podborauto
    }
  }
})

var app = new Vue({
  delimiters: ['${', '}'],
  el: '#order-popup',
  data() {
      return{
        testVue:'true'
      }
    },
  methods: {
    validateBeforeSubmit(e) {
       e.preventDefault()
        var radios = document.querySelectorAll('input[type="radio"]:checked');
        var rating = radios.length>0? radios[0].value: null |'';
    $.ajax({
        type:'POST',
        url: "/user/review_create/",
        data:{
            order_id: order_id,
            title:$('#title').val(),
            text:$('#text').val(),
            rating:rating,
            csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val(),
            action: 'post'
        },
        success:function(json){
            document.getElementById("post-form").reset();
            $( "#post-form" ).hide();
            $('#signupServiceRating').prepend('<div class="popup-text"> <h2>Спасибо за Ваш отзыв!</h2> </div>');
        },
        error : function(xhr,errmsg,err) {
        console.log(xhr.status + ": " + xhr.responseText); // provide a bit more info about the error to the console
    }
    });
    },
  }
})