import Vue from 'vue';
import funcs from '../mixins/funcs'
import orders from '../mixins/orders'

const OrderDetailCriteria = Vue.component('order-detail-criteria', {
	props: {
		title: String,
		head:{
			type: Boolean,
			default: false
		}
	},
	template: `
    <div class="ord-detail__criteries_row" :class="{'header no-hover':head}" data-criteria-row >
		<div class="ord-detail__criteries_title">{{title}}</div>
		<div class="ord-detail__criteries_text">
			<slot></slot>
		</div>
    </div>
	`
})

var clickOutside = Vue.directive('click-outside', {
	bind: function (el, binding, vnode) {
		el.clickOutsideEvent = function (event) {
			// here I check that click was outside the el and his childrens
			if (!(el == event.target || el.contains(event.target))) {
				// and if it did, call method provided in attribute value
				vnode.context[binding.expression](event);
			}
		};
		document.body.addEventListener('click', el.clickOutsideEvent)
	},
	unbind: function (el) {
		document.body.removeEventListener('click', el.clickOutsideEvent)
	},
})

const OrderChangeExecutor = Vue.component('order-change-executor', {
	props: ['order','type',],
	mixins: [funcs,orders],
	directives: {
	  	clickOutside
	},
	data () {
		return {
			visualState: false,
			selectRef:null,
			selectSelector:'#order-change-executor-select',
		}
	},
	methods: {
		getSelectData(){
			//load data to select,open select,bind change event
			var filial = this.order.filial ? this.order.filial.id : null;
			var group = this.type === 'operator' ? 'Оператор' : 'Эксперт';

			this.loadToSelect2(this.selectSelector, '/user/user-list/', {groups__name: group, filials__id: filial}, 'id', 'full_name');
			this.selectRef.select2('open');
			this.selectRef.on('select2:select',this.selectNewExecutor)
			setTimeout(() => {
				$('.select2-search__field').attr('autofocus', 'autofocus').focus();// double check
			}, 200)
		},
		selectNewExecutor(e){
			var data = e.params.data;
			var {id,text:full_name} = data;
			var oldExecutor = this.order[this.type].length ? this.order[this.type][0] : null;
			var clonedOrder = Object.assign({},this.order);
			var self = this;

			clonedOrder[this.type].length = 0;
			clonedOrder[this.type][0] = {id:null,full_name:'Загрузка...'}
			//emit to parent component
			this.$emit('update:order', clonedOrder)
			this.sendNewExecutorToApi({id,full_name})
				.then(
					function(succsess){
						//set new executor
						clonedOrder[self.type][0] = {id,full_name}
						self.$emit('update:order', clonedOrder)
					},
					function(error){
						//set prev executor
						oldExecutor ? clonedOrder[self.type][0] = oldExecutor : clonedOrder[self.type].length = 0;
						self.$emit('update:order', clonedOrder)
					}
				)
			this.hide();
		},
		sendNewExecutorToApi({id,full_name}){
			var self = this;
			return new Promise(function (resolve,reject){
				return self.setExpertToOrder(id,self.type)
					.done(function(success) {
						resolve(success);
					})
					.fail(function(error) {
						reject(error)
					})
			})
		},
		resetSelect(){
			if(this.selectRef && this.selectRef.hasClass("select2-hidden-accessible")){
				this.selectRef.off('select2:select');
				this.selectRef.select2("destroy");
			}
			this.selectRef = null;
			this.visualState = false;
		},
		show: function () {
			this.visualState = true
		},
		hide: function () {
			this.visualState = false
		},
		onClickOutside (event) {
			this.hide();
		}
	},
	watch: {
		visualState() {
			if (this.visualState){
				document.addEventListener('click', this.visualSwitch);
				//check when markup upplie to dom
				setTimeout(() => {
					//add select ref
					this.selectRef = $(this.selectSelector);
					this.getSelectData();
				}, 100)
			}else {
				//reset states
				this.resetSelect();
			}
		},
	},
	template: `
		<div class="order-change-executor" :class="{'active':visualState}" >
			<div class="order-change-executor__button plus-icon mini" @click.stop="show">
				<div class="icon icon_m">
					<svg><use xlink:href="#icon_plus_24"></use></svg>
				</div>
			</div>
			<div class="order-change-executor__select" v-if="visualState" >
				<select id="order-change-executor-select" ></select>				
				<div class="order-change-executor__bck" @click="onClickOutside"></div>
			</div>
		</div>
	`
})

export {
	OrderDetailCriteria,
	OrderChangeExecutor,
}