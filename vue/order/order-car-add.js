import Vue from 'vue';
import carEditForm from '../auto/auto-edit-form-component';
import searchCar from '../auto/search-car-component';


function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}


var app = new Vue({
  delimiters: ['${', '}'],
  el: '#order',
  data: {
    selected: store.selectedCars,
    csrf: csrf,
    store: store,
    carEditVisible: store.carEditVisible,
  },
  components: {
    carEditForm,
    searchCar,
  },
  watch: {
    'store.carEditVisible'(newValue) {
      this.carEditVisible = newValue;
    }
  },
  methods: {
    addCarsToForm(form) {
      var data = new FormData(form);
      var selectedId = [];
      const selectedCars = this.$refs.searchCar.selected;
      for (var i = 0; i < selectedCars.length; i++)
        selectedId.push(selectedCars[i].id)

      data.append('cars', JSON.stringify(selectedId));
      data.append('csrfmiddlewaretoken', this.csrf);
      $('#loader-wrapper').show();
      $.ajax({
        url: $("#main-form").attr('action'),
        data: data,
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data) {
          if (data.redirect !== undefined)
            location.href = data.redirect;
            $('.section__body').hide();
        },
        error: function(error) {
          $('.section__body').hide();
          console.log(error);
        }
      });
    }
  }
})
