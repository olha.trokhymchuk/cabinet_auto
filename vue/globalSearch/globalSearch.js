import Vue from 'vue';

import list from '../mixins/list';
import search from '../mixins/search';
import funcs from '../mixins/funcs';
import orders from '../mixins/orders';

import DateRange from '../components/date-range';
import dropdown from '../components/dropdown';
import { OrderChangeExecutor } from '../order/order-detail-components'

Vue.config.devtools = true
//get search query params from window.location obj
const getSearchQueryFromUrl = () => {
	const params = new Map(location.search.slice(1).split('&').map(kv => kv.split('=')))
	return params
}

const initialGetSearchFromUrl = () => {
	try {
		const querySearch = getSearchQueryFromUrl().get('query') || '';
		const querySearchEncoded = decodeURI(querySearch);
		return querySearchEncoded;
	} catch(e) {
		console.error(e);
	}
}

var globalSearch = new Vue({
	delimiters: ['${', '}'],
	el: '#global-search',
	mixins: [list, search, funcs, orders],
	components: {
		'date-range': DateRange,
		'order-change-executor': OrderChangeExecutor,
		dropdown
	},
	data: {
		searchApi: '/user/api/search/',
		csrf: csrf,
		isLoading: true,
		isError: false,
		querySearch: initialGetSearchFromUrl(),
		querySearchAjax: null,
		timeout: null,

		orders: [],
		reports: [],
		users: [],
		reviews: [],
	},
	mounted() {
		this.getGlobalSearchData()
		//set focus on search input when page load
		//this.$nextTick(() => this.$refs.searchInputRef.focus());
	},
	methods: {
		//hide/show preloader
		isEmptySearch: function () {
			const isEmpty = this.orders.length === 0 && this.reports.length === 0 && this.users.length;
			return isEmpty === 0;
		},
		setHistoryPush(){
			let querySearch = this.querySearch.trim();
			if(!querySearch) return;
			const tabTitle = $(document).find("title").text();
			querySearch = `?query=${querySearch}`
			window.history.pushState(null, tabTitle, querySearch);
		},
		//main func to seearh data from rest api
		getGlobalSearchData() {
			if (this.querySearchAjax != null) {
				this.querySearchAjax.abort();
				this.querySearchAjax = null;
			}
			//fix transliterate
			const self = this;
			const data = {
				query: this.querySearch.trim(),
				csrfmiddlewaretoken: this.csrf
			}

			this.querySearchAjax = $.ajax({
				method: 'POST',
				url: self.searchApi,
				data,
				beforeSend: function () {
					if (!self.isLoading) {
						self.isLoading = true;
						self.isError = false;

						//reset data
						self.orders = [];
						self.reports = [];
						self.users = [];
						self.reviews = [];
					}
				},
				success: function (data) {
					const { orders = [], reports = [], users = [], reviews = [] } = data;
					self.orders = orders;
					self.reports = reports;
					self.users = users;
					self.reviews = reviews;
					self.isError = false;
					
					if(reviews.length > 0){
						setTimeout(function(){
							self.getRaiting();
						}, 1000);
					}
				},
				error: function (error) {
					console.log('error', error)
					self.isError = true;
				},
				complete: function () {
					self.isLoading = false;
				}
			});
		},
		querySearchKeyUp: function () {
			//debounce input data when user types on keyboard
			clearTimeout(this.timeout); // clear timeout variable
			var self = this;
			this.timeout = setTimeout(function () {
				self.getGlobalSearchData();
				self.setHistoryPush();
			}, 500);
		},
		//detect type array
		getObjFromTypeString(type) {
			let objName = null;

			switch (type) {
				case 'order':
					objName = 'orders'
					break;
				case 'report':
					objName = 'reports'
					break;
				case 'user':
					objName = 'users'
					break;
				case 'review':
					objName = 'reviews'
					break;
			}
			return objName;
		},
		//copy functions from mixin list.js - removeObject and closeObject
		removeObject(obj, type) {
			var isConfirmedAction = confirm('Вы уверены что хотите удалить ?')
			if(isConfirmedAction){
				if (!type) return;
				var self = this;
				var objList = this.getObjFromTypeString(type)
				$('#loader-wrapper').show();
				$.ajax({
					url: `/${type}/delete/${obj.id}/`,
					type: "POST",
					data: { csrfmiddlewaretoken: this.csrf },
					success: function () {
						var index = self[objList].indexOf(obj);
						self[objList].splice(index, 1);
						$('.dropdown_open').removeClass('dropdown_open');
					},
					complete: function () {
						$('#loader-wrapper').hide();
					}
				});
			}
		},
		closeObject(obj, type) {
			if (!type) return;
			var self = this;
			var objList = this.getObjFromTypeString(type)
			$('#loader-wrapper').show();
			$.ajax({
				url: `/${type}/close/${obj.id}/`,
				type: "POST",
				data: { csrfmiddlewaretoken: this.csrf },
				success: function () {
					var index = self[objList].indexOf(obj);
					self[objList].splice(index, 1);
					$('.dropdown_open').removeClass('dropdown_open');
				},
				complete: function () {
					$('#loader-wrapper').hide();
				}
			});
		},
		updateObjects() {
			this.getGlobalSearchData();
		},
		isOrderIdExist(){
			return this.getQuerystring('order') || false;
		},

	},
})
