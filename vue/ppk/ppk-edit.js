import Vue from 'vue';
import VeeValidate from 'vee-validate';

Vue.use(VeeValidate);


var app = new Vue({
  delimiters: ['${', '}'],
  el: '#ppk-edit',
  mixins: [funcs],
  data: {
    method: 1,
  },
  methods: {
    validateBeforeSubmit(e) {
      this.$validator.validateAll().then((result) => {
        if (result) {
          $("#form").submit();
        }
      })
    }
  },
})

