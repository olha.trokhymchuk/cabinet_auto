import Vue from 'vue';
import select2 from '../vue/select2-directive';
import range from '../mixins/range';
import serverValidation from '../mixins/server-validation';
import ru from "vee-validate/dist/locale/ru";
import funcs from '../mixins/funcs';

VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});


var app = new Vue({
  delimiters: ['${', '}'],
  el: '#ppk-add',
  mixins: [range, serverValidation, funcs],
  data: {
    csrf: csrf,
    year0: year0,
    year1: year1,
    engineCapacity0: engineCapacity0,
    engineCapacity1: engineCapacity1,
    horsepower0: horsepower0,
    horsepower1: horsepower1,
    yearsRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
    engineCapacityRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
    horsepowerRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
    loading: false,
    rangeFields: ['yearsRange', 'engineCapacityRange', 'horsepowerRange'],
    cost: cost,
    filterValues: {},
  },
  directives: {
    select2: select2
  },
  watch: {
    year0: function(year) {
      this.yearsRange.select0 = parseInt(year);
    },
    year1: function(year) {
      this.yearsRange.select1 = parseInt(year);
    },
    engineCapacity0: function(capacity) {
      this.engineCapacityRange.select0 = parseInt(capacity);
    },
    engineCapacity1: function(capacity) {
      this.engineCapacityRange.select1 = parseInt(capacity);
    },
    horsepower0: function(power) {
      this.horsepowerRange.select0 = parseInt(power);
    },
    horsepower1: function(power) {
      this.horsepowerRange.select1 = parseInt(power);
    },
    'cost'(newValue) {
      this.changeHandler(newValue, 'cost');
    },
  },
  mounted: function() {
    this.loadInitial();
  },
  methods: {
    back() {
      window.history.back();
    },
    loadInitial() {
      var self = this;
      self.loading = true;
      $.ajax({
        url: '/auto/filter/',
        cache: false,
        method: 'GET',
        success: function (data) {
          self.filterValues = data;
          self.loading = false;
          self.yearsRange.init = data.years;
          self.yearsRange.select0 = self.year0;
          self.yearsRange.options0 = data.years;
          self.yearsRange.select1 = self.year1;
          self.yearsRange.options1 = data.years;

          self.engineCapacityRange.init = data.capacities;
          self.engineCapacityRange.select0 = parseFloat(self.engineCapacity0);
          self.engineCapacityRange.options0 = data.capacities;
          self.engineCapacityRange.select1 = parseFloat(self.engineCapacity1);
          self.engineCapacityRange.options1 = data.capacities;

          self.horsepowerRange.init = data.horsepowers;
          self.horsepowerRange.select0 = parseInt(self.horsepower0);
          self.horsepowerRange.options0 = data.horsepowers;
          self.horsepowerRange.select1 = parseInt(self.horsepower1);
          self.horsepowerRange.options1 = data.horsepowers;
        },
        error: function (error) {
          self.loading = false;
          console.log(error);
        }
      });
    },
    validateBeforeSubmit(e) {
      this.$validator.validateAll().then((result) => {
        if (result) {
          var formElement = document.querySelector("form");
          var formData = new FormData(formElement);
          formData.append('csrfmiddlewaretoken', this.csrf);
          var url = $("#form").attr('action');
          this.submit(url, formData);
        }
      })
    }
  }
})

