import Vue from 'vue';
import VModal from 'vue-js-modal';
import ru from "vee-validate/dist/locale/ru";
import orders from '../mixins/orders'

VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});
Vue.use(VModal);

export default Vue.component('setexpert', {
  delimiters: ['${', '}'],
  template: '#set_expert',
  props: ['type_expert', 'order'],
  components: {
    VModal: VModal,
  },
  mixins: [orders],
  watch: {
    'order'(order){
      console.log(order[this.type_expert][0]);
      console.log(typeof order[this.type_expert][0] != 'undefined');
      if(typeof order[this.type_expert][0] != 'undefined' &&  order[this.type_expert][0] != false) {
        this.expert = order[this.type_expert][0];
        console.log(order[this.type_expert][0]);

      }else{
        this.expert = {"first_name": "эксперт не назначен",
          "full_name": "экперт не назначен",
          "id": 0
        }
      }
      this.filial = order.filial;
    },
  },
  data: function() {
    return {
      csrf: csrf,
      expert: {"first_name": "эксперт не назначен",
              "full_name": "экперт не назначен",
              "id": 0
            },
      filial: ""

    }
  },
  methods: {
    setExpert(user) {
      var self = this;
      //let data = {'expert': user,'type_expert': this.type_expert};
      //from orders mixin
      this.setExpertToOrder(user,this.type_expert)
        .done(function(success) {
          self.$emit('event_expert', user)
          self.modalClosed();
        })
        .fail(function(error) {
          console.log('error',error)
        })
    },
    modalClosed(user) {
        this.$modal.hide(this.type_expert);
    },
  }
})
