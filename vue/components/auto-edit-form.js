import Vue from 'vue';
import VModal from 'vue-js-modal';
import axios from 'axios';

import { bus } from '../report/report-bus';

import select2 from '../vue/select2-directive';
import Grid from 'vue-js-grid';

import funcs from '../mixins/funcs';
import automix from '../mixins/auto-mix';
import gallery from '../components/gallery';
import photos_mix from '../mixins/photos_mix';
import VueResource from 'vue-resource';

Vue.use(VueResource);
Vue.use(Grid)

Vue.use(VModal);

export default Vue.component('car-edit-form-new', {
  props: ['autoAction', 'addReportIsVisible', 'addAutoIsVisible', 'report'],
  components: {
    FileUpload: VueUploadComponent,
  },
  mixins: [funcs, automix, photos_mix],
  directives: {
    select2: select2,
  }, 
  watch: {
    'editFile.show'(newValue, oldValue) {
      if (!newValue && oldValue) {
        this.$refs.upload.update(this.editFile.id, { error: this.editFile.error || '' });
        this.$modal.hide('edit-modal');
      }
      if (newValue) {
        this.$nextTick(function () {
          if (!this.$refs.editImage) {
            return
          }
          let cropper = new Cropper(this.$refs.editImage, {
            autoCrop: false,
          })
          this.editFile = {
            ...this.editFile,
            cropper
          }
        })
      }
    },
    'addData.show'(show) {
      if (show) {
        this.addData.name = ''
        this.addData.type = ''
        this.addData.content = ''
      }
    },
    'method'(method) {
      this.$nextTick(function() {
        $('select').select2({});
        bindLoaders();
      })
    },
    'url'(url) {
      clearTimeout(this.autoRuTimer);
      let self = this;

      this.autoRuTimer = setTimeout(function () {
        if (self.method !== 1 && self.method !== '1')
          return

        self.$validator.validate('url').then((success) => {
          if (success) {
            let handlerUrl = self.detectSiteUrl(self.url);
            let data = new FormData();
            data.append('csrfmiddlewaretoken', self.csrf);
            data.append('url', self.url);
            $("input[name='url']").prop('disabled', true);
            $('#loader-wrapper').show();
            $('.loade_bar').show();
            $.ajax({
              url: handlerUrl,
              data : data,
              cache: false,
              contentType: false,
              processData: false,
              xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        $('#process').text(Math.ceil(percentComplete * 100) + '%')
                    }
                }, false);
                return xhr;
              },
              type: 'POST',
              data: data,
              success: function(data) {
                self.cost =  data.cost;
                self.mileage =  data.mileage;
                $("input[name='url']").prop('disabled', false);
                self.method = 2;
                self.files = [];
                if (data.photos) {
                  data.photos.forEach(function (base64, i) {
                    let file = self.base64ImageToFile(base64)
                    let photo = {
                      blob: URL.createObjectURL(file),
                      file: file,
                      type: 'image/jpeg',
                      size: file.size,
                      id: i,
                      autoRu: true
                    }

                    self.autoRuFiles.push(photo);
                  })
                $('#loader-wrapper').hide();
                $('.loade_stage').hide();

                }

                self.$nextTick(function () {
                  unBindLoaders();
                  let mark = data['mark_auto'];
                  let model = data['model_auto'];
                  load(mark, model, function() {
                    for (let key in data) {
                      $(`[name='${key}']`).val(data[key]);
                    }

                    $('select').select2({});
                    self.$forceUpdate();
                  })
                });
                $('#loader-wrapper').hide();
                $('.loade_stage').hide();
              },
              error: function (error) {
                  if(error.responseText.indexOf('GrabTimeoutError') !== -1){
                    self.$validator.errors.add(url, "Время выполнения истекло");
                    self.errors.add('url', "Время выполнения истекло");
                  }
                  $("input[name='url']").prop('disabled', false);
                  $('#loader-wrapper').hide();
                  $('.loade_stage').hide();
              }
            })
          }
        })
      }, 2000)
    },
    'cost'(newValue) {
      this.changeHandler(newValue, 'cost');
    },
    'mileage'(newValue) {
      this.changeHandler(newValue, 'mileage');
    },
    'report.auto.mark_auto'(newValue) {
      if(newValue!=null){
        this.getAutoFormData()
      }
    },
    'report.auto.model_auto'(newValue) {
      if(newValue!=null){
        this.getAutoFormData()
      }
    },
  },
  data: function() {
    return {
		mileage: typeof mileage != "undefined" ? mileage: 0,
		html_title: "Добавление автомобиля",
		cost: typeof cost != "undefined" ? cost : 0 ,
		autoRuTimer: null,
		method: store.method,
		files: [],
		url: '',
		uploadedFiles: store.uploadedFiles,
		minSize: 1024,
		size: 1024 * 1024 * 10,
		name: 'photo',
		dropDirectory: true,
		drop: true,
		addIndex: false,
		thread: 3,
		autoCompress: 1024 * 1024,
		uploadAuto: false,
		isOption: false,
		isPositionChanged: false,
		addData: {
			show: false,
			name: '',
			type: '',
			content: '',
		},
		csrf: store.csrf,
		editFile: {
			show: false,
			name: '',
		},
		edit: true,
    auto_form_data: null,
		base_info: {
			vin: {
        name: "VIN",
        label: "",
        type: "",
        item_type: "input",
        value: "",
        class: "fieldset__item_vin",
        validate: "max: 17, unique_vin: true"
      },
      year_auto: {
				name: "дата выпуска",
				label: "year_auto",
				type: "",
				item_type: "select",
				value: "year_auto",
				class: "fieldset__item_date",
        validate: ""
			},
      owners: {
        name: "кол-во владельцев",
        label: "",
        type: "",
        item_type: "input",
        value: "",
        class: "fieldset__item_owners",
        validate: "'numeric|max:3'"
      },
			mark_auto: {
        name: "марка",
        label: "mark_auto",
        type: "",
        item_type: "select",
        value: "mark_auto",
        class: "fieldset__item_brand",
        validate: ""
      },
      model_auto: {
        name: "модель",
        label: "model_auto",
        type: "",
        item_type: "select",
        value: "model_auto",
        class: "fieldset__item_model",
        validate: ""
      },
      generation: {
        name: "поколение",
        label: "generation",
        type: "",
        item_type: "select",
        value: "generation",
        class: "fieldset__item_generation",
        validate: ""
      },
      body_type_auto: {
        name: "кузов",
        label: "body_type_auto",
        type: "",
        item_type: "select",
        value: "body_type_auto",
        class: "fieldset__item_body",
        validate: ""
      },
      color_auto: {
        name: "цвет",
        label: "",
        type: "",
        item_type: "input",
        value: "",
        class: "fieldset__item_color",
        validate: ""
      },
      equipment: {
        name: "комплектация",
        label: "",
        type: "",
        item_type: "input",
        value: "",
        class: "fieldset__item_color",
        validate: ""
      },
      engine_type: {
        name: "двигатель",
        label: "",
        type: "",
        item_type: "select",
        value: "engine_type",
        class: "fieldset__item_engine",
        validate: ""
      },
      engine_capacity: {
        name: "объём(л)",
        label: "",
        type: "",
        item_type: "select",
        value: "engine_capacity",
        class: "fieldset__item_engine-volume",
        validate: ""
      },
      horsepower: {
        name: "мощность(л.с.)",
        label: "",
        type: "number",
        item_type: "input",
        value: "",
        class: "fieldset__item_engine-power",
        validate: "numeric|max:5"
      },
      transmission_type: {
        name: "трансмиссия",
        label: "",
        type: "number",
        item_type: "select",
        value: "transmission_type",
        class: "fieldset__item_engine-power",
        validate: ""
      },
      drive_type: {
        name: "привод",
        label: "",
        type: "number",
        item_type: "select",
        value: "drive_type",
        class: "fieldset__item_transmission",
        validate: ""
      },
      salon_auto: {
        name: "салон",
        label: "",
        type: "",
        item_type: "select",
        value: "",
        class: "fieldset__item_interior-type",
        validate: "",
        select_value: [{name:'кожа',id:'кожа'},{name:'велюр',id:'велюр'},{name:'ткань',id:'ткань'},{name:'комбинированный',id:'комбинированный'}]
      },
      color_salon: {
        name: "цвет салона",
        label: "",
        type: "text",
        item_type: "input",
        value: "",
        class: "fieldset__item_interior-color",
        validate: "",
      },
      cost: {
        name: "цена",
        label: "",
        type: "text",
        item_type: "input",
        value: "",
        class: "fieldset__item_price",
        validate: "",
      },
      comment: {
				name: "описание",
				label: "",
				type: "textarea",
				item_type: "textarea",
				value: "",
				class: "fieldset__item_description",
        validate: "",
			},
		},
		bayer_info: {
      author_type: {
        name: "тип продавца",
        label: "",
        type: "",
        item_type: "select",
        value: "author_type",
        class: "fieldset__item_owner-type",
        validate: "",
        select_value: []
      },
      author: {
        name: "имя",
        label: "",
        type: "",
        item_type: "input",
        value: "",
        class: "fieldset__item_owner-name",
        validate: "",
        select_value: []
      },
      phone: {
        name: "телефон",
        label: "",
        type: "text",
        item_type: "input",
        value: "",
        class: "fieldset__item_owner-phone",
        validate: "max: 12",
        //validate: "{ regex: /^\+?\d+$/ }",
      },
      location: {
        name: "местоположение",
        label: "",
        type: "text",
        item_type: "input",
        value: "",
        class: "fieldset__item_referrer",
        validate: "unique_source",
        select_value: []
      },
      source: {
        name: "объявление",
        label: "",
        type: "text",
        item_type: "input",
        value: "",
        class: "fieldset__item_referrer",
        validate: "",
      },
		},
		checked_info: {
			resell: {
				name: "перекуп",
				label: "",
				type: "",
				item_type: "checkbox",
				value: "",
				class: "",
        validate: ""
			},
			//send_autospot: {
			//	name: "отправить в автоспот",
			//	label: "",
			//	type: "",
			//	item_type: "checkbox",
			//	value: "",
			//	class: "",
      // validate: ""
			//},
		}
	}
  },
  mounted: function() {
    this.getAutoFormData();
  },
  methods: {
    getAutoFormData() {
      let formData = new FormData();
      let headers = {
        "X-CSRFToken": this.csrf,
        'Content-Type': 'multipart/form-data'
      };

      axios.post('/auto/api/form_data/', formData, {withCredentials: true, headers}).then(response => {
        this.auto_form_data = response.data;
      });
    },
    detectSiteUrl(url) {
      if(url.indexOf('auto.ru') !== -1) {
        return '/auto/get-data-from-auto-ru/';
      }
      if(url.indexOf('avito.ru') !== -1) {
        $(".label_default").html('ссылка на объявление avito.ru');
        return '/auto/get-data-from-avito-ru/';
      }
    },
    
    getFileBySortKey(sortKey) {
      for (var i = 0; i < this.uploadedFiles.length; i++) {
        if (this.uploadedFiles[i].sort_key == sortKey) return this.uploadedFiles[i];
      }
      return null;
    },
    onChangeSorting(e) {
      let self = this;
      e.items.forEach(function(galleryItem) {
        let file = self.getFileBySortKey(galleryItem.item.sort_key);
        file.sort_key = galleryItem.sort;
      })
      this.isPositionChanged = true;
    },
    onEditUploadedFileShow(file) {
      this.editFile = { ...file, type: 'image/jpeg', name: file.id }
      this.$refs.upload.update(file, { error: 'edit' })
      this.$modal.show('edit-modal');
    },
    onEditAutoRuFileShow(file) {
      this.editFile = { ...file, type: 'image/jpeg', name: file.id }
      this.$refs.upload.update(file, { error: 'edit' })
      this.$modal.show('edit-modal');
    },
    onEditorFile() {
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        this.editFile.show = false
        return
      }

      let data = {
        name: this.editFile.name,
      }

      if (this.editFile.cropper) {
        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
        let arr = new Uint8Array(binStr.length);
        for (let i = 0; i < binStr.length; i++) {
          arr[i] = binStr.charCodeAt(i);
        }
        data.file = new File([arr], data.name, { type: this.editFile.type });
        data.size = data.file.size;
      }
      this.$refs.upload.update(this.editFile.id, data);
      this.editFile.error = '';
      this.editFile.show = false;
    },
    onEditorUploadedFile() {
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        this.editFile.show = false
        return
      }

      let data = {
        name: this.editFile.name,
      }

      if (this.editFile.cropper) {
        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
        let arr = new Uint8Array(binStr.length);
        for (let i = 0; i < binStr.length; i++) {
          arr[i] = binStr.charCodeAt(i);
        }
        data.file = new File([arr], data.name, { type: this.editFile.type });
        data.size = data.file.size;
      }

      this.editFile.error = '';
      this.editFile.show = false;
      this.updateUploadedFile(data.file, this.editFile.id, this.editFile.sort_key);
    },
    onEditorAutoRuFile() {
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        this.editFile.show = false
        return
      }

      let data = {
        name: this.editFile.name,
      }

      if (this.editFile.cropper) {
        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
        let arr = new Uint8Array(binStr.length);
        for (let i = 0; i < binStr.length; i++) {
          arr[i] = binStr.charCodeAt(i);
        }
        data.file = new File([arr], data.name, { type: this.editFile.type });
        data.size = data.file.size;
      }

      this.editFile.error = '';
      this.editFile.show = false;
      let self = this;
      let index = this.autoRuFiles.findIndex(function (file) {
        return file.id === self.editFile.id;
      })

      this.autoRuFiles[index].file = data.file;
      this.autoRuFiles[index].blob = URL.createObjectURL(data.file)
    },
    // add folder
    onAddFolder() {
      if (!this.$refs.upload.features.directory) {
        this.alert('Your browser does not support')
        return
      }
      let input = this.$refs.upload.$el.querySelector('input')
      input.directory = true
      input.webkitdirectory = true
      this.directory = true
      input.onclick = null
      input.click()
      input.onclick = (e) => {
        this.directory = false
        input.directory = false
        input.webkitdirectory = false
      }
    },
    onAddData() {
      this.addData.show = false
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        return
      }
      let file = new window.File([this.addData.content], this.addData.name, {
        type: this.addData.type,
      })
      this.$refs.upload.add(file)
    },
    updateUploadedFile(file, fileId, sortKey) {
      var self = this;
      $('#loader-wrapper').show();
      $('.loade_stage').show();
      let data = new FormData();
      data.append('csrfmiddlewaretoken', this.csrf);
      data.append('photo', file);

      $.ajax({
        url: `/auto/update-photo-auto/${fileId}/${sortKey}`,
        method: 'POST',
        data : data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
          let newUploadedFile = JSON.parse(data);
          let index = self.uploadedFiles.map(function(f) { return f.id; }).indexOf(fileId);
          self.uploadedFiles.splice(index, 1, newUploadedFile);
          $('#loader-wrapper').hide();
          $('.loade_stage').hide();
        },
        error: function (error) {
            console.log(error);
        }
      });
    },
    removeUploadedFile(file) {
      var self = this;
      $('#loader-wrapper').show();
      $('.loade_stage').show();
      $.ajax({
        url: '/auto/delete-photo-auto/' + file.id,
        method: 'POST',
        data : {csrfmiddlewaretoken: this.csrf},
        success: function (data) {
          let index = self.uploadedFiles.indexOf(file);
          self.uploadedFiles.splice(index, 1);
          $('#loader-wrapper').hide();
          $('.loade_stage').hide();
        },
        error: function (error) {
            console.log(error);
        }
      });
    },
    removeAutoRuFile(file) {
      let index = this.autoRuFiles.indexOf(file);
      this.autoRuFiles.splice(index, 1);
    },
  },
  template: `
  <div class="form form_default car-form">
    
    <div v-if="method == 1" class="car-form__item car-form__item_auto">
      <div class="field">
        <label class="label label_default label_s">ссылка на объявление auto.ru</label>
        <div class="control control_default control_m">
          <input v-model="url" name="url" v-validate="'required|url|unique_url'" type="text" class="validateVin"/>
        </div>
        <div v-show="errors.has('url')" class="msg msg_field msg_fail">{{ errors.first("url") }}</div>
      </div>
    </div>
    <div class="list list_m">
      <div class="list__item">
        <label class="check check_l check_default">
          <input v-model="method" type="radio" name="carEditMethod" value="1"/>
          <div class="check__icon"></div>
          <div class="check__content">
            <h4>Автоматически</h4>
            <div class="check__description">указав ссылку на объявление auto.ru</div>
          </div>
        </label>
      </div>
      <div class="list__item">
        <label class="check check_l check_default">
          <input v-model="method" type="radio" name="carEditMethod" value="2" checked="checked"/>
          <div class="check__icon"></div>
          <div class="check__content">
            <h4>Вручную</h4>
            <div class="check__description">заполнив необходимые поля</div>
          </div>
        </label>
      </div>
    </div>
    <div  v-if="!edit && method == 1" class="car-form__item car-form__item_auto">
      <div class="field">
        <label class="label label_default label_s">ссылка на объявление auto.ru</label>
        <div class="control control_default control_m">
          <input v-model="url" name="url" v-validate="'required|url|unique_url'" type="text" class="validateVin"/>
        </div>
        <div v-show="errors.has('url')" class="msg msg_field msg_fail">{{ errors.first("url") }}</div>
      </div>
    </div>

    <div v-if="method == 2" class="form__item car-form__item car-form__item_manual">
      <div class="form__item form-group form-group_default">
        <div class="form-group__header">
          <h3>Основная информация</h3>
        </div>
        <div class="form-group__body fieldset fieldset_car-basic">
          <control-auto-element v-for="(select, name, i) in base_info"
            :name="select.name"
            :auto_form_data="auto_form_data"
            :value="select.value"
            :label="select.label?select.label:name" 
            :type="select.type" 
            :item_type="select.item_type" 
            :class_value="select.class"
            :validate="select.validate"
            :select_value="select.select_value"
            :key="i"
          >
          </control-auto-element>
        </div>
      </div>

      <div class="form__item form-group form-group_default">
        <div class="form-group__header">
          <h3>Информация о продавце</h3>
        </div>
        <div class="form-group__body fieldset fieldset_car-owner">
          <control-auto-element v-for="(select, name, i) in bayer_info" 
            :name="select.name"
            :auto_form_data="auto_form_data"
            :value="select.value"
            :label="name"  
            :type="select.type" 
            :item_type="select.item_type" 
            :class_value="select.class"
            :validate="select.validate"
            :select_value="select.select_value"
            :key="i"
          >
          </control-auto-element>
        </div>
      </div>

      <div class="form__item fieldset_grid3">
        <control-auto-element v-for="(select, name, i) in checked_info" 
          :name="select.name"
          :auto_form_data="auto_form_data"
          :value="select.value"
          :label="name" 
          :type="select.type" 
          :item_type="select.item_type" 
          :select_value="select.select_value"
          :class_value="select.class"
          :validate="select.validate"
          :key="i"
        >
        </control-auto-element>
        
      </div>
    </div>

    <div v-if="method == 2" class="form__item fieldset fieldset_grid3">
      <div v-if="method == 2 && addReportIsVisible == 1" @click="validateBeforeSubmit();" class="btn btn_m btn_primary">
        <div class="btn__title">Сохранить авто</div>
      </div>
    </div>
  </div>
  `,
})
