import Vue from 'vue';

export default Vue.component('dropdown', {
	props: ['order'],
	methods:{
		openDropdown(e) {
     	 	$(e.target).parents('.dropdown').toggleClass('dropdown_open');
    		},
	},
	template: `
		<div class="dropdown">
			<div class="dropdown__control" @click="openDropdown">
				<div class="btn btn_m btn_circle">
					<div class="btn__icon icon icon_m">
						<svg>
							<use xlink:href="#icon_three_dots"></use>
						</svg>
					</div>
				</div>
			</div>
			<div class="dropdown__content">
				<div class="menu-controls">
					<slot></slot>
				</div>
			</div>
		</div>
	`
})