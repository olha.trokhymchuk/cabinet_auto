import Vue from 'vue';
import VModal from 'vue-js-modal';
import photos_mix from '../mixins/photos_mix';
import ru from "vee-validate/dist/locale/ru";

VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});


export default Vue.component('gallery', {
  props: {
      numb_el:{
        type: String
      },
      multiple: {
        type: Boolean,
        default: false
      },
      manualAction: {
        type: String
      },
      uploaded: {
        type: Array,
        default: []
      },
      updatePhotoUrl: {
        type: String
      },
      deletePhotoUrl: {
        type: String
      },
      thing: {
        type: String
      },
      cover: {
        type: String
      },
      type: {
        type: String
      },
      clear_files: {
        type: Boolean,
        default: false
      },
      upload_disabled: {
        type: String
      },
      uploaded_message: {
        type: String,
        default: 'Загрузить изображения'
      },  
  },
  template: `
    <div>
      <div v-if="id"> 
        <modal height="auto" :name="numb_el" :scrollable="true" :click-to-close="true" @opened="modalOpened" @closed="modalClosed">
          <div :class="{'modal-backdrop': true, 'fade': true}"></div>
          <div id="modal-edit-file" tabindex="-1" role="dialog">
            <div class="form-group" v-if="(editFile.blob || editFile.image) && editFile.type && editFile.type.substr(0, 6) === 'image/'">
              <div class="edit-image">
                <img  v-if="editFile.blob" id="edit-image1" :src="editFile.blob" ref="editImage"/>
                <img  v-else-if="editFile.image" id="edit-image2" :src="editFile.image" ref="editImage"/>
              </div>
              <div v-bind:class="{ add_cover: cover }">
                <button type="button" class="btn btn-edit-image-action btn_m btn_primary" @click="editFile.cropper.rotate(-90)">&#10226;</button>
                <button type="button" class="btn btn-edit-image-action btn_m btn_primary" @click="editFile.cropper.rotate(90)">&#10227;</button>
                <button type="button" class="btn btn-edit-image-action btn_m btn_primary" @click="editFile.cropper.clear()">Сбросить</button>
                <button type="button" class="btn btn-edit-image-action btn_m btn_primary" @click.prevent="clearEditorPhoto()">Закрыть</button>
                <button v-if="editFile.blob" type="button" class="btn btn-edit-image-action btn_m btn_primary" @click="onEditorFile(numb_el)">Сохранить</button>
                <button v-if="editFile.image" type="button" class="btn btn-edit-image-action btn_m btn_primary" @click="onEditorUploadedFile(numb_el)">Сохранить</button>
              </div>
            </div>
          </div>
        </modal>
        <div class="uploadegallery-item" v-for="(file, index) in inside_uploaded" :key="file.id || file.pk" v-if="!deleted.includes(file)">
          <img :value="file" :id="file.id || file.pk" class="gallery-image" :src="getBlobForUploadedFile(file)"/>
          <div class="car__actions">
            <div class="dropdown">
              <div class="dropdown__control" @click="openDropdown">
                <div class="btn btn_m btn_circle">
                  <div class="btn__icon icon icon_m">
                    <svg>
                      <use xlink:href="#icon_menu_24"></use>
                    </svg>
                  </div>
                </div>
              </div>
              <div class="dropdown__content">
                <div class="menu-controls">
                  <div v-if="cover" class="menu-controls__item" style="cursor: pointer;"><a class="menu-controls__link" @click.prevent="onCover(file)">На обложку</a></div>
                  <div class="menu-controls__item" style="cursor: pointer;"><a class="menu-controls__link" @click.prevent="onEditUploadedFileShow(file, numb_el)">Редактировать</a></div> 
                  <div class="menu-controls__item" style="cursor: pointer;"><a class="menu-controls__link" @click.prevent="deleted.push(file)">Удалить</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="gallery-item loaded" v-for="(file, index) in files" :key="file.id">
          <img :id="file.id" class="gallery-image" :src="file.blob"/>
          <div class="car__actions">
            <div class="dropdown">
              <div class="dropdown__control" @click="openDropdown">
                <div class="btn btn_m btn_circle">
                  <div class="btn__icon icon icon_m">
                    <svg>
                      <use xlink:href="#icon_menu_24"></use>
                    </svg>
                  </div>
                </div>
              </div>
              <div class="dropdown__content">
                <div class="menu-controls">
                  <div class="menu-controls__item" style="cursor: pointer;"><a class="menu-controls__link" @click.prevent="file.active || file.success || file.error === 'compressing' ? false : onEditFileShow(file)">Редактировать</a></div>
                  <div class="menu-controls__item" style="cursor: pointer;"><a class="menu-controls__link" @click.prevent="$refs.upload.remove(file)">Удалить</a></div>
                  <div class="menu-controls__item" style="cursor: pointer;">
                    <button type="button" class="btn btn-edit-image-action btn_m btn_primary" @click="onRotateFile(file, -90)">&#10226;</button>
                  </div>
                  <div class="menu-controls__item" style="cursor: pointer;">
                    <button type="button" class="btn btn-edit-image-action btn_m btn_primary" @click="onRotateFile(file, 90)">&#10227;</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <file-upload class="btn btn_primary btn_m" style="padding-top: 15px; width: 100%;" ref="upload" extensions="gif,jpg,jpeg,png,webp" accept="image/png,image/gif,image/jpeg,image/webp" :disabled="upload_disabled == 'disabled'" :multiple="multiple" v-model="files" @input-file="inputFile" @input-filter="inputFilter" :input-id="'file-' + numb_el"><span>{{uploaded_message}}</span></file-upload>
    </div>

  `,
  components: {
    FileUpload: VueUploadComponent,
    VModal: VModal,
  },
  mixins: [photos_mix],
  mounted: function() {
    this.id = `{{this._uid}}`;
    try {
      this.inside_uploaded = this.uploaded;
    } catch (e) { }
  },
  watch: {
    'uploaded'(value) {
        this.inside_uploaded = value;
    },
    'editFile.show'(newValue, oldValue) {
      if (!newValue && oldValue && this.$refs.upload) {
        this.$refs.upload.update(this.editFile.pk, { error: this.editFile.error || '' });
        this.$modal.hide(this.id);
      }
      if (newValue) {
        this.$nextTick(function () {
          this.$emit('event_change_photo')
          if (!this.$refs.editImage) {
            return
          }
          let cropper = new Cropper(this.$refs.editImage, {
            autoCrop: false,
          })
          this.editFile = {
            ...this.editFile,
            cropper
          }
        })
      }
    },
    'addData.show'(show) {
      if (show) {
        this.addData.name = ''
        this.addData.type = ''
        this.addData.content = ''
      }
    },
    'clear_files'(clear){
      if(clear==true) {
        this.files = [];
        this.deleted = [];
      }
    },
    'files'(files){
      try {
          this.max_counter = this.max_counter + this.files.length;
      } catch (e) { console.error(e) }
      this.$emit('event_upload_photo', files, this.type);
    },
    'uploaded'(uploaded){
      try {
          this.max_counter = this.max_counter + this.uploaded.length;

      } catch (e) { console.error(e) }
    },
    'deleted'(deleted){
      this.$emit('event_delete_photo', deleted)
    }
  },
  data: function() {
    return {
      id: '',
      max_counter: 0,
      files: [],
      changed: [],
      deleted: [],
      minSize: 1024,
      size: 1024 * 1024 * 10,
      name: 'photo',
      uploadAuto: false,
      dropDirectory: true,
      drop: true,
      addIndex: false,
      thread: 3,
      autoCompress: 1024 * 1024,
      isOption: false,
      isPositionChanged: false,

      addData: {
        show: false,
        name: '',
        type: '',
        content: '',
      },
      csrf: csrf,
      editFile: {
        show: false,
        name: '',
      },
      inside_uploaded: {}
    }
  },
  methods: {
    getBlobForUploadedFile(file) {
      for (var i = 0; i < this.changed.length; i++) {
        if (this.changed[i].id == file.pk) {
          return this.changed[i].image_small;
        }
      }

      return file.image_small;
    },
    updateAutoCover(file, report_id) {
      let data = new FormData();
      data.append('csrfmiddlewaretoken', this.csrf);
      data.append('photo', file.image);
      data.append('google_photo', file.image_google);
      let url = '/report/udate-auto-cover/' + report_id + '/';
      $.ajax({
        url: url,
        method: 'POST',
        data : data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
          console.log(data)
        },
        error: function (error) {
            console.log(error);
        }
      });
    },
    updateUploadedFile(file, fileId, sortKey) {
      var self = this;
      let data = new FormData();
      data.append('csrfmiddlewaretoken', this.csrf);
      data.append('photo', file);
      $.ajax({
        url: this.updatePhotoUrl + fileId,
        method: 'POST',
        data : data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {},
        error: function (error) {
            console.log(error);
        }
      });
    },
    clearEditorPhoto(){

	      $(".v--modal-overlay.scrollable").css({"display":"none"});
	      $("body").removeClass('v--modal-block-scroll');
    },
    removeUploadedFile(file) {
      var self = this;

      $.ajax({
        url: this.deletePhotoUrl + file.id,
        method: 'POST',
        data : {csrfmiddlewaretoken: this.csrf},
        success: function (data) {
          let index = self.uploaded.indexOf(file);
          self.uploaded.splice(index, 1);
        },
        error: function (error) {
            console.log(error);
        }
      });
    },
    isDeleted(item){
      return !this.deleted.find((deleted) => deleted.name == item.name);
    },
  }
})
