import Vue from 'vue';
import VModal from 'vue-js-modal';
import select2 from '../vue/select2-directive';
import Grid from 'vue-js-grid';

import funcs from '../mixins/funcs';
import automix from '../mixins/auto-mix';
import bus from '../vue/bus';
Vue.use(Grid)

Vue.use(VModal);

export default Vue.component('create-report', {
  delimiters: ['${', '}'],
  template: '#create-report',
  props: ['autoAction', 'addReportIsVisible'],
  components: {
	   FileUpload: VueUploadComponent,
  },
  mixins: [funcs, automix],
  directives: {
    select2: select2,
  },
  methods: {
  	saveAndAddPreReport(){
        bus.$emit('my-event', 'create')
    },
  }
})
