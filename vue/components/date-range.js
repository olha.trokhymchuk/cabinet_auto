import Vue from 'vue';

const getDateEnd = (dateStart, dateEnd) => {
	try {
		if (dateEnd) return dateEnd;
		if (dateStart instanceof Date && !isNaN(dateStart)) return '';

		var monthPassedCount = 2;// two month
		var dateEndObj = new Date(dateStart);// convert string to date
		var newDate = new Date(dateEndObj.setMonth(dateEndObj.getMonth() + monthPassedCount));//add 2 month to dateEnd
		var newDateUTC = new Date(Date.UTC(newDate.getFullYear(), newDate.getMonth(), newDate.getDate()));

		return newDateUTC.toISOString().slice(0, 10)//converted new date - like '2019-07-13'
	} catch (error) {
		console.log(e)
		return ''
	}
}

export default Vue.component('date-range', {
	data: function () {
		return {
			date_start: this.order.date_start,
			date_end: getDateEnd(this.order.date_start,this.order.date_end),
			dateArr:[],
			weekPassCounter:0,
		}
	},
	created(){
		this.getMarkupWeeks()
	},
	props: ['order'],
	methods:{
		getDayWeek: function (date){
			return date.split('-').reverse().slice(0,2).join('.');
		},
		getMarkupWeeks(){
			var start = new Date(this.date_start);
			var end = new Date(this.date_end);
			var dateNow = new Date();
			var sDate;
			var eDate;

			while(start <= end){
				if (start.getDay() == 1 || (this.dateArr.length == 0 && !sDate)){
					sDate = new Date(start.getTime());
				}

				if ((sDate && start.getDay() == 0) || start.getTime() == end.getTime()){
					eDate = new Date(start.getTime());
				}

				if(sDate && eDate){
					var weekPass = eDate <= dateNow;
					if(weekPass){
						this.weekPassCounter++;
					}
					this.dateArr.push({
						'startDate': sDate,
						'endDate': eDate,
						weekPass,
					});
					sDate = undefined;
					eDate = undefined;
				}

				start.setDate(start.getDate() + 1);
			}
		},
	},
	computed:{
		weekPassType:function () {
			var classType = {};
			if(this.weekPassCounter <= Math.ceil(this.dateArr.length/2) ){
				classType.active = true;
			}else {
				if(this.weekPassCounter === this.dateArr.length){
					classType.fail = true;
				}else {
					classType.danger = true;
				}
			}
			return classType
		}
	},
	template: `
		<div class="ord__date-range">
			<ul :class="weekPassType">
				<li v-for="date in dateArr" :class="{ week_pass: date.weekPass }"></li>
			</ul>
			<div class="ord__date-range_text">
				<span>{{getDayWeek(this.date_start)}}</span>
				<span>{{getDayWeek(this.date_end)}}</span>
			</div>
		</div>`
})