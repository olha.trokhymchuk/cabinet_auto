import Vue from 'vue';

import report_detail from '../mixins/report-detail';
import step_mix, { inside, power } from '../mixins/step_mix';


export const checker =  Vue.component('checker', {
	props: ['value','name','okrasheno','skol_carapina',
			'vmyatina','corozia_rgavchina','zameneno','com'],
	mixins: [step_mix],
	template: `
		<div  v-if="value !==null" class="item">
		    <div class="name">
		        <div v-bind:class="value ? 'con good' : 'con bad' "></div>
		        <h4 v-text="stepByValue(name,value)">></h4>
		    </div>
		    <div v-if="!value" class="description">
		        <ul>
		            <li v-if="okrasheno" >Окрашено</li>
		            <li v-if="skol_carapina" >Скол/царапина</li>
		            <li v-if="vmyatina" >Вмятина</li>
		            <li v-if="corozia_rgavchina" >Коррозия/ржавчина</li>
		            <li v-if="zameneno" >Заменено</li>
		        </ul>
		        <blockquote v-if="com">
	                {{com}}
	            </blockquote>
		    </div>
		</div>
	`
});
export const shema =  Vue.component('shema', {
	props: ['value','name','label','okrasheno','skol_carapina',
			'vmyatina','corozia_rgavchina','zameneno','com','onClick'],
	data: function() {
		return {
			inside: inside,
			power: power,
		}
	},
	methods: {
	    showItme: function(item) {
	    	this.$emit('event_show_item', item)
	    },
	    getCoordinate(label, side) {
	    	try {
	    		return this.inside[label][side] -1 + '%';
	    	} catch(e) {

	    	}
	    	try {
	    		return this.power[label][side] -1 + '%';
	    	} catch(e) {

	    	}
	    },

    },
	template: `
			<div v-if="value !==null" class="pos"
			   v-bind:style="{left: getCoordinate(label, 'left'),top:getCoordinate(label, 'top')}"
				:class=" value ? 'good'  : 'bad' "
				@click='showItme({
					value: value,
					name: name,
					description: com,
					list: {
						"Окрашено": okrasheno,
						"Скол/царапина": skol_carapina,
						"Вмятина": vmyatina,
						"Коррозия/ржавчина": corozia_rgavchina,
						"Заменено": zameneno

					}
		        })'>
	        </div>
	`
});
export const checker2 =  Vue.component('checker2', {
	props: ['value','name','com','des'],
	mixins: [step_mix, report_detail],
	template: `
		<div  v-if="value !==null" class="item">
			<div class="name">
				<div v-bind:class=" value ? 'con good' : 'con bad' "></div>
				<h4 v-text="stepByValue(name,value)"></h4>
			</div>
			<div class="description">
				<ul v-if="des && item.list.length > 0 ">
					<li v-for=" des in item.list"> {{ des }} </li>
				</ul>
				<blockquote v-if=" detectEmptyMarkup(com) ">{{ com }} </blockquote>
			</div>
		</div>
	`
});
export const checkermsg =  Vue.component('checkermsg', {
	props: ['value','name','com','s-msg'],
	mixins: [step_mix, report_detail],
	template: `
		<div class="item">
			<div class="name">
				<div v-bind:class="value ? 'con good' : value == null ? '' : 'con bad' "></div>
				<h4 v-text="stepByValue(name,value)"></h4>
			</div>
			<div class="description">
				<blockquote v-if="value != null">{{ com }}</blockquote>
				<blockquote v-else>Проверка не проводилась</blockquote>
			</div>
		</div>
	`
});
export const range =  Vue.component('range', {
	mixins: [step_mix, report_detail],
	props: ['value','name','com','range'],
	template: `
        <div v-if="value !==null" class="item percenter">
            <div class="named">
				<div class="text" v-text="stepByValue(name,value) + '-' + range + '%'"></div>
                <div class="info-hide">
                    <div v-if=" detectEmptyMarkup(com)" class="texted"> {{ com }} </div>
                </div>
            </div>

            <div class="progress-bar">
                <div class="level" v-bind:style="{ right: 100 - range + '%', backgroundColor: calculateColor(range) }"></div>
            </div>
        </div>
	`
});
export const vin =  Vue.component('vin', {
	mixins: [report_detail, step_mix],
	props: ['value','name','index','group'],
	methods: {
		clicked() {
			this.$emit('clicked');
		}
	},
	template: `
        <div v-if="value" class="item">
			<div class="name">
				<div v-if="group != 'legal'">
					<div class="con good"></div>
				</div>
				<a @click="clicked" href="javascript:;" v-if="name">
					<h4>
					{{name}}
					<span class="icon-doc"></span>
					</h4>
				</a>
			</div>
		</div>
	`
});

export const checker_glass =  Vue.component('checker-glass', {
	props: ['value','name','skol',
			'treshina','zapotevanie','zameneno','com'],
	mixins: [step_mix],
	template: `
		<div  v-if="value !==null" class="item">
		    <div class="name">
		        <div v-bind:class="value ? 'con good' : 'con bad' "></div>
		        <h4 v-text="stepByValue(name,value)"></h4>
		    </div>
		    <div v-if="!value" class="description">
		        <ul>
		            <li v-if="skol" >Скол</li>
                    <li v-if="treshina" >Трещина</li>
                    <li v-if="zapotevanie" >Запотевание</li>
		            <li v-if="zameneno" >Заменено</li>
		        </ul>
		        <blockquote v-if="com">
	                {{com}}
	            </blockquote>
		    </div>

		</div>
	`
});

export const shema_glass_twise =  Vue.component('shema-glass-twise', {
	props: ['value','name','label','skol','treshina',
			'zapotevanie','zameneno','com','onClick'],
	data: function() {
		return {
			inside: inside,
			power: power,
		}
	},
	mounted: function() {

  	},
	methods: {
	    showItme: function(item) {
	    	this.$emit('event_show_item', item)
	    },
	    getCoordinate(label, side, twise) {
	    	try {
	    		return this.inside[label][twise][side] -1 + '%';
	    	} catch(e) {

	    	}
	    	try {
	    		return this.power[label][twise][side] -1 + '%';
	    	} catch(e) {

	    	}
	    }

    },
	template: `
	<em>
		<div v-if="value !==null"
			class="pos" v-bind:style="{left: getCoordinate(label, 'left', 'rights'),top:getCoordinate(label, 'top', 'rights')}"
			:class=" value ? 'good'  : 'bad' "
			@click='showItme({
				value: value,
				name: name,
				description: com,
				list: {
					"Скол": skol,
					"Трещина": treshina,
					"Запотевание": zapotevanie,
					"Заменено": zameneno,

				}
			})'>
        </div>
		<div v-if="value !==null"
			class="pos" v-bind:style="{left: getCoordinate(label, 'left', 'lefts'),top:getCoordinate(label, 'top', 'lefts')}"
			:class=" value ? 'good'  : 'bad' "
			@click='showItme({
				value: value,
				name: name,
				description: com,
				list: {
					"Скол": skol,
					"Трещина": treshina,
					"Запотевание": zapotevanie,
					"Заменено": zameneno,

				}
			})'>
        </div>
    </em>
	`
});

export const shema_glass =  Vue.component('shema-glass', {
	props: ['value','name','label','skol','treshina',
			'zapotevanie','zameneno','com','onClick'],
	data: function() {
		return {
			inside: inside,
			power: power,
		}
	},
	methods: {
	    showItme: function(item) {
	    	this.$emit('event_show_item', item)
	    },
	    getCoordinate(label, side) {
	    	try {
	    		return this.inside[label][side] -1 + '%';
	    	} catch(e) {

	    	}
	    	try {
	    		return this.power[label][side] -1 + '%';
	    	} catch(e) {

	    	}
	    },

    },
	template: `
		<div v-if="value !==null"
			class="pos" v-bind:style="{left: getCoordinate(label, 'left'),top:getCoordinate(label, 'top')}"
			:class=" value ? 'good'  : 'bad' "
			@click='showItme({
				value: value,
				name: name,
				description: com,
				list: {
					"Скол": skol,
					"Трещина": treshina,
					"Запотевание": zapotevanie,
					"Заменено": zameneno,
				}
			})'>
        </div>
	`
});
export const hidden_image =  Vue.component('hidden-image', {
	props: ['value','name','group','index'],
	template: `
		<div class="slider-popup full-size-slider" :id="group + index">
		    <div class="conteiner this-has-list-of-slides">
		        <div class="closer" data-fancybox-close></div>
		        <div class="download-button">
		            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 479 479" style="enable-background:new 0 0 479 479;" xml:space="preserve">
		                <g
		                    <g>
		                        <path style="fill:#f1ba46" d="M158.4,196.8c-5.3,5.3-5.3,13.8,0,19.1l71.6,71.6c2.6,2.6,6.1,4,9.5,4s6.9-1.3,9.5-4l71.6-71.6c5.3-5.3,5.3-13.8,0-19.1
		                            s-13.8-5.3-19.1,0L253,245.3V13.5C253,6,247,0,239.5,0S226,6,226,13.5v231.8l-48.5-48.5C172.2,191.5,163.6,191.5,158.4,196.8z"/>
		                        <path style="fill:#f1ba46" d="M460.2,307.4l-47-170c-1.1-3.9-3.8-7-7.4-8.7l-1.3-0.6c-1.8-0.8-3.7-1.2-5.6-1.2h-84.7c-7.5,0-13.5,6-13.5,13.5
		                            s6,13.5,13.5,13.5h75.6l39.7,143.8h-105c-7.5,0-13.5,6-13.5,13.5v12.2c0,17.9-14.5,32.4-32.4,32.4h-82.4
		                            c-17.9,0-32.4-14.5-32.4-32.4v-12.2c0-7.5-6-13.5-13.5-13.5H49.4l39.9-144.1h75.6c7.5,0,13.5-6,13.5-13.5s-6-13.5-13.5-13.5H80.1
		                            c-1.9,0-3.8,0.4-5.6,1.2l-1.3,0.6c-3.6,1.7-6.4,4.8-7.4,8.7l-47,170c-0.3,1.2-0.5,2.4-0.5,3.6v70.9c0,53.7,43.7,97.4,97.4,97.4
		                            h247.6c53.7,0,97.4-43.7,97.4-97.4V311C460.7,309.7,460.6,308.5,460.2,307.4z M433.7,381.6c0,38.8-31.6,70.4-70.4,70.4H115.7
		                            c-38.8,0-70.4-31.6-70.4-70.4v-56.9H137c0.7,32.1,27.1,58,59.4,58h82.4c32.3,0,58.7-25.9,59.4-58h95.7v56.9H433.7z"/>
		                    </g>
		                </g>
		            </svg>
		        </div>
			     <div class="top-slider swiper-container">
			   	   <div class="swiper-wrapper">
                     <div v-for="(item2, index) in value" class="con swiper-slide">
                        <div class="gallary_popup_img">
                           <div class="gallary_popup_img_holder">
                              <img :src="item2.image" alt="image" :data-src="item2.image">
                           </div>
                        </div>
                     </div>
				      </div>
		        </div>
              <div class="nav-slider swiper-container">
                  <div class="swiper-wrapper">
                     <div v-for="(item2, index2) in value" class="con swiper-slide">
                        <img :src="item2.image" >
                     </div>
                  </div>
              </div>
		    </div>
		</div>
	`
});


export const hidden_image_single =  Vue.component('hidden-image-single', {
	props: ['value','name','group','index'],
	template: `
		<div class="single-pop" :id="group + index">
            <div class="convert">
                <div class="contein-top">
                    <div class="named">
                        <h3> {{ name }} </h3>
                    </div>
                    <div class="closer" data-fancybox-close></div>
                </div>
                <div v-for="( item, index ) in value" class="contein-imager">
                    <img :src="item.image" alt="">
                </div>
            </div>
        </div>
	`
});