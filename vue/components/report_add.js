import Vue from 'vue';

import VModal from 'vue-js-modal';
Vue.use(VModal)

import { bus } from '../report/report-bus';
import select2_derective from '../vue/select2-directive';

import VueResource from 'vue-resource';
Vue.use(VueResource);

import ru from "vee-validate/dist/locale/ru";
const dictionary = {
  ru: {
    messages: {
      unique_source: function () {
        return "Такой автомобиль уже существует"
      },
      unique_url: function () {
        return "Такой автомобиль уже существует"
      },
      unique_vin: function () {
        return "Такой автомобиль уже существует"
      }
    }
  }
}
VeeValidate.Validator.addLocale(ru);
VeeValidate.Validator.updateDictionary(dictionary);
Vue.use(VeeValidate, {locale: 'ru', events: 'input|blur'});

import step_mix from '../mixins/step_mix';
import funcs from '../mixins/funcs';


export const step_button =  Vue.component('step-button', {
	props: ['value','name','label','i','currentStep','onClick'],
	data() {
		return {
			sterp_name: "шаги отчета"
		}
	},
	methods: {
	    showStep: function(label) {
	    	this.$emit('event_show_step', label)
	    }
	      
    },
	template: `
		<a class="report-nav__item" :class="{'report-nav__item_active': label == currentStep}" style="cursor: pointer"
		 @click="showStep(label)">
			<div class="report-nav__progress progress bg_info js-report-edit">
				<div class="progress__bar"></div>
				<div class="progress__mask"></div>
			</div>
			<div class="report-nav__index">{{ i + 1 }}</div>
			<div class="report-nav__title">{{ name }} <span v-if="i === 0">(Обязательно)</span> </div>
		</a>
	`
});

export const boolean_field =  Vue.component('boolean-field', {
	props: ['value','name','com','com_name','label','data_id','required','groupIndex','fieldIndex'],
	mixins: [step_mix, funcs],
	data() {
		return {
			inside_value: this.value,
			comment: this.com
		}
	},
	mounted: function () {
		this.inside_value = this.value;
		this.comment = this.com;
	},
	watch: {
		'inside_value'(value) {
			if(value!=this.value) {
				this.$emit('event_change_field', this.name, value)
				
			}
		},
		'comment'(value) {
			if(value != this.com) {
				this.$emit('event_change_field', this.com_name, value)
			}
		},
		'value'(value) {
			this.inside_value = value;
		},
		'com'(com) {
			this.comment = com;
		},
	},
	template: `
		<div :data-id="data_id" class="report-field report-field_diagnosis"

			:class="{
				'report-field_active': isActive(inside_value),
				'report-field_positive': isPositive(inside_value),
				'report-field_negative': isNegative(inside_value)
			}">
	        <div  class="report-field__header">
	          <h5 v-if="inside_value == true || inside_value == false || inside_value == ''"  class="report-field__title" v-text="stepByValue(label,inside_value)">
	          </h5> 
	          <h5 v-else class="report-field__title">{{ label }}
	          </h5>
	          <div class="report-field__controls">
	            <label class="check check_binary check_binary_fail"
	            :data-id="'element_' + data_id"
	            >
	              <input v-model="inside_value" :value="false" @click="unchec(false)" type="radio" :required="required"
	              />
	              <div class="check__icon icon icon_m icon_fail icon_circle">
	                <svg>
	                  <use xlink:href="#icon_close_24"></use>
	                </svg>
	              </div>
	            </label>
	            <label class="check check_binary check_binary_success"
	            :data-id="'element_' + data_id"
	            >
	              <input v-model="inside_value" :value="true"  @click="unchec(true)" type="radio" :required="required"
	              />
	              <div class="check__icon icon icon_m icon_success icon_circle">
	                <svg>
	                  <use xlink:href="#icon_check_24"></use>
	                </svg>
	              </div>
	            </label>
	          </div>
	        </div>
	        <div  class="report-field__body">
	          <div class="report-field__control report-field__control_negative field">
	            <div class="field__control control control_default control_m">
	              <textarea v-model="comment" class="report-field__comment"></textarea>
	              <div v-show="notField(inside_value, label, comment, data_id)" class="msg msg_field msg_fail">Поле комментарий обязательно для заполнения
	              </div>
	            </div>
	          </div>
	        </div>
	    </div>
	`
});


export const range_field =  Vue.component('range-field', {
	props: ['value','name','com','com_name','range','range_name','label','data_id','required','groupIndex','fieldIndex'],
	mixins: [step_mix, funcs],
	data() {
		return {
			inside_value: this.value,
			comment: this.com,
			inside_range: this.range?this.range:25,
			activeColor: 'red',
			show_range: true
		}
	},
	mounted: function () {
		this.inside_value = this.value;
	},
	watch: {
		'value'(value) {
			this.inside_value = value
		},
		'inside_value'(value) {
			this.$emit('event_change_field', this.name, value);
			if(value === true) {
				this.inside_range = 75;
			}else{
				this.inside_range = 25;	
			}
		},
		'range'(value) {
			this.inside_range = value;
			this.show_range = true;
		},
		'inside_range'(value) {
			try {
				this.$emit('event_change_field', this.range_name, parseInt(value, 10));
			} catch (e) { }
		},
		'com'(value) {
			this.comment = value;
		},
		'comment'(value) {
			this.$emit('event_change_field', this.com_name, value)
		},
	},
	template: `
		<div :data-id="data_id" class="report-field report-field_diagnosis"
			:class="{
			'report-field_active': isActive(inside_value),
			'report-field_positive': isPositive(inside_value),
			'report-field_negative': isNegative(inside_value)
			}"
		>
			<div class="report-field__header">
				<h5 v-if="value == true || value == false || value == ''"  class="report-field__title" v-text="stepByValue(label,value)">
				</h5> 
				<h5 v-else class="report-field__title"> {{ label }}
				</h5>
				<div class="report-field__controls">
					<label class="check check_binary check_binary_fail"
					:data-id="'element_' + data_id"
					>
						<input v-model="inside_value" :value="false" @click="unchec(false)" type="radio" :required="required"
						/>
						<div class="check__icon icon icon_m icon_fail icon_circle">
							<svg>
								<use xlink:href="#icon_close_24"></use>
							</svg>
						</div>
					</label>
					<label class="check check_binary check_binary_success"
					:data-id="'element_' + data_id"
					>
						<input v-model="inside_value"  :value="true" @click="unchec(true)" type="radio" :required="required"
						/>
						<div class="check__icon icon icon_m icon_success icon_circle">
							<svg>
								<use xlink:href="#icon_check_24"></use>
							</svg>
						</div>
					</label>
				</div>
			</div>
			<div class="report-field__body">
				<div class="report-field__control nouislider_test report-field__control_positive" v-cloak>
					<slider v-if="show_range" v-model="inside_range"></slider>
				</div>
				<div class="report-field__control report-field__control_negative field">
					<div class="field__control control control_default control_m">
						<textarea v-model="comment" class="report-field__comment"></textarea>	
					</div>
					<div v-show="notField(inside_value, label, comment, data_id)" class="msg msg_field msg_fail">
						Поле комментарий обязательно для заполнения
					</div>
				</div>
			</div>
		</div>
	`
});


export const header_omment =  Vue.component('header-comment', {
	props: ['name','label','scheme'],
	mixins: [step_mix, funcs],
	data() {
		return {
			header_name: "Хедер компонент",
			inside_scheme: false
		}
	},
    watch: {
    'inside_scheme'(value) {
			this.$emit('event_change_scheme', value)
		},
	},
	template: `
		<div  class="report-form__header">
			<h2 class="report-form__title">{{ name }}</h2>
			<div v-show="label == 'kuzov'" class="report-form__switch btn btn_s btn_default" @click="inside_scheme = !inside_scheme">
				<div class="btn__title">Схема|Список</div>
			</div>
		</div>
	`
});

export const comment_step =  Vue.component('comment-step', {
	props: ['name','value','label','scheme'],
	mixins: [step_mix, funcs],
	data() {
		return {
			inside_value: this.value,
			header_name: "комментарий компонент"
		}
	},
	mounted: function () {
		this.inside_value = this.value;
	},
	watch: {
		value: function (value) {
			this.inside_value = this.value;
		},
		inside_value: function (value) {
			if(value !=this.value) {
				this.$emit('event_change_field', this.label, value)
			}
		},
	},
	template: `
		<div class="report-group">
			<div class="report-group__body">
				<div class="report-field_info">
					<div class="report-field__header">
						<h5 class="report-field__title">Комментарий к разделу {{ name }}</h5>
					</div>
					<div class="report-field__body">
						<div class="report-field__control field report-field__control_positive">
							<div class="field__control control control_default control_m">
								<textarea v-model="inside_value" class="report-field__comment"></textarea>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	`
});

export const control_auto_element =  Vue.component('control-auto-element', {
	props: ['name','value','label','type','item_type','select_value','class_value','report','auto_form_data','validate'],
	data() {
		return {
			control_name: "управляющий елемент"
		}
	},
	template: `
		<div class="fieldset__item" :class="class_value">
			<select-auto v-if="item_type=='select'" 
					:name="name"
					:value="value"
					:label="label" 
					:report="report" 
					:auto_form_data="auto_form_data" 
					:select_value="select_value" 
				>
			</select-auto>
			<input-auto v-if="item_type=='input'" 
					:name="name"
					:value="value"
					:label="label"
					:type="type"
					:validate="validate"
				>
			</input-auto>
			<checbox-auto v-if="item_type=='checkbox'" 
					:name="name"
					:value="value"
					:label="label"
				>
			</checbox-auto>
			<textarea-auto v-if="item_type=='textarea'" 
					:name="name"
					:value="value"
					:label="label"
				>
			</textarea-auto>
		</div>
	`
});

export const textarea_auto =  Vue.component('textarea-auto', {
	props: ['name','value','label'],
	template: `
		<i>
			<label class="label label_default label_s" :for="label">{{ name }}</label>
			<div class="control control_default control_m">
				<textarea  
					v-model="inside_value" 
					:name="label"
				/>
			</div>
		</i>
`
});

export const select_auto =  Vue.component('select-auto', {
	props: ['name','value','label','select_value','report','auto_form_data'],
	mixins: [step_mix, funcs],
	directives: {
		select2: select2_derective,
	},
	data: function () {
		return {
			inside_value: this.value,
			inside_select_vaule: this.select_value
		}
	},
	mounted: function () {
		try {
			this.inside_select_vaule = this.select_value;
		} catch (e) { }
	},
	watch: {
		auto_form_data: function(value) {
			try {	
				if(value[this.value] !=="undefined" && value[this.value] !=undefined) {
					this.inside_select_vaule = value[this.value]	
				}
			} catch (e) { }
		},
		value: function(value) {
			this.inside_value = value;
		},
		select_value: function(value) {
			this.inside_select_vaule = value
			if(typeof value == undefined) {
				this.inside_select_vaule = this.auto_form_data[this.value]
			}
		},
		inside_value: function (value) {
			bus.$emit('event_auto_data', this.label, value, this.name);
		},
	},
	template: `
		<div>
			<label class="label label_default label_s" for="label">{{ name }}</label>
			<div class="control control_default control_m">
				<select :id="label" v-model="inside_value" v-select2="inside_value" :name="label">
					<option v-if="inside_select_vaule && label !== 'year_auto'" v-for="instance in inside_select_vaule"
						:value="instance.id">
						<span>{{ instance.name }}</span>
					</option>
					<option v-if="label == 'year_auto'" v-for="instance in inside_select_vaule.slice().reverse()"
						:value="instance.id">
						<span>{{ instance.name }}</span>
					</option>
					<option  v-if="!inside_select_vaule">{{ name }} не найдены </option>
				</select>
			</div>
		</div>
	`
});

export const input_auto =  Vue.component('input-auto', {
	props: ['name','value','label','type','validate'],
	mounted: function () {
		try {
			this.inside_value = this.value;
		} catch (e) { }
	},
	data: function () {
		return {
			inside_value: "",
			validate_str: "",
			errors_data: {}
		}
	},
	watch: {
		inside_value: function (value) {
			if(this.label == "phone"){
				let validator = value.match(/^\++\d+$/);
				if(!validator && value ) {
					this.$validator.errors.add({
					  id: 1111,
					  field:"телефон",
					  msg: "поле телефон имеет ошибочный формат",
					  scope: null,
					});
				}else{
					this.$validator.errors.remove("телефон")
				}
			}
			bus.$emit('event_auto_data', this.label, value);
		},
		validate: function (value) {
			this.validate_str = String(validate);
		}
	},
	template: `
		<i>	
		<label class="label label_default label_s" :for="label">{{ name }}</label>
		<div class="control control_default control_m">
			<input  
				v-model="inside_value" 
				:name="label"
				v-validate="validate"
				class="validateVin"
			/>
		</div>
		<div  v-show="errors.has(name)" class="msg msg_field msg_fail">
			{{errors.first(name)}}
		</div>
		</i>
`
});

export const checkbox_auto =  Vue.component('checbox-auto', {
	props: ['name','value','label'],
	data: function () {
		return {
			inside_value: false
		}
	},
	watch: {
		inside_value: function (value) {
			bus.$emit('event_auto_data', this.label, value);
		}
	},
	template: `
		<label class="check check_m check_default" :for="'id_' + label">
			<input v-model="inside_value" type="checkbox" :id="'id_' +label" :name="label" />
			<div class="check__icon"></div>
			<div class="check__content">
				<div class="check__title">{{ name }}</div>
			</div>
		</label>
`
});


export const submit_step =  Vue.component('submit-step', {
	props: ['name', 'label'],
	mixins: [step_mix, funcs],
	methods: {
		validateBeforeSubmit: function(label) {
			this.$emit('event_validate_before_submit')
		}
	},
    data() {
		return {
			submit_step_name: "Сабмит кнопки"
		}
	},
	template: `
		<div class="report-group">
	        <div class="report-group__body">
	          <div class="report-field_info">
	            <button class="btn btn_m btn_primary" @click.prevent="validateBeforeSubmit(label)">
	              <div class="btn__grid">
	                  <div class="btn__title">{{ name }}</div>
						<!--Далее Продолжить Сохранить-->
	              </div>
	            </button>
	          </div>
	        </div>
	    </div>
	`
});

export const scheme_edit_marker =  Vue.component('scheme-edit-marker', {
	props: ['field','name','value','type','data_id'],
	mixins: [step_mix, funcs],
    data() {
		return {
			submit_step_name: "Схема"
		}
	},
	template: `
	<label>

		<div v-if="!field.lefts && !field.rights" class="car-scheme__area" :data-id="data_id" @click="onMarkerClick(data_id)" v-bind:style="{left: field.left + '%',top:field.top+ '%'}">
			<div class="car-scheme__marker" 
			:class="{'car-scheme__marker_negative':markerIsNegative(value, name), 'car-scheme__marker_positive': markerIsPositive(value, name)}"></div>
		</div>
		<div v-if="field.lefts" class="car-scheme__area" :data-id="data_id" @click="onMarkerClick(data_id)" v-bind:style="{left: field.lefts.left + '%',top:field.lefts.top+ '%'}">
			<div class="car-scheme__marker" 
			:class="{'car-scheme__marker_negative':markerIsNegative(value, name), 'car-scheme__marker_positive': markerIsPositive(value, name)}"></div>
		</div>
		<div v-if="field.rights" class="car-scheme__area" :data-id="data_id" @click="onMarkerClick(data_id)" v-bind:style="{left: field.rights.left + '%',top:field.rights.top+ '%'}">
			<div class="car-scheme__marker" 
			:class="{'car-scheme__marker_negative':markerIsNegative(value, name), 'car-scheme__marker_positive': markerIsPositive(value, name)}"></div>
		</div>
	</label>

	`
});

export const scheme_edit1 =  Vue.component('scheme-edi1', {
	props: ['field','type','data_id'],
	mixins: [step_mix, funcs],
    data() {
		return {
			submit_step_name: "Схема"
		}
	},
	template: `
	<label>
		<div  class="car-scheme__area" :data-id="data_id" @click="onMarkerClick(data_id)" v-bind:style="{left: field.left + '%',top:field.top+ '%'}">
			<div class="car-scheme__marker" :class="{'car-scheme__marker_negative':markerIsNegative(1, data_id), 'car-scheme__marker_positive': markerIsPositive(1, data_id)}"></div>
		</div>
	</label>
	`
});


export const shema_edit_popup =  Vue.component('shema-edit-popup', {
	props: ['value','field','fieldss','data_id','com','onClick'],
	mixins: [step_mix, funcs],
	data: function() {
		return {
			submit_step_name: "Редактирование маркера поп ап",
			inside_value: this.fieldss[this.value],
			comment: this.fieldss[this.field.com],
			okrasheno:this.fieldss[this.field.okrasheno],
			skol_carapina:this.fieldss[this.field.skol_carapina],
			vmyatina:this.fieldss[this.field.vmyatina],
			corozia_rgavchina:this.fieldss[this.field.corozia_rgavchina],
			skol:this.fieldss[this.field.skol],
			treshina:this.fieldss[this.field.treshina],
			zapotevanie:this.fieldss[this.field.zapotevanie],
			zameneno:this.fieldss[this.field.zameneno]
		}
	},
	mounted(){
		this.inside_value=this.fieldss[this.value];
		this.comment=this.fieldss[this.field.com];
		this.okrasheno = this.fieldss[this.field.okrasheno];
		this.skol_carapina = this.fieldss[this.field.skol_carapina];
		this.vmyatina = this.fieldss[this.field.vmyatina];
		this.corozia_rgavchina = this.fieldss[this.field.corozia_rgavchina];
		this.skol = this.fieldss[this.field.skol];
		this.treshina = this.fieldss[this.field.treshina];
		this.zapotevanie = this.fieldss[this.field.zapotevanie];
		this.zameneno = this.fieldss[this.field.zameneno];
	},
	watch: {
		inside_value: function (value) {
			this.$emit('event_change_field', this.value, this.inside_value)
		},
		comment: function (value) {
			this.$emit('event_change_field', this.field.com, value)
		},
		okrasheno: function (value) {
			this.$emit('event_change_field', this.field.okrasheno, value)
		},
		skol_carapina: function (value) {
			this.$emit('event_change_field', this.field.skol_carapina, value)
		},
		vmyatina: function (value) {
			this.$emit('event_change_field', this.field.vmyatina, value)
		},
		corozia_rgavchina: function (value) {
			this.$emit('event_change_field', this.field.corozia_rgavchina, value)
		},
		skol: function (value) {
			this.$emit('event_change_field', this.field.skol, value)
		},
		treshina: function (value) {
			this.$emit('event_change_field', this.field.treshina, value)
		},
		zapotevanie: function (value) {
			this.$emit('event_change_field', this.field.zapotevanie, value)
		},
		zameneno: function (value) {
			this.$emit('event_change_field', this.field.zameneno, value)
		}

	},
	template: `
		<div :data-id="data_id" class="report-field report-field_diagnosis"
			:class="{
				'report-field_active': isActive(fieldss[value]),
				'report-field_positive': isPositive(fieldss[value]),
				'report-field_negative': isNegative(fieldss[value])
			}">
			<div  class="report-field__header">
			  <h5 class="report-field__title" v-text="stepByValue(field.name,fieldss[value])">
			  </h5> 
			  <div class="report-field__controls">
			    <label class="check check_binary check_binary_fail" :data-id="'element_' + data_id">
			      <input class="check_binary_fail" @click="unchec(false)"  v-model="inside_value"
			        :value="false" type="radio" :required="field.required"
			      />
			      <div class="check__icon icon icon_m icon_fail icon_circle">
			        <svg>
			          <use xlink:href="#icon_close_24"></use>
			        </svg>
			      </div>
			    </label>
			    <label class="check check_binary check_binary_success"
			    :data-id="'element_' + data_id">
			      <input v-model="inside_value" @click="unchec(true)" :value="true" type="radio"
			      :required="field.required"
			      />
			      <div class="check__icon icon icon_m icon_success icon_circle">
			        <svg>
			          <use xlink:href="#icon_check_24"></use>
			        </svg>
			      </div>
			    </label>
			  </div>
			</div>

			<div  class="report-field__body">
				<div v-if="!inside_value" class="report-field__control report-field__control_negative">
					<div  class="list list_xs">
						<div v-if="field.okrasheno" class="list__item">
							<label class="check check_m check_default">
								<input v-model="okrasheno" type="checkbox"/>
								<div class="check__icon"></div>
								<div class="check__title">Окрашено</div>
							</label>
						</div>
						<div v-if="field.skol_carapina" class="list__item">
							<label class="check check_m check_default">
								<input v-model="skol_carapina" type="checkbox"/>
								<div class="check__icon"></div>
								<div class="check__title">Скол/царапина</div>
							</label>
						</div>
						<div v-if="field.vmyatina" class="list__item">
							<label class="check check_m check_default">
								<input v-model="vmyatina" type="checkbox"/>
								<div class="check__icon"></div>
								<div class="check__title">Вмятина</div>
							</label>
						</div>
						<div v-if="field.corozia_rgavchina" class="list__item">
							<label class="check check_m check_default">
								<input v-model="corozia_rgavchina" type="checkbox"/>
								<div class="check__icon"></div>
								<div class="check__title">Коррозия/ржавчина</div>
							</label>
						</div>
						<div v-if="field.skol" class="list__item">
							<label class="check check_m check_default">
								<input v-model="skol" type="checkbox"/>
								<div class="check__icon"></div>
								<div class="check__title">Скол</div>
							</label>
						</div>
						<div v-if="field.treshina" class="list__item">
							<label class="check check_m check_default">
								<input v-model="treshina" type="checkbox"/>
								<div class="check__icon"></div>
								<div class="check__title">Трещина</div>
							</label>
						</div>
						<div v-if="field.zapotevaniezapotevanie" class="list__item">
							<label class="check check_m check_default">
								<input v-model="zapotevanie" type="checkbox"/>
								<div class="check__icon"></div>
								<div class="check__title">Запотевание</div>
							</label>
						</div>
						<div v-if="field.zameneno" class="list__item">
							<label class="check check_m check_default">
								<input v-model="zameneno" type="checkbox"/>
								<div class="check__icon"></div>
								<div class="check__title">Заменено</div>
							</label>
						</div>
					</div>
				</div>
			    <div  class="report-field__control report-field__control_negative field">
			      <div class="field__control control control_default control_m">
			        <textarea  v-model="comment" class="report-field__comment"></textarea>
			   	 	<div v-show="notField(inside_value, field.name, comment, data_id)" class="msg msg_field msg_fail">Поле комментарий обязательно для заполнения
	              </div>
			   	  </div>
			    </div>
			</div>
		</div>
	`
});


export const select2 = Vue.component('select2', {
	props: ['options', 'value', 'multiple', 'ajax', 'photo_url'],
	template: '#select2-template',
	data() {
		return {
			select2_name: "селект2 компонент"
		}
	},
	mounted: function () {
		this.rebind();
	},
	watch: {
		value: function (value) {
			if (this.ajax) {
				$(this.$el).val(value).select2({ ajax: this.getAjaxConfig(), multiple: this.multiple });
			} else {
				$(this.$el).val(value).select2({ multiple: this.multiple });
			}
		},
		options: function (options) {
			if (this.ajax) {
				$(this.$el).empty().select2({ ajax: this.getAjaxConfig(), multiple: this.multiple });
			} else {
				$(this.$el).empty().select2({ data: this.options, multiple: this.multiple });
			}
		}
	},
  computed: {
    slotPassed() {
      return !!this.$slots.default[0].text.length
    }
  },
  methods: {
    getAjaxConfig() {
      let self = this;
      return {
        url: this.ajax,
        delay: 250,
        data: function (params) {
          return {
            'page': params.page || 1,
            'querystring': params.term
          }
        },
        processResults: function (data) {
          let results = []
          for (let i = 0; i < data.objects.length; i++) {
            let id = data.objects[i].id;
            let text = data.objects[i].name;
            if (self.value) {
              if (!self.value.includes(`${id}`))
                results.push({id: id, text: text})
            } else {
              results.push({id: id, text: text})
            }

          }

          return {
            'results': results,
            'pagination': {
              'more': data.page.has_next
            }
          };
        },
        cache: true
      }
    },
    rebind: function () {
      let vm = this;

      if (this.ajax) {
        $(this.$el)
          .select2({
            multiple: this.multiple,
            ajax: this.getAjaxConfig(),
            data: this.options
          })
          .val(this.value)
          .trigger('change')
          .on('change', function () {
            vm.$emit('input', $(this).val());
          })
      } else {
        $(this.$el)
          .select2({ data: this.options, multiple: this.multiple })
          .val(this.value)
          .trigger('change')
          .on('change', function () {
            vm.$emit('input', $(this).val());
          })
      }

      this.$forceUpdate();
    }
  },
  destroyed: function () {
    $(this.$el).off().select2('destroy')
  }
})

export const slider = Vue.component('slider', {
	props: ['value', 'initial'],
	template: '<div :id="sliderId"></div>',
	data: function () {
		return {
			sliderId: this.uuid4(),
			slider: null,
		}
	},
	watch: {
		'value'(value) {
			//this.slider.noUiSlider.destroy();
			//noUiSlider.create(this.slider, {
		    //  start: value,
		    //  step: 5,
		    //  range: {
		    //    min: 0,
		    //    max: 100
		    //  },
		    //  pips: {
		    //    mode: 'positions',
		    //    density: 5,
		    //    values: [0, 25, 50, 75, 100],
		    //  }
		    //});
		    //this.slider.noUiSlider.set(value);
		},
	},

  mounted: function () {
    let vm = this;
    this.slider = document.getElementById(this.sliderId);
    let value = this.value ? this.value : 0;

    noUiSlider.create(this.slider, {
      start: value,
      step: 5,
      range: {
        min: 0,
        max: 100
      },
      pips: {
        mode: 'positions',
        density: 5,
        values: [0, 25, 50, 75, 100],
      }
    });

    this.slider.noUiSlider.on('slide', function(value) {
      vm.$emit('input', value);
    });
    this.slider.ontouchmove = function(e) {
		if (e.cancelable) {
    		console.log(e);
			e.preventDefault();
			//e.stopPropagation();
			this.slider.noUiSlider.destroy();
		}
	}

  },
  methods: {
    uuid4: function () {
      return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8)
        return v.toString(16)
      })
    },
  }
})


export const info_field_base =  Vue.component('info-field', {
	props: ['label','name','value','data_id','required','validate','type','humanize','error'],
	mixins: [step_mix, funcs],
	data() {
		return {
			inside_value: "",
			//inside_value: this.value, for checbox and text area
			comment: false
		}
	},
	mounted: function () {
		let value = this.value;
		if(this.humanize){
			value = this.changeHandler(value);
		}
		this.inside_value = value;
	},
	watch: {
		'inside_value'(value) {
			if(this.extract_number(value, this.humanize)!=this.value) {
				this.$emit('event_change_field', this.name, this.extract_number(value, this.humanize));
			}
			if(this.humanize){
				this.inside_value = this.changeHandler(value);
			}
		},
		// 'value'(value) {
		// 	this.inside_value = this.value; for checbox and textarea
		// },
	},
	template: `
		<div v-bind:class="{ 'list__item': type=='checkbox', 'report-field report-field_info': type=='text' || type=='number'|| type=='textarea' }">
			<div v-if="type=='text' || type=='number'|| type=='textarea'" class="report-field__header">
				<h5 class="report-field__title">{{label}}</h5>
			</div>
			<div v-if="type=='text' || type=='number'" class="report-field__body">
			  <div class="report-field__control report-field__control_positive">
			    <div class="field__control control control_default control_m">
			      <input :name="label"
			      v-model="inside_value" :type="type"/>
			    </div>
			    <div  v-show="getError(error, name)" class="msg msg_field msg_fail">
					<span v-for="name in error[name]">{{ translater(name) }}</span>
				</div>
			  </div>
			</div>


			<label v-if="type=='checkbox'"  class="check check_m check_default">
				<input v-model="inside_value" type="checkbox"/>
				<div class="check__icon"></div>
				<div  class="check__title">{{label}}</div>
			</label>

			<div  v-if="type=='textarea'" class="field__control control control_default control_m report_textarea">
				<textarea v-model="inside_value" class="report-field__comment"></textarea>
				<div v-show="required && notField(inside_value, label, comment, data_id, required)" class="msg msg_field msg_fail">Поле {{label}} обязательно для заполнения
				</div>
			</div>

		</div>
	`
});

export const modal_alert =  Vue.component('modal-alert', {
	props: ['name','alert','value','all_device'],
	components: {
		VModal: VModal,
	},
	watch: {
		'alert'(alert) {
			if(alert && (this.all_device || window.innerWidth > 1280)){
				this.show();
			}
		},

	},
	methods: {
		show () {
			this.$modal.show(this.name);
		},
		hide () {
			this.$modal.hide(this.name);
		}
	},
	template: `
		<modal height="auto" :name="name" :scrollable="true" :click-to-close="true" @closed="hide">
			<div style="color: red;font-size: 14px;  padding: 20px;">
				{{ value }}
			</div>
		</modal>
	`
});
