import Vue from 'vue';
import funcs from '../mixins/funcs';
import select2 from '../vue/select2-directive';
import ru from "vee-validate/dist/locale/ru";

VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});



export default Vue.component('executor', {
  delimiters: ['${', '}'],
  props: ['initialRole', 'initialUser',
        'userlist', 'selected', 'filial', 'type_expert', 'order'
      ],
  template: '#executor-template',
  mixins: [funcs],
  ready() {

    this.onChangeFilial();
  },
  mounted: function() {
    if(this.type_expert == "expert") {
        this.expert_type_name = "Назначить на подбор";
      }
      if(this.type_expert == "transferring") {
        this.expert_type_name = "Назначить на сдачу";
      }
      if(this.type_expert == "expert_check") {
        this.expert_type_name = "Назначить на перепроверку";
      }
    this.state.executors[this.userlist] = {'role': this.role, 'user': this.user};
    if (this.user && this.user.full_name) {
      var newOption = $("<option></option>").val(this.user.id).text(this.user.full_name);
      $(this.select).append(newOption).trigger('change');
    }
    $('select').select2({});
  },
  destroyed: function() {
    delete this.state.executors[this.userlist];
  },
  watch: {
    'state.filial'(newFilial) {
      this.onChangeFilial();
    },
    'role'(role) {
      this.user = '';
      this.$nextTick(function () {
       this.onChangeFilial();
      });
    },
    'user'(newUser) {
      this.state.executors[this.userlist] = {'role': this.role, 'user': parseInt(this.user)};
    },
    'newUser'(newUser) {
      this.onChangeFilial();
      this.state.executors[this.userlist] = {'role': this.role, 'user': parseInt(this.user)};
    },
    'newRole'(newRole) {
      //check if 'эксперт по сдаче' has already inserted - disable select
      if(newRole === 'transferring' && $('.select2-selection__rendered[title="эксперт по сдаче"]').length > 1 ){
        $(this.select).prop("disabled", true)
      }else {
        $(this.select).prop("disabled", false)
        this.user = '';
        this.$nextTick(function () {
         this.onChangeFilial();
        });
      }
    }
  },
  computed: {
    select: function() {
      return `#${this.userlist}`;
    },
    role: {
      get: function() {
        if (this.initialRole && !this.newRole)
          return this.initialRole;
        return this.newRole
      },
      set: function(role) {
        this.newRole = role;
      }
    },
    user: {
      get: function() {
        if (this.initialUser && !this.newUser)
          return this.initialUser;
        return this.newUser
      },
      set: function(user) {
        this.newUser = user;
      }
    }
  },
  directives: {
    select2: select2
  },
  data() {
    return {
      state: globalState,
      newRole: '',
      newUser: '',
      expert_type_name: '',

    }
  },
  methods: {
    onChangeFilial: function() {
      var group = this.role == 'operator' ? 'Оператор' : 'Эксперт';
      var exclude = [];
      for (var key in this.state.executors) {
        if (this.state.executors[key].role == this.role) {
          exclude.push(this.state.executors[key].user)
        }
      }
      //console.log('this',this)
      var filial = this.filial?this.filial.id:this.state.filial;
      this.loadToSelect2(this.select, '/user/user-list/', {groups__name: group, filials__id: filial}, 'id', 'full_name', exclude);
    },
    closeModal: function(user) {
    	this.$emit('event_child', user)
    }
  }
});