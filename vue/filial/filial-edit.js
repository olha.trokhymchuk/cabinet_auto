import Vue from 'vue';
import serverValidation from '../mixins/server-validation';
import ru from "vee-validate/dist/locale/ru";


VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});


new Vue({
  delimiters: ['${', '}'],
  el: '#filial-edit',
  mixins: [serverValidation],
  data: {
    csrf: csrf,
  },
  methods: {
    validateBeforeSubmit(e) {
      this.$validator.validateAll().then((result) => {
        if (result) {
          var formElement = document.querySelector("form");
          var formData = new FormData(formElement);
          var url = $("#form").attr('action');
          this.submit(url, formData);
        }
      })
    }
  }
})
