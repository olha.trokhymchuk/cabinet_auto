import Vue from 'vue';

import VModal from 'vue-js-modal';

import set_expert from '../components/set_expert';
import executor from '../components/executor';

import funcs from '../mixins/funcs';
import orders from '../mixins/orders';
import DateRange from '../components/date-range';
import dropdown from '../components/dropdown';
import {OrderChangeExecutor} from '../order/order-detail-components';
import axios from "axios";


Vue.use(VModal);


new Vue({
  delimiters: ['${', '}'],
  mixins: [funcs, orders],
  components: {
    'date-range': DateRange,
    'order-change-executor': OrderChangeExecutor,
    dropdown
  },
  el: '#filial-actions',
  mounted: function() {
    this.res = this.getQuerystringBySlash();
    this.getFilialDetail(this.res['filial-detail']);
    try {
      var page = this.res['params'].page;
      if (typeof page != 'undefined') {
        page = "&page=" + page;
      } else {
        page = "";
      }
    } catch(err){
      console.log(err);
    }
    try {
        var tab = this.res['params'].tab;
        if (typeof tab != 'undefined') {
          this.current_page[tab] = page;
          this.tab = "?tab=" + tab;
          this.openTab(this.res['params'].tab);
        } else {
          this.tab = "?tab=order_filial";
          this.current_page['order_filial'] = page;
          this.openTab('order_filial');
        }
    } catch (err) {
        this.current_page['order_filial'] = page;
        this.openTab('order_filial');
    }
  },
  data: {
    csrf: csrf,
    order_filial: "",
    pages: {},
    filial: {},
    current_url: "",
    current_page: {},
    order: {},
    report_list_filial: "",
    report_list: "",
    user_list_filial: "",
    objects: "",
    logs: "",
    deleteUrl: '/order/delete',
  },
  methods: {
    getFilialDetail(id) {
      $('#loader-wrapper').show();
      var self = this;
      var url = "/user/api/filial_detail/" + id + "/";
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_filial_data(data)
          },
          error: function(error) {
            console.log(error);
          }
      })
    },
    deleteFilial(id) {
      self = this;
      $.ajax({
        url : `/user/delete-filial/${id}/`,
        type: "POST",
        headers: {'X-CSRFToken': this.csrf },
        processData: false,
        contentType: false,
        success: function() {
          window.location = '/user/filial_list/';
        }
      });
    },
    openTab(tab) {
      this.current_url = tab
      this.set_query_params();
      var url = "/user/filial-detail" + this.new_part;
      window.history.pushState({path:url},'',url);
      $('.user_tab').hide();
      $('.tabs__item').removeClass("tabs__item_active");
      $('#' + tab + '_tab').addClass("tabs__item_active");
      $('#' + tab).show();
      if(this.order_filial == "" && tab ==  "order_filial") {
        this.getFilialOrders(this.res['filial-detail']);
        return;
      }
      if(this.report_list == "" && tab ==  "report_list_filial") {
        this.getFilialReports(this.res['filial-detail']);
      }
      if(this.user_list_filial == "" && tab ==  "user_list_filial") {
        this.getFilialUsers(this.res['filial-detail']);
      }
      if(this.logs == "" && tab ==  "filial_logs") {
        this.getFilialLogs(this.res['filial-detail']);
      }
    },
    getFilialOrders(id, url) {
      $('#loader-wrapper').show();
      var self = this;
      var defalut_url = "/order/api/" + this.current_url + "/" + id + "/" + this.tab + this.current_page[this.current_url];
      url = url !== undefined ? url : defalut_url;
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_order_data(data);
            $('#loader-wrapper').hide();
          },
          error: function(error) {
            console.log(error);
            $('#loader-wrapper').hide();
          }
      })
    },
    set_order_data(data) {
      this[this.current_url] = data.results;
      this.set_pagination(data);
    },
    set_pagination(data) {
      this.pages[this.current_url] = {
        'has_previous': data.previous,
        'has_next': data.next,
        'num_pages': data.lastPage,
        'number': data.current,
        'previous_page_number': data.current - 1,
        'next_page_number': data.current + 1,
        'count': data.countItemsOnPage

      };
    },
    set_filial_data(data) {
      this.filial = data;
      var filial_name = data.name ? data.name: "";
      $('title').text(filial_name);
    },
    set_query_params() {
      var page = this.current_page[this.current_url];
      if (typeof page == 'undefined') {
        page = "";
      }
      this.new_part = "/" + this.res['filial-detail'] + "/?tab=" + this.current_url + page;
    },
    changedExpert() {
      this.getFilialOrders(this.res['filial-detail']);
    },
    toPage(page) {
      this.current_page[this.current_url] = "&page=" + page;
      this.set_query_params();
      switch(this.current_url) {
        case 'report_list_filial':
          var first_url_part = 'user';
          var url = "/" + first_url_part + "/api/" + this.current_url + this.new_part;
          this.getFilialReports(null, url);
          break;
        case 'user_list_filial':
          var first_url_part = 'user';
          var url = "/" + first_url_part + "/api/" + this.current_url + this.new_part;
          this.getFilialUsers(null, url);
          break;
        case 'filial_logs':
          var first_url_part = 'user';
          var url = "/" + first_url_part + "/api/" + this.current_url + this.new_part;
          this.getFilialLogs(null, url);
          break;
        default:
          var first_url_part = 'order';
          var url = "/" + first_url_part + "/api/" + this.current_url + this.new_part;

          this.getFilialOrders(null, url);
          break;
      }
      url = "/user/filial-detail" + this.new_part;
      window.history.pushState({path:url},'',url);
    },
    getFilialReports(id, url) {
      $('#loader-wrapper').show();
      var self = this;
      var defalut_url = "/user/api/report_list_filial/" + id + "/" + this.tab + this.current_page[this.current_url];
      url = url !== undefined ? url : defalut_url;
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_report_data(data);
            $('#loader-wrapper').hide();
          },
          error: function(error) {
            console.log(error);
            $('#loader-wrapper').hide();
          }
      })
    },
    set_report_data(data) {
      this.report_list = data.results;
      this.set_pagination(data);
    },
    getFilialUsers(id, url) {
      $('#loader-wrapper').show();
      var self = this;
      var defalut_url = "/user/api/user_list_filial/" + id + "/" + this.tab + this.current_page[this.current_url];
      url = url !== undefined ? url : defalut_url;
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_user_data(data);
            $('#loader-wrapper').hide();
          },
          error: function(error) {
            console.log(error);
            $('#loader-wrapper').hide();
          }
      })
    },
    set_user_data(data) {
      this.user_list_filial = this.objects = data.results;
      this.set_pagination(data);
    },
    getFilialLogs(id, url) {
      $('#loader-wrapper').show();
      var self = this;
      var defalut_url = "/user/api/filial_logs/" + id + "/" + this.tab + this.current_page[this.current_url];;
      url = url !== undefined ? url : defalut_url;
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_filial_log_data(data);
            $('#loader-wrapper').hide();
          },
          error: function(error) {
            console.log(error);
            $('#loader-wrapper').hide();
          }
      })
    },
    set_filial_log_data(data) {
      this.logs = data.results;
      this.set_pagination(data);
    },


    //detect type array
    getObjFromTypeString(type) {
			let objName = null;
			switch (type) {
				case 'order':
					objName = 'orders'
					break;
				case 'report':
					objName = 'reports'
					break;
				case 'user':
					objName = 'users'
					break;
			}
			return objName;
		},
		//copy functions from mixin list.js - removeObject and closeObject
		removeObject(obj, type) {
			var isConfirmedAction = confirm('Вы уверены что хотите удалить ?')
			if(isConfirmedAction){
				if (!type) return;
				var self = this;
				var objList = this.getObjFromTypeString(type)
				$('#loader-wrapper').show();
				$.ajax({
					url: `/${type}/delete/${obj.id}/`,
					type: "POST",
					data: { csrfmiddlewaretoken: this.csrf },
					success: function () {
            try {
              var index = self[objList].indexOf(obj);
              self[objList].splice(index, 1);
            } catch (error) {
              console.log(error)
            }
						$('.dropdown_open').removeClass('dropdown_open');
					},
					complete: function () {
						$('#loader-wrapper').hide();
					}
				});
			}
		},
		closeObject(obj, type) {
			if (!type) return;
			var self = this;
			var objList = this.getObjFromTypeString(type)
			$('#loader-wrapper').show();
			$.ajax({
				url: `/${type}/close/${obj.id}/`,
				type: "POST",
				data: { csrfmiddlewaretoken: this.csrf },
				success: function () {
          try {
            var index = self[objList].indexOf(obj);
            self[objList].splice(index, 1);
          } catch (error) {
            console.log(error)
          }
					$('.dropdown_open').removeClass('dropdown_open');
				},
				complete: function () {
					$('#loader-wrapper').hide();
				}
			});
		},
        handleCheckChange(event, id){
          let url = '/user/update_employee_email_sender/';
          let formData = new FormData();
          formData.append('id', id);
          formData.append('send_email', Number(event.target.checked));
          let headers = {
            "X-CSRFToken": this.csrf,
          };
        axios.post(url, formData, {headers}).then(response => {})
        }
  }
});