import Vue from 'vue';
import list from '../mixins/list';
import search from '../mixins/search';


new Vue({
  delimiters: ['${', '}'],
  el: '#filials',
  mixins: [list, search],
  data: {
    deleteUrl: '/user/delete-filial'
  },
  methods:{
    openDropdown(e){
      $(e.target).parents('.dropdown').toggleClass('dropdown_open');
    }
  },
});
