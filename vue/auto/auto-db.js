import Vue from 'vue';
import axios from 'axios';
import dropdown from '../components/dropdown';

Vue.directive('select', {
    twoWay: true,
    bind: function (el, binding, vnode) {
        $(el).select2().on("select2:select", (e) => { 
            el.dispatchEvent(new Event('change', { target: e.target }));
        });
    },
});

var autodb = new Vue({
    delimiters: ['${', '}'],
    el: '#auto-db',
    data(){
        return{
            allList:false,
            autoSelected: 'test',
            choiseMode: '',
            choiseGeneration: '',
            typeButt: 'Все марки',
            allCars: [],
            models: [],
            generation: [],
        }

    },
    created(){
        this.getAllCar();
        $('.auto-list-text').show();
        $('.section__body').show();
        $('#loader-wrapper').hide();

    },
    watch:{
        choiseMode(){
            this.getModelCar();
        },
        choiseGeneration(){
            this.getGenerationsCar();
        }
    },
    methods: {
        openMoreAutoList(){
            this.allList = !this.allList;
            if(this.allList){
                this.typeButt = 'Cкрыть'
            }else{
                this.typeButt = 'Все марки'
            }
        },
        getAllCar(){
            let dataAttr = $('.mark_auto').attr('data-attr');
            axios.get('/auto/marks/').then((response) =>{
                this.allCars = response.data;

                this.allCars.map((item) =>{
                    if(item.id == dataAttr){
                        this.choiseMode = item.id;
                        this.getModelCar();
                    }
                })
            })
            $('.mark_auto').val('US').trigger('change');
            $('.mark_auto').val('US').change();
        },
        getModelCar(){
            let dataAttr = $('.model_auto').attr('data-attr');
            this.models = [];
            axios.get('/auto/models/'+this.choiseMode).then((response) =>{
                this.models = response.data.all_models;

                this.models.map((item) =>{
                    if(item.id == dataAttr){
                        this.choiseGeneration = item.id;
                    }
                })

            })
        },
        getGenerationsCar(){
            this.generation = [];
            axios.get('/auto/generations/'+this.choiseGeneration).then((response) =>{
                this.generation = response.data;
            })
        },
    },
});