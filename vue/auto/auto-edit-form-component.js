import Vue from 'vue';
import VModal from 'vue-js-modal';
import select2 from '../vue/select2-directive';
import Grid from 'vue-js-grid';

import funcs from '../mixins/funcs';
import automix from '../mixins/auto-mix';
import gallery from '../components/gallery';
import photos_mix from '../mixins/photos_mix';
import VueResource from 'vue-resource';

Vue.use(VueResource);
Vue.use(Grid)

Vue.use(VModal);

export default Vue.component('car-edit-form', {
  delimiters: ['${', '}'],
  template: '#car-edit',
  props: ['autoAction', 'addReportIsVisible', 'addAutoIsVisible'],
  components: {
    FileUpload: VueUploadComponent,
  },
  mixins: [funcs, automix, photos_mix],
  directives: {
    select2: select2,
  }, 
  watch: {
    'editFile.show'(newValue, oldValue) {
      if (!newValue && oldValue) {
        this.$refs.upload.update(this.editFile.id, { error: this.editFile.error || '' });
        this.$modal.hide('edit-modal');
      }
      if (newValue) {
        this.$nextTick(function () {
          if (!this.$refs.editImage) {
            return
          }
          let cropper = new Cropper(this.$refs.editImage, {
            autoCrop: false,
          })
          this.editFile = {
            ...this.editFile,
            cropper
          }
        })
      }
    },
    'addData.show'(show) {
      if (show) {
        this.addData.name = ''
        this.addData.type = ''
        this.addData.content = ''
      }
    },
    'method'(method) {
      this.$nextTick(function() {
        $('select').select2({});
        bindLoaders();
      })
    },
    'url'(url) {
      clearTimeout(this.autoRuTimer);
      let self = this;

      this.autoRuTimer = setTimeout(function () {
        if (self.method !== 1 && self.method !== '1')
          return

        self.$validator.validate('url').then((success) => {
          if (success) {
            let handlerUrl = self.detectSiteUrl(self.url);
            let data = new FormData();
            data.append('csrfmiddlewaretoken', self.csrf);
            data.append('url', self.url);
            $("input[name='url']").prop('disabled', true);
            $('#loader-wrapper').show();
            $('.loade_bar').show();
            $.ajax({
              url: handlerUrl,
              data : data,
              cache: false,
              contentType: false,
              processData: false,
              xhr: function () {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        $('#process').text(Math.ceil(percentComplete * 100) + '%')
                    }
                }, false);
                return xhr;
              },
              type: 'POST',
              data: data,
              success: function(data) {
                self.cost =  data.cost;
                self.mileage =  data.mileage;
                $("input[name='url']").prop('disabled', false);
                self.method = 2;
                self.files = [];
                if (data.photos) {
                  data.photos.forEach(function (base64, i) {
                    let file = self.base64ImageToFile(base64)
                    let photo = {
                      blob: URL.createObjectURL(file),
                      file: file,
                      type: 'image/jpeg',
                      size: file.size,
                      id: i,
                      autoRu: true
                    }

                    self.autoRuFiles.push(photo);
                  })
                $('#loader-wrapper').hide();

                }

                self.$nextTick(function () {
                  unBindLoaders();
                  let mark = data['mark_auto'];
                  let model = data['model_auto'];
                  load(mark, model, function() {
                    for (let key in data) {
                      $(`[name='${key}']`).val(data[key]);
                    }

                    $('select').select2({});
                    self.$forceUpdate();
                  })
                });
                $('#loader-wrapper').hide();
              },
              error: function (error) {
                  if(error.responseText.indexOf('GrabTimeoutError') !== -1){
                    self.$validator.errors.add(url, "Время выполнения истекло");
                    self.errors.add('url', "Время выполнения истекло");
                  }
                  $("input[name='url']").prop('disabled', false);
                  $('#loader-wrapper').hide();
              }
            })
          }
        })
      }, 2000)
    },
    'cost'(newValue) {
      this.changeHandler(newValue, 'cost');
    },
    'mileage'(newValue) {
      this.changeHandler(newValue, 'mileage');
    },
  },
  mounted: function() {
    let IsReport = window.location.href.includes('report_auto') ? true : false;
    
    if(IsReport){
      $('.save_auto').addClass('disabled');
    }
  },
  data: function() {
    return {
      mileage: typeof mileage != "undefined" ? mileage: 0,
      cost: typeof cost != "undefined" ? cost : 0 ,
      autoRuTimer: null,
      method: store.method,
      files: [],
      url: '',
      uploadedFiles: store.uploadedFiles,
      minSize: 1024,
      size: 1024 * 1024 * 10,
      name: 'photo',
      dropDirectory: true,
      drop: true,
      addIndex: false,
      thread: 3,
      autoCompress: 1024 * 1024,
      uploadAuto: false,
      isOption: false,
      isPositionChanged: false,
      addData: {
        show: false,
        name: '',
        type: '',
        content: '',
      },
      csrf: store.csrf,
      editFile: {
        show: false,
        name: '',
      },
      mark_auto: '',
      model_auto: '',
      generation: ''
    }
  },
  methods: {
    detectSiteUrl(url) {
      if(url.indexOf('auto.ru') !== -1) {
        return '/auto/get-data-from-auto-ru/';
      }
      if(url.indexOf('avito.ru') !== -1) {
        $(".label_default").html('ссылка на объявление avito.ru');
        return '/auto/get-data-from-avito-ru/';
      }
    },
    upCase(e){
      var target = e.target;
      var value = target.value;
      value = this.transliterate(value);
      target.value = value.toUpperCase();
    },
    showValid(e){
      var target = e.target;
      var container = target.closest('.fieldset__item_vin');
      var erMsg = container.lastElementChild;
      
      if(erMsg.classList.contains('msg_fail')){
        if(erMsg.innerText.length > 0){
          erMsg.style.display = 'block'
        }
      }
    },
    getFileBySortKey(sortKey) {
      for (var i = 0; i < this.uploadedFiles.length; i++) {
        if (this.uploadedFiles[i].sort_key == sortKey) return this.uploadedFiles[i];
      }
      return null;
    },
    onChangeSorting(e) {
      let self = this;
      e.items.forEach(function(galleryItem) {
        let file = self.getFileBySortKey(galleryItem.item.sort_key);
        file.sort_key = galleryItem.sort;
      })
      this.isPositionChanged = true;
    },
    onEditUploadedFileShow(file) {
      this.editFile = { ...file, type: 'image/jpeg', name: file.id }
      this.$refs.upload.update(file, { error: 'edit' })
      this.$modal.show('edit-modal');
    },
    onEditAutoRuFileShow(file) {
      this.editFile = { ...file, type: 'image/jpeg', name: file.id }
      this.$refs.upload.update(file, { error: 'edit' })
      this.$modal.show('edit-modal');
    },
    onEditorFile() {
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        this.editFile.show = false
        return
      }

      let data = {
        name: this.editFile.name,
      }

      if (this.editFile.cropper) {
        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
        let arr = new Uint8Array(binStr.length);
        for (let i = 0; i < binStr.length; i++) {
          arr[i] = binStr.charCodeAt(i);
        }
        data.file = new File([arr], data.name, { type: this.editFile.type });
        data.size = data.file.size;
      }
      this.$refs.upload.update(this.editFile.id, data);
      this.editFile.error = '';
      this.editFile.show = false;
    },
    onEditorUploadedFile() {
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        this.editFile.show = false
        return
      }

      let data = {
        name: this.editFile.name,
      }

      if (this.editFile.cropper) {
        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
        let arr = new Uint8Array(binStr.length);
        for (let i = 0; i < binStr.length; i++) {
          arr[i] = binStr.charCodeAt(i);
        }
        data.file = new File([arr], data.name, { type: this.editFile.type });
        data.size = data.file.size;
      }

      this.editFile.error = '';
      this.editFile.show = false;
      this.updateUploadedFile(data.file, this.editFile.id, this.editFile.sort_key);
    },
    onEditorAutoRuFile() {
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        this.editFile.show = false
        return
      }

      let data = {
        name: this.editFile.name,
      }

      if (this.editFile.cropper) {
        let binStr = atob(this.editFile.cropper.getCroppedCanvas().toDataURL(this.editFile.type).split(',')[1]);
        let arr = new Uint8Array(binStr.length);
        for (let i = 0; i < binStr.length; i++) {
          arr[i] = binStr.charCodeAt(i);
        }
        data.file = new File([arr], data.name, { type: this.editFile.type });
        data.size = data.file.size;
      }

      this.editFile.error = '';
      this.editFile.show = false;
      let self = this;
      let index = this.autoRuFiles.findIndex(function (file) {
        return file.id === self.editFile.id;
      })

      this.autoRuFiles[index].file = data.file;
      this.autoRuFiles[index].blob = URL.createObjectURL(data.file)
    },
    // add folder
    onAddFolder() {
      if (!this.$refs.upload.features.directory) {
        this.alert('Your browser does not support')
        return
      }
      let input = this.$refs.upload.$el.querySelector('input')
      input.directory = true
      input.webkitdirectory = true
      this.directory = true
      input.onclick = null
      input.click()
      input.onclick = (e) => {
        this.directory = false
        input.directory = false
        input.webkitdirectory = false
      }
    },
    onAddData() {
      this.addData.show = false
      if (!this.$refs.upload.features.html5) {
        this.alert('Your browser does not support')
        return
      }
      let file = new window.File([this.addData.content], this.addData.name, {
        type: this.addData.type,
      })
      this.$refs.upload.add(file)
    },
    updateUploadedFile(file, fileId, sortKey) {
      var self = this;
      $('#loader-wrapper').show();
      let data = new FormData();
      data.append('csrfmiddlewaretoken', this.csrf);
      data.append('photo', file);

      $.ajax({
        url: `/auto/update-photo-auto/${fileId}/${sortKey}`,
        method: 'POST',
        data : data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
          let newUploadedFile = JSON.parse(data);
          let index = self.uploadedFiles.map(function(f) { return f.id; }).indexOf(fileId);
          self.uploadedFiles.splice(index, 1, newUploadedFile);
          $('#loader-wrapper').hide();
        },
        error: function (error) {
            console.log(error);
        }
      });
    },
    removeAutoLoadFile(file) {
      let conf = confirm('Вы уверены что хотите удалить данный файл ?');
      if(conf){
        $.ajax({
          url: `/auto/delete-recommended-photos/`,
           method: 'POST',
          data : {csrfmiddlewaretoken: this.csrf, photo_id: file },
          success: function (data) {
             console.log(data);
            $('.gallery__item[data-photo_id="'+file+'"]').hide();
          },
          error: function (error) {
              console.log(error);
          }
        });
      }
    },
    removeUploadedFile(file) {
      var self = this;
      $('#loader-wrapper').show();
      $.ajax({
        url: '/auto/delete-photo-auto/' + file.id,
        method: 'POST',
        data : {csrfmiddlewaretoken: this.csrf},
        success: function (data) {
          let index = self.uploadedFiles.indexOf(file);
          self.uploadedFiles.splice(index, 1);
          $('#loader-wrapper').hide();
        },
        error: function (error) {
            console.log(error);
        }
      });
    },
    removeAutoRuFile(file) {
      let index = this.autoRuFiles.indexOf(file);
      this.autoRuFiles.splice(index, 1);
    },
  }
})
