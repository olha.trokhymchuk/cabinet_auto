import Vue from 'vue';
import Datepicker from 'vuejs-datepicker';
import select2 from '../vue/select2-directive';
import filterMixin from '../mixins/filter';
import range from '../mixins/range';


function getParams (url) {
  var params = {};
  var parser = document.createElement('a');
  parser.href = url;
  var query = parser.search.substring(1);
  var vars = query.split('&');

  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=');
    params[pair[0]] = decodeURIComponent(pair[1]);
  }

  return params;
};


var filter = new Vue({
  delimiters: ['${', '}'],
  el: '#filter',
  mixins: [filterMixin, range],
  data: {
    initialization: true,
    marks: [],
    models: [],
    generations: [],
    mark_auto: '',
    model_auto: '',
    generation: '',
    cost__lte: '',
    body_type_auto: [],
    transmission_type: [],
    color_auto: [],
    engine_type: [],
    drive_type: [],
    salon_auto: [],
    color_salon: [],
    mileage__lt: '',
    year__gte: '',
    year__lte: '',
    capacity__gte: '',
    capacity__lte: '',
    engine_capacity__gte: '',
    engine_capacity__lte: '',
    horsepower__gte: '',
    horsepower__lte: '',
    equipment: '',
    owners__lte: '',
    filter_recommended: false,
    recommended: false,
    yearsRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
    capacitiesRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
    horsepowersRange: {init: [], options0: [], options1: [], select0: 0, select1: 0},
    filterValues: {},
    filterFields: ['mark_auto', 'model_auto', 'generation', 'body_type_auto',
                   'color_auto', 'engine_type', 'drive_type', 'cost__lte', 'equipment',
                   'salon_auto', 'color_salon', 'mileage__lt', 'year__gte', 'year__lte',
                   'engine_capacity__gte', 'engine_capacity__lte', 'horsepower__gte', 'horsepower__lte',
                   'transmission_type', 'owners__lte', 'recommended'
                   ],
    rangeFields: ['yearsRange', 'capacitiesRange', 'horsepowersRange'],
    loading: false,
  },
  watch: {
    year__gte: function(year) {
      this.yearsRange.select0 = parseInt(year);
    },
    year__lte: function(year) {
      this.yearsRange.select1 = parseInt(year);
    },
    engine_capacity__gte: function(capacity) {
      this.capacitiesRange.select0 = parseFloat(capacity);
    },
    engine_capacity__lte: function(capacity) {
      this.capacitiesRange.select1 = parseFloat(capacity);
    },
    horsepower__gte: function(horsepower) {
      this.horsepowersRange.select0 = parseInt(horsepower);
    },
    horsepower__lte: function(horsepower) {
      this.horsepowersRange.select1 = parseInt(horsepower);
    },
    mark_auto: function(newMark) {
      if (!this.initialization) {
        this.model_auto = '';
        this.generation = '';
      }
    },
    model_auto: function(newModel) {
      if (!this.initialization) {
        this.generation = '';
      }
    }
  },
  mounted: function() {
    this.loadMarks();
    this.loadInitial();
    var url = new URL(window.location.href);
    var params = getParams(url);
    for (var key in params) {
      var value = params[key];
      if (this.hasOwnProperty(key)) {
        if (value.includes(',')) {
          this[key] = value.split(',');
        } else if (this[key].constructor === Array) {
          this[key] = [value];
        } else {
          this[key] = value;
        }
      }
    }
  },
  directives: {
    select2: select2
  },
  methods: {
    loadMarks() {
      var self = this;
      $.get("/auto/marks/", function (data) {
        self.marks = data;
      });
    },
    loadModels(mark) {
      var self = this;
      $.get("/auto/models/" + mark, function (data) {
        self.models = data;
      });
    },
    loadGenerations(model) {
      var self = this;
      $.get("/auto/generations/" + model, function (data) {
        self.generations = data;
      });
    },
    loadInitial() {
      var self = this;
      $.ajax({
        url: '/auto/filter/',
        cache: false,
        method: 'GET',
        success: function (data) {
          self.filterValues = data;
          self.loading = false;
          self.yearsRange.init = data.years.reverse();
          self.yearsRange.select0 = self.year__gte;
          self.yearsRange.options0 = data.years;
          self.yearsRange.select1 = self.year__lte;
          self.yearsRange.options1 = data.years;

          self.capacitiesRange.init = data.capacities;
          self.capacitiesRange.select0 = parseFloat(self.engine_capacity__gte);
          self.capacitiesRange.options0 = data.capacities;
          self.capacitiesRange.select1 = parseFloat(self.engine_capacity__lte);
          self.capacitiesRange.options1 = data.capacities;

          self.horsepowersRange.init = data.horsepowers;
          self.horsepowersRange.select0 = parseInt(self.horsepower__gte);
          self.horsepowersRange.options0 = data.horsepowers;
          self.horsepowersRange.select1 = parseInt(self.horsepower__lte);
          self.horsepowersRange.options1 = data.horsepowers;
          self.initialization = false;
        },
        error: function (error) {
          self.loading = false;
          console.log(error);
        }
      });
    }
  }
})
