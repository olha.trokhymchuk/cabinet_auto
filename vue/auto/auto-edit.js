import Vue from 'vue';
import carEditForm from './auto-edit-form-component';
import automix from '../mixins/auto-mix';
import  myEvent  from '../vue/bus';


new Vue({
  el: '#auto-edit',
  mixins: [automix],
  components: {
    carEditForm,
  },
  data: {
    objects: [],
  },
  mounted () {
      let self = this;
      document.addEventListener("addReport", function(e) {
        e.target.classList.add('redirect-clicked');
        self.saveAndAddReport();
      })
  },
  
})

