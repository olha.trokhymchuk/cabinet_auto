import Vue from 'vue';
import list from '../mixins/list';
import funcs from '../mixins/funcs';
import search from '../mixins/search';
import select2 from '../vue/select2-directive';

var app = new Vue({
  delimiters: ['${', '}'],
  el: '#cars',
  mixins: [list, search, funcs],
  data: {
    deleteUrl: '/auto/delete-auto'
  },
  directives: {
    select2: select2
  },
  methods: {
    addReport(e, car) {
      const url = `/report/add-report-for-auto/${car.id}/`;
      let target = e.target;

      target.closest('.menu-controls').classList.add('disabled');

      $.ajax({
        url: url,
        headers: {'X-CSRFToken': this.csrf },
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data) {
          if (data.redirect) {
            window.location.href = data.redirect;
          } else if (data.errors) {
            console.log(data.errors)
          }
        }
      })
    }
  }
})
