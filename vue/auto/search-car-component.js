import Vue from 'vue';

import { bus } from '../report/report-bus';
import funcs from '../mixins/funcs';
import automix from '../mixins/auto-mix';
import axios from 'axios';

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  } else {
    return uri + separator + key + "=" + value;
  }
}

function getQuerystring(key) {
  var query = window.location.search.substring(1);
  var vars = query.split("&");
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    if (pair[0] == key) {
      return pair[1];
    }
  }
}

export default Vue.component('search-car', {
  delimiters: ['${', '}'],
  props: ['multiple', 'filtered', 'objectsUrl', 'fieldssId', 'create_report'],
  template: '#search-car',
  mixins: [automix, funcs],
  mounted: function() {
    var countLoadMore = 2;
    var self = this;
    document.getElementById('loadMore').addEventListener("click",function(e){
      self.updateObjects(countLoadMore++)
   });
  },
  data: function() {
    return {
      store: store,
      selected: [],
      querystring: '',
      timer: null,
      objects: [],
      count: 0,
      loading: false,
      csrf: csrf,
      url: window.location.href,
      deleteUrl: '',
      page: {
        num_pages: 1,
        number: 1,
        has_previous: false,
        has_next: false,
        has_other_pages: false,
        start_index: 1,
        end_index: 1
      },
      processing: true,
      notFound : false,
    }
  },
  watch: {
    'querystring'(querystring) {
      var self = this;
      clearTimeout(this.timer);
      this.notFound = false;
      this.processing = true;
      this.timer = setTimeout(function () {
        self.selected.splice(0, self.selected.length);
        var query = updateQueryStringParameter('', 'querystring', self.querystring);
        var title = $(document).find("title").text();
        window.history.pushState(null, title, query);
        var id = self.fieldssId !== undefined ? self.fieldssId : '';
        self.url = self.objectsUrl.split('?')[0] + id + query;
        self.updateObjects();
        self.selected = [];
      }, 1000)
    },
    'selected'(newValue) {
      if (newValue.length > 0){
        $('#submit-button').show();
        $('#report-button').show();
      }
      else{
        $('#submit-button').hide();
        $('#report-button').hide();
      }
    }
  },
  methods: {
    updateObjects(loadMore = false) {
      if (this.querystring === '' || !this.querystring.replace(/\s/g, '').length){
        this.processing = false;
        this.objects = [];
        return;
      }

      var self = this;
      var loadMoreBtn = document.getElementById('loadMore');
      this.loading = true;

      $.ajax({
        url: this.url,
        cache: false,
        method: 'GET',
        success: function (data) {

          self.getGooglePhoto(data.objects);
          if(!loadMore){
            self.objects = data.objects.slice(0, 10);
          }else{
            self.objects = data.objects.slice(0, 10 * loadMore);
          }
          
          if(loadMoreBtn != null && data.objects.length > self.objects.length){
            loadMoreBtn.classList.remove('disabled');
          }else if(loadMoreBtn != null ){
            loadMoreBtn.classList.add('disabled')
          }
          
          self.count = data.count;
          self.page = data.page;

          self.loading = false;
          self.processing = false;
          
          data.count <= 0 ? self.notFound = true : self.notFound = false;
        },
        error: function (error) {
          self.loading = false;
          console.log(error);
        }
      });
    },
    getGooglePhoto(objects){
      let objectsId = [];
      objects.map((item) => {
        objectsId.push(item.last_report_id);
      });
      //let self = this;
      // $.ajax({
      //   url: '/google_photo/reports-cover-photo/',
      //   data: {
      //     csrfmiddlewaretoken: this.csrf,
      //     requestIds: JSON.stringify(objectsId),
      //   },
      //   method: 'POST',
      //   success: function (data) {
      //     let key;
      //     console.log(data);
      //     for(key in data){
      //       self.objects.map(item =>{
      //         console.log('key', key);
      //         console.log('item.id', item.last_report_id);
      //         console.log('data[key]', data[key]);
      //         if(item.last_report_id == key){
      //           item.image = data[key];
      //         }
      //       })
      //     }
      //     console.log('self.objects: ', self.objects)
      //   },
      //   error: function (error) {
      //   }
      // })
    },
    toggle(car) {
      var index = this.selected.indexOf(car);
      bus.$emit('event_select_auto', car);
      if (index < 0) {
        if (this.multiple) {
          this.selected.push(car);
        } else {
          this.selected = [car]
        }
      } else {
        this.selected.splice(index, 1);
      }

      this.$forceUpdate();
    },
    openAutoForm() {
      let domain = (new URL(window.location.href));
      let self = this;
      let isEdited = window.location.href.includes('report-edit') ? true : false;
      let isOrder = window.location.href.includes('order/add-auto/') ? true : false;
      let orderId = window.location.href.split('/')[5];

      switch(true){
        case isEdited === true:
          axios.get('/report/view-api3/' + self.fieldssId).then((response) => {
            self.carId = response.data.auto.id;
          }).then(() => {
           window.location.href = domain.origin + '/auto/edit-auto/' + this.carId;
          });
        break;
        case isOrder === true:
          window.location.href = domain.origin + '/auto/add-auto?report_auto&orderId='+ orderId;
        break;
        default:
          window.location.href = domain.origin + '/auto/add-auto?report_auto';
        break;
      }
    },
  }
})
