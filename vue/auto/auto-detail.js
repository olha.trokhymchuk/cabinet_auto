import Vue from 'vue';


var autoDetail = new Vue({
  delimiters: ['${', '}'],
  el: '#auto-detail',
  data: {
    csrf: csrf,
    form: {
      vin:{
        value:'321'
      }
    },
  },
  mounted(){
    window.addEventListener('load', () => {
      this.detectAnchorLink()
    })
  },
  methods: {
    openDropdown(e) {
      $(e.target).parents('.dropdown').toggleClass('dropdown_open');
    },
    addReport(e, id) {
      const url = `/report/add-report-for-auto/${id}/`;
      let target = e.target;

      target.closest('.menu-controls').classList.add('disabled');

      $.ajax({
        url: url,
        headers: {'X-CSRFToken': this.csrf },
        processData: false,
        contentType: false,
        type: 'POST',
        success: function(data) {
          target.closest('.menu-controls').classList.remove('disabled');
          if (data.redirect) {
            window.location.href = data.redirect;
          } else if (data.errors) {
            console.log(data.errors)
          }
        }
      })
    },
    removeCar(id) {
      self = this;
      $.ajax({
        url : `/auto/delete-auto/${id}/`,
        type: "POST",
        headers: {'X-CSRFToken': this.csrf },
        processData: false,
        contentType: false,
        success: function() {
          window.location = '/auto/all-auto/';
        }
      });
    },

    removeRecommendedCar(id) {
      let confirmRemoveCar = confirm('Вы уверены что хотите удалить ?');
      if(confirmRemoveCar){
        self = this;
        $.ajax({
          url : `/auto/delete-recommended-auto/${id}/`,
          type: "POST",
          headers: {'X-CSRFToken': this.csrf },
          processData: false,
          contentType: false,
          success: function() {
            window.location = '/auto/recommended-auto/';
          }
        });
      }
    },
    detectAnchorLink(){
      try {
        if(window.location.hash) {
          var hash = window.location.hash;
          if($(hash).length){
            $('html,body').animate({scrollTop: $(hash).offset().top},300);
          }
        }
      } catch (error) {
        console.error(error)
      }
    },
    salesObject(id) {
      var isConfirmedAction = confirm('Вы уверены что хотите пометить авто как “Проданное”?')
      if (isConfirmedAction) {
        var self = this;
        $('#loader-wrapper').show();
        $.ajax({
          url: `/auto/sales-auto/${id}/`,
          type: "POST",
          data: {csrfmiddlewaretoken: this.csrf},
          success: function (count) {
            $('#loader-wrapper').hide();
            $('.dropdown_open').removeClass('dropdown_open');
          },
          error: function (e) {
            $('#loader-wrapper').hide();
            $('.dropdown_open').removeClass('dropdown_open');
          }
        });
      }
    },
  }
});