import Vue from 'vue'
import Vue2TouchEvents from 'vue2-touch-events'
// Vue.prototype.$bus = new Vue({})
Vue.use(Vue2TouchEvents, {
    disableClick: false,
    touchClass: '',
    tapTolerance: 10,
    swipeTolerance: 30,
    longTapTimeInterval: 400
})
var myEvent = new CustomEvent("addReport", {
    // detail: {
    //     username: "davidwalsh"
    // }
});

export default myEvent;
