import Vue from 'vue';
import ru from "vee-validate/dist/locale/ru";
import funcs from '../mixins/funcs';
import list from '../mixins/list';
import orders from '../mixins/orders';

import VModal from 'vue-js-modal';

import set_expert from '../components/set_expert';
import executor from '../components/executor';
import dropdown from '../components/dropdown';
import {OrderChangeExecutor} from '../order/order-detail-components';
import DateRange from '../components/date-range';

VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});

Vue.use(VModal);

var app = new Vue({
  delimiters: ['${', '}'],
  el: '#user-detail',
  mixins: [funcs, list, orders],

  directives: {

  },
  components: {
    set_expert,
    executor,
    'order-change-executor': OrderChangeExecutor,
    dropdown,
    'date-range': DateRange,
  },
  mounted: function() {
    this.res = this.getQuerystringBySlash();
    this.getUserDetail(this.res['user_detail']);
    // this.getСounts(this.res['user_detail']);
    try {
      var page = this.res['params'].page;
      if (typeof page != 'undefined') {
        page = "&page=" + page;
      } else {
        page = "";
      }
    } catch(err){
      console.log(err);
    }
    try {
        var tab = this.res['params'].tab;
        if (typeof tab != 'undefined') {
          this.current_page[tab] = page;
          this.tab = "?tab=" + tab;
          this.openTab(this.res['params'].tab);
        } else {
          this.tab = "?tab=selection";
          this.current_page['selection'] = page;
          this.openTab('selection');
        }
    } catch (err) {
        this.current_page['selection'] = page;
        this.openTab('selection');
    }
  },
  data: {
    csrf: csrf,
    user: {},
    selection: "",
    expert_day: "",
    other: "",
    transferringord: "",
    outside_diagnose: "",
    report_list: "",
    reviews_list_user: "",
    pages: {},
    count: "",
    current_url: "",
    user_logs: "",
    logs: "",
    type_expert:"",
    order: {},
    current_page: {},
    tab: "",
    new_part: "",

  },
  watch: {
    selection: function (newObjects) {
      if(newObjects.length > 0){
        this.updateOrderCriteriesSlider();
      }
    },
    outside_diagnose: function (newObjects) {
      if(newObjects.length > 0){
        this.updateOrderCriteriesSlider();
      }
    },
    expert_day: function (newObjects) {
      if(newObjects.length > 0){
        this.updateOrderCriteriesSlider();
      }
    },
    other: function (newObjects) {
      if(newObjects.length > 0){
        this.updateOrderCriteriesSlider();
      }
    },
    transferringord: function (newObjects) {
      if(newObjects.length > 0){
        this.updateOrderCriteriesSlider();
      }
    },
  },
  methods: {
    getUserDetail(id) {
      $('#loader-wrapper').show();
      var self = this;
      var url = "/user/api/detail/" + id + "/";
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_user_data(data)
          },
          error: function(error) {
            console.log(error);
          }
      })
    },
    getСounts(id) {
      var self = this;
      var url = "/user/api/counts/" + id + "/";
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_counts_data(data)
          },
          error: function(error) {
            console.log(error);
          }
      })
    },
    getUserOrders(id, url) {
      $('#loader-wrapper').show();
      var self = this;
      this.defalut_url = "/order/api/" + this.current_url + "/" + id + "/" + this.tab + this.current_page[this.current_url];
      url = url !== undefined ? url : this.defalut_url;

  		$.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
  			url: url,
	        type: 'GET',
	        success: function(data) {
            self.set_order_data(data);
            $('#loader-wrapper').hide();
	        },
	        error: function(error) {
	          console.log(error);
            $('#loader-wrapper').hide();
	        }
      })
    },
    getUserReports(id, url) {
      $('#loader-wrapper').show();
      var self = this;
      var defalut_url = "/user/api/report_list/" + id + "/" + this.tab + this.current_page[this.current_url];
      url = url !== undefined ? url : defalut_url;
      console.log(url);
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_report_data(data);
            $('#loader-wrapper').hide();
          },
          error: function(error) {
            console.log(error);
            $('#loader-wrapper').hide();
          }
      })
    },
    getUserReviews(id, url) {
      $('#loader-wrapper').show();
      var self = this;
      var defalut_url = "/user/api/reviews_list/" + id + "/" + this.tab + this.current_page[this.current_url];
      url = url !== undefined ? url : defalut_url;
      console.log(url);
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_reviews_data(data);
            $('#loader-wrapper').hide();

            setTimeout(function(){
              self.getRaiting();
            }, 100)
          },
          error: function(error) {
            console.log(error);
            $('#loader-wrapper').hide();
          }
      })
    },
    getUserLogs(id, url) {
      $('#loader-wrapper').show();
      var self = this;
      var defalut_url = "/user/api/user_logs/" + id + "/" + this.tab + this.current_page[this.current_url];;
      url = url !== undefined ? url : defalut_url;
      console.log(url);
      $.ajax({
        headers: {
          "X-CSRFToken": this.csrf
        },
        xhrFields: {
          withCredentials: true
        },
        url: url,
          type: 'GET',
          success: function(data) {
            self.set_user_log_data(data);
            $('#loader-wrapper').hide();
          },
          error: function(error) {
            console.log(error);
            $('#loader-wrapper').hide();
          }
      })
    },
    set_user_data(data) {
      this.user = data;
      var patronymic = data.patronymic ? data.patronymic: "";
      var last_name = data.last_name ? data.last_name: "";
      var first_name = data.first_name ? data.first_name: "";
      $('title').text(last_name + " " + first_name + " " + patronymic);
    },
    set_counts_data(data) {

    },
    set_order_data(data) {
      this[this.current_url] = data.results;
      this.set_pagination(data);
    },
    set_report_data(data) {
      this.report_list = data.results;
      this.set_pagination(data);
    },
    set_reviews_data(data) {
      this.reviews_list_user = data.results;
    },
    set_pagination(data) {
      this.pages[this.current_url] = {
        'has_previous': data.previous,
        'has_next': data.next,
        'num_pages': data.lastPage,
        'number': data.current,
        'previous_page_number': data.current - 1,
        'next_page_number': data.current + 1,
        'count': data.countItemsOnPage

      };
    },
    set_user_log_data(data) {
      this.logs = data.results;
      this.set_pagination(data);
    },
    set_query_params() {
      var page = this.current_page[this.current_url];
      if (typeof page == 'undefined') {
        page = "";
      }
      console.log(this.current_page);
      this.new_part = "/" + this.res['user_detail'] + "/?tab=" + this.current_url + page;
    },
    toPage(page) {
      this.current_page[this.current_url] = "&page=" + page;
      this.set_query_params();
      switch(this.current_url) {
        case 'report_list':
          var first_url_part = 'user';
          var newurl = '?para=hello&p=1';
          var url = "/" + first_url_part + "/api/" + this.current_url + this.new_part;
          this.getUserReports(null, url);
          break;
        case 'user_logs':
          var first_url_part = 'user';
          var url = "/" + first_url_part + "/api/" + this.current_url + this.new_part;
          this.getUserLogs(null, url);
          break;
        default:
          var first_url_part = 'order';
          var url = "/" + first_url_part + "/api/" + this.current_url + this.new_part;
          this.getUserOrders(null, url);
          break;
      }
      url = "/user/user_detail" + this.new_part;
      window.history.pushState({path:url},'',url);
    },
    openTab(tab) {
      this.current_url = tab
      this.set_query_params();
      var url = "/user/user_detail" + this.new_part;
      window.history.pushState({path:url},'',url);
      $('.user_tab').hide();
      $('.tabs__item').removeClass("tabs__item_active");
      $('#' + tab).show();
      $('#' + tab + '_tab').addClass("tabs__item_active");
      if(this.user_logs == "" && tab ==  "user_logs") {
        this.getUserLogs(this.res['user_detail']);
        return;
      }
      if(this.outside_diagnose == "" && tab ==  "outside_diagnose") {
        this.getUserOrders(this.res['user_detail']);
        return;
      }
      if(this.selection == "" && tab ==  "selection") {
        this.getUserOrders(this.res['user_detail']);
        return;
      }
      if(this.expert_day == "" && tab ==  "expert_day") {
        this.getUserOrders(this.res['user_detail']);
        return;
      }
      if(this.other == "" && tab ==  "other") {
        this.getUserOrders(this.res['user_detail']);
        return;
      }
      if(this.transferringord == "" && tab ==  "transferringord") {
        this.getUserOrders(this.res['user_detail']);
        return;
      }
      if(this.report_list == "" && tab ==  "report_list") {
        this.getUserReports(this.res['user_detail']);
        return;
      }
      if(this.reviews_list_user == "" && tab ==  "reviews_list_user") {
        this.getUserReviews(this.res['user_detail']);
      }

    },
    changeAutoFrom(id) {
      const self = this;
      $.ajax({
        url : `/order/change_auto_from/${id}/`,
        type: "POST",
        data : {csrfmiddlewaretoken: this.csrf},
        success: function(response) {
          if(self.current_url ==  "outside_diagnose") {
            self.getUserOrders(self.res['user_detail']);
            return;
          }
          if(self.current_url ==  "selection") {
            self.getUserOrders(self.res['user_detail']);
            return;
          }
          if(self.current_url ==  "expert_day") {
            self.getUserOrders(self.res['user_detail']);
            return;
          }
          if(self.current_url==  "other") {
            self.getUserOrders(self.res['user_detail']);
            return;
          }
          if(self.current_url==  "transferringord") {
            self.getUserOrders(self.res['user_detail']);
            return;
          }
          console.log("changeAutoFrom");
        }
      });
    }

  }
})
