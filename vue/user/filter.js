import Vue from 'vue';
import select2 from '../vue/select2-directive';
import filterMixin from '../mixins/filter';


var filter = new Vue({
  delimiters: ['${', '}'],
  el: '#filter',
  mixins: [filterMixin],
  data: {
    filials: [],
    groups: [],
    expert_levels: [],
    filials__id: '',
    groups__name: '',
    expert_level: '',
    filterFields: ['filials__id', 'groups__name', 'expert_level']
  },
  mounted: function() {
    this.loadInitial();
  },
  directives: {
    select2: select2
  },
  methods: {
    loadInitial() {
      var self = this;
      $.ajax({
        url: '/user/filter/' + this.uri,
        cache: false,
        method: 'GET',
        success: function (data) {
          var url = new URL(window.location.href);
          for (var key in data)
            self[key] = data[key]

          self.loading = false;
        },
        error: function (error) {
          self.loading = false;
          console.log(error);
        }
      })
    }
  }
})
