import Vue from 'vue';
import select2 from '../vue/select2-directive';
import serverValidation from '../mixins/server-validation';
import ru from "vee-validate/dist/locale/ru";
import funcs from '../mixins/funcs';
import axios from 'axios';

VeeValidate.Validator.addLocale(ru);
Vue.use(VeeValidate, {locale: 'ru'});


var app = new Vue({
  delimiters: ['${', '}'],
  el: '#user-edit',
  mixins: [serverValidation,funcs],
  data: {
    csrf: csrf,
    group: '',
    expertGroupId: expertGroupId,
    password: null,
    password_confirm: null,
    isNewUser: window.location.href.split('/').includes('new_user'),
    size: 10,
    equipment: {
      thickness_gauge: '',
      launch: '',
      launch_password: '',
      comp_diagnostic: '',
      gas_analyze: false,
      endoscope: false,
      straightedge: false,
      flashlight: false,
      flashlight_ultra_violet: false,
      mirror: false,
      currency_detector: false,
      glu_protector_tire: false,
      compression_tester: false
    }
  },
  directives: {
    select2: select2
  },
  mounted: function() {
    this.group = $('select[name=groups]').val();
    $(document).on('change', '.user-checkbox', function(){
      this.checked ? $(this).val(true) : $(this).val(false)
    })

    if(this.isNewUser){
      $('.user-thumb-edit').addClass('disabled');
    }

    $(document).ready(function(){
      var location = window.location.href;
      
      if(location.includes('edit_user') || location.includes('edit_self')){
        $('.readonly-switch input, .readonly-switch select').attr('readonly','readonly')
        $('.readonly-switch .control, .readonly-switch #upload-avatar-button, .readonly-switch #remove-avatar-button').addClass('readonly');
      }

      $('.user-checkbox').each(function(index, elem){
        $(elem).val() == 'True' ? $(elem).attr('checked', true) : $(elem).removeAttr('checked');
      })
    })
  },
  watch: {
    'group'(group) {
      this.$nextTick(function() {
        $('select').select2({});
      });
    }
  },
  methods: {
    validateBeforeSubmit(e) {
      $('.user-checkbox').each(function(index, elem){
        elem.checked ? $(elem).val(true) : $(elem).val(false);
      });
      
      this.$validator.validateAll().then((result) => {
        if (result) {
          var form = $("#form");
          var url = form.attr('action');
          var equipmentBlock = form.find('#expertEquipment');
          var formData = new FormData();
          let id = user_id;

          formData.append('csrfmiddlewaretoken', this.csrf);

          if(this.isNewUser){
            var formElement = document.querySelector("form");
            var formData = new FormData(formElement);
            
            this.submit(url, formData);
          }else{
            if(equipmentBlock.length > 0){
              var serializeData = $('form').serializeArray({
                checkboxesAsBools: true
              });
              var data = {};
              serializeData.forEach(item => data[item.name] = item.value)
            
              formData.append('data', JSON.stringify(data));
            }
            else if($('#id_filials').length >= 1 && equipmentBlock.length === 0){
              formData.append('data', JSON.stringify(this.equipment));
            }
            else{
              var formElement = document.querySelector("form");
              var formData = new FormData(formElement);
            }
  
            if($('#avatar').get(0).files.length > 0){
              let image = $('#avatar').get(0).files[0];
              let formAvatarData = new FormData();
  
              formAvatarData.append("avatar", image);
              formAvatarData.append("user_id", id);
  
              axios.post('/user/change-user-avatar-by-mobile-app/', formAvatarData).then(response => {
                this.submit(url, formData);
              });
            }
            else{
              axios.delete('/user/change-user-avatar-by-mobile-app?user_id=' + id).then(response => {
                this.submit(url, formData);
              });
            }
          }

        }
      })

    $.fn.serialize = function (options) {
        return $.param(this.serializeArray(options));
    };

    $.fn.serializeArray = function (options) {
        var o = $.extend({
            checkboxesAsBools: false
        }, options || {});

        var rselectTextarea = /select|textarea/i;
        var rinput = /text|hidden|password|search|number/i;

        return this.map(function () {
            return this.elements ? $.makeArray(this.elements) : this;
        })
        .filter(function () {
            return this.name && !this.disabled &&
                (this.checked
                || (o.checkboxesAsBools && this.type === 'checkbox')
                || rselectTextarea.test(this.nodeName)
                || rinput.test(this.type));
            })
            .map(function (i, elem) {
                var val = $(this).val();
                return val == null ?
                null :
                $.isArray(val) ?
                $.map(val, function (val, i) {
                    return { name: elem.name, value: val };
                }) :
                {
                    name: elem.name,
                    value: (o.checkboxesAsBools && this.type === 'checkbox') ?
                        (this.checked ? true : false) :
                        val
                };
            }).get();
    };

    },
    generatePass(e) {
      let password = '';
      let CharacterSet = 'bdfghjkmnpqrstuvwyz123456789';
      for(let i=0; i < this.size; i++) {
        password += CharacterSet.charAt(Math.floor(Math.random() * CharacterSet.length));
      }
      this.password = password;
      this.password_confirm = password;
    }
  }
})
