import serverValidation from '../mixins/server-validation';

import VeeValidate from 'vee-validate';


Vue.use(VeeValidate, {locale: 'ru'});
Vue.config.devtools = true

new Vue({
    delimiters: ["${", "}"],
    el: "#client-support-page",
    mixins: [serverValidation],
    data: {
        csrf: csrf
    },
    mounted() {
        this.flagsFunc();
        this.getOrderDonePopupInfo();

        $(document).on('click', '.support_btn', function(){
            $.fancybox.close();
            $.fancybox.open({
                src: "#client-support-page",
                type: "inline",
                opts: {}
            });
        })
    },
    created() {
        $('.fieldset__error').hide();
    },
    methods: {
        validateBeforeSubmit(e, config = {}) {
            e.preventDefault();
            this.$validator.validateAll().then(result => {
                if (result) {
                    var formElement = e.path[0];
                    var formData = new FormData(formElement);
                    formData.append("csrfmiddlewaretoken", this.csrf);
                    var url = $(formElement).attr("action");
                    this.submit(url, formData, function () {
                        $.fancybox.close();
                        formElement.reset();
                        if (!config.noSuccess) {
                            try {
                                $.fancybox.open({
                                    src: "#call_success",
                                    type: "inline",
                                    opts: {}
                                });
                                $('[name="email"], [name="text"]').val('');
                                setTimeout(function () {
                                    $('#loader-wrapper').hide();
                                    $('.fieldset__error').hide();
                                    $.fancybox.close();
                                }, 2000);
                            } catch (e) {
                                console.log(e);
                            }
                        }
                    });
                } else {
                    $('.fieldset__error').show();
                }
            });
        },
        flagsFunc() {
            try {
                // func from https://авто-подбор.рф
                $(document).ready(function () {
                    $(".flags .selected").on("click", function () {
                        var list = $(this)
                            .closest(".flags")
                            .find(".lists");
                        if (list.hasClass("active")) {
                            list.removeClass("active").slideUp();
                        } else {
                            list.addClass("active").slideDown();
                        }
                    });
                    $(".flags .lists").on("click", "li", function () {
                        var place = $(this).attr("data-placeholder");
                        var mask = $(this).attr("data-mask");
                        var code = $(this).attr("data-iso-code");
                        $(this)
                            .closest(".flags")
                            .find(".selected .flag")
                            .attr("class", "flag")
                            .addClass(code);
                        $(this)
                            .closest(".control")
                            .find('input[name="tel"]')
                            .attr("placeholder", place)
                            .mask(mask);
                        $(this)
                            .closest(".flags")
                            .find(".lists")
                            .removeClass("active")
                            .slideUp();
                    });
                });
            } catch (e) {
                console.log(e);
            }
        },
        getOrderDonePopupInfo() {
            var that = this;

            var popup = $('#signupServiceRating');
            //var isShowedServicePopup = localStorage.getItem('signupServiceRating')
            if (popup.length > 0) {
                try {

                    if ($('.rating-starts').length) {
                        
                        $(document).on('change', '.rating-starts input[name="rating"]', function (e) {
                            var value = parseFloat($(e.target).val());
                            var inputReviewHolder = $('#review_text');
                            $('.btn_raiting').removeClass('disabled');
                            $('.raiting_msg').remove();
                            if (value <= 3) {
                                inputReviewHolder.attr('placeholder', 'Расскажите что мы можем улучшить в нашем сервисе. Мы ценим каждый отзыв от нашего клиента')
                            } else {
                                inputReviewHolder.attr('placeholder', 'Напишите Ваш отзыв')
                            }
                        });
                    }

                    $(document).on('click', '.btn_raiting', function(){
                        $('.btn_raiting').addClass('disabled');
                    })

                    $.ajax({
                        type: "POST",
                        url: "/order/api/get_order_done_popup_info/",
                        data: {csrfmiddlewaretoken: that.csrf}
                    })
                    .done(function (response) {

                        if (response["name"]) {
                            Object.keys(response).map(key => {
                                var value = response[key];
                                popup.find('input[name="' + key + '"]').val(value)
                            })
                           
                            //localStorage.setItem('signupServiceStation',true)
                        }
                        $.fancybox.open({src: '#signupServiceRating'});
                        $('#signupServiceRating').fadeIn(0);
                    })
                } catch (error) {
                    console.log('getOrderDonePopupInfo error', error)
                }
            }
        }
    }
});




