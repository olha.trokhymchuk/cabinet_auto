import Vue from 'vue';
import list from '../mixins/list';
import funcs from '../mixins/funcs';
import search from '../mixins/search';


new Vue({
  delimiters: ['${', '}'],
  el: '#users',
  mixins: [list, search, funcs],
  data: {
    deleteUrl: '/user/delete'
  },
  updated: function () {
    this.$nextTick(function () {
     // $('.phone_mask').mask('+7 (000) 000-0000');
     $('.phone_mask');
    })
  }
});
