function mq_resize() {
    var eventPhone = '';
    if (window.matchMedia('(min-width: 768px)').matches) {
        eventPhone = 'mq_phone_';
    } else {
        eventPhone = 'mq_phone';
    }
    if (window.matchMedia('(min-width: 1280px)').matches) {
        eventTablet = 'mq_tablet_';
    } else {
        eventTablet = 'mq_tablet';
    }
    // console.log(eventPhone + ' ' + eventTablet);
    $(document).trigger(eventPhone);
    $(document).trigger(eventTablet);
}

$(window).on('resize', $.debounce( 250, mq_resize ));

$(document).ready(function () {
    mq_resize();
});
