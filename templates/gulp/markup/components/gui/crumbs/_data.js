var data = {
    crumbsBrandDetail: [
        {
            title: 'Skinbutik.ru',
            link: 'index.html'
        },
        {
            title: 'Бренды',
            link: 'brands.html'
        }
    ],
    crumbsBlog: [
        {
            title: 'Skinbutik.ru',
            link: 'index.html'
        }
    ],
    crumbsBlogDetail: [
        {
            title: 'Skinbutik.ru',
            link: 'index.html'
        },
        {
            title: 'Блог',
            link: 'blog.html'
        }
    ],
    crumbsCatalog: [
        {
            title: 'Skinbutik.ru',
            link: 'index.html'
        },
        {
            title: 'Каталог',
            link: 'catalog.html'
        }
    ],
    crumbsNabor: [
        {
            title: 'Skinbutik.ru',
            link: 'index.html'
        },
        {
            title: 'Наборы',
            link: 'product-set.html'
        }
    ],
    crumbsProductDetail: [
        {
            title: 'Skinbutik.ru',
            link: 'index.html'
        },
        {
            title: 'Каталог',
            link: 'catalog.html'
        },
        {
            title: 'Название категории товара',
            link: 'catalog.html'
        }
    ],
    crumbsOrderDetail: [
    {
        title: 'Skinbutik.ru',
        link: 'index.html'
    },
    {
        title: 'Заказы',
        link: 'orders.html'
    }
]
};



