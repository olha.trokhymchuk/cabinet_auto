$('.dropdown__control').on('click', function () {
    $(this).parents('.dropdown').toggleClass('dropdown_open');
});

var handler = function (e) {
    // if the target is a descendent of container do nothing
    if ($(e.target).is('.dropdown, .dropdown *')) {
        return;
    }

    // remove event handler from document
    // $(document).off('click', handler);

    $('.dropdown_open').removeClass('dropdown_open');
};

$(document).on('click', handler);
