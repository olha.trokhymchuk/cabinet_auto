// todo все переписать, т.к. просто прототип для демонстрации

const notificationHtml = '<div class="notification__msg notification__msg_new"><div class="msg msg_notification msg_success"><div class="msg__content">test msg</div></div></div>';
const notificationContent = $('.notification__content');

notificationDemo = function () {
    notificationContent.prepend(notificationHtml);
    notificationShow();
};

notificationShow = function () {
    $('.notification__msg_new').slideDown(200).removeClass('notification__msg_new');
};

notificationHide = function (object) {
    $(object).slideUp(200);
};

$(document).on('click', '.notification__msg', function () {
    notificationHide(this);
});

$(document).on('click', '.js-demo-notification', function (e) {
    e.preventDefault();
    notificationDemo();
});
