scrollInit = function () {
    // console.log('scroll init');
    $('.scroll').each(function () {
        var scrollWidth = $(this).width();
        var scrollHeight = Math.round($(this).height());
        var contentHeight = Math.round($(this).find('.scroll__content').height());
        var heightDiff = contentHeight - scrollHeight;
        // console.log(scrollWidth + ' ' + scrollHeight + ' ' + contentHeight + ' ' + heightDiff);

        if (heightDiff > 0) {
            $(this).addClass('scroll_active scroll_bottom').find('.scroll__content').css('width', scrollWidth);
        } else {
            $(this).removeClass('scroll_active scroll_bottom');
        }

        $(this).find('.scroll__overflow').scroll(function () {
            var scrollPos = $(this).scrollTop();
            // console.log(scrollPos);
            if (scrollPos > 0) {
                $(this).parents('.scroll').addClass('scroll_top');
            }
            if (scrollPos < heightDiff) {
                $(this).parents('.scroll').addClass('scroll_bottom');
            }
            if (scrollPos >= heightDiff) {
                $(this).parents('.scroll').removeClass('scroll_bottom');
            }
            if (scrollPos === 0) {
                $(this).parents('.scroll').removeClass('scroll_top');
            }
        });

    });
};

scrollDestroy = function () {
    $('.scroll_active').each(function () {
        $(this).removeClass('scroll_active, scroll_bottom, scroll_top').find('.scroll__content').css('width', '');
    });
};

$(document).on('mq_tablet_', function () {
    scrollInit();
});

$(document).on('mq_tablet', function () {
    scrollDestroy();
});
