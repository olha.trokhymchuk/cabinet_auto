function flip(object) {
    var flip_box = $(object);
    var flip__front = $(object).find('.flip__front');
    var flip__back = $(object).find('.flip__back');
    if (flip_box.hasClass('flip_active')) {
        flip_box.removeClass('flip_active').addClass('flip_out');
        flip__back.one(animationsEvents, function () {
            flip_box.removeClass('flip_out');
        });
    } else {
        flip_box.addClass('flip_in');
        flip__front.one(animationsEvents, function () {
            flip_box.removeClass('flip_in').addClass('flip_active');
        });
    }
}
