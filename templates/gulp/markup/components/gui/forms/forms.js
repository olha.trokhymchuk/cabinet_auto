$(document).on('keyup', '.field input, .field textarea', function () {
    if ($(this).val()) {
        $(this).parents('.field').addClass('field_filled');
    }
});

$(document).on('focus', '.field input, .field select, .field textarea', function (e) {
    e.preventDefault();
    $(this).parents('.field').addClass('field_focus');
});

$(document).on('blur', '.field input, .field select, .field textarea', function (e) {
    e.preventDefault();
    $(this).parents('.field').removeClass('field_focus');
    if (!$(this).val()) {
        $(this).parents('.field').removeClass('field_filled');
    } else {
        $(this).parents('.field').addClass('field_filled');
    }
});
placeholders_init = function () {
    $('.field input, .field textarea').each(function () {
        if ($(this).val()) {
            $(this).parents('.field').addClass('field_filled');
        }
    });
};

$(document).ready(function () {
    placeholders_init();
});
