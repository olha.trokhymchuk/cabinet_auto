rightbarToggle = function () {
    const rightbar = $('.right-bar');
    const rightbarBox = $('.right-bar__box');
    const rightbarBtnToggle = $('.right-bar-toggle');

    if (rightbar.hasClass('right-bar_open')) {
        rightbarBtnToggle.removeClass('btn_active');
        $('html').removeClass('html_right-bar');
        rightbar.removeClass('right-bar_open').addClass('right-bar_out');
        rightbarBox.one(animationsEvents, function () {
            rightbar.removeClass('right-bar_out');
        });
    } else {
        rightbarBtnToggle.addClass('btn_active');
        $('html').addClass('html_right-bar');
        rightbar.addClass('right-bar_in');
        rightbarBox.one(animationsEvents, function () {
            rightbar.removeClass('right-bar_in').addClass('right-bar_open');
        });
    }
};

$('.right-bar-toggle, .right-bar__overlay').on('click', function () {
    rightbarToggle();
});
