var data = {
    project: {
        defaults: {
            slogan: 'bokarev',
            alt: 'bokarev',
            phone: '+7 (495) 777-77-77',
            phone_url: '84957777777',
            hours: 'bokarev',
            address: 'bokarev',
            entity: 'bokarev',
            mail: 'bokarev'
        }
    },
    head: {
        main: {
            pageTitle: 'bokarev',
            pageDescription: 'bokarev',
            pageKeywords: 'bokarev'
        }
    }
};
