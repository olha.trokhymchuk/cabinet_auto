var bodyWidth;
var popupOverlay;
var popup;
var popupContent;
var popupScroll;
var popupCloseBtn;

$(document).ready(function () {
    popupOverlay = $('.popup__overlay');
    popup = $('.popup');
    popupContent = $('.popup__content');
    bodyWidth = ($(window).width());
    popupScroll = $('.popup__scroll');
    popupCloseBtn = $('.popup__close');
});

$(window).on('resize', function () {
    bodyWidth = ($(window).width());
    // console.log(bodyWidth);
    if ($('.popup__content:visible').length > 0) {
        popupScroll.css({'height': popup.actual('height')});
        // console.log(popup.actual('height'));
    }
});

// функция, чтобы получил параметры из хэша
function hashParametersParse() {
    var hashParams = {};
    var e,
        a = /\+/g,  // Regex for replacing addition symbol with a space
        r = /([^&;=]+)=?([^&;]*)/g,
        d = function (s) {
            return decodeURIComponent(s.replace(a, ' '));
        },
        q = window.location.hash.substring(1);
    while ((e = r.exec(q))) {
        hashParams[d(e[1])] = d(e[2]);
    }
    return hashParams;
}

function popupOpen(srcType, content, hashChange, callback) {
    // если не указали hashChange, то считаем его равным 0
    if (typeof (hashChange) === 'undefined') {
        hashChange = 0;
    }

    // функция, которая будет вызываться после загрузки попапа - callback
    if (typeof (callback) === 'undefined') {
        callback = function () {
        };
    }

    if ($('.popup__content:visible').length > 0) {
        // если у нас уже показан один попап, то убираем старый попап и вызываем функцию показа нового попап - все дивы используются те же, ничего нового не создаем
        // console.log('popup out 1');
        popupContent.removeClass('popup__content_open').addClass('popup__content_out').one(animationsEvents, function () {
            // console.log('popup out 2');
            popupContent.removeClass('popup__content_out');
            // грузим новый попап
            popupOpen(srcType, content, hashChange, callback);
        });
        return;
    }

    if (srcType === 'url') {
        if (hashChange) {
            window.location.hash = '#popup_url_hash=' + content;
        }
        // грузим контент в попап по урлу, сам урл передан в content

        $.get(content, function (data) {
            // эта операция асинхронная, т.е. надо подождать пока подгрузиться контент, получить его в переменную, а потом показать попап, но уже напрямую передав туда содержимое
            popupOverlay.removeClass('popup__overlay_loading');
            popupOpen('text', data, hashChange, callback);
        });

        // выходим из функции
        return false;

        // т.к. аякс на локалхосте не работает, как костыль тут просто беру содержимое дива

    } else if (srcType === 'text') {
        // контент передан уже при вызове функции в content
        popupContentHtml = content;
    }

    // если сейчас на экране попапов нет
    popupContent.html(popupContentHtml);
    popupOpenAnimations();
    callback();
    return false;
}


function popupOpenAnimations() {
    if (popupOverlay.hasClass('popup__overlay_open')) {
        // если уже есть у оверлея этот класс, значит это второй попап и просто вызываем popupOpenCallback()
        popupOpenCallback();
    } else {
        // если это первый попап, то биндим эвент и даем state-open

        // лучше сначала вешать бинд, а потом уже давать класс. так надежнее сработает эвент, имхо.
        // one() - значит один раз биндим вызов функции, повторно подобный эвент ловится не будет. то что нам и надо.
        $('html').addClass('html_popup');
        $('body, .top-bar').css({'width': bodyWidth});
        popupScroll.css({'height': popup.actual('height')});
        popup.addClass('popup_active');

        popupOverlay.addClass('popup__overlay_in').one(animationsEvents, function () {
            popupOverlay.removeClass('popup__overlay_in').addClass('popup__overlay_open');
            popupCloseBtn.addClass('popup__close_in').one(animationsEvents, function () {
                popupCloseBtn.removeClass('popup__close_in').addClass('popup__close_open');
            });
            popupOpenCallback();
            placeholdersInit();
        });
    }
}

function popupOpenCallback() {
    popupContent.addClass('popup__content_in').one(animationsEvents, function () {
        popupContent.removeClass('popup__content_in').addClass('popup__content_open');
    });
}

function popupClose() {
    // просто скрываем элементы
    popupCloseAnimations();

    // на всякий случай обнуляем хэш
    hashParams = hashParametersParse();
    newHash = '';
    if (hashParams) {
        for (i in hashParams) {
            if (i === 'popup_url_hash') {
                continue;
            }

            newHash += i;

            if (hashParams[i]) {
                newHash += '=' + hashParams[i];
            }
        }
    }
    if (!newHash) {
        history.pushState('', document.title, window.location.pathname);
    } else {
        window.location.hash = newHash;
    }
    // очищать содержимое не стоит, лишняя операция и нагрузка.
}

function popupCloseAnimations() {
    popupCloseBtn.addClass('popup__close_out').removeClass('popup__close_open').one(animationsEvents, function () {
        popupCloseBtn.removeClass('popup__close_out');
    });
    popupContent.addClass('popup__content_out').removeClass('popup__content_open').one(animationsEvents, function () {
        popupContent.removeClass('popup__content_out');
        popupOverlay.removeClass('popup__overlay_open').addClass('popup__overlay_out').one(animationsEvents, function () {
            popupOverlay.removeClass('popup__overlay_out');
            $('html').removeClass('html_popup');
            $('body, .top-bar').css({'width': ''});
            popup.removeClass('popup_active');
        });
    });
}
