function formCallback() {
    var xmlHttp;
    function srvTime() {
        try {
            // FF, Opera, Safari, Chrome
            xmlHttp = new XMLHttpRequest();
        } catch (err1) {
            // IE
            try {
                xmlHttp = new ActiveXObject('Msxml2.XMLHTTP');
            } catch (err2) {
                try {
                    xmlHttp = new ActiveXObject('Microsoft.XMLHTTP');
                } catch (eerr3) {
                    // AJAX not supported, use CPU time.
                    // alert('AJAX not supported');
                }
            }
        }
        xmlHttp.open('HEAD', window.location.href.toString(), false);
        xmlHttp.setRequestHeader('Content-Type', 'text/html');
        xmlHttp.send('');
        return xmlHttp.getResponseHeader('Date');
    }

    const st = srvTime();
    const date = new Date(st);

    // var localTime = new Date();

    const hr = date.getHours();
    var popupMsg;

    if ((hr === 9) || (hr === 10) || (hr === 11) || (hr === 12) || (hr === 13) || (hr === 14) || (hr === 15) || (hr === 16) || (hr === 17) || (hr === 18) || (hr === 19) || (hr === 20) || (hr === 21) || (hr === 22) || (hr === 23)) {
        popupMsg = 'В ближайшее время наш менеджер свяжется с вами';
    }
    if ((hr === 0) || (hr === 1) || (hr === 2) || (hr === 3) || (hr === 4) || (hr === 5) || (hr === 6) || (hr === 7) || (hr === 8)) {
        popupMsg = 'Наш менеджер свяжется с вами в начале рабочего дня';
    }

    const form = $('.popup-callback__form');

    form.submit(function (e) {
        e.preventDefault();
        const formData = form.serialize();
        flip('.popup-callback');

        $.ajax({
            type: 'POST',
            url: form.attr('action'),
            data: formData
        })

            .done(function () {
                $('.popup-callback__fail').hide();
                $('.popup-callback__success .box__text').html(popupMsg);
                yaCounter40307895.reachGoal('callback_done');
            })

            .fail(function () {
                $('.popup-callback__success').hide();
                yaCounter40307895.reachGoal('callback_fail');
            });

    });
}
