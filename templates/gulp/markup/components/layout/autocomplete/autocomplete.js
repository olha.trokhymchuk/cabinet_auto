$(document).ready(function () {
    $('.search input').on('click', function () {
        autocompleteOpen();
    });
    $('.autocomplete__overlay').on('click', function () {
        autocompleteClose();
    });
});

function autocompleteOpen() {
    if (!$('html').hasClass('html_autocomplete')) {
        $('html').addClass('html_autocomplete');
        $('.autocomplete').addClass('autocomplete_in');
        $('.autocomplete__box').one(animationsEvents, function () {
            $('.autocomplete').removeClass('autocomplete_in').addClass('autocomplete_open');
        });
    }
}

function autocompleteClose() {
    if ($('html').hasClass('html_autocomplete')) {
        $('html').removeClass('html_autocomplete');
        $('.autocomplete').removeClass('autocomplete_open').addClass('autocomplete_out');
        $('.autocomplete__box').one(animationsEvents, function () {
            $('.autocomplete').removeClass('autocomplete_out');
        });
    }
}
