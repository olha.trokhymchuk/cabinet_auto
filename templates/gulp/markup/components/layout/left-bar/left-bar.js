
leftbarToggle = function () {
    const leftbar = $('.left-bar');
    const leftbarBox = $('.left-bar__box');
    const leftbarBtnToggle = $('.top-bar__mobile-menu');

    if (leftbar.hasClass('left-bar_open')) {
        leftbarBtnToggle.removeClass('btn_active');
        $('html').removeClass('html_left-bar');
        leftbar.removeClass('left-bar_open').addClass('left-bar_out');
        leftbarBox.one(animationsEvents, function () {
            leftbar.removeClass('left-bar_out');
        });
    } else {
        leftbarBtnToggle.addClass('btn_active');
        $('html').addClass('html_left-bar');
        leftbar.addClass('left-bar_in');
        leftbarBox.one(animationsEvents, function () {
            leftbar.removeClass('left-bar_in').addClass('left-bar_open');
        });
    }
};

$('.top-bar__mobile-menu, .left-bar__overlay').on('click', function () {
    leftbarToggle();
});


