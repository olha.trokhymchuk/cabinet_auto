var data = {
    reportsTable: [
        {
            id: '№1254',
            complete: '0%',
            type: 'Отчёт',
            date: 'от 20 января 2018',
            thumb: 'static/img/content/car_01.jpg',
            title: 'Mercedes-Benz E-klasse IV',
            vin: 'WDD21205*1A****56',
            user: 'Иванов Иван',
            status: 'новый',
            mod: 'mod'
        },
        {
            id: '№236544',
            complete: '30%',
            type: 'Отчёт',
            date: 'от 20 января 2018',
            thumb: 'static/img/content/car_01.jpg',
            title: 'Mercedes-Benz E-klasse IV',
            vin: 'WDD21205*1A****56',
            user: 'Иванов Иван',
            status: 'в работе',
            mod: 'mod'
        },
        {
            id: '№546543',
            complete: '76%',
            type: 'Перепроверка',
            date: 'от 20 января 2018',
            thumb: 'static/img/content/car_01.jpg',
            title: 'Audi A6 IV',
            vin: '2341205DASFASD2342',
            user: 'Петр Петров',
            status: 'ожидает проверки',
            result: 'рекомендован',
            mod: 'mod'
        },
        {
            id: '№83435',
            complete: '90%',
            type: 'Сдача',
            date: 'от 20 января 2018',
            thumb: 'static/img/content/car_01.jpg',
            title: 'BMW X5',
            vin: 'AS23823429304DSJKAK',
            user: 'Петр Петров',
            status: 'проверен',
            result: 'не рекомендован',
            mod: 'mod'
        },
        {
            id: '№527223',
            complete: '100%',
            type: 'Перепроверка',
            date: 'от 20 января 2018',
            thumb: 'static/img/content/car_01.jpg',
            title: 'Mercedes-Benz E-klasse IV',
            vin: 'WDD21205*1A****56',
            user: 'Петр Петров',
            status: 'проверен',
            result: 'не рекомендован',
            mod: 'mod'
        }
    ],
    reportView: [
        {
            title: 'Общая информация', //шаг отчета
        },
        {
            title: 'Юридическая проверка', //шаг отчета
            groups: [
                {
                    fields: [
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'ГИБДД', //заголовок поля
                            result: 'negative',
                            comment: 'содержание комментария к полю',
                            controls: [
                                {
                                    type: 'gallery',
                                    value: [
                                        {
                                            value: 'http://demo.os-cars.ru/static/lk/reports/FlmiWou4HZg.jpg'
                                        },
                                        {
                                            value: 'http://demo.os-cars.ru/static/lk/reports/hFgNOjxVyII.jpg'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'ФССП', //заголовок поля
                            controls: [
                                {
                                    type: 'gallery',
                                    value: [
                                        {
                                            value: 'static/img/content/car_01.jpg'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Автокод', //заголовок поля
                            controls: [
                                {
                                    type: 'gallery',
                                    value: [
                                        {
                                            value: 'static/img/content/car_01.jpg'
                                        },
                                        {
                                            value: 'static/img/content/car_01.jpg'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            title: 'Кузов', //шаг отчета
            comment: true,
            groups: [
                {
                    title: 'Внешние элементы',
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Передний бампер',
                            result: 'negative',
                            comment: 'содержание комментария к полю',
                            controls: [
                                {
                                    type: 'check',
                                    options: [
                                        {
                                            title: 'вмятина'
                                        },
                                        {
                                            title: 'царапина'
                                        }
                                    ],

                                },
                                {
                                    type: 'gallery',
                                    value: [
                                        {
                                            value: 'http://demo.os-cars.ru/static/lk/reports/common/IMG_1810_nY8e2GY.JPG'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Капот',
                            result: 'negative',
                            controls: [
                                {
                                    type: 'check',
                                    options: [
                                        {
                                            title: 'следы окрашивания'
                                        }
                                    ]
                                },
                                {
                                    type: 'gallery',
                                    value: [
                                        {
                                            value: 'http://demo.os-cars.ru/static/lk/reports/common/IMG_1847_1I0E8vC.JPG'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    title: 'Силовая часть',
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Телевизор',
                            result: 'negative',
                            controls: [
                                {
                                    type: 'check',
                                    options: [
                                        {
                                            title: 'вмятина'
                                        }
                                    ]
                                },
                                {
                                    type: 'gallery',
                                    value: [
                                        {
                                            value: 'static/img/content/car_01.jpg'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Правая передняя стойка',
                            result: 'negative',
                            controls: [
                                {
                                    type: 'check',
                                    options: [
                                        {
                                            title: 'следы окрашивания'
                                        }
                                    ]
                                },
                                {
                                    type: 'gallery',
                                    value: [
                                        {
                                            value: 'http://demo.os-cars.ru/static/lk/reports/common/IMG_1816.JPG'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            title: 'Рулевое управление, подвеска и тормоза', //шаг отчета
            comment: true,
            gallery: true,
            groups: [
                {
                    title: 'Передняя ось', //группа полей внутри шага
                    fields: [
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток тормозных колодок', //заголовок поля
                            result: 'negative',
                            comment: 'содержание комментария к полю',
                            controls: [
                                {
                                    type: 'range',
                                    value: '30%'
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток тормозных дисков', //заголовок поля
                            controls: [
                                {
                                    type: 'range',
                                    value: '80%'
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток передних шин', //заголовок поля
                            controls: [
                                {
                                    type: 'range',
                                    value: '75%'
                                }
                            ]
                        }
                    ]
                },
                {
                    title: 'Задняя ось', //группа полей внутри шага
                    fields: [
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток тормозных колодок', //заголовок поля
                            controls: [
                                {
                                    type: 'range',
                                    value: '90%'
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток тормозных дисков', //заголовок поля
                            controls: [
                                {
                                    type: 'range',
                                    value: '90%'
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток передних шин', //заголовок поля
                            controls: [
                                {
                                    type: 'range',
                                    value: '90%'
                                }
                            ]
                        }
                    ]
                },
                {
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Тормозная жидкость'
                        },
                        {
                            type: 'diagnosis',
                            title: 'Жидкость гидроусилителя'
                        }
                    ]
                },
                {
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Стуки, скрипы в подвеске'
                        },
                        {
                            type: 'diagnosis',
                            title: 'Люфты в рулевом управление',
                            result: 'negative',
                            comment: 'содержание комментария к полю'
                        },
                        {
                            type: 'diagnosis',
                            title: 'Отклонение от прямолинейного движение'
                        }
                    ]
                }
            ]
        },
        {
            title: 'Компьютерная диагностика', //шаг отчета
            comment: true,
            groups: [
                {
                    fields: [
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Чтение Vin из ЭБУ', //заголовок поля
                            result: 'negative',
                            comment: 'содержание комментария к полю'
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Связь с основным ЭБУ (ДВС, АКПП, SRS, Immobiliser, ABS/ESP)'
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Наличие ошибок в ЭБУ'
                        }
                    ]
                },
                {
                    fields: [
                        {
                            title: 'Количество прописанных ключей', //заголовок поля
                            controls: [
                                {
                                    type: 'input',
                                    value: '2'
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],
    reportRealData: [
        {
            title: 'Рулевое управление, подвеска и тормоза', //шаг отчета
            groups: [
                {
                    title: 'Передняя ось', //группа полей внутри шага
                    fields: [
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток тормозных колодок', //заголовок поля
                            controls: [
                                {
                                    type: 'range'
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток тормозных дисков', //заголовок поля
                            controls: [
                                {
                                    type: 'range'
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток передних шин', //заголовок поля
                            controls: [
                                {
                                    type: 'range'
                                }
                            ]
                        }
                    ]
                },
                {
                    title: 'Задняя ось', //группа полей внутри шага
                    fields: [
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток тормозных колодок', //заголовок поля
                            controls: [
                                {
                                    type: 'range'
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток тормозных дисков', //заголовок поля
                            controls: [
                                {
                                    type: 'range'
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',  //тип поля(информационное/диагностическое)
                            title: 'Остаток передних шин', //заголовок поля
                            controls: [
                                {
                                    type: 'range'
                                }
                            ]
                        }
                    ]
                },
                {
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Тормозная жидкость'
                        },
                        {
                            type: 'diagnosis',
                            title: 'Жидкость гидроусилителя'
                        }
                    ]
                },
                {
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Стуки, скрипы в подвеске'
                        },
                        {
                            type: 'diagnosis',
                            title: 'Люфты в рулевом управление',
                        },
                        {
                            type: 'diagnosis',
                            title: 'Отклонение от прямолинейного движение'
                        }
                    ]
                }
            ]
        }
    ],
    report: [
        {
            title: 'Наименование шага', //шаг отчета
            groups: [
                {
                    title: 'Информационные поля', //группа полей внутри шага
                    fields: [
                        {
                            type: 'info',  //тип поля(информационное/диагностическое)
                            title: 'Поле checkbox', //заголовок поля
                            controls: [
                                {
                                    type: 'check', //тип контрола(чебокс/радобатон/инпут/селект/ренж)
                                    options: [
                                        {
                                            title: 'option1' //опции контрола для чебокс/радобатон/селект
                                        },
                                        {
                                            title: 'option2'
                                        },
                                        {
                                            title: 'option3'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'info',
                            title: 'Поле radio',
                            controls: [
                                {
                                    type: 'radio',
                                    options: [
                                        {
                                            title: 'option1'
                                        },
                                        {
                                            title: 'option2'
                                        },
                                        {
                                            title: 'option3'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'info',
                            title: 'Поле input',
                            controls: [
                                {
                                    type: 'input'
                                }
                            ]
                        },
                        {
                            type: 'info',
                            title: 'Поле select',
                            controls: [
                                {
                                    type: 'select',
                                    options: [
                                        {
                                            title: 'option1'
                                        },
                                        {
                                            title: 'option2'
                                        },
                                        {
                                            title: 'option3'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'info',
                            title: 'Поле заливки изображений',
                            controls: [
                                {
                                    type: 'gallery'
                                }
                            ]
                        }
                    ]
                },
                {
                    title: 'Диагностические поля',
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Просто диагностическое поле'
                        },
                        {
                            type: 'diagnosis',
                            control: 'range',
                            title: 'Поле типа range',
                            controls: [
                                {
                                    type: 'range'
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Поле типа body',
                            controls: [
                                {
                                    type: 'check', //тип контрола(чебокс/радобатон/инпут/селект/ренж)
                                    negative: true,
                                    options: [
                                        {
                                            title: 'option1'
                                        },
                                        {
                                            title: 'option2'
                                        },
                                        {
                                            title: 'option3'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Навороченное поле с кучей контролов и зависимостью от positive/negative',
                            controls: [
                                {
                                    type: 'input', //тип контрола(чебокс/радобатон/инпут/селект/ренж)
                                },
                                {
                                    type: 'select', //тип контрола(чебокс/радобатон/инпут/селект/ренж)
                                    options: [
                                        {
                                            title: 'option1'
                                        },
                                        {
                                            title: 'option2'
                                        },
                                        {
                                            title: 'option3'
                                        }
                                    ]
                                },
                                {
                                    type: 'radio', //тип контрола(чебокс/радобатон/инпут/селект/ренж)
                                    negative: true,
                                    options: [
                                        {
                                            title: 'option1'
                                        },
                                        {
                                            title: 'option2'
                                        },
                                        {
                                            title: 'option3'
                                        }
                                    ]
                                },
                                {
                                    type: 'check', //тип контрола(чебокс/радобатон/инпут/селект/ренж)
                                    negative: true,
                                    options: [
                                        {
                                            title: 'option1'
                                        },
                                        {
                                            title: 'option2'
                                        },
                                        {
                                            title: 'option3'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],
    reportForm2: [
        {
            control: 'body',
            modItem: '',
            title: 'Передний бампер'
        },
        {
            control: 'body',
            modItem: '',
            title: 'Капот'
        },
        {
            control: 'body',
            modItem: '',
            title: 'Крыша'
        }
    ],
    reportForm: [
        {
            control: 'input',
            modItem: '',
            title: 'Поле типа input'
        },
        {
            control: 'select',
            modItem: '',
            title: 'Поле типа select'
        },
        {
            control: 'range',
            modItem: '',
            title: 'Поле типа range'
        },
        {
            control: '',
            modItem: '',
            title: 'Дефолтное поле'
        },
        {
            control: 'range',
            modItem: '',
            title: 'Передние тормозные колодки'
        },
        {
            control: 'range',
            modItem: '',
            title: 'Передние тормозные диски'
        },
        {

            control: 'range',
            modItem: '',
            title: 'Передние шины'
        },
        {
            divider: true,
            control: '',
            modItem: '',
            title: 'Задние тормозные колодки'
        },
        {
            control: '',
            modItem: '',
            title: 'Задние тормозные диски'
        },
        {
            control: '',
            modItem: '',
            title: 'Задние шины'
        },
        {
            divider: true,
            control: '',
            modItem: '',
            title: 'Тормозная жидкость'
        },
        {
            control: '',
            modItem: '',
            title: 'Жидкость гидроуселителя'
        },
        {
            divider: true,
            control: '',
            modItem: '',
            title: 'Стуки, скрипы в подвеске'
        },
        {
            control: '',
            modItem: '',
            title: 'Люфты в рулевом управление'
        },
        {
            control: '',
            modItem: '',
            title: 'Отклонение от прямолинейного движение'
        }
    ],


    reportNav: [
        {
            modItem: '',
            title: 'Общая информация',
            progress: '100'
        },
        {
            modItem: '',
            title: 'Юридическая проверка',
            progress: '60'
        },
        {
            modItem: 'report-nav__item_active',
            title: 'Кузов',
            progress: '100'
        },
        {
            modItem: '',
            title: 'Двигатель и трансмиссия',
            progress: '70'
        },
        {
            modItem: '',
            title: 'Рулевое управление, подвеска и тормоза',
            progress: '30'
        },
        {
            modItem: '',
            title: 'Компьютерная диагностика'
        },
        {
            modItem: '',
            title: 'Электрика и безопасность. Отопление и вентиляция'
        },
        {
            modItem: '',
            title: 'Состояние салона'
        },
        {
            modItem: '',
            title: 'Таблички и маркировки VIN'
        },
        {
            modItem: '',
            title: 'Осмотр на подьемнике'
        },
        {
            modItem: '',
            title: 'Дополнительное оборудование'
        },
        {
            modItem: '',
            title: 'Заключение эксперта'
        }
    ],

    reportNav1: [
        {
            modItem: 'report-nav__item_active',
            title: 'Привязка автомобиля',
            progress: '0'
        },
        {
            modItem: '',
            title: 'Общая информация'
        },
        {
            modItem: '',
            title: 'Юридическая проверка',
            progress: ''
        },
        {
            modItem: '',
            title: 'Кузов',
            progress: ''
        },
        {
            modItem: '',
            title: 'Двигатель и трансмиссия',
            progress: ''
        },
        {
            modItem: '',
            title: 'Рулевое управление, подвеска и тормоза',
            progress: ''
        },
        {
            modItem: '',
            title: 'Компьютерная диагностика'
        },
        {
            modItem: '',
            title: 'Электрика и безопасность. Отопление и вентиляция'
        },
        {
            modItem: '',
            title: 'Состояние салона'
        },
        {
            modItem: '',
            title: 'Таблички и маркировки VIN'
        },
        {
            modItem: '',
            title: 'Осмотр на подьемнике'
        },
        {
            modItem: '',
            title: 'Дополнительное оборудование'
        },
        {
            modItem: '',
            title: 'Заключение эксперта'
        }
    ],
    reportBody: [
        {
            title: 'Кузов', //шаг отчета
            type: 'body', //шаг отчета
            groups: [
                {
                    title: 'Внешние элементы',
                    scheme: '%=static=%img/general/report_body_1.png',
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Передний бампер',
                            top: '12%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Фара передняя левая',
                            top: '16%',
                            left: '38%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Запотевание'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Фара передняя правая',
                            top: '16%',
                            left: '62%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Запотевание'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Капот',
                            top: '28%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Переднее левое крыло',
                            top: '30%',
                            left: '25%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Переднее правое крыло',
                            top: '30%',
                            left: '75%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Передняя левая дверь',
                            top: '44%',
                            left: '22%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стекло переднее левое',
                            top: '48%',
                            left: '32%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Передняя правая дверь',
                            top: '44%',
                            left: '78%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стекло преднее правое',
                            top: '48%',
                            left: '68%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Запотевание'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Левый порог',
                            top: '50%',
                            left: '15%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Правый порог',
                            top: '50%',
                            left: '85%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стекло лобовое',
                            top: '40%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Крыша',
                            top: '50%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стекло заднее',
                            top: '68%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Задняя левая дверь',
                            top: '56%',
                            left: '22%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стекло заднее левое',
                            top: '57%',
                            left: '32%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Задняя правая дверь',
                            top: '56%',
                            left: '78%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стекло заднее правое',
                            top: '57%',
                            left: '68%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Заднее левое крыло',
                            top: '70%',
                            left: '25%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Заднее правое крыло',
                            top: '70%',
                            left: '75%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Багажник',
                            top: '75%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Фонарь задний левый',
                            top: '82%',
                            left: '40%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Запотевание'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Фонарь задний правый',
                            top: '82%',
                            left: '60%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Скол'
                                        },
                                        {
                                            title: 'Трещина'
                                        },
                                        {
                                            title: 'Запотевание'
                                        },
                                        {
                                            title: 'Заменено'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Задний бампер',
                            top: '86%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    title: 'Силовые элементы',
                    scheme: '%=static=%img/general/report_body_2.png',
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Передний лонжерон левый',
                            top: '16%',
                            left: '41%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Передний лонжерон правый',
                            top: '16%',
                            left: '59%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Чашка передняя левая',
                            top: '23%',
                            left: '38%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Чашка передняя правая',
                            top: '23%',
                            left: '62%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Моторный щит',
                            top: '25%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Центральный тоннель',
                            top: '35%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Пол',
                            top: '42%',
                            left: '46%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стойка передняя левая',
                            top: '39%',
                            left: '25%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стойка передняя правая',
                            top: '39%',
                            left: '75%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стойка центральная левая',
                            top: '53%',
                            left: '20%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стойка центральная правая',
                            top: '53%',
                            left: '80%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стойка задняя левая',
                            top: '70%',
                            left: '25%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стойка задняя правая',
                            top: '70%',
                            left: '75%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Порог левый',
                            top: '50%',
                            left: '8%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Порог правый',
                            top: '50%',
                            left: '92%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Задняя чашка левая',
                            top: '69%',
                            left: '40%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Задняя чашка правая',
                            top: '69%',
                            left: '60%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Пол багажника',
                            top: '76%',
                            left: '50%',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ],
    reportScheme: [
        {
            title: 'Кузов схема', //шаг отчета
            groups: [
                {
                    title: 'Внешние элементы',
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Капот',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },

                        {
                            type: 'diagnosis',
                            title: 'Крыша',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },

                        {
                            type: 'diagnosis',
                            title: 'Багажник',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                },
                {
                    title: 'Силовые элементы',
                    fields: [
                        {
                            type: 'diagnosis',
                            title: 'Телевизор',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Лонжерон передний левый',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Лонжерон передний правый',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стойка передняя левая',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            type: 'diagnosis',
                            title: 'Стойка передняя правая',
                            controls: [
                                {
                                    type: 'check',
                                    negative: true,
                                    options: [
                                        {
                                            title: 'Окрашено'
                                        },
                                        {
                                            title: 'Скол/царапина'
                                        },
                                        {
                                            title: 'Вмятина'
                                        },
                                        {
                                            title: 'Коррозия/ржавчина'
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ]
        }
    ]
};



