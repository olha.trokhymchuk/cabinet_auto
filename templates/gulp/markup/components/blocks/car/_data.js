var data = {
    fieldsetCarBasic: [
        {
            title:'VIN',
            mod:'vin',
            type:'input'
        },
        {
            title:'дата выпуска',
            mod:'date',
            type:'input'
        },
        {
            title:'кол-во владельцев',
            mod:'owners',
            type:'input'
        },
        {
            title:'марка',
            mod:'brand',
            type:'select',
            options: [
                {
                    title: 'Audi'
                },
                {
                    title: 'Bmw'
                },
                {
                    title: 'Mercedes'
                }
            ]
        },
        {
            title:'модель',
            mod:'model',
            type:'select',
            options: [
                {
                    title: 'A1'
                },
                {
                    title: 'A3'
                },
                {
                    title: 'A4'
                },
                {
                    title: 'A5'
                },
                {
                    title: 'A6'
                },
                {
                    title: 'A7'
                },
                {
                    title: 'A8'
                }
            ]
        },
        {
            title:'поколение',
            mod:'generation',
            type:'select',
            options: [
                {
                    title: 'I'
                },
                {
                    title: 'II'
                },
                {
                    title: 'III'
                }
            ]
        },
        {
            title:'кузов',
            mod:'body',
            type:'select',
            options: [
                {
                    title: 'седан'
                },
                {
                    title: 'хэтчбэк'
                },
                {
                    title: 'универсал'
                }
            ]
        },
        {
            title:'цвет',
            mod:'color',
            type:'select',
            options: [
                {
                    title: 'белый'
                },
                {
                    title: 'черный'
                },
                {
                    title: 'серебро'
                }
            ]
        },
        {
            mod:'empty'
        },
        {
            title:'двигатель',
            mod:'engine-type',
            type:'select',
            options: [
                {
                    title: 'бензин'
                },
                {
                    title: 'дизель'
                },
                {
                    title: 'гибрид'
                },
                {
                    title: 'электро'
                }
            ]
        },
        {
            title:'объём(л)',
            mod:'engine-volume',
            type:'input'
        },
        {
            title:'мощность(л.с.)',
            mod:'engine-power',
            type:'input'
        },
        {
            title:'трансмиссия',
            mod:'gearbox-type',
            type:'select',
            options: [
                {
                    title: 'МКПП'
                },
                {
                    title: 'АКПП'
                },
                {
                    title: 'Робот'
                },
                {
                    title: 'Вариатор'
                }
            ]
        },
        {
            title:'привод',
            mod:'transmission',
            type:'select',
            options: [
                {
                    title: 'передний привод'
                },
                {
                    title: 'задний привод'
                },
                {
                    title: 'полный привод'
                }
            ]
        },
        {
            mod:'empty'
        },

        {
            title:'салон',
            mod:'interior-type',
            type:'select',
            options: [
                {
                    title: 'кожа'
                },
                {
                    title: 'велюр'
                },
                {
                    title: 'ткань'
                }
            ]
        },
        {
            title:'цвет салона',
            mod:'interior-color',
            type:'input'
        },
        {
            mod:'empty'
        },
        {
            title:'пробег',
            mod:'mileage',
            type:'input'
        },
        {
            title:'цена',
            mod:'price',
            type:'input'
        },
        {
            mod:'empty'
        },
        {
            title:'описание',
            mod:'description',
            type:'textarea'
        }
    ],
    fieldsetCarOwner: [
        {
            title:'тип продавца',
            mod:'owner-type',
            type:'select',
            options: [
                {
                    title: 'частное лицо'
                },
                {
                    title: 'компания'
                }
            ]
        },
        {
            title:'имя',
            mod:'owner-name',
            type:'input'
        },
        {
            title:'телефон',
            mod:'owner-phone',
            type:'input'
        },
        {
            title:'местоположение',
            mod:'owner-location',
            type:'input'
        },
        {
            title:'источник',
            mod:'referrer',
            type:'input'
        }
    ]
};


