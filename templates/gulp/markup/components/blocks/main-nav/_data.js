var data = {
    mainNav: [
        {
            title: 'Пользователи',
            link: 'users.html',
            linkAdd: 'user-edit.html',
            linkAddTitle: 'Добавить пользователя',
            count: '250',
        },
        {
            title: 'Филиалы',
            link: 'filials.html',
            linkAdd: 'filial-edit.html',
            linkAddTitle: 'Добавить филиал',
            count: '10',
        },
        {
            title: 'Автомобили',
            link: 'cars.html',
            linkAdd: 'car-edit.html',
            linkAddTitle: 'Добавить автомобиль',
            count: '5402',
        },
        {
            title: 'Заказы',
            link: 'orders.html',
            linkAdd: 'order-edit.html',
            linkAddTitle: 'Добавить заказ',
            count: '15',
        },
        {
            title: 'Отчёты',
            link: 'reports.html',
            linkAdd: 'report-edit.html',
            linkAddTitle: 'Добавить отчёт',
            count: '25',
        }
    ]
};
