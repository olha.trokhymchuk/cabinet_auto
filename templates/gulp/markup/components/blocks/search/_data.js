var data = {
    searchResults: [
        {
            title: 'Автомобили',
            count: '3',
            items: [
                {
                    title: 'Mercedes-Benz E-klasse IV <span class="search-results__hl">WDD212</span>05*1A****56',
                    link: 'link'
                },
                {
                    title: '<span class="search-results__hl">Audi</span> A6 KLSAIWDKA932392SDJK',
                    link: 'link'
                },
                {
                    title: 'BMW <span class="search-results__hl">X5</span> ASDAWDD212051A56',
                    link: 'link'
                }
            ]
        },
        {
            title: 'Отчёты',
            count: '1',
            items: [
                {
                    title: '<span class="search-results__hl">322</span> 20 октября 2017 Mercedes-Benz E-klasse IV WDD21205*1A****56',
                    link: 'link'
                }
            ]
        },
        {
            title: 'Заказы',
            count: '4',
            items: [
                {
                    title: '<span class="search-results__hl">777</span> 1 октября 2017 Наименование услуги',
                    link: 'link'
                },
                {
                    title: '<span class="search-results__hl">999</span> 13 августа 2017 Подбор под ключ',
                    link: 'link'
                },
                {
                    title: '<span class="search-results__hl">35</span> 5 января 2016 Выездная диагностика',
                    link: 'link'
                },
                {
                    title: '<span class="search-results__hl">69</span> 20 октября 2017',
                    link: 'link'
                }
            ]
        },
        {
            title: 'Пользователи',
            count: '2',
            items: [
                {
                    title: '<span class="search-results__hl">Бокарев</span> Денис den@bokarev.ru +7(985)768-62-42',
                    link: 'link'
                },
                {
                    title: 'Бокарев Денис <span class="search-results__hl">den@bokarev.ru</span> +7(985)768-62-42',
                    link: 'link'
                },
                {
                    title: 'Бокарев Денис den@bokarev.ru +7(985)768-<span class="search-results__hl">62-42</span>',
                    link: 'link'
                }
            ]
        }
    ]
};
