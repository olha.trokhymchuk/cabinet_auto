'use strict';

const gulp = tars.packages.gulp;
const runSequence = tars.packages.runSequence.use(gulp);

/**
 * Build dev-version (without watchers)
 */
module.exports = () => {
    return gulp.task('main:build-dev', function (cb) {
        tars.options.notify = false;
        tars.options.watch.isActive = this.seq.slice(-1)[0] === 'dev' ? true : false; // eslint-disable-line no-invalid-this

        runSequence(
            'service:clean',
            ['images:minify-svg', 'images:raster-svg'],
            [
                'css:compile-css', 'css:move-separate',
                'html:concat-mocks-data',
                'other:move-misc-files', 'other:move-fonts', 'other:move-assets',
                'images:move-content-img', 'images:move-plugins-img', 'images:move-general-img',
                'js:move-separate'
            ],
            [
                'js:processing',
                'html:compile-templates'
            ],
            cb
        );
    });
};
