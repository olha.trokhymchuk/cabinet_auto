'use strict';

// This is example of task function

const gulp = tars.packages.gulp;
const plumber = tars.packages.plumber;
const notifier = tars.helpers.notifier;
const gm = require('gulp-gm');

// const imagesFolderPath = `./markup/${tars.config.fs.staticFolderName}/${tars.config.fs.imagesFolderName}`;

// const tarsConfig = tars.config;
// Include browserSync, if you need to reload browser:
// const browserSync = tars.packages.browserSync;

/**
 * Task description
 */
module.exports = function () {
    return gulp.task('imgresize', function () {
        return gulp.src('./markup/static/img/_resize/**/*.*')
            .pipe(plumber({
                errorHandler: function (error) {
                    notifier.error('An error occurred while imgresize.', error);
                }
            }))
            // Do stuff here, like
            // .pipe(less())
            .pipe(gm(function (gmfile) {
                return gmfile.resize(null, 800).composite('./markup/static/img/watermark.png').gravity('Center').quality(90);
            }))

            .pipe(gulp.dest('./markup/static/img/content/'))

            // If you need to reload browser, uncomment the row below
            // .pipe(browserSync.reload({ stream:true }))


            .pipe(
                // You can change text of success message
                notifier.success('imgresize has been finished')
            );

        // You can return callback, if you can't return pipe
        // done(null);
    });
};
