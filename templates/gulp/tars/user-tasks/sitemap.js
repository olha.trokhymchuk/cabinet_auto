'use strict';

// This is example of task function

const gulp = tars.packages.gulp;
const gm = require('gulp-gm');
const sitemap = require('gulp-sitemap');

// const imagesFolderPath = `./markup/${tars.config.fs.staticFolderName}/${tars.config.fs.imagesFolderName}`;

// const tarsConfig = tars.config;
// Include browserSync, if you need to reload browser:
// const browserSync = tars.packages.browserSync;

/**
 * Task description
 */
module.exports = function () {
    return gulp.task('sitemap', function () {
        return gulp.src([
            './dev/*.html',
            '!./dev/popup-*.html'
        ], {
            read: false
        })
            .pipe(sitemap({
                siteUrl: 'http://rutoto.ru'
            }))
            .pipe(gulp.dest('./markup/static/misc'));
    });
};
