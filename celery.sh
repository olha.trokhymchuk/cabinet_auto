#!/usr/bin/env bash

#celery -A autopodbor -Q default worker -l info
/home/aleksey/.venv/bin/celery -A autopodbor worker --concurrency=1 -l info --queues=default,report,users,orders --purge --beat
