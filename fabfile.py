from fabric.api import *

import deploy.fabric.setup as setup

from fabric.utils import _AttributeDict

import os

env.project = _AttributeDict({
    'name': 'autodev',
    'username': 'autodev',  # group assumed to be the same
    'src_repo': 'git@gitlab.com:aleksey_chernov/cabinet.auto-podbor.rf.git',
    'reqs': 'requirements.txt',
    'src_dir': 'source_tmp',  # rel from home
    'src_web': 'autopodbor',  # rel from home
    'src_branch': 'develop',
    'pip_cache': '.pip-cache',  # rel path from home
    'venv': '.venv',
    'touch_file': '/tmp/aleksey.txt',

    'persistent_dirs': [
        {'media': [
            'cache',
            'ckeditor',
        ]},
    ],

    'links': [
        ('media', 'media')
    ]
})

env.project.home = os.path.join('/home', env.project.username)
env.project.pip = os.path.join(env.project.home, env.project.venv,
                               'bin', 'pip')
env.project.python = os.path.join(env.project.home, env.project.venv,
                                  'bin', 'python')


def co_branch():
    local('git checkout {branch}'.format(branch=env.project.src_branch))


def push():
    local('git push origin {branch}'.format(branch=env.project.src_branch))


@task
def prod():
    env.type = 'prod'
    env.hosts = [
        'aleksey@148.251.13.108'
    ]
    env.project.name = 'aleksey'
    env.project.username = 'aleksey'
    env.project.src_branch = 'master'
    env.project.home = os.path.join('/home', env.project.username)
    env.project.pip = os.path.join(env.project.home, env.project.venv,
                                   'bin', 'pip')
    env.project.python = os.path.join(env.project.home, env.project.venv,
                                      'bin', 'python')


@task
def dev():
    env.type = 'dev'
    env.hosts = [
        'autodev@148.251.13.108'
    ]
    env.project.name = 'autodev'
    env.project.username = 'autodev'
    env.project.src_branch = 'develop'  # git BRANCH!!!!!!!!
    env.project.home = os.path.join('/home', env.project.username)
    env.project.pip = os.path.join(env.project.home, env.project.venv,
                                   'bin', 'pip')
    env.project.python = os.path.join(env.project.home, env.project.venv,
                                      'bin', 'python')
    # co_branch()
    # push()


@task
def front():
    env.type = 'front'
    env.hosts = [
        'autofront@148.251.13.108'
    ]
    env.project.name = 'autofront'
    env.project.username = 'autofront'
    env.project.src_branch = 'develop2'
    env.project.home = os.path.join('/home', env.project.username)
    env.project.pip = os.path.join(env.project.home, env.project.venv,
                                   'bin', 'pip')
    env.project.python = os.path.join(env.project.home, env.project.venv,
                                      'bin', 'python')
    co_branch()
    push()


@task
def full_deploy():
    setup.full()


@task
def remove_image_list():
    f = open('image_list.tsv', 'r')
    out = open('image_list_success.tsv', 'r')
    read_out = out.read()
    index = 1
    success = []
    for line in f:
        print ('')
        print ('=========START==========')
        print ('')
        if line not in read_out:
            if len(line.split('/')) == 7:
                src = env.project.home + '/media/' + line
                with settings(warn_only=True):
                    print(src.split('/'))
                    print ('')
                    if len(src.split('/')) == 11 and src.split('/')[5] == 'ppk':
                        run('rm -rf %s' % os.path.dirname(src))
                        console_out = 'file %s removed! line:= %s' % (str(src), str(index))
                        print(console_out)
                        print ('')
                    else:
                        print ('line %s is empty! line:= %s' % (str(src), str(index)))
                        print ('')
                success.append(line)
            else:
                print ('line %s is empty! line:= %s' % (str(line), str(index)))
                print ('')
        if index >= 600000:
            break
        index += 1
        print ('')
        print ('=========END==========')
        print ('')
    f.close()
    out.close()
    out = open('image_list_success.tsv', 'a')
    out.writelines(success)
    out.close()


@task
def complete_deploy():
    setup.complete()


try:
    from fabfile_local import *
except ImportError:
    pass

try:
    from fabfile_local import modify

    modify(globals())
except ImportError:
    pass
