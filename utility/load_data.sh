#!/bin/bash

python manage.py loaddata ./auto/fixtures/MarkAuto.json
python manage.py loaddata ./auto/fixtures/ModelAuto.json
python manage.py loaddata ./auto/fixtures/Generation.json
python manage.py loaddata ./core/fixtures/Group.json
python manage.py loaddata ./notifications/fixtures/MailTemplate.json
python manage.py loaddata ./auto/fixtures/celery-beat.json

